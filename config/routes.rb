HeaderAd::Application.routes.draw do

  #get "corheaders/hbase"
  match 'corheaders/hbase/:headerid', to: 'corheaders#hbase', via: [:get]
  match 'corheaders/count/:headerid', to: 'corheaders#count', via: [:get]
  match 'corheaders/links/:headerid', to: 'corheaders#links', via: [:get]
  # Newsletter
  match 'corheaders/newsletter', to: 'corheaders#newsletter', via: [:get]
  # Analytics 
  match 'header/:id/analytics', to: 'headers#analytics',:as => 'analytics',via: [:get]
  match 'corheaders/renderjs/:headerid', to: 'corheaders#renderjs', via: [:get]

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  authenticated :user do
    root to: "headers#index", :as => "dashbord"
  end

  get "pages/index"
  
  get "/admin" => "admin/base#index", :as => "admin"

  namespace "admin" do

    resources :users

  end
  resources :headers do
  	get :preview, :on => :member
  end

  root to: "pages#index"

end
