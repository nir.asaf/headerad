class Link < ActiveRecord::Base
	belongs_to :header
	has_many :trackers
end
