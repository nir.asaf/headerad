class Header < ActiveRecord::Base
	belongs_to :user
	has_many :links
	has_many :trackers
	has_many :newsletters
	validates :name,:site, :presence => true
	validates :site, :format => URI::regexp(%w(http https))
	# validates :height,:split_header,:numeric => true
	mount_uploader :logo, LogoUploader

	def style_for_header
		style = "background-color:##{background};" if background
		style += "height: #{height}px;" if height
		if show_border
			style += "border-bottom:#{border_bottom}px solid;" if border_bottom
			style += "border-color: ##{border_color};" if border_color
			style += "box-shadow: ##{box_shadow}px;" if box_shadow
			style += "clear:both;"
		end
		style

	end
end
