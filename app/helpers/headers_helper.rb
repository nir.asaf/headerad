module HeadersHelper
	def headers_chart_data
		@header.links.uniq.map do |link|
			{
				link_id: link.id,
				count: @header.trackers.where("link_id=?",link.id).count
			}
		end
	end	
end
