class UserNotification < ActionMailer::Base
  default from: "your@defaultmail.com"

  def send_signup_email(user)
  	@user = user
    mail( :to => @user, :subject => 'Thanks for signing up for Headerad' )
  end
end
