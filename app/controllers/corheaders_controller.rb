class CorheadersController < ApplicationController
  before_action :cors_preflight_check
  after_action :cors_set_access_control_headers

# For all responses in this controller, return the CORS access control headers.

def cors_set_access_control_headers
  headers['Access-Control-Allow-Origin'] = '*'
  headers['Access-Control-Allow-Methods'] = 'GET'
  headers['Access-Control-Max-Age'] = "1728000"
end

# If this is a preflight OPTIONS request, then short-circuit the
# request, return only the necessary headers and return an empty
# text/plain.

def cors_preflight_check
  if request.method == :options
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version'
    headers['Access-Control-Max-Age'] = '1728000'
    render :text => '', :content_type => 'text/plain'
  end
end

def hbase
	@data = Header.find(params[:headerid])
	respond_to do |format|
		format.json { render json: @data }
    end
end
def count
	@link = Header.find(params[:headerid]).links.where("links=?",params[:link]).first
	@link.trackers.create(:header_id => params[:headerid])
	respond_to do |format|
		format.json {render json: @target}
	end
end
def links
	@header = Header.find(params[:headerid])
	if @header.links.length < 1
		params.map { |k,v| v }.uniq.each do |name|
			@header.links.create(:links => name) unless name.mb_chars.length > 40 || name.index("http").nil?
		end
	end
	respond_to do |format|
		format.json {render json: @header }
	end

end
def newsletter
	UserNotification.send_signup_email(params[:email]).deliver
	Newsletter.create(:email => params[:email],:header_id => params[:headerid])
	respond_to do |format|
		format.html {render nothing: true}
	end
end
def renderjs
@h = Header.find(params[:headerid])
@host_name = env["HTTP_HOST"]
respond_to do |format|
format.js
end
end
end
