class HeadersController < ApplicationController
  before_action :authenticate_user!
  before_action :find_header, :only => [:edit,:update,:preview,:show]

  def new
    @header = current_user.headers.new
  end

  def create
    @header = current_user.headers.new(header_params)
    if @header.save
      flash[:notice] = 'Header Ad successfully created'
      redirect_to header_path(@header.id)
    else
      render :action => :new
    end
  end

  def edit
  end

  def update
    if @header.update_attributes(header_params)
      flash[:notice] = 'Header Ad has been successfully updated'
      redirect_to header_path(@header)
    else
      render :action => :edit
    end
  end

  def show
  end

  def index
    @headers = current_user.headers
    @headers << Header.find_by_public(true) unless Header.find_by_public(true).nil?
    @host_name = request.env["HTTP_HOST"]
    @twitter_message = "Message "
  end

  def preview
  end

  def analytics
    @header = Header.find(params[:id])
    @tracker = @header.trackers.all
  end


  protected
  def find_header
    @header = Header.find params[:id]
  end

  private
  def header_params
    params.require(:header).permit(:name, :site, :height, :background, :logo, :split_header, :add_back_button,:border_bottom ,:show_border ,:border_color ,:box_shadow,:fb_page,:twitter_page,:header_text,:newsletter_email)
  end
end
