/*!
 * jQuery JavaScript Library v1.11.0
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-01-23T21:02Z
 */


(function( global, factory ) {

	if ( typeof module === "object" && typeof module.exports === "object" ) {
		// For CommonJS and CommonJS-like environments where a proper window is present,
		// execute the factory and get jQuery
		// For environments that do not inherently posses a window with a document
		// (such as Node.js), expose a jQuery-making factory as module.exports
		// This accentuates the need for the creation of a real window
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
}(typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Can't do this because several apps including ASP.NET trace
// the stack via arguments.caller.callee and Firefox dies if
// you try to trace through "use strict" call chains. (#13335)
// Support: Firefox 18+
//

var deletedIds = [];

var slice = deletedIds.slice;

var concat = deletedIds.concat;

var push = deletedIds.push;

var indexOf = deletedIds.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var trim = "".trim;

var support = {};



var
	version = "1.11.0",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {
		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Make sure we trim BOM and NBSP (here's looking at you, Safari 5.0 and IE)
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

	// Matches dashed string for camelizing
	rmsPrefix = /^-ms-/,
	rdashAlpha = /-([\da-z])/gi,

	// Used by jQuery.camelCase as callback to replace()
	fcamelCase = function( all, letter ) {
		return letter.toUpperCase();
	};

jQuery.fn = jQuery.prototype = {
	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// Start with an empty selector
	selector: "",

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {
		return num != null ?

			// Return a 'clean' array
			( num < 0 ? this[ num + this.length ] : this[ num ] ) :

			// Return just the object
			slice.call( this );
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;
		ret.context = this.context;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	// (You can seed the arguments with an array of args, but this is
	// only used internally.)
	each: function( callback, args ) {
		return jQuery.each( this, callback, args );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map(this, function( elem, i ) {
			return callback.call( elem, i, elem );
		}));
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[j] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor(null);
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: deletedIds.sort,
	splice: deletedIds.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var src, copyIsArray, copy, name, options, clone,
		target = arguments[0] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
		target = {};
	}

	// extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {
		// Only deal with non-null/undefined values
		if ( (options = arguments[ i ]) != null ) {
			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)) ) ) {
					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && jQuery.isArray(src) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject(src) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend({
	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	// See test/unit/core.js for details concerning isFunction.
	// Since version 1.3, DOM methods and functions like alert
	// aren't supported. They return false on IE (#2968).
	isFunction: function( obj ) {
		return jQuery.type(obj) === "function";
	},

	isArray: Array.isArray || function( obj ) {
		return jQuery.type(obj) === "array";
	},

	isWindow: function( obj ) {
		/* jshint eqeqeq: false */
		return obj != null && obj == obj.window;
	},

	isNumeric: function( obj ) {
		// parseFloat NaNs numeric-cast false positives (null|true|false|"")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		return obj - parseFloat( obj ) >= 0;
	},

	isEmptyObject: function( obj ) {
		var name;
		for ( name in obj ) {
			return false;
		}
		return true;
	},

	isPlainObject: function( obj ) {
		var key;

		// Must be an Object.
		// Because of IE, we also have to check the presence of the constructor property.
		// Make sure that DOM nodes and window objects don't pass through, as well
		if ( !obj || jQuery.type(obj) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
			return false;
		}

		try {
			// Not own constructor property must be Object
			if ( obj.constructor &&
				!hasOwn.call(obj, "constructor") &&
				!hasOwn.call(obj.constructor.prototype, "isPrototypeOf") ) {
				return false;
			}
		} catch ( e ) {
			// IE8,9 Will throw exceptions on certain host objects #9897
			return false;
		}

		// Support: IE<9
		// Handle iteration over inherited properties before own properties.
		if ( support.ownLast ) {
			for ( key in obj ) {
				return hasOwn.call( obj, key );
			}
		}

		// Own properties are enumerated firstly, so to speed up,
		// if last one is own, then all properties are own.
		for ( key in obj ) {}

		return key === undefined || hasOwn.call( obj, key );
	},

	type: function( obj ) {
		if ( obj == null ) {
			return obj + "";
		}
		return typeof obj === "object" || typeof obj === "function" ?
			class2type[ toString.call(obj) ] || "object" :
			typeof obj;
	},

	// Evaluates a script in a global context
	// Workarounds based on findings by Jim Driscoll
	// http://weblogs.java.net/blog/driscoll/archive/2009/09/08/eval-javascript-global-context
	globalEval: function( data ) {
		if ( data && jQuery.trim( data ) ) {
			// We use execScript on Internet Explorer
			// We use an anonymous function so that context is window
			// rather than jQuery in Firefox
			( window.execScript || function( data ) {
				window[ "eval" ].call( window, data );
			} )( data );
		}
	},

	// Convert dashed to camelCase; used by the css and data modules
	// Microsoft forgot to hump their vendor prefix (#9572)
	camelCase: function( string ) {
		return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
	},

	nodeName: function( elem, name ) {
		return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
	},

	// args is for internal usage only
	each: function( obj, callback, args ) {
		var value,
			i = 0,
			length = obj.length,
			isArray = isArraylike( obj );

		if ( args ) {
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback.apply( obj[ i ], args );

					if ( value === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					value = callback.apply( obj[ i ], args );

					if ( value === false ) {
						break;
					}
				}
			}

		// A special, fast, case for the most common use of each
		} else {
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback.call( obj[ i ], i, obj[ i ] );

					if ( value === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					value = callback.call( obj[ i ], i, obj[ i ] );

					if ( value === false ) {
						break;
					}
				}
			}
		}

		return obj;
	},

	// Use native String.trim function wherever possible
	trim: trim && !trim.call("\uFEFF\xA0") ?
		function( text ) {
			return text == null ?
				"" :
				trim.call( text );
		} :

		// Otherwise use our own trimming functionality
		function( text ) {
			return text == null ?
				"" :
				( text + "" ).replace( rtrim, "" );
		},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArraylike( Object(arr) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		var len;

		if ( arr ) {
			if ( indexOf ) {
				return indexOf.call( arr, elem, i );
			}

			len = arr.length;
			i = i ? i < 0 ? Math.max( 0, len + i ) : i : 0;

			for ( ; i < len; i++ ) {
				// Skip accessing in sparse arrays
				if ( i in arr && arr[ i ] === elem ) {
					return i;
				}
			}
		}

		return -1;
	},

	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		while ( j < len ) {
			first[ i++ ] = second[ j++ ];
		}

		// Support: IE<9
		// Workaround casting of .length to NaN on otherwise arraylike objects (e.g., NodeLists)
		if ( len !== len ) {
			while ( second[j] !== undefined ) {
				first[ i++ ] = second[ j++ ];
			}
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var value,
			i = 0,
			length = elems.length,
			isArray = isArraylike( elems ),
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArray ) {
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// Bind a function to a context, optionally partially applying any
	// arguments.
	proxy: function( fn, context ) {
		var args, proxy, tmp;

		if ( typeof context === "string" ) {
			tmp = fn[ context ];
			context = fn;
			fn = tmp;
		}

		// Quick check to determine if target is callable, in the spec
		// this throws a TypeError, but we will just return undefined.
		if ( !jQuery.isFunction( fn ) ) {
			return undefined;
		}

		// Simulated bind
		args = slice.call( arguments, 2 );
		proxy = function() {
			return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
		};

		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || jQuery.guid++;

		return proxy;
	},

	now: function() {
		return +( new Date() );
	},

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
});

// Populate the class2type map
jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(i, name) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
});

function isArraylike( obj ) {
	var length = obj.length,
		type = jQuery.type( obj );

	if ( type === "function" || jQuery.isWindow( obj ) ) {
		return false;
	}

	if ( obj.nodeType === 1 && length ) {
		return true;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v1.10.16
 * http://sizzlejs.com/
 *
 * Copyright 2013 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-01-13
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	compile,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + -(new Date()),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// General-purpose constants
	strundefined = typeof undefined,
	MAX_NEGATIVE = 1 << 31,

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf if we can't use a native one
	indexOf = arr.indexOf || function( elem ) {
		var i = 0,
			len = this.length;
		for ( ; i < len; i++ ) {
			if ( this[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// Whitespace characters http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",
	// http://www.w3.org/TR/css3-syntax/#characters
	characterEncoding = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

	// Loosely modeled on CSS identifier characters
	// An unquoted value should be a CSS identifier http://www.w3.org/TR/css3-selectors/#attribute-selectors
	// Proper syntax: http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = characterEncoding.replace( "w", "w#" ),

	// Acceptable operators http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + characterEncoding + ")" + whitespace +
		"*(?:([*^$|!~]?=)" + whitespace + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + identifier + ")|)|)" + whitespace + "*\\]",

	// Prefer arguments quoted,
	//   then not containing pseudos/brackets,
	//   then attribute selectors/non-parenthetical expressions,
	//   then anything else
	// These preferences are here to reduce the number of selectors
	//   needing tokenize in the PSEUDO preFilter
	pseudos = ":(" + characterEncoding + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + attributes.replace( 3, 8 ) + ")*)|.*)\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + characterEncoding + ")" ),
		"CLASS": new RegExp( "^\\.(" + characterEncoding + ")" ),
		"TAG": new RegExp( "^(" + characterEncoding.replace( "w", "w*" ) + ")" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,
	rescape = /'|\\/g,

	// CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	};

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var match, elem, m, nodeType,
		// QSA vars
		i, groups, old, nid, newContext, newSelector;

	if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
		setDocument( context );
	}

	context = context || document;
	results = results || [];

	if ( !selector || typeof selector !== "string" ) {
		return results;
	}

	if ( (nodeType = context.nodeType) !== 1 && nodeType !== 9 ) {
		return [];
	}

	if ( documentIsHTML && !seed ) {

		// Shortcuts
		if ( (match = rquickExpr.exec( selector )) ) {
			// Speed-up: Sizzle("#ID")
			if ( (m = match[1]) ) {
				if ( nodeType === 9 ) {
					elem = context.getElementById( m );
					// Check parentNode to catch when Blackberry 4.6 returns
					// nodes that are no longer in the document (jQuery #6963)
					if ( elem && elem.parentNode ) {
						// Handle the case where IE, Opera, and Webkit return items
						// by name instead of ID
						if ( elem.id === m ) {
							results.push( elem );
							return results;
						}
					} else {
						return results;
					}
				} else {
					// Context is not a document
					if ( context.ownerDocument && (elem = context.ownerDocument.getElementById( m )) &&
						contains( context, elem ) && elem.id === m ) {
						results.push( elem );
						return results;
					}
				}

			// Speed-up: Sizzle("TAG")
			} else if ( match[2] ) {
				push.apply( results, context.getElementsByTagName( selector ) );
				return results;

			// Speed-up: Sizzle(".CLASS")
			} else if ( (m = match[3]) && support.getElementsByClassName && context.getElementsByClassName ) {
				push.apply( results, context.getElementsByClassName( m ) );
				return results;
			}
		}

		// QSA path
		if ( support.qsa && (!rbuggyQSA || !rbuggyQSA.test( selector )) ) {
			nid = old = expando;
			newContext = context;
			newSelector = nodeType === 9 && selector;

			// qSA works strangely on Element-rooted queries
			// We can work around this by specifying an extra ID on the root
			// and working up from there (Thanks to Andrew Dupont for the technique)
			// IE 8 doesn't work on object elements
			if ( nodeType === 1 && context.nodeName.toLowerCase() !== "object" ) {
				groups = tokenize( selector );

				if ( (old = context.getAttribute("id")) ) {
					nid = old.replace( rescape, "\\$&" );
				} else {
					context.setAttribute( "id", nid );
				}
				nid = "[id='" + nid + "'] ";

				i = groups.length;
				while ( i-- ) {
					groups[i] = nid + toSelector( groups[i] );
				}
				newContext = rsibling.test( selector ) && testContext( context.parentNode ) || context;
				newSelector = groups.join(",");
			}

			if ( newSelector ) {
				try {
					push.apply( results,
						newContext.querySelectorAll( newSelector )
					);
					return results;
				} catch(qsaError) {
				} finally {
					if ( !old ) {
						context.removeAttribute("id");
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {Function(string, Object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
function assert( fn ) {
	var div = document.createElement("div");

	try {
		return !!fn( div );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( div.parentNode ) {
			div.parentNode.removeChild( div );
		}
		// release memory in IE
		div = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = attrs.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			( ~b.sourceIndex || MAX_NEGATIVE ) -
			( ~a.sourceIndex || MAX_NEGATIVE );

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== strundefined && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare,
		doc = node ? node.ownerDocument || node : preferredDoc,
		parent = doc.defaultView;

	// If no document and documentElement is available, return
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Set our document
	document = doc;
	docElem = doc.documentElement;

	// Support tests
	documentIsHTML = !isXML( doc );

	// Support: IE>8
	// If iframe document is assigned to "document" variable and if iframe has been reloaded,
	// IE will throw "permission denied" error when accessing "document" variable, see jQuery #13936
	// IE6-8 do not support the defaultView property so parent will be undefined
	if ( parent && parent !== parent.top ) {
		// IE11 does not have attachEvent, so all must suffer
		if ( parent.addEventListener ) {
			parent.addEventListener( "unload", function() {
				setDocument();
			}, false );
		} else if ( parent.attachEvent ) {
			parent.attachEvent( "onunload", function() {
				setDocument();
			});
		}
	}

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties (excepting IE8 booleans)
	support.attributes = assert(function( div ) {
		div.className = "i";
		return !div.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( div ) {
		div.appendChild( doc.createComment("") );
		return !div.getElementsByTagName("*").length;
	});

	// Check if getElementsByClassName can be trusted
	support.getElementsByClassName = rnative.test( doc.getElementsByClassName ) && assert(function( div ) {
		div.innerHTML = "<div class='a'></div><div class='a i'></div>";

		// Support: Safari<4
		// Catch class over-caching
		div.firstChild.className = "i";
		// Support: Opera<10
		// Catch gEBCN failure to find non-leading classes
		return div.getElementsByClassName("i").length === 2;
	});

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( div ) {
		docElem.appendChild( div ).id = expando;
		return !doc.getElementsByName || !doc.getElementsByName( expando ).length;
	});

	// ID find and filter
	if ( support.getById ) {
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== strundefined && documentIsHTML ) {
				var m = context.getElementById( id );
				// Check parentNode to catch when Blackberry 4.6 returns
				// nodes that are no longer in the document #6963
				return m && m.parentNode ? [m] : [];
			}
		};
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
	} else {
		// Support: IE6/7
		// getElementById is not reliable as a find shortcut
		delete Expr.find["ID"];

		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== strundefined && elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== strundefined ) {
				return context.getElementsByTagName( tag );
			}
		} :
		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== strundefined && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See http://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( doc.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( div ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// http://bugs.jquery.com/ticket/12359
			div.innerHTML = "<select t=''><option selected=''></option></select>";

			// Support: IE8, Opera 10-12
			// Nothing should be selected when empty strings follow ^= or $= or *=
			if ( div.querySelectorAll("[t^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !div.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}
		});

		assert(function( div ) {
			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = doc.createElement("input");
			input.setAttribute( "type", "hidden" );
			div.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( div.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":enabled").length ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			div.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( div ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( div, "div" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( div, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully does not implement inclusive descendent
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === doc || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === doc || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf.call( sortInput, a ) - indexOf.call( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === doc ? -1 :
				b === doc ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf.call( sortInput, a ) - indexOf.call( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return doc;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch(e) {}
	}

	return Sizzle( expr, document, null, [elem] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[5] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] && match[4] !== undefined ) {
				match[2] = match[4];

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== strundefined && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, outerCache, node, diff, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) {
										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {
							// Seek `elem` from a previously-cached index
							outerCache = parent[ expando ] || (parent[ expando ] = {});
							cache = outerCache[ type ] || [];
							nodeIndex = cache[0] === dirruns && cache[1];
							diff = cache[0] === dirruns && cache[2];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									outerCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						// Use previously-cached element index if available
						} else if ( useCache && (cache = (elem[ expando ] || (elem[ expando ] = {}))[ type ]) && cache[0] === dirruns ) {
							diff = cache[1];

						// xml :nth-child(...) or :nth-last-child(...) or :nth(-last)?-of-type(...)
						} else {
							// Use the same loop as above to seek `elem` from the start
							while ( (node = ++nodeIndex && node && node[ dir ] ||
								(diff = nodeIndex = 0) || start.pop()) ) {

								if ( ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) && ++diff ) {
									// Cache the index of each encountered element
									if ( useCache ) {
										(node[ expando ] || (node[ expando ] = {}))[ type ] = [ dirruns, diff ];
									}

									if ( node === elem ) {
										break;
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf.call( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": function( elem ) {
			return elem.disabled === false;
		},

		"disabled": function( elem ) {
			return elem.disabled === true;
		},

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

function tokenize( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
}

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		checkNonElements = base && dir === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from dir caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});
						if ( (oldCache = outerCache[ dir ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							outerCache[ dir ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf.call( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf.call( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			return ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context !== document && context;
			}

			// Add elements passing elementMatchers directly to results
			// Keep `i` a string if there are no elements so `matchedCount` will be "00" below
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context, xml ) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// Apply set filters to unmatched elements
			matchedCount += i;
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, group /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !group ) {
			group = tokenize( selector );
		}
		i = group.length;
		while ( i-- ) {
			cached = matcherFromTokens( group[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );
	}
	return cached;
};

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function select( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		match = tokenize( selector );

	if ( !seed ) {
		// Try to minimize operations if there is only one group
		if ( match.length === 1 ) {

			// Take a shortcut and set the context if the root selector is an ID
			tokens = match[0] = match[0].slice( 0 );
			if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
					support.getById && context.nodeType === 9 && documentIsHTML &&
					Expr.relative[ tokens[1].type ] ) {

				context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
				if ( !context ) {
					return results;
				}
				selector = selector.slice( tokens.shift().value.length );
			}

			// Fetch a seed set for right-to-left matching
			i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
			while ( i-- ) {
				token = tokens[i];

				// Abort if we hit a combinator
				if ( Expr.relative[ (type = token.type) ] ) {
					break;
				}
				if ( (find = Expr.find[ type ]) ) {
					// Search, expanding context for leading sibling combinators
					if ( (seed = find(
						token.matches[0].replace( runescape, funescape ),
						rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
					)) ) {

						// If seed is empty or no tokens remain, we can return early
						tokens.splice( i, 1 );
						selector = seed.length && toSelector( tokens );
						if ( !selector ) {
							push.apply( results, seed );
							return results;
						}

						break;
					}
				}
			}
		}
	}

	// Compile and execute a filtering function
	// Provide `match` to avoid retokenization if we modified the selector above
	compile( selector, match )(
		seed,
		context,
		!documentIsHTML,
		results,
		rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
}

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome<14
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( div1 ) {
	// Should return 1, but returns 4 (following)
	return div1.compareDocumentPosition( document.createElement("div") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( div ) {
	div.innerHTML = "<a href='#'></a>";
	return div.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( div ) {
	div.innerHTML = "<input/>";
	div.firstChild.setAttribute( "value", "" );
	return div.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( div ) {
	return div.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;
jQuery.expr[":"] = jQuery.expr.pseudos;
jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;



var rneedsContext = jQuery.expr.match.needsContext;

var rsingleTag = (/^<(\w+)\s*\/?>(?:<\/\1>|)$/);



var risSimple = /^.[^:#\[\.,]*$/;

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( jQuery.isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			/* jshint -W018 */
			return !!qualifier.call( elem, i, elem ) !== not;
		});

	}

	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		});

	}

	if ( typeof qualifier === "string" ) {
		if ( risSimple.test( qualifier ) ) {
			return jQuery.filter( qualifier, elements, not );
		}

		qualifier = jQuery.filter( qualifier, elements );
	}

	return jQuery.grep( elements, function( elem ) {
		return ( jQuery.inArray( elem, qualifier ) >= 0 ) !== not;
	});
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	return elems.length === 1 && elem.nodeType === 1 ?
		jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [] :
		jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
			return elem.nodeType === 1;
		}));
};

jQuery.fn.extend({
	find: function( selector ) {
		var i,
			ret = [],
			self = this,
			len = self.length;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter(function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			}) );
		}

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		// Needed because $( selector, context ) becomes $( context ).find( selector )
		ret = this.pushStack( len > 1 ? jQuery.unique( ret ) : ret );
		ret.selector = this.selector ? this.selector + " " + selector : selector;
		return ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow(this, selector || [], false) );
	},
	not: function( selector ) {
		return this.pushStack( winnow(this, selector || [], true) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
});


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// Use the correct document accordingly with window argument (sandbox)
	document = window.document,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,

	init = jQuery.fn.init = function( selector, context ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector.charAt(0) === "<" && selector.charAt( selector.length - 1 ) === ">" && selector.length >= 3 ) {
				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && (match[1] || !context) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[1] ) {
					context = context instanceof jQuery ? context[0] : context;

					// scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[1],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[1] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {
							// Properties of context are called as methods if possible
							if ( jQuery.isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[2] );

					// Check parentNode to catch when Blackberry 4.6 returns
					// nodes that are no longer in the document #6963
					if ( elem && elem.parentNode ) {
						// Handle the case where IE and Opera return items
						// by name instead of ID
						if ( elem.id !== match[2] ) {
							return rootjQuery.find( selector );
						}

						// Otherwise, we inject the element directly into the jQuery object
						this.length = 1;
						this[0] = elem;
					}

					this.context = document;
					this.selector = selector;
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || rootjQuery ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this.context = this[0] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( jQuery.isFunction( selector ) ) {
			return typeof rootjQuery.ready !== "undefined" ?
				rootjQuery.ready( selector ) :
				// Execute immediately if ready is not present
				selector( jQuery );
		}

		if ( selector.selector !== undefined ) {
			this.selector = selector.selector;
			this.context = selector.context;
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,
	// methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.extend({
	dir: function( elem, dir, until ) {
		var matched = [],
			cur = elem[ dir ];

		while ( cur && cur.nodeType !== 9 && (until === undefined || cur.nodeType !== 1 || !jQuery( cur ).is( until )) ) {
			if ( cur.nodeType === 1 ) {
				matched.push( cur );
			}
			cur = cur[dir];
		}
		return matched;
	},

	sibling: function( n, elem ) {
		var r = [];

		for ( ; n; n = n.nextSibling ) {
			if ( n.nodeType === 1 && n !== elem ) {
				r.push( n );
			}
		}

		return r;
	}
});

jQuery.fn.extend({
	has: function( target ) {
		var i,
			targets = jQuery( target, this ),
			len = targets.length;

		return this.filter(function() {
			for ( i = 0; i < len; i++ ) {
				if ( jQuery.contains( this, targets[i] ) ) {
					return true;
				}
			}
		});
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			pos = rneedsContext.test( selectors ) || typeof selectors !== "string" ?
				jQuery( selectors, context || this.context ) :
				0;

		for ( ; i < l; i++ ) {
			for ( cur = this[i]; cur && cur !== context; cur = cur.parentNode ) {
				// Always skip document fragments
				if ( cur.nodeType < 11 && (pos ?
					pos.index(cur) > -1 :

					// Don't pass non-elements to Sizzle
					cur.nodeType === 1 &&
						jQuery.find.matchesSelector(cur, selectors)) ) {

					matched.push( cur );
					break;
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.unique( matched ) : matched );
	},

	// Determine the position of an element within
	// the matched set of elements
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[0] && this[0].parentNode ) ? this.first().prevAll().length : -1;
		}

		// index in selector
		if ( typeof elem === "string" ) {
			return jQuery.inArray( this[0], jQuery( elem ) );
		}

		// Locate the position of the desired element
		return jQuery.inArray(
			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[0] : elem, this );
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.unique(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter(selector)
		);
	}
});

function sibling( cur, dir ) {
	do {
		cur = cur[ dir ];
	} while ( cur && cur.nodeType !== 1 );

	return cur;
}

jQuery.each({
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return jQuery.dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return jQuery.dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return jQuery.dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return jQuery.sibling( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return jQuery.sibling( elem.firstChild );
	},
	contents: function( elem ) {
		return jQuery.nodeName( elem, "iframe" ) ?
			elem.contentDocument || elem.contentWindow.document :
			jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var ret = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			ret = jQuery.filter( selector, ret );
		}

		if ( this.length > 1 ) {
			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				ret = jQuery.unique( ret );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				ret = ret.reverse();
			}
		}

		return this.pushStack( ret );
	};
});
var rnotwhite = (/\S+/g);



// String to Object options format cache
var optionsCache = {};

// Convert String-formatted options into Object-formatted ones and store in cache
function createOptions( options ) {
	var object = optionsCache[ options ] = {};
	jQuery.each( options.match( rnotwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	});
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		( optionsCache[ options ] || createOptions( options ) ) :
		jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
		firing,
		// Last fire value (for non-forgettable lists)
		memory,
		// Flag to know if list was already fired
		fired,
		// End of the loop when firing
		firingLength,
		// Index of currently firing callback (modified by remove if needed)
		firingIndex,
		// First callback to fire (used internally by add and fireWith)
		firingStart,
		// Actual callback list
		list = [],
		// Stack of fire calls for repeatable lists
		stack = !options.once && [],
		// Fire callbacks
		fire = function( data ) {
			memory = options.memory && data;
			fired = true;
			firingIndex = firingStart || 0;
			firingStart = 0;
			firingLength = list.length;
			firing = true;
			for ( ; list && firingIndex < firingLength; firingIndex++ ) {
				if ( list[ firingIndex ].apply( data[ 0 ], data[ 1 ] ) === false && options.stopOnFalse ) {
					memory = false; // To prevent further calls using add
					break;
				}
			}
			firing = false;
			if ( list ) {
				if ( stack ) {
					if ( stack.length ) {
						fire( stack.shift() );
					}
				} else if ( memory ) {
					list = [];
				} else {
					self.disable();
				}
			}
		},
		// Actual Callbacks object
		self = {
			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {
					// First, we save the current length
					var start = list.length;
					(function add( args ) {
						jQuery.each( args, function( _, arg ) {
							var type = jQuery.type( arg );
							if ( type === "function" ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && type !== "string" ) {
								// Inspect recursively
								add( arg );
							}
						});
					})( arguments );
					// Do we need to add the callbacks to the
					// current firing batch?
					if ( firing ) {
						firingLength = list.length;
					// With memory, if we're not firing then
					// we should call right away
					} else if ( memory ) {
						firingStart = start;
						fire( memory );
					}
				}
				return this;
			},
			// Remove a callback from the list
			remove: function() {
				if ( list ) {
					jQuery.each( arguments, function( _, arg ) {
						var index;
						while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
							list.splice( index, 1 );
							// Handle firing indexes
							if ( firing ) {
								if ( index <= firingLength ) {
									firingLength--;
								}
								if ( index <= firingIndex ) {
									firingIndex--;
								}
							}
						}
					});
				}
				return this;
			},
			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ? jQuery.inArray( fn, list ) > -1 : !!( list && list.length );
			},
			// Remove all callbacks from the list
			empty: function() {
				list = [];
				firingLength = 0;
				return this;
			},
			// Have the list do nothing anymore
			disable: function() {
				list = stack = memory = undefined;
				return this;
			},
			// Is it disabled?
			disabled: function() {
				return !list;
			},
			// Lock the list in its current state
			lock: function() {
				stack = undefined;
				if ( !memory ) {
					self.disable();
				}
				return this;
			},
			// Is it locked?
			locked: function() {
				return !stack;
			},
			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( list && ( !fired || stack ) ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					if ( firing ) {
						stack.push( args );
					} else {
						fire( args );
					}
				}
				return this;
			},
			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},
			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


jQuery.extend({

	Deferred: function( func ) {
		var tuples = [
				// action, add listener, listener list, final state
				[ "resolve", "done", jQuery.Callbacks("once memory"), "resolved" ],
				[ "reject", "fail", jQuery.Callbacks("once memory"), "rejected" ],
				[ "notify", "progress", jQuery.Callbacks("memory") ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				then: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;
					return jQuery.Deferred(function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {
							var fn = jQuery.isFunction( fns[ i ] ) && fns[ i ];
							// deferred[ done | fail | progress ] for forwarding actions to newDefer
							deferred[ tuple[1] ](function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && jQuery.isFunction( returned.promise ) ) {
									returned.promise()
										.done( newDefer.resolve )
										.fail( newDefer.reject )
										.progress( newDefer.notify );
								} else {
									newDefer[ tuple[ 0 ] + "With" ]( this === promise ? newDefer.promise() : this, fn ? [ returned ] : arguments );
								}
							});
						});
						fns = null;
					}).promise();
				},
				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Keep pipe for back-compat
		promise.pipe = promise.then;

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 3 ];

			// promise[ done | fail | progress ] = list.add
			promise[ tuple[1] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(function() {
					// state = [ resolved | rejected ]
					state = stateString;

				// [ reject_list | resolve_list ].disable; progress_list.lock
				}, tuples[ i ^ 1 ][ 2 ].disable, tuples[ 2 ][ 2 ].lock );
			}

			// deferred[ resolve | reject | notify ]
			deferred[ tuple[0] ] = function() {
				deferred[ tuple[0] + "With" ]( this === deferred ? promise : this, arguments );
				return this;
			};
			deferred[ tuple[0] + "With" ] = list.fireWith;
		});

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( subordinate /* , ..., subordinateN */ ) {
		var i = 0,
			resolveValues = slice.call( arguments ),
			length = resolveValues.length,

			// the count of uncompleted subordinates
			remaining = length !== 1 || ( subordinate && jQuery.isFunction( subordinate.promise ) ) ? length : 0,

			// the master Deferred. If resolveValues consist of only a single Deferred, just use that.
			deferred = remaining === 1 ? subordinate : jQuery.Deferred(),

			// Update function for both resolve and progress values
			updateFunc = function( i, contexts, values ) {
				return function( value ) {
					contexts[ i ] = this;
					values[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( values === progressValues ) {
						deferred.notifyWith( contexts, values );

					} else if ( !(--remaining) ) {
						deferred.resolveWith( contexts, values );
					}
				};
			},

			progressValues, progressContexts, resolveContexts;

		// add listeners to Deferred subordinates; treat others as resolved
		if ( length > 1 ) {
			progressValues = new Array( length );
			progressContexts = new Array( length );
			resolveContexts = new Array( length );
			for ( ; i < length; i++ ) {
				if ( resolveValues[ i ] && jQuery.isFunction( resolveValues[ i ].promise ) ) {
					resolveValues[ i ].promise()
						.done( updateFunc( i, resolveContexts, resolveValues ) )
						.fail( deferred.reject )
						.progress( updateFunc( i, progressContexts, progressValues ) );
				} else {
					--remaining;
				}
			}
		}

		// if we're not waiting on anything, resolve the master
		if ( !remaining ) {
			deferred.resolveWith( resolveContexts, resolveValues );
		}

		return deferred.promise();
	}
});


// The deferred used on DOM ready
var readyList;

jQuery.fn.ready = function( fn ) {
	// Add the callback
	jQuery.ready.promise().done( fn );

	return this;
};

jQuery.extend({
	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Hold (or release) the ready event
	holdReady: function( hold ) {
		if ( hold ) {
			jQuery.readyWait++;
		} else {
			jQuery.ready( true );
		}
	},

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
		if ( !document.body ) {
			return setTimeout( jQuery.ready );
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );

		// Trigger any bound ready events
		if ( jQuery.fn.trigger ) {
			jQuery( document ).trigger("ready").off("ready");
		}
	}
});

/**
 * Clean-up method for dom ready events
 */
function detach() {
	if ( document.addEventListener ) {
		document.removeEventListener( "DOMContentLoaded", completed, false );
		window.removeEventListener( "load", completed, false );

	} else {
		document.detachEvent( "onreadystatechange", completed );
		window.detachEvent( "onload", completed );
	}
}

/**
 * The ready event handler and self cleanup method
 */
function completed() {
	// readyState === "complete" is good enough for us to call the dom ready in oldIE
	if ( document.addEventListener || event.type === "load" || document.readyState === "complete" ) {
		detach();
		jQuery.ready();
	}
}

jQuery.ready.promise = function( obj ) {
	if ( !readyList ) {

		readyList = jQuery.Deferred();

		// Catch cases where $(document).ready() is called after the browser event has already occurred.
		// we once tried to use readyState "interactive" here, but it caused issues like the one
		// discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15
		if ( document.readyState === "complete" ) {
			// Handle it asynchronously to allow scripts the opportunity to delay ready
			setTimeout( jQuery.ready );

		// Standards-based browsers support DOMContentLoaded
		} else if ( document.addEventListener ) {
			// Use the handy event callback
			document.addEventListener( "DOMContentLoaded", completed, false );

			// A fallback to window.onload, that will always work
			window.addEventListener( "load", completed, false );

		// If IE event model is used
		} else {
			// Ensure firing before onload, maybe late but safe also for iframes
			document.attachEvent( "onreadystatechange", completed );

			// A fallback to window.onload, that will always work
			window.attachEvent( "onload", completed );

			// If IE and not a frame
			// continually check to see if the document is ready
			var top = false;

			try {
				top = window.frameElement == null && document.documentElement;
			} catch(e) {}

			if ( top && top.doScroll ) {
				(function doScrollCheck() {
					if ( !jQuery.isReady ) {

						try {
							// Use the trick by Diego Perini
							// http://javascript.nwbox.com/IEContentLoaded/
							top.doScroll("left");
						} catch(e) {
							return setTimeout( doScrollCheck, 50 );
						}

						// detach all dom ready events
						detach();

						// and execute any waiting functions
						jQuery.ready();
					}
				})();
			}
		}
	}
	return readyList.promise( obj );
};


var strundefined = typeof undefined;



// Support: IE<9
// Iteration over object's inherited properties before its own
var i;
for ( i in jQuery( support ) ) {
	break;
}
support.ownLast = i !== "0";

// Note: most support tests are defined in their respective modules.
// false until the test is run
support.inlineBlockNeedsLayout = false;

jQuery(function() {
	// We need to execute this one support test ASAP because we need to know
	// if body.style.zoom needs to be set.

	var container, div,
		body = document.getElementsByTagName("body")[0];

	if ( !body ) {
		// Return for frameset docs that don't have a body
		return;
	}

	// Setup
	container = document.createElement( "div" );
	container.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px";

	div = document.createElement( "div" );
	body.appendChild( container ).appendChild( div );

	if ( typeof div.style.zoom !== strundefined ) {
		// Support: IE<8
		// Check if natively block-level elements act like inline-block
		// elements when setting their display to 'inline' and giving
		// them layout
		div.style.cssText = "border:0;margin:0;width:1px;padding:1px;display:inline;zoom:1";

		if ( (support.inlineBlockNeedsLayout = ( div.offsetWidth === 3 )) ) {
			// Prevent IE 6 from affecting layout for positioned elements #11048
			// Prevent IE from shrinking the body in IE 7 mode #12869
			// Support: IE<8
			body.style.zoom = 1;
		}
	}

	body.removeChild( container );

	// Null elements to avoid leaks in IE
	container = div = null;
});




(function() {
	var div = document.createElement( "div" );

	// Execute the test only if not already executed in another module.
	if (support.deleteExpando == null) {
		// Support: IE<9
		support.deleteExpando = true;
		try {
			delete div.test;
		} catch( e ) {
			support.deleteExpando = false;
		}
	}

	// Null elements to avoid leaks in IE.
	div = null;
})();


/**
 * Determines whether an object can have data
 */
jQuery.acceptData = function( elem ) {
	var noData = jQuery.noData[ (elem.nodeName + " ").toLowerCase() ],
		nodeType = +elem.nodeType || 1;

	// Do not set data on non-element DOM nodes because it will not be cleared (#8335).
	return nodeType !== 1 && nodeType !== 9 ?
		false :

		// Nodes accept data unless otherwise specified; rejection can be conditional
		!noData || noData !== true && elem.getAttribute("classid") === noData;
};


var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /([A-Z])/g;

function dataAttr( elem, key, data ) {
	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {

		var name = "data-" + key.replace( rmultiDash, "-$1" ).toLowerCase();

		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = data === "true" ? true :
					data === "false" ? false :
					data === "null" ? null :
					// Only convert to a number if it doesn't change the string
					+data + "" === data ? +data :
					rbrace.test( data ) ? jQuery.parseJSON( data ) :
					data;
			} catch( e ) {}

			// Make sure we set the data so it isn't changed later
			jQuery.data( elem, key, data );

		} else {
			data = undefined;
		}
	}

	return data;
}

// checks a cache object for emptiness
function isEmptyDataObject( obj ) {
	var name;
	for ( name in obj ) {

		// if the public data object is empty, the private is still empty
		if ( name === "data" && jQuery.isEmptyObject( obj[name] ) ) {
			continue;
		}
		if ( name !== "toJSON" ) {
			return false;
		}
	}

	return true;
}

function internalData( elem, name, data, pvt /* Internal Use Only */ ) {
	if ( !jQuery.acceptData( elem ) ) {
		return;
	}

	var ret, thisCache,
		internalKey = jQuery.expando,

		// We have to handle DOM nodes and JS objects differently because IE6-7
		// can't GC object references properly across the DOM-JS boundary
		isNode = elem.nodeType,

		// Only DOM nodes need the global jQuery cache; JS object data is
		// attached directly to the object so GC can occur automatically
		cache = isNode ? jQuery.cache : elem,

		// Only defining an ID for JS objects if its cache already exists allows
		// the code to shortcut on the same path as a DOM node with no cache
		id = isNode ? elem[ internalKey ] : elem[ internalKey ] && internalKey;

	// Avoid doing any more work than we need to when trying to get data on an
	// object that has no data at all
	if ( (!id || !cache[id] || (!pvt && !cache[id].data)) && data === undefined && typeof name === "string" ) {
		return;
	}

	if ( !id ) {
		// Only DOM nodes need a new unique ID for each element since their data
		// ends up in the global cache
		if ( isNode ) {
			id = elem[ internalKey ] = deletedIds.pop() || jQuery.guid++;
		} else {
			id = internalKey;
		}
	}

	if ( !cache[ id ] ) {
		// Avoid exposing jQuery metadata on plain JS objects when the object
		// is serialized using JSON.stringify
		cache[ id ] = isNode ? {} : { toJSON: jQuery.noop };
	}

	// An object can be passed to jQuery.data instead of a key/value pair; this gets
	// shallow copied over onto the existing cache
	if ( typeof name === "object" || typeof name === "function" ) {
		if ( pvt ) {
			cache[ id ] = jQuery.extend( cache[ id ], name );
		} else {
			cache[ id ].data = jQuery.extend( cache[ id ].data, name );
		}
	}

	thisCache = cache[ id ];

	// jQuery data() is stored in a separate object inside the object's internal data
	// cache in order to avoid key collisions between internal data and user-defined
	// data.
	if ( !pvt ) {
		if ( !thisCache.data ) {
			thisCache.data = {};
		}

		thisCache = thisCache.data;
	}

	if ( data !== undefined ) {
		thisCache[ jQuery.camelCase( name ) ] = data;
	}

	// Check for both converted-to-camel and non-converted data property names
	// If a data property was specified
	if ( typeof name === "string" ) {

		// First Try to find as-is property data
		ret = thisCache[ name ];

		// Test for null|undefined property data
		if ( ret == null ) {

			// Try to find the camelCased property
			ret = thisCache[ jQuery.camelCase( name ) ];
		}
	} else {
		ret = thisCache;
	}

	return ret;
}

function internalRemoveData( elem, name, pvt ) {
	if ( !jQuery.acceptData( elem ) ) {
		return;
	}

	var thisCache, i,
		isNode = elem.nodeType,

		// See jQuery.data for more information
		cache = isNode ? jQuery.cache : elem,
		id = isNode ? elem[ jQuery.expando ] : jQuery.expando;

	// If there is already no cache entry for this object, there is no
	// purpose in continuing
	if ( !cache[ id ] ) {
		return;
	}

	if ( name ) {

		thisCache = pvt ? cache[ id ] : cache[ id ].data;

		if ( thisCache ) {

			// Support array or space separated string names for data keys
			if ( !jQuery.isArray( name ) ) {

				// try the string as a key before any manipulation
				if ( name in thisCache ) {
					name = [ name ];
				} else {

					// split the camel cased version by spaces unless a key with the spaces exists
					name = jQuery.camelCase( name );
					if ( name in thisCache ) {
						name = [ name ];
					} else {
						name = name.split(" ");
					}
				}
			} else {
				// If "name" is an array of keys...
				// When data is initially created, via ("key", "val") signature,
				// keys will be converted to camelCase.
				// Since there is no way to tell _how_ a key was added, remove
				// both plain key and camelCase key. #12786
				// This will only penalize the array argument path.
				name = name.concat( jQuery.map( name, jQuery.camelCase ) );
			}

			i = name.length;
			while ( i-- ) {
				delete thisCache[ name[i] ];
			}

			// If there is no data left in the cache, we want to continue
			// and let the cache object itself get destroyed
			if ( pvt ? !isEmptyDataObject(thisCache) : !jQuery.isEmptyObject(thisCache) ) {
				return;
			}
		}
	}

	// See jQuery.data for more information
	if ( !pvt ) {
		delete cache[ id ].data;

		// Don't destroy the parent cache unless the internal data object
		// had been the only thing left in it
		if ( !isEmptyDataObject( cache[ id ] ) ) {
			return;
		}
	}

	// Destroy the cache
	if ( isNode ) {
		jQuery.cleanData( [ elem ], true );

	// Use delete when supported for expandos or `cache` is not a window per isWindow (#10080)
	/* jshint eqeqeq: false */
	} else if ( support.deleteExpando || cache != cache.window ) {
		/* jshint eqeqeq: true */
		delete cache[ id ];

	// When all else fails, null
	} else {
		cache[ id ] = null;
	}
}

jQuery.extend({
	cache: {},

	// The following elements (space-suffixed to avoid Object.prototype collisions)
	// throw uncatchable exceptions if you attempt to set expando properties
	noData: {
		"applet ": true,
		"embed ": true,
		// ...but Flash objects (which have this classid) *can* handle expandos
		"object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
	},

	hasData: function( elem ) {
		elem = elem.nodeType ? jQuery.cache[ elem[jQuery.expando] ] : elem[ jQuery.expando ];
		return !!elem && !isEmptyDataObject( elem );
	},

	data: function( elem, name, data ) {
		return internalData( elem, name, data );
	},

	removeData: function( elem, name ) {
		return internalRemoveData( elem, name );
	},

	// For internal use only.
	_data: function( elem, name, data ) {
		return internalData( elem, name, data, true );
	},

	_removeData: function( elem, name ) {
		return internalRemoveData( elem, name, true );
	}
});

jQuery.fn.extend({
	data: function( key, value ) {
		var i, name, data,
			elem = this[0],
			attrs = elem && elem.attributes;

		// Special expections of .data basically thwart jQuery.access,
		// so implement the relevant behavior ourselves

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = jQuery.data( elem );

				if ( elem.nodeType === 1 && !jQuery._data( elem, "parsedAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {
						name = attrs[i].name;

						if ( name.indexOf("data-") === 0 ) {
							name = jQuery.camelCase( name.slice(5) );

							dataAttr( elem, name, data[ name ] );
						}
					}
					jQuery._data( elem, "parsedAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each(function() {
				jQuery.data( this, key );
			});
		}

		return arguments.length > 1 ?

			// Sets one value
			this.each(function() {
				jQuery.data( this, key, value );
			}) :

			// Gets one value
			// Try to fetch any internally stored data first
			elem ? dataAttr( elem, key, jQuery.data( elem, key ) ) : undefined;
	},

	removeData: function( key ) {
		return this.each(function() {
			jQuery.removeData( this, key );
		});
	}
});


jQuery.extend({
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = jQuery._data( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || jQuery.isArray(data) ) {
					queue = jQuery._data( elem, type, jQuery.makeArray(data) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// not intended for public consumption - generates a queueHooks object, or returns the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return jQuery._data( elem, key ) || jQuery._data( elem, key, {
			empty: jQuery.Callbacks("once memory").add(function() {
				jQuery._removeData( elem, type + "queue" );
				jQuery._removeData( elem, key );
			})
		});
	}
});

jQuery.fn.extend({
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[0], type );
		}

		return data === undefined ?
			this :
			this.each(function() {
				var queue = jQuery.queue( this, type, data );

				// ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[0] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			});
	},
	dequeue: function( type ) {
		return this.each(function() {
			jQuery.dequeue( this, type );
		});
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},
	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = jQuery._data( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
});
var pnum = (/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;

var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHidden = function( elem, el ) {
		// isHidden might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;
		return jQuery.css( elem, "display" ) === "none" || !jQuery.contains( elem.ownerDocument, elem );
	};



// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = jQuery.access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		length = elems.length,
		bulk = key == null;

	// Sets many values
	if ( jQuery.type( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			jQuery.access( elems, fn, i, key[i], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !jQuery.isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {
			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < length; i++ ) {
				fn( elems[i], key, raw ? value : value.call( elems[i], i, fn( elems[i], key ) ) );
			}
		}
	}

	return chainable ?
		elems :

		// Gets
		bulk ?
			fn.call( elems ) :
			length ? fn( elems[0], key ) : emptyGet;
};
var rcheckableType = (/^(?:checkbox|radio)$/i);



(function() {
	var fragment = document.createDocumentFragment(),
		div = document.createElement("div"),
		input = document.createElement("input");

	// Setup
	div.setAttribute( "className", "t" );
	div.innerHTML = "  <link/><table></table><a href='/a'>a</a>";

	// IE strips leading whitespace when .innerHTML is used
	support.leadingWhitespace = div.firstChild.nodeType === 3;

	// Make sure that tbody elements aren't automatically inserted
	// IE will insert them into empty tables
	support.tbody = !div.getElementsByTagName( "tbody" ).length;

	// Make sure that link elements get serialized correctly by innerHTML
	// This requires a wrapper element in IE
	support.htmlSerialize = !!div.getElementsByTagName( "link" ).length;

	// Makes sure cloning an html5 element does not cause problems
	// Where outerHTML is undefined, this still works
	support.html5Clone =
		document.createElement( "nav" ).cloneNode( true ).outerHTML !== "<:nav></:nav>";

	// Check if a disconnected checkbox will retain its checked
	// value of true after appended to the DOM (IE6/7)
	input.type = "checkbox";
	input.checked = true;
	fragment.appendChild( input );
	support.appendChecked = input.checked;

	// Make sure textarea (and checkbox) defaultValue is properly cloned
	// Support: IE6-IE11+
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;

	// #11217 - WebKit loses check when the name is after the checked attribute
	fragment.appendChild( div );
	div.innerHTML = "<input type='radio' checked='checked' name='t'/>";

	// Support: Safari 5.1, iOS 5.1, Android 4.x, Android 2.3
	// old WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE<9
	// Opera does not clone events (and typeof div.attachEvent === undefined).
	// IE9-10 clones events bound via attachEvent, but they don't trigger with .click()
	support.noCloneEvent = true;
	if ( div.attachEvent ) {
		div.attachEvent( "onclick", function() {
			support.noCloneEvent = false;
		});

		div.cloneNode( true ).click();
	}

	// Execute the test only if not already executed in another module.
	if (support.deleteExpando == null) {
		// Support: IE<9
		support.deleteExpando = true;
		try {
			delete div.test;
		} catch( e ) {
			support.deleteExpando = false;
		}
	}

	// Null elements to avoid leaks in IE.
	fragment = div = input = null;
})();


(function() {
	var i, eventName,
		div = document.createElement( "div" );

	// Support: IE<9 (lack submit/change bubble), Firefox 23+ (lack focusin event)
	for ( i in { submit: true, change: true, focusin: true }) {
		eventName = "on" + i;

		if ( !(support[ i + "Bubbles" ] = eventName in window) ) {
			// Beware of CSP restrictions (https://developer.mozilla.org/en/Security/CSP)
			div.setAttribute( eventName, "t" );
			support[ i + "Bubbles" ] = div.attributes[ eventName ].expando === false;
		}
	}

	// Null elements to avoid leaks in IE.
	div = null;
})();


var rformElems = /^(?:input|select|textarea)$/i,
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|contextmenu)|click/,
	rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {
		var tmp, events, t, handleObjIn,
			special, eventHandle, handleObj,
			handlers, type, namespaces, origType,
			elemData = jQuery._data( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !(events = elemData.events) ) {
			events = elemData.events = {};
		}
		if ( !(eventHandle = elemData.handle) ) {
			eventHandle = elemData.handle = function( e ) {
				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== strundefined && (!e || jQuery.event.triggered !== e.type) ?
					jQuery.event.dispatch.apply( eventHandle.elem, arguments ) :
					undefined;
			};
			// Add elem as a property of the handle fn to prevent a memory leak with IE non-native events
			eventHandle.elem = elem;
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend({
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join(".")
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !(handlers = events[ type ]) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener/attachEvent if the special events handler returns false
				if ( !special.setup || special.setup.call( elem, data, namespaces, eventHandle ) === false ) {
					// Bind the global event handler to the element
					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle, false );

					} else if ( elem.attachEvent ) {
						elem.attachEvent( "on" + type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

		// Nullify elem to prevent memory leaks in IE
		elem = null;
	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {
		var j, handleObj, tmp,
			origCount, t, events,
			special, handlers, type,
			namespaces, origType,
			elemData = jQuery.hasData( elem ) && jQuery._data( elem );

		if ( !elemData || !(events = elemData.events) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[2] && new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector || selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown || special.teardown.call( elem, namespaces, elemData.handle ) === false ) {
					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			delete elemData.handle;

			// removeData also checks for emptiness and clears the expando if empty
			// so use it instead of delete
			jQuery._removeData( elem, "events" );
		}
	},

	trigger: function( event, data, elem, onlyHandlers ) {
		var handle, ontype, cur,
			bubbleType, special, tmp, i,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split(".") : [];

		cur = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf(".") >= 0 ) {
			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split(".");
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf(":") < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join(".");
		event.namespace_re = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === (elem.ownerDocument || document) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( (cur = eventPath[i++]) && !event.isPropagationStopped() ) {

			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( jQuery._data( cur, "events" ) || {} )[ event.type ] && jQuery._data( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && jQuery.acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( (!special._default || special._default.apply( eventPath.pop(), data ) === false) &&
				jQuery.acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name name as the event.
				// Can't use an .isFunction() check here because IE6/7 fails that test.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && elem[ type ] && !jQuery.isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;
					try {
						elem[ type ]();
					} catch ( e ) {
						// IE<9 dies on focus/blur to hidden element (#1486,#12518)
						// only reproducible on winXP IE8 native, not IE9 in IE8 mode
					}
					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	dispatch: function( event ) {

		// Make a writable jQuery.Event from the native event object
		event = jQuery.event.fix( event );

		var i, ret, handleObj, matched, j,
			handlerQueue = [],
			args = slice.call( arguments ),
			handlers = ( jQuery._data( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[0] = event;
		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( (matched = handlerQueue[ i++ ]) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( (handleObj = matched.handlers[ j++ ]) && !event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or
				// 2) have namespace(s) a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.namespace_re || event.namespace_re.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( (jQuery.event.special[ handleObj.origType ] || {}).handle || handleObj.handler )
							.apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( (event.result = ret) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var sel, handleObj, matches, i,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		// Black-hole SVG <use> instance trees (#13180)
		// Avoid non-left-click bubbling in Firefox (#3861)
		if ( delegateCount && cur.nodeType && (!event.button || event.type !== "click") ) {

			/* jshint eqeqeq: false */
			for ( ; cur != this; cur = cur.parentNode || this ) {
				/* jshint eqeqeq: true */

				// Don't check non-elements (#13208)
				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.nodeType === 1 && (cur.disabled !== true || event.type !== "click") ) {
					matches = [];
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matches[ sel ] === undefined ) {
							matches[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) >= 0 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matches[ sel ] ) {
							matches.push( handleObj );
						}
					}
					if ( matches.length ) {
						handlerQueue.push({ elem: cur, handlers: matches });
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		if ( delegateCount < handlers.length ) {
			handlerQueue.push({ elem: this, handlers: handlers.slice( delegateCount ) });
		}

		return handlerQueue;
	},

	fix: function( event ) {
		if ( event[ jQuery.expando ] ) {
			return event;
		}

		// Create a writable copy of the event object and normalize some properties
		var i, prop, copy,
			type = event.type,
			originalEvent = event,
			fixHook = this.fixHooks[ type ];

		if ( !fixHook ) {
			this.fixHooks[ type ] = fixHook =
				rmouseEvent.test( type ) ? this.mouseHooks :
				rkeyEvent.test( type ) ? this.keyHooks :
				{};
		}
		copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

		event = new jQuery.Event( originalEvent );

		i = copy.length;
		while ( i-- ) {
			prop = copy[ i ];
			event[ prop ] = originalEvent[ prop ];
		}

		// Support: IE<9
		// Fix target property (#1925)
		if ( !event.target ) {
			event.target = originalEvent.srcElement || document;
		}

		// Support: Chrome 23+, Safari?
		// Target should not be a text node (#504, #13143)
		if ( event.target.nodeType === 3 ) {
			event.target = event.target.parentNode;
		}

		// Support: IE<9
		// For mouse/key events, metaKey==false if it's undefined (#3368, #11328)
		event.metaKey = !!event.metaKey;

		return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
	},

	// Includes some event props shared by KeyEvent and MouseEvent
	props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),

	fixHooks: {},

	keyHooks: {
		props: "char charCode key keyCode".split(" "),
		filter: function( event, original ) {

			// Add which for key events
			if ( event.which == null ) {
				event.which = original.charCode != null ? original.charCode : original.keyCode;
			}

			return event;
		}
	},

	mouseHooks: {
		props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
		filter: function( event, original ) {
			var body, eventDoc, doc,
				button = original.button,
				fromElement = original.fromElement;

			// Calculate pageX/Y if missing and clientX/Y available
			if ( event.pageX == null && original.clientX != null ) {
				eventDoc = event.target.ownerDocument || document;
				doc = eventDoc.documentElement;
				body = eventDoc.body;

				event.pageX = original.clientX + ( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) - ( doc && doc.clientLeft || body && body.clientLeft || 0 );
				event.pageY = original.clientY + ( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) - ( doc && doc.clientTop  || body && body.clientTop  || 0 );
			}

			// Add relatedTarget, if necessary
			if ( !event.relatedTarget && fromElement ) {
				event.relatedTarget = fromElement === event.target ? original.toElement : fromElement;
			}

			// Add which for click: 1 === left; 2 === middle; 3 === right
			// Note: button is not normalized, so don't use it
			if ( !event.which && button !== undefined ) {
				event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
			}

			return event;
		}
	},

	special: {
		load: {
			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {
			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					try {
						this.focus();
						return false;
					} catch ( e ) {
						// Support: IE<9
						// If we error on focus to hidden element (#1486, #12518),
						// let .trigger() run the handlers
					}
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {
			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( jQuery.nodeName( this, "input" ) && this.type === "checkbox" && this.click ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return jQuery.nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Even when returnValue equals to undefined Firefox will still show alert
				if ( event.result !== undefined ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	},

	simulate: function( type, elem, event, bubble ) {
		// Piggyback on a donor event to simulate a different one.
		// Fake originalEvent to avoid donor's stopPropagation, but if the
		// simulated event prevents default then we do the same on the donor.
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true,
				originalEvent: {}
			}
		);
		if ( bubble ) {
			jQuery.event.trigger( e, null, elem );
		} else {
			jQuery.event.dispatch.call( elem, e );
		}
		if ( e.isDefaultPrevented() ) {
			event.preventDefault();
		}
	}
};

jQuery.removeEvent = document.removeEventListener ?
	function( elem, type, handle ) {
		if ( elem.removeEventListener ) {
			elem.removeEventListener( type, handle, false );
		}
	} :
	function( elem, type, handle ) {
		var name = "on" + type;

		if ( elem.detachEvent ) {

			// #8545, #7054, preventing memory leaks for custom events in IE6-8
			// detachEvent needed property on element, by name of that event, to properly expose it to GC
			if ( typeof elem[ name ] === strundefined ) {
				elem[ name ] = null;
			}

			elem.detachEvent( name, handle );
		}
	};

jQuery.Event = function( src, props ) {
	// Allow instantiation without the 'new' keyword
	if ( !(this instanceof jQuery.Event) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined && (
				// Support: IE < 9
				src.returnValue === false ||
				// Support: Android < 4.0
				src.getPreventDefault && src.getPreventDefault() ) ?
			returnTrue :
			returnFalse;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || jQuery.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;
		if ( !e ) {
			return;
		}

		// If preventDefault exists, run it on the original event
		if ( e.preventDefault ) {
			e.preventDefault();

		// Support: IE
		// Otherwise set the returnValue property of the original event to false
		} else {
			e.returnValue = false;
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;
		if ( !e ) {
			return;
		}
		// If stopPropagation exists, run it on the original event
		if ( e.stopPropagation ) {
			e.stopPropagation();
		}

		// Support: IE
		// Set the cancelBubble property of the original event to true
		e.cancelBubble = true;
	},
	stopImmediatePropagation: function() {
		this.isImmediatePropagationStopped = returnTrue;
		this.stopPropagation();
	}
};

// Create mouseenter/leave events using mouseover/out and event-time checks
jQuery.each({
	mouseenter: "mouseover",
	mouseleave: "mouseout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mousenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || (related !== target && !jQuery.contains( target, related )) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
});

// IE submit delegation
if ( !support.submitBubbles ) {

	jQuery.event.special.submit = {
		setup: function() {
			// Only need this for delegated form submit events
			if ( jQuery.nodeName( this, "form" ) ) {
				return false;
			}

			// Lazy-add a submit handler when a descendant form may potentially be submitted
			jQuery.event.add( this, "click._submit keypress._submit", function( e ) {
				// Node name check avoids a VML-related crash in IE (#9807)
				var elem = e.target,
					form = jQuery.nodeName( elem, "input" ) || jQuery.nodeName( elem, "button" ) ? elem.form : undefined;
				if ( form && !jQuery._data( form, "submitBubbles" ) ) {
					jQuery.event.add( form, "submit._submit", function( event ) {
						event._submit_bubble = true;
					});
					jQuery._data( form, "submitBubbles", true );
				}
			});
			// return undefined since we don't need an event listener
		},

		postDispatch: function( event ) {
			// If form was submitted by the user, bubble the event up the tree
			if ( event._submit_bubble ) {
				delete event._submit_bubble;
				if ( this.parentNode && !event.isTrigger ) {
					jQuery.event.simulate( "submit", this.parentNode, event, true );
				}
			}
		},

		teardown: function() {
			// Only need this for delegated form submit events
			if ( jQuery.nodeName( this, "form" ) ) {
				return false;
			}

			// Remove delegated handlers; cleanData eventually reaps submit handlers attached above
			jQuery.event.remove( this, "._submit" );
		}
	};
}

// IE change delegation and checkbox/radio fix
if ( !support.changeBubbles ) {

	jQuery.event.special.change = {

		setup: function() {

			if ( rformElems.test( this.nodeName ) ) {
				// IE doesn't fire change on a check/radio until blur; trigger it on click
				// after a propertychange. Eat the blur-change in special.change.handle.
				// This still fires onchange a second time for check/radio after blur.
				if ( this.type === "checkbox" || this.type === "radio" ) {
					jQuery.event.add( this, "propertychange._change", function( event ) {
						if ( event.originalEvent.propertyName === "checked" ) {
							this._just_changed = true;
						}
					});
					jQuery.event.add( this, "click._change", function( event ) {
						if ( this._just_changed && !event.isTrigger ) {
							this._just_changed = false;
						}
						// Allow triggered, simulated change events (#11500)
						jQuery.event.simulate( "change", this, event, true );
					});
				}
				return false;
			}
			// Delegated event; lazy-add a change handler on descendant inputs
			jQuery.event.add( this, "beforeactivate._change", function( e ) {
				var elem = e.target;

				if ( rformElems.test( elem.nodeName ) && !jQuery._data( elem, "changeBubbles" ) ) {
					jQuery.event.add( elem, "change._change", function( event ) {
						if ( this.parentNode && !event.isSimulated && !event.isTrigger ) {
							jQuery.event.simulate( "change", this.parentNode, event, true );
						}
					});
					jQuery._data( elem, "changeBubbles", true );
				}
			});
		},

		handle: function( event ) {
			var elem = event.target;

			// Swallow native change events from checkbox/radio, we already triggered them above
			if ( this !== elem || event.isSimulated || event.isTrigger || (elem.type !== "radio" && elem.type !== "checkbox") ) {
				return event.handleObj.handler.apply( this, arguments );
			}
		},

		teardown: function() {
			jQuery.event.remove( this, "._change" );

			return !rformElems.test( this.nodeName );
		}
	};
}

// Create "bubbling" focus and blur events
if ( !support.focusinBubbles ) {
	jQuery.each({ focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
				jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ), true );
			};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = jQuery._data( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				jQuery._data( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = jQuery._data( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					jQuery._removeData( doc, fix );
				} else {
					jQuery._data( doc, fix, attaches );
				}
			}
		};
	});
}

jQuery.fn.extend({

	on: function( types, selector, data, fn, /*INTERNAL*/ one ) {
		var type, origFn;

		// Types can be a map of types/handlers
		if ( typeof types === "object" ) {
			// ( types-Object, selector, data )
			if ( typeof selector !== "string" ) {
				// ( types-Object, data )
				data = data || selector;
				selector = undefined;
			}
			for ( type in types ) {
				this.on( type, selector, data, types[ type ], one );
			}
			return this;
		}

		if ( data == null && fn == null ) {
			// ( types, fn )
			fn = selector;
			data = selector = undefined;
		} else if ( fn == null ) {
			if ( typeof selector === "string" ) {
				// ( types, selector, fn )
				fn = data;
				data = undefined;
			} else {
				// ( types, data, fn )
				fn = data;
				data = selector;
				selector = undefined;
			}
		}
		if ( fn === false ) {
			fn = returnFalse;
		} else if ( !fn ) {
			return this;
		}

		if ( one === 1 ) {
			origFn = fn;
			fn = function( event ) {
				// Can use an empty set, since event contains the info
				jQuery().off( event );
				return origFn.apply( this, arguments );
			};
			// Use same guid so caller can remove using origFn
			fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
		}
		return this.each( function() {
			jQuery.event.add( this, types, fn, data, selector );
		});
	},
	one: function( types, selector, data, fn ) {
		return this.on( types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {
			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {
			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {
			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each(function() {
			jQuery.event.remove( this, types, fn, selector );
		});
	},

	trigger: function( type, data ) {
		return this.each(function() {
			jQuery.event.trigger( type, data, this );
		});
	},
	triggerHandler: function( type, data ) {
		var elem = this[0];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
});


function createSafeFragment( document ) {
	var list = nodeNames.split( "|" ),
		safeFrag = document.createDocumentFragment();

	if ( safeFrag.createElement ) {
		while ( list.length ) {
			safeFrag.createElement(
				list.pop()
			);
		}
	}
	return safeFrag;
}

var nodeNames = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|" +
		"header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
	rinlinejQuery = / jQuery\d+="(?:null|\d+)"/g,
	rnoshimcache = new RegExp("<(?:" + nodeNames + ")[\\s/>]", "i"),
	rleadingWhitespace = /^\s+/,
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
	rtagName = /<([\w:]+)/,
	rtbody = /<tbody/i,
	rhtml = /<|&#?\w+;/,
	rnoInnerhtml = /<(?:script|style|link)/i,
	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rscriptType = /^$|\/(?:java|ecma)script/i,
	rscriptTypeMasked = /^true\/(.*)/,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,

	// We have to close these tags to support XHTML (#13200)
	wrapMap = {
		option: [ 1, "<select multiple='multiple'>", "</select>" ],
		legend: [ 1, "<fieldset>", "</fieldset>" ],
		area: [ 1, "<map>", "</map>" ],
		param: [ 1, "<object>", "</object>" ],
		thead: [ 1, "<table>", "</table>" ],
		tr: [ 2, "<table><tbody>", "</tbody></table>" ],
		col: [ 2, "<table><tbody></tbody><colgroup>", "</colgroup></table>" ],
		td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

		// IE6-8 can't serialize link, script, style, or any html5 (NoScope) tags,
		// unless wrapped in a div with non-breaking characters in front of it.
		_default: support.htmlSerialize ? [ 0, "", "" ] : [ 1, "X<div>", "</div>"  ]
	},
	safeFragment = createSafeFragment( document ),
	fragmentDiv = safeFragment.appendChild( document.createElement("div") );

wrapMap.optgroup = wrapMap.option;
wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;

function getAll( context, tag ) {
	var elems, elem,
		i = 0,
		found = typeof context.getElementsByTagName !== strundefined ? context.getElementsByTagName( tag || "*" ) :
			typeof context.querySelectorAll !== strundefined ? context.querySelectorAll( tag || "*" ) :
			undefined;

	if ( !found ) {
		for ( found = [], elems = context.childNodes || context; (elem = elems[i]) != null; i++ ) {
			if ( !tag || jQuery.nodeName( elem, tag ) ) {
				found.push( elem );
			} else {
				jQuery.merge( found, getAll( elem, tag ) );
			}
		}
	}

	return tag === undefined || tag && jQuery.nodeName( context, tag ) ?
		jQuery.merge( [ context ], found ) :
		found;
}

// Used in buildFragment, fixes the defaultChecked property
function fixDefaultChecked( elem ) {
	if ( rcheckableType.test( elem.type ) ) {
		elem.defaultChecked = elem.checked;
	}
}

// Support: IE<8
// Manipulating tables requires a tbody
function manipulationTarget( elem, content ) {
	return jQuery.nodeName( elem, "table" ) &&
		jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ?

		elem.getElementsByTagName("tbody")[0] ||
			elem.appendChild( elem.ownerDocument.createElement("tbody") ) :
		elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = (jQuery.find.attr( elem, "type" ) !== null) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	var match = rscriptTypeMasked.exec( elem.type );
	if ( match ) {
		elem.type = match[1];
	} else {
		elem.removeAttribute("type");
	}
	return elem;
}

// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var elem,
		i = 0;
	for ( ; (elem = elems[i]) != null; i++ ) {
		jQuery._data( elem, "globalEval", !refElements || jQuery._data( refElements[i], "globalEval" ) );
	}
}

function cloneCopyEvent( src, dest ) {

	if ( dest.nodeType !== 1 || !jQuery.hasData( src ) ) {
		return;
	}

	var type, i, l,
		oldData = jQuery._data( src ),
		curData = jQuery._data( dest, oldData ),
		events = oldData.events;

	if ( events ) {
		delete curData.handle;
		curData.events = {};

		for ( type in events ) {
			for ( i = 0, l = events[ type ].length; i < l; i++ ) {
				jQuery.event.add( dest, type, events[ type ][ i ] );
			}
		}
	}

	// make the cloned public data object a copy from the original
	if ( curData.data ) {
		curData.data = jQuery.extend( {}, curData.data );
	}
}

function fixCloneNodeIssues( src, dest ) {
	var nodeName, e, data;

	// We do not need to do anything for non-Elements
	if ( dest.nodeType !== 1 ) {
		return;
	}

	nodeName = dest.nodeName.toLowerCase();

	// IE6-8 copies events bound via attachEvent when using cloneNode.
	if ( !support.noCloneEvent && dest[ jQuery.expando ] ) {
		data = jQuery._data( dest );

		for ( e in data.events ) {
			jQuery.removeEvent( dest, e, data.handle );
		}

		// Event data gets referenced instead of copied if the expando gets copied too
		dest.removeAttribute( jQuery.expando );
	}

	// IE blanks contents when cloning scripts, and tries to evaluate newly-set text
	if ( nodeName === "script" && dest.text !== src.text ) {
		disableScript( dest ).text = src.text;
		restoreScript( dest );

	// IE6-10 improperly clones children of object elements using classid.
	// IE10 throws NoModificationAllowedError if parent is null, #12132.
	} else if ( nodeName === "object" ) {
		if ( dest.parentNode ) {
			dest.outerHTML = src.outerHTML;
		}

		// This path appears unavoidable for IE9. When cloning an object
		// element in IE9, the outerHTML strategy above is not sufficient.
		// If the src has innerHTML and the destination does not,
		// copy the src.innerHTML into the dest.innerHTML. #10324
		if ( support.html5Clone && ( src.innerHTML && !jQuery.trim(dest.innerHTML) ) ) {
			dest.innerHTML = src.innerHTML;
		}

	} else if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		// IE6-8 fails to persist the checked state of a cloned checkbox
		// or radio button. Worse, IE6-7 fail to give the cloned element
		// a checked appearance if the defaultChecked value isn't also set

		dest.defaultChecked = dest.checked = src.checked;

		// IE6-7 get confused and end up setting the value of a cloned
		// checkbox/radio button to an empty string instead of "on"
		if ( dest.value !== src.value ) {
			dest.value = src.value;
		}

	// IE6-8 fails to return the selected option to the default selected
	// state when cloning options
	} else if ( nodeName === "option" ) {
		dest.defaultSelected = dest.selected = src.defaultSelected;

	// IE6-8 fails to set the defaultValue to the correct value when
	// cloning other types of input fields
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

jQuery.extend({
	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var destElements, node, clone, i, srcElements,
			inPage = jQuery.contains( elem.ownerDocument, elem );

		if ( support.html5Clone || jQuery.isXMLDoc(elem) || !rnoshimcache.test( "<" + elem.nodeName + ">" ) ) {
			clone = elem.cloneNode( true );

		// IE<=8 does not properly clone detached, unknown element nodes
		} else {
			fragmentDiv.innerHTML = elem.outerHTML;
			fragmentDiv.removeChild( clone = fragmentDiv.firstChild );
		}

		if ( (!support.noCloneEvent || !support.noCloneChecked) &&
				(elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem) ) {

			// We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			// Fix all IE cloning issues
			for ( i = 0; (node = srcElements[i]) != null; ++i ) {
				// Ensure that the destination node is not null; Fixes #9587
				if ( destElements[i] ) {
					fixCloneNodeIssues( node, destElements[i] );
				}
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0; (node = srcElements[i]) != null; i++ ) {
					cloneCopyEvent( node, destElements[i] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		destElements = srcElements = node = null;

		// Return the cloned set
		return clone;
	},

	buildFragment: function( elems, context, scripts, selection ) {
		var j, elem, contains,
			tmp, tag, tbody, wrap,
			l = elems.length,

			// Ensure a safe fragment
			safe = createSafeFragment( context ),

			nodes = [],
			i = 0;

		for ( ; i < l; i++ ) {
			elem = elems[ i ];

			if ( elem || elem === 0 ) {

				// Add nodes directly
				if ( jQuery.type( elem ) === "object" ) {
					jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

				// Convert non-html into a text node
				} else if ( !rhtml.test( elem ) ) {
					nodes.push( context.createTextNode( elem ) );

				// Convert html into DOM nodes
				} else {
					tmp = tmp || safe.appendChild( context.createElement("div") );

					// Deserialize a standard representation
					tag = (rtagName.exec( elem ) || [ "", "" ])[ 1 ].toLowerCase();
					wrap = wrapMap[ tag ] || wrapMap._default;

					tmp.innerHTML = wrap[1] + elem.replace( rxhtmlTag, "<$1></$2>" ) + wrap[2];

					// Descend through wrappers to the right content
					j = wrap[0];
					while ( j-- ) {
						tmp = tmp.lastChild;
					}

					// Manually add leading whitespace removed by IE
					if ( !support.leadingWhitespace && rleadingWhitespace.test( elem ) ) {
						nodes.push( context.createTextNode( rleadingWhitespace.exec( elem )[0] ) );
					}

					// Remove IE's autoinserted <tbody> from table fragments
					if ( !support.tbody ) {

						// String was a <table>, *may* have spurious <tbody>
						elem = tag === "table" && !rtbody.test( elem ) ?
							tmp.firstChild :

							// String was a bare <thead> or <tfoot>
							wrap[1] === "<table>" && !rtbody.test( elem ) ?
								tmp :
								0;

						j = elem && elem.childNodes.length;
						while ( j-- ) {
							if ( jQuery.nodeName( (tbody = elem.childNodes[j]), "tbody" ) && !tbody.childNodes.length ) {
								elem.removeChild( tbody );
							}
						}
					}

					jQuery.merge( nodes, tmp.childNodes );

					// Fix #12392 for WebKit and IE > 9
					tmp.textContent = "";

					// Fix #12392 for oldIE
					while ( tmp.firstChild ) {
						tmp.removeChild( tmp.firstChild );
					}

					// Remember the top-level container for proper cleanup
					tmp = safe.lastChild;
				}
			}
		}

		// Fix #11356: Clear elements from fragment
		if ( tmp ) {
			safe.removeChild( tmp );
		}

		// Reset defaultChecked for any radios and checkboxes
		// about to be appended to the DOM in IE 6/7 (#8060)
		if ( !support.appendChecked ) {
			jQuery.grep( getAll( nodes, "input" ), fixDefaultChecked );
		}

		i = 0;
		while ( (elem = nodes[ i++ ]) ) {

			// #4087 - If origin and destination elements are the same, and this is
			// that element, do not do anything
			if ( selection && jQuery.inArray( elem, selection ) !== -1 ) {
				continue;
			}

			contains = jQuery.contains( elem.ownerDocument, elem );

			// Append to fragment
			tmp = getAll( safe.appendChild( elem ), "script" );

			// Preserve script evaluation history
			if ( contains ) {
				setGlobalEval( tmp );
			}

			// Capture executables
			if ( scripts ) {
				j = 0;
				while ( (elem = tmp[ j++ ]) ) {
					if ( rscriptType.test( elem.type || "" ) ) {
						scripts.push( elem );
					}
				}
			}
		}

		tmp = null;

		return safe;
	},

	cleanData: function( elems, /* internal */ acceptData ) {
		var elem, type, id, data,
			i = 0,
			internalKey = jQuery.expando,
			cache = jQuery.cache,
			deleteExpando = support.deleteExpando,
			special = jQuery.event.special;

		for ( ; (elem = elems[i]) != null; i++ ) {
			if ( acceptData || jQuery.acceptData( elem ) ) {

				id = elem[ internalKey ];
				data = id && cache[ id ];

				if ( data ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}

					// Remove cache only if it was not already removed by jQuery.event.remove
					if ( cache[ id ] ) {

						delete cache[ id ];

						// IE does not allow us to delete expando properties from nodes,
						// nor does it have a removeAttribute function on Document nodes;
						// we must handle all of these cases
						if ( deleteExpando ) {
							delete elem[ internalKey ];

						} else if ( typeof elem.removeAttribute !== strundefined ) {
							elem.removeAttribute( internalKey );

						} else {
							elem[ internalKey ] = null;
						}

						deletedIds.push( id );
					}
				}
			}
		}
	}
});

jQuery.fn.extend({
	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().append( ( this[0] && this[0].ownerDocument || document ).createTextNode( value ) );
		}, null, value, arguments.length );
	},

	append: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		});
	},

	prepend: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		});
	},

	before: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		});
	},

	after: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		});
	},

	remove: function( selector, keepData /* Internal Use Only */ ) {
		var elem,
			elems = selector ? jQuery.filter( selector, this ) : this,
			i = 0;

		for ( ; (elem = elems[i]) != null; i++ ) {

			if ( !keepData && elem.nodeType === 1 ) {
				jQuery.cleanData( getAll( elem ) );
			}

			if ( elem.parentNode ) {
				if ( keepData && jQuery.contains( elem.ownerDocument, elem ) ) {
					setGlobalEval( getAll( elem, "script" ) );
				}
				elem.parentNode.removeChild( elem );
			}
		}

		return this;
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; (elem = this[i]) != null; i++ ) {
			// Remove element nodes and prevent memory leaks
			if ( elem.nodeType === 1 ) {
				jQuery.cleanData( getAll( elem, false ) );
			}

			// Remove any remaining nodes
			while ( elem.firstChild ) {
				elem.removeChild( elem.firstChild );
			}

			// If this is a select, ensure that it displays empty (#12336)
			// Support: IE<9
			if ( elem.options && jQuery.nodeName( elem, "select" ) ) {
				elem.options.length = 0;
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map(function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		});
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined ) {
				return elem.nodeType === 1 ?
					elem.innerHTML.replace( rinlinejQuery, "" ) :
					undefined;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				( support.htmlSerialize || !rnoshimcache.test( value )  ) &&
				( support.leadingWhitespace || !rleadingWhitespace.test( value ) ) &&
				!wrapMap[ (rtagName.exec( value ) || [ "", "" ])[ 1 ].toLowerCase() ] ) {

				value = value.replace( rxhtmlTag, "<$1></$2>" );

				try {
					for (; i < l; i++ ) {
						// Remove element nodes and prevent memory leaks
						elem = this[i] || {};
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch(e) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var arg = arguments[ 0 ];

		// Make the changes, replacing each context element with the new content
		this.domManip( arguments, function( elem ) {
			arg = this.parentNode;

			jQuery.cleanData( getAll( this ) );

			if ( arg ) {
				arg.replaceChild( elem, this );
			}
		});

		// Force removal if there was no new content (e.g., from empty arguments)
		return arg && (arg.length || arg.nodeType) ? this : this.remove();
	},

	detach: function( selector ) {
		return this.remove( selector, true );
	},

	domManip: function( args, callback ) {

		// Flatten any nested arrays
		args = concat.apply( [], args );

		var first, node, hasScripts,
			scripts, doc, fragment,
			i = 0,
			l = this.length,
			set = this,
			iNoClone = l - 1,
			value = args[0],
			isFunction = jQuery.isFunction( value );

		// We can't cloneNode fragments that contain checked, in WebKit
		if ( isFunction ||
				( l > 1 && typeof value === "string" &&
					!support.checkClone && rchecked.test( value ) ) ) {
			return this.each(function( index ) {
				var self = set.eq( index );
				if ( isFunction ) {
					args[0] = value.call( this, index, self.html() );
				}
				self.domManip( args, callback );
			});
		}

		if ( l ) {
			fragment = jQuery.buildFragment( args, this[ 0 ].ownerDocument, false, this );
			first = fragment.firstChild;

			if ( fragment.childNodes.length === 1 ) {
				fragment = first;
			}

			if ( first ) {
				scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
				hasScripts = scripts.length;

				// Use the original fragment for the last item instead of the first because it can end up
				// being emptied incorrectly in certain situations (#8070).
				for ( ; i < l; i++ ) {
					node = fragment;

					if ( i !== iNoClone ) {
						node = jQuery.clone( node, true, true );

						// Keep references to cloned scripts for later restoration
						if ( hasScripts ) {
							jQuery.merge( scripts, getAll( node, "script" ) );
						}
					}

					callback.call( this[i], node, i );
				}

				if ( hasScripts ) {
					doc = scripts[ scripts.length - 1 ].ownerDocument;

					// Reenable scripts
					jQuery.map( scripts, restoreScript );

					// Evaluate executable scripts on first document insertion
					for ( i = 0; i < hasScripts; i++ ) {
						node = scripts[ i ];
						if ( rscriptType.test( node.type || "" ) &&
							!jQuery._data( node, "globalEval" ) && jQuery.contains( doc, node ) ) {

							if ( node.src ) {
								// Optional AJAX dependency, but won't run scripts if not present
								if ( jQuery._evalUrl ) {
									jQuery._evalUrl( node.src );
								}
							} else {
								jQuery.globalEval( ( node.text || node.textContent || node.innerHTML || "" ).replace( rcleanScript, "" ) );
							}
						}
					}
				}

				// Fix #11809: Avoid leaking memory
				fragment = first = null;
			}
		}

		return this;
	}
});

jQuery.each({
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			i = 0,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone(true);
			jQuery( insert[i] )[ original ]( elems );

			// Modern browsers can apply jQuery collections as arrays, but oldIE needs a .get()
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
});


var iframe,
	elemdisplay = {};

/**
 * Retrieve the actual display of a element
 * @param {String} name nodeName of the element
 * @param {Object} doc Document object
 */
// Called only from within defaultDisplay
function actualDisplay( name, doc ) {
	var elem = jQuery( doc.createElement( name ) ).appendTo( doc.body ),

		// getDefaultComputedStyle might be reliably used only on attached element
		display = window.getDefaultComputedStyle ?

			// Use of this method is a temporary fix (more like optmization) until something better comes along,
			// since it was removed from specification and supported only in FF
			window.getDefaultComputedStyle( elem[ 0 ] ).display : jQuery.css( elem[ 0 ], "display" );

	// We don't have any data stored on the element,
	// so use "detach" method as fast way to get rid of the element
	elem.detach();

	return display;
}

/**
 * Try to determine the default display value of an element
 * @param {String} nodeName
 */
function defaultDisplay( nodeName ) {
	var doc = document,
		display = elemdisplay[ nodeName ];

	if ( !display ) {
		display = actualDisplay( nodeName, doc );

		// If the simple way fails, read from inside an iframe
		if ( display === "none" || !display ) {

			// Use the already-created iframe if possible
			iframe = (iframe || jQuery( "<iframe frameborder='0' width='0' height='0'/>" )).appendTo( doc.documentElement );

			// Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
			doc = ( iframe[ 0 ].contentWindow || iframe[ 0 ].contentDocument ).document;

			// Support: IE
			doc.write();
			doc.close();

			display = actualDisplay( nodeName, doc );
			iframe.detach();
		}

		// Store the correct default display
		elemdisplay[ nodeName ] = display;
	}

	return display;
}


(function() {
	var a, shrinkWrapBlocksVal,
		div = document.createElement( "div" ),
		divReset =
			"-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;" +
			"display:block;padding:0;margin:0;border:0";

	// Setup
	div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
	a = div.getElementsByTagName( "a" )[ 0 ];

	a.style.cssText = "float:left;opacity:.5";

	// Make sure that element opacity exists
	// (IE uses filter instead)
	// Use a regex to work around a WebKit issue. See #5145
	support.opacity = /^0.5/.test( a.style.opacity );

	// Verify style float existence
	// (IE uses styleFloat instead of cssFloat)
	support.cssFloat = !!a.style.cssFloat;

	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	// Null elements to avoid leaks in IE.
	a = div = null;

	support.shrinkWrapBlocks = function() {
		var body, container, div, containerStyles;

		if ( shrinkWrapBlocksVal == null ) {
			body = document.getElementsByTagName( "body" )[ 0 ];
			if ( !body ) {
				// Test fired too early or in an unsupported environment, exit.
				return;
			}

			containerStyles = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px";
			container = document.createElement( "div" );
			div = document.createElement( "div" );

			body.appendChild( container ).appendChild( div );

			// Will be changed later if needed.
			shrinkWrapBlocksVal = false;

			if ( typeof div.style.zoom !== strundefined ) {
				// Support: IE6
				// Check if elements with layout shrink-wrap their children
				div.style.cssText = divReset + ";width:1px;padding:1px;zoom:1";
				div.innerHTML = "<div></div>";
				div.firstChild.style.width = "5px";
				shrinkWrapBlocksVal = div.offsetWidth !== 3;
			}

			body.removeChild( container );

			// Null elements to avoid leaks in IE.
			body = container = div = null;
		}

		return shrinkWrapBlocksVal;
	};

})();
var rmargin = (/^margin/);

var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );



var getStyles, curCSS,
	rposition = /^(top|right|bottom|left)$/;

if ( window.getComputedStyle ) {
	getStyles = function( elem ) {
		return elem.ownerDocument.defaultView.getComputedStyle( elem, null );
	};

	curCSS = function( elem, name, computed ) {
		var width, minWidth, maxWidth, ret,
			style = elem.style;

		computed = computed || getStyles( elem );

		// getPropertyValue is only needed for .css('filter') in IE9, see #12537
		ret = computed ? computed.getPropertyValue( name ) || computed[ name ] : undefined;

		if ( computed ) {

			if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
				ret = jQuery.style( elem, name );
			}

			// A tribute to the "awesome hack by Dean Edwards"
			// Chrome < 17 and Safari 5.0 uses "computed value" instead of "used value" for margin-right
			// Safari 5.1.7 (at least) returns percentage for a larger set of values, but width seems to be reliably pixels
			// this is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values
			if ( rnumnonpx.test( ret ) && rmargin.test( name ) ) {

				// Remember the original values
				width = style.width;
				minWidth = style.minWidth;
				maxWidth = style.maxWidth;

				// Put in the new values to get a computed value out
				style.minWidth = style.maxWidth = style.width = ret;
				ret = computed.width;

				// Revert the changed values
				style.width = width;
				style.minWidth = minWidth;
				style.maxWidth = maxWidth;
			}
		}

		// Support: IE
		// IE returns zIndex value as an integer.
		return ret === undefined ?
			ret :
			ret + "";
	};
} else if ( document.documentElement.currentStyle ) {
	getStyles = function( elem ) {
		return elem.currentStyle;
	};

	curCSS = function( elem, name, computed ) {
		var left, rs, rsLeft, ret,
			style = elem.style;

		computed = computed || getStyles( elem );
		ret = computed ? computed[ name ] : undefined;

		// Avoid setting ret to empty string here
		// so we don't default to auto
		if ( ret == null && style && style[ name ] ) {
			ret = style[ name ];
		}

		// From the awesome hack by Dean Edwards
		// http://erik.eae.net/archives/2007/07/27/18.54.15/#comment-102291

		// If we're not dealing with a regular pixel number
		// but a number that has a weird ending, we need to convert it to pixels
		// but not position css attributes, as those are proportional to the parent element instead
		// and we can't measure the parent instead because it might trigger a "stacking dolls" problem
		if ( rnumnonpx.test( ret ) && !rposition.test( name ) ) {

			// Remember the original values
			left = style.left;
			rs = elem.runtimeStyle;
			rsLeft = rs && rs.left;

			// Put in the new values to get a computed value out
			if ( rsLeft ) {
				rs.left = elem.currentStyle.left;
			}
			style.left = name === "fontSize" ? "1em" : ret;
			ret = style.pixelLeft + "px";

			// Revert the changed values
			style.left = left;
			if ( rsLeft ) {
				rs.left = rsLeft;
			}
		}

		// Support: IE
		// IE returns zIndex value as an integer.
		return ret === undefined ?
			ret :
			ret + "" || "auto";
	};
}




function addGetHookIf( conditionFn, hookFn ) {
	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			var condition = conditionFn();

			if ( condition == null ) {
				// The test was not ready at this point; screw the hook this time
				// but check again when needed next time.
				return;
			}

			if ( condition ) {
				// Hook not needed (or it's not possible to use it due to missing dependency),
				// remove it.
				// Since there are no other hooks for marginRight, remove the whole object.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.

			return (this.get = hookFn).apply( this, arguments );
		}
	};
}


(function() {
	var a, reliableHiddenOffsetsVal, boxSizingVal, boxSizingReliableVal,
		pixelPositionVal, reliableMarginRightVal,
		div = document.createElement( "div" ),
		containerStyles = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px",
		divReset =
			"-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;" +
			"display:block;padding:0;margin:0;border:0";

	// Setup
	div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
	a = div.getElementsByTagName( "a" )[ 0 ];

	a.style.cssText = "float:left;opacity:.5";

	// Make sure that element opacity exists
	// (IE uses filter instead)
	// Use a regex to work around a WebKit issue. See #5145
	support.opacity = /^0.5/.test( a.style.opacity );

	// Verify style float existence
	// (IE uses styleFloat instead of cssFloat)
	support.cssFloat = !!a.style.cssFloat;

	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	// Null elements to avoid leaks in IE.
	a = div = null;

	jQuery.extend(support, {
		reliableHiddenOffsets: function() {
			if ( reliableHiddenOffsetsVal != null ) {
				return reliableHiddenOffsetsVal;
			}

			var container, tds, isSupported,
				div = document.createElement( "div" ),
				body = document.getElementsByTagName( "body" )[ 0 ];

			if ( !body ) {
				// Return for frameset docs that don't have a body
				return;
			}

			// Setup
			div.setAttribute( "className", "t" );
			div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";

			container = document.createElement( "div" );
			container.style.cssText = containerStyles;

			body.appendChild( container ).appendChild( div );

			// Support: IE8
			// Check if table cells still have offsetWidth/Height when they are set
			// to display:none and there are still other visible table cells in a
			// table row; if so, offsetWidth/Height are not reliable for use when
			// determining if an element has been hidden directly using
			// display:none (it is still safe to use offsets if a parent element is
			// hidden; don safety goggles and see bug #4512 for more information).
			div.innerHTML = "<table><tr><td></td><td>t</td></tr></table>";
			tds = div.getElementsByTagName( "td" );
			tds[ 0 ].style.cssText = "padding:0;margin:0;border:0;display:none";
			isSupported = ( tds[ 0 ].offsetHeight === 0 );

			tds[ 0 ].style.display = "";
			tds[ 1 ].style.display = "none";

			// Support: IE8
			// Check if empty table cells still have offsetWidth/Height
			reliableHiddenOffsetsVal = isSupported && ( tds[ 0 ].offsetHeight === 0 );

			body.removeChild( container );

			// Null elements to avoid leaks in IE.
			div = body = null;

			return reliableHiddenOffsetsVal;
		},

		boxSizing: function() {
			if ( boxSizingVal == null ) {
				computeStyleTests();
			}
			return boxSizingVal;
		},

		boxSizingReliable: function() {
			if ( boxSizingReliableVal == null ) {
				computeStyleTests();
			}
			return boxSizingReliableVal;
		},

		pixelPosition: function() {
			if ( pixelPositionVal == null ) {
				computeStyleTests();
			}
			return pixelPositionVal;
		},

		reliableMarginRight: function() {
			var body, container, div, marginDiv;

			// Use window.getComputedStyle because jsdom on node.js will break without it.
			if ( reliableMarginRightVal == null && window.getComputedStyle ) {
				body = document.getElementsByTagName( "body" )[ 0 ];
				if ( !body ) {
					// Test fired too early or in an unsupported environment, exit.
					return;
				}

				container = document.createElement( "div" );
				div = document.createElement( "div" );
				container.style.cssText = containerStyles;

				body.appendChild( container ).appendChild( div );

				// Check if div with explicit width and no margin-right incorrectly
				// gets computed margin-right based on width of container. (#3333)
				// Fails in WebKit before Feb 2011 nightlies
				// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
				marginDiv = div.appendChild( document.createElement( "div" ) );
				marginDiv.style.cssText = div.style.cssText = divReset;
				marginDiv.style.marginRight = marginDiv.style.width = "0";
				div.style.width = "1px";

				reliableMarginRightVal =
					!parseFloat( ( window.getComputedStyle( marginDiv, null ) || {} ).marginRight );

				body.removeChild( container );
			}

			return reliableMarginRightVal;
		}
	});

	function computeStyleTests() {
		var container, div,
			body = document.getElementsByTagName( "body" )[ 0 ];

		if ( !body ) {
			// Test fired too early or in an unsupported environment, exit.
			return;
		}

		container = document.createElement( "div" );
		div = document.createElement( "div" );
		container.style.cssText = containerStyles;

		body.appendChild( container ).appendChild( div );

		div.style.cssText =
			"-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;" +
				"position:absolute;display:block;padding:1px;border:1px;width:4px;" +
				"margin-top:1%;top:1%";

		// Workaround failing boxSizing test due to offsetWidth returning wrong value
		// with some non-1 values of body zoom, ticket #13543
		jQuery.swap( body, body.style.zoom != null ? { zoom: 1 } : {}, function() {
			boxSizingVal = div.offsetWidth === 4;
		});

		// Will be changed later if needed.
		boxSizingReliableVal = true;
		pixelPositionVal = false;
		reliableMarginRightVal = true;

		// Use window.getComputedStyle because jsdom on node.js will break without it.
		if ( window.getComputedStyle ) {
			pixelPositionVal = ( window.getComputedStyle( div, null ) || {} ).top !== "1%";
			boxSizingReliableVal =
				( window.getComputedStyle( div, null ) || { width: "4px" } ).width === "4px";
		}

		body.removeChild( container );

		// Null elements to avoid leaks in IE.
		div = body = null;
	}

})();


// A method for quickly swapping in/out CSS properties to get correct calculations.
jQuery.swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};


var
		ralpha = /alpha\([^)]*\)/i,
	ropacity = /opacity\s*=\s*([^)]*)/,

	// swappable if display is none or starts with table except "table", "table-cell", or "table-caption"
	// see here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rnumsplit = new RegExp( "^(" + pnum + ")(.*)$", "i" ),
	rrelNum = new RegExp( "^([+-])=(" + pnum + ")", "i" ),

	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: 0,
		fontWeight: 400
	},

	cssPrefixes = [ "Webkit", "O", "Moz", "ms" ];


// return a css property mapped to a potentially vendor prefixed property
function vendorPropName( style, name ) {

	// shortcut for names that are not vendor prefixed
	if ( name in style ) {
		return name;
	}

	// check for vendor prefixed names
	var capName = name.charAt(0).toUpperCase() + name.slice(1),
		origName = name,
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in style ) {
			return name;
		}
	}

	return origName;
}

function showHide( elements, show ) {
	var display, elem, hidden,
		values = [],
		index = 0,
		length = elements.length;

	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		values[ index ] = jQuery._data( elem, "olddisplay" );
		display = elem.style.display;
		if ( show ) {
			// Reset the inline display of this element to learn if it is
			// being hidden by cascaded rules or not
			if ( !values[ index ] && display === "none" ) {
				elem.style.display = "";
			}

			// Set elements which have been overridden with display: none
			// in a stylesheet to whatever the default browser style is
			// for such an element
			if ( elem.style.display === "" && isHidden( elem ) ) {
				values[ index ] = jQuery._data( elem, "olddisplay", defaultDisplay(elem.nodeName) );
			}
		} else {

			if ( !values[ index ] ) {
				hidden = isHidden( elem );

				if ( display && display !== "none" || !hidden ) {
					jQuery._data( elem, "olddisplay", hidden ? display : jQuery.css( elem, "display" ) );
				}
			}
		}
	}

	// Set the display of most of the elements in a second loop
	// to avoid the constant reflow
	for ( index = 0; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}
		if ( !show || elem.style.display === "none" || elem.style.display === "" ) {
			elem.style.display = show ? values[ index ] || "" : "none";
		}
	}

	return elements;
}

function setPositiveNumber( elem, value, subtract ) {
	var matches = rnumsplit.exec( value );
	return matches ?
		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 1 ] - ( subtract || 0 ) ) + ( matches[ 2 ] || "px" ) :
		value;
}

function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
	var i = extra === ( isBorderBox ? "border" : "content" ) ?
		// If we already have the right measurement, avoid augmentation
		4 :
		// Otherwise initialize for horizontal or vertical properties
		name === "width" ? 1 : 0,

		val = 0;

	for ( ; i < 4; i += 2 ) {
		// both box models exclude margin, so add it if we want it
		if ( extra === "margin" ) {
			val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
		}

		if ( isBorderBox ) {
			// border-box includes padding, so remove it if we want content
			if ( extra === "content" ) {
				val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// at this point, extra isn't border nor margin, so remove border
			if ( extra !== "margin" ) {
				val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		} else {
			// at this point, extra isn't content, so add padding
			val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// at this point, extra isn't content nor padding, so add border
			if ( extra !== "padding" ) {
				val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	return val;
}

function getWidthOrHeight( elem, name, extra ) {

	// Start with offset property, which is equivalent to the border-box value
	var valueIsBorderBox = true,
		val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
		styles = getStyles( elem ),
		isBorderBox = support.boxSizing() && jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

	// some non-html elements return undefined for offsetWidth, so check for null/undefined
	// svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
	// MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
	if ( val <= 0 || val == null ) {
		// Fall back to computed then uncomputed css if necessary
		val = curCSS( elem, name, styles );
		if ( val < 0 || val == null ) {
			val = elem.style[ name ];
		}

		// Computed unit is not pixels. Stop here and return.
		if ( rnumnonpx.test(val) ) {
			return val;
		}

		// we need the check for style in case a browser which returns unreliable values
		// for getComputedStyle silently falls back to the reliable elem.style
		valueIsBorderBox = isBorderBox && ( support.boxSizingReliable() || val === elem.style[ name ] );

		// Normalize "", auto, and prepare for extra
		val = parseFloat( val ) || 0;
	}

	// use the active box-sizing model to add/subtract irrelevant styles
	return ( val +
		augmentWidthOrHeight(
			elem,
			name,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles
		)
	) + "px";
}

jQuery.extend({
	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {
					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"columnCount": true,
		"fillOpacity": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {
		// normalize float css property
		"float": support.cssFloat ? "cssFloat" : "styleFloat"
	},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {
		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = jQuery.camelCase( name ),
			style = elem.style;

		name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( style, origName ) );

		// gets hook for the prefixed version
		// followed by the unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// convert relative number strings (+= or -=) to relative numbers. #7345
			if ( type === "string" && (ret = rrelNum.exec( value )) ) {
				value = ( ret[1] + 1 ) * ret[2] + parseFloat( jQuery.css( elem, name ) );
				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set. See: #7116
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add 'px' to the (except for certain CSS properties)
			if ( type === "number" && !jQuery.cssNumber[ origName ] ) {
				value += "px";
			}

			// Fixes #8908, it can be done more correctly by specifing setters in cssHooks,
			// but it would mean to define eight (for every problematic property) identical functions
			if ( !support.clearCloneStyle && value === "" && name.indexOf("background") === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !("set" in hooks) || (value = hooks.set( elem, value, extra )) !== undefined ) {

				// Support: IE
				// Swallow errors from 'invalid' CSS values (#5509)
				try {
					// Support: Chrome, Safari
					// Setting style to blank string required to delete "style: x !important;"
					style[ name ] = "";
					style[ name ] = value;
				} catch(e) {}
			}

		} else {
			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks && (ret = hooks.get( elem, false, extra )) !== undefined ) {
				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var num, val, hooks,
			origName = jQuery.camelCase( name );

		// Make sure that we're working with the right name
		name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( elem.style, origName ) );

		// gets hook for the prefixed version
		// followed by the unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		//convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Return, converting to number if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || jQuery.isNumeric( num ) ? num || 0 : val;
		}
		return val;
	}
});

jQuery.each([ "height", "width" ], function( i, name ) {
	jQuery.cssHooks[ name ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {
				// certain elements can have dimension info if we invisibly show them
				// however, it must have a current display style that would benefit from this
				return elem.offsetWidth === 0 && rdisplayswap.test( jQuery.css( elem, "display" ) ) ?
					jQuery.swap( elem, cssShow, function() {
						return getWidthOrHeight( elem, name, extra );
					}) :
					getWidthOrHeight( elem, name, extra );
			}
		},

		set: function( elem, value, extra ) {
			var styles = extra && getStyles( elem );
			return setPositiveNumber( elem, value, extra ?
				augmentWidthOrHeight(
					elem,
					name,
					extra,
					support.boxSizing() && jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
					styles
				) : 0
			);
		}
	};
});

if ( !support.opacity ) {
	jQuery.cssHooks.opacity = {
		get: function( elem, computed ) {
			// IE uses filters for opacity
			return ropacity.test( (computed && elem.currentStyle ? elem.currentStyle.filter : elem.style.filter) || "" ) ?
				( 0.01 * parseFloat( RegExp.$1 ) ) + "" :
				computed ? "1" : "";
		},

		set: function( elem, value ) {
			var style = elem.style,
				currentStyle = elem.currentStyle,
				opacity = jQuery.isNumeric( value ) ? "alpha(opacity=" + value * 100 + ")" : "",
				filter = currentStyle && currentStyle.filter || style.filter || "";

			// IE has trouble with opacity if it does not have layout
			// Force it by setting the zoom level
			style.zoom = 1;

			// if setting opacity to 1, and no other filters exist - attempt to remove filter attribute #6652
			// if value === "", then remove inline opacity #12685
			if ( ( value >= 1 || value === "" ) &&
					jQuery.trim( filter.replace( ralpha, "" ) ) === "" &&
					style.removeAttribute ) {

				// Setting style.filter to null, "" & " " still leave "filter:" in the cssText
				// if "filter:" is present at all, clearType is disabled, we want to avoid this
				// style.removeAttribute is IE Only, but so apparently is this code path...
				style.removeAttribute( "filter" );

				// if there is no filter style applied in a css rule or unset inline opacity, we are done
				if ( value === "" || currentStyle && !currentStyle.filter ) {
					return;
				}
			}

			// otherwise, set new filter values
			style.filter = ralpha.test( filter ) ?
				filter.replace( ralpha, opacity ) :
				filter + " " + opacity;
		}
	};
}

jQuery.cssHooks.marginRight = addGetHookIf( support.reliableMarginRight,
	function( elem, computed ) {
		if ( computed ) {
			// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
			// Work around by temporarily setting element display to inline-block
			return jQuery.swap( elem, { "display": "inline-block" },
				curCSS, [ elem, "marginRight" ] );
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each({
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// assumes a single number if not a string
				parts = typeof value === "string" ? value.split(" ") : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( !rmargin.test( prefix ) ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
});

jQuery.fn.extend({
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( jQuery.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	},
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each(function() {
			if ( isHidden( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		});
	}
});


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || "swing";
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			if ( tween.elem[ tween.prop ] != null &&
				(!tween.elem.style || tween.elem.style[ tween.prop ] == null) ) {
				return tween.elem[ tween.prop ];
			}

			// passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails
			// so, simple values such as "10px" are parsed to Float.
			// complex values such as "rotate(1rad)" are returned as is.
			result = jQuery.css( tween.elem, tween.prop, "" );
			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {
			// use step hook for back compat - use cssHook if its there - use .style if its
			// available and use plain properties where available
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.style && ( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null || jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE <=9
// Panic based approach to setting things on disconnected nodes

Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	}
};

jQuery.fx = Tween.prototype.init;

// Back Compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, timerId,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rfxnum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" ),
	rrun = /queueHooks$/,
	animationPrefilters = [ defaultPrefilter ],
	tweeners = {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value ),
				target = tween.cur(),
				parts = rfxnum.exec( value ),
				unit = parts && parts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

				// Starting value computation is required for potential unit mismatches
				start = ( jQuery.cssNumber[ prop ] || unit !== "px" && +target ) &&
					rfxnum.exec( jQuery.css( tween.elem, prop ) ),
				scale = 1,
				maxIterations = 20;

			if ( start && start[ 3 ] !== unit ) {
				// Trust units reported by jQuery.css
				unit = unit || start[ 3 ];

				// Make sure we update the tween properties later on
				parts = parts || [];

				// Iteratively approximate from a nonzero starting point
				start = +target || 1;

				do {
					// If previous iteration zeroed out, double until we get *something*
					// Use a string for doubling factor so we don't accidentally see scale as unchanged below
					scale = scale || ".5";

					// Adjust and apply
					start = start / scale;
					jQuery.style( tween.elem, prop, start + unit );

				// Update scale, tolerating zero or NaN from tween.cur()
				// And breaking the loop if scale is unchanged or perfect, or if we've just had enough
				} while ( scale !== (scale = tween.cur() / target) && scale !== 1 && --maxIterations );
			}

			// Update tween properties
			if ( parts ) {
				start = tween.start = +start || +target || 0;
				tween.unit = unit;
				// If a +=/-= token was provided, we're doing a relative animation
				tween.end = parts[ 1 ] ?
					start + ( parts[ 1 ] + 1 ) * parts[ 2 ] :
					+parts[ 2 ];
			}

			return tween;
		} ]
	};

// Animations created synchronously will run synchronously
function createFxNow() {
	setTimeout(function() {
		fxNow = undefined;
	});
	return ( fxNow = jQuery.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		attrs = { height: type },
		i = 0;

	// if we include width, step value is 1 to do all cssExpand values,
	// if we don't include width, step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4 ; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( tweeners[ prop ] || [] ).concat( tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( (tween = collection[ index ].call( animation, prop, value )) ) {

			// we're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	/* jshint validthis: true */
	var prop, value, toggle, tween, hooks, oldfire, display, dDisplay,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHidden( elem ),
		dataShow = jQuery._data( elem, "fxshow" );

	// handle queue: false promises
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always(function() {
			// doing this makes sure that the complete handler will be called
			// before this completes
			anim.always(function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			});
		});
	}

	// height/width overflow pass
	if ( elem.nodeType === 1 && ( "height" in props || "width" in props ) ) {
		// Make sure that nothing sneaks out
		// Record all 3 overflow attributes because IE does not
		// change the overflow attribute when overflowX and
		// overflowY are set to the same value
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Set display property to inline-block for height/width
		// animations on inline elements that are having width/height animated
		display = jQuery.css( elem, "display" );
		dDisplay = defaultDisplay( elem.nodeName );
		if ( display === "none" ) {
			display = dDisplay;
		}
		if ( display === "inline" &&
				jQuery.css( elem, "float" ) === "none" ) {

			// inline-level elements accept inline-block;
			// block-level elements need to be inline with layout
			if ( !support.inlineBlockNeedsLayout || dDisplay === "inline" ) {
				style.display = "inline-block";
			} else {
				style.zoom = 1;
			}
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		if ( !support.shrinkWrapBlocks() ) {
			anim.always(function() {
				style.overflow = opts.overflow[ 0 ];
				style.overflowX = opts.overflow[ 1 ];
				style.overflowY = opts.overflow[ 2 ];
			});
		}
	}

	// show/hide pass
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.exec( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// If there is dataShow left over from a stopped hide or show and we are going to proceed with show, we should pretend to be hidden
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
		}
	}

	if ( !jQuery.isEmptyObject( orig ) ) {
		if ( dataShow ) {
			if ( "hidden" in dataShow ) {
				hidden = dataShow.hidden;
			}
		} else {
			dataShow = jQuery._data( elem, "fxshow", {} );
		}

		// store state if its toggle - enables .stop().toggle() to "reverse"
		if ( toggle ) {
			dataShow.hidden = !hidden;
		}
		if ( hidden ) {
			jQuery( elem ).show();
		} else {
			anim.done(function() {
				jQuery( elem ).hide();
			});
		}
		anim.done(function() {
			var prop;
			jQuery._removeData( elem, "fxshow" );
			for ( prop in orig ) {
				jQuery.style( elem, prop, orig[ prop ] );
			}
		});
		for ( prop in orig ) {
			tween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );

			if ( !( prop in dataShow ) ) {
				dataShow[ prop ] = tween.start;
				if ( hidden ) {
					tween.end = tween.start;
					tween.start = prop === "width" || prop === "height" ? 1 : 0;
				}
			}
		}
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = jQuery.camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( jQuery.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// not quite $.extend, this wont overwrite keys already present.
			// also - reusing 'index' from above because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = animationPrefilters.length,
		deferred = jQuery.Deferred().always( function() {
			// don't match elem in the :animated selector
			delete tick.elem;
		}),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),
				// archaic crash bug won't allow us to use 1 - ( 0.5 || 0 ) (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length ; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ]);

			if ( percent < 1 && length ) {
				return remaining;
			} else {
				deferred.resolveWith( elem, [ animation ] );
				return false;
			}
		},
		animation = deferred.promise({
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, { specialEasing: {} }, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,
					// if we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length ; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// resolve when we played the last frame
				// otherwise, reject
				if ( gotoEnd ) {
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		}),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length ; index++ ) {
		result = animationPrefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( jQuery.isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		})
	);

	// attach callbacks from options
	return animation.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );
}

jQuery.Animation = jQuery.extend( Animation, {
	tweener: function( props, callback ) {
		if ( jQuery.isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.split(" ");
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length ; index++ ) {
			prop = props[ index ];
			tweeners[ prop ] = tweeners[ prop ] || [];
			tweeners[ prop ].unshift( callback );
		}
	},

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			animationPrefilters.unshift( callback );
		} else {
			animationPrefilters.push( callback );
		}
	}
});

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			jQuery.isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
	};

	opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration :
		opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;

	// normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( jQuery.isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend({
	fadeTo: function( speed, to, easing, callback ) {

		// show any hidden elements after setting opacity to 0
		return this.filter( isHidden ).css( "opacity", 0 ).show()

			// animate to the value specified
			.end().animate({ opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {
				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || jQuery._data( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each(function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = jQuery._data( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && (type == null || timers[ index ].queue === type) ) {
					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// start the next in the queue if the last step wasn't forced
			// timers currently will call their complete callbacks, which will dequeue
			// but only if they were gotoEnd
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		});
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each(function() {
			var index,
				data = jQuery._data( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// enable finishing flag on private data
			data.finish = true;

			// empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// turn off finishing flag
			delete data.finish;
		});
	}
});

jQuery.each([ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
});

// Generate shortcuts for custom animations
jQuery.each({
	slideDown: genFx("show"),
	slideUp: genFx("hide"),
	slideToggle: genFx("toggle"),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
});

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		timers = jQuery.timers,
		i = 0;

	fxNow = jQuery.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];
		// Checks the timer has not already been removed
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	if ( timer() ) {
		jQuery.fx.start();
	} else {
		jQuery.timers.pop();
	}
};

jQuery.fx.interval = 13;

jQuery.fx.start = function() {
	if ( !timerId ) {
		timerId = setInterval( jQuery.fx.tick, jQuery.fx.interval );
	}
};

jQuery.fx.stop = function() {
	clearInterval( timerId );
	timerId = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,
	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = setTimeout( next, time );
		hooks.stop = function() {
			clearTimeout( timeout );
		};
	});
};


(function() {
	var a, input, select, opt,
		div = document.createElement("div" );

	// Setup
	div.setAttribute( "className", "t" );
	div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
	a = div.getElementsByTagName("a")[ 0 ];

	// First batch of tests.
	select = document.createElement("select");
	opt = select.appendChild( document.createElement("option") );
	input = div.getElementsByTagName("input")[ 0 ];

	a.style.cssText = "top:1px";

	// Test setAttribute on camelCase class. If it works, we need attrFixes when doing get/setAttribute (ie6/7)
	support.getSetAttribute = div.className !== "t";

	// Get the style information from getAttribute
	// (IE uses .cssText instead)
	support.style = /top/.test( a.getAttribute("style") );

	// Make sure that URLs aren't manipulated
	// (IE normalizes it by default)
	support.hrefNormalized = a.getAttribute("href") === "/a";

	// Check the default checkbox/radio value ("" on WebKit; "on" elsewhere)
	support.checkOn = !!input.value;

	// Make sure that a selected-by-default option has a working selected property.
	// (WebKit defaults to false instead of true, IE too, if it's in an optgroup)
	support.optSelected = opt.selected;

	// Tests for enctype support on a form (#6743)
	support.enctype = !!document.createElement("form").enctype;

	// Make sure that the options inside disabled selects aren't marked as disabled
	// (WebKit marks them as disabled)
	select.disabled = true;
	support.optDisabled = !opt.disabled;

	// Support: IE8 only
	// Check if we can trust getAttribute("value")
	input = document.createElement( "input" );
	input.setAttribute( "value", "" );
	support.input = input.getAttribute( "value" ) === "";

	// Check if an input maintains its value after becoming a radio
	input.value = "t";
	input.setAttribute( "type", "radio" );
	support.radioValue = input.value === "t";

	// Null elements to avoid leaks in IE.
	a = input = select = opt = div = null;
})();


var rreturn = /\r/g;

jQuery.fn.extend({
	val: function( value ) {
		var hooks, ret, isFunction,
			elem = this[0];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] || jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks && "get" in hooks && (ret = hooks.get( elem, "value" )) !== undefined ) {
					return ret;
				}

				ret = elem.value;

				return typeof ret === "string" ?
					// handle most common string cases
					ret.replace(rreturn, "") :
					// handle cases where value is null/undef or number
					ret == null ? "" : ret;
			}

			return;
		}

		isFunction = jQuery.isFunction( value );

		return this.each(function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( isFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";
			} else if ( typeof val === "number" ) {
				val += "";
			} else if ( jQuery.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				});
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !("set" in hooks) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		});
	}
});

jQuery.extend({
	valHooks: {
		option: {
			get: function( elem ) {
				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :
					jQuery.text( elem );
			}
		},
		select: {
			get: function( elem ) {
				var value, option,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one" || index < 0,
					values = one ? null : [],
					max = one ? index + 1 : options.length,
					i = index < 0 ?
						max :
						one ? index : 0;

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// oldIE doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&
							// Don't return options that are disabled or in a disabled optgroup
							( support.optDisabled ? !option.disabled : option.getAttribute("disabled") === null ) &&
							( !option.parentNode.disabled || !jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];

					if ( jQuery.inArray( jQuery.valHooks.option.get( option ), values ) >= 0 ) {

						// Support: IE6
						// When new option element is added to select box we need to
						// force reflow of newly added node in order to workaround delay
						// of initialization properties
						try {
							option.selected = optionSet = true;

						} catch ( _ ) {

							// Will be executed only in IE6
							option.scrollHeight;
						}

					} else {
						option.selected = false;
					}
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}

				return options;
			}
		}
	}
});

// Radios and checkboxes getter/setter
jQuery.each([ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( jQuery.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery(elem).val(), value ) >= 0 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			// Support: Webkit
			// "" is returned instead of "on" if a value isn't specified
			return elem.getAttribute("value") === null ? "on" : elem.value;
		};
	}
});




var nodeHook, boolHook,
	attrHandle = jQuery.expr.attrHandle,
	ruseDefault = /^(?:checked|selected)$/i,
	getSetAttribute = support.getSetAttribute,
	getSetInput = support.input;

jQuery.fn.extend({
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each(function() {
			jQuery.removeAttr( this, name );
		});
	}
});

jQuery.extend({
	attr: function( elem, name, value ) {
		var hooks, ret,
			nType = elem.nodeType;

		// don't get/set attributes on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === strundefined ) {
			return jQuery.prop( elem, name, value );
		}

		// All attributes are lowercase
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			name = name.toLowerCase();
			hooks = jQuery.attrHooks[ name ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : nodeHook );
		}

		if ( value !== undefined ) {

			if ( value === null ) {
				jQuery.removeAttr( elem, name );

			} else if ( hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ) {
				return ret;

			} else {
				elem.setAttribute( name, value + "" );
				return value;
			}

		} else if ( hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ) {
			return ret;

		} else {
			ret = jQuery.find.attr( elem, name );

			// Non-existent attributes return null, we normalize to undefined
			return ret == null ?
				undefined :
				ret;
		}
	},

	removeAttr: function( elem, value ) {
		var name, propName,
			i = 0,
			attrNames = value && value.match( rnotwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( (name = attrNames[i++]) ) {
				propName = jQuery.propFix[ name ] || name;

				// Boolean attributes get special treatment (#10870)
				if ( jQuery.expr.match.bool.test( name ) ) {
					// Set corresponding property to false
					if ( getSetInput && getSetAttribute || !ruseDefault.test( name ) ) {
						elem[ propName ] = false;
					// Support: IE<9
					// Also clear defaultChecked/defaultSelected (if appropriate)
					} else {
						elem[ jQuery.camelCase( "default-" + name ) ] =
							elem[ propName ] = false;
					}

				// See #9699 for explanation of this approach (setting first, then removal)
				} else {
					jQuery.attr( elem, name, "" );
				}

				elem.removeAttribute( getSetAttribute ? name : propName );
			}
		}
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" && jQuery.nodeName(elem, "input") ) {
					// Setting the type on a radio button after the value resets the value in IE6-9
					// Reset value to default in case type is set after value during creation
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	}
});

// Hook for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {
			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else if ( getSetInput && getSetAttribute || !ruseDefault.test( name ) ) {
			// IE<8 needs the *property* name
			elem.setAttribute( !getSetAttribute && jQuery.propFix[ name ] || name, name );

		// Use defaultChecked and defaultSelected for oldIE
		} else {
			elem[ jQuery.camelCase( "default-" + name ) ] = elem[ name ] = true;
		}

		return name;
	}
};

// Retrieve booleans specially
jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {

	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = getSetInput && getSetAttribute || !ruseDefault.test( name ) ?
		function( elem, name, isXML ) {
			var ret, handle;
			if ( !isXML ) {
				// Avoid an infinite loop by temporarily removing this function from the getter
				handle = attrHandle[ name ];
				attrHandle[ name ] = ret;
				ret = getter( elem, name, isXML ) != null ?
					name.toLowerCase() :
					null;
				attrHandle[ name ] = handle;
			}
			return ret;
		} :
		function( elem, name, isXML ) {
			if ( !isXML ) {
				return elem[ jQuery.camelCase( "default-" + name ) ] ?
					name.toLowerCase() :
					null;
			}
		};
});

// fix oldIE attroperties
if ( !getSetInput || !getSetAttribute ) {
	jQuery.attrHooks.value = {
		set: function( elem, value, name ) {
			if ( jQuery.nodeName( elem, "input" ) ) {
				// Does not return so that setAttribute is also used
				elem.defaultValue = value;
			} else {
				// Use nodeHook if defined (#1954); otherwise setAttribute is fine
				return nodeHook && nodeHook.set( elem, value, name );
			}
		}
	};
}

// IE6/7 do not support getting/setting some attributes with get/setAttribute
if ( !getSetAttribute ) {

	// Use this for any attribute in IE6/7
	// This fixes almost every IE6/7 issue
	nodeHook = {
		set: function( elem, value, name ) {
			// Set the existing or create a new attribute node
			var ret = elem.getAttributeNode( name );
			if ( !ret ) {
				elem.setAttributeNode(
					(ret = elem.ownerDocument.createAttribute( name ))
				);
			}

			ret.value = value += "";

			// Break association with cloned elements by also using setAttribute (#9646)
			if ( name === "value" || value === elem.getAttribute( name ) ) {
				return value;
			}
		}
	};

	// Some attributes are constructed with empty-string values when not defined
	attrHandle.id = attrHandle.name = attrHandle.coords =
		function( elem, name, isXML ) {
			var ret;
			if ( !isXML ) {
				return (ret = elem.getAttributeNode( name )) && ret.value !== "" ?
					ret.value :
					null;
			}
		};

	// Fixing value retrieval on a button requires this module
	jQuery.valHooks.button = {
		get: function( elem, name ) {
			var ret = elem.getAttributeNode( name );
			if ( ret && ret.specified ) {
				return ret.value;
			}
		},
		set: nodeHook.set
	};

	// Set contenteditable to false on removals(#10429)
	// Setting to empty string throws an error as an invalid value
	jQuery.attrHooks.contenteditable = {
		set: function( elem, value, name ) {
			nodeHook.set( elem, value === "" ? false : value, name );
		}
	};

	// Set width and height to auto instead of 0 on empty string( Bug #8150 )
	// This is for removals
	jQuery.each([ "width", "height" ], function( i, name ) {
		jQuery.attrHooks[ name ] = {
			set: function( elem, value ) {
				if ( value === "" ) {
					elem.setAttribute( name, "auto" );
					return value;
				}
			}
		};
	});
}

if ( !support.style ) {
	jQuery.attrHooks.style = {
		get: function( elem ) {
			// Return undefined in the case of empty string
			// Note: IE uppercases css property names, but if we were to .toLowerCase()
			// .cssText, that would destroy case senstitivity in URL's, like in "background"
			return elem.style.cssText || undefined;
		},
		set: function( elem, value ) {
			return ( elem.style.cssText = value + "" );
		}
	};
}




var rfocusable = /^(?:input|select|textarea|button|object)$/i,
	rclickable = /^(?:a|area)$/i;

jQuery.fn.extend({
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		name = jQuery.propFix[ name ] || name;
		return this.each(function() {
			// try/catch handles cases where IE balks (such as removing a property on window)
			try {
				this[ name ] = undefined;
				delete this[ name ];
			} catch( e ) {}
		});
	}
});

jQuery.extend({
	propFix: {
		"for": "htmlFor",
		"class": "className"
	},

	prop: function( elem, name, value ) {
		var ret, hooks, notxml,
			nType = elem.nodeType;

		// don't get/set properties on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		notxml = nType !== 1 || !jQuery.isXMLDoc( elem );

		if ( notxml ) {
			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			return hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ?
				ret :
				( elem[ name ] = value );

		} else {
			return hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ?
				ret :
				elem[ name ];
		}
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {
				// elem.tabIndex doesn't always return the correct value when it hasn't been explicitly set
				// http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				// Use proper attribute retrieval(#12072)
				var tabindex = jQuery.find.attr( elem, "tabindex" );

				return tabindex ?
					parseInt( tabindex, 10 ) :
					rfocusable.test( elem.nodeName ) || rclickable.test( elem.nodeName ) && elem.href ?
						0 :
						-1;
			}
		}
	}
});

// Some attributes require a special call on IE
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !support.hrefNormalized ) {
	// href/src property should get the full normalized URL (#10299/#12915)
	jQuery.each([ "href", "src" ], function( i, name ) {
		jQuery.propHooks[ name ] = {
			get: function( elem ) {
				return elem.getAttribute( name, 4 );
			}
		};
	});
}

// Support: Safari, IE9+
// mis-reports the default selected property of an option
// Accessing the parent's selectedIndex property fixes it
if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {
			var parent = elem.parentNode;

			if ( parent ) {
				parent.selectedIndex;

				// Make sure that it also works with optgroups, see #5701
				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
			return null;
		}
	};
}

jQuery.each([
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
});

// IE6/7 call enctype encoding
if ( !support.enctype ) {
	jQuery.propFix.enctype = "encoding";
}




var rclass = /[\t\r\n\f]/g;

jQuery.fn.extend({
	addClass: function( value ) {
		var classes, elem, cur, clazz, j, finalValue,
			i = 0,
			len = this.length,
			proceed = typeof value === "string" && value;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).addClass( value.call( this, j, this.className ) );
			});
		}

		if ( proceed ) {
			// The disjunction here is for better compressibility (see removeClass)
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				cur = elem.nodeType === 1 && ( elem.className ?
					( " " + elem.className + " " ).replace( rclass, " " ) :
					" "
				);

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// only assign if different to avoid unneeded rendering.
					finalValue = jQuery.trim( cur );
					if ( elem.className !== finalValue ) {
						elem.className = finalValue;
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, clazz, j, finalValue,
			i = 0,
			len = this.length,
			proceed = arguments.length === 0 || typeof value === "string" && value;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).removeClass( value.call( this, j, this.className ) );
			});
		}
		if ( proceed ) {
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( elem.className ?
					( " " + elem.className + " " ).replace( rclass, " " ) :
					""
				);

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) >= 0 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// only assign if different to avoid unneeded rendering.
					finalValue = value ? jQuery.trim( cur ) : "";
					if ( elem.className !== finalValue ) {
						elem.className = finalValue;
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value;

		if ( typeof stateVal === "boolean" && type === "string" ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( i ) {
				jQuery( this ).toggleClass( value.call(this, i, this.className, stateVal), stateVal );
			});
		}

		return this.each(function() {
			if ( type === "string" ) {
				// toggle individual class names
				var className,
					i = 0,
					self = jQuery( this ),
					classNames = value.match( rnotwhite ) || [];

				while ( (className = classNames[ i++ ]) ) {
					// check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( type === strundefined || type === "boolean" ) {
				if ( this.className ) {
					// store className if set
					jQuery._data( this, "__className__", this.className );
				}

				// If the element has a class name or if we're passed "false",
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				this.className = this.className || value === false ? "" : jQuery._data( this, "__className__" ) || "";
			}
		});
	},

	hasClass: function( selector ) {
		var className = " " + selector + " ",
			i = 0,
			l = this.length;
		for ( ; i < l; i++ ) {
			if ( this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf( className ) >= 0 ) {
				return true;
			}
		}

		return false;
	}
});




// Return jQuery for attributes-only inclusion


jQuery.each( ("blur focus focusin focusout load resize scroll unload click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup error contextmenu").split(" "), function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
});

jQuery.fn.extend({
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	},

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {
		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ? this.off( selector, "**" ) : this.off( types, selector || "**", fn );
	}
});


var nonce = jQuery.now();

var rquery = (/\?/);



var rvalidtokens = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;

jQuery.parseJSON = function( data ) {
	// Attempt to parse using the native JSON parser first
	if ( window.JSON && window.JSON.parse ) {
		// Support: Android 2.3
		// Workaround failure to string-cast null input
		return window.JSON.parse( data + "" );
	}

	var requireNonComma,
		depth = null,
		str = jQuery.trim( data + "" );

	// Guard against invalid (and possibly dangerous) input by ensuring that nothing remains
	// after removing valid tokens
	return str && !jQuery.trim( str.replace( rvalidtokens, function( token, comma, open, close ) {

		// Force termination if we see a misplaced comma
		if ( requireNonComma && comma ) {
			depth = 0;
		}

		// Perform no more replacements after returning to outermost depth
		if ( depth === 0 ) {
			return token;
		}

		// Commas must not follow "[", "{", or ","
		requireNonComma = open || comma;

		// Determine new depth
		// array/object open ("[" or "{"): depth += true - false (increment)
		// array/object close ("]" or "}"): depth += false - true (decrement)
		// other cases ("," or primitive): depth += true - true (numeric cast)
		depth += !close - !open;

		// Remove this token
		return "";
	}) ) ?
		( Function( "return " + str ) )() :
		jQuery.error( "Invalid JSON: " + data );
};


// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml, tmp;
	if ( !data || typeof data !== "string" ) {
		return null;
	}
	try {
		if ( window.DOMParser ) { // Standard
			tmp = new DOMParser();
			xml = tmp.parseFromString( data, "text/xml" );
		} else { // IE
			xml = new ActiveXObject( "Microsoft.XMLDOM" );
			xml.async = "false";
			xml.loadXML( data );
		}
	} catch( e ) {
		xml = undefined;
	}
	if ( !xml || !xml.documentElement || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	// Document location
	ajaxLocParts,
	ajaxLocation,

	rhash = /#.*$/,
	rts = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg, // IE leaves an \r character at EOL
	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,
	rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat("*");

// #8138, IE may throw an exception when accessing
// a field from window.location if document.domain has been set
try {
	ajaxLocation = location.href;
} catch( e ) {
	// Use the href attribute of an A element
	// since IE will modify it given document.location
	ajaxLocation = document.createElement( "a" );
	ajaxLocation.href = "";
	ajaxLocation = ajaxLocation.href;
}

// Segment location into parts
ajaxLocParts = rurl.exec( ajaxLocation.toLowerCase() ) || [];

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnotwhite ) || [];

		if ( jQuery.isFunction( func ) ) {
			// For each dataType in the dataTypeExpression
			while ( (dataType = dataTypes[i++]) ) {
				// Prepend if requested
				if ( dataType.charAt( 0 ) === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					(structure[ dataType ] = structure[ dataType ] || []).unshift( func );

				// Otherwise append
				} else {
					(structure[ dataType ] = structure[ dataType ] || []).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[ dataTypeOrTransport ] ) {
				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		});
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var deep, key,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || (deep = {}) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {
	var firstDataType, ct, finalDataType, type,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {
		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[0] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}
		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},
		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

			// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {
								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s[ "throws" ] ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return { state: "parsererror", error: conv ? e : "No conversion from " + prev + " to " + current };
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend({

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: ajaxLocation,
		type: "GET",
		isLocal: rlocalProtocol.test( ajaxLocParts[ 1 ] ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /xml/,
			html: /html/,
			json: /json/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": jQuery.parseJSON,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var // Cross-domain detection vars
			parts,
			// Loop variable
			i,
			// URL without anti-cache param
			cacheURL,
			// Response headers as string
			responseHeadersString,
			// timeout handle
			timeoutTimer,

			// To know if global events are to be dispatched
			fireGlobals,

			transport,
			// Response headers
			responseHeaders,
			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),
			// Callbacks context
			callbackContext = s.context || s,
			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context && ( callbackContext.nodeType || callbackContext.jquery ) ?
				jQuery( callbackContext ) :
				jQuery.event,
			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks("once memory"),
			// Status-dependent callbacks
			statusCode = s.statusCode || {},
			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},
			// The jqXHR state
			state = 0,
			// Default abort message
			strAbort = "canceled",
			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( state === 2 ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( (match = rheaders.exec( responseHeadersString )) ) {
								responseHeaders[ match[1].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return state === 2 ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					var lname = name.toLowerCase();
					if ( !state ) {
						name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( !state ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( state < 2 ) {
							for ( code in map ) {
								// Lazy-add the new callback in a way that preserves old ones
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						} else {
							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR ).complete = completeDeferred.add;
		jqXHR.success = jqXHR.done;
		jqXHR.error = jqXHR.fail;

		// Remove hash character (#7531: and string promotion)
		// Add protocol if not provided (#5866: IE7 issue with protocol-less urls)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || ajaxLocation ) + "" ).replace( rhash, "" ).replace( rprotocol, ajaxLocParts[ 1 ] + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().match( rnotwhite ) || [ "" ];

		// A cross-domain request is in order when we have a protocol:host:port mismatch
		if ( s.crossDomain == null ) {
			parts = rurl.exec( s.url.toLowerCase() );
			s.crossDomain = !!( parts &&
				( parts[ 1 ] !== ajaxLocParts[ 1 ] || parts[ 2 ] !== ajaxLocParts[ 2 ] ||
					( parts[ 3 ] || ( parts[ 1 ] === "http:" ? "80" : "443" ) ) !==
						( ajaxLocParts[ 3 ] || ( ajaxLocParts[ 1 ] === "http:" ? "80" : "443" ) ) )
			);
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( state === 2 ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		fireGlobals = s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger("ajaxStart");
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		cacheURL = s.url;

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// If data is available, append data to url
			if ( s.data ) {
				cacheURL = ( s.url += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data );
				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add anti-cache in url if needed
			if ( s.cache === false ) {
				s.url = rts.test( cacheURL ) ?

					// If there is already a '_' parameter, set its value
					cacheURL.replace( rts, "$1_=" + nonce++ ) :

					// Otherwise add one to the end
					cacheURL + ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + nonce++;
			}
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[0] ] ?
				s.accepts[ s.dataTypes[0] ] + ( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend && ( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {
			// Abort if not done already and return
			return jqXHR.abort();
		}

		// aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		for ( i in { success: 1, error: 1, complete: 1 } ) {
			jqXHR[ i ]( s[ i ] );
		}

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}
			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = setTimeout(function() {
					jqXHR.abort("timeout");
				}, s.timeout );
			}

			try {
				state = 1;
				transport.send( requestHeaders, done );
			} catch ( e ) {
				// Propagate exception as error if not done
				if ( state < 2 ) {
					done( -1, e );
				// Simply rethrow otherwise
				} else {
					throw e;
				}
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Called once
			if ( state === 2 ) {
				return;
			}

			// State is "done" now
			state = 2;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader("Last-Modified");
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader("etag");
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {
				// We extract error from statusText
				// then normalize statusText and status for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );
				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger("ajaxStop");
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
});

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {
		// shift arguments if data argument was omitted
		if ( jQuery.isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		return jQuery.ajax({
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		});
	};
});

// Attach a bunch of functions for handling common AJAX events
jQuery.each( [ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
});


jQuery._evalUrl = function( url ) {
	return jQuery.ajax({
		url: url,
		type: "GET",
		dataType: "script",
		async: false,
		global: false,
		"throws": true
	});
};


jQuery.fn.extend({
	wrapAll: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each(function(i) {
				jQuery(this).wrapAll( html.call(this, i) );
			});
		}

		if ( this[0] ) {
			// The elements to wrap the target around
			var wrap = jQuery( html, this[0].ownerDocument ).eq(0).clone(true);

			if ( this[0].parentNode ) {
				wrap.insertBefore( this[0] );
			}

			wrap.map(function() {
				var elem = this;

				while ( elem.firstChild && elem.firstChild.nodeType === 1 ) {
					elem = elem.firstChild;
				}

				return elem;
			}).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each(function(i) {
				jQuery(this).wrapInner( html.call(this, i) );
			});
		}

		return this.each(function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		});
	},

	wrap: function( html ) {
		var isFunction = jQuery.isFunction( html );

		return this.each(function(i) {
			jQuery( this ).wrapAll( isFunction ? html.call(this, i) : html );
		});
	},

	unwrap: function() {
		return this.parent().each(function() {
			if ( !jQuery.nodeName( this, "body" ) ) {
				jQuery( this ).replaceWith( this.childNodes );
			}
		}).end();
	}
});


jQuery.expr.filters.hidden = function( elem ) {
	// Support: Opera <= 12.12
	// Opera reports offsetWidths and offsetHeights less than zero on some elements
	return elem.offsetWidth <= 0 && elem.offsetHeight <= 0 ||
		(!support.reliableHiddenOffsets() &&
			((elem.style && elem.style.display) || jQuery.css( elem, "display" )) === "none");
};

jQuery.expr.filters.visible = function( elem ) {
	return !jQuery.expr.filters.hidden( elem );
};




var r20 = /%20/g,
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( jQuery.isArray( obj ) ) {
		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {
				// Treat each array item as a scalar.
				add( prefix, v );

			} else {
				// Item is non-scalar (array or object), encode its numeric index.
				buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, traditional, add );
			}
		});

	} else if ( !traditional && jQuery.type( obj ) === "object" ) {
		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {
		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, value ) {
			// If value is a function, invoke it and return its value
			value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
			s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
		};

	// Set traditional to true for jQuery <= 1.3.2 behavior.
	if ( traditional === undefined ) {
		traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		});

	} else {
		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" ).replace( r20, "+" );
};

jQuery.fn.extend({
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map(function() {
			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		})
		.filter(function() {
			var type = this.type;
			// Use .is(":disabled") so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		})
		.map(function( i, elem ) {
			var val = jQuery( this ).val();

			return val == null ?
				null :
				jQuery.isArray( val ) ?
					jQuery.map( val, function( val ) {
						return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
					}) :
					{ name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		}).get();
	}
});


// Create the request object
// (This is still attached to ajaxSettings for backward compatibility)
jQuery.ajaxSettings.xhr = window.ActiveXObject !== undefined ?
	// Support: IE6+
	function() {

		// XHR cannot access local files, always use ActiveX for that case
		return !this.isLocal &&

			// Support: IE7-8
			// oldIE XHR does not support non-RFC2616 methods (#13240)
			// See http://msdn.microsoft.com/en-us/library/ie/ms536648(v=vs.85).aspx
			// and http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9
			// Although this check for six methods instead of eight
			// since IE also does not support "trace" and "connect"
			/^(get|post|head|put|delete|options)$/i.test( this.type ) &&

			createStandardXHR() || createActiveXHR();
	} :
	// For all other browsers, use the standard XMLHttpRequest object
	createStandardXHR;

var xhrId = 0,
	xhrCallbacks = {},
	xhrSupported = jQuery.ajaxSettings.xhr();

// Support: IE<10
// Open requests must be manually aborted on unload (#5280)
if ( window.ActiveXObject ) {
	jQuery( window ).on( "unload", function() {
		for ( var key in xhrCallbacks ) {
			xhrCallbacks[ key ]( undefined, true );
		}
	});
}

// Determine support properties
support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
xhrSupported = support.ajax = !!xhrSupported;

// Create transport if the browser can provide an xhr
if ( xhrSupported ) {

	jQuery.ajaxTransport(function( options ) {
		// Cross domain only allowed if supported through XMLHttpRequest
		if ( !options.crossDomain || support.cors ) {

			var callback;

			return {
				send: function( headers, complete ) {
					var i,
						xhr = options.xhr(),
						id = ++xhrId;

					// Open the socket
					xhr.open( options.type, options.url, options.async, options.username, options.password );

					// Apply custom fields if provided
					if ( options.xhrFields ) {
						for ( i in options.xhrFields ) {
							xhr[ i ] = options.xhrFields[ i ];
						}
					}

					// Override mime type if needed
					if ( options.mimeType && xhr.overrideMimeType ) {
						xhr.overrideMimeType( options.mimeType );
					}

					// X-Requested-With header
					// For cross-domain requests, seeing as conditions for a preflight are
					// akin to a jigsaw puzzle, we simply never set it to be sure.
					// (it can always be set on a per-request basis or even using ajaxSetup)
					// For same-domain requests, won't change header if already provided.
					if ( !options.crossDomain && !headers["X-Requested-With"] ) {
						headers["X-Requested-With"] = "XMLHttpRequest";
					}

					// Set headers
					for ( i in headers ) {
						// Support: IE<9
						// IE's ActiveXObject throws a 'Type Mismatch' exception when setting
						// request header to a null-value.
						//
						// To keep consistent with other XHR implementations, cast the value
						// to string and ignore `undefined`.
						if ( headers[ i ] !== undefined ) {
							xhr.setRequestHeader( i, headers[ i ] + "" );
						}
					}

					// Do send the request
					// This may raise an exception which is actually
					// handled in jQuery.ajax (so no try/catch here)
					xhr.send( ( options.hasContent && options.data ) || null );

					// Listener
					callback = function( _, isAbort ) {
						var status, statusText, responses;

						// Was never called and is aborted or complete
						if ( callback && ( isAbort || xhr.readyState === 4 ) ) {
							// Clean up
							delete xhrCallbacks[ id ];
							callback = undefined;
							xhr.onreadystatechange = jQuery.noop;

							// Abort manually if needed
							if ( isAbort ) {
								if ( xhr.readyState !== 4 ) {
									xhr.abort();
								}
							} else {
								responses = {};
								status = xhr.status;

								// Support: IE<10
								// Accessing binary-data responseText throws an exception
								// (#11426)
								if ( typeof xhr.responseText === "string" ) {
									responses.text = xhr.responseText;
								}

								// Firefox throws an exception when accessing
								// statusText for faulty cross-domain requests
								try {
									statusText = xhr.statusText;
								} catch( e ) {
									// We normalize with Webkit giving an empty statusText
									statusText = "";
								}

								// Filter status for non standard behaviors

								// If the request is local and we have data: assume a success
								// (success with no data won't get notified, that's the best we
								// can do given current implementations)
								if ( !status && options.isLocal && !options.crossDomain ) {
									status = responses.text ? 200 : 404;
								// IE - #1450: sometimes returns 1223 when it should be 204
								} else if ( status === 1223 ) {
									status = 204;
								}
							}
						}

						// Call complete if needed
						if ( responses ) {
							complete( status, statusText, responses, xhr.getAllResponseHeaders() );
						}
					};

					if ( !options.async ) {
						// if we're in sync mode we fire the callback
						callback();
					} else if ( xhr.readyState === 4 ) {
						// (IE6 & IE7) if it's in cache and has been
						// retrieved directly we need to fire the callback
						setTimeout( callback );
					} else {
						// Add to the list of active xhr callbacks
						xhr.onreadystatechange = xhrCallbacks[ id ] = callback;
					}
				},

				abort: function() {
					if ( callback ) {
						callback( undefined, true );
					}
				}
			};
		}
	});
}

// Functions to create xhrs
function createStandardXHR() {
	try {
		return new window.XMLHttpRequest();
	} catch( e ) {}
}

function createActiveXHR() {
	try {
		return new window.ActiveXObject( "Microsoft.XMLHTTP" );
	} catch( e ) {}
}




// Install script dataType
jQuery.ajaxSetup({
	accepts: {
		script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /(?:java|ecma)script/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
});

// Handle cache's special case and global
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
		s.global = false;
	}
});

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function(s) {

	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {

		var script,
			head = document.head || jQuery("head")[0] || document.documentElement;

		return {

			send: function( _, callback ) {

				script = document.createElement("script");

				script.async = true;

				if ( s.scriptCharset ) {
					script.charset = s.scriptCharset;
				}

				script.src = s.url;

				// Attach handlers for all browsers
				script.onload = script.onreadystatechange = function( _, isAbort ) {

					if ( isAbort || !script.readyState || /loaded|complete/.test( script.readyState ) ) {

						// Handle memory leak in IE
						script.onload = script.onreadystatechange = null;

						// Remove the script
						if ( script.parentNode ) {
							script.parentNode.removeChild( script );
						}

						// Dereference the script
						script = null;

						// Callback if not abort
						if ( !isAbort ) {
							callback( 200, "success" );
						}
					}
				};

				// Circumvent IE6 bugs with base elements (#2709 and #4378) by prepending
				// Use native DOM manipulation to avoid our domManip AJAX trickery
				head.insertBefore( script, head.firstChild );
			},

			abort: function() {
				if ( script ) {
					script.onload( undefined, true );
				}
			}
		};
	}
});




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup({
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
});

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" && !( s.contentType || "" ).indexOf("application/x-www-form-urlencoded") && rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters["script json"] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always(function() {
			// Restore preexisting value
			window[ callbackName ] = overwritten;

			// Save back as free
			if ( s[ callbackName ] ) {
				// make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && jQuery.isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		});

		// Delegate to script
		return "script";
	}
});




// data: string of html
// context (optional): If specified, the fragment will be created in this context, defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( !data || typeof data !== "string" ) {
		return null;
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}
	context = context || document;

	var parsed = rsingleTag.exec( data ),
		scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[1] ) ];
	}

	parsed = jQuery.buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


// Keep a copy of the old load method
var _load = jQuery.fn.load;

/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	if ( typeof url !== "string" && _load ) {
		return _load.apply( this, arguments );
	}

	var selector, response, type,
		self = this,
		off = url.indexOf(" ");

	if ( off >= 0 ) {
		selector = url.slice( off, url.length );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( jQuery.isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax({
			url: url,

			// if "type" variable is undefined, then "GET" method will be used
			type: type,
			dataType: "html",
			data: params
		}).done(function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery("<div>").append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		}).complete( callback && function( jqXHR, status ) {
			self.each( callback, response || [ jqXHR.responseText, status, jqXHR ] );
		});
	}

	return this;
};




jQuery.expr.filters.animated = function( elem ) {
	return jQuery.grep(jQuery.timers, function( fn ) {
		return elem === fn.elem;
	}).length;
};





var docElem = window.document.documentElement;

/**
 * Gets a window from an element
 */
function getWindow( elem ) {
	return jQuery.isWindow( elem ) ?
		elem :
		elem.nodeType === 9 ?
			elem.defaultView || elem.parentWindow :
			false;
}

jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			jQuery.inArray("auto", [ curCSSTop, curCSSLeft ] ) > -1;

		// need to be able to calculate position if either top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;
		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( jQuery.isFunction( options ) ) {
			options = options.call( elem, i, curOffset );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );
		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend({
	offset: function( options ) {
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each(function( i ) {
					jQuery.offset.setOffset( this, options, i );
				});
		}

		var docElem, win,
			box = { top: 0, left: 0 },
			elem = this[ 0 ],
			doc = elem && elem.ownerDocument;

		if ( !doc ) {
			return;
		}

		docElem = doc.documentElement;

		// Make sure it's not a disconnected DOM node
		if ( !jQuery.contains( docElem, elem ) ) {
			return box;
		}

		// If we don't have gBCR, just use 0,0 rather than error
		// BlackBerry 5, iOS 3 (original iPhone)
		if ( typeof elem.getBoundingClientRect !== strundefined ) {
			box = elem.getBoundingClientRect();
		}
		win = getWindow( doc );
		return {
			top: box.top  + ( win.pageYOffset || docElem.scrollTop )  - ( docElem.clientTop  || 0 ),
			left: box.left + ( win.pageXOffset || docElem.scrollLeft ) - ( docElem.clientLeft || 0 )
		};
	},

	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset,
			parentOffset = { top: 0, left: 0 },
			elem = this[ 0 ];

		// fixed elements are offset from window (parentOffset = {top:0, left: 0}, because it is its only offset parent
		if ( jQuery.css( elem, "position" ) === "fixed" ) {
			// we assume that getBoundingClientRect is available when computed position is fixed
			offset = elem.getBoundingClientRect();
		} else {
			// Get *real* offsetParent
			offsetParent = this.offsetParent();

			// Get correct offsets
			offset = this.offset();
			if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
				parentOffset = offsetParent.offset();
			}

			// Add offsetParent borders
			parentOffset.top  += jQuery.css( offsetParent[ 0 ], "borderTopWidth", true );
			parentOffset.left += jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true );
		}

		// Subtract parent offsets and element margins
		// note: when an element has margin: auto the offsetLeft and marginLeft
		// are the same in Safari causing offset.left to incorrectly be 0
		return {
			top:  offset.top  - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true)
		};
	},

	offsetParent: function() {
		return this.map(function() {
			var offsetParent = this.offsetParent || docElem;

			while ( offsetParent && ( !jQuery.nodeName( offsetParent, "html" ) && jQuery.css( offsetParent, "position" ) === "static" ) ) {
				offsetParent = offsetParent.offsetParent;
			}
			return offsetParent || docElem;
		});
	}
});

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = /Y/.test( prop );

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {
			var win = getWindow( elem );

			if ( val === undefined ) {
				return win ? (prop in win) ? win[ prop ] :
					win.document.documentElement[ method ] :
					elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : jQuery( win ).scrollLeft(),
					top ? val : jQuery( win ).scrollTop()
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length, null );
	};
});

// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// getComputedStyle returns percent when specified for top/left/bottom/right
// rather than make the css module depend on the offset module, we just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );
				// if curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
});


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name }, function( defaultExtra, funcName ) {
		// margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( jQuery.isWindow( elem ) ) {
					// As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
					// isn't a whole lot we can do. See pull request at this URL for discussion:
					// https://github.com/jquery/jquery/pull/764
					return elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height], whichever is greatest
					// unfortunately, this causes bug #3838 in IE6/8 only, but there is currently no good, small way to fix it.
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?
					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable, null );
		};
	});
});


// The number of elements contained in the matched element set
jQuery.fn.size = function() {
	return this.length;
};

jQuery.fn.andSelf = jQuery.fn.addBack;




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.
if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	});
}




var
	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in
// AMD (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( typeof noGlobal === strundefined ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;

}));
(function($, undefined) {

/**
 * Unobtrusive scripting adapter for jQuery
 * https://github.com/rails/jquery-ujs
 *
 * Requires jQuery 1.7.0 or later.
 *
 * Released under the MIT license
 *
 */

  // Cut down on the number of issues from people inadvertently including jquery_ujs twice
  // by detecting and raising an error when it happens.
  if ( $.rails !== undefined ) {
    $.error('jquery-ujs has already been loaded!');
  }

  // Shorthand to make it a little easier to call public rails functions from within rails.js
  var rails;
  var $document = $(document);

  $.rails = rails = {
    // Link elements bound by jquery-ujs
    linkClickSelector: 'a[data-confirm], a[data-method], a[data-remote], a[data-disable-with]',

    // Button elements bound by jquery-ujs
    buttonClickSelector: 'button[data-remote]',

    // Select elements bound by jquery-ujs
    inputChangeSelector: 'select[data-remote], input[data-remote], textarea[data-remote]',

    // Form elements bound by jquery-ujs
    formSubmitSelector: 'form',

    // Form input elements bound by jquery-ujs
    formInputClickSelector: 'form input[type=submit], form input[type=image], form button[type=submit], form button:not([type])',

    // Form input elements disabled during form submission
    disableSelector: 'input[data-disable-with], button[data-disable-with], textarea[data-disable-with]',

    // Form input elements re-enabled after form submission
    enableSelector: 'input[data-disable-with]:disabled, button[data-disable-with]:disabled, textarea[data-disable-with]:disabled',

    // Form required input elements
    requiredInputSelector: 'input[name][required]:not([disabled]),textarea[name][required]:not([disabled])',

    // Form file input elements
    fileInputSelector: 'input[type=file]',

    // Link onClick disable selector with possible reenable after remote submission
    linkDisableSelector: 'a[data-disable-with]',

    // Make sure that every Ajax request sends the CSRF token
    CSRFProtection: function(xhr) {
      var token = $('meta[name="csrf-token"]').attr('content');
      if (token) xhr.setRequestHeader('X-CSRF-Token', token);
    },

    // making sure that all forms have actual up-to-date token(cached forms contain old one)
    refreshCSRFTokens: function(){
      var csrfToken = $('meta[name=csrf-token]').attr('content');
      var csrfParam = $('meta[name=csrf-param]').attr('content');
      $('form input[name="' + csrfParam + '"]').val(csrfToken);
    },

    // Triggers an event on an element and returns false if the event result is false
    fire: function(obj, name, data) {
      var event = $.Event(name);
      obj.trigger(event, data);
      return event.result !== false;
    },

    // Default confirm dialog, may be overridden with custom confirm dialog in $.rails.confirm
    confirm: function(message) {
      return confirm(message);
    },

    // Default ajax function, may be overridden with custom function in $.rails.ajax
    ajax: function(options) {
      return $.ajax(options);
    },

    // Default way to get an element's href. May be overridden at $.rails.href.
    href: function(element) {
      return element.attr('href');
    },

    // Submits "remote" forms and links with ajax
    handleRemote: function(element) {
      var method, url, data, elCrossDomain, crossDomain, withCredentials, dataType, options;

      if (rails.fire(element, 'ajax:before')) {
        elCrossDomain = element.data('cross-domain');
        crossDomain = elCrossDomain === undefined ? null : elCrossDomain;
        withCredentials = element.data('with-credentials') || null;
        dataType = element.data('type') || ($.ajaxSettings && $.ajaxSettings.dataType);

        if (element.is('form')) {
          method = element.attr('method');
          url = element.attr('action');
          data = element.serializeArray();
          // memoized value from clicked submit button
          var button = element.data('ujs:submit-button');
          if (button) {
            data.push(button);
            element.data('ujs:submit-button', null);
          }
        } else if (element.is(rails.inputChangeSelector)) {
          method = element.data('method');
          url = element.data('url');
          data = element.serialize();
          if (element.data('params')) data = data + "&" + element.data('params');
        } else if (element.is(rails.buttonClickSelector)) {
          method = element.data('method') || 'get';
          url = element.data('url');
          data = element.serialize();
          if (element.data('params')) data = data + "&" + element.data('params');
        } else {
          method = element.data('method');
          url = rails.href(element);
          data = element.data('params') || null;
        }

        options = {
          type: method || 'GET', data: data, dataType: dataType,
          // stopping the "ajax:beforeSend" event will cancel the ajax request
          beforeSend: function(xhr, settings) {
            if (settings.dataType === undefined) {
              xhr.setRequestHeader('accept', '*/*;q=0.5, ' + settings.accepts.script);
            }
            return rails.fire(element, 'ajax:beforeSend', [xhr, settings]);
          },
          success: function(data, status, xhr) {
            element.trigger('ajax:success', [data, status, xhr]);
          },
          complete: function(xhr, status) {
            element.trigger('ajax:complete', [xhr, status]);
          },
          error: function(xhr, status, error) {
            element.trigger('ajax:error', [xhr, status, error]);
          },
          crossDomain: crossDomain
        };

        // There is no withCredentials for IE6-8 when
        // "Enable native XMLHTTP support" is disabled
        if (withCredentials) {
          options.xhrFields = {
            withCredentials: withCredentials
          };
        }

        // Only pass url to `ajax` options if not blank
        if (url) { options.url = url; }

        var jqxhr = rails.ajax(options);
        element.trigger('ajax:send', jqxhr);
        return jqxhr;
      } else {
        return false;
      }
    },

    // Handles "data-method" on links such as:
    // <a href="/users/5" data-method="delete" rel="nofollow" data-confirm="Are you sure?">Delete</a>
    handleMethod: function(link) {
      var href = rails.href(link),
        method = link.data('method'),
        target = link.attr('target'),
        csrfToken = $('meta[name=csrf-token]').attr('content'),
        csrfParam = $('meta[name=csrf-param]').attr('content'),
        form = $('<form method="post" action="' + href + '"></form>'),
        metadataInput = '<input name="_method" value="' + method + '" type="hidden" />';

      if (csrfParam !== undefined && csrfToken !== undefined) {
        metadataInput += '<input name="' + csrfParam + '" value="' + csrfToken + '" type="hidden" />';
      }

      if (target) { form.attr('target', target); }

      form.hide().append(metadataInput).appendTo('body');
      form.submit();
    },

    /* Disables form elements:
      - Caches element value in 'ujs:enable-with' data store
      - Replaces element text with value of 'data-disable-with' attribute
      - Sets disabled property to true
    */
    disableFormElements: function(form) {
      form.find(rails.disableSelector).each(function() {
        var element = $(this), method = element.is('button') ? 'html' : 'val';
        element.data('ujs:enable-with', element[method]());
        element[method](element.data('disable-with'));
        element.prop('disabled', true);
      });
    },

    /* Re-enables disabled form elements:
      - Replaces element text with cached value from 'ujs:enable-with' data store (created in `disableFormElements`)
      - Sets disabled property to false
    */
    enableFormElements: function(form) {
      form.find(rails.enableSelector).each(function() {
        var element = $(this), method = element.is('button') ? 'html' : 'val';
        if (element.data('ujs:enable-with')) element[method](element.data('ujs:enable-with'));
        element.prop('disabled', false);
      });
    },

   /* For 'data-confirm' attribute:
      - Fires `confirm` event
      - Shows the confirmation dialog
      - Fires the `confirm:complete` event

      Returns `true` if no function stops the chain and user chose yes; `false` otherwise.
      Attaching a handler to the element's `confirm` event that returns a `falsy` value cancels the confirmation dialog.
      Attaching a handler to the element's `confirm:complete` event that returns a `falsy` value makes this function
      return false. The `confirm:complete` event is fired whether or not the user answered true or false to the dialog.
   */
    allowAction: function(element) {
      var message = element.data('confirm'),
          answer = false, callback;
      if (!message) { return true; }

      if (rails.fire(element, 'confirm')) {
        answer = rails.confirm(message);
        callback = rails.fire(element, 'confirm:complete', [answer]);
      }
      return answer && callback;
    },

    // Helper function which checks for blank inputs in a form that match the specified CSS selector
    blankInputs: function(form, specifiedSelector, nonBlank) {
      var inputs = $(), input, valueToCheck,
          selector = specifiedSelector || 'input,textarea',
          allInputs = form.find(selector);

      allInputs.each(function() {
        input = $(this);
        valueToCheck = input.is('input[type=checkbox],input[type=radio]') ? input.is(':checked') : input.val();
        // If nonBlank and valueToCheck are both truthy, or nonBlank and valueToCheck are both falsey
        if (!valueToCheck === !nonBlank) {

          // Don't count unchecked required radio if other radio with same name is checked
          if (input.is('input[type=radio]') && allInputs.filter('input[type=radio]:checked[name="' + input.attr('name') + '"]').length) {
            return true; // Skip to next input
          }

          inputs = inputs.add(input);
        }
      });
      return inputs.length ? inputs : false;
    },

    // Helper function which checks for non-blank inputs in a form that match the specified CSS selector
    nonBlankInputs: function(form, specifiedSelector) {
      return rails.blankInputs(form, specifiedSelector, true); // true specifies nonBlank
    },

    // Helper function, needed to provide consistent behavior in IE
    stopEverything: function(e) {
      $(e.target).trigger('ujs:everythingStopped');
      e.stopImmediatePropagation();
      return false;
    },

    //  replace element's html with the 'data-disable-with' after storing original html
    //  and prevent clicking on it
    disableElement: function(element) {
      element.data('ujs:enable-with', element.html()); // store enabled state
      element.html(element.data('disable-with')); // set to disabled state
      element.bind('click.railsDisable', function(e) { // prevent further clicking
        return rails.stopEverything(e);
      });
    },

    // restore element to its original state which was disabled by 'disableElement' above
    enableElement: function(element) {
      if (element.data('ujs:enable-with') !== undefined) {
        element.html(element.data('ujs:enable-with')); // set to old enabled state
        element.removeData('ujs:enable-with'); // clean up cache
      }
      element.unbind('click.railsDisable'); // enable element
    }

  };

  if (rails.fire($document, 'rails:attachBindings')) {

    $.ajaxPrefilter(function(options, originalOptions, xhr){ if ( !options.crossDomain ) { rails.CSRFProtection(xhr); }});

    $document.delegate(rails.linkDisableSelector, 'ajax:complete', function() {
        rails.enableElement($(this));
    });

    $document.delegate(rails.linkClickSelector, 'click.rails', function(e) {
      var link = $(this), method = link.data('method'), data = link.data('params'), metaClick = e.metaKey || e.ctrlKey;
      if (!rails.allowAction(link)) return rails.stopEverything(e);

      if (!metaClick && link.is(rails.linkDisableSelector)) rails.disableElement(link);

      if (link.data('remote') !== undefined) {
        if (metaClick && (!method || method === 'GET') && !data) { return true; }

        var handleRemote = rails.handleRemote(link);
        // response from rails.handleRemote() will either be false or a deferred object promise.
        if (handleRemote === false) {
          rails.enableElement(link);
        } else {
          handleRemote.error( function() { rails.enableElement(link); } );
        }
        return false;

      } else if (link.data('method')) {
        rails.handleMethod(link);
        return false;
      }
    });

    $document.delegate(rails.buttonClickSelector, 'click.rails', function(e) {
      var button = $(this);
      if (!rails.allowAction(button)) return rails.stopEverything(e);

      rails.handleRemote(button);
      return false;
    });

    $document.delegate(rails.inputChangeSelector, 'change.rails', function(e) {
      var link = $(this);
      if (!rails.allowAction(link)) return rails.stopEverything(e);

      rails.handleRemote(link);
      return false;
    });

    $document.delegate(rails.formSubmitSelector, 'submit.rails', function(e) {
      var form = $(this),
        remote = form.data('remote') !== undefined,
        blankRequiredInputs = rails.blankInputs(form, rails.requiredInputSelector),
        nonBlankFileInputs = rails.nonBlankInputs(form, rails.fileInputSelector);

      if (!rails.allowAction(form)) return rails.stopEverything(e);

      // skip other logic when required values are missing or file upload is present
      if (blankRequiredInputs && form.attr("novalidate") == undefined && rails.fire(form, 'ajax:aborted:required', [blankRequiredInputs])) {
        return rails.stopEverything(e);
      }

      if (remote) {
        if (nonBlankFileInputs) {
          // slight timeout so that the submit button gets properly serialized
          // (make it easy for event handler to serialize form without disabled values)
          setTimeout(function(){ rails.disableFormElements(form); }, 13);
          var aborted = rails.fire(form, 'ajax:aborted:file', [nonBlankFileInputs]);

          // re-enable form elements if event bindings return false (canceling normal form submission)
          if (!aborted) { setTimeout(function(){ rails.enableFormElements(form); }, 13); }

          return aborted;
        }

        rails.handleRemote(form);
        return false;

      } else {
        // slight timeout so that the submit button gets properly serialized
        setTimeout(function(){ rails.disableFormElements(form); }, 13);
      }
    });

    $document.delegate(rails.formInputClickSelector, 'click.rails', function(event) {
      var button = $(this);

      if (!rails.allowAction(button)) return rails.stopEverything(event);

      // register the pressed submit button
      var name = button.attr('name'),
        data = name ? {name:name, value:button.val()} : null;

      button.closest('form').data('ujs:submit-button', data);
    });

    $document.delegate(rails.formSubmitSelector, 'ajax:beforeSend.rails', function(event) {
      if (this == event.target) rails.disableFormElements($(this));
    });

    $document.delegate(rails.formSubmitSelector, 'ajax:complete.rails', function(event) {
      if (this == event.target) rails.enableFormElements($(this));
    });

    $(function(){
      rails.refreshCSRFTokens();
    });
  }

})( jQuery );
/* ========================================================================
 * Bootstrap: affix.js v3.1.1
 * http://getbootstrap.com/javascript/#affix
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */



+function ($) {
  'use strict';

  // AFFIX CLASS DEFINITION
  // ======================

  var Affix = function (element, options) {
    this.options = $.extend({}, Affix.DEFAULTS, options)
    this.$window = $(window)
      .on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.bs.affix.data-api',  $.proxy(this.checkPositionWithEventLoop, this))

    this.$element     = $(element)
    this.affixed      =
    this.unpin        =
    this.pinnedOffset = null

    this.checkPosition()
  }

  Affix.RESET = 'affix affix-top affix-bottom'

  Affix.DEFAULTS = {
    offset: 0
  }

  Affix.prototype.getPinnedOffset = function () {
    if (this.pinnedOffset) return this.pinnedOffset
    this.$element.removeClass(Affix.RESET).addClass('affix')
    var scrollTop = this.$window.scrollTop()
    var position  = this.$element.offset()
    return (this.pinnedOffset = position.top - scrollTop)
  }

  Affix.prototype.checkPositionWithEventLoop = function () {
    setTimeout($.proxy(this.checkPosition, this), 1)
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var scrollHeight = $(document).height()
    var scrollTop    = this.$window.scrollTop()
    var position     = this.$element.offset()
    var offset       = this.options.offset
    var offsetTop    = offset.top
    var offsetBottom = offset.bottom

    if (typeof offset != 'object')         offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function')    offsetTop    = offset.top(this.$element)
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)

    var affix = this.unpin   != null && (scrollTop + this.unpin <= position.top) ? false :
                offsetBottom != null && (position.top + this.$element.height() >= scrollHeight - offsetBottom) ? 'bottom' :
                offsetTop    != null && (scrollTop <= offsetTop) ? 'top' : false

    if (this.affixed === affix) return
    if (this.unpin != null) this.$element.css('top', '')

    var affixType = 'affix' + (affix ? '-' + affix : '')
    var e         = $.Event(affixType + '.bs.affix')

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    this.affixed = affix
    this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null

    this.$element
      .removeClass(Affix.RESET)
      .addClass(affixType)
      .trigger($.Event(affixType.replace('affix', 'affixed')))

    if (affix == 'bottom') {
      this.$element.offset({ top: position.top })
    }
  }


  // AFFIX PLUGIN DEFINITION
  // =======================

  var old = $.fn.affix

  $.fn.affix = function (option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.affix')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.affix.Constructor = Affix


  // AFFIX NO CONFLICT
  // =================

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


  // AFFIX DATA-API
  // ==============

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
      var data = $spy.data()

      data.offset = data.offset || {}

      if (data.offsetBottom) data.offset.bottom = data.offsetBottom
      if (data.offsetTop)    data.offset.top    = data.offsetTop

      $spy.affix(data)
    })
  })

}(jQuery);
/* ========================================================================
 * Bootstrap: alert.js v3.1.1
 * http://getbootstrap.com/javascript/#alerts
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */



+function ($) {
  'use strict';

  // ALERT CLASS DEFINITION
  // ======================

  var dismiss = '[data-dismiss="alert"]'
  var Alert   = function (el) {
    $(el).on('click', dismiss, this.close)
  }

  Alert.prototype.close = function (e) {
    var $this    = $(this)
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = $(selector)

    if (e) e.preventDefault()

    if (!$parent.length) {
      $parent = $this.hasClass('alert') ? $this : $this.parent()
    }

    $parent.trigger(e = $.Event('close.bs.alert'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      $parent.trigger('closed.bs.alert').remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent
        .one($.support.transition.end, removeElement)
        .emulateTransitionEnd(150) :
      removeElement()
  }


  // ALERT PLUGIN DEFINITION
  // =======================

  var old = $.fn.alert

  $.fn.alert = function (option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.alert')

      if (!data) $this.data('bs.alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.alert.Constructor = Alert


  // ALERT NO CONFLICT
  // =================

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


  // ALERT DATA-API
  // ==============

  $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)

}(jQuery);
/* ========================================================================
 * Bootstrap: button.js v3.1.1
 * http://getbootstrap.com/javascript/#buttons
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */



+function ($) {
  'use strict';

  // BUTTON PUBLIC CLASS DEFINITION
  // ==============================

  var Button = function (element, options) {
    this.$element  = $(element)
    this.options   = $.extend({}, Button.DEFAULTS, options)
    this.isLoading = false
  }

  Button.DEFAULTS = {
    loadingText: 'loading...'
  }

  Button.prototype.setState = function (state) {
    var d    = 'disabled'
    var $el  = this.$element
    var val  = $el.is('input') ? 'val' : 'html'
    var data = $el.data()

    state = state + 'Text'

    if (!data.resetText) $el.data('resetText', $el[val]())

    $el[val](data[state] || this.options[state])

    // push to event loop to allow forms to submit
    setTimeout($.proxy(function () {
      if (state == 'loadingText') {
        this.isLoading = true
        $el.addClass(d).attr(d, d)
      } else if (this.isLoading) {
        this.isLoading = false
        $el.removeClass(d).removeAttr(d)
      }
    }, this), 0)
  }

  Button.prototype.toggle = function () {
    var changed = true
    var $parent = this.$element.closest('[data-toggle="buttons"]')

    if ($parent.length) {
      var $input = this.$element.find('input')
      if ($input.prop('type') == 'radio') {
        if ($input.prop('checked') && this.$element.hasClass('active')) changed = false
        else $parent.find('.active').removeClass('active')
      }
      if (changed) $input.prop('checked', !this.$element.hasClass('active')).trigger('change')
    }

    if (changed) this.$element.toggleClass('active')
  }


  // BUTTON PLUGIN DEFINITION
  // ========================

  var old = $.fn.button

  $.fn.button = function (option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.button')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.button', (data = new Button(this, options)))

      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  $.fn.button.Constructor = Button


  // BUTTON NO CONFLICT
  // ==================

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


  // BUTTON DATA-API
  // ===============

  $(document).on('click.bs.button.data-api', '[data-toggle^=button]', function (e) {
    var $btn = $(e.target)
    if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
    $btn.button('toggle')
    e.preventDefault()
  })

}(jQuery);
/* ========================================================================
 * Bootstrap: carousel.js v3.1.1
 * http://getbootstrap.com/javascript/#carousel
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */



+function ($) {
  'use strict';

  // CAROUSEL CLASS DEFINITION
  // =========================

  var Carousel = function (element, options) {
    this.$element    = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options     = options
    this.paused      =
    this.sliding     =
    this.interval    =
    this.$active     =
    this.$items      = null

    this.options.pause == 'hover' && this.$element
      .on('mouseenter', $.proxy(this.pause, this))
      .on('mouseleave', $.proxy(this.cycle, this))
  }

  Carousel.DEFAULTS = {
    interval: 5000,
    pause: 'hover',
    wrap: true
  }

  Carousel.prototype.cycle =  function (e) {
    e || (this.paused = false)

    this.interval && clearInterval(this.interval)

    this.options.interval
      && !this.paused
      && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))

    return this
  }

  Carousel.prototype.getActiveIndex = function () {
    this.$active = this.$element.find('.item.active')
    this.$items  = this.$active.parent().children('.item')

    return this.$items.index(this.$active)
  }

  Carousel.prototype.to = function (pos) {
    var that        = this
    var activeIndex = this.getActiveIndex()

    if (pos > (this.$items.length - 1) || pos < 0) return

    if (this.sliding)       return this.$element.one('slid.bs.carousel', function () { that.to(pos) }) // yes, "slid". not a typo. past tense of "to slide".
    if (activeIndex == pos) return this.pause().cycle()

    return this.slide(pos > activeIndex ? 'next' : 'prev', $(this.$items[pos]))
  }

  Carousel.prototype.pause = function (e) {
    e || (this.paused = true)

    if (this.$element.find('.next, .prev').length && $.support.transition) {
      this.$element.trigger($.support.transition.end)
      this.cycle(true)
    }

    this.interval = clearInterval(this.interval)

    return this
  }

  Carousel.prototype.next = function () {
    if (this.sliding) return
    return this.slide('next')
  }

  Carousel.prototype.prev = function () {
    if (this.sliding) return
    return this.slide('prev')
  }

  Carousel.prototype.slide = function (type, next) {
    var $active   = this.$element.find('.item.active')
    var $next     = next || $active[type]()
    var isCycling = this.interval
    var direction = type == 'next' ? 'left' : 'right'
    var fallback  = type == 'next' ? 'first' : 'last'
    var that      = this

    if (!$next.length) {
      if (!this.options.wrap) return
      $next = this.$element.find('.item')[fallback]()
    }

    if ($next.hasClass('active')) return this.sliding = false

    var e = $.Event('slide.bs.carousel', { relatedTarget: $next[0], direction: direction })
    this.$element.trigger(e)
    if (e.isDefaultPrevented()) return

    this.sliding = true

    isCycling && this.pause()

    if (this.$indicators.length) {
      this.$indicators.find('.active').removeClass('active')
      this.$element.one('slid.bs.carousel', function () { // yes, "slid". not a typo. past tense of "to slide".
        var $nextIndicator = $(that.$indicators.children()[that.getActiveIndex()])
        $nextIndicator && $nextIndicator.addClass('active')
      })
    }

    if ($.support.transition && this.$element.hasClass('slide')) {
      $next.addClass(type)
      $next[0].offsetWidth // force reflow
      $active.addClass(direction)
      $next.addClass(direction)
      $active
        .one($.support.transition.end, function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () { that.$element.trigger('slid.bs.carousel') }, 0) // yes, "slid". not a typo. past tense of "to slide".
        })
        .emulateTransitionEnd($active.css('transition-duration').slice(0, -1) * 1000)
    } else {
      $active.removeClass('active')
      $next.addClass('active')
      this.sliding = false
      this.$element.trigger('slid.bs.carousel') // yes, "slid". not a typo. past tense of "to slide".
    }

    isCycling && this.cycle()

    return this
  }


  // CAROUSEL PLUGIN DEFINITION
  // ==========================

  var old = $.fn.carousel

  $.fn.carousel = function (option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.carousel')
      var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
      var action  = typeof option == 'string' ? option : options.slide

      if (!data) $this.data('bs.carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  $.fn.carousel.Constructor = Carousel


  // CAROUSEL NO CONFLICT
  // ====================

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }


  // CAROUSEL DATA-API
  // =================

  $(document).on('click.bs.carousel.data-api', '[data-slide], [data-slide-to]', function (e) {
    var $this   = $(this), href
    var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
    var options = $.extend({}, $target.data(), $this.data())
    var slideIndex = $this.attr('data-slide-to')
    if (slideIndex) options.interval = false

    $target.carousel(options)

    if (slideIndex = $this.attr('data-slide-to')) {
      $target.data('bs.carousel').to(slideIndex)
    }

    e.preventDefault()
  })

  $(window).on('load', function () {
    $('[data-ride="carousel"]').each(function () {
      var $carousel = $(this)
      $carousel.carousel($carousel.data())
    })
  })

}(jQuery);
/* ========================================================================
 * Bootstrap: collapse.js v3.1.1
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */



+function ($) {
  'use strict';

  // COLLAPSE PUBLIC CLASS DEFINITION
  // ================================

  var Collapse = function (element, options) {
    this.$element      = $(element)
    this.options       = $.extend({}, Collapse.DEFAULTS, options)
    this.transitioning = null

    if (this.options.parent) this.$parent = $(this.options.parent)
    if (this.options.toggle) this.toggle()
  }

  Collapse.DEFAULTS = {
    toggle: true
  }

  Collapse.prototype.dimension = function () {
    var hasWidth = this.$element.hasClass('width')
    return hasWidth ? 'width' : 'height'
  }

  Collapse.prototype.show = function () {
    if (this.transitioning || this.$element.hasClass('in')) return

    var startEvent = $.Event('show.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var actives = this.$parent && this.$parent.find('> .panel > .in')

    if (actives && actives.length) {
      var hasData = actives.data('bs.collapse')
      if (hasData && hasData.transitioning) return
      actives.collapse('hide')
      hasData || actives.data('bs.collapse', null)
    }

    var dimension = this.dimension()

    this.$element
      .removeClass('collapse')
      .addClass('collapsing')[dimension](0)

    this.transitioning = 1

    var complete = function (e) {
      if (e && e.target != this.$element[0]) {
        this.$element
          .one($.support.transition.end, $.proxy(complete, this))
        return
      }
      this.$element
        .removeClass('collapsing')
        .addClass('collapse in')[dimension]('auto')
      this.transitioning = 0
      this.$element.trigger('shown.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    var scrollSize = $.camelCase(['scroll', dimension].join('-'))

    this.$element
      .one($.support.transition.end, $.proxy(complete, this))
      .emulateTransitionEnd(350)[dimension](this.$element[0][scrollSize])
  }

  Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element[dimension](this.$element[dimension]())[0].offsetHeight

    this.$element
      .addClass('collapsing')
      .removeClass('collapse')
      .removeClass('in')

    this.transitioning = 1

    var complete = function (e) {
      if (e && e.target != this.$element[0]) {
        this.$element
          .one($.support.transition.end, $.proxy(complete, this))
        return
      }
      this.transitioning = 0
      this.$element
        .trigger('hidden.bs.collapse')
        .removeClass('collapsing')
        .addClass('collapse')
    }

    if (!$.support.transition) return complete.call(this)

    this.$element
      [dimension](0)
      .one($.support.transition.end, $.proxy(complete, this))
      .emulateTransitionEnd(350)
  }

  Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
  }


  // COLLAPSE PLUGIN DEFINITION
  // ==========================

  var old = $.fn.collapse

  $.fn.collapse = function (option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.collapse')
      var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data && options.toggle && option == 'show') option = !option
      if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.collapse.Constructor = Collapse


  // COLLAPSE NO CONFLICT
  // ====================

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


  // COLLAPSE DATA-API
  // =================

  $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function (e) {
    var $this   = $(this), href
    var target  = $this.attr('data-target')
        || e.preventDefault()
        || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') //strip for ie7
    var $target = $(target)
    var data    = $target.data('bs.collapse')
    var option  = data ? 'toggle' : $this.data()
    var parent  = $this.attr('data-parent')
    var $parent = parent && $(parent)

    if (!data || !data.transitioning) {
      if ($parent) $parent.find('[data-toggle="collapse"][data-parent="' + parent + '"]').not($this).addClass('collapsed')
      $this[$target.hasClass('in') ? 'addClass' : 'removeClass']('collapsed')
    }

    $target.collapse(option)
  })

}(jQuery);
/* ========================================================================
 * Bootstrap: dropdown.js v3.1.1
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */



+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle   = '[data-toggle="dropdown"]'
  var Dropdown = function (element) {
    $(element).on('click.bs.dropdown', this.toggle)
  }

  Dropdown.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    clearMenus()

    if (!isActive) {
      if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $('<div class="dropdown-backdrop"/>').insertAfter($(this)).on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.trigger('focus')

      $parent
        .toggleClass('open')
        .trigger('shown.bs.dropdown', relatedTarget)
    }

    return false
  }

  Dropdown.prototype.keydown = function (e) {
    if (!/(38|40|27)/.test(e.keyCode)) return

    var $this = $(this)

    e.preventDefault()
    e.stopPropagation()

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    if (!isActive || (isActive && e.keyCode == 27)) {
      if (e.which == 27) $parent.find(toggle).trigger('focus')
      return $this.trigger('click')
    }

    var desc = ' li:not(.divider):visible a'
    var $items = $parent.find('[role="menu"]' + desc + ', [role="listbox"]' + desc)

    if (!$items.length) return

    var index = $items.index($items.filter(':focus'))

    if (e.keyCode == 38 && index > 0)                 index--                        // up
    if (e.keyCode == 40 && index < $items.length - 1) index++                        // down
    if (!~index)                                      index = 0

    $items.eq(index).trigger('focus')
  }

  function clearMenus(e) {
    $(backdrop).remove()
    $(toggle).each(function () {
      var $parent = getParent($(this))
      var relatedTarget = { relatedTarget: this }
      if (!$parent.hasClass('open')) return
      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))
      if (e.isDefaultPrevented()) return
      $parent.removeClass('open').trigger('hidden.bs.dropdown', relatedTarget)
    })
  }

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  var old = $.fn.dropdown

  $.fn.dropdown = function (option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.dropdown')

      if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.dropdown.Constructor = Dropdown


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  $(document)
    .on('click.bs.dropdown.data-api', clearMenus)
    .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
    .on('keydown.bs.dropdown.data-api', toggle + ', [role="menu"], [role="listbox"]', Dropdown.prototype.keydown)

}(jQuery);
/* ========================================================================
 * Bootstrap: tab.js v3.1.1
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */



+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    this.element = $(element)
  }

  Tab.prototype.show = function () {
    var $this    = this.element
    var $ul      = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var previous = $ul.find('.active:last a')[0]
    var e        = $.Event('show.bs.tab', {
      relatedTarget: previous
    })

    $this.trigger(e)

    if (e.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.parent('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: previous
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active    = container.find('> .active')
    var transition = callback
      && $.support.transition
      && $active.hasClass('fade')

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
        .removeClass('active')

      element.addClass('active')

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu')) {
        element.closest('li.dropdown').addClass('active')
      }

      callback && callback()
    }

    transition ?
      $active
        .one($.support.transition.end, next)
        .emulateTransitionEnd(150) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  var old = $.fn.tab

  $.fn.tab = function ( option ) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


  // TAB DATA-API
  // ============

  $(document).on('click.bs.tab.data-api', '[data-toggle="tab"], [data-toggle="pill"]', function (e) {
    e.preventDefault()
    $(this).tab('show')
  })

}(jQuery);
/* ========================================================================
 * Bootstrap: transition.js v3.1.1
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */



+function ($) {
  'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
      WebkitTransition : 'webkitTransitionEnd',
      MozTransition    : 'transitionend',
      OTransition      : 'oTransitionEnd otransitionend',
      transition       : 'transitionend'
    }

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return { end: transEndEventNames[name] }
      }
    }

    return false // explicit for ie8 (  ._.)
  }

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
    var called = false, $el = this
    $(this).one($.support.transition.end, function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
  }

  $(function () {
    $.support.transition = transitionEnd()
  })

}(jQuery);
/* ========================================================================
 * Bootstrap: scrollspy.js v3.1.1
 * http://getbootstrap.com/javascript/#scrollspy
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */



+function ($) {
  'use strict';

  // SCROLLSPY CLASS DEFINITION
  // ==========================

  function ScrollSpy(element, options) {
    var href
    var process  = $.proxy(this.process, this)

    this.$element       = $(element).is('body') ? $(window) : $(element)
    this.$body          = $('body')
    this.$scrollElement = this.$element.on('scroll.bs.scrollspy', process)
    this.options        = $.extend({}, ScrollSpy.DEFAULTS, options)
    this.selector       = (this.options.target
      || ((href = $(element).attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
      || '') + ' .nav li > a'
    this.offsets        = $([])
    this.targets        = $([])
    this.activeTarget   = null

    this.refresh()
    this.process()
  }

  ScrollSpy.DEFAULTS = {
    offset: 10
  }

  ScrollSpy.prototype.refresh = function () {
    var offsetMethod = this.$element[0] == window ? 'offset' : 'position'

    this.offsets = $([])
    this.targets = $([])

    var self     = this

    this.$body
      .find(this.selector)
      .map(function () {
        var $el   = $(this)
        var href  = $el.data('target') || $el.attr('href')
        var $href = /^#./.test(href) && $(href)

        return ($href
          && $href.length
          && $href.is(':visible')
          && [[ $href[offsetMethod]().top + (!$.isWindow(self.$scrollElement.get(0)) && self.$scrollElement.scrollTop()), href ]]) || null
      })
      .sort(function (a, b) { return a[0] - b[0] })
      .each(function () {
        self.offsets.push(this[0])
        self.targets.push(this[1])
      })
  }

  ScrollSpy.prototype.process = function () {
    var scrollTop    = this.$scrollElement.scrollTop() + this.options.offset
    var scrollHeight = this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
    var maxScroll    = scrollHeight - this.$scrollElement.height()
    var offsets      = this.offsets
    var targets      = this.targets
    var activeTarget = this.activeTarget
    var i

    if (scrollTop >= maxScroll) {
      return activeTarget != (i = targets.last()[0]) && this.activate(i)
    }

    if (activeTarget && scrollTop <= offsets[0]) {
      return activeTarget != (i = targets[0]) && this.activate(i)
    }

    for (i = offsets.length; i--;) {
      activeTarget != targets[i]
        && scrollTop >= offsets[i]
        && (!offsets[i + 1] || scrollTop <= offsets[i + 1])
        && this.activate( targets[i] )
    }
  }

  ScrollSpy.prototype.activate = function (target) {
    this.activeTarget = target

    $(this.selector)
      .parentsUntil(this.options.target, '.active')
      .removeClass('active')

    var selector = this.selector +
        '[data-target="' + target + '"],' +
        this.selector + '[href="' + target + '"]'

    var active = $(selector)
      .parents('li')
      .addClass('active')

    if (active.parent('.dropdown-menu').length) {
      active = active
        .closest('li.dropdown')
        .addClass('active')
    }

    active.trigger('activate.bs.scrollspy')
  }


  // SCROLLSPY PLUGIN DEFINITION
  // ===========================

  var old = $.fn.scrollspy

  $.fn.scrollspy = function (option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.scrollspy')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.scrollspy.Constructor = ScrollSpy


  // SCROLLSPY NO CONFLICT
  // =====================

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


  // SCROLLSPY DATA-API
  // ==================

  $(window).on('load.bs.scrollspy.data-api', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      $spy.scrollspy($spy.data())
    })
  })

}(jQuery);
/* ========================================================================
 * Bootstrap: modal.js v3.1.1
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */



+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options        = options
    this.$body          = $(document.body)
    this.$element       = $(element)
    this.$backdrop      =
    this.isShown        = null
    this.scrollbarWidth = 0

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.$body.addClass('modal-open')

    this.setScrollbar()
    this.escape()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element
        .addClass('in')
        .attr('aria-hidden', false)

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$element.find('.modal-dialog') // wait for modal to slide in
          .one($.support.transition.end, function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(300) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.$body.removeClass('modal-open')

    this.resetScrollbar()
    this.escape()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .attr('aria-hidden', true)
      .off('click.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one($.support.transition.end, $.proxy(this.hideModal, this))
        .emulateTransitionEnd(300) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keyup.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keyup.dismiss.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.removeBackdrop()
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus.call(this.$element[0])
          : this.hide.call(this)
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one($.support.transition.end, callback)
          .emulateTransitionEnd(150) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one($.support.transition.end, callback)
          .emulateTransitionEnd(150) :
        callback()

    } else if (callback) {
      callback()
    }
  }

  Modal.prototype.checkScrollbar = function () {
    if (document.body.clientWidth >= window.innerWidth) return
    this.scrollbarWidth = this.scrollbarWidth || this.measureScrollbar()
  }

  Modal.prototype.setScrollbar =  function () {
    var bodyPad = parseInt(this.$body.css('padding-right') || 0)
    if (this.scrollbarWidth) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', '')
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  var old = $.fn.modal

  $.fn.modal = function (option, _relatedTarget) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this   = $(this)
    var href    = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) //strip for ie7
    var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

    if ($this.is('a')) e.preventDefault()

    $target
      .modal(option, this)
      .one('hide', function () {
        $this.is(':visible') && $this.trigger('focus')
      })
  })

}(jQuery);
/* ========================================================================
 * Bootstrap: tooltip.js v3.1.1
 * http://getbootstrap.com/javascript/#tooltip
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */



+function ($) {
  'use strict';

  // TOOLTIP PUBLIC CLASS DEFINITION
  // ===============================

  var Tooltip = function (element, options) {
    this.type       =
    this.options    =
    this.enabled    =
    this.timeout    =
    this.hoverState =
    this.$element   = null

    this.init('tooltip', element, options)
  }

  Tooltip.DEFAULTS = {
    animation: true,
    placement: 'top',
    selector: false,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    container: false,
    viewport: {
      selector: 'body',
      padding: 0
    }
  }

  Tooltip.prototype.init = function (type, element, options) {
    this.enabled   = true
    this.type      = type
    this.$element  = $(element)
    this.options   = this.getOptions(options)
    this.$viewport = this.options.viewport && $(this.options.viewport.selector || this.options.viewport)

    var triggers = this.options.trigger.split(' ')

    for (var i = triggers.length; i--;) {
      var trigger = triggers[i]

      if (trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (trigger != 'manual') {
        var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin'
        var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

        this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }
    }

    this.options.selector ?
      (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
      this.fixTitle()
  }

  Tooltip.prototype.getDefaults = function () {
    return Tooltip.DEFAULTS
  }

  Tooltip.prototype.getOptions = function (options) {
    options = $.extend({}, this.getDefaults(), this.$element.data(), options)

    if (options.delay && typeof options.delay == 'number') {
      options.delay = {
        show: options.delay,
        hide: options.delay
      }
    }

    return options
  }

  Tooltip.prototype.getDelegateOptions = function () {
    var options  = {}
    var defaults = this.getDefaults()

    this._options && $.each(this._options, function (key, value) {
      if (defaults[key] != value) options[key] = value
    })

    return options
  }

  Tooltip.prototype.enter = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)

    clearTimeout(self.timeout)

    self.hoverState = 'in'

    if (!self.options.delay || !self.options.delay.show) return self.show()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'in') self.show()
    }, self.options.delay.show)
  }

  Tooltip.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)

    clearTimeout(self.timeout)

    self.hoverState = 'out'

    if (!self.options.delay || !self.options.delay.hide) return self.hide()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'out') self.hide()
    }, self.options.delay.hide)
  }

  Tooltip.prototype.show = function () {
    var e = $.Event('show.bs.' + this.type)

    if (this.hasContent() && this.enabled) {
      this.$element.trigger(e)

      if (e.isDefaultPrevented()) return
      var that = this;

      var $tip = this.tip()

      this.setContent()

      if (this.options.animation) $tip.addClass('fade')

      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip
        .detach()
        .css({ top: 0, left: 0, display: 'block' })
        .addClass(placement)

      this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)

      var pos          = this.getPosition()
      var actualWidth  = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var $parent      = this.$element.parent()
        var parentDim    = this.getPosition($parent)

        placement = placement == 'bottom' && pos.top   + pos.height       + actualHeight - parentDim.scroll > parentDim.height ? 'top'    :
                    placement == 'top'    && pos.top   - parentDim.scroll - actualHeight < 0                                   ? 'bottom' :
                    placement == 'right'  && pos.right + actualWidth      > parentDim.width                                    ? 'left'   :
                    placement == 'left'   && pos.left  - actualWidth      < parentDim.left                                     ? 'right'  :
                    placement

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)

      this.applyPlacement(calculatedOffset, placement)
      this.hoverState = null

      var complete = function() {
        that.$element.trigger('shown.bs.' + that.type)
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        $tip
          .one($.support.transition.end, complete)
          .emulateTransitionEnd(150) :
        complete()
    }
  }

  Tooltip.prototype.applyPlacement = function (offset, placement) {
    var $tip   = this.tip()
    var width  = $tip[0].offsetWidth
    var height = $tip[0].offsetHeight

    // manually read margins because getBoundingClientRect includes difference
    var marginTop = parseInt($tip.css('margin-top'), 10)
    var marginLeft = parseInt($tip.css('margin-left'), 10)

    // we must check for NaN for ie 8/9
    if (isNaN(marginTop))  marginTop  = 0
    if (isNaN(marginLeft)) marginLeft = 0

    offset.top  = offset.top  + marginTop
    offset.left = offset.left + marginLeft

    // $.fn.offset doesn't round pixel values
    // so we use setOffset directly with our own function B-0
    $.offset.setOffset($tip[0], $.extend({
      using: function (props) {
        $tip.css({
          top: Math.round(props.top),
          left: Math.round(props.left)
        })
      }
    }, offset), 0)

    $tip.addClass('in')

    // check to see if placing tip in new offset caused the tip to resize itself
    var actualWidth  = $tip[0].offsetWidth
    var actualHeight = $tip[0].offsetHeight

    if (placement == 'top' && actualHeight != height) {
      offset.top = offset.top + height - actualHeight
    }

    var delta = this.getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight)

    if (delta.left) offset.left += delta.left
    else offset.top += delta.top

    var arrowDelta          = delta.left ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight
    var arrowPosition       = delta.left ? 'left'        : 'top'
    var arrowOffsetPosition = delta.left ? 'offsetWidth' : 'offsetHeight'

    $tip.offset(offset)
    this.replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], arrowPosition)
  }

  Tooltip.prototype.replaceArrow = function (delta, dimension, position) {
    this.arrow().css(position, delta ? (50 * (1 - delta / dimension) + '%') : '')
  }

  Tooltip.prototype.setContent = function () {
    var $tip  = this.tip()
    var title = this.getTitle()

    $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
    $tip.removeClass('fade in top bottom left right')
  }

  Tooltip.prototype.hide = function () {
    var that = this
    var $tip = this.tip()
    var e    = $.Event('hide.bs.' + this.type)

    function complete() {
      if (that.hoverState != 'in') $tip.detach()
      that.$element.trigger('hidden.bs.' + that.type)
    }

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    $tip.removeClass('in')

    $.support.transition && this.$tip.hasClass('fade') ?
      $tip
        .one($.support.transition.end, complete)
        .emulateTransitionEnd(150) :
      complete()

    this.hoverState = null

    return this
  }

  Tooltip.prototype.fixTitle = function () {
    var $e = this.$element
    if ($e.attr('title') || typeof($e.attr('data-original-title')) != 'string') {
      $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
    }
  }

  Tooltip.prototype.hasContent = function () {
    return this.getTitle()
  }

  Tooltip.prototype.getPosition = function ($element) {
    $element   = $element || this.$element
    var el     = $element[0]
    var isBody = el.tagName == 'BODY'
    return $.extend({}, (typeof el.getBoundingClientRect == 'function') ? el.getBoundingClientRect() : null, {
      scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop(),
      width:  isBody ? $(window).width()  : $element.outerWidth(),
      height: isBody ? $(window).height() : $element.outerHeight()
    }, isBody ? {top: 0, left: 0} : $element.offset())
  }

  Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
    return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2  } :
           placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2  } :
           placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
        /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width   }

  }

  Tooltip.prototype.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
    var delta = { top: 0, left: 0 }
    if (!this.$viewport) return delta

    var viewportPadding = this.options.viewport && this.options.viewport.padding || 0
    var viewportDimensions = this.getPosition(this.$viewport)

    if (/right|left/.test(placement)) {
      var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll
      var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight
      if (topEdgeOffset < viewportDimensions.top) { // top overflow
        delta.top = viewportDimensions.top - topEdgeOffset
      } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) { // bottom overflow
        delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset
      }
    } else {
      var leftEdgeOffset  = pos.left - viewportPadding
      var rightEdgeOffset = pos.left + viewportPadding + actualWidth
      if (leftEdgeOffset < viewportDimensions.left) { // left overflow
        delta.left = viewportDimensions.left - leftEdgeOffset
      } else if (rightEdgeOffset > viewportDimensions.width) { // right overflow
        delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset
      }
    }

    return delta
  }

  Tooltip.prototype.getTitle = function () {
    var title
    var $e = this.$element
    var o  = this.options

    title = $e.attr('data-original-title')
      || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

    return title
  }

  Tooltip.prototype.tip = function () {
    return this.$tip = this.$tip || $(this.options.template)
  }

  Tooltip.prototype.arrow = function () {
    return this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow')
  }

  Tooltip.prototype.validate = function () {
    if (!this.$element[0].parentNode) {
      this.hide()
      this.$element = null
      this.options  = null
    }
  }

  Tooltip.prototype.enable = function () {
    this.enabled = true
  }

  Tooltip.prototype.disable = function () {
    this.enabled = false
  }

  Tooltip.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled
  }

  Tooltip.prototype.toggle = function (e) {
    var self = e ? $(e.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type) : this
    self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
  }

  Tooltip.prototype.destroy = function () {
    clearTimeout(this.timeout)
    this.hide().$element.off('.' + this.type).removeData('bs.' + this.type)
  }


  // TOOLTIP PLUGIN DEFINITION
  // =========================

  var old = $.fn.tooltip

  $.fn.tooltip = function (option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.tooltip')
      var options = typeof option == 'object' && option

      if (!data && option == 'destroy') return
      if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tooltip.Constructor = Tooltip


  // TOOLTIP NO CONFLICT
  // ===================

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(jQuery);
/* ========================================================================
 * Bootstrap: popover.js v3.1.1
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */



+function ($) {
  'use strict';

  // POPOVER PUBLIC CLASS DEFINITION
  // ===============================

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }

  if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')

  Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


  // NOTE: POPOVER EXTENDS tooltip.js
  // ================================

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)

  Popover.prototype.constructor = Popover

  Popover.prototype.getDefaults = function () {
    return Popover.DEFAULTS
  }

  Popover.prototype.setContent = function () {
    var $tip    = this.tip()
    var title   = this.getTitle()
    var content = this.getContent()

    $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
    $tip.find('.popover-content').empty()[ // we use append for html objects to maintain js events
      this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
    ](content)

    $tip.removeClass('fade top bottom left right in')

    // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
    // this manually by checking the contents.
    if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
  }

  Popover.prototype.hasContent = function () {
    return this.getTitle() || this.getContent()
  }

  Popover.prototype.getContent = function () {
    var $e = this.$element
    var o  = this.options

    return $e.attr('data-content')
      || (typeof o.content == 'function' ?
            o.content.call($e[0]) :
            o.content)
  }

  Popover.prototype.arrow = function () {
    return this.$arrow = this.$arrow || this.tip().find('.arrow')
  }

  Popover.prototype.tip = function () {
    if (!this.$tip) this.$tip = $(this.options.template)
    return this.$tip
  }


  // POPOVER PLUGIN DEFINITION
  // =========================

  var old = $.fn.popover

  $.fn.popover = function (option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.popover')
      var options = typeof option == 'object' && option

      if (!data && option == 'destroy') return
      if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.popover.Constructor = Popover


  // POPOVER NO CONFLICT
  // ===================

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(jQuery);












/* =========================================================
 * bootstrap-slider.js v2.0.0
 * http://www.eyecon.ro/bootstrap-slider
 * =========================================================
 * Copyright 2012 Stefan Petre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */

 
!function( $ ) {

	var Slider = function(element, options) {
		this.element = $(element);
		this.picker = $('<div class="slider">'+
							'<div class="slider-track">'+
								'<div class="slider-selection"></div>'+
								'<div class="slider-handle"></div>'+
								'<div class="slider-handle"></div>'+
							'</div>'+
							'<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'+
						'</div>')
							.insertBefore(this.element)
							.append(this.element);
		this.id = this.element.data('slider-id')||options.id;
		if (this.id) {
			this.picker[0].id = this.id;
		}

		if (typeof Modernizr !== 'undefined' && Modernizr.touch) {
			this.touchCapable = true;
		}

		var tooltip = this.element.data('slider-tooltip')||options.tooltip;

		this.tooltip = this.picker.find('.tooltip');
		this.tooltipInner = this.tooltip.find('div.tooltip-inner');

		this.orientation = this.element.data('slider-orientation')||options.orientation;
		switch(this.orientation) {
			case 'vertical':
				this.picker.addClass('slider-vertical');
				this.stylePos = 'top';
				this.mousePos = 'pageY';
				this.sizePos = 'offsetHeight';
				this.tooltip.addClass('right')[0].style.left = '100%';
				break;
			default:
				this.picker
					.addClass('slider-horizontal')
					.css('width', this.element.outerWidth());
				this.orientation = 'horizontal';
				this.stylePos = 'left';
				this.mousePos = 'pageX';
				this.sizePos = 'offsetWidth';
				this.tooltip.addClass('top')[0].style.top = -this.tooltip.outerHeight() - 14 + 'px';
				break;
		}

		this.min = this.element.data('slider-min')||options.min;
		this.max = this.element.data('slider-max')||options.max;
		this.step = this.element.data('slider-step')||options.step;
		this.value = this.element.data('slider-value')||options.value;
		if (this.value[1]) {
			this.range = true;
		}

		this.selection = this.element.data('slider-selection')||options.selection;
		this.selectionEl = this.picker.find('.slider-selection');
		if (this.selection === 'none') {
			this.selectionEl.addClass('hide');
		}
		this.selectionElStyle = this.selectionEl[0].style;


		this.handle1 = this.picker.find('.slider-handle:first');
		this.handle1Stype = this.handle1[0].style;
		this.handle2 = this.picker.find('.slider-handle:last');
		this.handle2Stype = this.handle2[0].style;

		var handle = this.element.data('slider-handle')||options.handle;
		switch(handle) {
			case 'round':
				this.handle1.addClass('round');
				this.handle2.addClass('round');
				break
			case 'triangle':
				this.handle1.addClass('triangle');
				this.handle2.addClass('triangle');
				break
		}

		if (this.range) {
			this.value[0] = Math.max(this.min, Math.min(this.max, this.value[0]));
			this.value[1] = Math.max(this.min, Math.min(this.max, this.value[1]));
		} else {
			this.value = [ Math.max(this.min, Math.min(this.max, this.value))];
			this.handle2.addClass('hide');
			if (this.selection == 'after') {
				this.value[1] = this.max;
			} else {
				this.value[1] = this.min;
			}
		}
		this.diff = this.max - this.min;
		this.percentage = [
			(this.value[0]-this.min)*100/this.diff,
			(this.value[1]-this.min)*100/this.diff,
			this.step*100/this.diff
		];

		this.offset = this.picker.offset();
		this.size = this.picker[0][this.sizePos];

		this.formater = options.formater;

		this.layout();

		if (this.touchCapable) {
			// Touch: Bind touch events:
			this.picker.on({
				touchstart: $.proxy(this.mousedown, this)
			});
		} else {
			this.picker.on({
				mousedown: $.proxy(this.mousedown, this)
			});
		}

		if (tooltip === 'show') {
			this.picker.on({
				mouseenter: $.proxy(this.showTooltip, this),
				mouseleave: $.proxy(this.hideTooltip, this)
			});
		} else {
			this.tooltip.addClass('hide');
		}
	};

	Slider.prototype = {
		constructor: Slider,

		over: false,
		inDrag: false,
		
		showTooltip: function(){
			this.tooltip.addClass('in');
			//var left = Math.round(this.percent*this.width);
			//this.tooltip.css('left', left - this.tooltip.outerWidth()/2);
			this.over = true;
		},
		
		hideTooltip: function(){
			if (this.inDrag === false) {
				this.tooltip.removeClass('in');
			}
			this.over = false;
		},

		layout: function(){
			this.handle1Stype[this.stylePos] = this.percentage[0]+'%';
			this.handle2Stype[this.stylePos] = this.percentage[1]+'%';
			if (this.orientation == 'vertical') {
				this.selectionElStyle.top = Math.min(this.percentage[0], this.percentage[1]) +'%';
				this.selectionElStyle.height = Math.abs(this.percentage[0] - this.percentage[1]) +'%';
			} else {
				this.selectionElStyle.left = Math.min(this.percentage[0], this.percentage[1]) +'%';
				this.selectionElStyle.width = Math.abs(this.percentage[0] - this.percentage[1]) +'%';
			}
			if (this.range) {
				this.tooltipInner.text(
					this.formater(this.value[0]) + 
					' : ' + 
					this.formater(this.value[1])
				);
				this.tooltip[0].style[this.stylePos] = this.size * (this.percentage[0] + (this.percentage[1] - this.percentage[0])/2)/100 - (this.orientation === 'vertical' ? this.tooltip.outerHeight()/2 : this.tooltip.outerWidth()/2) +'px';
			} else {
				this.tooltipInner.text(
					this.formater(this.value[0])
				);
				this.tooltip[0].style[this.stylePos] = this.size * this.percentage[0]/100 - (this.orientation === 'vertical' ? this.tooltip.outerHeight()/2 : this.tooltip.outerWidth()/2) +'px';
			}
		},

		mousedown: function(ev) {

			// Touch: Get the original event:
			if (this.touchCapable && ev.type === 'touchstart') {
				ev = ev.originalEvent;
			}

			this.offset = this.picker.offset();
			this.size = this.picker[0][this.sizePos];

			var percentage = this.getPercentage(ev);

			if (this.range) {
				var diff1 = Math.abs(this.percentage[0] - percentage);
				var diff2 = Math.abs(this.percentage[1] - percentage);
				this.dragged = (diff1 < diff2) ? 0 : 1;
			} else {
				this.dragged = 0;
			}

			this.percentage[this.dragged] = percentage;
			this.layout();

			if (this.touchCapable) {
				// Touch: Bind touch events:
				$(document).on({
					touchmove: $.proxy(this.mousemove, this),
					touchend: $.proxy(this.mouseup, this)
				});
			} else {
				$(document).on({
					mousemove: $.proxy(this.mousemove, this),
					mouseup: $.proxy(this.mouseup, this)
				});
			}

			this.inDrag = true;
			var val = this.calculateValue();
			this.element.trigger({
					type: 'slideStart',
					value: val
				}).trigger({
					type: 'slide',
					value: val
				});
			return false;
		},

		mousemove: function(ev) {
			
			// Touch: Get the original event:
			if (this.touchCapable && ev.type === 'touchmove') {
				ev = ev.originalEvent;
			}

			var percentage = this.getPercentage(ev);
			if (this.range) {
				if (this.dragged === 0 && this.percentage[1] < percentage) {
					this.percentage[0] = this.percentage[1];
					this.dragged = 1;
				} else if (this.dragged === 1 && this.percentage[0] > percentage) {
					this.percentage[1] = this.percentage[0];
					this.dragged = 0;
				}
			}
			this.percentage[this.dragged] = percentage;
			this.layout();
			var val = this.calculateValue();
			this.element
				.trigger({
					type: 'slide',
					value: val
				})
				.data('value', val)
				.prop('value', val);
			return false;
		},

		mouseup: function(ev) {
			if (this.touchCapable) {
				// Touch: Bind touch events:
				$(document).off({
					touchmove: this.mousemove,
					touchend: this.mouseup
				});
			} else {
				$(document).off({
					mousemove: this.mousemove,
					mouseup: this.mouseup
				});
			}

			this.inDrag = false;
			if (this.over == false) {
				this.hideTooltip();
			}
			this.element;
			var val = this.calculateValue();
			this.element
				.trigger({
					type: 'slideStop',
					value: val
				})
				.data('value', val)
				.prop('value', val);
			return false;
		},

		calculateValue: function() {
			var val;
			if (this.range) {
				val = [
					(this.min + Math.round((this.diff * this.percentage[0]/100)/this.step)*this.step),
					(this.min + Math.round((this.diff * this.percentage[1]/100)/this.step)*this.step)
				];
				this.value = val;
			} else {
				val = (this.min + Math.round((this.diff * this.percentage[0]/100)/this.step)*this.step);
				this.value = [val, this.value[1]];
			}
			return val;
		},

		getPercentage: function(ev) {
			if (this.touchCapable) {
				ev = ev.touches[0];
			}
			var percentage = (ev[this.mousePos] - this.offset[this.stylePos])*100/this.size;
			percentage = Math.round(percentage/this.percentage[2])*this.percentage[2];
			return Math.max(0, Math.min(100, percentage));
		},

		getValue: function() {
			if (this.range) {
				return this.value;
			}
			return this.value[0];
		},

		setValue: function(val) {
			this.value = val;

			if (this.range) {
				this.value[0] = Math.max(this.min, Math.min(this.max, this.value[0]));
				this.value[1] = Math.max(this.min, Math.min(this.max, this.value[1]));
			} else {
				this.value = [ Math.max(this.min, Math.min(this.max, this.value))];
				this.handle2.addClass('hide');
				if (this.selection == 'after') {
					this.value[1] = this.max;
				} else {
					this.value[1] = this.min;
				}
			}
			this.diff = this.max - this.min;
			this.percentage = [
				(this.value[0]-this.min)*100/this.diff,
				(this.value[1]-this.min)*100/this.diff,
				this.step*100/this.diff
			];
			this.layout();
		}
	};

	$.fn.slider = function ( option, val ) {
		return this.each(function () {
			var $this = $(this),
				data = $this.data('slider'),
				options = typeof option === 'object' && option;
			if (!data)  {
				$this.data('slider', (data = new Slider(this, $.extend({}, $.fn.slider.defaults,options))));
			}
			if (typeof option == 'string') {
				data[option](val);
			}
		})
	};

	$.fn.slider.defaults = {
		min: 0,
		max: 10,
		step: 1,
		orientation: 'horizontal',
		value: 5,
		selection: 'before',
		tooltip: 'show',
		handle: 'round',
		formater: function(value) {
			return value;
		}
	};

	$.fn.slider.Constructor = Slider;

}( window.jQuery );
/*
colpick Color Picker
Copyright 2013 Jose Vargas. Licensed under GPL license. Based on Stefan Petre's Color Picker www.eyecon.ro, dual licensed under the MIT and GPL licenses

For usage and examples: colpick.com/plugin
 */


(function ($) {
	var colpick = function () {
		var
			tpl = '<div class="colpick"><div class="colpick_color"><div class="colpick_color_overlay1"><div class="colpick_color_overlay2"><div class="colpick_selector_outer"><div class="colpick_selector_inner"></div></div></div></div></div><div class="colpick_hue"><div class="colpick_hue_arrs"><div class="colpick_hue_larr"></div><div class="colpick_hue_rarr"></div></div></div><div class="colpick_new_color"></div><div class="colpick_current_color"></div><div class="colpick_hex_field"><div class="colpick_field_letter">#</div><input type="text" maxlength="6" size="6" /></div><div class="colpick_rgb_r colpick_field"><div class="colpick_field_letter">R</div><input type="text" maxlength="3" size="3" /><div class="colpick_field_arrs"><div class="colpick_field_uarr"></div><div class="colpick_field_darr"></div></div></div><div class="colpick_rgb_g colpick_field"><div class="colpick_field_letter">G</div><input type="text" maxlength="3" size="3" /><div class="colpick_field_arrs"><div class="colpick_field_uarr"></div><div class="colpick_field_darr"></div></div></div><div class="colpick_rgb_b colpick_field"><div class="colpick_field_letter">B</div><input type="text" maxlength="3" size="3" /><div class="colpick_field_arrs"><div class="colpick_field_uarr"></div><div class="colpick_field_darr"></div></div></div><div class="colpick_hsb_h colpick_field"><div class="colpick_field_letter">H</div><input type="text" maxlength="3" size="3" /><div class="colpick_field_arrs"><div class="colpick_field_uarr"></div><div class="colpick_field_darr"></div></div></div><div class="colpick_hsb_s colpick_field"><div class="colpick_field_letter">S</div><input type="text" maxlength="3" size="3" /><div class="colpick_field_arrs"><div class="colpick_field_uarr"></div><div class="colpick_field_darr"></div></div></div><div class="colpick_hsb_b colpick_field"><div class="colpick_field_letter">B</div><input type="text" maxlength="3" size="3" /><div class="colpick_field_arrs"><div class="colpick_field_uarr"></div><div class="colpick_field_darr"></div></div></div><div class="colpick_submit"></div></div>',
			defaults = {
				showEvent: 'click',
				onShow: function () {},
				onBeforeShow: function(){},
				onHide: function () {},
				onChange: function () {},
				onSubmit: function () {},
				colorScheme: 'light',
				color: '3289c7',
				livePreview: true,
				flat: false,
				layout: 'full',
				submit: 1,
				submitText: 'OK',
				height: 156
			},
			//Fill the inputs of the plugin
			fillRGBFields = function  (hsb, cal) {
				var rgb = hsbToRgb(hsb);
				$(cal).data('colpick').fields
					.eq(1).val(rgb.r).end()
					.eq(2).val(rgb.g).end()
					.eq(3).val(rgb.b).end();
			},
			fillHSBFields = function  (hsb, cal) {
				$(cal).data('colpick').fields
					.eq(4).val(Math.round(hsb.h)).end()
					.eq(5).val(Math.round(hsb.s)).end()
					.eq(6).val(Math.round(hsb.b)).end();
			},
			fillHexFields = function (hsb, cal) {
				$(cal).data('colpick').fields.eq(0).val(hsbToHex(hsb));
			},
			//Set the round selector position
			setSelector = function (hsb, cal) {
				$(cal).data('colpick').selector.css('backgroundColor', '#' + hsbToHex({h: hsb.h, s: 100, b: 100}));
				$(cal).data('colpick').selectorIndic.css({
					left: parseInt($(cal).data('colpick').height * hsb.s/100, 10),
					top: parseInt($(cal).data('colpick').height * (100-hsb.b)/100, 10)
				});
			},
			//Set the hue selector position
			setHue = function (hsb, cal) {
				$(cal).data('colpick').hue.css('top', parseInt($(cal).data('colpick').height - $(cal).data('colpick').height * hsb.h/360, 10));
			},
			//Set current and new colors
			setCurrentColor = function (hsb, cal) {
				$(cal).data('colpick').currentColor.css('backgroundColor', '#' + hsbToHex(hsb));
			},
			setNewColor = function (hsb, cal) {
				$(cal).data('colpick').newColor.css('backgroundColor', '#' + hsbToHex(hsb));
			},
			//Called when the new color is changed
			change = function (ev) {
				var cal = $(this).parent().parent(), col;
				if (this.parentNode.className.indexOf('_hex') > 0) {
					cal.data('colpick').color = col = hexToHsb(fixHex(this.value));
					fillRGBFields(col, cal.get(0));
					fillHSBFields(col, cal.get(0));
				} else if (this.parentNode.className.indexOf('_hsb') > 0) {
					cal.data('colpick').color = col = fixHSB({
						h: parseInt(cal.data('colpick').fields.eq(4).val(), 10),
						s: parseInt(cal.data('colpick').fields.eq(5).val(), 10),
						b: parseInt(cal.data('colpick').fields.eq(6).val(), 10)
					});
					fillRGBFields(col, cal.get(0));
					fillHexFields(col, cal.get(0));
				} else {
					cal.data('colpick').color = col = rgbToHsb(fixRGB({
						r: parseInt(cal.data('colpick').fields.eq(1).val(), 10),
						g: parseInt(cal.data('colpick').fields.eq(2).val(), 10),
						b: parseInt(cal.data('colpick').fields.eq(3).val(), 10)
					}));
					fillHexFields(col, cal.get(0));
					fillHSBFields(col, cal.get(0));
				}
				setSelector(col, cal.get(0));
				setHue(col, cal.get(0));
				setNewColor(col, cal.get(0));
				cal.data('colpick').onChange.apply(cal.parent(), [col, hsbToHex(col), hsbToRgb(col), cal.data('colpick').el, 0]);
			},
			//Change style on blur and on focus of inputs
			blur = function (ev) {
				$(this).parent().removeClass('colpick_focus');
			},
			focus = function () {
				$(this).parent().parent().data('colpick').fields.parent().removeClass('colpick_focus');
				$(this).parent().addClass('colpick_focus');
			},
			//Increment/decrement arrows functions
			downIncrement = function (ev) {
				ev.preventDefault ? ev.preventDefault() : ev.returnValue = false;
				var field = $(this).parent().find('input').focus();
				var current = {
					el: $(this).parent().addClass('colpick_slider'),
					max: this.parentNode.className.indexOf('_hsb_h') > 0 ? 360 : (this.parentNode.className.indexOf('_hsb') > 0 ? 100 : 255),
					y: ev.pageY,
					field: field,
					val: parseInt(field.val(), 10),
					preview: $(this).parent().parent().data('colpick').livePreview
				};
				$(document).mouseup(current, upIncrement);
				$(document).mousemove(current, moveIncrement);
			},
			moveIncrement = function (ev) {
				ev.data.field.val(Math.max(0, Math.min(ev.data.max, parseInt(ev.data.val - ev.pageY + ev.data.y, 10))));
				if (ev.data.preview) {
					change.apply(ev.data.field.get(0), [true]);
				}
				return false;
			},
			upIncrement = function (ev) {
				change.apply(ev.data.field.get(0), [true]);
				ev.data.el.removeClass('colpick_slider').find('input').focus();
				$(document).off('mouseup', upIncrement);
				$(document).off('mousemove', moveIncrement);
				return false;
			},
			//Hue slider functions
			downHue = function (ev) {
				ev.preventDefault ? ev.preventDefault() : ev.returnValue = false;
				var current = {
					cal: $(this).parent(),
					y: $(this).offset().top
				};
				$(document).on('mouseup touchend',current,upHue);
				$(document).on('mousemove touchmove',current,moveHue);
				
				var pageY = ((ev.type == 'touchstart') ? ev.originalEvent.changedTouches[0].pageY : ev.pageY );
				change.apply(
					current.cal.data('colpick')
					.fields.eq(4).val(parseInt(360*(current.cal.data('colpick').height - (pageY - current.y))/current.cal.data('colpick').height, 10))
						.get(0),
					[current.cal.data('colpick').livePreview]
				);
				return false;
			},
			moveHue = function (ev) {
				var pageY = ((ev.type == 'touchmove') ? ev.originalEvent.changedTouches[0].pageY : ev.pageY );
				change.apply(
					ev.data.cal.data('colpick')
					.fields.eq(4).val(parseInt(360*(ev.data.cal.data('colpick').height - Math.max(0,Math.min(ev.data.cal.data('colpick').height,(pageY - ev.data.y))))/ev.data.cal.data('colpick').height, 10))
						.get(0),
					[ev.data.preview]
				);
				return false;
			},
			upHue = function (ev) {
				fillRGBFields(ev.data.cal.data('colpick').color, ev.data.cal.get(0));
				fillHexFields(ev.data.cal.data('colpick').color, ev.data.cal.get(0));
				$(document).off('mouseup touchend',upHue);
				$(document).off('mousemove touchmove',moveHue);
				return false;
			},
			//Color selector functions
			downSelector = function (ev) {
				ev.preventDefault ? ev.preventDefault() : ev.returnValue = false;
				var current = {
					cal: $(this).parent(),
					pos: $(this).offset()
				};
				current.preview = current.cal.data('colpick').livePreview;
				
				$(document).on('mouseup touchend',current,upSelector);
				$(document).on('mousemove touchmove',current,moveSelector);

				var payeX,pageY;
				if(ev.type == 'touchstart') {
					pageX = ev.originalEvent.changedTouches[0].pageX,
					pageY = ev.originalEvent.changedTouches[0].pageY;
				} else {
					pageX = ev.pageX;
					pageY = ev.pageY;
				}

				change.apply(
					current.cal.data('colpick').fields
					.eq(6).val(parseInt(100*(current.cal.data('colpick').height - (pageY - current.pos.top))/current.cal.data('colpick').height, 10)).end()
					.eq(5).val(parseInt(100*(pageX - current.pos.left)/current.cal.data('colpick').height, 10))
					.get(0),
					[current.preview]
				);
				return false;
			},
			moveSelector = function (ev) {
				var payeX,pageY;
				if(ev.type == 'touchmove') {
					pageX = ev.originalEvent.changedTouches[0].pageX,
					pageY = ev.originalEvent.changedTouches[0].pageY;
				} else {
					pageX = ev.pageX;
					pageY = ev.pageY;
				}

				change.apply(
					ev.data.cal.data('colpick').fields
					.eq(6).val(parseInt(100*(ev.data.cal.data('colpick').height - Math.max(0,Math.min(ev.data.cal.data('colpick').height,(pageY - ev.data.pos.top))))/ev.data.cal.data('colpick').height, 10)).end()
					.eq(5).val(parseInt(100*(Math.max(0,Math.min(ev.data.cal.data('colpick').height,(pageX - ev.data.pos.left))))/ev.data.cal.data('colpick').height, 10))
					.get(0),
					[ev.data.preview]
				);
				return false;
			},
			upSelector = function (ev) {
				fillRGBFields(ev.data.cal.data('colpick').color, ev.data.cal.get(0));
				fillHexFields(ev.data.cal.data('colpick').color, ev.data.cal.get(0));
				$(document).off('mouseup touchend',upSelector);
				$(document).off('mousemove touchmove',moveSelector);
				return false;
			},
			//Submit button
			clickSubmit = function (ev) {
				var cal = $(this).parent();
				var col = cal.data('colpick').color;
				cal.data('colpick').origColor = col;
				setCurrentColor(col, cal.get(0));
				cal.data('colpick').onSubmit(col, hsbToHex(col), hsbToRgb(col), cal.data('colpick').el);
			},
			//Show/hide the color picker
			show = function (ev) {
				// Prevent the trigger of any direct parent
				ev.stopPropagation();
				var cal = $('#' + $(this).data('colpickId'));
				cal.data('colpick').onBeforeShow.apply(this, [cal.get(0)]);
				var pos = $(this).offset();
				var top = pos.top + this.offsetHeight;
				var left = pos.left;
				var viewPort = getViewport();
				var calW = cal.width();
				if (left + calW > viewPort.l + viewPort.w) {
					left -= calW;
				}
				cal.css({left: left + 'px', top: top + 'px'});
				if (cal.data('colpick').onShow.apply(this, [cal.get(0)]) != false) {
					cal.show();
				}
				//Hide when user clicks outside
				$('html').mousedown({cal:cal}, hide);
				cal.mousedown(function(ev){ev.stopPropagation();})
			},
			hide = function (ev) {
				if (ev.data.cal.data('colpick').onHide.apply(this, [ev.data.cal.get(0)]) != false) {
					ev.data.cal.hide();
				}
				$('html').off('mousedown', hide);
			},
			getViewport = function () {
				var m = document.compatMode == 'CSS1Compat';
				return {
					l : window.pageXOffset || (m ? document.documentElement.scrollLeft : document.body.scrollLeft),
					w : window.innerWidth || (m ? document.documentElement.clientWidth : document.body.clientWidth)
				};
			},
			//Fix the values if the user enters a negative or high value
			fixHSB = function (hsb) {
				return {
					h: Math.min(360, Math.max(0, hsb.h)),
					s: Math.min(100, Math.max(0, hsb.s)),
					b: Math.min(100, Math.max(0, hsb.b))
				};
			}, 
			fixRGB = function (rgb) {
				return {
					r: Math.min(255, Math.max(0, rgb.r)),
					g: Math.min(255, Math.max(0, rgb.g)),
					b: Math.min(255, Math.max(0, rgb.b))
				};
			},
			fixHex = function (hex) {
				var len = 6 - hex.length;
				if (len > 0) {
					var o = [];
					for (var i=0; i<len; i++) {
						o.push('0');
					}
					o.push(hex);
					hex = o.join('');
				}
				return hex;
			},
			restoreOriginal = function () {
				var cal = $(this).parent();
				var col = cal.data('colpick').origColor;
				cal.data('colpick').color = col;
				fillRGBFields(col, cal.get(0));
				fillHexFields(col, cal.get(0));
				fillHSBFields(col, cal.get(0));
				setSelector(col, cal.get(0));
				setHue(col, cal.get(0));
				setNewColor(col, cal.get(0));
			};
		return {
			init: function (opt) {
				opt = $.extend({}, defaults, opt||{});
				//Set color
				if (typeof opt.color == 'string') {
					opt.color = hexToHsb(opt.color);
				} else if (opt.color.r != undefined && opt.color.g != undefined && opt.color.b != undefined) {
					opt.color = rgbToHsb(opt.color);
				} else if (opt.color.h != undefined && opt.color.s != undefined && opt.color.b != undefined) {
					opt.color = fixHSB(opt.color);
				} else {
					return this;
				}
				
				//For each selected DOM element
				return this.each(function () {
					//If the element does not have an ID
					if (!$(this).data('colpickId')) {
						var options = $.extend({}, opt);
						options.origColor = opt.color;
						//Generate and assign a random ID
						var id = 'collorpicker_' + parseInt(Math.random() * 1000);
						$(this).data('colpickId', id);
						//Set the tpl's ID and get the HTML
						var cal = $(tpl).attr('id', id);
						//Add class according to layout
						cal.addClass('colpick_'+options.layout+(options.submit?'':' colpick_'+options.layout+'_ns'));
						//Add class if the color scheme is not default
						if(options.colorScheme != 'light') {
							cal.addClass('colpick_'+options.colorScheme);
						}
						//Setup submit button
						cal.find('div.colpick_submit').html(options.submitText).click(clickSubmit);
						//Setup input fields
						options.fields = cal.find('input').change(change).blur(blur).focus(focus);
						cal.find('div.colpick_field_arrs').mousedown(downIncrement).end().find('div.colpick_current_color').click(restoreOriginal);
						//Setup hue selector
						options.selector = cal.find('div.colpick_color').on('mousedown touchstart',downSelector);
						options.selectorIndic = options.selector.find('div.colpick_selector_outer');
						//Store parts of the plugin
						options.el = this;
						options.hue = cal.find('div.colpick_hue_arrs');
						huebar = options.hue.parent();
						//Paint the hue bar
						var UA = navigator.userAgent.toLowerCase();
						var isIE = navigator.appName === 'Microsoft Internet Explorer';
						var IEver = isIE ? parseFloat( UA.match( /msie ([0-9]{1,}[\.0-9]{0,})/ )[1] ) : 0;
						var ngIE = ( isIE && IEver < 10 );
						var stops = ['#ff0000','#ff0080','#ff00ff','#8000ff','#0000ff','#0080ff','#00ffff','#00ff80','#00ff00','#80ff00','#ffff00','#ff8000','#ff0000'];
						if(ngIE) {
							var i, div;
							for(i=0; i<=11; i++) {
								div = $('<div></div>').attr('style','height:8.333333%; filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='+stops[i]+', endColorstr='+stops[i+1]+'); -ms-filter: "progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='+stops[i]+', endColorstr='+stops[i+1]+')";');
								huebar.append(div);
							}
						} else {
							stopList = stops.join(',');
							huebar.attr('style','background:-webkit-linear-gradient(top,'+stopList+'); background: -o-linear-gradient(top,'+stopList+'); background: -ms-linear-gradient(top,'+stopList+'); background:-moz-linear-gradient(top,'+stopList+'); -webkit-linear-gradient(top,'+stopList+'); background:linear-gradient(to bottom,'+stopList+'); ');
						}
						cal.find('div.colpick_hue').on('mousedown touchstart',downHue);
						options.newColor = cal.find('div.colpick_new_color');
						options.currentColor = cal.find('div.colpick_current_color');
						//Store options and fill with default color
						cal.data('colpick', options);
						fillRGBFields(options.color, cal.get(0));
						fillHSBFields(options.color, cal.get(0));
						fillHexFields(options.color, cal.get(0));
						setHue(options.color, cal.get(0));
						setSelector(options.color, cal.get(0));
						setCurrentColor(options.color, cal.get(0));
						setNewColor(options.color, cal.get(0));
						//Append to body if flat=false, else show in place
						if (options.flat) {
							cal.appendTo(this).show();
							cal.css({
								position: 'relative',
								display: 'block'
							});
						} else {
							cal.appendTo(document.body);
							$(this).on(options.showEvent, show);
							cal.css({
								position:'absolute'
							});
						}
					}
				});
			},
			//Shows the picker
			showPicker: function() {
				return this.each( function () {
					if ($(this).data('colpickId')) {
						show.apply(this);
					}
				});
			},
			//Hides the picker
			hidePicker: function() {
				return this.each( function () {
					if ($(this).data('colpickId')) {
						$('#' + $(this).data('colpickId')).hide();
					}
				});
			},
			//Sets a color as new and current (default)
			setColor: function(col, setCurrent) {
				setCurrent = (typeof setCurrent === "undefined") ? 1 : setCurrent;
				if (typeof col == 'string') {
					col = hexToHsb(col);
				} else if (col.r != undefined && col.g != undefined && col.b != undefined) {
					col = rgbToHsb(col);
				} else if (col.h != undefined && col.s != undefined && col.b != undefined) {
					col = fixHSB(col);
				} else {
					return this;
				}
				return this.each(function(){
					if ($(this).data('colpickId')) {
						var cal = $('#' + $(this).data('colpickId'));
						cal.data('colpick').color = col;
						cal.data('colpick').origColor = col;
						fillRGBFields(col, cal.get(0));
						fillHSBFields(col, cal.get(0));
						fillHexFields(col, cal.get(0));
						setHue(col, cal.get(0));
						setSelector(col, cal.get(0));
						
						setNewColor(col, cal.get(0));
						cal.data('colpick').onChange.apply(cal.parent(), [col, hsbToHex(col), hsbToRgb(col), cal.data('colpick').el, 1]);
						if(setCurrent) {
							setCurrentColor(col, cal.get(0));
						}
					}
				});
			}
		};
	}();
	//Color space convertions
	var hexToRgb = function (hex) {
		var hex = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
		return {r: hex >> 16, g: (hex & 0x00FF00) >> 8, b: (hex & 0x0000FF)};
	};
	var hexToHsb = function (hex) {
		return rgbToHsb(hexToRgb(hex));
	};
	var rgbToHsb = function (rgb) {
		var hsb = {h: 0, s: 0, b: 0};
		var min = Math.min(rgb.r, rgb.g, rgb.b);
		var max = Math.max(rgb.r, rgb.g, rgb.b);
		var delta = max - min;
		hsb.b = max;
		hsb.s = max != 0 ? 255 * delta / max : 0;
		if (hsb.s != 0) {
			if (rgb.r == max) hsb.h = (rgb.g - rgb.b) / delta;
			else if (rgb.g == max) hsb.h = 2 + (rgb.b - rgb.r) / delta;
			else hsb.h = 4 + (rgb.r - rgb.g) / delta;
		} else hsb.h = -1;
		hsb.h *= 60;
		if (hsb.h < 0) hsb.h += 360;
		hsb.s *= 100/255;
		hsb.b *= 100/255;
		return hsb;
	};
	var hsbToRgb = function (hsb) {
		var rgb = {};
		var h = hsb.h;
		var s = hsb.s*255/100;
		var v = hsb.b*255/100;
		if(s == 0) {
			rgb.r = rgb.g = rgb.b = v;
		} else {
			var t1 = v;
			var t2 = (255-s)*v/255;
			var t3 = (t1-t2)*(h%60)/60;
			if(h==360) h = 0;
			if(h<60) {rgb.r=t1;	rgb.b=t2; rgb.g=t2+t3}
			else if(h<120) {rgb.g=t1; rgb.b=t2;	rgb.r=t1-t3}
			else if(h<180) {rgb.g=t1; rgb.r=t2;	rgb.b=t2+t3}
			else if(h<240) {rgb.b=t1; rgb.r=t2;	rgb.g=t1-t3}
			else if(h<300) {rgb.b=t1; rgb.g=t2;	rgb.r=t2+t3}
			else if(h<360) {rgb.r=t1; rgb.g=t2;	rgb.b=t1-t3}
			else {rgb.r=0; rgb.g=0;	rgb.b=0}
		}
		return {r:Math.round(rgb.r), g:Math.round(rgb.g), b:Math.round(rgb.b)};
	};
	var rgbToHex = function (rgb) {
		var hex = [
			rgb.r.toString(16),
			rgb.g.toString(16),
			rgb.b.toString(16)
		];
		$.each(hex, function (nr, val) {
			if (val.length == 1) {
				hex[nr] = '0' + val;
			}
		});
		return hex.join('');
	};
	var hsbToHex = function (hsb) {
		return rgbToHex(hsbToRgb(hsb));
	};
	$.fn.extend({
		colpick: colpick.init,
		colpickHide: colpick.hidePicker,
		colpickShow: colpick.showPicker,
		colpickSetColor: colpick.setColor
	});
	$.extend({
		colpick:{ 
			rgbToHex: rgbToHex,
			rgbToHsb: rgbToHsb,
			hsbToHex: hsbToHex,
			hsbToRgb: hsbToRgb,
			hexToHsb: hexToHsb,
			hexToRgb: hexToRgb
		}
	});
})(jQuery);
function showImagePreview(imgSelector,previewContainer){
	var preview = $(previewContainer);
	console.log(preview);
	$(imgSelector).change(function(event){
		 var input = $(event.currentTarget);
	   var file = input[0].files[0];
	   var reader = new FileReader();
	   reader.onload = function(e){
	       image_base64 = e.target.result;
	       preview.attr("src", image_base64);
	   };
	   reader.readAsDataURL(file);
	});
}

;
/*
 AngularJS v1.2.17
 (c) 2010-2014 Google, Inc. http://angularjs.org
 License: MIT
*/

(function(n,e,A){'use strict';function x(s,g,k){return{restrict:"ECA",terminal:!0,priority:400,transclude:"element",link:function(a,c,b,f,w){function y(){p&&(p.remove(),p=null);h&&(h.$destroy(),h=null);l&&(k.leave(l,function(){p=null}),p=l,l=null)}function v(){var b=s.current&&s.current.locals;if(e.isDefined(b&&b.$template)){var b=a.$new(),d=s.current;l=w(b,function(d){k.enter(d,null,l||c,function(){!e.isDefined(t)||t&&!a.$eval(t)||g()});y()});h=d.scope=b;h.$emit("$viewContentLoaded");h.$eval(u)}else y()}
var h,l,p,t=b.autoscroll,u=b.onload||"";a.$on("$routeChangeSuccess",v);v()}}}function z(e,g,k){return{restrict:"ECA",priority:-400,link:function(a,c){var b=k.current,f=b.locals;c.html(f.$template);var w=e(c.contents());b.controller&&(f.$scope=a,f=g(b.controller,f),b.controllerAs&&(a[b.controllerAs]=f),c.data("$ngControllerController",f),c.children().data("$ngControllerController",f));w(a)}}}n=e.module("ngRoute",["ng"]).provider("$route",function(){function s(a,c){return e.extend(new (e.extend(function(){},
{prototype:a})),c)}function g(a,e){var b=e.caseInsensitiveMatch,f={originalPath:a,regexp:a},k=f.keys=[];a=a.replace(/([().])/g,"\\$1").replace(/(\/)?:(\w+)([\?\*])?/g,function(a,e,b,c){a="?"===c?c:null;c="*"===c?c:null;k.push({name:b,optional:!!a});e=e||"";return""+(a?"":e)+"(?:"+(a?e:"")+(c&&"(.+?)"||"([^/]+)")+(a||"")+")"+(a||"")}).replace(/([\/$\*])/g,"\\$1");f.regexp=RegExp("^"+a+"$",b?"i":"");return f}var k={};this.when=function(a,c){k[a]=e.extend({reloadOnSearch:!0},c,a&&g(a,c));if(a){var b=
"/"==a[a.length-1]?a.substr(0,a.length-1):a+"/";k[b]=e.extend({redirectTo:a},g(b,c))}return this};this.otherwise=function(a){this.when(null,a);return this};this.$get=["$rootScope","$location","$routeParams","$q","$injector","$http","$templateCache","$sce",function(a,c,b,f,g,n,v,h){function l(){var d=p(),m=r.current;if(d&&m&&d.$$route===m.$$route&&e.equals(d.pathParams,m.pathParams)&&!d.reloadOnSearch&&!u)m.params=d.params,e.copy(m.params,b),a.$broadcast("$routeUpdate",m);else if(d||m)u=!1,a.$broadcast("$routeChangeStart",
d,m),(r.current=d)&&d.redirectTo&&(e.isString(d.redirectTo)?c.path(t(d.redirectTo,d.params)).search(d.params).replace():c.url(d.redirectTo(d.pathParams,c.path(),c.search())).replace()),f.when(d).then(function(){if(d){var a=e.extend({},d.resolve),c,b;e.forEach(a,function(d,c){a[c]=e.isString(d)?g.get(d):g.invoke(d)});e.isDefined(c=d.template)?e.isFunction(c)&&(c=c(d.params)):e.isDefined(b=d.templateUrl)&&(e.isFunction(b)&&(b=b(d.params)),b=h.getTrustedResourceUrl(b),e.isDefined(b)&&(d.loadedTemplateUrl=
b,c=n.get(b,{cache:v}).then(function(a){return a.data})));e.isDefined(c)&&(a.$template=c);return f.all(a)}}).then(function(c){d==r.current&&(d&&(d.locals=c,e.copy(d.params,b)),a.$broadcast("$routeChangeSuccess",d,m))},function(c){d==r.current&&a.$broadcast("$routeChangeError",d,m,c)})}function p(){var a,b;e.forEach(k,function(f,k){var q;if(q=!b){var g=c.path();q=f.keys;var l={};if(f.regexp)if(g=f.regexp.exec(g)){for(var h=1,p=g.length;h<p;++h){var n=q[h-1],r="string"==typeof g[h]?decodeURIComponent(g[h]):
g[h];n&&r&&(l[n.name]=r)}q=l}else q=null;else q=null;q=a=q}q&&(b=s(f,{params:e.extend({},c.search(),a),pathParams:a}),b.$$route=f)});return b||k[null]&&s(k[null],{params:{},pathParams:{}})}function t(a,c){var b=[];e.forEach((a||"").split(":"),function(a,d){if(0===d)b.push(a);else{var e=a.match(/(\w+)(.*)/),f=e[1];b.push(c[f]);b.push(e[2]||"");delete c[f]}});return b.join("")}var u=!1,r={routes:k,reload:function(){u=!0;a.$evalAsync(l)}};a.$on("$locationChangeSuccess",l);return r}]});n.provider("$routeParams",
function(){this.$get=function(){return{}}});n.directive("ngView",x);n.directive("ngView",z);x.$inject=["$route","$anchorScroll","$animate"];z.$inject=["$compile","$controller","$route"]})(window,window.angular);
//# sourceMappingURL=angular-route.min.js.map
;
{
"version":3,
"file":"angular-route.min.js",
"lineCount":13,
"mappings":"A;;;;;aAKC,SAAQ,CAACA,CAAD,CAASC,CAAT,CAAkBC,CAAlB,CAA6B,CAizBtCC,QAASA,EAAa,CAAIC,CAAJ,CAAcC,CAAd,CAA+BC,CAA/B,CAAyC,CAC7D,MAAO,UACK,KADL,UAEK,CAAA,CAFL,UAGK,GAHL,YAIO,SAJP,MAKCC,QAAQ,CAACC,CAAD,CAAQC,CAAR,CAAkBC,CAAlB,CAAwBC,CAAxB,CAA8BC,CAA9B,CAA2C,CAUrDC,QAASA,EAAe,EAAG,CACtBC,CAAH,GACEA,CAAAC,OAAA,EACA,CAAAD,CAAA,CAAkB,IAFpB,CAIGE,EAAH,GACEA,CAAAC,SAAA,EACA,CAAAD,CAAA,CAAe,IAFjB,CAIGE,EAAH,GACEZ,CAAAa,MAAA,CAAeD,CAAf,CAA+B,QAAQ,EAAG,CACxCJ,CAAA,CAAkB,IADsB,CAA1C,CAIA,CADAA,CACA,CADkBI,CAClB,CAAAA,CAAA,CAAiB,IALnB,CATyB,CAkB3BE,QAASA,EAAM,EAAG,CAAA,IACZC,EAASjB,CAAAkB,QAATD,EAA2BjB,CAAAkB,QAAAD,OAG/B,IAAIpB,CAAAsB,UAAA,CAFWF,CAEX,EAFqBA,CAAAG,UAErB,CAAJ,CAAiC,CAC3BC,IAAAA,EAAWjB,CAAAkB,KAAA,EAAXD,CACAH,EAAUlB,CAAAkB,QAkBdJ,EAAA,CAVYN,CAAAe,CAAYF,CAAZE,CAAsB,QAAQ,CAACA,CAAD,CAAQ,CAChDrB,CAAAsB,MAAA,CAAeD,CAAf,CAAsB,IAAtB,CAA4BT,CAA5B,EAA8CT,CAA9C,CAAwDoB,QAAuB,EAAG,CAC5E,CAAA5B,CAAAsB,UAAA,CAAkBO,CAAlB,CAAJ,EACOA,CADP,EACwB,CAAAtB,CAAAuB,MAAA,CAAYD,CAAZ,CADxB,EAEEzB,CAAA,EAH8E,CAAlF,CAMAQ,EAAA,EAPgD,CAAtCc,CAWZX,EAAA,CAAeM,CAAAd,MAAf,CAA+BiB,CAC/BT,EAAAgB,MAAA,CAAmB,oBAAnB,CACAhB,EAAAe,MAAA,CAAmBE,CAAnB,CAvB+B,CAAjC,IAyBEpB,EAAA,EA7Bc,CA5BmC;AAAA,IACjDG,CADiD,CAEjDE,CAFiD,CAGjDJ,CAHiD,CAIjDgB,EAAgBpB,CAAAwB,WAJiC,CAKjDD,EAAYvB,CAAAyB,OAAZF,EAA2B,EAE/BzB,EAAA4B,IAAA,CAAU,qBAAV,CAAiChB,CAAjC,CACAA,EAAA,EARqD,CALpD,CADsD,CA4E/DiB,QAASA,EAAwB,CAACC,CAAD,CAAWC,CAAX,CAAwBnC,CAAxB,CAAgC,CAC/D,MAAO,UACK,KADL,UAEM,IAFN,MAGCG,QAAQ,CAACC,CAAD,CAAQC,CAAR,CAAkB,CAAA,IAC1Ba,EAAUlB,CAAAkB,QADgB,CAE1BD,EAASC,CAAAD,OAEbZ,EAAA+B,KAAA,CAAcnB,CAAAG,UAAd,CAEA,KAAIjB,EAAO+B,CAAA,CAAS7B,CAAAgC,SAAA,EAAT,CAEPnB,EAAAoB,WAAJ,GACErB,CAAAsB,OAMA,CANgBnC,CAMhB,CALIkC,CAKJ,CALiBH,CAAA,CAAYjB,CAAAoB,WAAZ,CAAgCrB,CAAhC,CAKjB,CAJIC,CAAAsB,aAIJ,GAHEpC,CAAA,CAAMc,CAAAsB,aAAN,CAGF,CAHgCF,CAGhC,EADAjC,CAAAoC,KAAA,CAAc,yBAAd,CAAyCH,CAAzC,CACA,CAAAjC,CAAAqC,SAAA,EAAAD,KAAA,CAAyB,yBAAzB,CAAoDH,CAApD,CAPF,CAUAnC,EAAA,CAAKC,CAAL,CAlB8B,CAH3B,CADwD,CA32B7DuC,CAAAA,CAAgB9C,CAAA+C,OAAA,CAAe,SAAf,CAA0B,CAAC,IAAD,CAA1B,CAAAC,SAAA,CACa,QADb,CAkBpBC,QAAuB,EAAE,CACvBC,QAASA,EAAO,CAACC,CAAD,CAASC,CAAT,CAAgB,CAC9B,MAAOpD,EAAAqD,OAAA,CAAe,KAAKrD,CAAAqD,OAAA,CAAe,QAAQ,EAAG,EAA1B;AAA8B,WAAWF,CAAX,CAA9B,CAAL,CAAf,CAA0EC,CAA1E,CADuB,CA0IhCE,QAASA,EAAU,CAACC,CAAD,CAAOC,CAAP,CAAa,CAAA,IAC1BC,EAAcD,CAAAE,qBADY,CAE1BC,EAAM,cACUJ,CADV,QAEIA,CAFJ,CAFoB,CAM1BK,EAAOD,CAAAC,KAAPA,CAAkB,EAEtBL,EAAA,CAAOA,CAAAM,QAAA,CACI,UADJ,CACgB,MADhB,CAAAA,QAAA,CAEI,uBAFJ,CAE6B,QAAQ,CAACC,CAAD,CAAIC,CAAJ,CAAWC,CAAX,CAAgBC,CAAhB,CAAuB,CAC3DC,CAAAA,CAAsB,GAAX,GAAAD,CAAA,CAAiBA,CAAjB,CAA0B,IACrCE,EAAAA,CAAkB,GAAX,GAAAF,CAAA,CAAiBA,CAAjB,CAA0B,IACrCL,EAAAQ,KAAA,CAAU,MAAQJ,CAAR,UAAuB,CAAC,CAACE,CAAzB,CAAV,CACAH,EAAA,CAAQA,CAAR,EAAiB,EACjB,OAAO,EAAP,EACKG,CAAA,CAAW,EAAX,CAAgBH,CADrB,EAEI,KAFJ,EAGKG,CAAA,CAAWH,CAAX,CAAmB,EAHxB,GAIKI,CAJL,EAIa,OAJb,EAIwB,SAJxB,GAKKD,CALL,EAKiB,EALjB,EAMI,GANJ,EAOKA,CAPL,EAOiB,EAPjB,CAL+D,CAF5D,CAAAL,QAAA,CAgBI,YAhBJ,CAgBkB,MAhBlB,CAkBPF,EAAAU,OAAA,CAAiBC,MAAJ,CAAW,GAAX,CAAiBf,CAAjB,CAAwB,GAAxB,CAA6BE,CAAA,CAAc,GAAd,CAAoB,EAAjD,CACb,OAAOE,EA3BuB,CAtIhC,IAAIY,EAAS,EAqGb,KAAAC,KAAA,CAAYC,QAAQ,CAAClB,CAAD,CAAOmB,CAAP,CAAc,CAChCH,CAAA,CAAOhB,CAAP,CAAA,CAAevD,CAAAqD,OAAA,CACb,gBAAiB,CAAA,CAAjB,CADa,CAEbqB,CAFa,CAGbnB,CAHa,EAGLD,CAAA,CAAWC,CAAX,CAAiBmB,CAAjB,CAHK,CAOf,IAAInB,CAAJ,CAAU,CACR,IAAIoB;AAAuC,GACxB,EADCpB,CAAA,CAAKA,CAAAqB,OAAL,CAAiB,CAAjB,CACD,CAAXrB,CAAAsB,OAAA,CAAY,CAAZ,CAAetB,CAAAqB,OAAf,CAA2B,CAA3B,CAAW,CACXrB,CADW,CACL,GAEdgB,EAAA,CAAOI,CAAP,CAAA,CAAuB3E,CAAAqD,OAAA,CACrB,YAAaE,CAAb,CADqB,CAErBD,CAAA,CAAWqB,CAAX,CAAyBD,CAAzB,CAFqB,CALf,CAWV,MAAO,KAnByB,CA0ElC,KAAAI,UAAA,CAAiBC,QAAQ,CAACC,CAAD,CAAS,CAChC,IAAAR,KAAA,CAAU,IAAV,CAAgBQ,CAAhB,CACA,OAAO,KAFyB,CAMlC,KAAAC,KAAA,CAAY,CAAC,YAAD,CACC,WADD,CAEC,cAFD,CAGC,IAHD,CAIC,WAJD,CAKC,OALD,CAMC,gBAND,CAOC,MAPD,CAQR,QAAQ,CAACC,CAAD,CAAaC,CAAb,CAAwBC,CAAxB,CAAsCC,CAAtC,CAA0CC,CAA1C,CAAqDC,CAArD,CAA4DC,CAA5D,CAA4EC,CAA5E,CAAkF,CA2P5FC,QAASA,EAAW,EAAG,CAAA,IACjBC,EAAOC,CAAA,EADU,CAEjBC,EAAO1F,CAAAkB,QAEX,IAAIsE,CAAJ,EAAYE,CAAZ,EAAoBF,CAAAG,QAApB,GAAqCD,CAAAC,QAArC,EACO9F,CAAA+F,OAAA,CAAeJ,CAAAK,WAAf,CAAgCH,CAAAG,WAAhC,CADP,EAEO,CAACL,CAAAM,eAFR,EAE+B,CAACC,CAFhC,CAGEL,CAAAb,OAEA,CAFcW,CAAAX,OAEd,CADAhF,CAAAmG,KAAA,CAAaN,CAAAb,OAAb,CAA0BI,CAA1B,CACA,CAAAF,CAAAkB,WAAA,CAAsB,cAAtB,CAAsCP,CAAtC,CALF,KAMO,IAAIF,CAAJ,EAAYE,CAAZ,CACLK,CAeA,CAfc,CAAA,CAed,CAdAhB,CAAAkB,WAAA,CAAsB,mBAAtB;AAA2CT,CAA3C,CAAiDE,CAAjD,CAcA,EAbA1F,CAAAkB,QAaA,CAbiBsE,CAajB,GAXMA,CAAAU,WAWN,GAVQrG,CAAAsG,SAAA,CAAiBX,CAAAU,WAAjB,CAAJ,CACElB,CAAA5B,KAAA,CAAegD,CAAA,CAAYZ,CAAAU,WAAZ,CAA6BV,CAAAX,OAA7B,CAAf,CAAAwB,OAAA,CAAiEb,CAAAX,OAAjE,CAAAnB,QAAA,EADF,CAIEsB,CAAAsB,IAAA,CAAcd,CAAAU,WAAA,CAAgBV,CAAAK,WAAhB,CAAiCb,CAAA5B,KAAA,EAAjC,CAAmD4B,CAAAqB,OAAA,EAAnD,CAAd,CAAA3C,QAAA,EAMN,EAAAwB,CAAAb,KAAA,CAAQmB,CAAR,CAAAe,KAAA,CACO,QAAQ,EAAG,CACd,GAAIf,CAAJ,CAAU,CAAA,IACJvE,EAASpB,CAAAqD,OAAA,CAAe,EAAf,CAAmBsC,CAAAgB,QAAnB,CADL,CAEJC,CAFI,CAEMC,CAEd7G,EAAA8G,QAAA,CAAgB1F,CAAhB,CAAwB,QAAQ,CAAC2F,CAAD,CAAQ/C,CAAR,CAAa,CAC3C5C,CAAA,CAAO4C,CAAP,CAAA,CAAchE,CAAAsG,SAAA,CAAiBS,CAAjB,CAAA,CACVzB,CAAA0B,IAAA,CAAcD,CAAd,CADU,CACazB,CAAA2B,OAAA,CAAiBF,CAAjB,CAFgB,CAA7C,CAKI/G,EAAAsB,UAAA,CAAkBsF,CAAlB,CAA6BjB,CAAAiB,SAA7B,CAAJ,CACM5G,CAAAkH,WAAA,CAAmBN,CAAnB,CADN,GAEIA,CAFJ,CAEeA,CAAA,CAASjB,CAAAX,OAAT,CAFf,EAIWhF,CAAAsB,UAAA,CAAkBuF,CAAlB,CAAgClB,CAAAkB,YAAhC,CAJX,GAKM7G,CAAAkH,WAAA,CAAmBL,CAAnB,CAIJ,GAHEA,CAGF,CAHgBA,CAAA,CAAYlB,CAAAX,OAAZ,CAGhB,EADA6B,CACA,CADcpB,CAAA0B,sBAAA,CAA2BN,CAA3B,CACd,CAAI7G,CAAAsB,UAAA,CAAkBuF,CAAlB,CAAJ,GACElB,CAAAyB,kBACA;AADyBP,CACzB,CAAAD,CAAA,CAAWrB,CAAAyB,IAAA,CAAUH,CAAV,CAAuB,OAAQrB,CAAR,CAAvB,CAAAkB,KAAA,CACF,QAAQ,CAACW,CAAD,CAAW,CAAE,MAAOA,EAAAzE,KAAT,CADjB,CAFb,CATF,CAeI5C,EAAAsB,UAAA,CAAkBsF,CAAlB,CAAJ,GACExF,CAAA,UADF,CACwBwF,CADxB,CAGA,OAAOvB,EAAAiC,IAAA,CAAOlG,CAAP,CA3BC,CADI,CADlB,CAAAsF,KAAA,CAiCO,QAAQ,CAACtF,CAAD,CAAS,CAChBuE,CAAJ,EAAYxF,CAAAkB,QAAZ,GACMsE,CAIJ,GAHEA,CAAAvE,OACA,CADcA,CACd,CAAApB,CAAAmG,KAAA,CAAaR,CAAAX,OAAb,CAA0BI,CAA1B,CAEF,EAAAF,CAAAkB,WAAA,CAAsB,qBAAtB,CAA6CT,CAA7C,CAAmDE,CAAnD,CALF,CADoB,CAjCxB,CAyCK,QAAQ,CAAC0B,CAAD,CAAQ,CACb5B,CAAJ,EAAYxF,CAAAkB,QAAZ,EACE6D,CAAAkB,WAAA,CAAsB,mBAAtB,CAA2CT,CAA3C,CAAiDE,CAAjD,CAAuD0B,CAAvD,CAFe,CAzCrB,CA1BmB,CA+EvB3B,QAASA,EAAU,EAAG,CAAA,IAEhBZ,CAFgB,CAERwC,CACZxH,EAAA8G,QAAA,CAAgBvC,CAAhB,CAAwB,QAAQ,CAACG,CAAD,CAAQnB,CAAR,CAAc,CACxC,IAAA,CAAA,IAAA,CAAA,CAAA,CAAA,CAAA,CAAA,CAAW,IAAA,EAAA,CAAA,KAAA,EAzGbK,EAAAA,CAyGac,CAzGNd,KAAX,KACIoB,EAAS,EAEb,IAsGiBN,CAtGZL,OAAL,CAGA,GADIoD,CACJ,CAmGiB/C,CApGTL,OAAAqD,KAAA,CAAkBC,CAAlB,CACR,CAAA,CAEA,IATqC,IAS5BC,EAAI,CATwB,CASrBC,EAAMJ,CAAA7C,OAAtB,CAAgCgD,CAAhC,CAAoCC,CAApC,CAAyC,EAAED,CAA3C,CAA8C,CAC5C,IAAI5D,EAAMJ,CAAA,CAAKgE,CAAL,CAAS,CAAT,CAAV,CAEIE,EAAM,QACA,EADY,MAAOL,EAAA,CAAEG,CAAF,CACnB,CAAFG,kBAAA,CAAmBN,CAAA,CAAEG,CAAF,CAAnB,CAAE;AACFH,CAAA,CAAEG,CAAF,CAEJ5D,EAAJ,EAAW8D,CAAX,GACE9C,CAAA,CAAOhB,CAAAgE,KAAP,CADF,CACqBF,CADrB,CAP4C,CAW9C,CAAA,CAAO9C,CAbP,CAAA,IAAQ,EAAA,CAAO,IAHf,KAAmB,EAAA,CAAO,IAsGT,EAAA,CAAA,CAAA,CAAA,CAAX,CAAA,CAAJ,GACEwC,CAGA,CAHQtE,CAAA,CAAQwB,CAAR,CAAe,QACb1E,CAAAqD,OAAA,CAAe,EAAf,CAAmB8B,CAAAqB,OAAA,EAAnB,CAAuCxB,CAAvC,CADa,YAETA,CAFS,CAAf,CAGR,CAAAwC,CAAA1B,QAAA,CAAgBpB,CAJlB,CAD4C,CAA9C,CASA,OAAO8C,EAAP,EAAgBjD,CAAA,CAAO,IAAP,CAAhB,EAAgCrB,CAAA,CAAQqB,CAAA,CAAO,IAAP,CAAR,CAAsB,QAAS,EAAT,YAAwB,EAAxB,CAAtB,CAZZ,CAkBtBgC,QAASA,EAAW,CAAC0B,CAAD,CAASjD,CAAT,CAAiB,CACnC,IAAIkD,EAAS,EACblI,EAAA8G,QAAA,CAAiBqB,CAAAF,CAAAE,EAAQ,EAARA,OAAA,CAAkB,GAAlB,CAAjB,CAAyC,QAAQ,CAACC,CAAD,CAAUR,CAAV,CAAa,CAC5D,GAAU,CAAV,GAAIA,CAAJ,CACEM,CAAA9D,KAAA,CAAYgE,CAAZ,CADF,KAEO,CACL,IAAIC,EAAeD,CAAAZ,MAAA,CAAc,WAAd,CAAnB,CACIxD,EAAMqE,CAAA,CAAa,CAAb,CACVH,EAAA9D,KAAA,CAAYY,CAAA,CAAOhB,CAAP,CAAZ,CACAkE,EAAA9D,KAAA,CAAYiE,CAAA,CAAa,CAAb,CAAZ,EAA+B,EAA/B,CACA,QAAOrD,CAAA,CAAOhB,CAAP,CALF,CAHqD,CAA9D,CAWA,OAAOkE,EAAAI,KAAA,CAAY,EAAZ,CAb4B,CA5VuD,IA8LxFpC,EAAc,CAAA,CA9L0E,CA+LxF/F,EAAS,QACCoE,CADD,QAcCgE,QAAQ,EAAG,CACjBrC,CAAA,CAAc,CAAA,CACdhB,EAAAsD,WAAA,CAAsB9C,CAAtB,CAFiB,CAdZ,CAoBbR,EAAA/C,IAAA,CAAe,wBAAf,CAAyCuD,CAAzC,CAEA,OAAOvF,EArNqF,CARlF,CA1LW,CAlBL,CAkkBpB2C,EAAAE,SAAA,CAAuB,cAAvB;AAoCAyF,QAA6B,EAAG,CAC9B,IAAAxD,KAAA,CAAYyD,QAAQ,EAAG,CAAE,MAAO,EAAT,CADO,CApChC,CAwCA5F,EAAA6F,UAAA,CAAwB,QAAxB,CAAkCzI,CAAlC,CACA4C,EAAA6F,UAAA,CAAwB,QAAxB,CAAkCvG,CAAlC,CAmLAlC,EAAA0I,QAAA,CAAwB,CAAC,QAAD,CAAW,eAAX,CAA4B,UAA5B,CA4ExBxG,EAAAwG,QAAA,CAAmC,CAAC,UAAD,CAAa,aAAb,CAA4B,QAA5B,CA53BG,CAArC,CAAA,CAy5BE7I,MAz5BF,CAy5BUA,MAAAC,QAz5BV;",
"sources":["angular-route.js"],
"names":["window","angular","undefined","ngViewFactory","$route","$anchorScroll","$animate","link","scope","$element","attr","ctrl","$transclude","cleanupLastView","previousElement","remove","currentScope","$destroy","currentElement","leave","update","locals","current","isDefined","$template","newScope","$new","clone","enter","onNgViewEnter","autoScrollExp","$eval","$emit","onloadExp","autoscroll","onload","$on","ngViewFillContentFactory","$compile","$controller","html","contents","controller","$scope","controllerAs","data","children","ngRouteModule","module","provider","$RouteProvider","inherit","parent","extra","extend","pathRegExp","path","opts","insensitive","caseInsensitiveMatch","ret","keys","replace","_","slash","key","option","optional","star","push","regexp","RegExp","routes","when","this.when","route","redirectPath","length","substr","otherwise","this.otherwise","params","$get","$rootScope","$location","$routeParams","$q","$injector","$http","$templateCache","$sce","updateRoute","next","parseRoute","last","$$route","equals","pathParams","reloadOnSearch","forceReload","copy","$broadcast","redirectTo","isString","interpolate","search","url","then","resolve","template","templateUrl","forEach","value","get","invoke","isFunction","getTrustedResourceUrl","loadedTemplateUrl","response","all","error","match","m","exec","on","i","len","val","decodeURIComponent","name","string","result","split","segment","segmentMatch","join","reload","$evalAsync","$RouteParamsProvider","this.$get","directive","$inject"]
}
;
/*
 AngularJS v1.2.17
 (c) 2010-2014 Google, Inc. http://angularjs.org
 License: MIT
*/

(function(P,V,s){'use strict';function u(b){return function(){var a=arguments[0],c,a="["+(b?b+":":"")+a+"] http://errors.angularjs.org/1.2.17/"+(b?b+"/":"")+a;for(c=1;c<arguments.length;c++)a=a+(1==c?"?":"&")+"p"+(c-1)+"="+encodeURIComponent("function"==typeof arguments[c]?arguments[c].toString().replace(/ \{[\s\S]*$/,""):"undefined"==typeof arguments[c]?"undefined":"string"!=typeof arguments[c]?JSON.stringify(arguments[c]):arguments[c]);return Error(a)}}function db(b){if(null==b||Da(b))return!1;
var a=b.length;return 1===b.nodeType&&a?!0:C(b)||K(b)||0===a||"number"===typeof a&&0<a&&a-1 in b}function q(b,a,c){var d;if(b)if(Q(b))for(d in b)"prototype"==d||("length"==d||"name"==d||b.hasOwnProperty&&!b.hasOwnProperty(d))||a.call(c,b[d],d);else if(b.forEach&&b.forEach!==q)b.forEach(a,c);else if(db(b))for(d=0;d<b.length;d++)a.call(c,b[d],d);else for(d in b)b.hasOwnProperty(d)&&a.call(c,b[d],d);return b}function Wb(b){var a=[],c;for(c in b)b.hasOwnProperty(c)&&a.push(c);return a.sort()}function Sc(b,
a,c){for(var d=Wb(b),e=0;e<d.length;e++)a.call(c,b[d[e]],d[e]);return d}function Xb(b){return function(a,c){b(c,a)}}function eb(){for(var b=ja.length,a;b;){b--;a=ja[b].charCodeAt(0);if(57==a)return ja[b]="A",ja.join("");if(90==a)ja[b]="0";else return ja[b]=String.fromCharCode(a+1),ja.join("")}ja.unshift("0");return ja.join("")}function Yb(b,a){a?b.$$hashKey=a:delete b.$$hashKey}function E(b){var a=b.$$hashKey;q(arguments,function(a){a!==b&&q(a,function(a,c){b[c]=a})});Yb(b,a);return b}function Y(b){return parseInt(b,
10)}function Zb(b,a){return E(new (E(function(){},{prototype:b})),a)}function w(){}function Ea(b){return b}function aa(b){return function(){return b}}function H(b){return"undefined"===typeof b}function z(b){return"undefined"!==typeof b}function U(b){return null!=b&&"object"===typeof b}function C(b){return"string"===typeof b}function zb(b){return"number"===typeof b}function Ma(b){return"[object Date]"===wa.call(b)}function K(b){return"[object Array]"===wa.call(b)}function Q(b){return"function"===typeof b}
function fb(b){return"[object RegExp]"===wa.call(b)}function Da(b){return b&&b.document&&b.location&&b.alert&&b.setInterval}function Tc(b){return!(!b||!(b.nodeName||b.prop&&b.attr&&b.find))}function Uc(b,a,c){var d=[];q(b,function(b,f,g){d.push(a.call(c,b,f,g))});return d}function Na(b,a){if(b.indexOf)return b.indexOf(a);for(var c=0;c<b.length;c++)if(a===b[c])return c;return-1}function Oa(b,a){var c=Na(b,a);0<=c&&b.splice(c,1);return a}function Fa(b,a,c,d){if(Da(b)||b&&b.$evalAsync&&b.$watch)throw Pa("cpws");
if(a){if(b===a)throw Pa("cpi");c=c||[];d=d||[];if(U(b)){var e=Na(c,b);if(-1!==e)return d[e];c.push(b);d.push(a)}if(K(b))for(var f=a.length=0;f<b.length;f++)e=Fa(b[f],null,c,d),U(b[f])&&(c.push(b[f]),d.push(e)),a.push(e);else{var g=a.$$hashKey;q(a,function(b,c){delete a[c]});for(f in b)e=Fa(b[f],null,c,d),U(b[f])&&(c.push(b[f]),d.push(e)),a[f]=e;Yb(a,g)}}else(a=b)&&(K(b)?a=Fa(b,[],c,d):Ma(b)?a=new Date(b.getTime()):fb(b)?a=RegExp(b.source):U(b)&&(a=Fa(b,{},c,d)));return a}function ka(b,a){if(K(b)){a=
a||[];for(var c=0;c<b.length;c++)a[c]=b[c]}else if(U(b))for(c in a=a||{},b)!Ab.call(b,c)||"$"===c.charAt(0)&&"$"===c.charAt(1)||(a[c]=b[c]);return a||b}function xa(b,a){if(b===a)return!0;if(null===b||null===a)return!1;if(b!==b&&a!==a)return!0;var c=typeof b,d;if(c==typeof a&&"object"==c)if(K(b)){if(!K(a))return!1;if((c=b.length)==a.length){for(d=0;d<c;d++)if(!xa(b[d],a[d]))return!1;return!0}}else{if(Ma(b))return Ma(a)&&b.getTime()==a.getTime();if(fb(b)&&fb(a))return b.toString()==a.toString();if(b&&
b.$evalAsync&&b.$watch||a&&a.$evalAsync&&a.$watch||Da(b)||Da(a)||K(a))return!1;c={};for(d in b)if("$"!==d.charAt(0)&&!Q(b[d])){if(!xa(b[d],a[d]))return!1;c[d]=!0}for(d in a)if(!c.hasOwnProperty(d)&&"$"!==d.charAt(0)&&a[d]!==s&&!Q(a[d]))return!1;return!0}return!1}function $b(){return V.securityPolicy&&V.securityPolicy.isActive||V.querySelector&&!(!V.querySelector("[ng-csp]")&&!V.querySelector("[data-ng-csp]"))}function gb(b,a){var c=2<arguments.length?ya.call(arguments,2):[];return!Q(a)||a instanceof
RegExp?a:c.length?function(){return arguments.length?a.apply(b,c.concat(ya.call(arguments,0))):a.apply(b,c)}:function(){return arguments.length?a.apply(b,arguments):a.call(b)}}function Vc(b,a){var c=a;"string"===typeof b&&"$"===b.charAt(0)?c=s:Da(a)?c="$WINDOW":a&&V===a?c="$DOCUMENT":a&&(a.$evalAsync&&a.$watch)&&(c="$SCOPE");return c}function qa(b,a){return"undefined"===typeof b?s:JSON.stringify(b,Vc,a?"  ":null)}function ac(b){return C(b)?JSON.parse(b):b}function Qa(b){"function"===typeof b?b=!0:
b&&0!==b.length?(b=J(""+b),b=!("f"==b||"0"==b||"false"==b||"no"==b||"n"==b||"[]"==b)):b=!1;return b}function ha(b){b=y(b).clone();try{b.empty()}catch(a){}var c=y("<div>").append(b).html();try{return 3===b[0].nodeType?J(c):c.match(/^(<[^>]+>)/)[1].replace(/^<([\w\-]+)/,function(a,b){return"<"+J(b)})}catch(d){return J(c)}}function bc(b){try{return decodeURIComponent(b)}catch(a){}}function cc(b){var a={},c,d;q((b||"").split("&"),function(b){b&&(c=b.split("="),d=bc(c[0]),z(d)&&(b=z(c[1])?bc(c[1]):!0,
a[d]?K(a[d])?a[d].push(b):a[d]=[a[d],b]:a[d]=b))});return a}function Bb(b){var a=[];q(b,function(b,d){K(b)?q(b,function(b){a.push(za(d,!0)+(!0===b?"":"="+za(b,!0)))}):a.push(za(d,!0)+(!0===b?"":"="+za(b,!0)))});return a.length?a.join("&"):""}function hb(b){return za(b,!0).replace(/%26/gi,"&").replace(/%3D/gi,"=").replace(/%2B/gi,"+")}function za(b,a){return encodeURIComponent(b).replace(/%40/gi,"@").replace(/%3A/gi,":").replace(/%24/g,"$").replace(/%2C/gi,",").replace(/%20/g,a?"%20":"+")}function Wc(b,
a){function c(a){a&&d.push(a)}var d=[b],e,f,g=["ng:app","ng-app","x-ng-app","data-ng-app"],h=/\sng[:\-]app(:\s*([\w\d_]+);?)?\s/;q(g,function(a){g[a]=!0;c(V.getElementById(a));a=a.replace(":","\\:");b.querySelectorAll&&(q(b.querySelectorAll("."+a),c),q(b.querySelectorAll("."+a+"\\:"),c),q(b.querySelectorAll("["+a+"]"),c))});q(d,function(a){if(!e){var b=h.exec(" "+a.className+" ");b?(e=a,f=(b[2]||"").replace(/\s+/g,",")):q(a.attributes,function(b){!e&&g[b.name]&&(e=a,f=b.value)})}});e&&a(e,f?[f]:[])}
function dc(b,a){var c=function(){b=y(b);if(b.injector()){var c=b[0]===V?"document":ha(b);throw Pa("btstrpd",c);}a=a||[];a.unshift(["$provide",function(a){a.value("$rootElement",b)}]);a.unshift("ng");c=ec(a);c.invoke(["$rootScope","$rootElement","$compile","$injector","$animate",function(a,b,c,d,e){a.$apply(function(){b.data("$injector",d);c(b)(a)})}]);return c},d=/^NG_DEFER_BOOTSTRAP!/;if(P&&!d.test(P.name))return c();P.name=P.name.replace(d,"");Ra.resumeBootstrap=function(b){q(b,function(b){a.push(b)});
c()}}function ib(b,a){a=a||"_";return b.replace(Xc,function(b,d){return(d?a:"")+b.toLowerCase()})}function Cb(b,a,c){if(!b)throw Pa("areq",a||"?",c||"required");return b}function Sa(b,a,c){c&&K(b)&&(b=b[b.length-1]);Cb(Q(b),a,"not a function, got "+(b&&"object"==typeof b?b.constructor.name||"Object":typeof b));return b}function Aa(b,a){if("hasOwnProperty"===b)throw Pa("badname",a);}function fc(b,a,c){if(!a)return b;a=a.split(".");for(var d,e=b,f=a.length,g=0;g<f;g++)d=a[g],b&&(b=(e=b)[d]);return!c&&
Q(b)?gb(e,b):b}function Db(b){var a=b[0];b=b[b.length-1];if(a===b)return y(a);var c=[a];do{a=a.nextSibling;if(!a)break;c.push(a)}while(a!==b);return y(c)}function Yc(b){var a=u("$injector"),c=u("ng");b=b.angular||(b.angular={});b.$$minErr=b.$$minErr||u;return b.module||(b.module=function(){var b={};return function(e,f,g){if("hasOwnProperty"===e)throw c("badname","module");f&&b.hasOwnProperty(e)&&(b[e]=null);return b[e]||(b[e]=function(){function b(a,d,e){return function(){c[e||"push"]([a,d,arguments]);
return n}}if(!f)throw a("nomod",e);var c=[],d=[],l=b("$injector","invoke"),n={_invokeQueue:c,_runBlocks:d,requires:f,name:e,provider:b("$provide","provider"),factory:b("$provide","factory"),service:b("$provide","service"),value:b("$provide","value"),constant:b("$provide","constant","unshift"),animation:b("$animateProvider","register"),filter:b("$filterProvider","register"),controller:b("$controllerProvider","register"),directive:b("$compileProvider","directive"),config:l,run:function(a){d.push(a);
return this}};g&&l(g);return n}())}}())}function Zc(b){E(b,{bootstrap:dc,copy:Fa,extend:E,equals:xa,element:y,forEach:q,injector:ec,noop:w,bind:gb,toJson:qa,fromJson:ac,identity:Ea,isUndefined:H,isDefined:z,isString:C,isFunction:Q,isObject:U,isNumber:zb,isElement:Tc,isArray:K,version:$c,isDate:Ma,lowercase:J,uppercase:Ga,callbacks:{counter:0},$$minErr:u,$$csp:$b});Ta=Yc(P);try{Ta("ngLocale")}catch(a){Ta("ngLocale",[]).provider("$locale",ad)}Ta("ng",["ngLocale"],["$provide",function(a){a.provider({$$sanitizeUri:bd});
a.provider("$compile",gc).directive({a:cd,input:hc,textarea:hc,form:dd,script:ed,select:fd,style:gd,option:hd,ngBind:id,ngBindHtml:jd,ngBindTemplate:kd,ngClass:ld,ngClassEven:md,ngClassOdd:nd,ngCloak:od,ngController:pd,ngForm:qd,ngHide:rd,ngIf:sd,ngInclude:td,ngInit:ud,ngNonBindable:vd,ngPluralize:wd,ngRepeat:xd,ngShow:yd,ngStyle:zd,ngSwitch:Ad,ngSwitchWhen:Bd,ngSwitchDefault:Cd,ngOptions:Dd,ngTransclude:Ed,ngModel:Fd,ngList:Gd,ngChange:Hd,required:ic,ngRequired:ic,ngValue:Id}).directive({ngInclude:Jd}).directive(Eb).directive(jc);
a.provider({$anchorScroll:Kd,$animate:Ld,$browser:Md,$cacheFactory:Nd,$controller:Od,$document:Pd,$exceptionHandler:Qd,$filter:kc,$interpolate:Rd,$interval:Sd,$http:Td,$httpBackend:Ud,$location:Vd,$log:Wd,$parse:Xd,$rootScope:Yd,$q:Zd,$sce:$d,$sceDelegate:ae,$sniffer:be,$templateCache:ce,$timeout:de,$window:ee,$$rAF:fe,$$asyncCallback:ge})}])}function Ua(b){return b.replace(he,function(a,b,d,e){return e?d.toUpperCase():d}).replace(ie,"Moz$1")}function Fb(b,a,c,d){function e(b){var e=c&&b?[this.filter(b)]:
[this],m=a,k,l,n,p,r,A;if(!d||null!=b)for(;e.length;)for(k=e.shift(),l=0,n=k.length;l<n;l++)for(p=y(k[l]),m?p.triggerHandler("$destroy"):m=!m,r=0,p=(A=p.children()).length;r<p;r++)e.push(Ba(A[r]));return f.apply(this,arguments)}var f=Ba.fn[b],f=f.$original||f;e.$original=f;Ba.fn[b]=e}function M(b){if(b instanceof M)return b;C(b)&&(b=ba(b));if(!(this instanceof M)){if(C(b)&&"<"!=b.charAt(0))throw Gb("nosel");return new M(b)}if(C(b)){var a=b;b=V;var c;if(c=je.exec(a))b=[b.createElement(c[1])];else{var d=
b,e;b=d.createDocumentFragment();c=[];if(Hb.test(a)){d=b.appendChild(d.createElement("div"));e=(ke.exec(a)||["",""])[1].toLowerCase();e=da[e]||da._default;d.innerHTML="<div>&#160;</div>"+e[1]+a.replace(le,"<$1></$2>")+e[2];d.removeChild(d.firstChild);for(a=e[0];a--;)d=d.lastChild;a=0;for(e=d.childNodes.length;a<e;++a)c.push(d.childNodes[a]);d=b.firstChild;d.textContent=""}else c.push(d.createTextNode(a));b.textContent="";b.innerHTML="";b=c}Ib(this,b);y(V.createDocumentFragment()).append(this)}else Ib(this,
b)}function Jb(b){return b.cloneNode(!0)}function Ha(b){lc(b);var a=0;for(b=b.childNodes||[];a<b.length;a++)Ha(b[a])}function mc(b,a,c,d){if(z(d))throw Gb("offargs");var e=la(b,"events");la(b,"handle")&&(H(a)?q(e,function(a,c){Va(b,c,a);delete e[c]}):q(a.split(" "),function(a){H(c)?(Va(b,a,e[a]),delete e[a]):Oa(e[a]||[],c)}))}function lc(b,a){var c=b[jb],d=Wa[c];d&&(a?delete Wa[c].data[a]:(d.handle&&(d.events.$destroy&&d.handle({},"$destroy"),mc(b)),delete Wa[c],b[jb]=s))}function la(b,a,c){var d=
b[jb],d=Wa[d||-1];if(z(c))d||(b[jb]=d=++me,d=Wa[d]={}),d[a]=c;else return d&&d[a]}function nc(b,a,c){var d=la(b,"data"),e=z(c),f=!e&&z(a),g=f&&!U(a);d||g||la(b,"data",d={});if(e)d[a]=c;else if(f){if(g)return d&&d[a];E(d,a)}else return d}function Kb(b,a){return b.getAttribute?-1<(" "+(b.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ").indexOf(" "+a+" "):!1}function kb(b,a){a&&b.setAttribute&&q(a.split(" "),function(a){b.setAttribute("class",ba((" "+(b.getAttribute("class")||"")+" ").replace(/[\n\t]/g,
" ").replace(" "+ba(a)+" "," ")))})}function lb(b,a){if(a&&b.setAttribute){var c=(" "+(b.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ");q(a.split(" "),function(a){a=ba(a);-1===c.indexOf(" "+a+" ")&&(c+=a+" ")});b.setAttribute("class",ba(c))}}function Ib(b,a){if(a){a=a.nodeName||!z(a.length)||Da(a)?[a]:a;for(var c=0;c<a.length;c++)b.push(a[c])}}function oc(b,a){return mb(b,"$"+(a||"ngController")+"Controller")}function mb(b,a,c){b=y(b);9==b[0].nodeType&&(b=b.find("html"));for(a=K(a)?a:[a];b.length;){for(var d=
b[0],e=0,f=a.length;e<f;e++)if((c=b.data(a[e]))!==s)return c;b=y(d.parentNode||11===d.nodeType&&d.host)}}function pc(b){for(var a=0,c=b.childNodes;a<c.length;a++)Ha(c[a]);for(;b.firstChild;)b.removeChild(b.firstChild)}function qc(b,a){var c=nb[a.toLowerCase()];return c&&rc[b.nodeName]&&c}function ne(b,a){var c=function(c,e){c.preventDefault||(c.preventDefault=function(){c.returnValue=!1});c.stopPropagation||(c.stopPropagation=function(){c.cancelBubble=!0});c.target||(c.target=c.srcElement||V);if(H(c.defaultPrevented)){var f=
c.preventDefault;c.preventDefault=function(){c.defaultPrevented=!0;f.call(c)};c.defaultPrevented=!1}c.isDefaultPrevented=function(){return c.defaultPrevented||!1===c.returnValue};var g=ka(a[e||c.type]||[]);q(g,function(a){a.call(b,c)});8>=S?(c.preventDefault=null,c.stopPropagation=null,c.isDefaultPrevented=null):(delete c.preventDefault,delete c.stopPropagation,delete c.isDefaultPrevented)};c.elem=b;return c}function Ia(b){var a=typeof b,c;"object"==a&&null!==b?"function"==typeof(c=b.$$hashKey)?c=
b.$$hashKey():c===s&&(c=b.$$hashKey=eb()):c=b;return a+":"+c}function Xa(b){q(b,this.put,this)}function sc(b){var a,c;"function"==typeof b?(a=b.$inject)||(a=[],b.length&&(c=b.toString().replace(oe,""),c=c.match(pe),q(c[1].split(qe),function(b){b.replace(re,function(b,c,d){a.push(d)})})),b.$inject=a):K(b)?(c=b.length-1,Sa(b[c],"fn"),a=b.slice(0,c)):Sa(b,"fn",!0);return a}function ec(b){function a(a){return function(b,c){if(U(b))q(b,Xb(a));else return a(b,c)}}function c(a,b){Aa(a,"service");if(Q(b)||
K(b))b=n.instantiate(b);if(!b.$get)throw Ya("pget",a);return l[a+h]=b}function d(a,b){return c(a,{$get:b})}function e(a){var b=[],c,d,f,h;q(a,function(a){if(!k.get(a)){k.put(a,!0);try{if(C(a))for(c=Ta(a),b=b.concat(e(c.requires)).concat(c._runBlocks),d=c._invokeQueue,f=0,h=d.length;f<h;f++){var g=d[f],m=n.get(g[0]);m[g[1]].apply(m,g[2])}else Q(a)?b.push(n.invoke(a)):K(a)?b.push(n.invoke(a)):Sa(a,"module")}catch(l){throw K(a)&&(a=a[a.length-1]),l.message&&(l.stack&&-1==l.stack.indexOf(l.message))&&
(l=l.message+"\n"+l.stack),Ya("modulerr",a,l.stack||l.message||l);}}});return b}function f(a,b){function c(d){if(a.hasOwnProperty(d)){if(a[d]===g)throw Ya("cdep",m.join(" <- "));return a[d]}try{return m.unshift(d),a[d]=g,a[d]=b(d)}catch(e){throw a[d]===g&&delete a[d],e;}finally{m.shift()}}function d(a,b,e){var f=[],h=sc(a),g,m,k;m=0;for(g=h.length;m<g;m++){k=h[m];if("string"!==typeof k)throw Ya("itkn",k);f.push(e&&e.hasOwnProperty(k)?e[k]:c(k))}a.$inject||(a=a[g]);return a.apply(b,f)}return{invoke:d,
instantiate:function(a,b){var c=function(){},e;c.prototype=(K(a)?a[a.length-1]:a).prototype;c=new c;e=d(a,c,b);return U(e)||Q(e)?e:c},get:c,annotate:sc,has:function(b){return l.hasOwnProperty(b+h)||a.hasOwnProperty(b)}}}var g={},h="Provider",m=[],k=new Xa,l={$provide:{provider:a(c),factory:a(d),service:a(function(a,b){return d(a,["$injector",function(a){return a.instantiate(b)}])}),value:a(function(a,b){return d(a,aa(b))}),constant:a(function(a,b){Aa(a,"constant");l[a]=b;p[a]=b}),decorator:function(a,
b){var c=n.get(a+h),d=c.$get;c.$get=function(){var a=r.invoke(d,c);return r.invoke(b,null,{$delegate:a})}}}},n=l.$injector=f(l,function(){throw Ya("unpr",m.join(" <- "));}),p={},r=p.$injector=f(p,function(a){a=n.get(a+h);return r.invoke(a.$get,a)});q(e(b),function(a){r.invoke(a||w)});return r}function Kd(){var b=!0;this.disableAutoScrolling=function(){b=!1};this.$get=["$window","$location","$rootScope",function(a,c,d){function e(a){var b=null;q(a,function(a){b||"a"!==J(a.nodeName)||(b=a)});return b}
function f(){var b=c.hash(),d;b?(d=g.getElementById(b))?d.scrollIntoView():(d=e(g.getElementsByName(b)))?d.scrollIntoView():"top"===b&&a.scrollTo(0,0):a.scrollTo(0,0)}var g=a.document;b&&d.$watch(function(){return c.hash()},function(){d.$evalAsync(f)});return f}]}function ge(){this.$get=["$$rAF","$timeout",function(b,a){return b.supported?function(a){return b(a)}:function(b){return a(b,0,!1)}}]}function se(b,a,c,d){function e(a){try{a.apply(null,ya.call(arguments,1))}finally{if(A--,0===A)for(;D.length;)try{D.pop()()}catch(b){c.error(b)}}}
function f(a,b){(function T(){q(x,function(a){a()});t=b(T,a)})()}function g(){v=null;N!=h.url()&&(N=h.url(),q(ma,function(a){a(h.url())}))}var h=this,m=a[0],k=b.location,l=b.history,n=b.setTimeout,p=b.clearTimeout,r={};h.isMock=!1;var A=0,D=[];h.$$completeOutstandingRequest=e;h.$$incOutstandingRequestCount=function(){A++};h.notifyWhenNoOutstandingRequests=function(a){q(x,function(a){a()});0===A?a():D.push(a)};var x=[],t;h.addPollFn=function(a){H(t)&&f(100,n);x.push(a);return a};var N=k.href,B=a.find("base"),
v=null;h.url=function(a,c){k!==b.location&&(k=b.location);l!==b.history&&(l=b.history);if(a){if(N!=a)return N=a,d.history?c?l.replaceState(null,"",a):(l.pushState(null,"",a),B.attr("href",B.attr("href"))):(v=a,c?k.replace(a):k.href=a),h}else return v||k.href.replace(/%27/g,"'")};var ma=[],L=!1;h.onUrlChange=function(a){if(!L){if(d.history)y(b).on("popstate",g);if(d.hashchange)y(b).on("hashchange",g);else h.addPollFn(g);L=!0}ma.push(a);return a};h.baseHref=function(){var a=B.attr("href");return a?
a.replace(/^(https?\:)?\/\/[^\/]*/,""):""};var O={},ca="",F=h.baseHref();h.cookies=function(a,b){var d,e,f,h;if(a)b===s?m.cookie=escape(a)+"=;path="+F+";expires=Thu, 01 Jan 1970 00:00:00 GMT":C(b)&&(d=(m.cookie=escape(a)+"="+escape(b)+";path="+F).length+1,4096<d&&c.warn("Cookie '"+a+"' possibly not set or overflowed because it was too large ("+d+" > 4096 bytes)!"));else{if(m.cookie!==ca)for(ca=m.cookie,d=ca.split("; "),O={},f=0;f<d.length;f++)e=d[f],h=e.indexOf("="),0<h&&(a=unescape(e.substring(0,
h)),O[a]===s&&(O[a]=unescape(e.substring(h+1))));return O}};h.defer=function(a,b){var c;A++;c=n(function(){delete r[c];e(a)},b||0);r[c]=!0;return c};h.defer.cancel=function(a){return r[a]?(delete r[a],p(a),e(w),!0):!1}}function Md(){this.$get=["$window","$log","$sniffer","$document",function(b,a,c,d){return new se(b,d,a,c)}]}function Nd(){this.$get=function(){function b(b,d){function e(a){a!=n&&(p?p==a&&(p=a.n):p=a,f(a.n,a.p),f(a,n),n=a,n.n=null)}function f(a,b){a!=b&&(a&&(a.p=b),b&&(b.n=a))}if(b in
a)throw u("$cacheFactory")("iid",b);var g=0,h=E({},d,{id:b}),m={},k=d&&d.capacity||Number.MAX_VALUE,l={},n=null,p=null;return a[b]={put:function(a,b){if(k<Number.MAX_VALUE){var c=l[a]||(l[a]={key:a});e(c)}if(!H(b))return a in m||g++,m[a]=b,g>k&&this.remove(p.key),b},get:function(a){if(k<Number.MAX_VALUE){var b=l[a];if(!b)return;e(b)}return m[a]},remove:function(a){if(k<Number.MAX_VALUE){var b=l[a];if(!b)return;b==n&&(n=b.p);b==p&&(p=b.n);f(b.n,b.p);delete l[a]}delete m[a];g--},removeAll:function(){m=
{};g=0;l={};n=p=null},destroy:function(){l=h=m=null;delete a[b]},info:function(){return E({},h,{size:g})}}}var a={};b.info=function(){var b={};q(a,function(a,e){b[e]=a.info()});return b};b.get=function(b){return a[b]};return b}}function ce(){this.$get=["$cacheFactory",function(b){return b("templates")}]}function gc(b,a){var c={},d="Directive",e=/^\s*directive\:\s*([\d\w_\-]+)\s+(.*)$/,f=/(([\d\w_\-]+)(?:\:([^;]+))?;?)/,g=/^(on[a-z]+|formaction)$/;this.directive=function m(a,e){Aa(a,"directive");C(a)?
(Cb(e,"directiveFactory"),c.hasOwnProperty(a)||(c[a]=[],b.factory(a+d,["$injector","$exceptionHandler",function(b,d){var e=[];q(c[a],function(c,f){try{var g=b.invoke(c);Q(g)?g={compile:aa(g)}:!g.compile&&g.link&&(g.compile=aa(g.link));g.priority=g.priority||0;g.index=f;g.name=g.name||a;g.require=g.require||g.controller&&g.name;g.restrict=g.restrict||"A";e.push(g)}catch(m){d(m)}});return e}])),c[a].push(e)):q(a,Xb(m));return this};this.aHrefSanitizationWhitelist=function(b){return z(b)?(a.aHrefSanitizationWhitelist(b),
this):a.aHrefSanitizationWhitelist()};this.imgSrcSanitizationWhitelist=function(b){return z(b)?(a.imgSrcSanitizationWhitelist(b),this):a.imgSrcSanitizationWhitelist()};this.$get=["$injector","$interpolate","$exceptionHandler","$http","$templateCache","$parse","$controller","$rootScope","$document","$sce","$animate","$$sanitizeUri",function(a,b,l,n,p,r,A,D,x,t,N,B){function v(a,b,c,d,e){a instanceof y||(a=y(a));q(a,function(b,c){3==b.nodeType&&b.nodeValue.match(/\S+/)&&(a[c]=y(b).wrap("<span></span>").parent()[0])});
var f=L(a,b,a,c,d,e);ma(a,"ng-scope");return function(b,c,d){Cb(b,"scope");var e=c?Ja.clone.call(a):a;q(d,function(a,b){e.data("$"+b+"Controller",a)});d=0;for(var g=e.length;d<g;d++){var m=e[d].nodeType;1!==m&&9!==m||e.eq(d).data("$scope",b)}c&&c(e,b);f&&f(b,e,e);return e}}function ma(a,b){try{a.addClass(b)}catch(c){}}function L(a,b,c,d,e,f){function g(a,c,d,e){var f,k,l,r,n,p,A;f=c.length;var I=Array(f);for(n=0;n<f;n++)I[n]=c[n];A=n=0;for(p=m.length;n<p;A++)k=I[A],c=m[n++],f=m[n++],l=y(k),c?(c.scope?
(r=a.$new(),l.data("$scope",r)):r=a,(l=c.transclude)||!e&&b?c(f,r,k,d,O(a,l||b)):c(f,r,k,d,e)):f&&f(a,k.childNodes,s,e)}for(var m=[],k,l,r,n,p=0;p<a.length;p++)k=new Lb,l=ca(a[p],[],k,0===p?d:s,e),(f=l.length?ea(l,a[p],k,b,c,null,[],[],f):null)&&f.scope&&ma(y(a[p]),"ng-scope"),k=f&&f.terminal||!(r=a[p].childNodes)||!r.length?null:L(r,f?f.transclude:b),m.push(f,k),n=n||f||k,f=null;return n?g:null}function O(a,b){return function(c,d,e){var f=!1;c||(c=a.$new(),f=c.$$transcluded=!0);d=b(c,d,e);if(f)d.on("$destroy",
gb(c,c.$destroy));return d}}function ca(a,b,c,d,g){var m=c.$attr,k;switch(a.nodeType){case 1:T(b,na(Ka(a).toLowerCase()),"E",d,g);var l,r,n;k=a.attributes;for(var p=0,A=k&&k.length;p<A;p++){var x=!1,D=!1;l=k[p];if(!S||8<=S||l.specified){r=l.name;n=na(r);X.test(n)&&(r=ib(n.substr(6),"-"));var N=n.replace(/(Start|End)$/,"");n===N+"Start"&&(x=r,D=r.substr(0,r.length-5)+"end",r=r.substr(0,r.length-6));n=na(r.toLowerCase());m[n]=r;c[n]=l=ba(l.value);qc(a,n)&&(c[n]=!0);M(a,b,l,n);T(b,n,"A",d,g,x,D)}}a=
a.className;if(C(a)&&""!==a)for(;k=f.exec(a);)n=na(k[2]),T(b,n,"C",d,g)&&(c[n]=ba(k[3])),a=a.substr(k.index+k[0].length);break;case 3:u(b,a.nodeValue);break;case 8:try{if(k=e.exec(a.nodeValue))n=na(k[1]),T(b,n,"M",d,g)&&(c[n]=ba(k[2]))}catch(t){}}b.sort(H);return b}function F(a,b,c){var d=[],e=0;if(b&&a.hasAttribute&&a.hasAttribute(b)){do{if(!a)throw ia("uterdir",b,c);1==a.nodeType&&(a.hasAttribute(b)&&e++,a.hasAttribute(c)&&e--);d.push(a);a=a.nextSibling}while(0<e)}else d.push(a);return y(d)}function R(a,
b,c){return function(d,e,f,g,k){e=F(e[0],b,c);return a(d,e,f,g,k)}}function ea(a,c,d,e,f,g,m,n,p){function x(a,b,c,d){if(a){c&&(a=R(a,c,d));a.require=G.require;a.directiveName=u;if(O===G||G.$$isolateScope)a=uc(a,{isolateScope:!0});m.push(a)}if(b){c&&(b=R(b,c,d));b.require=G.require;b.directiveName=u;if(O===G||G.$$isolateScope)b=uc(b,{isolateScope:!0});n.push(b)}}function D(a,b,c,d){var e,f="data",g=!1;if(C(b)){for(;"^"==(e=b.charAt(0))||"?"==e;)b=b.substr(1),"^"==e&&(f="inheritedData"),g=g||"?"==
e;e=null;d&&"data"===f&&(e=d[b]);e=e||c[f]("$"+b+"Controller");if(!e&&!g)throw ia("ctreq",b,a);}else K(b)&&(e=[],q(b,function(b){e.push(D(a,b,c,d))}));return e}function N(a,e,f,g,p){function x(a,b){var c;2>arguments.length&&(b=a,a=s);E&&(c=ca);return p(a,b,c)}var t,I,v,B,R,F,ca={},ob;t=c===f?d:ka(d,new Lb(y(f),d.$attr));I=t.$$element;if(O){var T=/^\s*([@=&])(\??)\s*(\w*)\s*$/;g=y(f);F=e.$new(!0);!ea||ea!==O&&ea!==O.$$originalDirective?g.data("$isolateScopeNoTemplate",F):g.data("$isolateScope",F);
ma(g,"ng-isolate-scope");q(O.scope,function(a,c){var d=a.match(T)||[],f=d[3]||c,g="?"==d[2],d=d[1],m,l,n,p;F.$$isolateBindings[c]=d+f;switch(d){case "@":t.$observe(f,function(a){F[c]=a});t.$$observers[f].$$scope=e;t[f]&&(F[c]=b(t[f])(e));break;case "=":if(g&&!t[f])break;l=r(t[f]);p=l.literal?xa:function(a,b){return a===b};n=l.assign||function(){m=F[c]=l(e);throw ia("nonassign",t[f],O.name);};m=F[c]=l(e);F.$watch(function(){var a=l(e);p(a,F[c])||(p(a,m)?n(e,a=F[c]):F[c]=a);return m=a},null,l.literal);
break;case "&":l=r(t[f]);F[c]=function(a){return l(e,a)};break;default:throw ia("iscp",O.name,c,a);}})}ob=p&&x;L&&q(L,function(a){var b={$scope:a===O||a.$$isolateScope?F:e,$element:I,$attrs:t,$transclude:ob},c;R=a.controller;"@"==R&&(R=t[a.name]);c=A(R,b);ca[a.name]=c;E||I.data("$"+a.name+"Controller",c);a.controllerAs&&(b.$scope[a.controllerAs]=c)});g=0;for(v=m.length;g<v;g++)try{B=m[g],B(B.isolateScope?F:e,I,t,B.require&&D(B.directiveName,B.require,I,ca),ob)}catch(G){l(G,ha(I))}g=e;O&&(O.template||
null===O.templateUrl)&&(g=F);a&&a(g,f.childNodes,s,p);for(g=n.length-1;0<=g;g--)try{B=n[g],B(B.isolateScope?F:e,I,t,B.require&&D(B.directiveName,B.require,I,ca),ob)}catch(z){l(z,ha(I))}}p=p||{};for(var t=-Number.MAX_VALUE,B,L=p.controllerDirectives,O=p.newIsolateScopeDirective,ea=p.templateDirective,T=p.nonTlbTranscludeDirective,H=!1,E=p.hasElementTranscludeDirective,Z=d.$$element=y(c),G,u,W,Za=e,P,M=0,S=a.length;M<S;M++){G=a[M];var ra=G.$$start,X=G.$$end;ra&&(Z=F(c,ra,X));W=s;if(t>G.priority)break;
if(W=G.scope)B=B||G,G.templateUrl||(J("new/isolated scope",O,G,Z),U(W)&&(O=G));u=G.name;!G.templateUrl&&G.controller&&(W=G.controller,L=L||{},J("'"+u+"' controller",L[u],G,Z),L[u]=G);if(W=G.transclude)H=!0,G.$$tlb||(J("transclusion",T,G,Z),T=G),"element"==W?(E=!0,t=G.priority,W=F(c,ra,X),Z=d.$$element=y(V.createComment(" "+u+": "+d[u]+" ")),c=Z[0],pb(f,y(ya.call(W,0)),c),Za=v(W,e,t,g&&g.name,{nonTlbTranscludeDirective:T})):(W=y(Jb(c)).contents(),Z.empty(),Za=v(W,e));if(G.template)if(J("template",
ea,G,Z),ea=G,W=Q(G.template)?G.template(Z,d):G.template,W=Y(W),G.replace){g=G;W=Hb.test(W)?y(ba(W)):[];c=W[0];if(1!=W.length||1!==c.nodeType)throw ia("tplrt",u,"");pb(f,Z,c);S={$attr:{}};W=ca(c,[],S);var $=a.splice(M+1,a.length-(M+1));O&&tc(W);a=a.concat(W).concat($);z(d,S);S=a.length}else Z.html(W);if(G.templateUrl)J("template",ea,G,Z),ea=G,G.replace&&(g=G),N=w(a.splice(M,a.length-M),Z,d,f,Za,m,n,{controllerDirectives:L,newIsolateScopeDirective:O,templateDirective:ea,nonTlbTranscludeDirective:T}),
S=a.length;else if(G.compile)try{P=G.compile(Z,d,Za),Q(P)?x(null,P,ra,X):P&&x(P.pre,P.post,ra,X)}catch(aa){l(aa,ha(Z))}G.terminal&&(N.terminal=!0,t=Math.max(t,G.priority))}N.scope=B&&!0===B.scope;N.transclude=H&&Za;p.hasElementTranscludeDirective=E;return N}function tc(a){for(var b=0,c=a.length;b<c;b++)a[b]=Zb(a[b],{$$isolateScope:!0})}function T(b,e,f,g,k,r,n){if(e===k)return null;k=null;if(c.hasOwnProperty(e)){var p;e=a.get(e+d);for(var A=0,x=e.length;A<x;A++)try{p=e[A],(g===s||g>p.priority)&&-1!=
p.restrict.indexOf(f)&&(r&&(p=Zb(p,{$$start:r,$$end:n})),b.push(p),k=p)}catch(D){l(D)}}return k}function z(a,b){var c=b.$attr,d=a.$attr,e=a.$$element;q(a,function(d,e){"$"!=e.charAt(0)&&(b[e]&&b[e]!==d&&(d+=("style"===e?";":" ")+b[e]),a.$set(e,d,!0,c[e]))});q(b,function(b,f){"class"==f?(ma(e,b),a["class"]=(a["class"]?a["class"]+" ":"")+b):"style"==f?(e.attr("style",e.attr("style")+";"+b),a.style=(a.style?a.style+";":"")+b):"$"==f.charAt(0)||a.hasOwnProperty(f)||(a[f]=b,d[f]=c[f])})}function w(a,b,
c,d,e,f,g,k){var m=[],l,r,A=b[0],x=a.shift(),D=E({},x,{templateUrl:null,transclude:null,replace:null,$$originalDirective:x}),N=Q(x.templateUrl)?x.templateUrl(b,c):x.templateUrl;b.empty();n.get(t.getTrustedResourceUrl(N),{cache:p}).success(function(n){var p,t;n=Y(n);if(x.replace){n=Hb.test(n)?y(ba(n)):[];p=n[0];if(1!=n.length||1!==p.nodeType)throw ia("tplrt",x.name,N);n={$attr:{}};pb(d,b,p);var v=ca(p,[],n);U(x.scope)&&tc(v);a=v.concat(a);z(c,n)}else p=A,b.html(n);a.unshift(D);l=ea(a,p,c,e,b,x,f,g,
k);q(d,function(a,c){a==p&&(d[c]=b[0])});for(r=L(b[0].childNodes,e);m.length;){n=m.shift();t=m.shift();var B=m.shift(),R=m.shift(),v=b[0];if(t!==A){var F=t.className;k.hasElementTranscludeDirective&&x.replace||(v=Jb(p));pb(B,y(t),v);ma(y(v),F)}t=l.transclude?O(n,l.transclude):R;l(r,n,v,d,t)}m=null}).error(function(a,b,c,d){throw ia("tpload",d.url);});return function(a,b,c,d,e){m?(m.push(b),m.push(c),m.push(d),m.push(e)):l(r,b,c,d,e)}}function H(a,b){var c=b.priority-a.priority;return 0!==c?c:a.name!==
b.name?a.name<b.name?-1:1:a.index-b.index}function J(a,b,c,d){if(b)throw ia("multidir",b.name,c.name,a,ha(d));}function u(a,c){var d=b(c,!0);d&&a.push({priority:0,compile:aa(function(a,b){var c=b.parent(),e=c.data("$binding")||[];e.push(d);ma(c.data("$binding",e),"ng-binding");a.$watch(d,function(a){b[0].nodeValue=a})})})}function P(a,b){if("srcdoc"==b)return t.HTML;var c=Ka(a);if("xlinkHref"==b||"FORM"==c&&"action"==b||"IMG"!=c&&("src"==b||"ngSrc"==b))return t.RESOURCE_URL}function M(a,c,d,e){var f=
b(d,!0);if(f){if("multiple"===e&&"SELECT"===Ka(a))throw ia("selmulti",ha(a));c.push({priority:100,compile:function(){return{pre:function(c,d,m){d=m.$$observers||(m.$$observers={});if(g.test(e))throw ia("nodomevents");if(f=b(m[e],!0,P(a,e)))m[e]=f(c),(d[e]||(d[e]=[])).$$inter=!0,(m.$$observers&&m.$$observers[e].$$scope||c).$watch(f,function(a,b){"class"===e&&a!=b?m.$updateClass(a,b):m.$set(e,a)})}}}})}}function pb(a,b,c){var d=b[0],e=b.length,f=d.parentNode,g,m;if(a)for(g=0,m=a.length;g<m;g++)if(a[g]==
d){a[g++]=c;m=g+e-1;for(var k=a.length;g<k;g++,m++)m<k?a[g]=a[m]:delete a[g];a.length-=e-1;break}f&&f.replaceChild(c,d);a=V.createDocumentFragment();a.appendChild(d);c[y.expando]=d[y.expando];d=1;for(e=b.length;d<e;d++)f=b[d],y(f).remove(),a.appendChild(f),delete b[d];b[0]=c;b.length=1}function uc(a,b){return E(function(){return a.apply(null,arguments)},a,b)}var Lb=function(a,b){this.$$element=a;this.$attr=b||{}};Lb.prototype={$normalize:na,$addClass:function(a){a&&0<a.length&&N.addClass(this.$$element,
a)},$removeClass:function(a){a&&0<a.length&&N.removeClass(this.$$element,a)},$updateClass:function(a,b){var c=vc(a,b),d=vc(b,a);0===c.length?N.removeClass(this.$$element,d):0===d.length?N.addClass(this.$$element,c):N.setClass(this.$$element,c,d)},$set:function(a,b,c,d){var e=qc(this.$$element[0],a);e&&(this.$$element.prop(a,b),d=e);this[a]=b;d?this.$attr[a]=d:(d=this.$attr[a])||(this.$attr[a]=d=ib(a,"-"));e=Ka(this.$$element);if("A"===e&&"href"===a||"IMG"===e&&"src"===a)this[a]=b=B(b,"src"===a);!1!==
c&&(null===b||b===s?this.$$element.removeAttr(d):this.$$element.attr(d,b));(c=this.$$observers)&&q(c[a],function(a){try{a(b)}catch(c){l(c)}})},$observe:function(a,b){var c=this,d=c.$$observers||(c.$$observers={}),e=d[a]||(d[a]=[]);e.push(b);D.$evalAsync(function(){e.$$inter||b(c[a])});return b}};var Z=b.startSymbol(),ra=b.endSymbol(),Y="{{"==Z||"}}"==ra?Ea:function(a){return a.replace(/\{\{/g,Z).replace(/}}/g,ra)},X=/^ngAttr[A-Z]/;return v}]}function na(b){return Ua(b.replace(te,""))}function vc(b,
a){var c="",d=b.split(/\s+/),e=a.split(/\s+/),f=0;a:for(;f<d.length;f++){for(var g=d[f],h=0;h<e.length;h++)if(g==e[h])continue a;c+=(0<c.length?" ":"")+g}return c}function Od(){var b={},a=/^(\S+)(\s+as\s+(\w+))?$/;this.register=function(a,d){Aa(a,"controller");U(a)?E(b,a):b[a]=d};this.$get=["$injector","$window",function(c,d){return function(e,f){var g,h,m;C(e)&&(g=e.match(a),h=g[1],m=g[3],e=b.hasOwnProperty(h)?b[h]:fc(f.$scope,h,!0)||fc(d,h,!0),Sa(e,h,!0));g=c.instantiate(e,f);if(m){if(!f||"object"!=
typeof f.$scope)throw u("$controller")("noscp",h||e.name,m);f.$scope[m]=g}return g}}]}function Pd(){this.$get=["$window",function(b){return y(b.document)}]}function Qd(){this.$get=["$log",function(b){return function(a,c){b.error.apply(b,arguments)}}]}function wc(b){var a={},c,d,e;if(!b)return a;q(b.split("\n"),function(b){e=b.indexOf(":");c=J(ba(b.substr(0,e)));d=ba(b.substr(e+1));c&&(a[c]=a[c]?a[c]+(", "+d):d)});return a}function xc(b){var a=U(b)?b:s;return function(c){a||(a=wc(b));return c?a[J(c)]||
null:a}}function yc(b,a,c){if(Q(c))return c(b,a);q(c,function(c){b=c(b,a)});return b}function Td(){var b=/^\s*(\[|\{[^\{])/,a=/[\}\]]\s*$/,c=/^\)\]\}',?\n/,d={"Content-Type":"application/json;charset=utf-8"},e=this.defaults={transformResponse:[function(d){C(d)&&(d=d.replace(c,""),b.test(d)&&a.test(d)&&(d=ac(d)));return d}],transformRequest:[function(a){return U(a)&&"[object File]"!==wa.call(a)&&"[object Blob]"!==wa.call(a)?qa(a):a}],headers:{common:{Accept:"application/json, text/plain, */*"},post:ka(d),
put:ka(d),patch:ka(d)},xsrfCookieName:"XSRF-TOKEN",xsrfHeaderName:"X-XSRF-TOKEN"},f=this.interceptors=[],g=this.responseInterceptors=[];this.$get=["$httpBackend","$browser","$cacheFactory","$rootScope","$q","$injector",function(a,b,c,d,n,p){function r(a){function c(a){var b=E({},a,{data:yc(a.data,a.headers,d.transformResponse)});return 200<=a.status&&300>a.status?b:n.reject(b)}var d={method:"get",transformRequest:e.transformRequest,transformResponse:e.transformResponse},f=function(a){function b(a){var c;
q(a,function(b,d){Q(b)&&(c=b(),null!=c?a[d]=c:delete a[d])})}var c=e.headers,d=E({},a.headers),f,g,c=E({},c.common,c[J(a.method)]);b(c);b(d);a:for(f in c){a=J(f);for(g in d)if(J(g)===a)continue a;d[f]=c[f]}return d}(a);E(d,a);d.headers=f;d.method=Ga(d.method);(a=Mb(d.url)?b.cookies()[d.xsrfCookieName||e.xsrfCookieName]:s)&&(f[d.xsrfHeaderName||e.xsrfHeaderName]=a);var g=[function(a){f=a.headers;var b=yc(a.data,xc(f),a.transformRequest);H(a.data)&&q(f,function(a,b){"content-type"===J(b)&&delete f[b]});
H(a.withCredentials)&&!H(e.withCredentials)&&(a.withCredentials=e.withCredentials);return A(a,b,f).then(c,c)},s],h=n.when(d);for(q(t,function(a){(a.request||a.requestError)&&g.unshift(a.request,a.requestError);(a.response||a.responseError)&&g.push(a.response,a.responseError)});g.length;){a=g.shift();var k=g.shift(),h=h.then(a,k)}h.success=function(a){h.then(function(b){a(b.data,b.status,b.headers,d)});return h};h.error=function(a){h.then(null,function(b){a(b.data,b.status,b.headers,d)});return h};
return h}function A(b,c,f){function g(a,b,c,e){t&&(200<=a&&300>a?t.put(s,[a,b,wc(c),e]):t.remove(s));m(b,a,c,e);d.$$phase||d.$apply()}function m(a,c,d,e){c=Math.max(c,0);(200<=c&&300>c?p.resolve:p.reject)({data:a,status:c,headers:xc(d),config:b,statusText:e})}function k(){var a=Na(r.pendingRequests,b);-1!==a&&r.pendingRequests.splice(a,1)}var p=n.defer(),A=p.promise,t,q,s=D(b.url,b.params);r.pendingRequests.push(b);A.then(k,k);(b.cache||e.cache)&&(!1!==b.cache&&"GET"==b.method)&&(t=U(b.cache)?b.cache:
U(e.cache)?e.cache:x);if(t)if(q=t.get(s),z(q)){if(q.then)return q.then(k,k),q;K(q)?m(q[1],q[0],ka(q[2]),q[3]):m(q,200,{},"OK")}else t.put(s,A);H(q)&&a(b.method,s,c,g,f,b.timeout,b.withCredentials,b.responseType);return A}function D(a,b){if(!b)return a;var c=[];Sc(b,function(a,b){null===a||H(a)||(K(a)||(a=[a]),q(a,function(a){U(a)&&(a=qa(a));c.push(za(b)+"="+za(a))}))});0<c.length&&(a+=(-1==a.indexOf("?")?"?":"&")+c.join("&"));return a}var x=c("$http"),t=[];q(f,function(a){t.unshift(C(a)?p.get(a):
p.invoke(a))});q(g,function(a,b){var c=C(a)?p.get(a):p.invoke(a);t.splice(b,0,{response:function(a){return c(n.when(a))},responseError:function(a){return c(n.reject(a))}})});r.pendingRequests=[];(function(a){q(arguments,function(a){r[a]=function(b,c){return r(E(c||{},{method:a,url:b}))}})})("get","delete","head","jsonp");(function(a){q(arguments,function(a){r[a]=function(b,c,d){return r(E(d||{},{method:a,url:b,data:c}))}})})("post","put");r.defaults=e;return r}]}function ue(b){if(8>=S&&(!b.match(/^(get|post|head|put|delete|options)$/i)||
!P.XMLHttpRequest))return new P.ActiveXObject("Microsoft.XMLHTTP");if(P.XMLHttpRequest)return new P.XMLHttpRequest;throw u("$httpBackend")("noxhr");}function Ud(){this.$get=["$browser","$window","$document",function(b,a,c){return ve(b,ue,b.defer,a.angular.callbacks,c[0])}]}function ve(b,a,c,d,e){function f(a,b,c){var f=e.createElement("script"),g=null;f.type="text/javascript";f.src=a;f.async=!0;g=function(a){Va(f,"load",g);Va(f,"error",g);e.body.removeChild(f);f=null;var h=-1,A="unknown";a&&("load"!==
a.type||d[b].called||(a={type:"error"}),A=a.type,h="error"===a.type?404:200);c&&c(h,A)};qb(f,"load",g);qb(f,"error",g);8>=S&&(f.onreadystatechange=function(){C(f.readyState)&&/loaded|complete/.test(f.readyState)&&(f.onreadystatechange=null,g({type:"load"}))});e.body.appendChild(f);return g}var g=-1;return function(e,m,k,l,n,p,r,A){function D(){t=g;B&&B();v&&v.abort()}function x(a,d,e,f,g){L&&c.cancel(L);B=v=null;0===d&&(d=e?200:"file"==sa(m).protocol?404:0);a(1223===d?204:d,e,f,g||"");b.$$completeOutstandingRequest(w)}
var t;b.$$incOutstandingRequestCount();m=m||b.url();if("jsonp"==J(e)){var N="_"+(d.counter++).toString(36);d[N]=function(a){d[N].data=a;d[N].called=!0};var B=f(m.replace("JSON_CALLBACK","angular.callbacks."+N),N,function(a,b){x(l,a,d[N].data,"",b);d[N]=w})}else{var v=a(e);v.open(e,m,!0);q(n,function(a,b){z(a)&&v.setRequestHeader(b,a)});v.onreadystatechange=function(){if(v&&4==v.readyState){var a=null,b=null;t!==g&&(a=v.getAllResponseHeaders(),b="response"in v?v.response:v.responseText);x(l,t||v.status,
b,a,v.statusText||"")}};r&&(v.withCredentials=!0);if(A)try{v.responseType=A}catch(s){if("json"!==A)throw s;}v.send(k||null)}if(0<p)var L=c(D,p);else p&&p.then&&p.then(D)}}function Rd(){var b="{{",a="}}";this.startSymbol=function(a){return a?(b=a,this):b};this.endSymbol=function(b){return b?(a=b,this):a};this.$get=["$parse","$exceptionHandler","$sce",function(c,d,e){function f(f,k,l){for(var n,p,r=0,A=[],D=f.length,x=!1,t=[];r<D;)-1!=(n=f.indexOf(b,r))&&-1!=(p=f.indexOf(a,n+g))?(r!=n&&A.push(f.substring(r,
n)),A.push(r=c(x=f.substring(n+g,p))),r.exp=x,r=p+h,x=!0):(r!=D&&A.push(f.substring(r)),r=D);(D=A.length)||(A.push(""),D=1);if(l&&1<A.length)throw zc("noconcat",f);if(!k||x)return t.length=D,r=function(a){try{for(var b=0,c=D,g;b<c;b++){if("function"==typeof(g=A[b]))if(g=g(a),g=l?e.getTrusted(l,g):e.valueOf(g),null==g)g="";else switch(typeof g){case "string":break;case "number":g=""+g;break;default:g=qa(g)}t[b]=g}return t.join("")}catch(h){a=zc("interr",f,h.toString()),d(a)}},r.exp=f,r.parts=A,r}var g=
b.length,h=a.length;f.startSymbol=function(){return b};f.endSymbol=function(){return a};return f}]}function Sd(){this.$get=["$rootScope","$window","$q",function(b,a,c){function d(d,g,h,m){var k=a.setInterval,l=a.clearInterval,n=c.defer(),p=n.promise,r=0,A=z(m)&&!m;h=z(h)?h:0;p.then(null,null,d);p.$$intervalId=k(function(){n.notify(r++);0<h&&r>=h&&(n.resolve(r),l(p.$$intervalId),delete e[p.$$intervalId]);A||b.$apply()},g);e[p.$$intervalId]=n;return p}var e={};d.cancel=function(a){return a&&a.$$intervalId in
e?(e[a.$$intervalId].reject("canceled"),clearInterval(a.$$intervalId),delete e[a.$$intervalId],!0):!1};return d}]}function ad(){this.$get=function(){return{id:"en-us",NUMBER_FORMATS:{DECIMAL_SEP:".",GROUP_SEP:",",PATTERNS:[{minInt:1,minFrac:0,maxFrac:3,posPre:"",posSuf:"",negPre:"-",negSuf:"",gSize:3,lgSize:3},{minInt:1,minFrac:2,maxFrac:2,posPre:"\u00a4",posSuf:"",negPre:"(\u00a4",negSuf:")",gSize:3,lgSize:3}],CURRENCY_SYM:"$"},DATETIME_FORMATS:{MONTH:"January February March April May June July August September October November December".split(" "),
SHORTMONTH:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),DAY:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),SHORTDAY:"Sun Mon Tue Wed Thu Fri Sat".split(" "),AMPMS:["AM","PM"],medium:"MMM d, y h:mm:ss a","short":"M/d/yy h:mm a",fullDate:"EEEE, MMMM d, y",longDate:"MMMM d, y",mediumDate:"MMM d, y",shortDate:"M/d/yy",mediumTime:"h:mm:ss a",shortTime:"h:mm a"},pluralCat:function(b){return 1===b?"one":"other"}}}}function Nb(b){b=b.split("/");for(var a=b.length;a--;)b[a]=
hb(b[a]);return b.join("/")}function Ac(b,a,c){b=sa(b,c);a.$$protocol=b.protocol;a.$$host=b.hostname;a.$$port=Y(b.port)||we[b.protocol]||null}function Bc(b,a,c){var d="/"!==b.charAt(0);d&&(b="/"+b);b=sa(b,c);a.$$path=decodeURIComponent(d&&"/"===b.pathname.charAt(0)?b.pathname.substring(1):b.pathname);a.$$search=cc(b.search);a.$$hash=decodeURIComponent(b.hash);a.$$path&&"/"!=a.$$path.charAt(0)&&(a.$$path="/"+a.$$path)}function oa(b,a){if(0===a.indexOf(b))return a.substr(b.length)}function $a(b){var a=
b.indexOf("#");return-1==a?b:b.substr(0,a)}function Ob(b){return b.substr(0,$a(b).lastIndexOf("/")+1)}function Cc(b,a){this.$$html5=!0;a=a||"";var c=Ob(b);Ac(b,this,b);this.$$parse=function(a){var e=oa(c,a);if(!C(e))throw Pb("ipthprfx",a,c);Bc(e,this,b);this.$$path||(this.$$path="/");this.$$compose()};this.$$compose=function(){var a=Bb(this.$$search),b=this.$$hash?"#"+hb(this.$$hash):"";this.$$url=Nb(this.$$path)+(a?"?"+a:"")+b;this.$$absUrl=c+this.$$url.substr(1)};this.$$rewrite=function(d){var e;
if((e=oa(b,d))!==s)return d=e,(e=oa(a,e))!==s?c+(oa("/",e)||e):b+d;if((e=oa(c,d))!==s)return c+e;if(c==d+"/")return c}}function Qb(b,a){var c=Ob(b);Ac(b,this,b);this.$$parse=function(d){var e=oa(b,d)||oa(c,d),e="#"==e.charAt(0)?oa(a,e):this.$$html5?e:"";if(!C(e))throw Pb("ihshprfx",d,a);Bc(e,this,b);d=this.$$path;var f=/^\/[A-Z]:(\/.*)/;0===e.indexOf(b)&&(e=e.replace(b,""));f.exec(e)||(d=(e=f.exec(d))?e[1]:d);this.$$path=d;this.$$compose()};this.$$compose=function(){var c=Bb(this.$$search),e=this.$$hash?
"#"+hb(this.$$hash):"";this.$$url=Nb(this.$$path)+(c?"?"+c:"")+e;this.$$absUrl=b+(this.$$url?a+this.$$url:"")};this.$$rewrite=function(a){if($a(b)==$a(a))return a}}function Rb(b,a){this.$$html5=!0;Qb.apply(this,arguments);var c=Ob(b);this.$$rewrite=function(d){var e;if(b==$a(d))return d;if(e=oa(c,d))return b+a+e;if(c===d+"/")return c};this.$$compose=function(){var c=Bb(this.$$search),e=this.$$hash?"#"+hb(this.$$hash):"";this.$$url=Nb(this.$$path)+(c?"?"+c:"")+e;this.$$absUrl=b+a+this.$$url}}function rb(b){return function(){return this[b]}}
function Dc(b,a){return function(c){if(H(c))return this[b];this[b]=a(c);this.$$compose();return this}}function Vd(){var b="",a=!1;this.hashPrefix=function(a){return z(a)?(b=a,this):b};this.html5Mode=function(b){return z(b)?(a=b,this):a};this.$get=["$rootScope","$browser","$sniffer","$rootElement",function(c,d,e,f){function g(a){c.$broadcast("$locationChangeSuccess",h.absUrl(),a)}var h,m,k=d.baseHref(),l=d.url(),n;a?(n=l.substring(0,l.indexOf("/",l.indexOf("//")+2))+(k||"/"),m=e.history?Cc:Rb):(n=
$a(l),m=Qb);h=new m(n,"#"+b);h.$$parse(h.$$rewrite(l));f.on("click",function(a){if(!a.ctrlKey&&!a.metaKey&&2!=a.which){for(var e=y(a.target);"a"!==J(e[0].nodeName);)if(e[0]===f[0]||!(e=e.parent())[0])return;var g=e.prop("href");U(g)&&"[object SVGAnimatedString]"===g.toString()&&(g=sa(g.animVal).href);if(m===Rb){var k=e.attr("href")||e.attr("xlink:href");if(0>k.indexOf("://"))if(g="#"+b,"/"==k[0])g=n+g+k;else if("#"==k[0])g=n+g+(h.path()||"/")+k;else{for(var l=h.path().split("/"),k=k.split("/"),p=
0;p<k.length;p++)"."!=k[p]&&(".."==k[p]?l.pop():k[p].length&&l.push(k[p]));g=n+g+l.join("/")}}l=h.$$rewrite(g);g&&(!e.attr("target")&&l&&!a.isDefaultPrevented())&&(a.preventDefault(),l!=d.url()&&(h.$$parse(l),c.$apply(),P.angular["ff-684208-preventDefault"]=!0))}});h.absUrl()!=l&&d.url(h.absUrl(),!0);d.onUrlChange(function(a){h.absUrl()!=a&&(c.$evalAsync(function(){var b=h.absUrl();h.$$parse(a);c.$broadcast("$locationChangeStart",a,b).defaultPrevented?(h.$$parse(b),d.url(b)):g(b)}),c.$$phase||c.$digest())});
var p=0;c.$watch(function(){var a=d.url(),b=h.$$replace;p&&a==h.absUrl()||(p++,c.$evalAsync(function(){c.$broadcast("$locationChangeStart",h.absUrl(),a).defaultPrevented?h.$$parse(a):(d.url(h.absUrl(),b),g(a))}));h.$$replace=!1;return p});return h}]}function Wd(){var b=!0,a=this;this.debugEnabled=function(a){return z(a)?(b=a,this):b};this.$get=["$window",function(c){function d(a){a instanceof Error&&(a.stack?a=a.message&&-1===a.stack.indexOf(a.message)?"Error: "+a.message+"\n"+a.stack:a.stack:a.sourceURL&&
(a=a.message+"\n"+a.sourceURL+":"+a.line));return a}function e(a){var b=c.console||{},e=b[a]||b.log||w;a=!1;try{a=!!e.apply}catch(m){}return a?function(){var a=[];q(arguments,function(b){a.push(d(b))});return e.apply(b,a)}:function(a,b){e(a,null==b?"":b)}}return{log:e("log"),info:e("info"),warn:e("warn"),error:e("error"),debug:function(){var c=e("debug");return function(){b&&c.apply(a,arguments)}}()}}]}function fa(b,a){if("constructor"===b)throw Ca("isecfld",a);return b}function ab(b,a){if(b){if(b.constructor===
b)throw Ca("isecfn",a);if(b.document&&b.location&&b.alert&&b.setInterval)throw Ca("isecwindow",a);if(b.children&&(b.nodeName||b.prop&&b.attr&&b.find))throw Ca("isecdom",a);}return b}function sb(b,a,c,d,e){e=e||{};a=a.split(".");for(var f,g=0;1<a.length;g++){f=fa(a.shift(),d);var h=b[f];h||(h={},b[f]=h);b=h;b.then&&e.unwrapPromises&&(ta(d),"$$v"in b||function(a){a.then(function(b){a.$$v=b})}(b),b.$$v===s&&(b.$$v={}),b=b.$$v)}f=fa(a.shift(),d);return b[f]=c}function Ec(b,a,c,d,e,f,g){fa(b,f);fa(a,f);
fa(c,f);fa(d,f);fa(e,f);return g.unwrapPromises?function(g,m){var k=m&&m.hasOwnProperty(b)?m:g,l;if(null==k)return k;(k=k[b])&&k.then&&(ta(f),"$$v"in k||(l=k,l.$$v=s,l.then(function(a){l.$$v=a})),k=k.$$v);if(!a)return k;if(null==k)return s;(k=k[a])&&k.then&&(ta(f),"$$v"in k||(l=k,l.$$v=s,l.then(function(a){l.$$v=a})),k=k.$$v);if(!c)return k;if(null==k)return s;(k=k[c])&&k.then&&(ta(f),"$$v"in k||(l=k,l.$$v=s,l.then(function(a){l.$$v=a})),k=k.$$v);if(!d)return k;if(null==k)return s;(k=k[d])&&k.then&&
(ta(f),"$$v"in k||(l=k,l.$$v=s,l.then(function(a){l.$$v=a})),k=k.$$v);if(!e)return k;if(null==k)return s;(k=k[e])&&k.then&&(ta(f),"$$v"in k||(l=k,l.$$v=s,l.then(function(a){l.$$v=a})),k=k.$$v);return k}:function(f,g){var k=g&&g.hasOwnProperty(b)?g:f;if(null==k)return k;k=k[b];if(!a)return k;if(null==k)return s;k=k[a];if(!c)return k;if(null==k)return s;k=k[c];if(!d)return k;if(null==k)return s;k=k[d];return e?null==k?s:k=k[e]:k}}function xe(b,a){fa(b,a);return function(a,d){return null==a?s:(d&&d.hasOwnProperty(b)?
d:a)[b]}}function ye(b,a,c){fa(b,c);fa(a,c);return function(c,e){if(null==c)return s;c=(e&&e.hasOwnProperty(b)?e:c)[b];return null==c?s:c[a]}}function Fc(b,a,c){if(Sb.hasOwnProperty(b))return Sb[b];var d=b.split("."),e=d.length,f;if(a.unwrapPromises||1!==e)if(a.unwrapPromises||2!==e)if(a.csp)f=6>e?Ec(d[0],d[1],d[2],d[3],d[4],c,a):function(b,f){var g=0,h;do h=Ec(d[g++],d[g++],d[g++],d[g++],d[g++],c,a)(b,f),f=s,b=h;while(g<e);return h};else{var g="var p;\n";q(d,function(b,d){fa(b,c);g+="if(s == null) return undefined;\ns="+
(d?"s":'((k&&k.hasOwnProperty("'+b+'"))?k:s)')+'["'+b+'"];\n'+(a.unwrapPromises?'if (s && s.then) {\n pw("'+c.replace(/(["\r\n])/g,"\\$1")+'");\n if (!("$$v" in s)) {\n p=s;\n p.$$v = undefined;\n p.then(function(v) {p.$$v=v;});\n}\n s=s.$$v\n}\n':"")});var g=g+"return s;",h=new Function("s","k","pw",g);h.toString=aa(g);f=a.unwrapPromises?function(a,b){return h(a,b,ta)}:h}else f=ye(d[0],d[1],c);else f=xe(d[0],c);"hasOwnProperty"!==b&&(Sb[b]=f);return f}function Xd(){var b={},a={csp:!1,unwrapPromises:!1,
logPromiseWarnings:!0};this.unwrapPromises=function(b){return z(b)?(a.unwrapPromises=!!b,this):a.unwrapPromises};this.logPromiseWarnings=function(b){return z(b)?(a.logPromiseWarnings=b,this):a.logPromiseWarnings};this.$get=["$filter","$sniffer","$log",function(c,d,e){a.csp=d.csp;ta=function(b){a.logPromiseWarnings&&!Gc.hasOwnProperty(b)&&(Gc[b]=!0,e.warn("[$parse] Promise found in the expression `"+b+"`. Automatic unwrapping of promises in Angular expressions is deprecated."))};return function(d){var e;
switch(typeof d){case "string":if(b.hasOwnProperty(d))return b[d];e=new Tb(a);e=(new bb(e,c,a)).parse(d);"hasOwnProperty"!==d&&(b[d]=e);return e;case "function":return d;default:return w}}}]}function Zd(){this.$get=["$rootScope","$exceptionHandler",function(b,a){return ze(function(a){b.$evalAsync(a)},a)}]}function ze(b,a){function c(a){return a}function d(a){return g(a)}var e=function(){var g=[],k,l;return l={resolve:function(a){if(g){var c=g;g=s;k=f(a);c.length&&b(function(){for(var a,b=0,d=c.length;b<
d;b++)a=c[b],k.then(a[0],a[1],a[2])})}},reject:function(a){l.resolve(h(a))},notify:function(a){if(g){var c=g;g.length&&b(function(){for(var b,d=0,e=c.length;d<e;d++)b=c[d],b[2](a)})}},promise:{then:function(b,f,h){var l=e(),D=function(d){try{l.resolve((Q(b)?b:c)(d))}catch(e){l.reject(e),a(e)}},x=function(b){try{l.resolve((Q(f)?f:d)(b))}catch(c){l.reject(c),a(c)}},t=function(b){try{l.notify((Q(h)?h:c)(b))}catch(d){a(d)}};g?g.push([D,x,t]):k.then(D,x,t);return l.promise},"catch":function(a){return this.then(null,
a)},"finally":function(a){function b(a,c){var d=e();c?d.resolve(a):d.reject(a);return d.promise}function d(e,f){var g=null;try{g=(a||c)()}catch(h){return b(h,!1)}return g&&Q(g.then)?g.then(function(){return b(e,f)},function(a){return b(a,!1)}):b(e,f)}return this.then(function(a){return d(a,!0)},function(a){return d(a,!1)})}}}},f=function(a){return a&&Q(a.then)?a:{then:function(c){var d=e();b(function(){d.resolve(c(a))});return d.promise}}},g=function(a){var b=e();b.reject(a);return b.promise},h=function(c){return{then:function(f,
g){var h=e();b(function(){try{h.resolve((Q(g)?g:d)(c))}catch(b){h.reject(b),a(b)}});return h.promise}}};return{defer:e,reject:g,when:function(h,k,l,n){var p=e(),r,A=function(b){try{return(Q(k)?k:c)(b)}catch(d){return a(d),g(d)}},D=function(b){try{return(Q(l)?l:d)(b)}catch(c){return a(c),g(c)}},x=function(b){try{return(Q(n)?n:c)(b)}catch(d){a(d)}};b(function(){f(h).then(function(a){r||(r=!0,p.resolve(f(a).then(A,D,x)))},function(a){r||(r=!0,p.resolve(D(a)))},function(a){r||p.notify(x(a))})});return p.promise},
all:function(a){var b=e(),c=0,d=K(a)?[]:{};q(a,function(a,e){c++;f(a).then(function(a){d.hasOwnProperty(e)||(d[e]=a,--c||b.resolve(d))},function(a){d.hasOwnProperty(e)||b.reject(a)})});0===c&&b.resolve(d);return b.promise}}}function fe(){this.$get=["$window","$timeout",function(b,a){var c=b.requestAnimationFrame||b.webkitRequestAnimationFrame||b.mozRequestAnimationFrame,d=b.cancelAnimationFrame||b.webkitCancelAnimationFrame||b.mozCancelAnimationFrame||b.webkitCancelRequestAnimationFrame,e=!!c,f=e?
function(a){var b=c(a);return function(){d(b)}}:function(b){var c=a(b,16.66,!1);return function(){a.cancel(c)}};f.supported=e;return f}]}function Yd(){var b=10,a=u("$rootScope"),c=null;this.digestTtl=function(a){arguments.length&&(b=a);return b};this.$get=["$injector","$exceptionHandler","$parse","$browser",function(d,e,f,g){function h(){this.$id=eb();this.$$phase=this.$parent=this.$$watchers=this.$$nextSibling=this.$$prevSibling=this.$$childHead=this.$$childTail=null;this["this"]=this.$root=this;
this.$$destroyed=!1;this.$$asyncQueue=[];this.$$postDigestQueue=[];this.$$listeners={};this.$$listenerCount={};this.$$isolateBindings={}}function m(b){if(p.$$phase)throw a("inprog",p.$$phase);p.$$phase=b}function k(a,b){var c=f(a);Sa(c,b);return c}function l(a,b,c){do a.$$listenerCount[c]-=b,0===a.$$listenerCount[c]&&delete a.$$listenerCount[c];while(a=a.$parent)}function n(){}h.prototype={constructor:h,$new:function(a){a?(a=new h,a.$root=this.$root,a.$$asyncQueue=this.$$asyncQueue,a.$$postDigestQueue=
this.$$postDigestQueue):(this.$$childScopeClass||(this.$$childScopeClass=function(){this.$$watchers=this.$$nextSibling=this.$$childHead=this.$$childTail=null;this.$$listeners={};this.$$listenerCount={};this.$id=eb();this.$$childScopeClass=null},this.$$childScopeClass.prototype=this),a=new this.$$childScopeClass);a["this"]=a;a.$parent=this;a.$$prevSibling=this.$$childTail;this.$$childHead?this.$$childTail=this.$$childTail.$$nextSibling=a:this.$$childHead=this.$$childTail=a;return a},$watch:function(a,
b,d){var e=k(a,"watch"),f=this.$$watchers,g={fn:b,last:n,get:e,exp:a,eq:!!d};c=null;if(!Q(b)){var h=k(b||w,"listener");g.fn=function(a,b,c){h(c)}}if("string"==typeof a&&e.constant){var m=g.fn;g.fn=function(a,b,c){m.call(this,a,b,c);Oa(f,g)}}f||(f=this.$$watchers=[]);f.unshift(g);return function(){Oa(f,g);c=null}},$watchCollection:function(a,b){var c=this,d,e,g,h=1<b.length,k=0,m=f(a),l=[],n={},p=!0,q=0;return this.$watch(function(){d=m(c);var a,b;if(U(d))if(db(d))for(e!==l&&(e=l,q=e.length=0,k++),
a=d.length,q!==a&&(k++,e.length=q=a),b=0;b<a;b++)e[b]!==e[b]&&d[b]!==d[b]||e[b]===d[b]||(k++,e[b]=d[b]);else{e!==n&&(e=n={},q=0,k++);a=0;for(b in d)d.hasOwnProperty(b)&&(a++,e.hasOwnProperty(b)?e[b]!==d[b]&&(k++,e[b]=d[b]):(q++,e[b]=d[b],k++));if(q>a)for(b in k++,e)e.hasOwnProperty(b)&&!d.hasOwnProperty(b)&&(q--,delete e[b])}else e!==d&&(e=d,k++);return k},function(){p?(p=!1,b(d,d,c)):b(d,g,c);if(h)if(U(d))if(db(d)){g=Array(d.length);for(var a=0;a<d.length;a++)g[a]=d[a]}else for(a in g={},d)Ab.call(d,
a)&&(g[a]=d[a]);else g=d})},$digest:function(){var d,f,g,h,k=this.$$asyncQueue,l=this.$$postDigestQueue,q,v,s=b,L,O=[],y,F,R;m("$digest");c=null;do{v=!1;for(L=this;k.length;){try{R=k.shift(),R.scope.$eval(R.expression)}catch(z){p.$$phase=null,e(z)}c=null}a:do{if(h=L.$$watchers)for(q=h.length;q--;)try{if(d=h[q])if((f=d.get(L))!==(g=d.last)&&!(d.eq?xa(f,g):"number"==typeof f&&"number"==typeof g&&isNaN(f)&&isNaN(g)))v=!0,c=d,d.last=d.eq?Fa(f,null):f,d.fn(f,g===n?f:g,L),5>s&&(y=4-s,O[y]||(O[y]=[]),F=
Q(d.exp)?"fn: "+(d.exp.name||d.exp.toString()):d.exp,F+="; newVal: "+qa(f)+"; oldVal: "+qa(g),O[y].push(F));else if(d===c){v=!1;break a}}catch(C){p.$$phase=null,e(C)}if(!(h=L.$$childHead||L!==this&&L.$$nextSibling))for(;L!==this&&!(h=L.$$nextSibling);)L=L.$parent}while(L=h);if((v||k.length)&&!s--)throw p.$$phase=null,a("infdig",b,qa(O));}while(v||k.length);for(p.$$phase=null;l.length;)try{l.shift()()}catch(T){e(T)}},$destroy:function(){if(!this.$$destroyed){var a=this.$parent;this.$broadcast("$destroy");
this.$$destroyed=!0;this!==p&&(q(this.$$listenerCount,gb(null,l,this)),a.$$childHead==this&&(a.$$childHead=this.$$nextSibling),a.$$childTail==this&&(a.$$childTail=this.$$prevSibling),this.$$prevSibling&&(this.$$prevSibling.$$nextSibling=this.$$nextSibling),this.$$nextSibling&&(this.$$nextSibling.$$prevSibling=this.$$prevSibling),this.$parent=this.$$nextSibling=this.$$prevSibling=this.$$childHead=this.$$childTail=this.$root=null,this.$$listeners={},this.$$watchers=this.$$asyncQueue=this.$$postDigestQueue=
[],this.$destroy=this.$digest=this.$apply=w,this.$on=this.$watch=function(){return w})}},$eval:function(a,b){return f(a)(this,b)},$evalAsync:function(a){p.$$phase||p.$$asyncQueue.length||g.defer(function(){p.$$asyncQueue.length&&p.$digest()});this.$$asyncQueue.push({scope:this,expression:a})},$$postDigest:function(a){this.$$postDigestQueue.push(a)},$apply:function(a){try{return m("$apply"),this.$eval(a)}catch(b){e(b)}finally{p.$$phase=null;try{p.$digest()}catch(c){throw e(c),c;}}},$on:function(a,
b){var c=this.$$listeners[a];c||(this.$$listeners[a]=c=[]);c.push(b);var d=this;do d.$$listenerCount[a]||(d.$$listenerCount[a]=0),d.$$listenerCount[a]++;while(d=d.$parent);var e=this;return function(){c[Na(c,b)]=null;l(e,1,a)}},$emit:function(a,b){var c=[],d,f=this,g=!1,h={name:a,targetScope:f,stopPropagation:function(){g=!0},preventDefault:function(){h.defaultPrevented=!0},defaultPrevented:!1},k=[h].concat(ya.call(arguments,1)),m,l;do{d=f.$$listeners[a]||c;h.currentScope=f;m=0;for(l=d.length;m<l;m++)if(d[m])try{d[m].apply(null,
k)}catch(n){e(n)}else d.splice(m,1),m--,l--;if(g)break;f=f.$parent}while(f);return h},$broadcast:function(a,b){for(var c=this,d=this,f={name:a,targetScope:this,preventDefault:function(){f.defaultPrevented=!0},defaultPrevented:!1},g=[f].concat(ya.call(arguments,1)),h,k;c=d;){f.currentScope=c;d=c.$$listeners[a]||[];h=0;for(k=d.length;h<k;h++)if(d[h])try{d[h].apply(null,g)}catch(m){e(m)}else d.splice(h,1),h--,k--;if(!(d=c.$$listenerCount[a]&&c.$$childHead||c!==this&&c.$$nextSibling))for(;c!==this&&!(d=
c.$$nextSibling);)c=c.$parent}return f}};var p=new h;return p}]}function bd(){var b=/^\s*(https?|ftp|mailto|tel|file):/,a=/^\s*(https?|ftp|file):|data:image\//;this.aHrefSanitizationWhitelist=function(a){return z(a)?(b=a,this):b};this.imgSrcSanitizationWhitelist=function(b){return z(b)?(a=b,this):a};this.$get=function(){return function(c,d){var e=d?a:b,f;if(!S||8<=S)if(f=sa(c).href,""!==f&&!f.match(e))return"unsafe:"+f;return c}}}function Ae(b){if("self"===b)return b;if(C(b)){if(-1<b.indexOf("***"))throw ua("iwcard",
b);b=b.replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g,"\\$1").replace(/\x08/g,"\\x08").replace("\\*\\*",".*").replace("\\*","[^:/.?&;]*");return RegExp("^"+b+"$")}if(fb(b))return RegExp("^"+b.source+"$");throw ua("imatcher");}function Hc(b){var a=[];z(b)&&q(b,function(b){a.push(Ae(b))});return a}function ae(){this.SCE_CONTEXTS=ga;var b=["self"],a=[];this.resourceUrlWhitelist=function(a){arguments.length&&(b=Hc(a));return b};this.resourceUrlBlacklist=function(b){arguments.length&&(a=Hc(b));return a};this.$get=
["$injector",function(c){function d(a){var b=function(a){this.$$unwrapTrustedValue=function(){return a}};a&&(b.prototype=new a);b.prototype.valueOf=function(){return this.$$unwrapTrustedValue()};b.prototype.toString=function(){return this.$$unwrapTrustedValue().toString()};return b}var e=function(a){throw ua("unsafe");};c.has("$sanitize")&&(e=c.get("$sanitize"));var f=d(),g={};g[ga.HTML]=d(f);g[ga.CSS]=d(f);g[ga.URL]=d(f);g[ga.JS]=d(f);g[ga.RESOURCE_URL]=d(g[ga.URL]);return{trustAs:function(a,b){var c=
g.hasOwnProperty(a)?g[a]:null;if(!c)throw ua("icontext",a,b);if(null===b||b===s||""===b)return b;if("string"!==typeof b)throw ua("itype",a);return new c(b)},getTrusted:function(c,d){if(null===d||d===s||""===d)return d;var f=g.hasOwnProperty(c)?g[c]:null;if(f&&d instanceof f)return d.$$unwrapTrustedValue();if(c===ga.RESOURCE_URL){var f=sa(d.toString()),l,n,p=!1;l=0;for(n=b.length;l<n;l++)if("self"===b[l]?Mb(f):b[l].exec(f.href)){p=!0;break}if(p)for(l=0,n=a.length;l<n;l++)if("self"===a[l]?Mb(f):a[l].exec(f.href)){p=
!1;break}if(p)return d;throw ua("insecurl",d.toString());}if(c===ga.HTML)return e(d);throw ua("unsafe");},valueOf:function(a){return a instanceof f?a.$$unwrapTrustedValue():a}}}]}function $d(){var b=!0;this.enabled=function(a){arguments.length&&(b=!!a);return b};this.$get=["$parse","$sniffer","$sceDelegate",function(a,c,d){if(b&&c.msie&&8>c.msieDocumentMode)throw ua("iequirks");var e=ka(ga);e.isEnabled=function(){return b};e.trustAs=d.trustAs;e.getTrusted=d.getTrusted;e.valueOf=d.valueOf;b||(e.trustAs=
e.getTrusted=function(a,b){return b},e.valueOf=Ea);e.parseAs=function(b,c){var d=a(c);return d.literal&&d.constant?d:function(a,c){return e.getTrusted(b,d(a,c))}};var f=e.parseAs,g=e.getTrusted,h=e.trustAs;q(ga,function(a,b){var c=J(b);e[Ua("parse_as_"+c)]=function(b){return f(a,b)};e[Ua("get_trusted_"+c)]=function(b){return g(a,b)};e[Ua("trust_as_"+c)]=function(b){return h(a,b)}});return e}]}function be(){this.$get=["$window","$document",function(b,a){var c={},d=Y((/android (\d+)/.exec(J((b.navigator||
{}).userAgent))||[])[1]),e=/Boxee/i.test((b.navigator||{}).userAgent),f=a[0]||{},g=f.documentMode,h,m=/^(Moz|webkit|O|ms)(?=[A-Z])/,k=f.body&&f.body.style,l=!1,n=!1;if(k){for(var p in k)if(l=m.exec(p)){h=l[0];h=h.substr(0,1).toUpperCase()+h.substr(1);break}h||(h="WebkitOpacity"in k&&"webkit");l=!!("transition"in k||h+"Transition"in k);n=!!("animation"in k||h+"Animation"in k);!d||l&&n||(l=C(f.body.style.webkitTransition),n=C(f.body.style.webkitAnimation))}return{history:!(!b.history||!b.history.pushState||
4>d||e),hashchange:"onhashchange"in b&&(!g||7<g),hasEvent:function(a){if("input"==a&&9==S)return!1;if(H(c[a])){var b=f.createElement("div");c[a]="on"+a in b}return c[a]},csp:$b(),vendorPrefix:h,transitions:l,animations:n,android:d,msie:S,msieDocumentMode:g}}]}function de(){this.$get=["$rootScope","$browser","$q","$exceptionHandler",function(b,a,c,d){function e(e,h,m){var k=c.defer(),l=k.promise,n=z(m)&&!m;h=a.defer(function(){try{k.resolve(e())}catch(a){k.reject(a),d(a)}finally{delete f[l.$$timeoutId]}n||
b.$apply()},h);l.$$timeoutId=h;f[h]=k;return l}var f={};e.cancel=function(b){return b&&b.$$timeoutId in f?(f[b.$$timeoutId].reject("canceled"),delete f[b.$$timeoutId],a.defer.cancel(b.$$timeoutId)):!1};return e}]}function sa(b,a){var c=b;S&&(X.setAttribute("href",c),c=X.href);X.setAttribute("href",c);return{href:X.href,protocol:X.protocol?X.protocol.replace(/:$/,""):"",host:X.host,search:X.search?X.search.replace(/^\?/,""):"",hash:X.hash?X.hash.replace(/^#/,""):"",hostname:X.hostname,port:X.port,
pathname:"/"===X.pathname.charAt(0)?X.pathname:"/"+X.pathname}}function Mb(b){b=C(b)?sa(b):b;return b.protocol===Ic.protocol&&b.host===Ic.host}function ee(){this.$get=aa(P)}function kc(b){function a(d,e){if(U(d)){var f={};q(d,function(b,c){f[c]=a(c,b)});return f}return b.factory(d+c,e)}var c="Filter";this.register=a;this.$get=["$injector",function(a){return function(b){return a.get(b+c)}}];a("currency",Jc);a("date",Kc);a("filter",Be);a("json",Ce);a("limitTo",De);a("lowercase",Ee);a("number",Lc);a("orderBy",
Mc);a("uppercase",Fe)}function Be(){return function(b,a,c){if(!K(b))return b;var d=typeof c,e=[];e.check=function(a){for(var b=0;b<e.length;b++)if(!e[b](a))return!1;return!0};"function"!==d&&(c="boolean"===d&&c?function(a,b){return Ra.equals(a,b)}:function(a,b){if(a&&b&&"object"===typeof a&&"object"===typeof b){for(var d in a)if("$"!==d.charAt(0)&&Ab.call(a,d)&&c(a[d],b[d]))return!0;return!1}b=(""+b).toLowerCase();return-1<(""+a).toLowerCase().indexOf(b)});var f=function(a,b){if("string"==typeof b&&
"!"===b.charAt(0))return!f(a,b.substr(1));switch(typeof a){case "boolean":case "number":case "string":return c(a,b);case "object":switch(typeof b){case "object":return c(a,b);default:for(var d in a)if("$"!==d.charAt(0)&&f(a[d],b))return!0}return!1;case "array":for(d=0;d<a.length;d++)if(f(a[d],b))return!0;return!1;default:return!1}};switch(typeof a){case "boolean":case "number":case "string":a={$:a};case "object":for(var g in a)(function(b){"undefined"!=typeof a[b]&&e.push(function(c){return f("$"==
b?c:c&&c[b],a[b])})})(g);break;case "function":e.push(a);break;default:return b}d=[];for(g=0;g<b.length;g++){var h=b[g];e.check(h)&&d.push(h)}return d}}function Jc(b){var a=b.NUMBER_FORMATS;return function(b,d){H(d)&&(d=a.CURRENCY_SYM);return Nc(b,a.PATTERNS[1],a.GROUP_SEP,a.DECIMAL_SEP,2).replace(/\u00A4/g,d)}}function Lc(b){var a=b.NUMBER_FORMATS;return function(b,d){return Nc(b,a.PATTERNS[0],a.GROUP_SEP,a.DECIMAL_SEP,d)}}function Nc(b,a,c,d,e){if(null==b||!isFinite(b)||U(b))return"";var f=0>b;
b=Math.abs(b);var g=b+"",h="",m=[],k=!1;if(-1!==g.indexOf("e")){var l=g.match(/([\d\.]+)e(-?)(\d+)/);l&&"-"==l[2]&&l[3]>e+1?g="0":(h=g,k=!0)}if(k)0<e&&(-1<b&&1>b)&&(h=b.toFixed(e));else{g=(g.split(Oc)[1]||"").length;H(e)&&(e=Math.min(Math.max(a.minFrac,g),a.maxFrac));g=Math.pow(10,e+1);b=Math.floor(b*g+5)/g;b=(""+b).split(Oc);g=b[0];b=b[1]||"";var l=0,n=a.lgSize,p=a.gSize;if(g.length>=n+p)for(l=g.length-n,k=0;k<l;k++)0===(l-k)%p&&0!==k&&(h+=c),h+=g.charAt(k);for(k=l;k<g.length;k++)0===(g.length-k)%
n&&0!==k&&(h+=c),h+=g.charAt(k);for(;b.length<e;)b+="0";e&&"0"!==e&&(h+=d+b.substr(0,e))}m.push(f?a.negPre:a.posPre);m.push(h);m.push(f?a.negSuf:a.posSuf);return m.join("")}function Ub(b,a,c){var d="";0>b&&(d="-",b=-b);for(b=""+b;b.length<a;)b="0"+b;c&&(b=b.substr(b.length-a));return d+b}function $(b,a,c,d){c=c||0;return function(e){e=e["get"+b]();if(0<c||e>-c)e+=c;0===e&&-12==c&&(e=12);return Ub(e,a,d)}}function tb(b,a){return function(c,d){var e=c["get"+b](),f=Ga(a?"SHORT"+b:b);return d[f][e]}}
function Kc(b){function a(a){var b;if(b=a.match(c)){a=new Date(0);var f=0,g=0,h=b[8]?a.setUTCFullYear:a.setFullYear,m=b[8]?a.setUTCHours:a.setHours;b[9]&&(f=Y(b[9]+b[10]),g=Y(b[9]+b[11]));h.call(a,Y(b[1]),Y(b[2])-1,Y(b[3]));f=Y(b[4]||0)-f;g=Y(b[5]||0)-g;h=Y(b[6]||0);b=Math.round(1E3*parseFloat("0."+(b[7]||0)));m.call(a,f,g,h,b)}return a}var c=/^(\d{4})-?(\d\d)-?(\d\d)(?:T(\d\d)(?::?(\d\d)(?::?(\d\d)(?:\.(\d+))?)?)?(Z|([+-])(\d\d):?(\d\d))?)?$/;return function(c,e){var f="",g=[],h,m;e=e||"mediumDate";
e=b.DATETIME_FORMATS[e]||e;C(c)&&(c=Ge.test(c)?Y(c):a(c));zb(c)&&(c=new Date(c));if(!Ma(c))return c;for(;e;)(m=He.exec(e))?(g=g.concat(ya.call(m,1)),e=g.pop()):(g.push(e),e=null);q(g,function(a){h=Ie[a];f+=h?h(c,b.DATETIME_FORMATS):a.replace(/(^'|'$)/g,"").replace(/''/g,"'")});return f}}function Ce(){return function(b){return qa(b,!0)}}function De(){return function(b,a){if(!K(b)&&!C(b))return b;a=Infinity===Math.abs(Number(a))?Number(a):Y(a);if(C(b))return a?0<=a?b.slice(0,a):b.slice(a,b.length):
"";var c=[],d,e;a>b.length?a=b.length:a<-b.length&&(a=-b.length);0<a?(d=0,e=a):(d=b.length+a,e=b.length);for(;d<e;d++)c.push(b[d]);return c}}function Mc(b){return function(a,c,d){function e(a,b){return Qa(b)?function(b,c){return a(c,b)}:a}function f(a,b){var c=typeof a,d=typeof b;return c==d?("string"==c&&(a=a.toLowerCase(),b=b.toLowerCase()),a===b?0:a<b?-1:1):c<d?-1:1}if(!K(a)||!c)return a;c=K(c)?c:[c];c=Uc(c,function(a){var c=!1,d=a||Ea;if(C(a)){if("+"==a.charAt(0)||"-"==a.charAt(0))c="-"==a.charAt(0),
a=a.substring(1);d=b(a);if(d.constant){var g=d();return e(function(a,b){return f(a[g],b[g])},c)}}return e(function(a,b){return f(d(a),d(b))},c)});for(var g=[],h=0;h<a.length;h++)g.push(a[h]);return g.sort(e(function(a,b){for(var d=0;d<c.length;d++){var e=c[d](a,b);if(0!==e)return e}return 0},d))}}function va(b){Q(b)&&(b={link:b});b.restrict=b.restrict||"AC";return aa(b)}function Pc(b,a,c,d){function e(a,c){c=c?"-"+ib(c,"-"):"";d.removeClass(b,(a?ub:vb)+c);d.addClass(b,(a?vb:ub)+c)}var f=this,g=b.parent().controller("form")||
wb,h=0,m=f.$error={},k=[];f.$name=a.name||a.ngForm;f.$dirty=!1;f.$pristine=!0;f.$valid=!0;f.$invalid=!1;g.$addControl(f);b.addClass(La);e(!0);f.$addControl=function(a){Aa(a.$name,"input");k.push(a);a.$name&&(f[a.$name]=a)};f.$removeControl=function(a){a.$name&&f[a.$name]===a&&delete f[a.$name];q(m,function(b,c){f.$setValidity(c,!0,a)});Oa(k,a)};f.$setValidity=function(a,b,c){var d=m[a];if(b)d&&(Oa(d,c),d.length||(h--,h||(e(b),f.$valid=!0,f.$invalid=!1),m[a]=!1,e(!0,a),g.$setValidity(a,!0,f)));else{h||
e(b);if(d){if(-1!=Na(d,c))return}else m[a]=d=[],h++,e(!1,a),g.$setValidity(a,!1,f);d.push(c);f.$valid=!1;f.$invalid=!0}};f.$setDirty=function(){d.removeClass(b,La);d.addClass(b,xb);f.$dirty=!0;f.$pristine=!1;g.$setDirty()};f.$setPristine=function(){d.removeClass(b,xb);d.addClass(b,La);f.$dirty=!1;f.$pristine=!0;q(k,function(a){a.$setPristine()})}}function pa(b,a,c,d){b.$setValidity(a,c);return c?d:s}function Je(b,a,c){var d=c.prop("validity");U(d)&&b.$parsers.push(function(c){if(b.$error[a]||!(d.badInput||
d.customError||d.typeMismatch)||d.valueMissing)return c;b.$setValidity(a,!1)})}function yb(b,a,c,d,e,f){var g=a.prop("validity"),h=a[0].placeholder,m={};if(!e.android){var k=!1;a.on("compositionstart",function(a){k=!0});a.on("compositionend",function(){k=!1;l()})}var l=function(e){if(!k){var f=a.val();if(S&&"input"===(e||m).type&&a[0].placeholder!==h)h=a[0].placeholder;else if(Qa(c.ngTrim||"T")&&(f=ba(f)),d.$viewValue!==f||g&&""===f&&!g.valueMissing)b.$$phase?d.$setViewValue(f):b.$apply(function(){d.$setViewValue(f)})}};
if(e.hasEvent("input"))a.on("input",l);else{var n,p=function(){n||(n=f.defer(function(){l();n=null}))};a.on("keydown",function(a){a=a.keyCode;91===a||(15<a&&19>a||37<=a&&40>=a)||p()});if(e.hasEvent("paste"))a.on("paste cut",p)}a.on("change",l);d.$render=function(){a.val(d.$isEmpty(d.$viewValue)?"":d.$viewValue)};var r=c.ngPattern;r&&((e=r.match(/^\/(.*)\/([gim]*)$/))?(r=RegExp(e[1],e[2]),e=function(a){return pa(d,"pattern",d.$isEmpty(a)||r.test(a),a)}):e=function(c){var e=b.$eval(r);if(!e||!e.test)throw u("ngPattern")("noregexp",
r,e,ha(a));return pa(d,"pattern",d.$isEmpty(c)||e.test(c),c)},d.$formatters.push(e),d.$parsers.push(e));if(c.ngMinlength){var q=Y(c.ngMinlength);e=function(a){return pa(d,"minlength",d.$isEmpty(a)||a.length>=q,a)};d.$parsers.push(e);d.$formatters.push(e)}if(c.ngMaxlength){var D=Y(c.ngMaxlength);e=function(a){return pa(d,"maxlength",d.$isEmpty(a)||a.length<=D,a)};d.$parsers.push(e);d.$formatters.push(e)}}function Vb(b,a){b="ngClass"+b;return["$animate",function(c){function d(a,b){var c=[],d=0;a:for(;d<
a.length;d++){for(var e=a[d],l=0;l<b.length;l++)if(e==b[l])continue a;c.push(e)}return c}function e(a){if(!K(a)){if(C(a))return a.split(" ");if(U(a)){var b=[];q(a,function(a,c){a&&(b=b.concat(c.split(" ")))});return b}}return a}return{restrict:"AC",link:function(f,g,h){function m(a,b){var c=g.data("$classCounts")||{},d=[];q(a,function(a){if(0<b||c[a])c[a]=(c[a]||0)+b,c[a]===+(0<b)&&d.push(a)});g.data("$classCounts",c);return d.join(" ")}function k(b){if(!0===a||f.$index%2===a){var k=e(b||[]);if(!l){var r=
m(k,1);h.$addClass(r)}else if(!xa(b,l)){var q=e(l),r=d(k,q),k=d(q,k),k=m(k,-1),r=m(r,1);0===r.length?c.removeClass(g,k):0===k.length?c.addClass(g,r):c.setClass(g,r,k)}}l=ka(b)}var l;f.$watch(h[b],k,!0);h.$observe("class",function(a){k(f.$eval(h[b]))});"ngClass"!==b&&f.$watch("$index",function(c,d){var g=c&1;if(g!==(d&1)){var k=e(f.$eval(h[b]));g===a?(g=m(k,1),h.$addClass(g)):(g=m(k,-1),h.$removeClass(g))}})}}}]}var J=function(b){return C(b)?b.toLowerCase():b},Ab=Object.prototype.hasOwnProperty,Ga=
function(b){return C(b)?b.toUpperCase():b},S,y,Ba,ya=[].slice,Ke=[].push,wa=Object.prototype.toString,Pa=u("ng"),Ra=P.angular||(P.angular={}),Ta,Ka,ja=["0","0","0"];S=Y((/msie (\d+)/.exec(J(navigator.userAgent))||[])[1]);isNaN(S)&&(S=Y((/trident\/.*; rv:(\d+)/.exec(J(navigator.userAgent))||[])[1]));w.$inject=[];Ea.$inject=[];var ba=function(){return String.prototype.trim?function(b){return C(b)?b.trim():b}:function(b){return C(b)?b.replace(/^\s\s*/,"").replace(/\s\s*$/,""):b}}();Ka=9>S?function(b){b=
b.nodeName?b:b[0];return b.scopeName&&"HTML"!=b.scopeName?Ga(b.scopeName+":"+b.nodeName):b.nodeName}:function(b){return b.nodeName?b.nodeName:b[0].nodeName};var Xc=/[A-Z]/g,$c={full:"1.2.17",major:1,minor:2,dot:17,codeName:"quantum-disentanglement"},Wa=M.cache={},jb=M.expando="ng"+(new Date).getTime(),me=1,qb=P.document.addEventListener?function(b,a,c){b.addEventListener(a,c,!1)}:function(b,a,c){b.attachEvent("on"+a,c)},Va=P.document.removeEventListener?function(b,a,c){b.removeEventListener(a,c,!1)}:
function(b,a,c){b.detachEvent("on"+a,c)};M._data=function(b){return this.cache[b[this.expando]]||{}};var he=/([\:\-\_]+(.))/g,ie=/^moz([A-Z])/,Gb=u("jqLite"),je=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,Hb=/<|&#?\w+;/,ke=/<([\w:]+)/,le=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,da={option:[1,'<select multiple="multiple">',"</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>",
"</tr></tbody></table>"],_default:[0,"",""]};da.optgroup=da.option;da.tbody=da.tfoot=da.colgroup=da.caption=da.thead;da.th=da.td;var Ja=M.prototype={ready:function(b){function a(){c||(c=!0,b())}var c=!1;"complete"===V.readyState?setTimeout(a):(this.on("DOMContentLoaded",a),M(P).on("load",a))},toString:function(){var b=[];q(this,function(a){b.push(""+a)});return"["+b.join(", ")+"]"},eq:function(b){return 0<=b?y(this[b]):y(this[this.length+b])},length:0,push:Ke,sort:[].sort,splice:[].splice},nb={};
q("multiple selected checked disabled readOnly required open".split(" "),function(b){nb[J(b)]=b});var rc={};q("input select option textarea button form details".split(" "),function(b){rc[Ga(b)]=!0});q({data:nc,inheritedData:mb,scope:function(b){return y(b).data("$scope")||mb(b.parentNode||b,["$isolateScope","$scope"])},isolateScope:function(b){return y(b).data("$isolateScope")||y(b).data("$isolateScopeNoTemplate")},controller:oc,injector:function(b){return mb(b,"$injector")},removeAttr:function(b,
a){b.removeAttribute(a)},hasClass:Kb,css:function(b,a,c){a=Ua(a);if(z(c))b.style[a]=c;else{var d;8>=S&&(d=b.currentStyle&&b.currentStyle[a],""===d&&(d="auto"));d=d||b.style[a];8>=S&&(d=""===d?s:d);return d}},attr:function(b,a,c){var d=J(a);if(nb[d])if(z(c))c?(b[a]=!0,b.setAttribute(a,d)):(b[a]=!1,b.removeAttribute(d));else return b[a]||(b.attributes.getNamedItem(a)||w).specified?d:s;else if(z(c))b.setAttribute(a,c);else if(b.getAttribute)return b=b.getAttribute(a,2),null===b?s:b},prop:function(b,
a,c){if(z(c))b[a]=c;else return b[a]},text:function(){function b(b,d){var e=a[b.nodeType];if(H(d))return e?b[e]:"";b[e]=d}var a=[];9>S?(a[1]="innerText",a[3]="nodeValue"):a[1]=a[3]="textContent";b.$dv="";return b}(),val:function(b,a){if(H(a)){if("SELECT"===Ka(b)&&b.multiple){var c=[];q(b.options,function(a){a.selected&&c.push(a.value||a.text)});return 0===c.length?null:c}return b.value}b.value=a},html:function(b,a){if(H(a))return b.innerHTML;for(var c=0,d=b.childNodes;c<d.length;c++)Ha(d[c]);b.innerHTML=
a},empty:pc},function(b,a){M.prototype[a]=function(a,d){var e,f;if(b!==pc&&(2==b.length&&b!==Kb&&b!==oc?a:d)===s){if(U(a)){for(e=0;e<this.length;e++)if(b===nc)b(this[e],a);else for(f in a)b(this[e],f,a[f]);return this}e=b.$dv;f=e===s?Math.min(this.length,1):this.length;for(var g=0;g<f;g++){var h=b(this[g],a,d);e=e?e+h:h}return e}for(e=0;e<this.length;e++)b(this[e],a,d);return this}});q({removeData:lc,dealoc:Ha,on:function a(c,d,e,f){if(z(f))throw Gb("onargs");var g=la(c,"events"),h=la(c,"handle");
g||la(c,"events",g={});h||la(c,"handle",h=ne(c,g));q(d.split(" "),function(d){var f=g[d];if(!f){if("mouseenter"==d||"mouseleave"==d){var l=V.body.contains||V.body.compareDocumentPosition?function(a,c){var d=9===a.nodeType?a.documentElement:a,e=c&&c.parentNode;return a===e||!!(e&&1===e.nodeType&&(d.contains?d.contains(e):a.compareDocumentPosition&&a.compareDocumentPosition(e)&16))}:function(a,c){if(c)for(;c=c.parentNode;)if(c===a)return!0;return!1};g[d]=[];a(c,{mouseleave:"mouseout",mouseenter:"mouseover"}[d],
function(a){var c=a.relatedTarget;c&&(c===this||l(this,c))||h(a,d)})}else qb(c,d,h),g[d]=[];f=g[d]}f.push(e)})},off:mc,one:function(a,c,d){a=y(a);a.on(c,function f(){a.off(c,d);a.off(c,f)});a.on(c,d)},replaceWith:function(a,c){var d,e=a.parentNode;Ha(a);q(new M(c),function(c){d?e.insertBefore(c,d.nextSibling):e.replaceChild(c,a);d=c})},children:function(a){var c=[];q(a.childNodes,function(a){1===a.nodeType&&c.push(a)});return c},contents:function(a){return a.contentDocument||a.childNodes||[]},append:function(a,
c){q(new M(c),function(c){1!==a.nodeType&&11!==a.nodeType||a.appendChild(c)})},prepend:function(a,c){if(1===a.nodeType){var d=a.firstChild;q(new M(c),function(c){a.insertBefore(c,d)})}},wrap:function(a,c){c=y(c)[0];var d=a.parentNode;d&&d.replaceChild(c,a);c.appendChild(a)},remove:function(a){Ha(a);var c=a.parentNode;c&&c.removeChild(a)},after:function(a,c){var d=a,e=a.parentNode;q(new M(c),function(a){e.insertBefore(a,d.nextSibling);d=a})},addClass:lb,removeClass:kb,toggleClass:function(a,c,d){c&&
q(c.split(" "),function(c){var f=d;H(f)&&(f=!Kb(a,c));(f?lb:kb)(a,c)})},parent:function(a){return(a=a.parentNode)&&11!==a.nodeType?a:null},next:function(a){if(a.nextElementSibling)return a.nextElementSibling;for(a=a.nextSibling;null!=a&&1!==a.nodeType;)a=a.nextSibling;return a},find:function(a,c){return a.getElementsByTagName?a.getElementsByTagName(c):[]},clone:Jb,triggerHandler:function(a,c,d){c=(la(a,"events")||{})[c];d=d||[];var e=[{preventDefault:w,stopPropagation:w}];q(c,function(c){c.apply(a,
e.concat(d))})}},function(a,c){M.prototype[c]=function(c,e,f){for(var g,h=0;h<this.length;h++)H(g)?(g=a(this[h],c,e,f),z(g)&&(g=y(g))):Ib(g,a(this[h],c,e,f));return z(g)?g:this};M.prototype.bind=M.prototype.on;M.prototype.unbind=M.prototype.off});Xa.prototype={put:function(a,c){this[Ia(a)]=c},get:function(a){return this[Ia(a)]},remove:function(a){var c=this[a=Ia(a)];delete this[a];return c}};var pe=/^function\s*[^\(]*\(\s*([^\)]*)\)/m,qe=/,/,re=/^\s*(_?)(\S+?)\1\s*$/,oe=/((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg,
Ya=u("$injector"),Le=u("$animate"),Ld=["$provide",function(a){this.$$selectors={};this.register=function(c,d){var e=c+"-animation";if(c&&"."!=c.charAt(0))throw Le("notcsel",c);this.$$selectors[c.substr(1)]=e;a.factory(e,d)};this.classNameFilter=function(a){1===arguments.length&&(this.$$classNameFilter=a instanceof RegExp?a:null);return this.$$classNameFilter};this.$get=["$timeout","$$asyncCallback",function(a,d){return{enter:function(a,c,g,h){g?g.after(a):(c&&c[0]||(c=g.parent()),c.append(a));h&&
d(h)},leave:function(a,c){a.remove();c&&d(c)},move:function(a,c,d,h){this.enter(a,c,d,h)},addClass:function(a,c,g){c=C(c)?c:K(c)?c.join(" "):"";q(a,function(a){lb(a,c)});g&&d(g)},removeClass:function(a,c,g){c=C(c)?c:K(c)?c.join(" "):"";q(a,function(a){kb(a,c)});g&&d(g)},setClass:function(a,c,g,h){q(a,function(a){lb(a,c);kb(a,g)});h&&d(h)},enabled:w}}]}],ia=u("$compile");gc.$inject=["$provide","$$sanitizeUriProvider"];var te=/^(x[\:\-_]|data[\:\-_])/i,zc=u("$interpolate"),Me=/^([^\?#]*)(\?([^#]*))?(#(.*))?$/,
we={http:80,https:443,ftp:21},Pb=u("$location");Rb.prototype=Qb.prototype=Cc.prototype={$$html5:!1,$$replace:!1,absUrl:rb("$$absUrl"),url:function(a,c){if(H(a))return this.$$url;var d=Me.exec(a);d[1]&&this.path(decodeURIComponent(d[1]));(d[2]||d[1])&&this.search(d[3]||"");this.hash(d[5]||"",c);return this},protocol:rb("$$protocol"),host:rb("$$host"),port:rb("$$port"),path:Dc("$$path",function(a){return"/"==a.charAt(0)?a:"/"+a}),search:function(a,c){switch(arguments.length){case 0:return this.$$search;
case 1:if(C(a))this.$$search=cc(a);else if(U(a))this.$$search=a;else throw Pb("isrcharg");break;default:H(c)||null===c?delete this.$$search[a]:this.$$search[a]=c}this.$$compose();return this},hash:Dc("$$hash",Ea),replace:function(){this.$$replace=!0;return this}};var Ca=u("$parse"),Gc={},ta,cb={"null":function(){return null},"true":function(){return!0},"false":function(){return!1},undefined:w,"+":function(a,c,d,e){d=d(a,c);e=e(a,c);return z(d)?z(e)?d+e:d:z(e)?e:s},"-":function(a,c,d,e){d=d(a,c);e=
e(a,c);return(z(d)?d:0)-(z(e)?e:0)},"*":function(a,c,d,e){return d(a,c)*e(a,c)},"/":function(a,c,d,e){return d(a,c)/e(a,c)},"%":function(a,c,d,e){return d(a,c)%e(a,c)},"^":function(a,c,d,e){return d(a,c)^e(a,c)},"=":w,"===":function(a,c,d,e){return d(a,c)===e(a,c)},"!==":function(a,c,d,e){return d(a,c)!==e(a,c)},"==":function(a,c,d,e){return d(a,c)==e(a,c)},"!=":function(a,c,d,e){return d(a,c)!=e(a,c)},"<":function(a,c,d,e){return d(a,c)<e(a,c)},">":function(a,c,d,e){return d(a,c)>e(a,c)},"<=":function(a,
c,d,e){return d(a,c)<=e(a,c)},">=":function(a,c,d,e){return d(a,c)>=e(a,c)},"&&":function(a,c,d,e){return d(a,c)&&e(a,c)},"||":function(a,c,d,e){return d(a,c)||e(a,c)},"&":function(a,c,d,e){return d(a,c)&e(a,c)},"|":function(a,c,d,e){return e(a,c)(a,c,d(a,c))},"!":function(a,c,d){return!d(a,c)}},Ne={n:"\n",f:"\f",r:"\r",t:"\t",v:"\v","'":"'",'"':'"'},Tb=function(a){this.options=a};Tb.prototype={constructor:Tb,lex:function(a){this.text=a;this.index=0;this.ch=s;this.lastCh=":";for(this.tokens=[];this.index<
this.text.length;){this.ch=this.text.charAt(this.index);if(this.is("\"'"))this.readString(this.ch);else if(this.isNumber(this.ch)||this.is(".")&&this.isNumber(this.peek()))this.readNumber();else if(this.isIdent(this.ch))this.readIdent();else if(this.is("(){}[].,;:?"))this.tokens.push({index:this.index,text:this.ch}),this.index++;else if(this.isWhitespace(this.ch)){this.index++;continue}else{a=this.ch+this.peek();var c=a+this.peek(2),d=cb[this.ch],e=cb[a],f=cb[c];f?(this.tokens.push({index:this.index,
text:c,fn:f}),this.index+=3):e?(this.tokens.push({index:this.index,text:a,fn:e}),this.index+=2):d?(this.tokens.push({index:this.index,text:this.ch,fn:d}),this.index+=1):this.throwError("Unexpected next character ",this.index,this.index+1)}this.lastCh=this.ch}return this.tokens},is:function(a){return-1!==a.indexOf(this.ch)},was:function(a){return-1!==a.indexOf(this.lastCh)},peek:function(a){a=a||1;return this.index+a<this.text.length?this.text.charAt(this.index+a):!1},isNumber:function(a){return"0"<=
a&&"9">=a},isWhitespace:function(a){return" "===a||"\r"===a||"\t"===a||"\n"===a||"\v"===a||"\u00a0"===a},isIdent:function(a){return"a"<=a&&"z">=a||"A"<=a&&"Z">=a||"_"===a||"$"===a},isExpOperator:function(a){return"-"===a||"+"===a||this.isNumber(a)},throwError:function(a,c,d){d=d||this.index;c=z(c)?"s "+c+"-"+this.index+" ["+this.text.substring(c,d)+"]":" "+d;throw Ca("lexerr",a,c,this.text);},readNumber:function(){for(var a="",c=this.index;this.index<this.text.length;){var d=J(this.text.charAt(this.index));
if("."==d||this.isNumber(d))a+=d;else{var e=this.peek();if("e"==d&&this.isExpOperator(e))a+=d;else if(this.isExpOperator(d)&&e&&this.isNumber(e)&&"e"==a.charAt(a.length-1))a+=d;else if(!this.isExpOperator(d)||e&&this.isNumber(e)||"e"!=a.charAt(a.length-1))break;else this.throwError("Invalid exponent")}this.index++}a*=1;this.tokens.push({index:c,text:a,literal:!0,constant:!0,fn:function(){return a}})},readIdent:function(){for(var a=this,c="",d=this.index,e,f,g,h;this.index<this.text.length;){h=this.text.charAt(this.index);
if("."===h||this.isIdent(h)||this.isNumber(h))"."===h&&(e=this.index),c+=h;else break;this.index++}if(e)for(f=this.index;f<this.text.length;){h=this.text.charAt(f);if("("===h){g=c.substr(e-d+1);c=c.substr(0,e-d);this.index=f;break}if(this.isWhitespace(h))f++;else break}d={index:d,text:c};if(cb.hasOwnProperty(c))d.fn=cb[c],d.literal=!0,d.constant=!0;else{var m=Fc(c,this.options,this.text);d.fn=E(function(a,c){return m(a,c)},{assign:function(d,e){return sb(d,c,e,a.text,a.options)}})}this.tokens.push(d);
g&&(this.tokens.push({index:e,text:"."}),this.tokens.push({index:e+1,text:g}))},readString:function(a){var c=this.index;this.index++;for(var d="",e=a,f=!1;this.index<this.text.length;){var g=this.text.charAt(this.index),e=e+g;if(f)"u"===g?(g=this.text.substring(this.index+1,this.index+5),g.match(/[\da-f]{4}/i)||this.throwError("Invalid unicode escape [\\u"+g+"]"),this.index+=4,d+=String.fromCharCode(parseInt(g,16))):d=(f=Ne[g])?d+f:d+g,f=!1;else if("\\"===g)f=!0;else{if(g===a){this.index++;this.tokens.push({index:c,
text:e,string:d,literal:!0,constant:!0,fn:function(){return d}});return}d+=g}this.index++}this.throwError("Unterminated quote",c)}};var bb=function(a,c,d){this.lexer=a;this.$filter=c;this.options=d};bb.ZERO=E(function(){return 0},{constant:!0});bb.prototype={constructor:bb,parse:function(a){this.text=a;this.tokens=this.lexer.lex(a);a=this.statements();0!==this.tokens.length&&this.throwError("is an unexpected token",this.tokens[0]);a.literal=!!a.literal;a.constant=!!a.constant;return a},primary:function(){var a;
if(this.expect("("))a=this.filterChain(),this.consume(")");else if(this.expect("["))a=this.arrayDeclaration();else if(this.expect("{"))a=this.object();else{var c=this.expect();(a=c.fn)||this.throwError("not a primary expression",c);a.literal=!!c.literal;a.constant=!!c.constant}for(var d;c=this.expect("(","[",".");)"("===c.text?(a=this.functionCall(a,d),d=null):"["===c.text?(d=a,a=this.objectIndex(a)):"."===c.text?(d=a,a=this.fieldAccess(a)):this.throwError("IMPOSSIBLE");return a},throwError:function(a,
c){throw Ca("syntax",c.text,a,c.index+1,this.text,this.text.substring(c.index));},peekToken:function(){if(0===this.tokens.length)throw Ca("ueoe",this.text);return this.tokens[0]},peek:function(a,c,d,e){if(0<this.tokens.length){var f=this.tokens[0],g=f.text;if(g===a||g===c||g===d||g===e||!(a||c||d||e))return f}return!1},expect:function(a,c,d,e){return(a=this.peek(a,c,d,e))?(this.tokens.shift(),a):!1},consume:function(a){this.expect(a)||this.throwError("is unexpected, expecting ["+a+"]",this.peek())},
unaryFn:function(a,c){return E(function(d,e){return a(d,e,c)},{constant:c.constant})},ternaryFn:function(a,c,d){return E(function(e,f){return a(e,f)?c(e,f):d(e,f)},{constant:a.constant&&c.constant&&d.constant})},binaryFn:function(a,c,d){return E(function(e,f){return c(e,f,a,d)},{constant:a.constant&&d.constant})},statements:function(){for(var a=[];;)if(0<this.tokens.length&&!this.peek("}",")",";","]")&&a.push(this.filterChain()),!this.expect(";"))return 1===a.length?a[0]:function(c,d){for(var e,f=
0;f<a.length;f++){var g=a[f];g&&(e=g(c,d))}return e}},filterChain:function(){for(var a=this.expression(),c;;)if(c=this.expect("|"))a=this.binaryFn(a,c.fn,this.filter());else return a},filter:function(){for(var a=this.expect(),c=this.$filter(a.text),d=[];;)if(a=this.expect(":"))d.push(this.expression());else{var e=function(a,e,h){h=[h];for(var m=0;m<d.length;m++)h.push(d[m](a,e));return c.apply(a,h)};return function(){return e}}},expression:function(){return this.assignment()},assignment:function(){var a=
this.ternary(),c,d;return(d=this.expect("="))?(a.assign||this.throwError("implies assignment but ["+this.text.substring(0,d.index)+"] can not be assigned to",d),c=this.ternary(),function(d,f){return a.assign(d,c(d,f),f)}):a},ternary:function(){var a=this.logicalOR(),c,d;if(this.expect("?")){c=this.ternary();if(d=this.expect(":"))return this.ternaryFn(a,c,this.ternary());this.throwError("expected :",d)}else return a},logicalOR:function(){for(var a=this.logicalAND(),c;;)if(c=this.expect("||"))a=this.binaryFn(a,
c.fn,this.logicalAND());else return a},logicalAND:function(){var a=this.equality(),c;if(c=this.expect("&&"))a=this.binaryFn(a,c.fn,this.logicalAND());return a},equality:function(){var a=this.relational(),c;if(c=this.expect("==","!=","===","!=="))a=this.binaryFn(a,c.fn,this.equality());return a},relational:function(){var a=this.additive(),c;if(c=this.expect("<",">","<=",">="))a=this.binaryFn(a,c.fn,this.relational());return a},additive:function(){for(var a=this.multiplicative(),c;c=this.expect("+",
"-");)a=this.binaryFn(a,c.fn,this.multiplicative());return a},multiplicative:function(){for(var a=this.unary(),c;c=this.expect("*","/","%");)a=this.binaryFn(a,c.fn,this.unary());return a},unary:function(){var a;return this.expect("+")?this.primary():(a=this.expect("-"))?this.binaryFn(bb.ZERO,a.fn,this.unary()):(a=this.expect("!"))?this.unaryFn(a.fn,this.unary()):this.primary()},fieldAccess:function(a){var c=this,d=this.expect().text,e=Fc(d,this.options,this.text);return E(function(c,d,h){return e(h||
a(c,d))},{assign:function(e,g,h){return sb(a(e,h),d,g,c.text,c.options)}})},objectIndex:function(a){var c=this,d=this.expression();this.consume("]");return E(function(e,f){var g=a(e,f),h=d(e,f),m;if(!g)return s;(g=ab(g[h],c.text))&&(g.then&&c.options.unwrapPromises)&&(m=g,"$$v"in g||(m.$$v=s,m.then(function(a){m.$$v=a})),g=g.$$v);return g},{assign:function(e,f,g){var h=d(e,g);return ab(a(e,g),c.text)[h]=f}})},functionCall:function(a,c){var d=[];if(")"!==this.peekToken().text){do d.push(this.expression());
while(this.expect(","))}this.consume(")");var e=this;return function(f,g){for(var h=[],m=c?c(f,g):f,k=0;k<d.length;k++)h.push(d[k](f,g));k=a(f,g,m)||w;ab(m,e.text);ab(k,e.text);h=k.apply?k.apply(m,h):k(h[0],h[1],h[2],h[3],h[4]);return ab(h,e.text)}},arrayDeclaration:function(){var a=[],c=!0;if("]"!==this.peekToken().text){do{if(this.peek("]"))break;var d=this.expression();a.push(d);d.constant||(c=!1)}while(this.expect(","))}this.consume("]");return E(function(c,d){for(var g=[],h=0;h<a.length;h++)g.push(a[h](c,
d));return g},{literal:!0,constant:c})},object:function(){var a=[],c=!0;if("}"!==this.peekToken().text){do{if(this.peek("}"))break;var d=this.expect(),d=d.string||d.text;this.consume(":");var e=this.expression();a.push({key:d,value:e});e.constant||(c=!1)}while(this.expect(","))}this.consume("}");return E(function(c,d){for(var e={},m=0;m<a.length;m++){var k=a[m];e[k.key]=k.value(c,d)}return e},{literal:!0,constant:c})}};var Sb={},ua=u("$sce"),ga={HTML:"html",CSS:"css",URL:"url",RESOURCE_URL:"resourceUrl",
JS:"js"},X=V.createElement("a"),Ic=sa(P.location.href,!0);kc.$inject=["$provide"];Jc.$inject=["$locale"];Lc.$inject=["$locale"];var Oc=".",Ie={yyyy:$("FullYear",4),yy:$("FullYear",2,0,!0),y:$("FullYear",1),MMMM:tb("Month"),MMM:tb("Month",!0),MM:$("Month",2,1),M:$("Month",1,1),dd:$("Date",2),d:$("Date",1),HH:$("Hours",2),H:$("Hours",1),hh:$("Hours",2,-12),h:$("Hours",1,-12),mm:$("Minutes",2),m:$("Minutes",1),ss:$("Seconds",2),s:$("Seconds",1),sss:$("Milliseconds",3),EEEE:tb("Day"),EEE:tb("Day",!0),
a:function(a,c){return 12>a.getHours()?c.AMPMS[0]:c.AMPMS[1]},Z:function(a){a=-1*a.getTimezoneOffset();return a=(0<=a?"+":"")+(Ub(Math[0<a?"floor":"ceil"](a/60),2)+Ub(Math.abs(a%60),2))}},He=/((?:[^yMdHhmsaZE']+)|(?:'(?:[^']|'')*')|(?:E+|y+|M+|d+|H+|h+|m+|s+|a|Z))(.*)/,Ge=/^\-?\d+$/;Kc.$inject=["$locale"];var Ee=aa(J),Fe=aa(Ga);Mc.$inject=["$parse"];var cd=aa({restrict:"E",compile:function(a,c){8>=S&&(c.href||c.name||c.$set("href",""),a.append(V.createComment("IE fix")));if(!c.href&&!c.xlinkHref&&
!c.name)return function(a,c){var f="[object SVGAnimatedString]"===wa.call(c.prop("href"))?"xlink:href":"href";c.on("click",function(a){c.attr(f)||a.preventDefault()})}}}),Eb={};q(nb,function(a,c){if("multiple"!=a){var d=na("ng-"+c);Eb[d]=function(){return{priority:100,link:function(a,f,g){a.$watch(g[d],function(a){g.$set(c,!!a)})}}}}});q(["src","srcset","href"],function(a){var c=na("ng-"+a);Eb[c]=function(){return{priority:99,link:function(d,e,f){var g=a,h=a;"href"===a&&"[object SVGAnimatedString]"===
wa.call(e.prop("href"))&&(h="xlinkHref",f.$attr[h]="xlink:href",g=null);f.$observe(c,function(a){a&&(f.$set(h,a),S&&g&&e.prop(g,f[h]))})}}}});var wb={$addControl:w,$removeControl:w,$setValidity:w,$setDirty:w,$setPristine:w};Pc.$inject=["$element","$attrs","$scope","$animate"];var Qc=function(a){return["$timeout",function(c){return{name:"form",restrict:a?"EAC":"E",controller:Pc,compile:function(){return{pre:function(a,e,f,g){if(!f.action){var h=function(a){a.preventDefault?a.preventDefault():a.returnValue=
!1};qb(e[0],"submit",h);e.on("$destroy",function(){c(function(){Va(e[0],"submit",h)},0,!1)})}var m=e.parent().controller("form"),k=f.name||f.ngForm;k&&sb(a,k,g,k);if(m)e.on("$destroy",function(){m.$removeControl(g);k&&sb(a,k,s,k);E(g,wb)})}}}}}]},dd=Qc(),qd=Qc(!0),Oe=/^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/,Pe=/^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*$/i,Qe=/^\s*(\-|\+)?(\d+|(\d*(\.\d*)))\s*$/,Rc={text:yb,number:function(a,c,d,e,f,g){yb(a,
c,d,e,f,g);e.$parsers.push(function(a){var c=e.$isEmpty(a);if(c||Qe.test(a))return e.$setValidity("number",!0),""===a?null:c?a:parseFloat(a);e.$setValidity("number",!1);return s});Je(e,"number",c);e.$formatters.push(function(a){return e.$isEmpty(a)?"":""+a});d.min&&(a=function(a){var c=parseFloat(d.min);return pa(e,"min",e.$isEmpty(a)||a>=c,a)},e.$parsers.push(a),e.$formatters.push(a));d.max&&(a=function(a){var c=parseFloat(d.max);return pa(e,"max",e.$isEmpty(a)||a<=c,a)},e.$parsers.push(a),e.$formatters.push(a));
e.$formatters.push(function(a){return pa(e,"number",e.$isEmpty(a)||zb(a),a)})},url:function(a,c,d,e,f,g){yb(a,c,d,e,f,g);a=function(a){return pa(e,"url",e.$isEmpty(a)||Oe.test(a),a)};e.$formatters.push(a);e.$parsers.push(a)},email:function(a,c,d,e,f,g){yb(a,c,d,e,f,g);a=function(a){return pa(e,"email",e.$isEmpty(a)||Pe.test(a),a)};e.$formatters.push(a);e.$parsers.push(a)},radio:function(a,c,d,e){H(d.name)&&c.attr("name",eb());c.on("click",function(){c[0].checked&&a.$apply(function(){e.$setViewValue(d.value)})});
e.$render=function(){c[0].checked=d.value==e.$viewValue};d.$observe("value",e.$render)},checkbox:function(a,c,d,e){var f=d.ngTrueValue,g=d.ngFalseValue;C(f)||(f=!0);C(g)||(g=!1);c.on("click",function(){a.$apply(function(){e.$setViewValue(c[0].checked)})});e.$render=function(){c[0].checked=e.$viewValue};e.$isEmpty=function(a){return a!==f};e.$formatters.push(function(a){return a===f});e.$parsers.push(function(a){return a?f:g})},hidden:w,button:w,submit:w,reset:w,file:w},hc=["$browser","$sniffer",function(a,
c){return{restrict:"E",require:"?ngModel",link:function(d,e,f,g){g&&(Rc[J(f.type)]||Rc.text)(d,e,f,g,c,a)}}}],vb="ng-valid",ub="ng-invalid",La="ng-pristine",xb="ng-dirty",Re=["$scope","$exceptionHandler","$attrs","$element","$parse","$animate",function(a,c,d,e,f,g){function h(a,c){c=c?"-"+ib(c,"-"):"";g.removeClass(e,(a?ub:vb)+c);g.addClass(e,(a?vb:ub)+c)}this.$modelValue=this.$viewValue=Number.NaN;this.$parsers=[];this.$formatters=[];this.$viewChangeListeners=[];this.$pristine=!0;this.$dirty=!1;
this.$valid=!0;this.$invalid=!1;this.$name=d.name;var m=f(d.ngModel),k=m.assign;if(!k)throw u("ngModel")("nonassign",d.ngModel,ha(e));this.$render=w;this.$isEmpty=function(a){return H(a)||""===a||null===a||a!==a};var l=e.inheritedData("$formController")||wb,n=0,p=this.$error={};e.addClass(La);h(!0);this.$setValidity=function(a,c){p[a]!==!c&&(c?(p[a]&&n--,n||(h(!0),this.$valid=!0,this.$invalid=!1)):(h(!1),this.$invalid=!0,this.$valid=!1,n++),p[a]=!c,h(c,a),l.$setValidity(a,c,this))};this.$setPristine=
function(){this.$dirty=!1;this.$pristine=!0;g.removeClass(e,xb);g.addClass(e,La)};this.$setViewValue=function(d){this.$viewValue=d;this.$pristine&&(this.$dirty=!0,this.$pristine=!1,g.removeClass(e,La),g.addClass(e,xb),l.$setDirty());q(this.$parsers,function(a){d=a(d)});this.$modelValue!==d&&(this.$modelValue=d,k(a,d),q(this.$viewChangeListeners,function(a){try{a()}catch(d){c(d)}}))};var r=this;a.$watch(function(){var c=m(a);if(r.$modelValue!==c){var d=r.$formatters,e=d.length;for(r.$modelValue=c;e--;)c=
d[e](c);r.$viewValue!==c&&(r.$viewValue=c,r.$render())}return c})}],Fd=function(){return{require:["ngModel","^?form"],controller:Re,link:function(a,c,d,e){var f=e[0],g=e[1]||wb;g.$addControl(f);a.$on("$destroy",function(){g.$removeControl(f)})}}},Hd=aa({require:"ngModel",link:function(a,c,d,e){e.$viewChangeListeners.push(function(){a.$eval(d.ngChange)})}}),ic=function(){return{require:"?ngModel",link:function(a,c,d,e){if(e){d.required=!0;var f=function(a){if(d.required&&e.$isEmpty(a))e.$setValidity("required",
!1);else return e.$setValidity("required",!0),a};e.$formatters.push(f);e.$parsers.unshift(f);d.$observe("required",function(){f(e.$viewValue)})}}}},Gd=function(){return{require:"ngModel",link:function(a,c,d,e){var f=(a=/\/(.*)\//.exec(d.ngList))&&RegExp(a[1])||d.ngList||",";e.$parsers.push(function(a){if(!H(a)){var c=[];a&&q(a.split(f),function(a){a&&c.push(ba(a))});return c}});e.$formatters.push(function(a){return K(a)?a.join(", "):s});e.$isEmpty=function(a){return!a||!a.length}}}},Se=/^(true|false|\d+)$/,
Id=function(){return{priority:100,compile:function(a,c){return Se.test(c.ngValue)?function(a,c,f){f.$set("value",a.$eval(f.ngValue))}:function(a,c,f){a.$watch(f.ngValue,function(a){f.$set("value",a)})}}}},id=va(function(a,c,d){c.addClass("ng-binding").data("$binding",d.ngBind);a.$watch(d.ngBind,function(a){c.text(a==s?"":a)})}),kd=["$interpolate",function(a){return function(c,d,e){c=a(d.attr(e.$attr.ngBindTemplate));d.addClass("ng-binding").data("$binding",c);e.$observe("ngBindTemplate",function(a){d.text(a)})}}],
jd=["$sce","$parse",function(a,c){return function(d,e,f){e.addClass("ng-binding").data("$binding",f.ngBindHtml);var g=c(f.ngBindHtml);d.$watch(function(){return(g(d)||"").toString()},function(c){e.html(a.getTrustedHtml(g(d))||"")})}}],ld=Vb("",!0),nd=Vb("Odd",0),md=Vb("Even",1),od=va({compile:function(a,c){c.$set("ngCloak",s);a.removeClass("ng-cloak")}}),pd=[function(){return{scope:!0,controller:"@",priority:500}}],jc={};q("click dblclick mousedown mouseup mouseover mouseout mousemove mouseenter mouseleave keydown keyup keypress submit focus blur copy cut paste".split(" "),
function(a){var c=na("ng-"+a);jc[c]=["$parse",function(d){return{compile:function(e,f){var g=d(f[c]);return function(c,d,e){d.on(J(a),function(a){c.$apply(function(){g(c,{$event:a})})})}}}}]});var sd=["$animate",function(a){return{transclude:"element",priority:600,terminal:!0,restrict:"A",$$tlb:!0,link:function(c,d,e,f,g){var h,m,k;c.$watch(e.ngIf,function(f){Qa(f)?m||(m=c.$new(),g(m,function(c){c[c.length++]=V.createComment(" end ngIf: "+e.ngIf+" ");h={clone:c};a.enter(c,d.parent(),d)})):(k&&(k.remove(),
k=null),m&&(m.$destroy(),m=null),h&&(k=Db(h.clone),a.leave(k,function(){k=null}),h=null))})}}}],td=["$http","$templateCache","$anchorScroll","$animate","$sce",function(a,c,d,e,f){return{restrict:"ECA",priority:400,terminal:!0,transclude:"element",controller:Ra.noop,compile:function(g,h){var m=h.ngInclude||h.src,k=h.onload||"",l=h.autoscroll;return function(g,h,q,s,D){var x=0,t,y,B,v=function(){y&&(y.remove(),y=null);t&&(t.$destroy(),t=null);B&&(e.leave(B,function(){y=null}),y=B,B=null)};g.$watch(f.parseAsResourceUrl(m),
function(f){var m=function(){!z(l)||l&&!g.$eval(l)||d()},q=++x;f?(a.get(f,{cache:c}).success(function(a){if(q===x){var c=g.$new();s.template=a;a=D(c,function(a){v();e.enter(a,null,h,m)});t=c;B=a;t.$emit("$includeContentLoaded");g.$eval(k)}}).error(function(){q===x&&v()}),g.$emit("$includeContentRequested")):(v(),s.template=null)})}}}}],Jd=["$compile",function(a){return{restrict:"ECA",priority:-400,require:"ngInclude",link:function(c,d,e,f){d.html(f.template);a(d.contents())(c)}}}],ud=va({priority:450,
compile:function(){return{pre:function(a,c,d){a.$eval(d.ngInit)}}}}),vd=va({terminal:!0,priority:1E3}),wd=["$locale","$interpolate",function(a,c){var d=/{}/g;return{restrict:"EA",link:function(e,f,g){var h=g.count,m=g.$attr.when&&f.attr(g.$attr.when),k=g.offset||0,l=e.$eval(m)||{},n={},p=c.startSymbol(),r=c.endSymbol(),s=/^when(Minus)?(.+)$/;q(g,function(a,c){s.test(c)&&(l[J(c.replace("when","").replace("Minus","-"))]=f.attr(g.$attr[c]))});q(l,function(a,e){n[e]=c(a.replace(d,p+h+"-"+k+r))});e.$watch(function(){var c=
parseFloat(e.$eval(h));if(isNaN(c))return"";c in l||(c=a.pluralCat(c-k));return n[c](e,f,!0)},function(a){f.text(a)})}}}],xd=["$parse","$animate",function(a,c){var d=u("ngRepeat");return{transclude:"element",priority:1E3,terminal:!0,$$tlb:!0,link:function(e,f,g,h,m){var k=g.ngRepeat,l=k.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/),n,p,r,s,D,x,t={$id:Ia};if(!l)throw d("iexp",k);g=l[1];h=l[2];(l=l[3])?(n=a(l),p=function(a,c,d){x&&(t[x]=a);t[D]=c;t.$index=d;return n(e,
t)}):(r=function(a,c){return Ia(c)},s=function(a){return a});l=g.match(/^(?:([\$\w]+)|\(([\$\w]+)\s*,\s*([\$\w]+)\))$/);if(!l)throw d("iidexp",g);D=l[3]||l[1];x=l[2];var z={};e.$watchCollection(h,function(a){var g,h,l=f[0],n,t={},F,R,C,w,T,u,E=[];if(db(a))T=a,n=p||r;else{n=p||s;T=[];for(C in a)a.hasOwnProperty(C)&&"$"!=C.charAt(0)&&T.push(C);T.sort()}F=T.length;h=E.length=T.length;for(g=0;g<h;g++)if(C=a===T?g:T[g],w=a[C],w=n(C,w,g),Aa(w,"`track by` id"),z.hasOwnProperty(w))u=z[w],delete z[w],t[w]=
u,E[g]=u;else{if(t.hasOwnProperty(w))throw q(E,function(a){a&&a.scope&&(z[a.id]=a)}),d("dupes",k,w);E[g]={id:w};t[w]=!1}for(C in z)z.hasOwnProperty(C)&&(u=z[C],g=Db(u.clone),c.leave(g),q(g,function(a){a.$$NG_REMOVED=!0}),u.scope.$destroy());g=0;for(h=T.length;g<h;g++){C=a===T?g:T[g];w=a[C];u=E[g];E[g-1]&&(l=E[g-1].clone[E[g-1].clone.length-1]);if(u.scope){R=u.scope;n=l;do n=n.nextSibling;while(n&&n.$$NG_REMOVED);u.clone[0]!=n&&c.move(Db(u.clone),null,y(l));l=u.clone[u.clone.length-1]}else R=e.$new();
R[D]=w;x&&(R[x]=C);R.$index=g;R.$first=0===g;R.$last=g===F-1;R.$middle=!(R.$first||R.$last);R.$odd=!(R.$even=0===(g&1));u.scope||m(R,function(a){a[a.length++]=V.createComment(" end ngRepeat: "+k+" ");c.enter(a,null,y(l));l=a;u.scope=R;u.clone=a;t[u.id]=u})}z=t})}}}],yd=["$animate",function(a){return function(c,d,e){c.$watch(e.ngShow,function(c){a[Qa(c)?"removeClass":"addClass"](d,"ng-hide")})}}],rd=["$animate",function(a){return function(c,d,e){c.$watch(e.ngHide,function(c){a[Qa(c)?"addClass":"removeClass"](d,
"ng-hide")})}}],zd=va(function(a,c,d){a.$watch(d.ngStyle,function(a,d){d&&a!==d&&q(d,function(a,d){c.css(d,"")});a&&c.css(a)},!0)}),Ad=["$animate",function(a){return{restrict:"EA",require:"ngSwitch",controller:["$scope",function(){this.cases={}}],link:function(c,d,e,f){var g=[],h=[],m=[],k=[];c.$watch(e.ngSwitch||e.on,function(d){var n,p;n=0;for(p=m.length;n<p;++n)m[n].remove();n=m.length=0;for(p=k.length;n<p;++n){var r=h[n];k[n].$destroy();m[n]=r;a.leave(r,function(){m.splice(n,1)})}h.length=0;k.length=
0;if(g=f.cases["!"+d]||f.cases["?"])c.$eval(e.change),q(g,function(d){var e=c.$new();k.push(e);d.transclude(e,function(c){var e=d.element;h.push(c);a.enter(c,e.parent(),e)})})})}}}],Bd=va({transclude:"element",priority:800,require:"^ngSwitch",link:function(a,c,d,e,f){e.cases["!"+d.ngSwitchWhen]=e.cases["!"+d.ngSwitchWhen]||[];e.cases["!"+d.ngSwitchWhen].push({transclude:f,element:c})}}),Cd=va({transclude:"element",priority:800,require:"^ngSwitch",link:function(a,c,d,e,f){e.cases["?"]=e.cases["?"]||
[];e.cases["?"].push({transclude:f,element:c})}}),Ed=va({link:function(a,c,d,e,f){if(!f)throw u("ngTransclude")("orphan",ha(c));f(function(a){c.empty();c.append(a)})}}),ed=["$templateCache",function(a){return{restrict:"E",terminal:!0,compile:function(c,d){"text/ng-template"==d.type&&a.put(d.id,c[0].text)}}}],Te=u("ngOptions"),Dd=aa({terminal:!0}),fd=["$compile","$parse",function(a,c){var d=/^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/,
e={$setViewValue:w};return{restrict:"E",require:["select","?ngModel"],controller:["$element","$scope","$attrs",function(a,c,d){var m=this,k={},l=e,n;m.databound=d.ngModel;m.init=function(a,c,d){l=a;n=d};m.addOption=function(c){Aa(c,'"option value"');k[c]=!0;l.$viewValue==c&&(a.val(c),n.parent()&&n.remove())};m.removeOption=function(a){this.hasOption(a)&&(delete k[a],l.$viewValue==a&&this.renderUnknownOption(a))};m.renderUnknownOption=function(c){c="? "+Ia(c)+" ?";n.val(c);a.prepend(n);a.val(c);n.prop("selected",
!0)};m.hasOption=function(a){return k.hasOwnProperty(a)};c.$on("$destroy",function(){m.renderUnknownOption=w})}],link:function(e,g,h,m){function k(a,c,d,e){d.$render=function(){var a=d.$viewValue;e.hasOption(a)?(w.parent()&&w.remove(),c.val(a),""===a&&x.prop("selected",!0)):H(a)&&x?c.val(""):e.renderUnknownOption(a)};c.on("change",function(){a.$apply(function(){w.parent()&&w.remove();d.$setViewValue(c.val())})})}function l(a,c,d){var e;d.$render=function(){var a=new Xa(d.$viewValue);q(c.find("option"),
function(c){c.selected=z(a.get(c.value))})};a.$watch(function(){xa(e,d.$viewValue)||(e=ka(d.$viewValue),d.$render())});c.on("change",function(){a.$apply(function(){var a=[];q(c.find("option"),function(c){c.selected&&a.push(c.value)});d.$setViewValue(a)})})}function n(e,f,g){function h(){var a={"":[]},c=[""],d,k,s,u,v;u=g.$modelValue;v=y(e)||[];var A=n?Wb(v):v,E,I,B;I={};s=!1;var F,H;if(r)if(w&&K(u))for(s=new Xa([]),B=0;B<u.length;B++)I[m]=u[B],s.put(w(e,I),u[B]);else s=new Xa(u);for(B=0;E=A.length,
B<E;B++){k=B;if(n){k=A[B];if("$"===k.charAt(0))continue;I[n]=k}I[m]=v[k];d=p(e,I)||"";(k=a[d])||(k=a[d]=[],c.push(d));r?d=z(s.remove(w?w(e,I):q(e,I))):(w?(d={},d[m]=u,d=w(e,d)===w(e,I)):d=u===q(e,I),s=s||d);F=l(e,I);F=z(F)?F:"";k.push({id:w?w(e,I):n?A[B]:B,label:F,selected:d})}r||(D||null===u?a[""].unshift({id:"",label:"",selected:!s}):s||a[""].unshift({id:"?",label:"",selected:!0}));I=0;for(A=c.length;I<A;I++){d=c[I];k=a[d];x.length<=I?(u={element:C.clone().attr("label",d),label:k.label},v=[u],x.push(v),
f.append(u.element)):(v=x[I],u=v[0],u.label!=d&&u.element.attr("label",u.label=d));F=null;B=0;for(E=k.length;B<E;B++)s=k[B],(d=v[B+1])?(F=d.element,d.label!==s.label&&F.text(d.label=s.label),d.id!==s.id&&F.val(d.id=s.id),d.selected!==s.selected&&F.prop("selected",d.selected=s.selected)):(""===s.id&&D?H=D:(H=t.clone()).val(s.id).attr("selected",s.selected).text(s.label),v.push({element:H,label:s.label,id:s.id,selected:s.selected}),F?F.after(H):u.element.append(H),F=H);for(B++;v.length>B;)v.pop().element.remove()}for(;x.length>
I;)x.pop()[0].element.remove()}var k;if(!(k=u.match(d)))throw Te("iexp",u,ha(f));var l=c(k[2]||k[1]),m=k[4]||k[6],n=k[5],p=c(k[3]||""),q=c(k[2]?k[1]:m),y=c(k[7]),w=k[8]?c(k[8]):null,x=[[{element:f,label:""}]];D&&(a(D)(e),D.removeClass("ng-scope"),D.remove());f.empty();f.on("change",function(){e.$apply(function(){var a,c=y(e)||[],d={},h,k,l,p,t,u,v;if(r)for(k=[],p=0,u=x.length;p<u;p++)for(a=x[p],l=1,t=a.length;l<t;l++){if((h=a[l].element)[0].selected){h=h.val();n&&(d[n]=h);if(w)for(v=0;v<c.length&&
(d[m]=c[v],w(e,d)!=h);v++);else d[m]=c[h];k.push(q(e,d))}}else{h=f.val();if("?"==h)k=s;else if(""===h)k=null;else if(w)for(v=0;v<c.length;v++){if(d[m]=c[v],w(e,d)==h){k=q(e,d);break}}else d[m]=c[h],n&&(d[n]=h),k=q(e,d);1<x[0].length&&x[0][1].id!==h&&(x[0][1].selected=!1)}g.$setViewValue(k)})});g.$render=h;e.$watch(h)}if(m[1]){var p=m[0];m=m[1];var r=h.multiple,u=h.ngOptions,D=!1,x,t=y(V.createElement("option")),C=y(V.createElement("optgroup")),w=t.clone();h=0;for(var v=g.children(),E=v.length;h<E;h++)if(""===
v[h].value){x=D=v.eq(h);break}p.init(m,D,w);r&&(m.$isEmpty=function(a){return!a||0===a.length});u?n(e,g,m):r?l(e,g,m):k(e,g,m,p)}}}}],hd=["$interpolate",function(a){var c={addOption:w,removeOption:w};return{restrict:"E",priority:100,compile:function(d,e){if(H(e.value)){var f=a(d.text(),!0);f||e.$set("value",d.text())}return function(a,d,e){var k=d.parent(),l=k.data("$selectController")||k.parent().data("$selectController");l&&l.databound?d.prop("selected",!1):l=c;f?a.$watch(f,function(a,c){e.$set("value",
a);a!==c&&l.removeOption(c);l.addOption(a)}):l.addOption(e.value);d.on("$destroy",function(){l.removeOption(e.value)})}}}}],gd=aa({restrict:"E",terminal:!0});P.angular.bootstrap?console.log("WARNING: Tried to load angular more than once."):((Ba=P.jQuery)&&Ba.fn.on?(y=Ba,E(Ba.fn,{scope:Ja.scope,isolateScope:Ja.isolateScope,controller:Ja.controller,injector:Ja.injector,inheritedData:Ja.inheritedData}),Fb("remove",!0,!0,!1),Fb("empty",!1,!1,!1),Fb("html",!1,!1,!0)):y=M,Ra.element=y,Zc(Ra),y(V).ready(function(){Wc(V,
dc)}))})(window,document);!window.angular.$$csp()&&window.angular.element(document).find("head").prepend('<style type="text/css">@charset "UTF-8";[ng\\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide{display:none !important;}ng\\:form{display:block;}.ng-animate-block-transitions{transition:0s all!important;-webkit-transition:0s all!important;}.ng-hide-add-active,.ng-hide-remove{display:block!important;}</style>');
//# sourceMappingURL=angular.min.js.map
;
{
"version":3,
"file":"angular.min.js",
"lineCount":211,
"mappings":"A;;;;;aAKC,SAAQ,CAACA,CAAD,CAASC,CAAT,CAAmBC,CAAnB,CAA8B,CA8BvCC,QAAAA,EAAAA,CAAAA,CAAAA,CAAAA,CAAAA,MAAAA,SAAAA,EAAAA,CAAAA,IAAAA,EAAAA,SAAAA,CAAAA,CAAAA,CAAAA,CAAAA,CAAAA,CAAAA,EAAAA,GAAAA,EAAAA,CAAAA,CAAAA,CAAAA,CAAAA,GAAAA,CAAAA,EAAAA,EAAAA,CAAAA,CAAAA,uCAAAA,EAAAA,CAAAA,CAAAA,CAAAA,CAAAA,GAAAA,CAAAA,EAAAA,EAAAA,CAAAA,KAAAA,CAAAA,CAAAA,CAAAA,CAAAA,CAAAA,CAAAA,SAAAA,OAAAA,CAAAA,CAAAA,EAAAA,CAAAA,CAAAA,CAAAA,CAAAA,EAAAA,CAAAA,EAAAA,CAAAA,CAAAA,GAAAA,CAAAA,GAAAA,EAAAA,GAAAA,EAAAA,CAAAA,CAAAA,CAAAA,EAAAA,GAAAA,CAAAA,kBAAAA,CAAAA,UAAAA,EAAAA,MAAAA,UAAAA,CAAAA,CAAAA,CAAAA,CAAAA,SAAAA,CAAAA,CAAAA,CAAAA,SAAAA,EAAAA,QAAAA,CAAAA,aAAAA,CAAAA,EAAAA,CAAAA,CAAAA,WAAAA,EAAAA,MAAAA,UAAAA,CAAAA,CAAAA,CAAAA,CAAAA,WAAAA,CAAAA,QAAAA,EAAAA,MAAAA,UAAAA,CAAAA,CAAAA,CAAAA,CAAAA,IAAAA,UAAAA,CAAAA,SAAAA,CAAAA,CAAAA,CAAAA,CAAAA,CAAAA,SAAAA,CAAAA,CAAAA,CAAAA,CAAAA,OAAAA,MAAAA,CAAAA,CAAAA,CAAAA,CAAAA,CAoOAC,QAASA,GAAW,CAACC,CAAD,CAAM,CACxB,GAAW,IAAX,EAAIA,CAAJ,EAAmBC,EAAA,CAASD,CAAT,CAAnB,CACE,MAAO,CAAA,CAGT;IAAIE,EAASF,CAAAE,OAEb,OAAqB,EAArB,GAAIF,CAAAG,SAAJ,EAA0BD,CAA1B,CACS,CAAA,CADT,CAIOE,CAAA,CAASJ,CAAT,CAJP,EAIwBK,CAAA,CAAQL,CAAR,CAJxB,EAImD,CAJnD,GAIwCE,CAJxC,EAKyB,QALzB,GAKO,MAAOA,EALd,EAK8C,CAL9C,CAKqCA,CALrC,EAKoDA,CALpD,CAK6D,CAL7D,GAKmEF,EAZ3C,CA4C1BM,QAASA,EAAO,CAACN,CAAD,CAAMO,CAAN,CAAgBC,CAAhB,CAAyB,CACvC,IAAIC,CACJ,IAAIT,CAAJ,CACE,GAAIU,CAAA,CAAWV,CAAX,CAAJ,CACE,IAAKS,CAAL,GAAYT,EAAZ,CAGa,WAAX,EAAIS,CAAJ,GAAiC,QAAjC,EAA0BA,CAA1B,EAAoD,MAApD,EAA6CA,CAA7C,EAAgET,CAAAW,eAAhE,EAAsF,CAAAX,CAAAW,eAAA,CAAmBF,CAAnB,CAAtF,GACEF,CAAAK,KAAA,CAAcJ,CAAd,CAAuBR,CAAA,CAAIS,CAAJ,CAAvB,CAAiCA,CAAjC,CALN,KAQO,IAAIT,CAAAM,QAAJ,EAAmBN,CAAAM,QAAnB,GAAmCA,CAAnC,CACLN,CAAAM,QAAA,CAAYC,CAAZ,CAAsBC,CAAtB,CADK,KAEA,IAAIT,EAAA,CAAYC,CAAZ,CAAJ,CACL,IAAKS,CAAL,CAAW,CAAX,CAAcA,CAAd,CAAoBT,CAAAE,OAApB,CAAgCO,CAAA,EAAhC,CACEF,CAAAK,KAAA,CAAcJ,CAAd,CAAuBR,CAAA,CAAIS,CAAJ,CAAvB,CAAiCA,CAAjC,CAFG,KAIL,KAAKA,CAAL,GAAYT,EAAZ,CACMA,CAAAW,eAAA,CAAmBF,CAAnB,CAAJ,EACEF,CAAAK,KAAA,CAAcJ,CAAd,CAAuBR,CAAA,CAAIS,CAAJ,CAAvB,CAAiCA,CAAjC,CAKR,OAAOT,EAxBgC,CA2BzCa,QAASA,GAAU,CAACb,CAAD,CAAM,CACvB,IAAIc,EAAO,EAAX,CACSL,CAAT,KAASA,CAAT,GAAgBT,EAAhB,CACMA,CAAAW,eAAA,CAAmBF,CAAnB,CAAJ,EACEK,CAAAC,KAAA,CAAUN,CAAV,CAGJ,OAAOK,EAAAE,KAAA,EAPgB,CAUzBC,QAASA,GAAa,CAACjB,CAAD;AAAMO,CAAN,CAAgBC,CAAhB,CAAyB,CAE7C,IADA,IAAIM,EAAOD,EAAA,CAAWb,CAAX,CAAX,CACUkB,EAAI,CAAd,CAAiBA,CAAjB,CAAqBJ,CAAAZ,OAArB,CAAkCgB,CAAA,EAAlC,CACEX,CAAAK,KAAA,CAAcJ,CAAd,CAAuBR,CAAA,CAAIc,CAAA,CAAKI,CAAL,CAAJ,CAAvB,CAAqCJ,CAAA,CAAKI,CAAL,CAArC,CAEF,OAAOJ,EALsC,CAc/CK,QAASA,GAAa,CAACC,CAAD,CAAa,CACjC,MAAO,SAAQ,CAACC,CAAD,CAAQZ,CAAR,CAAa,CAAEW,CAAA,CAAWX,CAAX,CAAgBY,CAAhB,CAAF,CADK,CAYnCC,QAASA,GAAO,EAAG,CAIjB,IAHA,IAAIC,EAAQC,EAAAtB,OAAZ,CACIuB,CAEJ,CAAMF,CAAN,CAAA,CAAa,CACXA,CAAA,EACAE,EAAA,CAAQD,EAAA,CAAID,CAAJ,CAAAG,WAAA,CAAsB,CAAtB,CACR,IAAa,EAAb,EAAID,CAAJ,CAEE,MADAD,GAAA,CAAID,CAAJ,CACO,CADM,GACN,CAAAC,EAAAG,KAAA,CAAS,EAAT,CAET,IAAa,EAAb,EAAIF,CAAJ,CACED,EAAA,CAAID,CAAJ,CAAA,CAAa,GADf,KAIE,OADAC,GAAA,CAAID,CAAJ,CACO,CADMK,MAAAC,aAAA,CAAoBJ,CAApB,CAA4B,CAA5B,CACN,CAAAD,EAAAG,KAAA,CAAS,EAAT,CAXE,CAcbH,EAAAM,QAAA,CAAY,GAAZ,CACA,OAAON,GAAAG,KAAA,CAAS,EAAT,CAnBU,CA4BnBI,QAASA,GAAU,CAAC/B,CAAD,CAAMgC,CAAN,CAAS,CACtBA,CAAJ,CACEhC,CAAAiC,UADF,CACkBD,CADlB,CAIE,OAAOhC,CAAAiC,UALiB,CAuB5BC,QAASA,EAAM,CAACC,CAAD,CAAM,CACnB,IAAIH,EAAIG,CAAAF,UACR3B,EAAA,CAAQ8B,SAAR,CAAmB,QAAQ,CAACpC,CAAD,CAAM,CAC3BA,CAAJ,GAAYmC,CAAZ,EACE7B,CAAA,CAAQN,CAAR,CAAa,QAAQ,CAACqB,CAAD,CAAQZ,CAAR,CAAa,CAChC0B,CAAA,CAAI1B,CAAJ,CAAA,CAAWY,CADqB,CAAlC,CAF6B,CAAjC,CAQAU,GAAA,CAAWI,CAAX,CAAeH,CAAf,CACA,OAAOG,EAXY,CAcrBE,QAASA,EAAG,CAACC,CAAD,CAAM,CAChB,MAAOC,SAAA,CAASD,CAAT;AAAc,EAAd,CADS,CAKlBE,QAASA,GAAO,CAACC,CAAD,CAASC,CAAT,CAAgB,CAC9B,MAAOR,EAAA,CAAO,KAAKA,CAAA,CAAO,QAAQ,EAAG,EAAlB,CAAsB,WAAWO,CAAX,CAAtB,CAAL,CAAP,CAA0DC,CAA1D,CADuB,CAoBhCC,QAASA,EAAI,EAAG,EAoBhBC,QAASA,GAAQ,CAACC,CAAD,CAAI,CAAC,MAAOA,EAAR,CAIrBC,QAASA,GAAO,CAACzB,CAAD,CAAQ,CAAC,MAAO,SAAQ,EAAG,CAAC,MAAOA,EAAR,CAAnB,CAcxB0B,QAASA,EAAW,CAAC1B,CAAD,CAAO,CAAC,MAAwB,WAAxB,GAAO,MAAOA,EAAf,CAe3B2B,QAASA,EAAS,CAAC3B,CAAD,CAAO,CAAC,MAAwB,WAAxB,GAAO,MAAOA,EAAf,CAgBzB4B,QAASA,EAAQ,CAAC5B,CAAD,CAAO,CAAC,MAAgB,KAAhB,EAAOA,CAAP,EAAyC,QAAzC,GAAwB,MAAOA,EAAhC,CAexBjB,QAASA,EAAQ,CAACiB,CAAD,CAAO,CAAC,MAAwB,QAAxB,GAAO,MAAOA,EAAf,CAexB6B,QAASA,GAAQ,CAAC7B,CAAD,CAAO,CAAC,MAAwB,QAAxB,GAAO,MAAOA,EAAf,CAexB8B,QAASA,GAAM,CAAC9B,CAAD,CAAQ,CACrB,MAAgC,eAAhC,GAAO+B,EAAAxC,KAAA,CAAcS,CAAd,CADc,CAiBvBhB,QAASA,EAAO,CAACgB,CAAD,CAAQ,CACtB,MAAgC,gBAAhC,GAAO+B,EAAAxC,KAAA,CAAcS,CAAd,CADe,CAiBxBX,QAASA,EAAU,CAACW,CAAD,CAAO,CAAC,MAAwB,UAAxB,GAAO,MAAOA,EAAf,CA3lBa;AAqmBvCgC,QAASA,GAAQ,CAAChC,CAAD,CAAQ,CACvB,MAAgC,iBAAhC,GAAO+B,EAAAxC,KAAA,CAAcS,CAAd,CADgB,CAYzBpB,QAASA,GAAQ,CAACD,CAAD,CAAM,CACrB,MAAOA,EAAP,EAAcA,CAAAJ,SAAd,EAA8BI,CAAAsD,SAA9B,EAA8CtD,CAAAuD,MAA9C,EAA2DvD,CAAAwD,YADtC,CAoDvBC,QAASA,GAAS,CAACC,CAAD,CAAO,CACvB,MAAO,EAAGA,CAAAA,CAAH,EACJ,EAAAA,CAAAC,SAAA,EACGD,CAAAE,KADH,EACgBF,CAAAG,KADhB,EAC6BH,CAAAI,KAD7B,CADI,CADgB,CA+BzBC,QAASA,GAAG,CAAC/D,CAAD,CAAMO,CAAN,CAAgBC,CAAhB,CAAyB,CACnC,IAAIwD,EAAU,EACd1D,EAAA,CAAQN,CAAR,CAAa,QAAQ,CAACqB,CAAD,CAAQE,CAAR,CAAe0C,CAAf,CAAqB,CACxCD,CAAAjD,KAAA,CAAaR,CAAAK,KAAA,CAAcJ,CAAd,CAAuBa,CAAvB,CAA8BE,CAA9B,CAAqC0C,CAArC,CAAb,CADwC,CAA1C,CAGA,OAAOD,EAL4B,CAwCrCE,QAASA,GAAO,CAACC,CAAD,CAAQnE,CAAR,CAAa,CAC3B,GAAImE,CAAAD,QAAJ,CAAmB,MAAOC,EAAAD,QAAA,CAAclE,CAAd,CAE1B,KAAK,IAAIkB,EAAI,CAAb,CAAgBA,CAAhB,CAAoBiD,CAAAjE,OAApB,CAAkCgB,CAAA,EAAlC,CACE,GAAIlB,CAAJ,GAAYmE,CAAA,CAAMjD,CAAN,CAAZ,CAAsB,MAAOA,EAE/B,OAAQ,EANmB,CAS7BkD,QAASA,GAAW,CAACD,CAAD,CAAQ9C,CAAR,CAAe,CACjC,IAAIE,EAAQ2C,EAAA,CAAQC,CAAR,CAAe9C,CAAf,CACA,EAAZ,EAAIE,CAAJ,EACE4C,CAAAE,OAAA,CAAa9C,CAAb,CAAoB,CAApB,CACF,OAAOF,EAJ0B,CA4EnCiD,QAASA,GAAI,CAACC,CAAD,CAASC,CAAT,CAAsBC,CAAtB,CAAmCC,CAAnC,CAA8C,CACzD,GAAIzE,EAAA,CAASsE,CAAT,CAAJ,EAAgCA,CAAhC,EAAgCA,CA3MlBI,WA2Md,EAAgCJ,CA3MAK,OA2MhC,CACE,KAAMC,GAAA,CAAS,MAAT,CAAN;AAIF,GAAKL,CAAL,CAaO,CACL,GAAID,CAAJ,GAAeC,CAAf,CAA4B,KAAMK,GAAA,CAAS,KAAT,CAAN,CAG5BJ,CAAA,CAAcA,CAAd,EAA6B,EAC7BC,EAAA,CAAYA,CAAZ,EAAyB,EAEzB,IAAIzB,CAAA,CAASsB,CAAT,CAAJ,CAAsB,CACpB,IAAIhD,EAAQ2C,EAAA,CAAQO,CAAR,CAAqBF,CAArB,CACZ,IAAe,EAAf,GAAIhD,CAAJ,CAAkB,MAAOmD,EAAA,CAAUnD,CAAV,CAEzBkD,EAAA1D,KAAA,CAAiBwD,CAAjB,CACAG,EAAA3D,KAAA,CAAeyD,CAAf,CALoB,CAStB,GAAInE,CAAA,CAAQkE,CAAR,CAAJ,CAEE,IAAM,IAAIrD,EADVsD,CAAAtE,OACUgB,CADW,CACrB,CAAiBA,CAAjB,CAAqBqD,CAAArE,OAArB,CAAoCgB,CAAA,EAApC,CACE4D,CAKA,CALSR,EAAA,CAAKC,CAAA,CAAOrD,CAAP,CAAL,CAAgB,IAAhB,CAAsBuD,CAAtB,CAAmCC,CAAnC,CAKT,CAJIzB,CAAA,CAASsB,CAAA,CAAOrD,CAAP,CAAT,CAIJ,GAHEuD,CAAA1D,KAAA,CAAiBwD,CAAA,CAAOrD,CAAP,CAAjB,CACA,CAAAwD,CAAA3D,KAAA,CAAe+D,CAAf,CAEF,EAAAN,CAAAzD,KAAA,CAAiB+D,CAAjB,CARJ,KAUO,CACL,IAAI9C,EAAIwC,CAAAvC,UACR3B,EAAA,CAAQkE,CAAR,CAAqB,QAAQ,CAACnD,CAAD,CAAQZ,CAAR,CAAa,CACxC,OAAO+D,CAAA,CAAY/D,CAAZ,CADiC,CAA1C,CAGA,KAAUA,CAAV,GAAiB8D,EAAjB,CACEO,CAKA,CALSR,EAAA,CAAKC,CAAA,CAAO9D,CAAP,CAAL,CAAkB,IAAlB,CAAwBgE,CAAxB,CAAqCC,CAArC,CAKT,CAJIzB,CAAA,CAASsB,CAAA,CAAO9D,CAAP,CAAT,CAIJ,GAHEgE,CAAA1D,KAAA,CAAiBwD,CAAA,CAAO9D,CAAP,CAAjB,CACA,CAAAiE,CAAA3D,KAAA,CAAe+D,CAAf,CAEF,EAAAN,CAAA,CAAY/D,CAAZ,CAAA,CAAmBqE,CAErB/C,GAAA,CAAWyC,CAAX,CAAuBxC,CAAvB,CAbK,CA1BF,CAbP,IAEE,CADAwC,CACA,CADcD,CACd,IACMlE,CAAA,CAAQkE,CAAR,CAAJ,CACEC,CADF,CACgBF,EAAA,CAAKC,CAAL,CAAa,EAAb,CAAiBE,CAAjB,CAA8BC,CAA9B,CADhB,CAEWvB,EAAA,CAAOoB,CAAP,CAAJ,CACLC,CADK,CACS,IAAIO,IAAJ,CAASR,CAAAS,QAAA,EAAT,CADT,CAEI3B,EAAA,CAASkB,CAAT,CAAJ,CACLC,CADK,CACaS,MAAJ,CAAWV,CAAAA,OAAX,CADT,CAEItB,CAAA,CAASsB,CAAT,CAFJ,GAGLC,CAHK,CAGSF,EAAA,CAAKC,CAAL,CAAa,EAAb,CAAiBE,CAAjB,CAA8BC,CAA9B,CAHT,CALT,CAsDF,OAAOF,EA9DkD,CAoE3DU,QAASA,GAAW,CAACC,CAAD,CAAMhD,CAAN,CAAW,CAC7B,GAAI9B,CAAA,CAAQ8E,CAAR,CAAJ,CAAkB,CAChBhD,CAAA;AAAMA,CAAN,EAAa,EAEb,KAAM,IAAIjB,EAAI,CAAd,CAAiBA,CAAjB,CAAqBiE,CAAAjF,OAArB,CAAiCgB,CAAA,EAAjC,CACEiB,CAAA,CAAIjB,CAAJ,CAAA,CAASiE,CAAA,CAAIjE,CAAJ,CAJK,CAAlB,IAMO,IAAI+B,CAAA,CAASkC,CAAT,CAAJ,CAGL,IAAS1E,CAAT,GAFA0B,EAEgBgD,CAFVhD,CAEUgD,EAFH,EAEGA,CAAAA,CAAhB,CACM,CAAAxE,EAAAC,KAAA,CAAoBuE,CAApB,CAAyB1E,CAAzB,CAAJ,EAAyD,GAAzD,GAAuCA,CAAA2E,OAAA,CAAW,CAAX,CAAvC,EAAkF,GAAlF,GAAgE3E,CAAA2E,OAAA,CAAW,CAAX,CAAhE,GACEjD,CAAA,CAAI1B,CAAJ,CADF,CACa0E,CAAA,CAAI1E,CAAJ,CADb,CAMJ,OAAO0B,EAAP,EAAcgD,CAjBe,CAkD/BE,QAASA,GAAM,CAACC,CAAD,CAAKC,CAAL,CAAS,CACtB,GAAID,CAAJ,GAAWC,CAAX,CAAe,MAAO,CAAA,CACtB,IAAW,IAAX,GAAID,CAAJ,EAA0B,IAA1B,GAAmBC,CAAnB,CAAgC,MAAO,CAAA,CACvC,IAAID,CAAJ,GAAWA,CAAX,EAAiBC,CAAjB,GAAwBA,CAAxB,CAA4B,MAAO,CAAA,CAHb,KAIlBC,EAAK,MAAOF,EAJM,CAIsB7E,CAC5C,IAAI+E,CAAJ,EADyBC,MAAOF,EAChC,EACY,QADZ,EACMC,CADN,CAEI,GAAInF,CAAA,CAAQiF,CAAR,CAAJ,CAAiB,CACf,GAAI,CAACjF,CAAA,CAAQkF,CAAR,CAAL,CAAkB,MAAO,CAAA,CACzB,KAAKrF,CAAL,CAAcoF,CAAApF,OAAd,GAA4BqF,CAAArF,OAA5B,CAAuC,CACrC,IAAIO,CAAJ,CAAQ,CAAR,CAAWA,CAAX,CAAeP,CAAf,CAAuBO,CAAA,EAAvB,CACE,GAAI,CAAC4E,EAAA,CAAOC,CAAA,CAAG7E,CAAH,CAAP,CAAgB8E,CAAA,CAAG9E,CAAH,CAAhB,CAAL,CAA+B,MAAO,CAAA,CAExC,OAAO,CAAA,CAJ8B,CAFxB,CAAjB,IAQO,CAAA,GAAI0C,EAAA,CAAOmC,CAAP,CAAJ,CACL,MAAOnC,GAAA,CAAOoC,CAAP,CAAP,EAAqBD,CAAAN,QAAA,EAArB,EAAqCO,CAAAP,QAAA,EAChC,IAAI3B,EAAA,CAASiC,CAAT,CAAJ,EAAoBjC,EAAA,CAASkC,CAAT,CAApB,CACL,MAAOD,EAAAlC,SAAA,EAAP,EAAwBmC,CAAAnC,SAAA,EAExB,IAAYkC,CAAZ;AAAYA,CApVJX,WAoVR,EAAYW,CApVcV,OAoV1B,EAA2BW,CAA3B,EAA2BA,CApVnBZ,WAoVR,EAA2BY,CApVDX,OAoV1B,EAAkC3E,EAAA,CAASqF,CAAT,CAAlC,EAAkDrF,EAAA,CAASsF,CAAT,CAAlD,EAAkElF,CAAA,CAAQkF,CAAR,CAAlE,CAA+E,MAAO,CAAA,CACtFG,EAAA,CAAS,EACT,KAAIjF,CAAJ,GAAW6E,EAAX,CACE,GAAsB,GAAtB,GAAI7E,CAAA2E,OAAA,CAAW,CAAX,CAAJ,EAA6B,CAAA1E,CAAA,CAAW4E,CAAA,CAAG7E,CAAH,CAAX,CAA7B,CAAA,CACA,GAAI,CAAC4E,EAAA,CAAOC,CAAA,CAAG7E,CAAH,CAAP,CAAgB8E,CAAA,CAAG9E,CAAH,CAAhB,CAAL,CAA+B,MAAO,CAAA,CACtCiF,EAAA,CAAOjF,CAAP,CAAA,CAAc,CAAA,CAFd,CAIF,IAAIA,CAAJ,GAAW8E,EAAX,CACE,GAAI,CAACG,CAAA/E,eAAA,CAAsBF,CAAtB,CAAL,EACsB,GADtB,GACIA,CAAA2E,OAAA,CAAW,CAAX,CADJ,EAEIG,CAAA,CAAG9E,CAAH,CAFJ,GAEgBZ,CAFhB,EAGI,CAACa,CAAA,CAAW6E,CAAA,CAAG9E,CAAH,CAAX,CAHL,CAG0B,MAAO,CAAA,CAEnC,OAAO,CAAA,CAlBF,CAsBX,MAAO,CAAA,CArCe,CAyCxBkF,QAASA,GAAG,EAAG,CACb,MAAQ/F,EAAAgG,eAAR,EAAmChG,CAAAgG,eAAAC,SAAnC,EACKjG,CAAAkG,cADL,EAEI,EAAG,CAAAlG,CAAAkG,cAAA,CAAuB,UAAvB,CAAH,EAAyC,CAAAlG,CAAAkG,cAAA,CAAuB,eAAvB,CAAzC,CAHS,CAmCfC,QAASA,GAAI,CAACC,CAAD,CAAOC,CAAP,CAAW,CACtB,IAAIC,EAA+B,CAAnB,CAAA9D,SAAAlC,OAAA,CAxBTiG,EAAAvF,KAAA,CAwB0CwB,SAxB1C,CAwBqDgE,CAxBrD,CAwBS,CAAiD,EACjE,OAAI,CAAA1F,CAAA,CAAWuF,CAAX,CAAJ,EAAwBA,CAAxB;AAAsChB,MAAtC,CAcSgB,CAdT,CACSC,CAAAhG,OACA,CAAH,QAAQ,EAAG,CACT,MAAOkC,UAAAlC,OACA,CAAH+F,CAAAI,MAAA,CAASL,CAAT,CAAeE,CAAAI,OAAA,CAAiBH,EAAAvF,KAAA,CAAWwB,SAAX,CAAsB,CAAtB,CAAjB,CAAf,CAAG,CACH6D,CAAAI,MAAA,CAASL,CAAT,CAAeE,CAAf,CAHK,CAAR,CAKH,QAAQ,EAAG,CACT,MAAO9D,UAAAlC,OACA,CAAH+F,CAAAI,MAAA,CAASL,CAAT,CAAe5D,SAAf,CAAG,CACH6D,CAAArF,KAAA,CAAQoF,CAAR,CAHK,CATK,CAqBxBO,QAASA,GAAc,CAAC9F,CAAD,CAAMY,CAAN,CAAa,CAClC,IAAImF,EAAMnF,CAES,SAAnB,GAAI,MAAOZ,EAAX,EAAiD,GAAjD,GAA+BA,CAAA2E,OAAA,CAAW,CAAX,CAA/B,CACEoB,CADF,CACQ3G,CADR,CAEWI,EAAA,CAASoB,CAAT,CAAJ,CACLmF,CADK,CACC,SADD,CAEInF,CAAJ,EAAczB,CAAd,GAA2ByB,CAA3B,CACLmF,CADK,CACC,WADD,CAEYnF,CAFZ,GAEYA,CA1aLsD,WAwaP,EAEYtD,CA1aauD,OAwazB,IAGL4B,CAHK,CAGC,QAHD,CAMP,OAAOA,EAb2B,CA+BpCC,QAASA,GAAM,CAACzG,CAAD,CAAM0G,CAAN,CAAc,CAC3B,MAAmB,WAAnB,GAAI,MAAO1G,EAAX,CAAuCH,CAAvC,CACO8G,IAAAC,UAAA,CAAe5G,CAAf,CAAoBuG,EAApB,CAAoCG,CAAA,CAAS,IAAT,CAAgB,IAApD,CAFoB,CAkB7BG,QAASA,GAAQ,CAACC,CAAD,CAAO,CACtB,MAAO1G,EAAA,CAAS0G,CAAT,CACA,CAADH,IAAAI,MAAA,CAAWD,CAAX,CAAC,CACDA,CAHgB,CAOxBE,QAASA,GAAS,CAAC3F,CAAD,CAAQ,CACH,UAArB,GAAI,MAAOA,EAAX,CACEA,CADF,CACU,CAAA,CADV;AAEWA,CAAJ,EAA8B,CAA9B,GAAaA,CAAAnB,OAAb,EACD+G,CACJ,CADQC,CAAA,CAAU,EAAV,CAAe7F,CAAf,CACR,CAAAA,CAAA,CAAQ,EAAO,GAAP,EAAE4F,CAAF,EAAmB,GAAnB,EAAcA,CAAd,EAA+B,OAA/B,EAA0BA,CAA1B,EAA+C,IAA/C,EAA0CA,CAA1C,EAA4D,GAA5D,EAAuDA,CAAvD,EAAwE,IAAxE,EAAmEA,CAAnE,CAFH,EAIL5F,CAJK,CAIG,CAAA,CAEV,OAAOA,EATiB,CAe1B8F,QAASA,GAAW,CAACC,CAAD,CAAU,CAC5BA,CAAA,CAAUC,CAAA,CAAOD,CAAP,CAAAE,MAAA,EACV,IAAI,CAGFF,CAAAG,MAAA,EAHE,CAIF,MAAMC,CAAN,CAAS,EAGX,IAAIC,EAAWJ,CAAA,CAAO,OAAP,CAAAK,OAAA,CAAuBN,CAAvB,CAAAO,KAAA,EACf,IAAI,CACF,MAHcC,EAGP,GAAAR,CAAA,CAAQ,CAAR,CAAAjH,SAAA,CAAoC+G,CAAA,CAAUO,CAAV,CAApC,CACHA,CAAAI,MAAA,CACQ,YADR,CACA,CAAsB,CAAtB,CAAAC,QAAA,CACU,aADV,CACyB,QAAQ,CAACD,CAAD,CAAQlE,CAAR,CAAkB,CAAE,MAAO,GAAP,CAAauD,CAAA,CAAUvD,CAAV,CAAf,CADnD,CAHF,CAKF,MAAM6D,CAAN,CAAS,CACT,MAAON,EAAA,CAAUO,CAAV,CADE,CAfiB,CAgC9BM,QAASA,GAAqB,CAAC1G,CAAD,CAAQ,CACpC,GAAI,CACF,MAAO2G,mBAAA,CAAmB3G,CAAnB,CADL,CAEF,MAAMmG,CAAN,CAAS,EAHyB,CAatCS,QAASA,GAAa,CAAYC,CAAZ,CAAsB,CAAA,IACtClI,EAAM,EADgC,CAC5BmI,CAD4B,CACjB1H,CACzBH,EAAA,CAAS8H,CAAAF,CAAAE,EAAY,EAAZA,OAAA,CAAsB,GAAtB,CAAT,CAAqC,QAAQ,CAACF,CAAD,CAAW,CACjDA,CAAL,GACEC,CAEA,CAFYD,CAAAE,MAAA,CAAe,GAAf,CAEZ,CADA3H,CACA,CADMsH,EAAA,CAAsBI,CAAA,CAAU,CAAV,CAAtB,CACN,CAAKnF,CAAA,CAAUvC,CAAV,CAAL,GACM+F,CACJ,CADUxD,CAAA,CAAUmF,CAAA,CAAU,CAAV,CAAV,CAAA,CAA0BJ,EAAA,CAAsBI,CAAA,CAAU,CAAV,CAAtB,CAA1B,CAAgE,CAAA,CAC1E;AAAKnI,CAAA,CAAIS,CAAJ,CAAL,CAEUJ,CAAA,CAAQL,CAAA,CAAIS,CAAJ,CAAR,CAAH,CACLT,CAAA,CAAIS,CAAJ,CAAAM,KAAA,CAAcyF,CAAd,CADK,CAGLxG,CAAA,CAAIS,CAAJ,CAHK,CAGM,CAACT,CAAA,CAAIS,CAAJ,CAAD,CAAU+F,CAAV,CALb,CACExG,CAAA,CAAIS,CAAJ,CADF,CACa+F,CAHf,CAHF,CADsD,CAAxD,CAgBA,OAAOxG,EAlBmC,CAqB5CqI,QAASA,GAAU,CAACrI,CAAD,CAAM,CACvB,IAAIsI,EAAQ,EACZhI,EAAA,CAAQN,CAAR,CAAa,QAAQ,CAACqB,CAAD,CAAQZ,CAAR,CAAa,CAC5BJ,CAAA,CAAQgB,CAAR,CAAJ,CACEf,CAAA,CAAQe,CAAR,CAAe,QAAQ,CAACkH,CAAD,CAAa,CAClCD,CAAAvH,KAAA,CAAWyH,EAAA,CAAe/H,CAAf,CAAoB,CAAA,CAApB,CAAX,EAC2B,CAAA,CAAf,GAAA8H,CAAA,CAAsB,EAAtB,CAA2B,GAA3B,CAAiCC,EAAA,CAAeD,CAAf,CAA2B,CAAA,CAA3B,CAD7C,EADkC,CAApC,CADF,CAMAD,CAAAvH,KAAA,CAAWyH,EAAA,CAAe/H,CAAf,CAAoB,CAAA,CAApB,CAAX,EACsB,CAAA,CAAV,GAAAY,CAAA,CAAiB,EAAjB,CAAsB,GAAtB,CAA4BmH,EAAA,CAAenH,CAAf,CAAsB,CAAA,CAAtB,CADxC,EAPgC,CAAlC,CAWA,OAAOiH,EAAApI,OAAA,CAAeoI,CAAA3G,KAAA,CAAW,GAAX,CAAf,CAAiC,EAbjB,CA4BzB8G,QAASA,GAAgB,CAACjC,CAAD,CAAM,CAC7B,MAAOgC,GAAA,CAAehC,CAAf,CAAoB,CAAA,CAApB,CAAAsB,QAAA,CACY,OADZ,CACqB,GADrB,CAAAA,QAAA,CAEY,OAFZ,CAEqB,GAFrB,CAAAA,QAAA,CAGY,OAHZ,CAGqB,GAHrB,CADsB,CAmB/BU,QAASA,GAAc,CAAChC,CAAD,CAAMkC,CAAN,CAAuB,CAC5C,MAAOC,mBAAA,CAAmBnC,CAAnB,CAAAsB,QAAA,CACY,OADZ,CACqB,GADrB,CAAAA,QAAA,CAEY,OAFZ,CAEqB,GAFrB,CAAAA,QAAA,CAGY,MAHZ,CAGoB,GAHpB,CAAAA,QAAA,CAIY,OAJZ,CAIqB,GAJrB,CAAAA,QAAA,CAKY,MALZ,CAKqBY,CAAA,CAAkB,KAAlB,CAA0B,GAL/C,CADqC,CAwD9CE,QAASA,GAAW,CAACxB,CAAD;AAAUyB,CAAV,CAAqB,CAOvCnB,QAASA,EAAM,CAACN,CAAD,CAAU,CACvBA,CAAA,EAAW0B,CAAA/H,KAAA,CAAcqG,CAAd,CADY,CAPc,IACnC0B,EAAW,CAAC1B,CAAD,CADwB,CAEnC2B,CAFmC,CAGnCC,CAHmC,CAInCC,EAAQ,CAAC,QAAD,CAAW,QAAX,CAAqB,UAArB,CAAiC,aAAjC,CAJ2B,CAKnCC,EAAsB,mCAM1B5I,EAAA,CAAQ2I,CAAR,CAAe,QAAQ,CAACE,CAAD,CAAO,CAC5BF,CAAA,CAAME,CAAN,CAAA,CAAc,CAAA,CACdzB,EAAA,CAAO9H,CAAAwJ,eAAA,CAAwBD,CAAxB,CAAP,CACAA,EAAA,CAAOA,CAAArB,QAAA,CAAa,GAAb,CAAkB,KAAlB,CACHV,EAAAiC,iBAAJ,GACE/I,CAAA,CAAQ8G,CAAAiC,iBAAA,CAAyB,GAAzB,CAA+BF,CAA/B,CAAR,CAA8CzB,CAA9C,CAEA,CADApH,CAAA,CAAQ8G,CAAAiC,iBAAA,CAAyB,GAAzB,CAA+BF,CAA/B,CAAsC,KAAtC,CAAR,CAAsDzB,CAAtD,CACA,CAAApH,CAAA,CAAQ8G,CAAAiC,iBAAA,CAAyB,GAAzB,CAA+BF,CAA/B,CAAsC,GAAtC,CAAR,CAAoDzB,CAApD,CAHF,CAJ4B,CAA9B,CAWApH,EAAA,CAAQwI,CAAR,CAAkB,QAAQ,CAAC1B,CAAD,CAAU,CAClC,GAAI,CAAC2B,CAAL,CAAiB,CAEf,IAAIlB,EAAQqB,CAAAI,KAAA,CADI,GACJ,CADUlC,CAAAmC,UACV,CAD8B,GAC9B,CACR1B,EAAJ,EACEkB,CACA,CADa3B,CACb,CAAA4B,CAAA,CAAUlB,CAAAD,CAAA,CAAM,CAAN,CAAAC,EAAY,EAAZA,SAAA,CAAwB,MAAxB,CAAgC,GAAhC,CAFZ,EAIExH,CAAA,CAAQ8G,CAAAoC,WAAR,CAA4B,QAAQ,CAAC3F,CAAD,CAAO,CACpCkF,CAAAA,CAAL,EAAmBE,CAAA,CAAMpF,CAAAsF,KAAN,CAAnB,GACEJ,CACA,CADa3B,CACb,CAAA4B,CAAA,CAASnF,CAAAxC,MAFX,CADyC,CAA3C,CAPa,CADiB,CAApC,CAiBI0H,EAAJ,EACEF,CAAA,CAAUE,CAAV,CAAsBC,CAAA,CAAS,CAACA,CAAD,CAAT,CAAoB,EAA1C,CAxCqC,CAxwCF;AA02CvCH,QAASA,GAAS,CAACzB,CAAD,CAAUqC,CAAV,CAAmB,CACnC,IAAIC,EAAcA,QAAQ,EAAG,CAC3BtC,CAAA,CAAUC,CAAA,CAAOD,CAAP,CAEV,IAAIA,CAAAuC,SAAA,EAAJ,CAAwB,CACtB,IAAIC,EAAOxC,CAAA,CAAQ,CAAR,CAAD,GAAgBxH,CAAhB,CAA4B,UAA5B,CAAyCuH,EAAA,CAAYC,CAAZ,CACnD,MAAMvC,GAAA,CAAS,SAAT,CAAwE+E,CAAxE,CAAN,CAFsB,CAKxBH,CAAA,CAAUA,CAAV,EAAqB,EACrBA,EAAA3H,QAAA,CAAgB,CAAC,UAAD,CAAa,QAAQ,CAAC+H,CAAD,CAAW,CAC9CA,CAAAxI,MAAA,CAAe,cAAf,CAA+B+F,CAA/B,CAD8C,CAAhC,CAAhB,CAGAqC,EAAA3H,QAAA,CAAgB,IAAhB,CACI6H,EAAAA,CAAWG,EAAA,CAAeL,CAAf,CACfE,EAAAI,OAAA,CAAgB,CAAC,YAAD,CAAe,cAAf,CAA+B,UAA/B,CAA2C,WAA3C,CAAwD,UAAxD,CACb,QAAQ,CAACC,CAAD,CAAQ5C,CAAR,CAAiB6C,CAAjB,CAA0BN,CAA1B,CAAoCO,CAApC,CAA6C,CACpDF,CAAAG,OAAA,CAAa,QAAQ,EAAG,CACtB/C,CAAAgD,KAAA,CAAa,WAAb,CAA0BT,CAA1B,CACAM,EAAA,CAAQ7C,CAAR,CAAA,CAAiB4C,CAAjB,CAFsB,CAAxB,CADoD,CADxC,CAAhB,CAQA,OAAOL,EAtBoB,CAA7B,CAyBIU,EAAqB,sBAEzB,IAAI1K,CAAJ,EAAc,CAAC0K,CAAAC,KAAA,CAAwB3K,CAAAwJ,KAAxB,CAAf,CACE,MAAOO,EAAA,EAGT/J,EAAAwJ,KAAA,CAAcxJ,CAAAwJ,KAAArB,QAAA,CAAoBuC,CAApB,CAAwC,EAAxC,CACdE,GAAAC,gBAAA,CAA0BC,QAAQ,CAACC,CAAD,CAAe,CAC/CpK,CAAA,CAAQoK,CAAR,CAAsB,QAAQ,CAAC1B,CAAD,CAAS,CACrCS,CAAA1I,KAAA,CAAaiI,CAAb,CADqC,CAAvC,CAGAU;CAAA,EAJ+C,CAjCd,CA0CrCiB,QAASA,GAAU,CAACxB,CAAD,CAAOyB,CAAP,CAAkB,CACnCA,CAAA,CAAYA,CAAZ,EAAyB,GACzB,OAAOzB,EAAArB,QAAA,CAAa+C,EAAb,CAAgC,QAAQ,CAACC,CAAD,CAASC,CAAT,CAAc,CAC3D,OAAQA,CAAA,CAAMH,CAAN,CAAkB,EAA1B,EAAgCE,CAAAE,YAAA,EAD2B,CAAtD,CAF4B,CAmCrCC,QAASA,GAAS,CAACC,CAAD,CAAM/B,CAAN,CAAYgC,CAAZ,CAAoB,CACpC,GAAI,CAACD,CAAL,CACE,KAAMrG,GAAA,CAAS,MAAT,CAA2CsE,CAA3C,EAAmD,GAAnD,CAA0DgC,CAA1D,EAAoE,UAApE,CAAN,CAEF,MAAOD,EAJ6B,CAOtCE,QAASA,GAAW,CAACF,CAAD,CAAM/B,CAAN,CAAYkC,CAAZ,CAAmC,CACjDA,CAAJ,EAA6BhL,CAAA,CAAQ6K,CAAR,CAA7B,GACIA,CADJ,CACUA,CAAA,CAAIA,CAAAhL,OAAJ,CAAiB,CAAjB,CADV,CAIA+K,GAAA,CAAUvK,CAAA,CAAWwK,CAAX,CAAV,CAA2B/B,CAA3B,CAAiC,sBAAjC,EACK+B,CAAA,EAAqB,QAArB,EAAO,MAAOA,EAAd,CAAgCA,CAAAI,YAAAnC,KAAhC,EAAwD,QAAxD,CAAmE,MAAO+B,EAD/E,EAEA,OAAOA,EAP8C,CAevDK,QAASA,GAAuB,CAACpC,CAAD,CAAO3I,CAAP,CAAgB,CAC9C,GAAa,gBAAb,GAAI2I,CAAJ,CACE,KAAMtE,GAAA,CAAS,SAAT,CAA8DrE,CAA9D,CAAN,CAF4C,CAchDgL,QAASA,GAAM,CAACxL,CAAD,CAAMyL,CAAN,CAAYC,CAAZ,CAA2B,CACxC,GAAI,CAACD,CAAL,CAAW,MAAOzL,EACdc,EAAAA,CAAO2K,CAAArD,MAAA,CAAW,GAAX,CAKX,KAJA,IAAI3H,CAAJ,CACIkL,EAAe3L,CADnB,CAEI4L,EAAM9K,CAAAZ,OAFV,CAISgB,EAAI,CAAb,CAAgBA,CAAhB,CAAoB0K,CAApB,CAAyB1K,CAAA,EAAzB,CACET,CACA,CADMK,CAAA,CAAKI,CAAL,CACN,CAAIlB,CAAJ,GACEA,CADF,CACQ,CAAC2L,CAAD,CAAgB3L,CAAhB,EAAqBS,CAArB,CADR,CAIF,OAAI,CAACiL,CAAL;AAAsBhL,CAAA,CAAWV,CAAX,CAAtB,CACS+F,EAAA,CAAK4F,CAAL,CAAmB3L,CAAnB,CADT,CAGOA,CAhBiC,CAwB1C6L,QAASA,GAAgB,CAACC,CAAD,CAAQ,CAAA,IAC3BC,EAAYD,CAAA,CAAM,CAAN,CACZE,EAAAA,CAAUF,CAAA,CAAMA,CAAA5L,OAAN,CAAqB,CAArB,CACd,IAAI6L,CAAJ,GAAkBC,CAAlB,CACE,MAAO3E,EAAA,CAAO0E,CAAP,CAIT,KAAIjD,EAAW,CAAC1B,CAAD,CAEf,GAAG,CACDA,CAAA,CAAUA,CAAA6E,YACV,IAAI,CAAC7E,CAAL,CAAc,KACd0B,EAAA/H,KAAA,CAAcqG,CAAd,CAHC,CAAH,MAISA,CAJT,GAIqB4E,CAJrB,CAMA,OAAO3E,EAAA,CAAOyB,CAAP,CAhBwB,CA4BjCoD,QAASA,GAAiB,CAACvM,CAAD,CAAS,CAEjC,IAAIwM,EAAkBrM,CAAA,CAAO,WAAP,CAAtB,CACI+E,EAAW/E,CAAA,CAAO,IAAP,CAMXyK,EAAAA,CAAiB5K,CAHZ,QAGL4K,GAAiB5K,CAHE,QAGnB4K,CAH+B,EAG/BA,CAGJA,EAAA6B,SAAA,CAAmB7B,CAAA6B,SAAnB,EAAuCtM,CAEvC,OAAcyK,EARL,OAQT,GAAcA,CARS,OAQvB,CAAiC8B,QAAQ,EAAG,CAE1C,IAAI5C,EAAU,EAqDd,OAAOT,SAAe,CAACG,CAAD,CAAOmD,CAAP,CAAiBC,CAAjB,CAA2B,CAE7C,GAAa,gBAAb,GAKsBpD,CALtB,CACE,KAAMtE,EAAA,CAAS,SAAT,CAIoBrE,QAJpB,CAAN,CAKA8L,CAAJ,EAAgB7C,CAAA9I,eAAA,CAAuBwI,CAAvB,CAAhB,GACEM,CAAA,CAAQN,CAAR,CADF,CACkB,IADlB,CAGA,OAAcM,EA1ET,CA0EkBN,CA1ElB,CA0EL,GAAcM,CA1EK,CA0EIN,CA1EJ,CA0EnB,CAA6BkD,QAAQ,EAAG,CAkNtCG,QAASA,EAAW,CAACC,CAAD,CAAWC,CAAX,CAAmBC,CAAnB,CAAiC,CACnD,MAAO,SAAQ,EAAG,CAChBC,CAAA,CAAYD,CAAZ,EAA4B,MAA5B,CAAA,CAAoC,CAACF,CAAD,CAAWC,CAAX,CAAmBtK,SAAnB,CAApC,CACA;MAAOyK,EAFS,CADiC,CAjNrD,GAAI,CAACP,CAAL,CACE,KAAMH,EAAA,CAAgB,OAAhB,CAEiDhD,CAFjD,CAAN,CAMF,IAAIyD,EAAc,EAAlB,CAGIE,EAAY,EAHhB,CAKIC,EAASP,CAAA,CAAY,WAAZ,CAAyB,QAAzB,CALb,CAQIK,EAAiB,cAELD,CAFK,YAGPE,CAHO,UAcTR,CAdS,MAuBbnD,CAvBa,UAoCTqD,CAAA,CAAY,UAAZ,CAAwB,UAAxB,CApCS,SA+CVA,CAAA,CAAY,UAAZ,CAAwB,SAAxB,CA/CU,SA0DVA,CAAA,CAAY,UAAZ,CAAwB,SAAxB,CA1DU,OAqEZA,CAAA,CAAY,UAAZ,CAAwB,OAAxB,CArEY,UAiFTA,CAAA,CAAY,UAAZ,CAAwB,UAAxB,CAAoC,SAApC,CAjFS,WAmHRA,CAAA,CAAY,kBAAZ,CAAgC,UAAhC,CAnHQ,QA8HXA,CAAA,CAAY,iBAAZ,CAA+B,UAA/B,CA9HW,YA0IPA,CAAA,CAAY,qBAAZ,CAAmC,UAAnC,CA1IO,WAuJRA,CAAA,CAAY,kBAAZ,CAAgC,WAAhC,CAvJQ,QAoKXO,CApKW,KAgLdC,QAAQ,CAACC,CAAD,CAAQ,CACnBH,CAAA/L,KAAA,CAAekM,CAAf,CACA;MAAO,KAFY,CAhLF,CAsLjBV,EAAJ,EACEQ,CAAA,CAAOR,CAAP,CAGF,OAAQM,EA1M8B,CA1ET,EA0E/B,CAX+C,CAvDP,CART,EAQnC,CAdiC,CAmZnCK,QAASA,GAAkB,CAAC3C,CAAD,CAAS,CAClCrI,CAAA,CAAOqI,CAAP,CAAgB,WACD1B,EADC,MAENvE,EAFM,QAGJpC,CAHI,QAIJmD,EAJI,SAKHgC,CALG,SAMH/G,CANG,UAOFwJ,EAPE,MAQPnH,CARO,MASPoD,EATO,QAUJU,EAVI,UAWFI,EAXE,UAYHjE,EAZG,aAaCG,CAbD,WAcDC,CAdC,UAeF5C,CAfE,YAgBAM,CAhBA,UAiBFuC,CAjBE,UAkBFC,EAlBE,WAmBDO,EAnBC,SAoBHpD,CApBG,SAqBH8M,EArBG,QAsBJhK,EAtBI,WAuBD+D,CAvBC,WAwBDkG,EAxBC,WAyBD,SAAU,CAAV,CAzBC,UA0BFtN,CA1BE,OA2BL6F,EA3BK,CAAhB,CA8BA0H,GAAA,CAAgBnB,EAAA,CAAkBvM,CAAlB,CAChB,IAAI,CACF0N,EAAA,CAAc,UAAd,CADE,CAEF,MAAO7F,CAAP,CAAU,CACV6F,EAAA,CAAc,UAAd,CAA0B,EAA1B,CAAAZ,SAAA,CAAuC,SAAvC,CAAkDa,EAAlD,CADU,CAIZD,EAAA,CAAc,IAAd,CAAoB,CAAC,UAAD,CAApB,CAAkC,CAAC,UAAD,CAChCE,QAAiB,CAAC1D,CAAD,CAAW,CAE1BA,CAAA4C,SAAA,CAAkB,eACDe,EADC,CAAlB,CAGA3D;CAAA4C,SAAA,CAAkB,UAAlB,CAA8BgB,EAA9B,CAAAC,UAAA,CACY,GACHC,EADG,OAECC,EAFD,UAGIA,EAHJ,MAIAC,EAJA,QAKEC,EALF,QAMEC,EANF,OAOCC,EAPD,QAQEC,EARF,QASEC,EATF,YAUMC,EAVN,gBAWUC,EAXV,SAYGC,EAZH,aAaOC,EAbP,YAcMC,EAdN,SAeGC,EAfH,cAgBQC,EAhBR,QAiBEC,EAjBF,QAkBEC,EAlBF,MAmBAC,EAnBA,WAoBKC,EApBL,QAqBEC,EArBF,eAsBSC,EAtBT,aAuBOC,EAvBP,UAwBIC,EAxBJ,QAyBEC,EAzBF,SA0BGC,EA1BH,UA2BIC,EA3BJ,cA4BQC,EA5BR,iBA6BWC,EA7BX,WA8BKC,EA9BL,cA+BQC,EA/BR,SAgCGC,EAhCH,QAiCEC,EAjCF,UAkCIC,EAlCJ,UAmCIC,EAnCJ,YAoCMA,EApCN,SAqCGC,EArCH,CADZ,CAAAnC,UAAA,CAwCY,WACGoC,EADH,CAxCZ,CAAApC,UAAA,CA2CYqC,EA3CZ,CAAArC,UAAA,CA4CYsC,EA5CZ,CA6CAnG;CAAA4C,SAAA,CAAkB,eACDwD,EADC,UAENC,EAFM,UAGNC,EAHM,eAIDC,EAJC,aAKHC,EALG,WAMLC,EANK,mBAOGC,EAPH,SAQPC,EARO,cASFC,EATE,WAULC,EAVK,OAWTC,EAXS,cAYFC,EAZE,WAaLC,EAbK,MAcVC,EAdU,QAeRC,EAfQ,YAgBJC,EAhBI,IAiBZC,EAjBY,MAkBVC,EAlBU,cAmBFC,EAnBE,UAoBNC,EApBM,gBAqBAC,EArBA,UAsBNC,EAtBM,SAuBPC,EAvBO,OAwBTC,EAxBS,iBAyBEC,EAzBF,CAAlB,CAlD0B,CADI,CAAlC,CAtCkC,CAwPpCC,QAASA,GAAS,CAACvI,CAAD,CAAO,CACvB,MAAOA,EAAArB,QAAA,CACG6J,EADH,CACyB,QAAQ,CAACC,CAAD,CAAIhH,CAAJ,CAAeE,CAAf,CAAuB+G,CAAvB,CAA+B,CACnE,MAAOA,EAAA,CAAS/G,CAAAgH,YAAA,EAAT,CAAgChH,CAD4B,CADhE,CAAAhD,QAAA,CAIGiK,EAJH,CAIoB,OAJpB,CADgB,CAgBzBC,QAASA,GAAuB,CAAC7I,CAAD,CAAO8I,CAAP,CAAqBC,CAArB,CAAkCC,CAAlC,CAAuD,CAMrFC,QAASA,EAAW,CAACC,CAAD,CAAQ,CAAA,IAEtBpO,EAAOiO,CAAA,EAAeG,CAAf,CAAuB,CAAC,IAAAC,OAAA,CAAYD,CAAZ,CAAD,CAAvB;AAA8C,CAAC,IAAD,CAF/B,CAGtBE,EAAYN,CAHU,CAItBO,CAJsB,CAIjBC,CAJiB,CAIPC,CAJO,CAKtBtL,CALsB,CAKbuL,CALa,CAKYC,CAEtC,IAAI,CAACT,CAAL,EAAqC,IAArC,EAA4BE,CAA5B,CACE,IAAA,CAAMpO,CAAA/D,OAAN,CAAA,CAEE,IADAsS,CACkB,CADZvO,CAAA4O,MAAA,EACY,CAAdJ,CAAc,CAAH,CAAG,CAAAC,CAAA,CAAYF,CAAAtS,OAA9B,CAA0CuS,CAA1C,CAAqDC,CAArD,CAAgED,CAAA,EAAhE,CAOE,IANArL,CAMoB,CANVC,CAAA,CAAOmL,CAAA,CAAIC,CAAJ,CAAP,CAMU,CALhBF,CAAJ,CACEnL,CAAA0L,eAAA,CAAuB,UAAvB,CADF,CAGEP,CAHF,CAGc,CAACA,CAEK,CAAhBI,CAAgB,CAAH,CAAG,CAAAI,CAAA,CAAe7S,CAAA0S,CAAA1S,CAAWkH,CAAAwL,SAAA,EAAX1S,QAAnC,CACIyS,CADJ,CACiBI,CADjB,CAEIJ,CAAA,EAFJ,CAGE1O,CAAAlD,KAAA,CAAUiS,EAAA,CAAOJ,CAAA,CAASD,CAAT,CAAP,CAAV,CAKR,OAAOM,EAAA5M,MAAA,CAAmB,IAAnB,CAAyBjE,SAAzB,CAzBmB,CAL5B,IAAI6Q,EAAeD,EAAA/M,GAAA,CAAUkD,CAAV,CAAnB,CACA8J,EAAeA,CAAAC,UAAfD,EAAyCA,CACzCb,EAAAc,UAAA,CAAwBD,CACxBD,GAAA/M,GAAA,CAAUkD,CAAV,CAAA,CAAkBiJ,CAJmE,CAyGvFe,QAASA,EAAM,CAAC/L,CAAD,CAAU,CACvB,GAAIA,CAAJ,WAAuB+L,EAAvB,CACE,MAAO/L,EAELhH,EAAA,CAASgH,CAAT,CAAJ,GACEA,CADF,CACYgM,EAAA,CAAKhM,CAAL,CADZ,CAGA,IAAI,EAAE,IAAF,WAAkB+L,EAAlB,CAAJ,CAA+B,CAC7B,GAAI/S,CAAA,CAASgH,CAAT,CAAJ,EAA8C,GAA9C,EAAyBA,CAAAhC,OAAA,CAAe,CAAf,CAAzB,CACE,KAAMiO,GAAA,CAAa,OAAb,CAAN,CAEF,MAAO,KAAIF,CAAJ,CAAW/L,CAAX,CAJsB,CAO/B,GAAIhH,CAAA,CAASgH,CAAT,CAAJ,CAAuB,CACgBA,IAAAA,EAAAA,CA1BvC5G,EAAA,CAAqBZ,CACrB,KAAI0T,CAEJ,IAAKA,CAAL,CAAcC,EAAAjK,KAAA,CAAuB3B,CAAvB,CAAd,CACS,CAAA,CAAA,CAAA,CAAA,cAAA,CAAA,CAAA,CAAA,CAAA,CAAA,CAAA,CADT,KAAA,CAIO,IAAA;AAAA,CAAA,CA1CQiC,CACX4J,EAAAA,CAAWhT,CAAAiT,uBAAA,EACX3H,EAAAA,CAAQ,EAEZ,IARQ4H,EAAApJ,KAAA,CA8CD3C,CA9CC,CAQR,CAGO,CACLgM,CAAA,CAAMH,CAAAI,YAAA,CAAqBpT,CAAAqT,cAAA,CAAsB,KAAtB,CAArB,CAENjK,EAAA,CAAM,CAACkK,EAAAxK,KAAA,CAgCF3B,CAhCE,CAAD,EAA+B,CAAC,EAAD,CAAK,EAAL,CAA/B,EAAyC,CAAzC,CAAAqD,YAAA,EACN+I,EAAA,CAAOC,EAAA,CAAQpK,CAAR,CAAP,EAAuBoK,EAAAC,SACvBN,EAAAO,UAAA,CAAgB,mBAAhB,CACEH,CAAA,CAAK,CAAL,CADF,CA8BKpM,CA7BOG,QAAA,CAAaqM,EAAb,CAA+B,WAA/B,CADZ,CAC0DJ,CAAA,CAAK,CAAL,CAC1DJ,EAAAS,YAAA,CAAgBT,CAAAU,WAAhB,CAIA,KADAnT,CACA,CADI6S,CAAA,CAAK,CAAL,CACJ,CAAO7S,CAAA,EAAP,CAAA,CACEyS,CAAA,CAAMA,CAAAW,UAGHC,EAAA,CAAE,CAAP,KAAUC,CAAV,CAAab,CAAAc,WAAAvU,OAAb,CAAoCqU,CAApC,CAAsCC,CAAtC,CAA0C,EAAED,CAA5C,CAA+CzI,CAAA/K,KAAA,CAAW4S,CAAAc,WAAA,CAAeF,CAAf,CAAX,CAE/CZ,EAAA,CAAMH,CAAAa,WACNV,EAAAe,YAAA,CAAkB,EAlBb,CAHP,IAEE5I,EAAA/K,KAAA,CAAWP,CAAAmU,eAAA,CAoCNhN,CApCM,CAAX,CAuBF6L,EAAAkB,YAAA,CAAuB,EACvBlB,EAAAU,UAAA,CAAqB,EACrB,EAAA,CAAOpI,CAOP,CAuBE8I,EAAA,CAAe,IAAf,CAvBF,CAuBE,CACevN,EAAAmM,CAAO5T,CAAA6T,uBAAA,EAAPD,CACf9L,OAAA,CAAgB,IAAhB,CAHqB,CAAvB,IAKEkN,GAAA,CAAe,IAAf;AAAqBxN,CAArB,CAnBqB,CAuBzByN,QAASA,GAAW,CAACzN,CAAD,CAAU,CAC5B,MAAOA,EAAA0N,UAAA,CAAkB,CAAA,CAAlB,CADqB,CAI9BC,QAASA,GAAY,CAAC3N,CAAD,CAAS,CAC5B4N,EAAA,CAAiB5N,CAAjB,CAD4B,KAElBlG,EAAI,CAAd,KAAiB0R,CAAjB,CAA4BxL,CAAAqN,WAA5B,EAAkD,EAAlD,CAAsDvT,CAAtD,CAA0D0R,CAAA1S,OAA1D,CAA2EgB,CAAA,EAA3E,CACE6T,EAAA,CAAanC,CAAA,CAAS1R,CAAT,CAAb,CAH0B,CAO9B+T,QAASA,GAAS,CAAC7N,CAAD,CAAU8N,CAAV,CAAgBjP,CAAhB,CAAoBkP,CAApB,CAAiC,CACjD,GAAInS,CAAA,CAAUmS,CAAV,CAAJ,CAA4B,KAAM9B,GAAA,CAAa,SAAb,CAAN,CADqB,IAG7C+B,EAASC,EAAA,CAAmBjO,CAAnB,CAA4B,QAA5B,CACAiO,GAAAC,CAAmBlO,CAAnBkO,CAA4B,QAA5BA,CAEb,GAEIvS,CAAA,CAAYmS,CAAZ,CAAJ,CACE5U,CAAA,CAAQ8U,CAAR,CAAgB,QAAQ,CAACG,CAAD,CAAeL,CAAf,CAAqB,CAC3CM,EAAA,CAAsBpO,CAAtB,CAA+B8N,CAA/B,CAAqCK,CAArC,CACA,QAAOH,CAAA,CAAOF,CAAP,CAFoC,CAA7C,CADF,CAME5U,CAAA,CAAQ4U,CAAA9M,MAAA,CAAW,GAAX,CAAR,CAAyB,QAAQ,CAAC8M,CAAD,CAAO,CAClCnS,CAAA,CAAYkD,CAAZ,CAAJ,EACEuP,EAAA,CAAsBpO,CAAtB,CAA+B8N,CAA/B,CAAqCE,CAAA,CAAOF,CAAP,CAArC,CACA,CAAA,OAAOE,CAAA,CAAOF,CAAP,CAFT,EAIE9Q,EAAA,CAAYgR,CAAA,CAAOF,CAAP,CAAZ,EAA4B,EAA5B,CAAgCjP,CAAhC,CALoC,CAAxC,CARF,CANiD,CAyBnD+O,QAASA,GAAgB,CAAC5N,CAAD,CAAU+B,CAAV,CAAgB,CAAA,IACnCsM,EAAYrO,CAAA,CAAQsO,EAAR,CADuB,CAEnCC,EAAeC,EAAA,CAAQH,CAAR,CAEfE,EAAJ,GACMxM,CAAJ,CACE,OAAOyM,EAAA,CAAQH,CAAR,CAAArL,KAAA,CAAwBjB,CAAxB,CADT,EAKIwM,CAAAL,OAKJ,GAJEK,CAAAP,OAAAS,SACA,EADgCF,CAAAL,OAAA,CAAoB,EAApB,CAAwB,UAAxB,CAChC,CAAAL,EAAA,CAAU7N,CAAV,CAGF,EADA,OAAOwO,EAAA,CAAQH,CAAR,CACP,CAAArO,CAAA,CAAQsO,EAAR,CAAA,CAAkB7V,CAVlB,CADF,CAJuC,CAmBzCwV,QAASA,GAAkB,CAACjO,CAAD,CAAU3G,CAAV,CAAeY,CAAf,CAAsB,CAAA,IAC3CoU;AAAYrO,CAAA,CAAQsO,EAAR,CAD+B,CAE3CC,EAAeC,EAAA,CAAQH,CAAR,EAAsB,EAAtB,CAEnB,IAAIzS,CAAA,CAAU3B,CAAV,CAAJ,CACOsU,CAIL,GAHEvO,CAAA,CAAQsO,EAAR,CACA,CADkBD,CAClB,CA1NuB,EAAEK,EA0NzB,CAAAH,CAAA,CAAeC,EAAA,CAAQH,CAAR,CAAf,CAAoC,EAEtC,EAAAE,CAAA,CAAalV,CAAb,CAAA,CAAoBY,CALtB,KAOE,OAAOsU,EAAP,EAAuBA,CAAA,CAAalV,CAAb,CAXsB,CAejDsV,QAASA,GAAU,CAAC3O,CAAD,CAAU3G,CAAV,CAAeY,CAAf,CAAsB,CAAA,IACnC+I,EAAOiL,EAAA,CAAmBjO,CAAnB,CAA4B,MAA5B,CAD4B,CAEnC4O,EAAWhT,CAAA,CAAU3B,CAAV,CAFwB,CAGnC4U,EAAa,CAACD,CAAdC,EAA0BjT,CAAA,CAAUvC,CAAV,CAHS,CAInCyV,EAAiBD,CAAjBC,EAA+B,CAACjT,CAAA,CAASxC,CAAT,CAE/B2J,EAAL,EAAc8L,CAAd,EACEb,EAAA,CAAmBjO,CAAnB,CAA4B,MAA5B,CAAoCgD,CAApC,CAA2C,EAA3C,CAGF,IAAI4L,CAAJ,CACE5L,CAAA,CAAK3J,CAAL,CAAA,CAAYY,CADd,KAGE,IAAI4U,CAAJ,CAAgB,CACd,GAAIC,CAAJ,CAEE,MAAO9L,EAAP,EAAeA,CAAA,CAAK3J,CAAL,CAEfyB,EAAA,CAAOkI,CAAP,CAAa3J,CAAb,CALY,CAAhB,IAQE,OAAO2J,EArB4B,CA0BzC+L,QAASA,GAAc,CAAC/O,CAAD,CAAUgP,CAAV,CAAoB,CACzC,MAAKhP,EAAAiP,aAAL,CAEuC,EAFvC,CACSvO,CAAA,GAAAA,EAAOV,CAAAiP,aAAA,CAAqB,OAArB,CAAPvO,EAAwC,EAAxCA,EAA8C,GAA9CA,SAAA,CAA2D,SAA3D,CAAsE,GAAtE,CAAA5D,QAAA,CACI,GADJ,CACUkS,CADV,CACqB,GADrB,CADT,CAAkC,CAAA,CADO,CAM3CE,QAASA,GAAiB,CAAClP,CAAD,CAAUmP,CAAV,CAAsB,CAC1CA,CAAJ,EAAkBnP,CAAAoP,aAAlB,EACElW,CAAA,CAAQiW,CAAAnO,MAAA,CAAiB,GAAjB,CAAR,CAA+B,QAAQ,CAACqO,CAAD,CAAW,CAChDrP,CAAAoP,aAAA,CAAqB,OAArB,CAA8BpD,EAAA,CACzBtL,CAAA,GAAAA,EAAOV,CAAAiP,aAAA,CAAqB,OAArB,CAAPvO,EAAwC,EAAxCA,EAA8C,GAA9CA,SAAA,CACQ,SADR;AACmB,GADnB,CAAAA,QAAA,CAEQ,GAFR,CAEcsL,EAAA,CAAKqD,CAAL,CAFd,CAE+B,GAF/B,CAEoC,GAFpC,CADyB,CAA9B,CADgD,CAAlD,CAF4C,CAYhDC,QAASA,GAAc,CAACtP,CAAD,CAAUmP,CAAV,CAAsB,CAC3C,GAAIA,CAAJ,EAAkBnP,CAAAoP,aAAlB,CAAwC,CACtC,IAAIG,EAAmB7O,CAAA,GAAAA,EAAOV,CAAAiP,aAAA,CAAqB,OAArB,CAAPvO,EAAwC,EAAxCA,EAA8C,GAA9CA,SAAA,CACU,SADV,CACqB,GADrB,CAGvBxH,EAAA,CAAQiW,CAAAnO,MAAA,CAAiB,GAAjB,CAAR,CAA+B,QAAQ,CAACqO,CAAD,CAAW,CAChDA,CAAA,CAAWrD,EAAA,CAAKqD,CAAL,CAC4C,GAAvD,GAAIE,CAAAzS,QAAA,CAAwB,GAAxB,CAA8BuS,CAA9B,CAAyC,GAAzC,CAAJ,GACEE,CADF,EACqBF,CADrB,CACgC,GADhC,CAFgD,CAAlD,CAOArP,EAAAoP,aAAA,CAAqB,OAArB,CAA8BpD,EAAA,CAAKuD,CAAL,CAA9B,CAXsC,CADG,CAgB7C/B,QAASA,GAAc,CAACgC,CAAD,CAAO9N,CAAP,CAAiB,CACtC,GAAIA,CAAJ,CAAc,CACZA,CAAA,CAAaA,CAAAnF,SACF,EADuB,CAAAX,CAAA,CAAU8F,CAAA5I,OAAV,CACvB,EADsDD,EAAA,CAAS6I,CAAT,CACtD,CACP,CAAEA,CAAF,CADO,CAAPA,CAEJ,KAAI,IAAI5H,EAAE,CAAV,CAAaA,CAAb,CAAiB4H,CAAA5I,OAAjB,CAAkCgB,CAAA,EAAlC,CACE0V,CAAA7V,KAAA,CAAU+H,CAAA,CAAS5H,CAAT,CAAV,CALU,CADwB,CAWxC2V,QAASA,GAAgB,CAACzP,CAAD,CAAU+B,CAAV,CAAgB,CACvC,MAAO2N,GAAA,CAAoB1P,CAApB,CAA6B,GAA7B,EAAoC+B,CAApC,EAA4C,cAA5C,EAA+D,YAA/D,CADgC,CAIzC2N,QAASA,GAAmB,CAAC1P,CAAD,CAAU+B,CAAV,CAAgB9H,CAAhB,CAAuB,CACjD+F,CAAA,CAAUC,CAAA,CAAOD,CAAP,CAIgB,EAA1B,EAAGA,CAAA,CAAQ,CAAR,CAAAjH,SAAH,GACEiH,CADF,CACYA,CAAAtD,KAAA,CAAa,MAAb,CADZ,CAKA,KAFImF,CAEJ,CAFY5I,CAAA,CAAQ8I,CAAR,CAAA,CAAgBA,CAAhB,CAAuB,CAACA,CAAD,CAEnC,CAAO/B,CAAAlH,OAAP,CAAA,CAAuB,CAErB,IADA,IAAIwD;AAAO0D,CAAA,CAAQ,CAAR,CAAX,CACSlG,EAAI,CADb,CACgB6V,EAAK9N,CAAA/I,OAArB,CAAmCgB,CAAnC,CAAuC6V,CAAvC,CAA2C7V,CAAA,EAA3C,CACE,IAAKG,CAAL,CAAa+F,CAAAgD,KAAA,CAAanB,CAAA,CAAM/H,CAAN,CAAb,CAAb,IAAyCrB,CAAzC,CAAoD,MAAOwB,EAM7D+F,EAAA,CAAUC,CAAA,CAAO3D,CAAAsT,WAAP,EAA6C,EAA7C,GAA2BtT,CAAAvD,SAA3B,EAAmDuD,CAAAuT,KAAnD,CATW,CAV0B,CAuBnDC,QAASA,GAAW,CAAC9P,CAAD,CAAU,CAC5B,IAD4B,IACnBlG,EAAI,CADe,CACZuT,EAAarN,CAAAqN,WAA7B,CAAiDvT,CAAjD,CAAqDuT,CAAAvU,OAArD,CAAwEgB,CAAA,EAAxE,CACE6T,EAAA,CAAaN,CAAA,CAAWvT,CAAX,CAAb,CAEF,KAAA,CAAOkG,CAAAiN,WAAP,CAAA,CACEjN,CAAAgN,YAAA,CAAoBhN,CAAAiN,WAApB,CAL0B,CA+D9B8C,QAASA,GAAkB,CAAC/P,CAAD,CAAU+B,CAAV,CAAgB,CAEzC,IAAIiO,EAAcC,EAAA,CAAalO,CAAA6B,YAAA,EAAb,CAGlB,OAAOoM,EAAP,EAAsBE,EAAA,CAAiBlQ,CAAAzD,SAAjB,CAAtB,EAA4DyT,CALnB,CAgM3CG,QAASA,GAAkB,CAACnQ,CAAD,CAAUgO,CAAV,CAAkB,CAC3C,IAAIG,EAAeA,QAAS,CAACiC,CAAD,CAAQtC,CAAR,CAAc,CACnCsC,CAAAC,eAAL,GACED,CAAAC,eADF,CACyBC,QAAQ,EAAG,CAChCF,CAAAG,YAAA,CAAoB,CAAA,CADY,CADpC,CAMKH,EAAAI,gBAAL,GACEJ,CAAAI,gBADF,CAC0BC,QAAQ,EAAG,CACjCL,CAAAM,aAAA,CAAqB,CAAA,CADY,CADrC,CAMKN,EAAAO,OAAL,GACEP,CAAAO,OADF,CACiBP,CAAAQ,WADjB,EACqCpY,CADrC,CAIA,IAAImD,CAAA,CAAYyU,CAAAS,iBAAZ,CAAJ,CAAyC,CACvC,IAAIC;AAAUV,CAAAC,eACdD,EAAAC,eAAA,CAAuBC,QAAQ,EAAG,CAChCF,CAAAS,iBAAA,CAAyB,CAAA,CACzBC,EAAAtX,KAAA,CAAa4W,CAAb,CAFgC,CAIlCA,EAAAS,iBAAA,CAAyB,CAAA,CANc,CASzCT,CAAAW,mBAAA,CAA2BC,QAAQ,EAAG,CACpC,MAAOZ,EAAAS,iBAAP,EAAuD,CAAA,CAAvD,GAAiCT,CAAAG,YADG,CAKtC,KAAIU,EAAoBnT,EAAA,CAAYkQ,CAAA,CAAOF,CAAP,EAAesC,CAAAtC,KAAf,CAAZ,EAA0C,EAA1C,CAExB5U,EAAA,CAAQ+X,CAAR,CAA2B,QAAQ,CAACpS,CAAD,CAAK,CACtCA,CAAArF,KAAA,CAAQwG,CAAR,CAAiBoQ,CAAjB,CADsC,CAAxC,CAMY,EAAZ,EAAIc,CAAJ,EAEEd,CAAAC,eAEA,CAFuB,IAEvB,CADAD,CAAAI,gBACA,CADwB,IACxB,CAAAJ,CAAAW,mBAAA,CAA2B,IAJ7B,GAOE,OAAOX,CAAAC,eAEP,CADA,OAAOD,CAAAI,gBACP,CAAA,OAAOJ,CAAAW,mBATT,CAvCwC,CAmD1C5C,EAAAgD,KAAA,CAAoBnR,CACpB,OAAOmO,EArDoC,CA+S7CiD,QAASA,GAAO,CAACxY,CAAD,CAAM,CAAA,IAChByY,EAAU,MAAOzY,EADD,CAEhBS,CAEW,SAAf,EAAIgY,CAAJ,EAAmC,IAAnC,GAA2BzY,CAA3B,CACsC,UAApC,EAAI,OAAQS,CAAR,CAAcT,CAAAiC,UAAd,CAAJ,CAEExB,CAFF;AAEQT,CAAAiC,UAAA,EAFR,CAGWxB,CAHX,GAGmBZ,CAHnB,GAIEY,CAJF,CAIQT,CAAAiC,UAJR,CAIwBX,EAAA,EAJxB,CADF,CAQEb,CARF,CAQQT,CAGR,OAAOyY,EAAP,CAAiB,GAAjB,CAAuBhY,CAfH,CAqBtBiY,QAASA,GAAO,CAACvU,CAAD,CAAO,CACrB7D,CAAA,CAAQ6D,CAAR,CAAe,IAAAwU,IAAf,CAAyB,IAAzB,CADqB,CAkGvBC,QAASA,GAAQ,CAAC3S,CAAD,CAAK,CAAA,IAChB4S,CADgB,CAEhBC,CAIa,WAAjB,EAAI,MAAO7S,EAAX,EACQ4S,CADR,CACkB5S,CAAA4S,QADlB,IAEIA,CAUA,CAVU,EAUV,CATI5S,CAAA/F,OASJ,GARE4Y,CAEA,CAFS7S,CAAA7C,SAAA,EAAA0E,QAAA,CAAsBiR,EAAtB,CAAsC,EAAtC,CAET,CADAC,CACA,CADUF,CAAAjR,MAAA,CAAaoR,EAAb,CACV,CAAA3Y,CAAA,CAAQ0Y,CAAA,CAAQ,CAAR,CAAA5Q,MAAA,CAAiB8Q,EAAjB,CAAR,CAAwC,QAAQ,CAAChO,CAAD,CAAK,CACnDA,CAAApD,QAAA,CAAYqR,EAAZ,CAAoB,QAAQ,CAACC,CAAD,CAAMC,CAAN,CAAkBlQ,CAAlB,CAAuB,CACjD0P,CAAA9X,KAAA,CAAaoI,CAAb,CADiD,CAAnD,CADmD,CAArD,CAMF,EAAAlD,CAAA4S,QAAA,CAAaA,CAZjB,EAcWxY,CAAA,CAAQ4F,CAAR,CAAJ,EACLqT,CAEA,CAFOrT,CAAA/F,OAEP,CAFmB,CAEnB,CADAkL,EAAA,CAAYnF,CAAA,CAAGqT,CAAH,CAAZ,CAAsB,IAAtB,CACA,CAAAT,CAAA,CAAU5S,CAAAE,MAAA,CAAS,CAAT,CAAYmT,CAAZ,CAHL,EAKLlO,EAAA,CAAYnF,CAAZ,CAAgB,IAAhB,CAAsB,CAAA,CAAtB,CAEF,OAAO4S,EA3Ba,CAygBtB/O,QAASA,GAAc,CAACyP,CAAD,CAAgB,CAmCrCC,QAASA,EAAa,CAACC,CAAD,CAAW,CAC/B,MAAO,SAAQ,CAAChZ,CAAD,CAAMY,CAAN,CAAa,CAC1B,GAAI4B,CAAA,CAASxC,CAAT,CAAJ,CACEH,CAAA,CAAQG,CAAR,CAAaU,EAAA,CAAcsY,CAAd,CAAb,CADF,KAGE,OAAOA,EAAA,CAAShZ,CAAT,CAAcY,CAAd,CAJiB,CADG,CAUjCoL,QAASA,EAAQ,CAACtD,CAAD,CAAOuQ,CAAP,CAAkB,CACjCnO,EAAA,CAAwBpC,CAAxB,CAA8B,SAA9B,CACA,IAAIzI,CAAA,CAAWgZ,CAAX,CAAJ;AAA6BrZ,CAAA,CAAQqZ,CAAR,CAA7B,CACEA,CAAA,CAAYC,CAAAC,YAAA,CAA6BF,CAA7B,CAEd,IAAI,CAACA,CAAAG,KAAL,CACE,KAAM1N,GAAA,CAAgB,MAAhB,CAA2EhD,CAA3E,CAAN,CAEF,MAAO2Q,EAAA,CAAc3Q,CAAd,CAAqB4Q,CAArB,CAAP,CAA8CL,CARb,CAWnCrN,QAASA,EAAO,CAAClD,CAAD,CAAO6Q,CAAP,CAAkB,CAAE,MAAOvN,EAAA,CAAStD,CAAT,CAAe,MAAQ6Q,CAAR,CAAf,CAAT,CA6BlCC,QAASA,EAAW,CAACV,CAAD,CAAe,CAAA,IAC7BzM,EAAY,EADiB,CACboN,CADa,CACHtN,CADG,CACU1L,CADV,CACa6V,CAC9CzW,EAAA,CAAQiZ,CAAR,CAAuB,QAAQ,CAACvQ,CAAD,CAAS,CACtC,GAAI,CAAAmR,CAAAC,IAAA,CAAkBpR,CAAlB,CAAJ,CAAA,CACAmR,CAAAxB,IAAA,CAAkB3P,CAAlB,CAA0B,CAAA,CAA1B,CAEA,IAAI,CACF,GAAI5I,CAAA,CAAS4I,CAAT,CAAJ,CAIE,IAHAkR,CAGgD,CAHrC7M,EAAA,CAAcrE,CAAd,CAGqC,CAFhD8D,CAEgD,CAFpCA,CAAAxG,OAAA,CAAiB2T,CAAA,CAAYC,CAAA5N,SAAZ,CAAjB,CAAAhG,OAAA,CAAwD4T,CAAAG,WAAxD,CAEoC,CAA5CzN,CAA4C,CAA9BsN,CAAAI,aAA8B,CAAPpZ,CAAO,CAAH,CAAG,CAAA6V,CAAA,CAAKnK,CAAA1M,OAArD,CAAyEgB,CAAzE,CAA6E6V,CAA7E,CAAiF7V,CAAA,EAAjF,CAAsF,CAAA,IAChFqZ,EAAa3N,CAAA,CAAY1L,CAAZ,CADmE,CAEhFuL,EAAWkN,CAAAS,IAAA,CAAqBG,CAAA,CAAW,CAAX,CAArB,CAEf9N,EAAA,CAAS8N,CAAA,CAAW,CAAX,CAAT,CAAAlU,MAAA,CAA8BoG,CAA9B,CAAwC8N,CAAA,CAAW,CAAX,CAAxC,CAJoF,CAJxF,IAUW7Z,EAAA,CAAWsI,CAAX,CAAJ,CACH8D,CAAA/L,KAAA,CAAe4Y,CAAA5P,OAAA,CAAwBf,CAAxB,CAAf,CADG,CAEI3I,CAAA,CAAQ2I,CAAR,CAAJ,CACH8D,CAAA/L,KAAA,CAAe4Y,CAAA5P,OAAA,CAAwBf,CAAxB,CAAf,CADG,CAGLoC,EAAA,CAAYpC,CAAZ,CAAoB,QAApB,CAhBA,CAkBF,MAAOxB,CAAP,CAAU,CAYV,KAXInH,EAAA,CAAQ2I,CAAR,CAWE,GAVJA,CAUI,CAVKA,CAAA,CAAOA,CAAA9I,OAAP,CAAuB,CAAvB,CAUL,EARFsH,CAAAgT,QAQE,GARWhT,CAAAiT,MAQX,EARqD,EAQrD,EARsBjT,CAAAiT,MAAAvW,QAAA,CAAgBsD,CAAAgT,QAAhB,CAQtB;CAFJhT,CAEI,CAFAA,CAAAgT,QAEA,CAFY,IAEZ,CAFmBhT,CAAAiT,MAEnB,EAAAtO,EAAA,CAAgB,UAAhB,CACInD,CADJ,CACYxB,CAAAiT,MADZ,EACuBjT,CAAAgT,QADvB,EACoChT,CADpC,CAAN,CAZU,CArBZ,CADsC,CAAxC,CAsCA,OAAOsF,EAxC0B,CA+CnC4N,QAASA,EAAsB,CAACC,CAAD,CAAQtO,CAAR,CAAiB,CAE9CuO,QAASA,EAAU,CAACC,CAAD,CAAc,CAC/B,GAAIF,CAAAha,eAAA,CAAqBka,CAArB,CAAJ,CAAuC,CACrC,GAAIF,CAAA,CAAME,CAAN,CAAJ,GAA2BC,CAA3B,CACE,KAAM3O,GAAA,CAAgB,MAAhB,CAA0DV,CAAA9J,KAAA,CAAU,MAAV,CAA1D,CAAN,CAEF,MAAOgZ,EAAA,CAAME,CAAN,CAJ8B,CAMrC,GAAI,CAGF,MAFApP,EAAA3J,QAAA,CAAa+Y,CAAb,CAEO,CADPF,CAAA,CAAME,CAAN,CACO,CADcC,CACd,CAAAH,CAAA,CAAME,CAAN,CAAA,CAAqBxO,CAAA,CAAQwO,CAAR,CAH1B,CAIF,MAAOE,CAAP,CAAY,CAIZ,KAHIJ,EAAA,CAAME,CAAN,CAGEE,GAHqBD,CAGrBC,EAFJ,OAAOJ,CAAA,CAAME,CAAN,CAEHE,CAAAA,CAAN,CAJY,CAJd,OASU,CACRtP,CAAAoH,MAAA,EADQ,CAhBmB,CAsBjC9I,QAASA,EAAM,CAAC9D,CAAD,CAAKD,CAAL,CAAWgV,CAAX,CAAkB,CAAA,IAC3BC,EAAO,EADoB,CAE3BpC,EAAUD,EAAA,CAAS3S,CAAT,CAFiB,CAG3B/F,CAH2B,CAGnBgB,CAHmB,CAI3BT,CAEAS,EAAA,CAAI,CAAR,KAAWhB,CAAX,CAAoB2Y,CAAA3Y,OAApB,CAAoCgB,CAApC,CAAwChB,CAAxC,CAAgDgB,CAAA,EAAhD,CAAqD,CACnDT,CAAA,CAAMoY,CAAA,CAAQ3X,CAAR,CACN,IAAmB,QAAnB,GAAI,MAAOT,EAAX,CACE,KAAM0L,GAAA,CAAgB,MAAhB,CACyE1L,CADzE,CAAN,CAGFwa,CAAAla,KAAA,CACEia,CACA,EADUA,CAAAra,eAAA,CAAsBF,CAAtB,CACV,CAAEua,CAAA,CAAOva,CAAP,CAAF,CACEma,CAAA,CAAWna,CAAX,CAHJ,CANmD,CAYhDwF,CAAA4S,QAAL,GAEE5S,CAFF,CAEOA,CAAA,CAAG/F,CAAH,CAFP,CAOA,OAAO+F,EAAAI,MAAA,CAASL,CAAT,CAAeiV,CAAf,CAzBwB,CAyCjC,MAAO,QACGlR,CADH;YAbP6P,QAAoB,CAACsB,CAAD,CAAOF,CAAP,CAAe,CAAA,IAC7BG,EAAcA,QAAQ,EAAG,EADI,CAEnBC,CAIdD,EAAAE,UAAA,CAAyBA,CAAAhb,CAAA,CAAQ6a,CAAR,CAAA,CAAgBA,CAAA,CAAKA,CAAAhb,OAAL,CAAmB,CAAnB,CAAhB,CAAwCgb,CAAxCG,WACzBC,EAAA,CAAW,IAAIH,CACfC,EAAA,CAAgBrR,CAAA,CAAOmR,CAAP,CAAaI,CAAb,CAAuBN,CAAvB,CAEhB,OAAO/X,EAAA,CAASmY,CAAT,CAAA,EAA2B1a,CAAA,CAAW0a,CAAX,CAA3B,CAAuDA,CAAvD,CAAuEE,CAV7C,CAa5B,KAGAV,CAHA,UAIKhC,EAJL,KAKA2C,QAAQ,CAACpS,CAAD,CAAO,CAClB,MAAO2Q,EAAAnZ,eAAA,CAA6BwI,CAA7B,CAAoC4Q,CAApC,CAAP,EAA8DY,CAAAha,eAAA,CAAqBwI,CAArB,CAD5C,CALf,CAjEuC,CApIX,IACjC2R,EAAgB,EADiB,CAEjCf,EAAiB,UAFgB,CAGjCtO,EAAO,EAH0B,CAIjC0O,EAAgB,IAAIzB,EAJa,CAKjCoB,EAAgB,UACJ,UACIN,CAAA,CAAc/M,CAAd,CADJ,SAEG+M,CAAA,CAAcnN,CAAd,CAFH,SAGGmN,CAAA,CAiDnBgC,QAAgB,CAACrS,CAAD,CAAOmC,CAAP,CAAoB,CAClC,MAAOe,EAAA,CAAQlD,CAAR,CAAc,CAAC,WAAD,CAAc,QAAQ,CAACsS,CAAD,CAAY,CACrD,MAAOA,EAAA7B,YAAA,CAAsBtO,CAAtB,CAD8C,CAAlC,CAAd,CAD2B,CAjDjB,CAHH,OAICkO,CAAA,CAsDjBnY,QAAc,CAAC8H,CAAD,CAAO3C,CAAP,CAAY,CAAE,MAAO6F,EAAA,CAAQlD,CAAR,CAAcrG,EAAA,CAAQ0D,CAAR,CAAd,CAAT,CAtDT,CAJD,UAKIgT,CAAA,CAuDpBkC,QAAiB,CAACvS,CAAD,CAAO9H,CAAP,CAAc,CAC7BkK,EAAA,CAAwBpC,CAAxB,CAA8B,UAA9B,CACA2Q,EAAA,CAAc3Q,CAAd,CAAA,CAAsB9H,CACtBsa,EAAA,CAAcxS,CAAd,CAAA,CAAsB9H,CAHO,CAvDX,CALJ,WAkEhBua,QAAkB,CAACf,CAAD;AAAcgB,CAAd,CAAuB,CAAA,IACnCC,EAAenC,CAAAS,IAAA,CAAqBS,CAArB,CAAmCd,CAAnC,CADoB,CAEnCgC,EAAWD,CAAAjC,KAEfiC,EAAAjC,KAAA,CAAoBmC,QAAQ,EAAG,CAC7B,IAAIC,EAAeC,CAAAnS,OAAA,CAAwBgS,CAAxB,CAAkCD,CAAlC,CACnB,OAAOI,EAAAnS,OAAA,CAAwB8R,CAAxB,CAAiC,IAAjC,CAAuC,WAAYI,CAAZ,CAAvC,CAFsB,CAJQ,CAlEzB,CADI,CALiB,CAejCtC,EAAoBG,CAAA2B,UAApB9B,CACIe,CAAA,CAAuBZ,CAAvB,CAAsC,QAAQ,EAAG,CAC/C,KAAM3N,GAAA,CAAgB,MAAhB,CAAiDV,CAAA9J,KAAA,CAAU,MAAV,CAAjD,CAAN,CAD+C,CAAjD,CAhB6B,CAmBjCga,EAAgB,EAnBiB,CAoBjCO,EAAoBP,CAAAF,UAApBS,CACIxB,CAAA,CAAuBiB,CAAvB,CAAsC,QAAQ,CAACQ,CAAD,CAAc,CACtD1P,CAAAA,CAAWkN,CAAAS,IAAA,CAAqB+B,CAArB,CAAmCpC,CAAnC,CACf,OAAOmC,EAAAnS,OAAA,CAAwB0C,CAAAoN,KAAxB,CAAuCpN,CAAvC,CAFmD,CAA5D,CAMRnM,EAAA,CAAQ2Z,CAAA,CAAYV,CAAZ,CAAR,CAAoC,QAAQ,CAACtT,CAAD,CAAK,CAAEiW,CAAAnS,OAAA,CAAwB9D,CAAxB,EAA8BtD,CAA9B,CAAF,CAAjD,CAEA,OAAOuZ,EA7B8B,CAkQvCjM,QAASA,GAAqB,EAAG,CAE/B,IAAImM,EAAuB,CAAA,CAE3B,KAAAC,qBAAA,CAA4BC,QAAQ,EAAG,CACrCF,CAAA,CAAuB,CAAA,CADc,CAIvC,KAAAvC,KAAA,CAAY,CAAC,SAAD,CAAY,WAAZ,CAAyB,YAAzB,CAAuC,QAAQ,CAAC0C,CAAD,CAAUC,CAAV,CAAqBC,CAArB,CAAiC,CAO1FC,QAASA,EAAc,CAACzY,CAAD,CAAO,CAC5B,IAAIa,EAAS,IACbxE,EAAA,CAAQ2D,CAAR,CAAc,QAAQ,CAACmD,CAAD,CAAU,CACzBtC,CAAL,EAA+C,GAA/C,GAAeoC,CAAA,CAAUE,CAAAzD,SAAV,CAAf,GAAoDmB,CAApD,CAA6DsC,CAA7D,CAD8B,CAAhC,CAGA,OAAOtC,EALqB,CAP4D;AAe1F6X,QAASA,EAAM,EAAG,CAAA,IACZC,EAAOJ,CAAAI,KAAA,EADK,CACaC,CAGxBD,EAAL,CAGK,CAAKC,CAAL,CAAWjd,CAAAwJ,eAAA,CAAwBwT,CAAxB,CAAX,EAA2CC,CAAAC,eAAA,EAA3C,CAGA,CAAKD,CAAL,CAAWH,CAAA,CAAe9c,CAAAmd,kBAAA,CAA2BH,CAA3B,CAAf,CAAX,EAA8DC,CAAAC,eAAA,EAA9D,CAGa,KAHb,GAGIF,CAHJ,EAGoBL,CAAAS,SAAA,CAAiB,CAAjB,CAAoB,CAApB,CATzB,CAAWT,CAAAS,SAAA,CAAiB,CAAjB,CAAoB,CAApB,CAJK,CAdlB,IAAIpd,EAAW2c,CAAA3c,SAgCXwc,EAAJ,EACEK,CAAA7X,OAAA,CAAkBqY,QAAwB,EAAG,CAAC,MAAOT,EAAAI,KAAA,EAAR,CAA7C,CACEM,QAA8B,EAAG,CAC/BT,CAAA9X,WAAA,CAAsBgY,CAAtB,CAD+B,CADnC,CAMF,OAAOA,EAxCmF,CAAhF,CARmB,CA0SjClL,QAASA,GAAuB,EAAE,CAChC,IAAAoI,KAAA,CAAY,CAAC,OAAD,CAAU,UAAV,CAAsB,QAAQ,CAACsD,CAAD,CAAQC,CAAR,CAAkB,CAC1D,MAAOD,EAAAE,UACA,CAAH,QAAQ,CAACpX,CAAD,CAAK,CAAE,MAAOkX,EAAA,CAAMlX,CAAN,CAAT,CAAV,CACH,QAAQ,CAACA,CAAD,CAAK,CACb,MAAOmX,EAAA,CAASnX,CAAT,CAAa,CAAb,CAAgB,CAAA,CAAhB,CADM,CAHyC,CAAhD,CADoB,CAgClCqX,QAASA,GAAO,CAAC3d,CAAD,CAASC,CAAT,CAAmB2d,CAAnB,CAAyBC,CAAzB,CAAmC,CAsBjDC,QAASA,EAA0B,CAACxX,CAAD,CAAK,CACtC,GAAI,CACFA,CAAAI,MAAA,CAAS,IAAT,CAxvGGF,EAAAvF,KAAA,CAwvGsBwB,SAxvGtB,CAwvGiCgE,CAxvGjC,CAwvGH,CADE,CAAJ,OAEU,CAER,GADAsX,CAAA,EACI,CAA4B,CAA5B,GAAAA,CAAJ,CACE,IAAA,CAAMC,CAAAzd,OAAN,CAAA,CACE,GAAI,CACFyd,CAAAC,IAAA,EAAA,EADE,CAEF,MAAOpW,CAAP,CAAU,CACV+V,CAAAM,MAAA,CAAWrW,CAAX,CADU,CANR,CAH4B,CAtBS;AAyFjDsW,QAASA,EAAW,CAACC,CAAD,CAAWC,CAAX,CAAuB,CACxCC,SAASA,EAAK,EAAG,CAChB3d,CAAA,CAAQ4d,CAAR,CAAiB,QAAQ,CAACC,CAAD,CAAQ,CAAEA,CAAA,EAAF,CAAjC,CACAC,EAAA,CAAcJ,CAAA,CAAWC,CAAX,CAAkBF,CAAlB,CAFE,CAAjBE,CAAA,EADwC,CAuE3CI,QAASA,EAAa,EAAG,CACvBC,CAAA,CAAc,IACVC,EAAJ,EAAsBvY,CAAAwY,IAAA,EAAtB,GAEAD,CACA,CADiBvY,CAAAwY,IAAA,EACjB,CAAAle,CAAA,CAAQme,EAAR,CAA4B,QAAQ,CAACC,CAAD,CAAW,CAC7CA,CAAA,CAAS1Y,CAAAwY,IAAA,EAAT,CAD6C,CAA/C,CAHA,CAFuB,CAhKwB,IAC7CxY,EAAO,IADsC,CAE7C2Y,EAAc/e,CAAA,CAAS,CAAT,CAF+B,CAG7C0D,EAAW3D,CAAA2D,SAHkC,CAI7Csb,EAAUjf,CAAAif,QAJmC,CAK7CZ,EAAare,CAAAqe,WALgC,CAM7Ca,EAAelf,CAAAkf,aAN8B,CAO7CC,EAAkB,EAEtB9Y,EAAA+Y,OAAA,CAAc,CAAA,CAEd,KAAIrB,EAA0B,CAA9B,CACIC,EAA8B,EAGlC3X,EAAAgZ,6BAAA,CAAoCvB,CACpCzX,EAAAiZ,6BAAA,CAAoCC,QAAQ,EAAG,CAAExB,CAAA,EAAF,CA6B/C1X,EAAAmZ,gCAAA,CAAuCC,QAAQ,CAACC,CAAD,CAAW,CAIxD/e,CAAA,CAAQ4d,CAAR,CAAiB,QAAQ,CAACC,CAAD,CAAQ,CAAEA,CAAA,EAAF,CAAjC,CAEgC,EAAhC,GAAIT,CAAJ,CACE2B,CAAA,EADF,CAGE1B,CAAA5c,KAAA,CAAiCse,CAAjC,CATsD,CA7CT,KA6D7CnB,EAAU,EA7DmC,CA8D7CE,CAaJpY,EAAAsZ,UAAA,CAAiBC,QAAQ,CAACtZ,CAAD,CAAK,CACxBlD,CAAA,CAAYqb,CAAZ,CAAJ,EAA8BN,CAAA,CAAY,GAAZ,CAAiBE,CAAjB,CAC9BE,EAAAnd,KAAA,CAAakF,CAAb,CACA,OAAOA,EAHqB,CA3EmB,KAoG7CsY,EAAiBjb,CAAAkc,KApG4B,CAqG7CC,EAAc7f,CAAAkE,KAAA,CAAc,MAAd,CArG+B;AAsG7Cwa,EAAc,IAqBlBtY,EAAAwY,IAAA,CAAWkB,QAAQ,CAAClB,CAAD,CAAM1W,CAAN,CAAe,CAE5BxE,CAAJ,GAAiB3D,CAAA2D,SAAjB,GAAkCA,CAAlC,CAA6C3D,CAAA2D,SAA7C,CACIsb,EAAJ,GAAgBjf,CAAAif,QAAhB,GAAgCA,CAAhC,CAA0Cjf,CAAAif,QAA1C,CAGA,IAAIJ,CAAJ,CACE,IAAID,CAAJ,EAAsBC,CAAtB,CAiBA,MAhBAD,EAgBOvY,CAhBUwY,CAgBVxY,CAfHwX,CAAAoB,QAAJ,CACM9W,CAAJ,CAAa8W,CAAAe,aAAA,CAAqB,IAArB,CAA2B,EAA3B,CAA+BnB,CAA/B,CAAb,EAEEI,CAAAgB,UAAA,CAAkB,IAAlB,CAAwB,EAAxB,CAA4BpB,CAA5B,CAEA,CAAAiB,CAAA5b,KAAA,CAAiB,MAAjB,CAAyB4b,CAAA5b,KAAA,CAAiB,MAAjB,CAAzB,CAJF,CADF,EAQEya,CACA,CADcE,CACd,CAAI1W,CAAJ,CACExE,CAAAwE,QAAA,CAAiB0W,CAAjB,CADF,CAGElb,CAAAkc,KAHF,CAGkBhB,CAZpB,CAeOxY,CAAAA,CAjBP,CADF,IAwBE,OAAOsY,EAAP,EAAsBhb,CAAAkc,KAAA1X,QAAA,CAAsB,MAAtB,CAA6B,GAA7B,CA9BQ,CA3He,KA6J7C2W,GAAqB,EA7JwB,CA8J7CoB,EAAgB,CAAA,CAiCpB7Z,EAAA8Z,YAAA,CAAmBC,QAAQ,CAACV,CAAD,CAAW,CAEpC,GAAI,CAACQ,CAAL,CAAoB,CAMlB,GAAIrC,CAAAoB,QAAJ,CAAsBvX,CAAA,CAAO1H,CAAP,CAAAqgB,GAAA,CAAkB,UAAlB,CAA8B3B,CAA9B,CAEtB,IAAIb,CAAAyC,WAAJ,CAAyB5Y,CAAA,CAAO1H,CAAP,CAAAqgB,GAAA,CAAkB,YAAlB,CAAgC3B,CAAhC,CAAzB,KAEKrY,EAAAsZ,UAAA,CAAejB,CAAf,CAELwB,EAAA,CAAgB,CAAA,CAZE,CAepBpB,EAAA1d,KAAA,CAAwBse,CAAxB,CACA,OAAOA,EAlB6B,CAkCtCrZ,EAAAka,SAAA,CAAgBC,QAAQ,EAAG,CACzB,IAAIX,EAAOC,CAAA5b,KAAA,CAAiB,MAAjB,CACX,OAAO2b,EAAA;AAAOA,CAAA1X,QAAA,CAAa,wBAAb,CAAuC,EAAvC,CAAP,CAAoD,EAFlC,CAQ3B,KAAIsY,EAAc,EAAlB,CACIC,GAAmB,EADvB,CAEIC,EAAata,CAAAka,SAAA,EAsBjBla,EAAAua,QAAA,CAAeC,QAAQ,CAACrX,CAAD,CAAO9H,CAAP,CAAc,CAAA,IAE/Bof,CAF+B,CAEJC,CAFI,CAEIxf,CAFJ,CAEOK,CAE1C,IAAI4H,CAAJ,CACM9H,CAAJ,GAAcxB,CAAd,CACE8e,CAAA+B,OADF,CACuBC,MAAA,CAAOxX,CAAP,CADvB,CACsC,SADtC,CACkDmX,CADlD,CAE0B,wCAF1B,CAIMlgB,CAAA,CAASiB,CAAT,CAJN,GAKIof,CAOA,CAPgBvgB,CAAAye,CAAA+B,OAAAxgB,CAAqBygB,MAAA,CAAOxX,CAAP,CAArBjJ,CAAoC,GAApCA,CAA0CygB,MAAA,CAAOtf,CAAP,CAA1CnB,CACM,QADNA,CACiBogB,CADjBpgB,QAOhB,CANsD,CAMtD,CAAmB,IAAnB,CAAIugB,CAAJ,EACElD,CAAAqD,KAAA,CAAU,UAAV,CAAsBzX,CAAtB,CACE,6DADF,CAEEsX,CAFF,CAEiB,iBAFjB,CAbN,CADF,KAoBO,CACL,GAAI9B,CAAA+B,OAAJ,GAA2BL,EAA3B,CAKE,IAJAA,EAIK,CAJc1B,CAAA+B,OAId,CAHLG,CAGK,CAHSR,EAAAjY,MAAA,CAAuB,IAAvB,CAGT,CAFLgY,CAEK,CAFS,EAET,CAAAlf,CAAA,CAAI,CAAT,CAAYA,CAAZ,CAAgB2f,CAAA3gB,OAAhB,CAAoCgB,CAAA,EAApC,CACEwf,CAEA,CAFSG,CAAA,CAAY3f,CAAZ,CAET,CADAK,CACA,CADQmf,CAAAxc,QAAA,CAAe,GAAf,CACR,CAAY,CAAZ,CAAI3C,CAAJ,GACE4H,CAIA,CAJO2X,QAAA,CAASJ,CAAAK,UAAA,CAAiB,CAAjB;AAAoBxf,CAApB,CAAT,CAIP,CAAI6e,CAAA,CAAYjX,CAAZ,CAAJ,GAA0BtJ,CAA1B,GACEugB,CAAA,CAAYjX,CAAZ,CADF,CACsB2X,QAAA,CAASJ,CAAAK,UAAA,CAAiBxf,CAAjB,CAAyB,CAAzB,CAAT,CADtB,CALF,CAWJ,OAAO6e,EApBF,CAxB4B,CA+DrCpa,EAAAgb,MAAA,CAAaC,QAAQ,CAAChb,CAAD,CAAKib,CAAL,CAAY,CAC/B,IAAIC,CACJzD,EAAA,EACAyD,EAAA,CAAYnD,CAAA,CAAW,QAAQ,EAAG,CAChC,OAAOc,CAAA,CAAgBqC,CAAhB,CACP1D,EAAA,CAA2BxX,CAA3B,CAFgC,CAAtB,CAGTib,CAHS,EAGA,CAHA,CAIZpC,EAAA,CAAgBqC,CAAhB,CAAA,CAA6B,CAAA,CAC7B,OAAOA,EARwB,CAsBjCnb,EAAAgb,MAAAI,OAAA,CAAoBC,QAAQ,CAACC,CAAD,CAAU,CACpC,MAAIxC,EAAA,CAAgBwC,CAAhB,CAAJ,EACE,OAAOxC,CAAA,CAAgBwC,CAAhB,CAGA,CAFPzC,CAAA,CAAayC,CAAb,CAEO,CADP7D,CAAA,CAA2B9a,CAA3B,CACO,CAAA,CAAA,CAJT,EAMO,CAAA,CAP6B,CAtVW,CAkWnDwN,QAASA,GAAgB,EAAE,CACzB,IAAA0J,KAAA,CAAY,CAAC,SAAD,CAAY,MAAZ,CAAoB,UAApB,CAAgC,WAAhC,CACR,QAAQ,CAAE0C,CAAF,CAAagB,CAAb,CAAqBC,CAArB,CAAiC+D,CAAjC,CAA2C,CACjD,MAAO,KAAIjE,EAAJ,CAAYf,CAAZ,CAAqBgF,CAArB,CAAgChE,CAAhC,CAAsCC,CAAtC,CAD0C,CAD3C,CADa,CAsF3BpN,QAASA,GAAqB,EAAG,CAE/B,IAAAyJ,KAAA,CAAY2H,QAAQ,EAAG,CAGrBC,QAASA,EAAY,CAACC,CAAD,CAAUC,CAAV,CAAmB,CAwMtCC,QAASA,EAAO,CAACC,CAAD,CAAQ,CAClBA,CAAJ,EAAaC,CAAb,GACOC,CAAL,CAEWA,CAFX,EAEuBF,CAFvB,GAGEE,CAHF,CAGaF,CAAAG,EAHb,EACED,CADF,CACaF,CAQb,CAHAI,CAAA,CAAKJ,CAAAG,EAAL,CAAcH,CAAAK,EAAd,CAGA,CAFAD,CAAA,CAAKJ,CAAL,CAAYC,CAAZ,CAEA,CADAA,CACA,CADWD,CACX,CAAAC,CAAAE,EAAA,CAAa,IAVf,CADsB,CAmBxBC,QAASA,EAAI,CAACE,CAAD,CAAYC,CAAZ,CAAuB,CAC9BD,CAAJ,EAAiBC,CAAjB,GACMD,CACJ,GADeA,CAAAD,EACf,CAD6BE,CAC7B,EAAIA,CAAJ,GAAeA,CAAAJ,EAAf,CAA6BG,CAA7B,CAFF,CADkC,CA1NpC,GAAIT,CAAJ;AAAeW,CAAf,CACE,KAAMviB,EAAA,CAAO,eAAP,CAAA,CAAwB,KAAxB,CAAkE4hB,CAAlE,CAAN,CAFoC,IAKlCY,EAAO,CAL2B,CAMlCC,EAAQrgB,CAAA,CAAO,EAAP,CAAWyf,CAAX,CAAoB,IAAKD,CAAL,CAApB,CAN0B,CAOlCtX,EAAO,EAP2B,CAQlCoY,EAAYb,CAAZa,EAAuBb,CAAAa,SAAvBA,EAA4CC,MAAAC,UARV,CASlCC,EAAU,EATwB,CAUlCb,EAAW,IAVuB,CAWlCC,EAAW,IAyCf,OAAOM,EAAA,CAAOX,CAAP,CAAP,CAAyB,KAoBlB/I,QAAQ,CAAClY,CAAD,CAAMY,CAAN,CAAa,CACxB,GAAImhB,CAAJ,CAAeC,MAAAC,UAAf,CAAiC,CAC/B,IAAIE,EAAWD,CAAA,CAAQliB,CAAR,CAAXmiB,GAA4BD,CAAA,CAAQliB,CAAR,CAA5BmiB,CAA2C,KAAMniB,CAAN,CAA3CmiB,CAEJhB,EAAA,CAAQgB,CAAR,CAH+B,CAMjC,GAAI,CAAA7f,CAAA,CAAY1B,CAAZ,CAAJ,CAQA,MAPMZ,EAOCY,GAPM+I,EAON/I,EAPaihB,CAAA,EAObjhB,CANP+I,CAAA,CAAK3J,CAAL,CAMOY,CANKA,CAMLA,CAJHihB,CAIGjhB,CAJImhB,CAIJnhB,EAHL,IAAAwhB,OAAA,CAAYd,CAAAthB,IAAZ,CAGKY,CAAAA,CAfiB,CApBH,KAiDlB+Y,QAAQ,CAAC3Z,CAAD,CAAM,CACjB,GAAI+hB,CAAJ,CAAeC,MAAAC,UAAf,CAAiC,CAC/B,IAAIE,EAAWD,CAAA,CAAQliB,CAAR,CAEf,IAAI,CAACmiB,CAAL,CAAe,MAEfhB,EAAA,CAAQgB,CAAR,CAL+B,CAQjC,MAAOxY,EAAA,CAAK3J,CAAL,CATU,CAjDI,QAwEfoiB,QAAQ,CAACpiB,CAAD,CAAM,CACpB,GAAI+hB,CAAJ,CAAeC,MAAAC,UAAf,CAAiC,CAC/B,IAAIE,EAAWD,CAAA,CAAQliB,CAAR,CAEf,IAAI,CAACmiB,CAAL,CAAe,MAEXA,EAAJ,EAAgBd,CAAhB,GAA0BA,CAA1B,CAAqCc,CAAAV,EAArC,CACIU,EAAJ,EAAgBb,CAAhB,GAA0BA,CAA1B,CAAqCa,CAAAZ,EAArC,CACAC,EAAA,CAAKW,CAAAZ,EAAL,CAAgBY,CAAAV,EAAhB,CAEA,QAAOS,CAAA,CAAQliB,CAAR,CATwB,CAYjC,OAAO2J,CAAA,CAAK3J,CAAL,CACP6hB,EAAA,EAdoB,CAxEC,WAkGZQ,QAAQ,EAAG,CACpB1Y,CAAA;AAAO,EACPkY,EAAA,CAAO,CACPK,EAAA,CAAU,EACVb,EAAA,CAAWC,CAAX,CAAsB,IAJF,CAlGC,SAmHdgB,QAAQ,EAAG,CAGlBJ,CAAA,CADAJ,CACA,CAFAnY,CAEA,CAFO,IAGP,QAAOiY,CAAA,CAAOX,CAAP,CAJW,CAnHG,MA2IjBsB,QAAQ,EAAG,CACf,MAAO9gB,EAAA,CAAO,EAAP,CAAWqgB,CAAX,CAAkB,MAAOD,CAAP,CAAlB,CADQ,CA3IM,CApDa,CAFxC,IAAID,EAAS,EA+ObZ,EAAAuB,KAAA,CAAoBC,QAAQ,EAAG,CAC7B,IAAID,EAAO,EACX1iB,EAAA,CAAQ+hB,CAAR,CAAgB,QAAQ,CAAC1H,CAAD,CAAQ+G,CAAR,CAAiB,CACvCsB,CAAA,CAAKtB,CAAL,CAAA,CAAgB/G,CAAAqI,KAAA,EADuB,CAAzC,CAGA,OAAOA,EALsB,CAmB/BvB,EAAArH,IAAA,CAAmB8I,QAAQ,CAACxB,CAAD,CAAU,CACnC,MAAOW,EAAA,CAAOX,CAAP,CAD4B,CAKrC,OAAOD,EAxQc,CAFQ,CAwTjCpQ,QAASA,GAAsB,EAAG,CAChC,IAAAwI,KAAA,CAAY,CAAC,eAAD,CAAkB,QAAQ,CAACsJ,CAAD,CAAgB,CACpD,MAAOA,EAAA,CAAc,WAAd,CAD6C,CAA1C,CADoB,CA8flC1V,QAASA,GAAgB,CAAC5D,CAAD,CAAWuZ,CAAX,CAAkC,CAAA,IACrDC,EAAgB,EADqC,CAErDC,EAAS,WAF4C,CAGrDC,EAA2B,wCAH0B,CAIrDC,EAAyB,gCAJ4B,CASrDC,EAA4B,yBAiB/B,KAAA/V,UAAA,CAAiBgW,QAASC,EAAiB,CAACxa,CAAD,CAAOya,CAAP,CAAyB,CACnErY,EAAA,CAAwBpC,CAAxB,CAA8B,WAA9B,CACI/I,EAAA,CAAS+I,CAAT,CAAJ;CACE8B,EAAA,CAAU2Y,CAAV,CAA4B,kBAA5B,CA2BA,CA1BKP,CAAA1iB,eAAA,CAA6BwI,CAA7B,CA0BL,GAzBEka,CAAA,CAAcla,CAAd,CACA,CADsB,EACtB,CAAAU,CAAAwC,QAAA,CAAiBlD,CAAjB,CAAwBma,CAAxB,CAAgC,CAAC,WAAD,CAAc,mBAAd,CAC9B,QAAQ,CAAC7H,CAAD,CAAYoI,CAAZ,CAA+B,CACrC,IAAIC,EAAa,EACjBxjB,EAAA,CAAQ+iB,CAAA,CAAcla,CAAd,CAAR,CAA6B,QAAQ,CAACya,CAAD,CAAmBriB,CAAnB,CAA0B,CAC7D,GAAI,CACF,IAAImM,EAAY+N,CAAA1R,OAAA,CAAiB6Z,CAAjB,CACZljB,EAAA,CAAWgN,CAAX,CAAJ,CACEA,CADF,CACc,SAAW5K,EAAA,CAAQ4K,CAAR,CAAX,CADd,CAEYzD,CAAAyD,CAAAzD,QAFZ,EAEiCyD,CAAAuU,KAFjC,GAGEvU,CAAAzD,QAHF,CAGsBnH,EAAA,CAAQ4K,CAAAuU,KAAR,CAHtB,CAKAvU,EAAAqW,SAAA,CAAqBrW,CAAAqW,SAArB,EAA2C,CAC3CrW,EAAAnM,MAAA,CAAkBA,CAClBmM,EAAAvE,KAAA,CAAiBuE,CAAAvE,KAAjB,EAAmCA,CACnCuE,EAAAsW,QAAA,CAAoBtW,CAAAsW,QAApB,EAA0CtW,CAAAuW,WAA1C,EAAkEvW,CAAAvE,KAClEuE,EAAAwW,SAAA,CAAqBxW,CAAAwW,SAArB,EAA2C,GAC3CJ,EAAA/iB,KAAA,CAAgB2M,CAAhB,CAZE,CAaF,MAAOlG,CAAP,CAAU,CACVqc,CAAA,CAAkBrc,CAAlB,CADU,CAdiD,CAA/D,CAkBA,OAAOsc,EApB8B,CADT,CAAhC,CAwBF,EAAAT,CAAA,CAAcla,CAAd,CAAApI,KAAA,CAAyB6iB,CAAzB,CA5BF,EA8BEtjB,CAAA,CAAQ6I,CAAR,CAAchI,EAAA,CAAcwiB,CAAd,CAAd,CAEF,OAAO,KAlC4D,CA0DrE,KAAAQ,2BAAA,CAAkCC,QAAQ,CAACC,CAAD,CAAS,CACjD,MAAIrhB,EAAA,CAAUqhB,CAAV,CAAJ,EACEjB,CAAAe,2BAAA,CAAiDE,CAAjD,CACO;AAAA,IAFT,EAISjB,CAAAe,2BAAA,EALwC,CA8BnD,KAAAG,4BAAA,CAAmCC,QAAQ,CAACF,CAAD,CAAS,CAClD,MAAIrhB,EAAA,CAAUqhB,CAAV,CAAJ,EACEjB,CAAAkB,4BAAA,CAAkDD,CAAlD,CACO,CAAA,IAFT,EAISjB,CAAAkB,4BAAA,EALyC,CASpD,KAAAzK,KAAA,CAAY,CACF,WADE,CACW,cADX,CAC2B,mBAD3B,CACgD,OADhD,CACyD,gBADzD,CAC2E,QAD3E,CAEF,aAFE,CAEa,YAFb,CAE2B,WAF3B,CAEwC,MAFxC,CAEgD,UAFhD,CAE4D,eAF5D,CAGV,QAAQ,CAAC4B,CAAD,CAAc+I,CAAd,CAA8BX,CAA9B,CAAmDY,CAAnD,CAA4DC,CAA5D,CAA8EC,CAA9E,CACCC,CADD,CACgBnI,CADhB,CAC8B8E,CAD9B,CAC2CsD,CAD3C,CACmDC,CADnD,CAC+DC,CAD/D,CAC8E,CAqLtF9a,QAASA,EAAO,CAAC+a,CAAD,CAAgBC,CAAhB,CAA8BC,CAA9B,CAA2CC,CAA3C,CACIC,CADJ,CAC4B,CACpCJ,CAAN,WAA+B3d,EAA/B,GAGE2d,CAHF,CAGkB3d,CAAA,CAAO2d,CAAP,CAHlB,CAOA1kB,EAAA,CAAQ0kB,CAAR,CAAuB,QAAQ,CAACthB,CAAD,CAAOnC,CAAP,CAAa,CACrB,CAArB,EAAImC,CAAAvD,SAAJ,EAA0CuD,CAAA2hB,UAAAxd,MAAA,CAAqB,KAArB,CAA1C,GACEmd,CAAA,CAAczjB,CAAd,CADF,CACgC8F,CAAA,CAAO3D,CAAP,CAAAqQ,KAAA,CAAkB,eAAlB,CAAAtR,OAAA,EAAA,CAA4C,CAA5C,CADhC,CAD0C,CAA5C,CAKA;IAAI6iB,EACIC,CAAA,CAAaP,CAAb,CAA4BC,CAA5B,CAA0CD,CAA1C,CACaE,CADb,CAC0BC,CAD1B,CAC2CC,CAD3C,CAERI,GAAA,CAAaR,CAAb,CAA4B,UAA5B,CACA,OAAOS,SAAqB,CAACzb,CAAD,CAAQ0b,CAAR,CAAwBC,CAAxB,CAA8C,CACxE1a,EAAA,CAAUjB,CAAV,CAAiB,OAAjB,CAGA,KAAI4b,EAAYF,CACA,CAAZG,EAAAve,MAAA1G,KAAA,CAA2BokB,CAA3B,CAAY,CACZA,CAEJ1kB,EAAA,CAAQqlB,CAAR,CAA+B,QAAQ,CAACrK,CAAD,CAAWnS,CAAX,CAAiB,CACtDyc,CAAAxb,KAAA,CAAe,GAAf,CAAqBjB,CAArB,CAA4B,YAA5B,CAA0CmS,CAA1C,CADsD,CAAxD,CAKQpa,EAAAA,CAAI,CAAZ,KAAI,IAAW6V,EAAK6O,CAAA1lB,OAApB,CAAsCgB,CAAtC,CAAwC6V,CAAxC,CAA4C7V,CAAA,EAA5C,CAAiD,CAC/C,IACIf,EADOylB,CAAAliB,CAAUxC,CAAVwC,CACIvD,SACE,EAAjB,GAAIA,CAAJ,EAAiD,CAAjD,GAAoCA,CAApC,EACEylB,CAAAE,GAAA,CAAa5kB,CAAb,CAAAkJ,KAAA,CAAqB,QAArB,CAA+BJ,CAA/B,CAJ6C,CAQ7C0b,CAAJ,EAAoBA,CAAA,CAAeE,CAAf,CAA0B5b,CAA1B,CAChBsb,EAAJ,EAAqBA,CAAA,CAAgBtb,CAAhB,CAAuB4b,CAAvB,CAAkCA,CAAlC,CACrB,OAAOA,EAvBiE,CAjBhC,CA4C5CJ,QAASA,GAAY,CAACO,CAAD,CAAWxc,CAAX,CAAsB,CACzC,GAAI,CACFwc,CAAAC,SAAA,CAAkBzc,CAAlB,CADE,CAEF,MAAM/B,CAAN,CAAS,EAH8B,CAwB3C+d,QAASA,EAAY,CAACU,CAAD,CAAWhB,CAAX,CAAyBiB,CAAzB,CAAuChB,CAAvC,CAAoDC,CAApD,CACGC,CADH,CAC2B,CAoC9CE,QAASA,EAAe,CAACtb,CAAD,CAAQic,CAAR,CAAkBC,CAAlB,CAAgCC,CAAhC,CAAmD,CAAA,IACzDC,CADyD,CAC5C1iB,CAD4C,CACtC2iB,CADsC,CAC/BC,CAD+B,CACAplB,CADA,CACG6V,CADH,CACOiL,CAG5EuE,EAAAA,CAAiBN,CAAA/lB,OAArB,KACIsmB,EAAqBC,KAAJ,CAAUF,CAAV,CACrB,KAAKrlB,CAAL,CAAS,CAAT,CAAYA,CAAZ,CAAgBqlB,CAAhB,CAAgCrlB,CAAA,EAAhC,CACEslB,CAAA,CAAetlB,CAAf,CAAA,CAAoB+kB,CAAA,CAAS/kB,CAAT,CAGX8gB,EAAP,CAAA9gB,CAAA,CAAI,CAAR,KAAkB6V,CAAlB,CAAuB2P,CAAAxmB,OAAvB,CAAuCgB,CAAvC,CAA2C6V,CAA3C,CAA+CiL,CAAA,EAA/C,CACEte,CAKA,CALO8iB,CAAA,CAAexE,CAAf,CAKP,CAJA2E,CAIA,CAJaD,CAAA,CAAQxlB,CAAA,EAAR,CAIb,CAHAklB,CAGA,CAHcM,CAAA,CAAQxlB,CAAA,EAAR,CAGd,CAFAmlB,CAEA,CAFQhf,CAAA,CAAO3D,CAAP,CAER,CAAIijB,CAAJ,EACMA,CAAA3c,MAAJ;CACEsc,CACA,CADatc,CAAA4c,KAAA,EACb,CAAAP,CAAAjc,KAAA,CAAW,QAAX,CAAqBkc,CAArB,CAFF,EAIEA,CAJF,CAIetc,CAGf,CAAA,CADA6c,CACA,CADoBF,CAAAG,WACpB,GAA2BX,CAAAA,CAA3B,EAAgDlB,CAAhD,CACE0B,CAAA,CAAWP,CAAX,CAAwBE,CAAxB,CAAoC5iB,CAApC,CAA0CwiB,CAA1C,CACEa,CAAA,CAAwB/c,CAAxB,CAA+B6c,CAA/B,EAAoD5B,CAApD,CADF,CADF,CAKE0B,CAAA,CAAWP,CAAX,CAAwBE,CAAxB,CAAoC5iB,CAApC,CAA0CwiB,CAA1C,CAAwDC,CAAxD,CAbJ,EAeWC,CAfX,EAgBEA,CAAA,CAAYpc,CAAZ,CAAmBtG,CAAA+Q,WAAnB,CAAoC5U,CAApC,CAA+CsmB,CAA/C,CAhCqE,CAhC3E,IAJ8C,IAC1CO,EAAU,EADgC,CAE1CM,CAF0C,CAEnClD,CAFmC,CAEXrP,CAFW,CAEcwS,CAFd,CAIrC/lB,EAAI,CAAb,CAAgBA,CAAhB,CAAoB+kB,CAAA/lB,OAApB,CAAqCgB,CAAA,EAArC,CACE8lB,CAyBA,CAzBQ,IAAIE,EAyBZ,CAtBApD,CAsBA,CAtBaqD,EAAA,CAAkBlB,CAAA,CAAS/kB,CAAT,CAAlB,CAA+B,EAA/B,CAAmC8lB,CAAnC,CAAgD,CAAN,GAAA9lB,CAAA,CAAUgkB,CAAV,CAAwBrlB,CAAlE,CACmBslB,CADnB,CAsBb,EAnBAwB,CAmBA,CAnBc7C,CAAA5jB,OACD,CAAPknB,EAAA,CAAsBtD,CAAtB,CAAkCmC,CAAA,CAAS/kB,CAAT,CAAlC,CAA+C8lB,CAA/C,CAAsD/B,CAAtD,CAAoEiB,CAApE,CACwB,IADxB,CAC8B,EAD9B,CACkC,EADlC,CACsCd,CADtC,CAAO,CAEP,IAgBN,GAdkBuB,CAAA3c,MAclB,EAbEwb,EAAA,CAAane,CAAA,CAAO4e,CAAA,CAAS/kB,CAAT,CAAP,CAAb,CAAkC,UAAlC,CAaF,CAVAklB,CAUA,CAVeO,CAGD,EAHeA,CAAAU,SAGf,EAFA,EAAE5S,CAAF,CAAewR,CAAA,CAAS/kB,CAAT,CAAAuT,WAAf,CAEA,EADA,CAACA,CAAAvU,OACD,CAAR,IAAQ,CACRqlB,CAAA,CAAa9Q,CAAb,CACGkS,CAAA,CAAaA,CAAAG,WAAb,CAAqC7B,CADxC,CAMN,CAHAyB,CAAA3lB,KAAA,CAAa4lB,CAAb,CAAyBP,CAAzB,CAGA,CAFAa,CAEA,CAFcA,CAEd,EAF6BN,CAE7B,EAF2CP,CAE3C,CAAAhB,CAAA,CAAyB,IAI3B,OAAO6B,EAAA,CAAc3B,CAAd,CAAgC,IAlCO,CA0EhDyB,QAASA,EAAuB,CAAC/c,CAAD,CAAQib,CAAR,CAAsB,CACpD,MAAOkB,SAA0B,CAACmB,CAAD,CAAmBC,CAAnB,CAA4BC,CAA5B,CAAyC,CACxE,IAAIC,EAAe,CAAA,CAEdH,EAAL,GACEA,CAEA,CAFmBtd,CAAA4c,KAAA,EAEnB,CAAAa,CAAA,CADAH,CAAAI,cACA,CADiC,CAAA,CAFnC,CAMIpgB,EAAAA,CAAQ2d,CAAA,CAAaqC,CAAb,CAA+BC,CAA/B,CAAwCC,CAAxC,CACZ,IAAIC,CAAJ,CACEngB,CAAA0Y,GAAA,CAAS,UAAT;AAAqBja,EAAA,CAAKuhB,CAAL,CAAuBA,CAAAzR,SAAvB,CAArB,CAEF,OAAOvO,EAbiE,CADtB,CA4BtD6f,QAASA,GAAiB,CAACzjB,CAAD,CAAOogB,CAAP,CAAmBkD,CAAnB,CAA0B9B,CAA1B,CAAuCC,CAAvC,CAAwD,CAAA,IAE5EwC,EAAWX,CAAAY,MAFiE,CAG5E/f,CAGJ,QALenE,CAAAvD,SAKf,EACE,KAAK,CAAL,CAEE0nB,CAAA,CAAa/D,CAAb,CACIgE,EAAA,CAAmBC,EAAA,CAAUrkB,CAAV,CAAAsH,YAAA,EAAnB,CADJ,CACuD,GADvD,CAC4Dka,CAD5D,CACyEC,CADzE,CAFF,KAMWthB,CANX,CAMiBsF,CANjB,CAMuB6e,CAA0BC,EAAAA,CAASvkB,CAAA8F,WAAxD,KANF,IAOW+K,EAAI,CAPf,CAOkBC,EAAKyT,CAALzT,EAAeyT,CAAA/nB,OAD/B,CAC8CqU,CAD9C,CACkDC,CADlD,CACsDD,CAAA,EADtD,CAC2D,CACzD,IAAI2T,EAAgB,CAAA,CAApB,CACIC,EAAc,CAAA,CAElBtkB,EAAA,CAAOokB,CAAA,CAAO1T,CAAP,CACP,IAAI,CAAC+D,CAAL,EAAqB,CAArB,EAAaA,CAAb,EAA0BzU,CAAAukB,UAA1B,CAA0C,CACxCjf,CAAA,CAAOtF,CAAAsF,KAEPkf,EAAA,CAAaP,EAAA,CAAmB3e,CAAnB,CACTmf,EAAAhe,KAAA,CAAqB+d,CAArB,CAAJ,GACElf,CADF,CACSwB,EAAA,CAAW0d,CAAAE,OAAA,CAAkB,CAAlB,CAAX,CAAiC,GAAjC,CADT,CAIA,KAAIC,EAAiBH,CAAAvgB,QAAA,CAAmB,cAAnB,CAAmC,EAAnC,CACjBugB,EAAJ,GAAmBG,CAAnB,CAAoC,OAApC,GACEN,CAEA,CAFgB/e,CAEhB,CADAgf,CACA,CADchf,CAAAof,OAAA,CAAY,CAAZ,CAAepf,CAAAjJ,OAAf,CAA6B,CAA7B,CACd,CADgD,KAChD,CAAAiJ,CAAA,CAAOA,CAAAof,OAAA,CAAY,CAAZ,CAAepf,CAAAjJ,OAAf,CAA6B,CAA7B,CAHT,CAMA8nB,EAAA,CAAQF,EAAA,CAAmB3e,CAAA6B,YAAA,EAAnB,CACR2c,EAAA,CAASK,CAAT,CAAA,CAAkB7e,CAClB6d,EAAA,CAAMgB,CAAN,CAAA,CAAe3mB,CAAf,CAAuB+R,EAAA,CAAKvP,CAAAxC,MAAL,CACnB8V,GAAA,CAAmBzT,CAAnB,CAAyBskB,CAAzB,CAAJ,GACEhB,CAAA,CAAMgB,CAAN,CADF,CACiB,CAAA,CADjB,CAGAS,EAAA,CAA4B/kB,CAA5B,CAAkCogB,CAAlC,CAA8CziB,CAA9C,CAAqD2mB,CAArD,CACAH,EAAA,CAAa/D,CAAb,CAAyBkE,CAAzB,CAAgC,GAAhC,CAAqC9C,CAArC,CAAkDC,CAAlD,CAAmE+C,CAAnE,CACcC,CADd,CAtBwC,CALe,CAiC3D5e,CAAA;AAAY7F,CAAA6F,UACZ,IAAInJ,CAAA,CAASmJ,CAAT,CAAJ,EAAyC,EAAzC,GAA2BA,CAA3B,CACE,IAAA,CAAO1B,CAAP,CAAe2b,CAAAla,KAAA,CAA4BC,CAA5B,CAAf,CAAA,CACEye,CAIA,CAJQF,EAAA,CAAmBjgB,CAAA,CAAM,CAAN,CAAnB,CAIR,CAHIggB,CAAA,CAAa/D,CAAb,CAAyBkE,CAAzB,CAAgC,GAAhC,CAAqC9C,CAArC,CAAkDC,CAAlD,CAGJ,GAFE6B,CAAA,CAAMgB,CAAN,CAEF,CAFiB5U,EAAA,CAAKvL,CAAA,CAAM,CAAN,CAAL,CAEjB,EAAA0B,CAAA,CAAYA,CAAAgf,OAAA,CAAiB1gB,CAAAtG,MAAjB,CAA+BsG,CAAA,CAAM,CAAN,CAAA3H,OAA/B,CAGhB,MACF,MAAK,CAAL,CACEwoB,CAAA,CAA4B5E,CAA5B,CAAwCpgB,CAAA2hB,UAAxC,CACA,MACF,MAAK,CAAL,CACE,GAAI,CAEF,GADAxd,CACA,CADQ0b,CAAAja,KAAA,CAA8B5F,CAAA2hB,UAA9B,CACR,CACE2C,CACA,CADQF,EAAA,CAAmBjgB,CAAA,CAAM,CAAN,CAAnB,CACR,CAAIggB,CAAA,CAAa/D,CAAb,CAAyBkE,CAAzB,CAAgC,GAAhC,CAAqC9C,CAArC,CAAkDC,CAAlD,CAAJ,GACE6B,CAAA,CAAMgB,CAAN,CADF,CACiB5U,EAAA,CAAKvL,CAAA,CAAM,CAAN,CAAL,CADjB,CAJA,CAQF,MAAOL,CAAP,CAAU,EAhEhB,CAwEAsc,CAAA9iB,KAAA,CAAgB2nB,CAAhB,CACA,OAAO7E,EA/EyE,CA0FlF8E,QAASA,EAAS,CAACllB,CAAD,CAAOmlB,CAAP,CAAkBC,CAAlB,CAA2B,CAC3C,IAAIhd,EAAQ,EAAZ,CACIid,EAAQ,CACZ,IAAIF,CAAJ,EAAiBnlB,CAAAslB,aAAjB,EAAsCtlB,CAAAslB,aAAA,CAAkBH,CAAlB,CAAtC,EAEE,EAAG,CACD,GAAI,CAACnlB,CAAL,CACE,KAAMulB,GAAA,CAAe,SAAf,CAEIJ,CAFJ,CAEeC,CAFf,CAAN,CAImB,CAArB,EAAIplB,CAAAvD,SAAJ,GACMuD,CAAAslB,aAAA,CAAkBH,CAAlB,CACJ,EADkCE,CAAA,EAClC,CAAIrlB,CAAAslB,aAAA,CAAkBF,CAAlB,CAAJ,EAAgCC,CAAA,EAFlC,CAIAjd,EAAA/K,KAAA,CAAW2C,CAAX,CACAA,EAAA,CAAOA,CAAAuI,YAXN,CAAH,MAYiB,CAZjB,CAYS8c,CAZT,CAFF,KAgBEjd,EAAA/K,KAAA,CAAW2C,CAAX,CAGF,OAAO2D,EAAA,CAAOyE,CAAP,CAtBoC,CAiC7Cod,QAASA,EAA0B,CAACC,CAAD;AAASN,CAAT,CAAoBC,CAApB,CAA6B,CAC9D,MAAO,SAAQ,CAAC9e,CAAD,CAAQ5C,CAAR,CAAiB4f,CAAjB,CAAwBQ,CAAxB,CAAqCvC,CAArC,CAAmD,CAChE7d,CAAA,CAAUwhB,CAAA,CAAUxhB,CAAA,CAAQ,CAAR,CAAV,CAAsByhB,CAAtB,CAAiCC,CAAjC,CACV,OAAOK,EAAA,CAAOnf,CAAP,CAAc5C,CAAd,CAAuB4f,CAAvB,CAA8BQ,CAA9B,CAA2CvC,CAA3C,CAFyD,CADJ,CA8BhEmC,QAASA,GAAqB,CAACtD,CAAD,CAAasF,CAAb,CAA0BC,CAA1B,CAAyCpE,CAAzC,CACCqE,CADD,CACeC,CADf,CACyCC,CADzC,CACqDC,CADrD,CAECrE,CAFD,CAEyB,CAiMrDsE,QAASA,EAAU,CAACC,CAAD,CAAMC,CAAN,CAAYf,CAAZ,CAAuBC,CAAvB,CAAgC,CACjD,GAAIa,CAAJ,CAAS,CACHd,CAAJ,GAAec,CAAf,CAAqBT,CAAA,CAA2BS,CAA3B,CAAgCd,CAAhC,CAA2CC,CAA3C,CAArB,CACAa,EAAA3F,QAAA,CAActW,CAAAsW,QACd2F,EAAAE,cAAA,CAAoBA,CACpB,IAAIC,CAAJ,GAAiCpc,CAAjC,EAA8CA,CAAAqc,eAA9C,CACEJ,CAAA,CAAMK,EAAA,CAAmBL,CAAnB,CAAwB,cAAe,CAAA,CAAf,CAAxB,CAERH,EAAAzoB,KAAA,CAAgB4oB,CAAhB,CAPO,CAST,GAAIC,CAAJ,CAAU,CACJf,CAAJ,GAAee,CAAf,CAAsBV,CAAA,CAA2BU,CAA3B,CAAiCf,CAAjC,CAA4CC,CAA5C,CAAtB,CACAc,EAAA5F,QAAA,CAAetW,CAAAsW,QACf4F,EAAAC,cAAA,CAAqBA,CACrB,IAAIC,CAAJ,GAAiCpc,CAAjC,EAA8CA,CAAAqc,eAA9C,CACEH,CAAA,CAAOI,EAAA,CAAmBJ,CAAnB,CAAyB,cAAe,CAAA,CAAf,CAAzB,CAETH,EAAA1oB,KAAA,CAAiB6oB,CAAjB,CAPQ,CAVuC,CAsBnDK,QAASA,EAAc,CAACJ,CAAD,CAAgB7F,CAAhB,CAAyB+B,CAAzB,CAAmCmE,CAAnC,CAAuD,CAAA,IACxE7oB,CADwE,CACjE8oB,EAAkB,MAD+C,CACvCC,EAAW,CAAA,CAChD,IAAIhqB,CAAA,CAAS4jB,CAAT,CAAJ,CAAuB,CACrB,IAAA,CAAqC,GAArC,GAAO3iB,CAAP,CAAe2iB,CAAA5e,OAAA,CAAe,CAAf,CAAf,GAAqD,GAArD,EAA4C/D,CAA5C,CAAA,CACE2iB,CAIA,CAJUA,CAAAuE,OAAA,CAAe,CAAf,CAIV,CAHa,GAGb,EAHIlnB,CAGJ,GAFE8oB,CAEF,CAFoB,eAEpB,EAAAC,CAAA,CAAWA,CAAX,EAAgC,GAAhC;AAAuB/oB,CAEzBA,EAAA,CAAQ,IAEJ6oB,EAAJ,EAA8C,MAA9C,GAA0BC,CAA1B,GACE9oB,CADF,CACU6oB,CAAA,CAAmBlG,CAAnB,CADV,CAGA3iB,EAAA,CAAQA,CAAR,EAAiB0kB,CAAA,CAASoE,CAAT,CAAA,CAA0B,GAA1B,CAAgCnG,CAAhC,CAA0C,YAA1C,CAEjB,IAAI,CAAC3iB,CAAL,EAAc,CAAC+oB,CAAf,CACE,KAAMnB,GAAA,CAAe,OAAf,CAEFjF,CAFE,CAEO6F,CAFP,CAAN,CAhBmB,CAAvB,IAqBWxpB,EAAA,CAAQ2jB,CAAR,CAAJ,GACL3iB,CACA,CADQ,EACR,CAAAf,CAAA,CAAQ0jB,CAAR,CAAiB,QAAQ,CAACA,CAAD,CAAU,CACjC3iB,CAAAN,KAAA,CAAWkpB,CAAA,CAAeJ,CAAf,CAA8B7F,CAA9B,CAAuC+B,CAAvC,CAAiDmE,CAAjD,CAAX,CADiC,CAAnC,CAFK,CAMP,OAAO7oB,EA7BqE,CAiC9EslB,QAASA,EAAU,CAACP,CAAD,CAAcpc,CAAd,CAAqBqgB,CAArB,CAA+BnE,CAA/B,CAA6CC,CAA7C,CAAgE,CAoKjFmE,QAASA,EAA0B,CAACtgB,CAAD,CAAQugB,CAAR,CAAuB,CACxD,IAAI5E,CAGmB,EAAvB,CAAIvjB,SAAAlC,OAAJ,GACEqqB,CACA,CADgBvgB,CAChB,CAAAA,CAAA,CAAQnK,CAFV,CAKI2qB,EAAJ,GACE7E,CADF,CAC0BuE,EAD1B,CAIA,OAAO/D,EAAA,CAAkBnc,CAAlB,CAAyBugB,CAAzB,CAAwC5E,CAAxC,CAbiD,CApKuB,IAC7EqB,CAD6E,CACtEjB,CADsE,CACzDhP,CADyD,CACrDoS,CADqD,CAC7ClF,CAD6C,CACjCwG,CADiC,CACnBP,GAAqB,EADF,CACMjF,EAGrF+B,EAAA,CADEoC,CAAJ,GAAoBiB,CAApB,CACUhB,CADV,CAGUnkB,EAAA,CAAYmkB,CAAZ,CAA2B,IAAInC,EAAJ,CAAe7f,CAAA,CAAOgjB,CAAP,CAAf,CAAiChB,CAAAzB,MAAjC,CAA3B,CAEV7B,EAAA,CAAWiB,CAAA0D,UAEX,IAAIZ,CAAJ,CAA8B,CAC5B,IAAIa,EAAe,8BACf/E,EAAAA,CAAYve,CAAA,CAAOgjB,CAAP,CAEhBI,EAAA,CAAezgB,CAAA4c,KAAA,CAAW,CAAA,CAAX,CAEXgE,EAAAA,EAAJ,EAA0BA,EAA1B,GAAgDd,CAAhD,EACIc,EADJ,GAC0Bd,CAAAe,oBAD1B,CAIEjF,CAAAxb,KAAA,CAAe,yBAAf,CAA0CqgB,CAA1C,CAJF,CAEE7E,CAAAxb,KAAA,CAAe,eAAf,CAAgCqgB,CAAhC,CAOFjF;EAAA,CAAaI,CAAb,CAAwB,kBAAxB,CAEAtlB,EAAA,CAAQwpB,CAAA9f,MAAR,CAAwC,QAAQ,CAAC8gB,CAAD,CAAaC,CAAb,CAAwB,CAAA,IAClEljB,EAAQijB,CAAAjjB,MAAA,CAAiB8iB,CAAjB,CAAR9iB,EAA0C,EADwB,CAElEmjB,EAAWnjB,CAAA,CAAM,CAAN,CAAXmjB,EAAuBD,CAF2C,CAGlEX,EAAwB,GAAxBA,EAAYviB,CAAA,CAAM,CAAN,CAHsD,CAIlEojB,EAAOpjB,CAAA,CAAM,CAAN,CAJ2D,CAKlEqjB,CALkE,CAMlEC,CANkE,CAMvDC,CANuD,CAM5CC,CAE1BZ,EAAAa,kBAAA,CAA+BP,CAA/B,CAAA,CAA4CE,CAA5C,CAAmDD,CAEnD,QAAQC,CAAR,EAEE,KAAK,GAAL,CACEjE,CAAAuE,SAAA,CAAeP,CAAf,CAAyB,QAAQ,CAAC3pB,CAAD,CAAQ,CACvCopB,CAAA,CAAaM,CAAb,CAAA,CAA0B1pB,CADa,CAAzC,CAGA2lB,EAAAwE,YAAA,CAAkBR,CAAlB,CAAAS,QAAA,CAAsCzhB,CAClCgd,EAAA,CAAMgE,CAAN,CAAJ,GAGEP,CAAA,CAAaM,CAAb,CAHF,CAG4BvG,CAAA,CAAawC,CAAA,CAAMgE,CAAN,CAAb,CAAA,CAA8BhhB,CAA9B,CAH5B,CAKA,MAEF,MAAK,GAAL,CACE,GAAIogB,CAAJ,EAAgB,CAACpD,CAAA,CAAMgE,CAAN,CAAjB,CACE,KAEFG,EAAA,CAAYxG,CAAA,CAAOqC,CAAA,CAAMgE,CAAN,CAAP,CAEVK,EAAA,CADEF,CAAAO,QAAJ,CACYrmB,EADZ,CAGYgmB,QAAQ,CAACM,CAAD,CAAGC,CAAH,CAAM,CAAE,MAAOD,EAAP,GAAaC,CAAf,CAE1BR,EAAA,CAAYD,CAAAU,OAAZ,EAAgC,QAAQ,EAAG,CAEzCX,CAAA,CAAYT,CAAA,CAAaM,CAAb,CAAZ,CAAsCI,CAAA,CAAUnhB,CAAV,CACtC,MAAMif,GAAA,CAAe,WAAf,CAEFjC,CAAA,CAAMgE,CAAN,CAFE,CAEelB,CAAA3gB,KAFf,CAAN,CAHyC,CAO3C+hB,EAAA,CAAYT,CAAA,CAAaM,CAAb,CAAZ,CAAsCI,CAAA,CAAUnhB,CAAV,CACtCygB,EAAA7lB,OAAA,CAAoBknB,QAAyB,EAAG,CAC9C,IAAIC,EAAcZ,CAAA,CAAUnhB,CAAV,CACbqhB,EAAA,CAAQU,CAAR,CAAqBtB,CAAA,CAAaM,CAAb,CAArB,CAAL,GAEOM,CAAA,CAAQU,CAAR,CAAqBb,CAArB,CAAL,CAKEE,CAAA,CAAUphB,CAAV,CAAiB+hB,CAAjB,CAA+BtB,CAAA,CAAaM,CAAb,CAA/B,CALF,CAEEN,CAAA,CAAaM,CAAb,CAFF,CAE4BgB,CAJ9B,CAUA,OAAOb,EAAP,CAAmBa,CAZ2B,CAAhD,CAaG,IAbH,CAaSZ,CAAAO,QAbT,CAcA;KAEF,MAAK,GAAL,CACEP,CAAA,CAAYxG,CAAA,CAAOqC,CAAA,CAAMgE,CAAN,CAAP,CACZP,EAAA,CAAaM,CAAb,CAAA,CAA0B,QAAQ,CAAC/P,CAAD,CAAS,CACzC,MAAOmQ,EAAA,CAAUnhB,CAAV,CAAiBgR,CAAjB,CADkC,CAG3C,MAEF,SACE,KAAMiO,GAAA,CAAe,MAAf,CAGFa,CAAA3gB,KAHE,CAG6B4hB,CAH7B,CAGwCD,CAHxC,CAAN,CAxDJ,CAVsE,CAAxE,CAjB4B,CA0F9B7F,EAAA,CAAekB,CAAf,EAAoCmE,CAChC0B,EAAJ,EACE1rB,CAAA,CAAQ0rB,CAAR,CAA8B,QAAQ,CAACte,CAAD,CAAY,CAAA,IAC5CsN,EAAS,QACHtN,CAAA,GAAcoc,CAAd,EAA0Cpc,CAAAqc,eAA1C,CAAqEU,CAArE,CAAoFzgB,CADjF,UAED+b,CAFC,QAGHiB,CAHG,aAIE/B,EAJF,CADmC,CAM7CgH,CAEHhI,EAAA,CAAavW,CAAAuW,WACK,IAAlB,EAAIA,CAAJ,GACEA,CADF,CACe+C,CAAA,CAAMtZ,CAAAvE,KAAN,CADf,CAIA8iB,EAAA,CAAqBrH,CAAA,CAAYX,CAAZ,CAAwBjJ,CAAxB,CAMrBkP,GAAA,CAAmBxc,CAAAvE,KAAnB,CAAA,CAAqC8iB,CAChCzB,EAAL,EACEzE,CAAA3b,KAAA,CAAc,GAAd,CAAoBsD,CAAAvE,KAApB,CAAqC,YAArC,CAAmD8iB,CAAnD,CAGEve,EAAAwe,aAAJ,GACElR,CAAAmR,OAAA,CAAcze,CAAAwe,aAAd,CADF,CAC0CD,CAD1C,CAxBgD,CAAlD,CA+BE/qB,EAAA,CAAI,CAAR,KAAW6V,CAAX,CAAgByS,CAAAtpB,OAAhB,CAAmCgB,CAAnC,CAAuC6V,CAAvC,CAA2C7V,CAAA,EAA3C,CACE,GAAI,CACFioB,CACA,CADSK,CAAA,CAAWtoB,CAAX,CACT,CAAAioB,CAAA,CAAOA,CAAAsB,aAAA,CAAsBA,CAAtB,CAAqCzgB,CAA5C,CAAmD+b,CAAnD,CAA6DiB,CAA7D,CACImC,CAAAnF,QADJ,EACsBiG,CAAA,CAAed,CAAAU,cAAf,CAAqCV,CAAAnF,QAArC,CAAqD+B,CAArD,CAA+DmE,EAA/D,CADtB,CAC0GjF,EAD1G,CAFE,CAIF,MAAOzd,CAAP,CAAU,CACVqc,CAAA,CAAkBrc,CAAlB,CAAqBL,EAAA,CAAY4e,CAAZ,CAArB,CADU,CAQVqG,CAAAA,CAAepiB,CACf8f,EAAJ,GAAiCA,CAAAuC,SAAjC;AAA+G,IAA/G,GAAsEvC,CAAAwC,YAAtE,IACEF,CADF,CACiB3B,CADjB,CAGArE,EAAA,EAAeA,CAAA,CAAYgG,CAAZ,CAA0B/B,CAAA5V,WAA1B,CAA+C5U,CAA/C,CAA0DsmB,CAA1D,CAGf,KAAIjlB,CAAJ,CAAQuoB,CAAAvpB,OAAR,CAA6B,CAA7B,CAAqC,CAArC,EAAgCgB,CAAhC,CAAwCA,CAAA,EAAxC,CACE,GAAI,CACFioB,CACA,CADSM,CAAA,CAAYvoB,CAAZ,CACT,CAAAioB,CAAA,CAAOA,CAAAsB,aAAA,CAAsBA,CAAtB,CAAqCzgB,CAA5C,CAAmD+b,CAAnD,CAA6DiB,CAA7D,CACImC,CAAAnF,QADJ,EACsBiG,CAAA,CAAed,CAAAU,cAAf,CAAqCV,CAAAnF,QAArC,CAAqD+B,CAArD,CAA+DmE,EAA/D,CADtB,CAC0GjF,EAD1G,CAFE,CAIF,MAAOzd,CAAP,CAAU,CACVqc,CAAA,CAAkBrc,CAAlB,CAAqBL,EAAA,CAAY4e,CAAZ,CAArB,CADU,CA9JmE,CAvPnFX,CAAA,CAAyBA,CAAzB,EAAmD,EAoBnD,KArBqD,IAGjDmH,EAAmB,CAAC9J,MAAAC,UAH6B,CAIjD8J,CAJiD,CAKjDR,EAAuB5G,CAAA4G,qBAL0B,CAMjDlC,EAA2B1E,CAAA0E,yBANsB,CAOjDc,GAAoBxF,CAAAwF,kBAP6B,CAQjD6B,EAA4BrH,CAAAqH,0BARqB,CASjDC,EAAyB,CAAA,CATwB,CAUjDlC,EAAgCpF,CAAAoF,8BAViB,CAWjDmC,EAAetD,CAAAqB,UAAfiC,CAAyCtlB,CAAA,CAAO+hB,CAAP,CAXQ,CAYjD1b,CAZiD,CAajDmc,CAbiD,CAcjD+C,CAdiD,CAgBjD/F,GAAoB5B,CAhB6B,CAiBjDkE,CAjBiD,CAqB7CjoB,EAAI,CArByC,CAqBtC6V,EAAK+M,CAAA5jB,OAApB,CAAuCgB,CAAvC,CAA2C6V,CAA3C,CAA+C7V,CAAA,EAA/C,CAAoD,CAClDwM,CAAA,CAAYoW,CAAA,CAAW5iB,CAAX,CACZ,KAAI2nB,GAAYnb,CAAAmf,QAAhB,CACI/D,EAAUpb,CAAAof,MAGVjE,GAAJ,GACE8D,CADF,CACiB/D,CAAA,CAAUQ,CAAV,CAAuBP,EAAvB,CAAkCC,CAAlC,CADjB,CAGA8D,EAAA,CAAY/sB,CAEZ,IAAI0sB,CAAJ,CAAuB7e,CAAAqW,SAAvB,CACE,KAGF;GAAIgJ,CAAJ,CAAqBrf,CAAA1D,MAArB,CACEwiB,CAIA,CAJoBA,CAIpB,EAJyC9e,CAIzC,CAAKA,CAAA4e,YAAL,GACEU,CAAA,CAAkB,oBAAlB,CAAwClD,CAAxC,CAAkEpc,CAAlE,CACkBif,CADlB,CAEA,CAAI1pB,CAAA,CAAS8pB,CAAT,CAAJ,GACEjD,CADF,CAC6Bpc,CAD7B,CAHF,CASFmc,EAAA,CAAgBnc,CAAAvE,KAEXmjB,EAAA5e,CAAA4e,YAAL,EAA8B5e,CAAAuW,WAA9B,GACE8I,CAIA,CAJiBrf,CAAAuW,WAIjB,CAHA+H,CAGA,CAHuBA,CAGvB,EAH+C,EAG/C,CAFAgB,CAAA,CAAkB,GAAlB,CAAwBnD,CAAxB,CAAwC,cAAxC,CACImC,CAAA,CAAqBnC,CAArB,CADJ,CACyCnc,CADzC,CACoDif,CADpD,CAEA,CAAAX,CAAA,CAAqBnC,CAArB,CAAA,CAAsCnc,CALxC,CAQA,IAAIqf,CAAJ,CAAqBrf,CAAAoZ,WAArB,CACE4F,CAUA,CAVyB,CAAA,CAUzB,CALKhf,CAAAuf,MAKL,GAJED,CAAA,CAAkB,cAAlB,CAAkCP,CAAlC,CAA6D/e,CAA7D,CAAwEif,CAAxE,CACA,CAAAF,CAAA,CAA4B/e,CAG9B,EAAsB,SAAtB,EAAIqf,CAAJ,EACEvC,CASA,CATgC,CAAA,CAShC,CARA+B,CAQA,CARmB7e,CAAAqW,SAQnB,CAPA6I,CAOA,CAPYhE,CAAA,CAAUQ,CAAV,CAAuBP,EAAvB,CAAkCC,CAAlC,CAOZ,CANA6D,CAMA,CANetD,CAAAqB,UAMf,CALIrjB,CAAA,CAAOzH,CAAAstB,cAAA,CAAuB,GAAvB,CAA6BrD,CAA7B,CAA6C,IAA7C,CACuBR,CAAA,CAAcQ,CAAd,CADvB,CACsD,GADtD,CAAP,CAKJ,CAHAT,CAGA,CAHcuD,CAAA,CAAa,CAAb,CAGd,CAFAQ,EAAA,CAAY7D,CAAZ,CAA0BjiB,CAAA,CAxpK7BlB,EAAAvF,KAAA,CAwpK8CgsB,CAxpK9C,CAA+B,CAA/B,CAwpK6B,CAA1B,CAAwDxD,CAAxD,CAEA,CAAAvC,EAAA,CAAoB5c,CAAA,CAAQ2iB,CAAR,CAAmB3H,CAAnB,CAAiCsH,CAAjC,CACQa,CADR,EAC4BA,CAAAjkB,KAD5B,CACmD,2BAQdsjB,CARc,CADnD,CAVtB,GAsBEG,CAEA,CAFYvlB,CAAA,CAAOwN,EAAA,CAAYuU,CAAZ,CAAP,CAAAiE,SAAA,EAEZ,CADAV,CAAAplB,MAAA,EACA,CAAAsf,EAAA,CAAoB5c,CAAA,CAAQ2iB,CAAR,CAAmB3H,CAAnB,CAxBtB,CA4BF,IAAIvX,CAAA2e,SAAJ,CAUE,GATAW,CAAA,CAAkB,UAAlB;AAA8BpC,EAA9B,CAAiDld,CAAjD,CAA4Dif,CAA5D,CASI7kB,CARJ8iB,EAQI9iB,CARgB4F,CAQhB5F,CANJilB,CAMIjlB,CANcpH,CAAA,CAAWgN,CAAA2e,SAAX,CACD,CAAX3e,CAAA2e,SAAA,CAAmBM,CAAnB,CAAiCtD,CAAjC,CAAW,CACX3b,CAAA2e,SAIFvkB,CAFJilB,CAEIjlB,CAFawlB,CAAA,CAAoBP,CAApB,CAEbjlB,CAAA4F,CAAA5F,QAAJ,CAAuB,CACrBslB,CAAA,CAAmB1f,CAEjBkf,EAAA,CAp8HJlZ,EAAApJ,KAAA,CAm8HuByiB,CAn8HvB,CAm8HE,CAGc1lB,CAAA,CAAO+L,EAAA,CAAK2Z,CAAL,CAAP,CAHd,CACc,EAId3D,EAAA,CAAcwD,CAAA,CAAU,CAAV,CAEd,IAAwB,CAAxB,EAAIA,CAAA1sB,OAAJ,EAAsD,CAAtD,GAA6BkpB,CAAAjpB,SAA7B,CACE,KAAM8oB,GAAA,CAAe,OAAf,CAEFY,CAFE,CAEa,EAFb,CAAN,CAKFsD,EAAA,CAAY7D,CAAZ,CAA0BqD,CAA1B,CAAwCvD,CAAxC,CAEImE,EAAAA,CAAmB,OAAQ,EAAR,CAOnBC,EAAAA,CAAqBrG,EAAA,CAAkBiC,CAAlB,CAA+B,EAA/B,CAAmCmE,CAAnC,CACzB,KAAIE,EAAwB3J,CAAAzf,OAAA,CAAkBnD,CAAlB,CAAsB,CAAtB,CAAyB4iB,CAAA5jB,OAAzB,EAA8CgB,CAA9C,CAAkD,CAAlD,EAExB4oB,EAAJ,EACE4D,EAAA,CAAwBF,CAAxB,CAEF1J,EAAA,CAAaA,CAAAxd,OAAA,CAAkBknB,CAAlB,CAAAlnB,OAAA,CAA6CmnB,CAA7C,CACbE,EAAA,CAAwBtE,CAAxB,CAAuCkE,CAAvC,CAEAxW,EAAA,CAAK+M,CAAA5jB,OAjCgB,CAAvB,IAmCEysB,EAAAhlB,KAAA,CAAkBolB,CAAlB,CAIJ,IAAIrf,CAAA4e,YAAJ,CACEU,CAAA,CAAkB,UAAlB,CAA8BpC,EAA9B,CAAiDld,CAAjD,CAA4Dif,CAA5D,CAcA,CAbA/B,EAaA,CAboBld,CAapB,CAXIA,CAAA5F,QAWJ,GAVEslB,CAUF,CAVqB1f,CAUrB,EAPAiZ,CAOA,CAPaiH,CAAA,CAAmB9J,CAAAzf,OAAA,CAAkBnD,CAAlB,CAAqB4iB,CAAA5jB,OAArB,CAAyCgB,CAAzC,CAAnB,CAAgEyrB,CAAhE,CACTtD,CADS,CACMC,CADN,CACoBzC,EADpB,CACuC2C,CADvC,CACmDC,CADnD,CACgE,sBACjDuC,CADiD,0BAE7ClC,CAF6C,mBAGpDc,EAHoD,2BAI5C6B,CAJ4C,CADhE,CAOb;AAAA1V,CAAA,CAAK+M,CAAA5jB,OAfP,KAgBO,IAAIwN,CAAAzD,QAAJ,CACL,GAAI,CACFkf,CACA,CADSzb,CAAAzD,QAAA,CAAkB0iB,CAAlB,CAAgCtD,CAAhC,CAA+CxC,EAA/C,CACT,CAAInmB,CAAA,CAAWyoB,CAAX,CAAJ,CACEO,CAAA,CAAW,IAAX,CAAiBP,CAAjB,CAAyBN,EAAzB,CAAoCC,CAApC,CADF,CAEWK,CAFX,EAGEO,CAAA,CAAWP,CAAAQ,IAAX,CAAuBR,CAAAS,KAAvB,CAAoCf,EAApC,CAA+CC,CAA/C,CALA,CAOF,MAAOthB,EAAP,CAAU,CACVqc,CAAA,CAAkBrc,EAAlB,CAAqBL,EAAA,CAAYwlB,CAAZ,CAArB,CADU,CAKVjf,CAAA2Z,SAAJ,GACEV,CAAAU,SACA,CADsB,CAAA,CACtB,CAAAkF,CAAA,CAAmBsB,IAAAC,IAAA,CAASvB,CAAT,CAA2B7e,CAAAqW,SAA3B,CAFrB,CA5JkD,CAmKpD4C,CAAA3c,MAAA,CAAmBwiB,CAAnB,EAAoE,CAAA,CAApE,GAAwCA,CAAAxiB,MACxC2c,EAAAG,WAAA,CAAwB4F,CAAxB,EAAkD7F,EAClDzB,EAAAoF,8BAAA,CAAuDA,CAGvD,OAAO7D,EA7L8C,CA8avD+G,QAASA,GAAuB,CAAC5J,CAAD,CAAa,CAE3C,IAF2C,IAElCvP,EAAI,CAF8B,CAE3BC,EAAKsP,CAAA5jB,OAArB,CAAwCqU,CAAxC,CAA4CC,CAA5C,CAAgDD,CAAA,EAAhD,CACEuP,CAAA,CAAWvP,CAAX,CAAA,CAAgB/R,EAAA,CAAQshB,CAAA,CAAWvP,CAAX,CAAR,CAAuB,gBAAiB,CAAA,CAAjB,CAAvB,CAHyB,CAqB7CsT,QAASA,EAAY,CAACkG,CAAD,CAAc5kB,CAAd,CAAoB7F,CAApB,CAA8B4hB,CAA9B,CAA2CC,CAA3C,CAA4D6I,CAA5D,CACCC,CADD,CACc,CACjC,GAAI9kB,CAAJ,GAAagc,CAAb,CAA8B,MAAO,KACjCtd,EAAAA,CAAQ,IACZ,IAAIwb,CAAA1iB,eAAA,CAA6BwI,CAA7B,CAAJ,CAAwC,CAAA,IAC9BuE,CAAWoW,EAAAA,CAAarI,CAAArB,IAAA,CAAcjR,CAAd,CAAqBma,CAArB,CAAhC,KADsC,IAElCpiB,EAAI,CAF8B,CAE3B6V,EAAK+M,CAAA5jB,OADhB,CACmCgB,CADnC,CACqC6V,CADrC,CACyC7V,CAAA,EADzC,CAEE,GAAI,CACFwM,CACA,CADYoW,CAAA,CAAW5iB,CAAX,CACZ,EAAMgkB,CAAN,GAAsBrlB,CAAtB,EAAmCqlB,CAAnC,CAAiDxX,CAAAqW,SAAjD,GAC8C,EAD9C;AACKrW,CAAAwW,SAAAhgB,QAAA,CAA2BZ,CAA3B,CADL,GAEM0qB,CAIJ,GAHEtgB,CAGF,CAHclL,EAAA,CAAQkL,CAAR,CAAmB,SAAUsgB,CAAV,OAAgCC,CAAhC,CAAnB,CAGd,EADAF,CAAAhtB,KAAA,CAAiB2M,CAAjB,CACA,CAAA7F,CAAA,CAAQ6F,CANV,CAFE,CAUF,MAAMlG,CAAN,CAAS,CAAEqc,CAAA,CAAkBrc,CAAlB,CAAF,CAbyB,CAgBxC,MAAOK,EAnB0B,CA+BnC8lB,QAASA,EAAuB,CAACxrB,CAAD,CAAMgD,CAAN,CAAW,CAAA,IACrC+oB,EAAU/oB,CAAAyiB,MAD2B,CAErCuG,EAAUhsB,CAAAylB,MAF2B,CAGrC7B,EAAW5jB,CAAAuoB,UAGfpqB,EAAA,CAAQ6B,CAAR,CAAa,QAAQ,CAACd,CAAD,CAAQZ,CAAR,CAAa,CACX,GAArB,EAAIA,CAAA2E,OAAA,CAAW,CAAX,CAAJ,GACMD,CAAA,CAAI1E,CAAJ,CAGJ,EAHgB0E,CAAA,CAAI1E,CAAJ,CAGhB,GAH6BY,CAG7B,GAFEA,CAEF,GAFoB,OAAR,GAAAZ,CAAA,CAAkB,GAAlB,CAAwB,GAEpC,EAF2C0E,CAAA,CAAI1E,CAAJ,CAE3C,EAAA0B,CAAAisB,KAAA,CAAS3tB,CAAT,CAAcY,CAAd,CAAqB,CAAA,CAArB,CAA2B6sB,CAAA,CAAQztB,CAAR,CAA3B,CAJF,CADgC,CAAlC,CAUAH,EAAA,CAAQ6E,CAAR,CAAa,QAAQ,CAAC9D,CAAD,CAAQZ,CAAR,CAAa,CACrB,OAAX,EAAIA,CAAJ,EACE+kB,EAAA,CAAaO,CAAb,CAAuB1kB,CAAvB,CACA,CAAAc,CAAA,CAAI,OAAJ,CAAA,EAAgBA,CAAA,CAAI,OAAJ,CAAA,CAAeA,CAAA,CAAI,OAAJ,CAAf,CAA8B,GAA9B,CAAoC,EAApD,EAA0Dd,CAF5D,EAGkB,OAAX,EAAIZ,CAAJ,EACLslB,CAAAliB,KAAA,CAAc,OAAd,CAAuBkiB,CAAAliB,KAAA,CAAc,OAAd,CAAvB,CAAgD,GAAhD,CAAsDxC,CAAtD,CACA,CAAAc,CAAA,MAAA,EAAgBA,CAAA,MAAA,CAAeA,CAAA,MAAf,CAA8B,GAA9B,CAAoC,EAApD,EAA0Dd,CAFrD,EAMqB,GANrB,EAMIZ,CAAA2E,OAAA,CAAW,CAAX,CANJ,EAM6BjD,CAAAxB,eAAA,CAAmBF,CAAnB,CAN7B,GAOL0B,CAAA,CAAI1B,CAAJ,CACA,CADWY,CACX,CAAA8sB,CAAA,CAAQ1tB,CAAR,CAAA,CAAeytB,CAAA,CAAQztB,CAAR,CARV,CAJyB,CAAlC,CAhByC,CAkC3CmtB,QAASA,EAAkB,CAAC9J,CAAD,CAAa6I,CAAb;AAA2B0B,CAA3B,CACvBnI,CADuB,CACTW,CADS,CACU2C,CADV,CACsBC,CADtB,CACmCrE,CADnC,CAC2D,CAAA,IAChFkJ,EAAY,EADoE,CAEhFC,CAFgF,CAGhFC,CAHgF,CAIhFC,EAA4B9B,CAAA,CAAa,CAAb,CAJoD,CAKhF+B,EAAqB5K,CAAAjR,MAAA,EAL2D,CAOhF8b,EAAuBzsB,CAAA,CAAO,EAAP,CAAWwsB,CAAX,CAA+B,aACvC,IADuC,YACrB,IADqB,SACN,IADM,qBACqBA,CADrB,CAA/B,CAPyD,CAUhFpC,EAAe5rB,CAAA,CAAWguB,CAAApC,YAAX,CACD,CAARoC,CAAApC,YAAA,CAA+BK,CAA/B,CAA6C0B,CAA7C,CAAQ,CACRK,CAAApC,YAEVK,EAAAplB,MAAA,EAEAkd,EAAArK,IAAA,CAAUyK,CAAA+J,sBAAA,CAA2BtC,CAA3B,CAAV,CAAmD,OAAQ5H,CAAR,CAAnD,CAAAmK,QAAA,CACU,QAAQ,CAACC,CAAD,CAAU,CAAA,IACpB1F,CADoB,CACuB2F,CAE/CD,EAAA,CAAUxB,CAAA,CAAoBwB,CAApB,CAEV,IAAIJ,CAAA5mB,QAAJ,CAAgC,CAE5B8kB,CAAA,CAl3IJlZ,EAAApJ,KAAA,CAi3IuBwkB,CAj3IvB,CAi3IE,CAGcznB,CAAA,CAAO+L,EAAA,CAAK0b,CAAL,CAAP,CAHd,CACc,EAId1F,EAAA,CAAcwD,CAAA,CAAU,CAAV,CAEd,IAAwB,CAAxB,EAAIA,CAAA1sB,OAAJ,EAAsD,CAAtD,GAA6BkpB,CAAAjpB,SAA7B,CACE,KAAM8oB,GAAA,CAAe,OAAf,CAEFyF,CAAAvlB,KAFE,CAEuBmjB,CAFvB,CAAN,CAKF0C,CAAA,CAAoB,OAAQ,EAAR,CACpB7B,GAAA,CAAYjH,CAAZ,CAA0ByG,CAA1B,CAAwCvD,CAAxC,CACA,KAAIoE,EAAqBrG,EAAA,CAAkBiC,CAAlB,CAA+B,EAA/B,CAAmC4F,CAAnC,CAErB/rB,EAAA,CAASyrB,CAAA1kB,MAAT,CAAJ,EACE0jB,EAAA,CAAwBF,CAAxB,CAEF1J,EAAA,CAAa0J,CAAAlnB,OAAA,CAA0Bwd,CAA1B,CACb6J,EAAA,CAAwBU,CAAxB,CAAgCW,CAAhC,CAtB8B,CAAhC,IAwBE5F,EACA,CADcqF,CACd,CAAA9B,CAAAhlB,KAAA,CAAkBmnB,CAAlB,CAGFhL,EAAAhiB,QAAA,CAAmB6sB,CAAnB,CAEAJ,EAAA,CAA0BnH,EAAA,CAAsBtD,CAAtB,CAAkCsF,CAAlC,CAA+CiF,CAA/C,CACtBxH,CADsB,CACH8F,CADG,CACW+B,CADX,CAC+BlF,CAD/B,CAC2CC,CAD3C;AAEtBrE,CAFsB,CAG1B9kB,EAAA,CAAQ4lB,CAAR,CAAsB,QAAQ,CAACxiB,CAAD,CAAOxC,CAAP,CAAU,CAClCwC,CAAJ,EAAY0lB,CAAZ,GACElD,CAAA,CAAahlB,CAAb,CADF,CACoByrB,CAAA,CAAa,CAAb,CADpB,CADsC,CAAxC,CAQA,KAHA6B,CAGA,CAH2BjJ,CAAA,CAAaoH,CAAA,CAAa,CAAb,CAAAlY,WAAb,CAAyCoS,CAAzC,CAG3B,CAAMyH,CAAApuB,OAAN,CAAA,CAAwB,CAClB8J,CAAAA,CAAQskB,CAAAzb,MAAA,EACRoc,EAAAA,CAAyBX,CAAAzb,MAAA,EAFP,KAGlBqc,EAAkBZ,CAAAzb,MAAA,EAHA,CAIlBsT,EAAoBmI,CAAAzb,MAAA,EAJF,CAKlBwX,EAAWsC,CAAA,CAAa,CAAb,CAEf,IAAIsC,CAAJ,GAA+BR,CAA/B,CAA0D,CACxD,IAAIU,EAAaF,CAAA1lB,UAEX6b,EAAAoF,8BAAN,EACIkE,CAAA5mB,QADJ,GAGEuiB,CAHF,CAGaxV,EAAA,CAAYuU,CAAZ,CAHb,CAMA+D,GAAA,CAAY+B,CAAZ,CAA6B7nB,CAAA,CAAO4nB,CAAP,CAA7B,CAA6D5E,CAA7D,CAGA7E,GAAA,CAAane,CAAA,CAAOgjB,CAAP,CAAb,CAA+B8E,CAA/B,CAZwD,CAexDJ,CAAA,CADER,CAAAzH,WAAJ,CAC2BC,CAAA,CAAwB/c,CAAxB,CAA+BukB,CAAAzH,WAA/B,CAD3B,CAG2BX,CAE3BoI,EAAA,CAAwBC,CAAxB,CAAkDxkB,CAAlD,CAAyDqgB,CAAzD,CAAmEnE,CAAnE,CACE6I,CADF,CA1BsB,CA6BxBT,CAAA,CAAY,IA3EY,CAD5B,CAAAzQ,MAAA,CA8EQ,QAAQ,CAACuR,CAAD,CAAWC,CAAX,CAAiBC,CAAjB,CAA0BviB,CAA1B,CAAkC,CAC9C,KAAMkc,GAAA,CAAe,QAAf,CAAyDlc,CAAAyR,IAAzD,CAAN,CAD8C,CA9ElD,CAkFA,OAAO+Q,SAA0B,CAACC,CAAD,CAAoBxlB,CAApB,CAA2BtG,CAA3B,CAAiC+rB,CAAjC,CAA8CtJ,CAA9C,CAAiE,CAC5FmI,CAAJ,EACEA,CAAAvtB,KAAA,CAAeiJ,CAAf,CAGA,CAFAskB,CAAAvtB,KAAA,CAAe2C,CAAf,CAEA,CADA4qB,CAAAvtB,KAAA,CAAe0uB,CAAf,CACA,CAAAnB,CAAAvtB,KAAA,CAAeolB,CAAf,CAJF,EAMEoI,CAAA,CAAwBC,CAAxB,CAAkDxkB,CAAlD,CAAyDtG,CAAzD,CAA+D+rB,CAA/D,CAA4EtJ,CAA5E,CAP8F,CAlGd,CAkHtFwC,QAASA,EAAU,CAACgD,CAAD,CAAIC,CAAJ,CAAO,CACxB,IAAI8D,EAAO9D,CAAA7H,SAAP2L,CAAoB/D,CAAA5H,SACxB,OAAa,EAAb,GAAI2L,CAAJ,CAAuBA,CAAvB,CACI/D,CAAAxiB,KAAJ;AAAeyiB,CAAAziB,KAAf,CAA+BwiB,CAAAxiB,KAAD,CAAUyiB,CAAAziB,KAAV,CAAqB,EAArB,CAAyB,CAAvD,CACOwiB,CAAApqB,MADP,CACiBqqB,CAAArqB,MAJO,CAQ1ByrB,QAASA,EAAiB,CAAC2C,CAAD,CAAOC,CAAP,CAA0BliB,CAA1B,CAAqCtG,CAArC,CAA8C,CACtE,GAAIwoB,CAAJ,CACE,KAAM3G,GAAA,CAAe,UAAf,CACF2G,CAAAzmB,KADE,CACsBuE,CAAAvE,KADtB,CACsCwmB,CADtC,CAC4CxoB,EAAA,CAAYC,CAAZ,CAD5C,CAAN,CAFoE,CAQxEshB,QAASA,EAA2B,CAAC5E,CAAD,CAAa+L,CAAb,CAAmB,CACrD,IAAIC,EAAgBtL,CAAA,CAAaqL,CAAb,CAAmB,CAAA,CAAnB,CAChBC,EAAJ,EACEhM,CAAA/iB,KAAA,CAAgB,UACJ,CADI,SAEL+B,EAAA,CAAQitB,QAA8B,CAAC/lB,CAAD,CAAQtG,CAAR,CAAc,CAAA,IACvDjB,EAASiB,CAAAjB,OAAA,EAD8C,CAEvDutB,EAAWvtB,CAAA2H,KAAA,CAAY,UAAZ,CAAX4lB,EAAsC,EAC1CA,EAAAjvB,KAAA,CAAc+uB,CAAd,CACAtK,GAAA,CAAa/iB,CAAA2H,KAAA,CAAY,UAAZ,CAAwB4lB,CAAxB,CAAb,CAAgD,YAAhD,CACAhmB,EAAApF,OAAA,CAAakrB,CAAb,CAA4BG,QAAiC,CAAC5uB,CAAD,CAAQ,CACnEqC,CAAA,CAAK,CAAL,CAAA2hB,UAAA,CAAoBhkB,CAD+C,CAArE,CAL2D,CAApD,CAFK,CAAhB,CAHmD,CAmBvD6uB,QAASA,EAAiB,CAACxsB,CAAD,CAAOysB,CAAP,CAA2B,CACnD,GAA0B,QAA1B,EAAIA,CAAJ,CACE,MAAOtL,EAAAuL,KAET,KAAIxmB,EAAMme,EAAA,CAAUrkB,CAAV,CAEV,IAA0B,WAA1B,EAAIysB,CAAJ,EACY,MADZ,EACKvmB,CADL,EAC4C,QAD5C,EACsBumB,CADtB,EAEY,KAFZ,EAEKvmB,CAFL,GAE4C,KAF5C,EAEsBumB,CAFtB,EAG4C,OAH5C,EAGsBA,CAHtB,EAIE,MAAOtL,EAAAwL,aAV0C,CAerD5H,QAASA,EAA2B,CAAC/kB,CAAD,CAAOogB,CAAP,CAAmBziB,CAAnB,CAA0B8H,CAA1B,CAAgC,CAClE,IAAI2mB;AAAgBtL,CAAA,CAAanjB,CAAb,CAAoB,CAAA,CAApB,CAGpB,IAAKyuB,CAAL,CAAA,CAGA,GAAa,UAAb,GAAI3mB,CAAJ,EAA+C,QAA/C,GAA2B4e,EAAA,CAAUrkB,CAAV,CAA3B,CACE,KAAMulB,GAAA,CAAe,UAAf,CAEF9hB,EAAA,CAAYzD,CAAZ,CAFE,CAAN,CAKFogB,CAAA/iB,KAAA,CAAgB,UACJ,GADI,SAELkJ,QAAQ,EAAG,CAChB,MAAO,KACAqmB,QAAiC,CAACtmB,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CACvD2nB,CAAAA,CAAe3nB,CAAA2nB,YAAfA,GAAoC3nB,CAAA2nB,YAApCA,CAAuD,EAAvDA,CAEJ,IAAI/H,CAAAnZ,KAAA,CAA+BnB,CAA/B,CAAJ,CACE,KAAM8f,GAAA,CAAe,aAAf,CAAN,CAWF,GAJA6G,CAIA,CAJgBtL,CAAA,CAAa3gB,CAAA,CAAKsF,CAAL,CAAb,CAAyB,CAAA,CAAzB,CAA+B+mB,CAAA,CAAkBxsB,CAAlB,CAAwByF,CAAxB,CAA/B,CAIhB,CAIAtF,CAAA,CAAKsF,CAAL,CAEC,CAFY2mB,CAAA,CAAc9lB,CAAd,CAEZ,CADAumB,CAAA/E,CAAA,CAAYriB,CAAZ,CAAAonB,GAAsB/E,CAAA,CAAYriB,CAAZ,CAAtBonB,CAA0C,EAA1CA,UACA,CADyD,CAAA,CACzD,CAAA3rB,CAAAf,CAAA2nB,YAAA5mB,EAAoBf,CAAA2nB,YAAA,CAAiBriB,CAAjB,CAAAsiB,QAApB7mB,EAAsDoF,CAAtDpF,QAAA,CACQkrB,CADR,CACuBG,QAAiC,CAACO,CAAD,CAAWC,CAAX,CAAqB,CAO9D,OAAZ,GAAGtnB,CAAH,EAAuBqnB,CAAvB,EAAmCC,CAAnC,CACE5sB,CAAA6sB,aAAA,CAAkBF,CAAlB,CAA4BC,CAA5B,CADF,CAGE5sB,CAAAuqB,KAAA,CAAUjlB,CAAV,CAAgBqnB,CAAhB,CAVwE,CAD7E,CArB0D,CADxD,CADS,CAFN,CAAhB,CATA,CAJkE,CAqEpErD,QAASA,GAAW,CAACjH,CAAD,CAAeyK,CAAf,CAAiCC,CAAjC,CAA0C,CAAA,IACxDC,EAAuBF,CAAA,CAAiB,CAAjB,CADiC,CAExDG,EAAcH,CAAAzwB,OAF0C,CAGxDuC,EAASouB,CAAA7Z,WAH+C,CAIxD9V,CAJwD,CAIrD6V,CAEP,IAAImP,CAAJ,CACE,IAAIhlB,CAAO,CAAH,CAAG,CAAA6V,CAAA,CAAKmP,CAAAhmB,OAAhB,CAAqCgB,CAArC,CAAyC6V,CAAzC,CAA6C7V,CAAA,EAA7C,CACE,GAAIglB,CAAA,CAAahlB,CAAb,CAAJ;AAAuB2vB,CAAvB,CAA6C,CAC3C3K,CAAA,CAAahlB,CAAA,EAAb,CAAA,CAAoB0vB,CACJG,EAAAA,CAAKxc,CAALwc,CAASD,CAATC,CAAuB,CAAvC,KAAK,IACIvc,EAAK0R,CAAAhmB,OADd,CAEKqU,CAFL,CAESC,CAFT,CAEaD,CAAA,EAAA,CAAKwc,CAAA,EAFlB,CAGMA,CAAJ,CAASvc,CAAT,CACE0R,CAAA,CAAa3R,CAAb,CADF,CACoB2R,CAAA,CAAa6K,CAAb,CADpB,CAGE,OAAO7K,CAAA,CAAa3R,CAAb,CAGX2R,EAAAhmB,OAAA,EAAuB4wB,CAAvB,CAAqC,CACrC,MAZ2C,CAiB7CruB,CAAJ,EACEA,CAAAuuB,aAAA,CAAoBJ,CAApB,CAA6BC,CAA7B,CAEErd,EAAAA,CAAW5T,CAAA6T,uBAAA,EACfD,EAAAI,YAAA,CAAqBid,CAArB,CACAD,EAAA,CAAQvpB,CAAA4pB,QAAR,CAAA,CAA0BJ,CAAA,CAAqBxpB,CAAA4pB,QAArB,CACjBC,EAAAA,CAAI,CAAb,KAAgBC,CAAhB,CAAqBR,CAAAzwB,OAArB,CAA8CgxB,CAA9C,CAAkDC,CAAlD,CAAsDD,CAAA,EAAtD,CACM9pB,CAGJ,CAHcupB,CAAA,CAAiBO,CAAjB,CAGd,CAFA7pB,CAAA,CAAOD,CAAP,CAAAyb,OAAA,EAEA,CADArP,CAAAI,YAAA,CAAqBxM,CAArB,CACA,CAAA,OAAOupB,CAAA,CAAiBO,CAAjB,CAGTP,EAAA,CAAiB,CAAjB,CAAA,CAAsBC,CACtBD,EAAAzwB,OAAA,CAA0B,CAvCkC,CA2C9D8pB,QAASA,GAAkB,CAAC/jB,CAAD,CAAKmrB,CAAL,CAAiB,CAC1C,MAAOlvB,EAAA,CAAO,QAAQ,EAAG,CAAE,MAAO+D,EAAAI,MAAA,CAAS,IAAT,CAAejE,SAAf,CAAT,CAAlB,CAAyD6D,CAAzD,CAA6DmrB,CAA7D,CADmC,CApxC5C,IAAIlK,GAAaA,QAAQ,CAAC9f,CAAD,CAAUvD,CAAV,CAAgB,CACvC,IAAA6mB,UAAA,CAAiBtjB,CACjB,KAAAwgB,MAAA,CAAa/jB,CAAb,EAAqB,EAFkB,CAKzCqjB,GAAA7L,UAAA,CAAuB,YACTyM,EADS,WAeTuJ,QAAQ,CAACC,CAAD,CAAW,CAC1BA,CAAH,EAAiC,CAAjC,CAAeA,CAAApxB,OAAf,EACE4kB,CAAAkB,SAAA,CAAkB,IAAA0E,UAAlB;AAAkC4G,CAAlC,CAF2B,CAfV,cAgCNC,QAAQ,CAACD,CAAD,CAAW,CAC7BA,CAAH,EAAiC,CAAjC,CAAeA,CAAApxB,OAAf,EACE4kB,CAAA0M,YAAA,CAAqB,IAAA9G,UAArB,CAAqC4G,CAArC,CAF8B,CAhCb,cAkDNZ,QAAQ,CAACe,CAAD,CAAatC,CAAb,CAAyB,CAC9C,IAAIuC,EAAQC,EAAA,CAAgBF,CAAhB,CAA4BtC,CAA5B,CAAZ,CACIyC,EAAWD,EAAA,CAAgBxC,CAAhB,CAA4BsC,CAA5B,CAEK,EAApB,GAAGC,CAAAxxB,OAAH,CACE4kB,CAAA0M,YAAA,CAAqB,IAAA9G,UAArB,CAAqCkH,CAArC,CADF,CAE8B,CAAvB,GAAGA,CAAA1xB,OAAH,CACL4kB,CAAAkB,SAAA,CAAkB,IAAA0E,UAAlB,CAAkCgH,CAAlC,CADK,CAGL5M,CAAA+M,SAAA,CAAkB,IAAAnH,UAAlB,CAAkCgH,CAAlC,CAAyCE,CAAzC,CAT4C,CAlD3B,MAwEfxD,QAAQ,CAAC3tB,CAAD,CAAMY,CAAN,CAAaywB,CAAb,CAAwB9G,CAAxB,CAAkC,CAAA,IAK1C+G,EAAa5a,EAAA,CAAmB,IAAAuT,UAAA,CAAe,CAAf,CAAnB,CAAsCjqB,CAAtC,CAIbsxB,EAAJ,GACE,IAAArH,UAAA9mB,KAAA,CAAoBnD,CAApB,CAAyBY,CAAzB,CACA,CAAA2pB,CAAA,CAAW+G,CAFb,CAKA,KAAA,CAAKtxB,CAAL,CAAA,CAAYY,CAGR2pB,EAAJ,CACE,IAAApD,MAAA,CAAWnnB,CAAX,CADF,CACoBuqB,CADpB,EAGEA,CAHF,CAGa,IAAApD,MAAA,CAAWnnB,CAAX,CAHb,IAKI,IAAAmnB,MAAA,CAAWnnB,CAAX,CALJ,CAKsBuqB,CALtB,CAKiCrgB,EAAA,CAAWlK,CAAX,CAAgB,GAAhB,CALjC,CASAkD,EAAA,CAAWokB,EAAA,CAAU,IAAA2C,UAAV,CAGX,IAAkB,GAAlB,GAAK/mB,CAAL,EAAiC,MAAjC,GAAyBlD,CAAzB,EACkB,KADlB,GACKkD,CADL,EACmC,KADnC,GAC2BlD,CAD3B,CAEE,IAAA,CAAKA,CAAL,CAAA,CAAYY,CAAZ,CAAoB0jB,CAAA,CAAc1jB,CAAd,CAA6B,KAA7B,GAAqBZ,CAArB,CAGJ,EAAA,CAAlB;AAAIqxB,CAAJ,GACgB,IAAd,GAAIzwB,CAAJ,EAAsBA,CAAtB,GAAgCxB,CAAhC,CACE,IAAA6qB,UAAAsH,WAAA,CAA0BhH,CAA1B,CADF,CAGE,IAAAN,UAAA7mB,KAAA,CAAoBmnB,CAApB,CAA8B3pB,CAA9B,CAJJ,CAUA,EADImqB,CACJ,CADkB,IAAAA,YAClB,GAAelrB,CAAA,CAAQkrB,CAAA,CAAY/qB,CAAZ,CAAR,CAA0B,QAAQ,CAACwF,CAAD,CAAK,CACpD,GAAI,CACFA,CAAA,CAAG5E,CAAH,CADE,CAEF,MAAOmG,CAAP,CAAU,CACVqc,CAAA,CAAkBrc,CAAlB,CADU,CAHwC,CAAvC,CA5C+B,CAxE3B,UAgJX+jB,QAAQ,CAAC9qB,CAAD,CAAMwF,CAAN,CAAU,CAAA,IACtB+gB,EAAQ,IADc,CAEtBwE,EAAexE,CAAAwE,YAAfA,GAAqCxE,CAAAwE,YAArCA,CAAyD,EAAzDA,CAFsB,CAGtByG,EAAazG,CAAA,CAAY/qB,CAAZ,CAAbwxB,GAAkCzG,CAAA,CAAY/qB,CAAZ,CAAlCwxB,CAAqD,EAArDA,CAEJA,EAAAlxB,KAAA,CAAekF,CAAf,CACAwW,EAAA9X,WAAA,CAAsB,QAAQ,EAAG,CAC1BstB,CAAA1B,QAAL,EAEEtqB,CAAA,CAAG+gB,CAAA,CAAMvmB,CAAN,CAAH,CAH6B,CAAjC,CAMA,OAAOwF,EAZmB,CAhJP,CAP+D,KAuKlFisB,EAAc1N,CAAA0N,YAAA,EAvKoE,CAwKlFC,GAAY3N,CAAA2N,UAAA,EAxKsE,CAyKlF7E,EAAsC,IAChB,EADC4E,CACD,EADsC,IACtC,EADwBC,EACxB,CAAhBvvB,EAAgB,CAChB0qB,QAA4B,CAACjB,CAAD,CAAW,CACvC,MAAOA,EAAAvkB,QAAA,CAAiB,OAAjB,CAA0BoqB,CAA1B,CAAApqB,QAAA,CAA+C,KAA/C,CAAsDqqB,EAAtD,CADgC,CA3KqC,CA8KlF7J,EAAkB,cAGtB,OAAOre,EAjL+E,CAJ5E,CA3H6C,CAu6C3D6d,QAASA,GAAkB,CAAC3e,CAAD,CAAO,CAChC,MAAOuI,GAAA,CAAUvI,CAAArB,QAAA,CAAasqB,EAAb,CAA4B,EAA5B,CAAV,CADyB,CA8DlCT,QAASA,GAAe,CAACU,CAAD;AAAOC,CAAP,CAAa,CAAA,IAC/BC,EAAS,EADsB,CAE/BC,EAAUH,CAAAjqB,MAAA,CAAW,KAAX,CAFqB,CAG/BqqB,EAAUH,CAAAlqB,MAAA,CAAW,KAAX,CAHqB,CAM3BlH,EAAI,CADZ,EAAA,CACA,IAAA,CAAeA,CAAf,CAAmBsxB,CAAAtyB,OAAnB,CAAmCgB,CAAA,EAAnC,CAAwC,CAEtC,IADA,IAAIwxB,EAAQF,CAAA,CAAQtxB,CAAR,CAAZ,CACQqT,EAAI,CAAZ,CAAeA,CAAf,CAAmBke,CAAAvyB,OAAnB,CAAmCqU,CAAA,EAAnC,CACE,GAAGme,CAAH,EAAYD,CAAA,CAAQle,CAAR,CAAZ,CAAwB,SAAS,CAEnCge,EAAA,GAA2B,CAAhB,CAAAA,CAAAryB,OAAA,CAAoB,GAApB,CAA0B,EAArC,EAA2CwyB,CALL,CAOxC,MAAOH,EAb4B,CA0BrCliB,QAASA,GAAmB,EAAG,CAAA,IACzBmX,EAAc,EADW,CAEzBmL,EAAY,yBAWhB,KAAAC,SAAA,CAAgBC,QAAQ,CAAC1pB,CAAD,CAAOmC,CAAP,CAAoB,CAC1CC,EAAA,CAAwBpC,CAAxB,CAA8B,YAA9B,CACIlG,EAAA,CAASkG,CAAT,CAAJ,CACEjH,CAAA,CAAOslB,CAAP,CAAoBre,CAApB,CADF,CAGEqe,CAAA,CAAYre,CAAZ,CAHF,CAGsBmC,CALoB,CAU5C,KAAAuO,KAAA,CAAY,CAAC,WAAD,CAAc,SAAd,CAAyB,QAAQ,CAAC4B,CAAD,CAAYc,CAAZ,CAAqB,CAwBhE,MAAO,SAAQ,CAACuW,CAAD,CAAa9X,CAAb,CAAqB,CAAA,IAC9BM,CAD8B,CACbhQ,CADa,CACAynB,CAE/B3yB,EAAA,CAAS0yB,CAAT,CAAH,GACEjrB,CAOA,CAPQirB,CAAAjrB,MAAA,CAAiB8qB,CAAjB,CAOR,CANArnB,CAMA,CANczD,CAAA,CAAM,CAAN,CAMd,CALAkrB,CAKA,CALalrB,CAAA,CAAM,CAAN,CAKb,CAJAirB,CAIA,CAJatL,CAAA7mB,eAAA,CAA2B2K,CAA3B,CACA,CAAPkc,CAAA,CAAYlc,CAAZ,CAAO,CACPE,EAAA,CAAOwP,CAAAmR,OAAP,CAAsB7gB,CAAtB,CAAmC,CAAA,CAAnC,CADO,EACqCE,EAAA,CAAO+Q,CAAP,CAAgBjR,CAAhB,CAA6B,CAAA,CAA7B,CAElD,CAAAF,EAAA,CAAY0nB,CAAZ,CAAwBxnB,CAAxB,CAAqC,CAAA,CAArC,CARF,CAWAgQ,EAAA,CAAWG,CAAA7B,YAAA,CAAsBkZ,CAAtB,CAAkC9X,CAAlC,CAEX,IAAI+X,CAAJ,CAAgB,CACd,GAAM/X,CAAAA,CAAN,EAAwC,QAAxC;AAAgB,MAAOA,EAAAmR,OAAvB,CACE,KAAMrsB,EAAA,CAAO,aAAP,CAAA,CAAsB,OAAtB,CAEFwL,CAFE,EAEawnB,CAAA3pB,KAFb,CAE8B4pB,CAF9B,CAAN,CAKF/X,CAAAmR,OAAA,CAAc4G,CAAd,CAAA,CAA4BzX,CAPd,CAUhB,MAAOA,EA1B2B,CAxB4B,CAAtD,CAvBiB,CAsG/BhL,QAASA,GAAiB,EAAE,CAC1B,IAAAuJ,KAAA,CAAY,CAAC,SAAD,CAAY,QAAQ,CAACla,CAAD,CAAQ,CACtC,MAAO0H,EAAA,CAAO1H,CAAAC,SAAP,CAD+B,CAA5B,CADc,CAsC5B2Q,QAASA,GAAyB,EAAG,CACnC,IAAAsJ,KAAA,CAAY,CAAC,MAAD,CAAS,QAAQ,CAAC0D,CAAD,CAAO,CAClC,MAAO,SAAQ,CAACyV,CAAD,CAAYC,CAAZ,CAAmB,CAChC1V,CAAAM,MAAAxX,MAAA,CAAiBkX,CAAjB,CAAuBnb,SAAvB,CADgC,CADA,CAAxB,CADuB,CAcrC8wB,QAASA,GAAY,CAAC5D,CAAD,CAAU,CAAA,IACzBhc,EAAS,EADgB,CACZ7S,CADY,CACP+F,CADO,CACFtF,CAE3B,IAAI,CAACouB,CAAL,CAAc,MAAOhc,EAErBhT,EAAA,CAAQgvB,CAAAlnB,MAAA,CAAc,IAAd,CAAR,CAA6B,QAAQ,CAAC+qB,CAAD,CAAO,CAC1CjyB,CAAA,CAAIiyB,CAAAjvB,QAAA,CAAa,GAAb,CACJzD,EAAA,CAAMyG,CAAA,CAAUkM,EAAA,CAAK+f,CAAA5K,OAAA,CAAY,CAAZ,CAAernB,CAAf,CAAL,CAAV,CACNsF,EAAA,CAAM4M,EAAA,CAAK+f,CAAA5K,OAAA,CAAYrnB,CAAZ,CAAgB,CAAhB,CAAL,CAEFT,EAAJ,GAEI6S,CAAA,CAAO7S,CAAP,CAFJ,CACM6S,CAAA,CAAO7S,CAAP,CAAJ,CACE6S,CAAA,CAAO7S,CAAP,CADF,EACiB,IADjB,CACwB+F,CADxB,EAGgBA,CAJlB,CAL0C,CAA5C,CAcA,OAAO8M,EAnBsB,CAmC/B8f,QAASA,GAAa,CAAC9D,CAAD,CAAU,CAC9B,IAAI+D,EAAapwB,CAAA,CAASqsB,CAAT,CAAA,CAAoBA,CAApB,CAA8BzvB,CAE/C,OAAO,SAAQ,CAACsJ,CAAD,CAAO,CACfkqB,CAAL,GAAiBA,CAAjB,CAA+BH,EAAA,CAAa5D,CAAb,CAA/B,CAEA,OAAInmB,EAAJ,CACSkqB,CAAA,CAAWnsB,CAAA,CAAUiC,CAAV,CAAX,CADT;AACwC,IADxC,CAIOkqB,CAPa,CAHQ,CAyBhCC,QAASA,GAAa,CAAClpB,CAAD,CAAOklB,CAAP,CAAgBiE,CAAhB,CAAqB,CACzC,GAAI7yB,CAAA,CAAW6yB,CAAX,CAAJ,CACE,MAAOA,EAAA,CAAInpB,CAAJ,CAAUklB,CAAV,CAEThvB,EAAA,CAAQizB,CAAR,CAAa,QAAQ,CAACttB,CAAD,CAAK,CACxBmE,CAAA,CAAOnE,CAAA,CAAGmE,CAAH,CAASklB,CAAT,CADiB,CAA1B,CAIA,OAAOllB,EARkC,CAiB3CuG,QAASA,GAAa,EAAG,CAAA,IACnB6iB,EAAa,kBADM,CAEnBC,EAAW,YAFQ,CAGnBC,EAAoB,cAHD,CAInBC,EAAgC,CAAC,cAAD,CAAiB,gCAAjB,CAJb,CAMnBC,EAAW,IAAAA,SAAXA,CAA2B,mBAEV,CAAC,QAAQ,CAACxpB,CAAD,CAAO,CAC7BhK,CAAA,CAASgK,CAAT,CAAJ,GAEEA,CACA,CADOA,CAAAtC,QAAA,CAAa4rB,CAAb,CAAgC,EAAhC,CACP,CAAIF,CAAAlpB,KAAA,CAAgBF,CAAhB,CAAJ,EAA6BqpB,CAAAnpB,KAAA,CAAcF,CAAd,CAA7B,GACEA,CADF,CACSvD,EAAA,CAASuD,CAAT,CADT,CAHF,CAMA,OAAOA,EAP0B,CAAhB,CAFU,kBAaX,CAAC,QAAQ,CAACypB,CAAD,CAAI,CAC7B,MAAO5wB,EAAA,CAAS4wB,CAAT,CAAA,EAxjNmB,eAwjNnB,GAxjNJzwB,EAAAxC,KAAA,CAwjN2BizB,CAxjN3B,CAwjNI,EAnjNmB,eAmjNnB,GAnjNJzwB,EAAAxC,KAAA,CAmjNyCizB,CAnjNzC,CAmjNI,CAA0CptB,EAAA,CAAOotB,CAAP,CAA1C,CAAsDA,CADhC,CAAb,CAbW,SAkBpB,QACC,QACI,mCADJ,CADD,MAIC3uB,EAAA,CAAYyuB,CAAZ,CAJD;IAKCzuB,EAAA,CAAYyuB,CAAZ,CALD,OAMCzuB,EAAA,CAAYyuB,CAAZ,CAND,CAlBoB,gBA2Bb,YA3Ba,gBA4Bb,cA5Ba,CANR,CAyCnBG,EAAuB,IAAAC,aAAvBD,CAA2C,EAzCxB,CA+CnBE,EAA+B,IAAAC,qBAA/BD,CAA2D,EAE/D,KAAAna,KAAA,CAAY,CAAC,cAAD,CAAiB,UAAjB,CAA6B,eAA7B,CAA8C,YAA9C,CAA4D,IAA5D,CAAkE,WAAlE,CACR,QAAQ,CAACqa,CAAD,CAAeC,CAAf,CAAyBhR,CAAzB,CAAwC1G,CAAxC,CAAoD2X,CAApD,CAAwD3Y,CAAxD,CAAmE,CAihB7EgJ,QAASA,EAAK,CAAC4P,CAAD,CAAgB,CA6E5BC,QAASA,EAAiB,CAAClF,CAAD,CAAW,CAEnC,IAAImF,EAAOryB,CAAA,CAAO,EAAP,CAAWktB,CAAX,CAAqB,MACxBkE,EAAA,CAAclE,CAAAhlB,KAAd,CAA6BglB,CAAAE,QAA7B,CAA+CviB,CAAAunB,kBAA/C,CADwB,CAArB,CAGX,OAzpBC,IA0pBM,EADWlF,CAAAoF,OACX,EA1pBoB,GA0pBpB,CADWpF,CAAAoF,OACX,CAAHD,CAAG,CACHH,CAAAK,OAAA,CAAUF,CAAV,CAP+B,CA5ErC,IAAIxnB,EAAS,QACH,KADG,kBAEO6mB,CAAAc,iBAFP,mBAGQd,CAAAU,kBAHR,CAAb,CAKIhF,EAiFJqF,QAAqB,CAAC5nB,CAAD,CAAS,CA2B5B6nB,QAASA,EAAW,CAACtF,CAAD,CAAU,CAC5B,IAAIuF,CAEJv0B;CAAA,CAAQgvB,CAAR,CAAiB,QAAQ,CAACwF,CAAD,CAAWC,CAAX,CAAmB,CACtCr0B,CAAA,CAAWo0B,CAAX,CAAJ,GACED,CACA,CADgBC,CAAA,EAChB,CAAqB,IAArB,EAAID,CAAJ,CACEvF,CAAA,CAAQyF,CAAR,CADF,CACoBF,CADpB,CAGE,OAAOvF,CAAA,CAAQyF,CAAR,CALX,CAD0C,CAA5C,CAH4B,CA3BF,IACxBC,EAAapB,CAAAtE,QADW,CAExB2F,EAAa/yB,CAAA,CAAO,EAAP,CAAW6K,CAAAuiB,QAAX,CAFW,CAGxB4F,CAHwB,CAGeC,CAHf,CAK5BH,EAAa9yB,CAAA,CAAO,EAAP,CAAW8yB,CAAAI,OAAX,CAA8BJ,CAAA,CAAW9tB,CAAA,CAAU6F,CAAAL,OAAV,CAAX,CAA9B,CAGbkoB,EAAA,CAAYI,CAAZ,CACAJ,EAAA,CAAYK,CAAZ,CAGA,EAAA,CACA,IAAKC,CAAL,GAAsBF,EAAtB,CAAkC,CAChCK,CAAA,CAAyBnuB,CAAA,CAAUguB,CAAV,CAEzB,KAAKC,CAAL,GAAsBF,EAAtB,CACE,GAAI/tB,CAAA,CAAUiuB,CAAV,CAAJ,GAAiCE,CAAjC,CACE,SAAS,CAIbJ,EAAA,CAAWC,CAAX,CAAA,CAA4BF,CAAA,CAAWE,CAAX,CATI,CAYlC,MAAOD,EAzBqB,CAjFhB,CAAaZ,CAAb,CAEdnyB,EAAA,CAAO6K,CAAP,CAAesnB,CAAf,CACAtnB,EAAAuiB,QAAA,CAAiBA,CACjBviB,EAAAL,OAAA,CAAgBU,EAAA,CAAUL,CAAAL,OAAV,CAKhB,EAHI4oB,CAGJ,CAHgBC,EAAA,CAAgBxoB,CAAAyR,IAAhB,CACA,CAAV2V,CAAA5T,QAAA,EAAA,CAAmBxT,CAAAyoB,eAAnB,EAA4C5B,CAAA4B,eAA5C,CAAU,CACV31B,CACN,IACEyvB,CAAA,CAASviB,CAAA0oB,eAAT,EAAkC7B,CAAA6B,eAAlC,CADF,CACgEH,CADhE,CA0BA,KAAII,EAAQ,CArBQC,QAAQ,CAAC5oB,CAAD,CAAS,CACnCuiB,CAAA,CAAUviB,CAAAuiB,QACV,KAAIsG,EAAUtC,EAAA,CAAcvmB,CAAA3C,KAAd,CAA2BgpB,EAAA,CAAc9D,CAAd,CAA3B,CAAmDviB,CAAA2nB,iBAAnD,CAGV3xB,EAAA,CAAYgK,CAAA3C,KAAZ,CAAJ,EACE9J,CAAA,CAAQgvB,CAAR,CAAiB,QAAQ,CAACjuB,CAAD,CAAQ0zB,CAAR,CAAgB,CACb,cAA1B,GAAI7tB,CAAA,CAAU6tB,CAAV,CAAJ,EACI,OAAOzF,CAAA,CAAQyF,CAAR,CAF4B,CAAzC,CAOEhyB;CAAA,CAAYgK,CAAA8oB,gBAAZ,CAAJ,EAA4C,CAAA9yB,CAAA,CAAY6wB,CAAAiC,gBAAZ,CAA5C,GACE9oB,CAAA8oB,gBADF,CAC2BjC,CAAAiC,gBAD3B,CAKA,OAAOC,EAAA,CAAQ/oB,CAAR,CAAgB6oB,CAAhB,CAAyBtG,CAAzB,CAAAyG,KAAA,CAAuCzB,CAAvC,CAA0DA,CAA1D,CAlB4B,CAqBzB,CAAgBz0B,CAAhB,CAAZ,CACIm2B,EAAU5B,CAAA6B,KAAA,CAAQlpB,CAAR,CAYd,KATAzM,CAAA,CAAQ41B,CAAR,CAA8B,QAAQ,CAACC,CAAD,CAAc,CAClD,CAAIA,CAAAC,QAAJ,EAA2BD,CAAAE,aAA3B,GACEX,CAAA5zB,QAAA,CAAcq0B,CAAAC,QAAd,CAAmCD,CAAAE,aAAnC,CAEF,EAAIF,CAAA/G,SAAJ,EAA4B+G,CAAAG,cAA5B,GACEZ,CAAA30B,KAAA,CAAWo1B,CAAA/G,SAAX,CAAiC+G,CAAAG,cAAjC,CALgD,CAApD,CASA,CAAMZ,CAAAx1B,OAAN,CAAA,CAAoB,CACdq2B,CAAAA,CAASb,CAAA7iB,MAAA,EACb,KAAI2jB,EAAWd,CAAA7iB,MAAA,EAAf,CAEAmjB,EAAUA,CAAAD,KAAA,CAAaQ,CAAb,CAAqBC,CAArB,CAJQ,CAOpBR,CAAAnH,QAAA,CAAkB4H,QAAQ,CAACxwB,CAAD,CAAK,CAC7B+vB,CAAAD,KAAA,CAAa,QAAQ,CAAC3G,CAAD,CAAW,CAC9BnpB,CAAA,CAAGmpB,CAAAhlB,KAAH,CAAkBglB,CAAAoF,OAAlB,CAAmCpF,CAAAE,QAAnC,CAAqDviB,CAArD,CAD8B,CAAhC,CAGA,OAAOipB,EAJsB,CAO/BA,EAAAnY,MAAA,CAAgB6Y,QAAQ,CAACzwB,CAAD,CAAK,CAC3B+vB,CAAAD,KAAA,CAAa,IAAb,CAAmB,QAAQ,CAAC3G,CAAD,CAAW,CACpCnpB,CAAA,CAAGmpB,CAAAhlB,KAAH,CAAkBglB,CAAAoF,OAAlB,CAAmCpF,CAAAE,QAAnC,CAAqDviB,CAArD,CADoC,CAAtC,CAGA,OAAOipB,EAJoB,CAO7B;MAAOA,EA3EqB,CAiQ9BF,QAASA,EAAO,CAAC/oB,CAAD,CAAS6oB,CAAT,CAAkBX,CAAlB,CAA8B,CAqD5C0B,QAASA,EAAI,CAACnC,CAAD,CAASpF,CAAT,CAAmBwH,CAAnB,CAAkCC,CAAlC,CAA8C,CACrDlc,CAAJ,GA93BC,GA+3BC,EAAc6Z,CAAd,EA/3ByB,GA+3BzB,CAAcA,CAAd,CACE7Z,CAAAhC,IAAA,CAAU6F,CAAV,CAAe,CAACgW,CAAD,CAASpF,CAAT,CAAmB8D,EAAA,CAAa0D,CAAb,CAAnB,CAAgDC,CAAhD,CAAf,CADF,CAIElc,CAAAkI,OAAA,CAAarE,CAAb,CALJ,CASAsY,EAAA,CAAe1H,CAAf,CAAyBoF,CAAzB,CAAiCoC,CAAjC,CAAgDC,CAAhD,CACKpa,EAAAsa,QAAL,EAAyBta,CAAAtS,OAAA,EAXgC,CAkB3D2sB,QAASA,EAAc,CAAC1H,CAAD,CAAWoF,CAAX,CAAmBlF,CAAnB,CAA4BuH,CAA5B,CAAwC,CAE7DrC,CAAA,CAAS3G,IAAAC,IAAA,CAAS0G,CAAT,CAAiB,CAAjB,CAER,EAn5BA,GAm5BA,EAAUA,CAAV,EAn5B0B,GAm5B1B,CAAUA,CAAV,CAAoBwC,CAAAC,QAApB,CAAuCD,CAAAvC,OAAvC,EAAwD,MACjDrF,CADiD,QAE/CoF,CAF+C,SAG9CpB,EAAA,CAAc9D,CAAd,CAH8C,QAI/CviB,CAJ+C,YAK1C8pB,CAL0C,CAAxD,CAJ4D,CAc/DK,QAASA,EAAgB,EAAG,CAC1B,IAAIC,EAAMjzB,EAAA,CAAQugB,CAAA2S,gBAAR,CAA+BrqB,CAA/B,CACG,GAAb,GAAIoqB,CAAJ,EAAgB1S,CAAA2S,gBAAA/yB,OAAA,CAA6B8yB,CAA7B,CAAkC,CAAlC,CAFU,CArFgB,IACxCH,EAAW5C,CAAApT,MAAA,EAD6B,CAExCgV,EAAUgB,CAAAhB,QAF8B,CAGxCrb,CAHwC,CAIxC0c,CAJwC,CAKxC7Y,EAAM8Y,CAAA,CAASvqB,CAAAyR,IAAT,CAAqBzR,CAAAwqB,OAArB,CAEV9S,EAAA2S,gBAAAr2B,KAAA,CAA2BgM,CAA3B,CACAipB,EAAAD,KAAA,CAAamB,CAAb,CAA+BA,CAA/B,CAGA,EAAKnqB,CAAA4N,MAAL,EAAqBiZ,CAAAjZ,MAArB,IAAyD,CAAA,CAAzD,GAAwC5N,CAAA4N,MAAxC,EAAmF,KAAnF,EAAkE5N,CAAAL,OAAlE,IACEiO,CADF,CACU1X,CAAA,CAAS8J,CAAA4N,MAAT,CAAA,CAAyB5N,CAAA4N,MAAzB;AACA1X,CAAA,CAAS2wB,CAAAjZ,MAAT,CAAA,CAA2BiZ,CAAAjZ,MAA3B,CACA6c,CAHV,CAMA,IAAI7c,CAAJ,CAEE,GADA0c,CACI,CADS1c,CAAAP,IAAA,CAAUoE,CAAV,CACT,CAAAxb,CAAA,CAAUq0B,CAAV,CAAJ,CAA2B,CACzB,GAAIA,CAAAtB,KAAJ,CAGE,MADAsB,EAAAtB,KAAA,CAAgBmB,CAAhB,CAAkCA,CAAlC,CACOG,CAAAA,CAGHh3B,EAAA,CAAQg3B,CAAR,CAAJ,CACEP,CAAA,CAAeO,CAAA,CAAW,CAAX,CAAf,CAA8BA,CAAA,CAAW,CAAX,CAA9B,CAA6CnyB,EAAA,CAAYmyB,CAAA,CAAW,CAAX,CAAZ,CAA7C,CAAyEA,CAAA,CAAW,CAAX,CAAzE,CADF,CAGEP,CAAA,CAAeO,CAAf,CAA2B,GAA3B,CAAgC,EAAhC,CAAoC,IAApC,CAVqB,CAA3B,IAeE1c,EAAAhC,IAAA,CAAU6F,CAAV,CAAewX,CAAf,CAKAjzB,EAAA,CAAYs0B,CAAZ,CAAJ,EACEnD,CAAA,CAAannB,CAAAL,OAAb,CAA4B8R,CAA5B,CAAiCoX,CAAjC,CAA0Ce,CAA1C,CAAgD1B,CAAhD,CAA4DloB,CAAA0qB,QAA5D,CACI1qB,CAAA8oB,gBADJ,CAC4B9oB,CAAA2qB,aAD5B,CAIF,OAAO1B,EA5CqC,CA4F9CsB,QAASA,EAAQ,CAAC9Y,CAAD,CAAM+Y,CAAN,CAAc,CACzB,GAAI,CAACA,CAAL,CAAa,MAAO/Y,EACpB,KAAIlW,EAAQ,EACZrH,GAAA,CAAcs2B,CAAd,CAAsB,QAAQ,CAACl2B,CAAD,CAAQZ,CAAR,CAAa,CAC3B,IAAd,GAAIY,CAAJ,EAAsB0B,CAAA,CAAY1B,CAAZ,CAAtB,GACKhB,CAAA,CAAQgB,CAAR,CAEL,GAFqBA,CAErB,CAF6B,CAACA,CAAD,CAE7B,EAAAf,CAAA,CAAQe,CAAR,CAAe,QAAQ,CAAC4F,CAAD,CAAI,CACrBhE,CAAA,CAASgE,CAAT,CAAJ,GACEA,CADF,CACMR,EAAA,CAAOQ,CAAP,CADN,CAGAqB,EAAAvH,KAAA,CAAWyH,EAAA,CAAe/H,CAAf,CAAX,CAAiC,GAAjC,CACW+H,EAAA,CAAevB,CAAf,CADX,CAJyB,CAA3B,CAHA,CADyC,CAA3C,CAYkB,EAAlB,CAAGqB,CAAApI,OAAH,GACEse,CADF,GACgC,EAAtB,EAACA,CAAAta,QAAA,CAAY,GAAZ,CAAD,CAA2B,GAA3B,CAAiC,GAD3C,EACkDoE,CAAA3G,KAAA,CAAW,GAAX,CADlD,CAGA,OAAO6c,EAlBkB,CA52B/B,IAAIgZ,EAAerU,CAAA,CAAc,OAAd,CAAnB,CAOI+S,EAAuB,EAE3B51B,EAAA,CAAQwzB,CAAR,CAA8B,QAAQ,CAAC6D,CAAD,CAAqB,CACzDzB,CAAAp0B,QAAA,CAA6B1B,CAAA,CAASu3B,CAAT,CACA,CAAvBlc,CAAArB,IAAA,CAAcud,CAAd,CAAuB;AAAalc,CAAA1R,OAAA,CAAiB4tB,CAAjB,CAD1C,CADyD,CAA3D,CAKAr3B,EAAA,CAAQ0zB,CAAR,CAAsC,QAAQ,CAAC2D,CAAD,CAAqBp2B,CAArB,CAA4B,CACxE,IAAIq2B,EAAax3B,CAAA,CAASu3B,CAAT,CACA,CAAXlc,CAAArB,IAAA,CAAcud,CAAd,CAAW,CACXlc,CAAA1R,OAAA,CAAiB4tB,CAAjB,CAONzB,EAAA7xB,OAAA,CAA4B9C,CAA5B,CAAmC,CAAnC,CAAsC,UAC1B6tB,QAAQ,CAACA,CAAD,CAAW,CAC3B,MAAOwI,EAAA,CAAWxD,CAAA6B,KAAA,CAAQ7G,CAAR,CAAX,CADoB,CADO,eAIrBkH,QAAQ,CAAClH,CAAD,CAAW,CAChC,MAAOwI,EAAA,CAAWxD,CAAAK,OAAA,CAAUrF,CAAV,CAAX,CADyB,CAJE,CAAtC,CAVwE,CAA1E,CAooBA3K,EAAA2S,gBAAA,CAAwB,EA+FxBS,UAA2B,CAAC5uB,CAAD,CAAQ,CACjC3I,CAAA,CAAQ8B,SAAR,CAAmB,QAAQ,CAAC+G,CAAD,CAAO,CAChCsb,CAAA,CAAMtb,CAAN,CAAA,CAAc,QAAQ,CAACqV,CAAD,CAAMzR,CAAN,CAAc,CAClC,MAAO0X,EAAA,CAAMviB,CAAA,CAAO6K,CAAP,EAAiB,EAAjB,CAAqB,QACxB5D,CADwB,KAE3BqV,CAF2B,CAArB,CAAN,CAD2B,CADJ,CAAlC,CADiC,CAAnCqZ,CA7CA,CAAmB,KAAnB,CAA0B,QAA1B,CAAoC,MAApC,CAA4C,OAA5C,CAyDAC,UAAmC,CAAC3uB,CAAD,CAAO,CACxC7I,CAAA,CAAQ8B,SAAR,CAAmB,QAAQ,CAAC+G,CAAD,CAAO,CAChCsb,CAAA,CAAMtb,CAAN,CAAA,CAAc,QAAQ,CAACqV,CAAD,CAAMpU,CAAN,CAAY2C,CAAZ,CAAoB,CACxC,MAAO0X,EAAA,CAAMviB,CAAA,CAAO6K,CAAP,EAAiB,EAAjB,CAAqB,QACxB5D,CADwB,KAE3BqV,CAF2B,MAG1BpU,CAH0B,CAArB,CAAN,CADiC,CADV,CAAlC,CADwC,CAA1C0tB,CA9BA,CAA2B,MAA3B,CAAmC,KAAnC,CAYArT,EAAAmP,SAAA,CAAiBA,CAGjB,OAAOnP,EAhvBsE,CADnE,CAjDW,CAy7BzBsT,QAASA,GAAS,CAACrrB,CAAD,CAAS,CAIvB,GAAY,CAAZ,EAAI4L,CAAJ,GAAkB,CAAC5L,CAAA7E,MAAA,CAAa,uCAAb,CAAnB;AACE,CAAClI,CAAAq4B,eADH,EAEE,MAAO,KAAIr4B,CAAAs4B,cAAJ,CAAyB,mBAAzB,CACF,IAAIt4B,CAAAq4B,eAAJ,CACL,MAAO,KAAIr4B,CAAAq4B,eAGb,MAAMl4B,EAAA,CAAO,cAAP,CAAA,CAAuB,OAAvB,CAAN,CAXuB,CA8B3B8Q,QAASA,GAAoB,EAAG,CAC9B,IAAAiJ,KAAA,CAAY,CAAC,UAAD,CAAa,SAAb,CAAwB,WAAxB,CAAqC,QAAQ,CAACsa,CAAD,CAAW5X,CAAX,CAAoBgF,CAApB,CAA+B,CACtF,MAAO2W,GAAA,CAAkB/D,CAAlB,CAA4B4D,EAA5B,CAAuC5D,CAAAnT,MAAvC,CAAuDzE,CAAAhS,QAAA4tB,UAAvD,CAAkF5W,CAAA,CAAU,CAAV,CAAlF,CAD+E,CAA5E,CADkB,CAMhC2W,QAASA,GAAiB,CAAC/D,CAAD,CAAW4D,CAAX,CAAsBK,CAAtB,CAAqCD,CAArC,CAAgDxZ,CAAhD,CAA6D,CAyHrF0Z,QAASA,EAAQ,CAAC7Z,CAAD,CAAM8Z,CAAN,CAAkB3B,CAAlB,CAAwB,CAAA,IAInC4B,EAAS5Z,CAAA9K,cAAA,CAA0B,QAA1B,CAJ0B,CAIWwL,EAAW,IAC7DkZ,EAAArjB,KAAA,CAAc,iBACdqjB,EAAApzB,IAAA,CAAaqZ,CACb+Z,EAAAC,MAAA,CAAe,CAAA,CAEfnZ,EAAA,CAAWA,QAAQ,CAAC7H,CAAD,CAAQ,CACzBhC,EAAA,CAAsB+iB,CAAtB,CAA8B,MAA9B,CAAsClZ,CAAtC,CACA7J,GAAA,CAAsB+iB,CAAtB,CAA8B,OAA9B,CAAuClZ,CAAvC,CACAV,EAAA8Z,KAAArkB,YAAA,CAA6BmkB,CAA7B,CACAA,EAAA,CAAS,IACT,KAAI/D,EAAU,EAAd,CACI3E,EAAO,SAEPrY,EAAJ,GACqB,MAInB;AAJIA,CAAAtC,KAIJ,EAJ8BijB,CAAA,CAAUG,CAAV,CAAAI,OAI9B,GAHElhB,CAGF,CAHU,MAAQ,OAAR,CAGV,EADAqY,CACA,CADOrY,CAAAtC,KACP,CAAAsf,CAAA,CAAwB,OAAf,GAAAhd,CAAAtC,KAAA,CAAyB,GAAzB,CAA+B,GAL1C,CAQIyhB,EAAJ,EACEA,CAAA,CAAKnC,CAAL,CAAa3E,CAAb,CAjBuB,CAqB3B8I,GAAA,CAAmBJ,CAAnB,CAA2B,MAA3B,CAAmClZ,CAAnC,CACAsZ,GAAA,CAAmBJ,CAAnB,CAA2B,OAA3B,CAAoClZ,CAApC,CAEY,EAAZ,EAAI/G,CAAJ,GACEigB,CAAAK,mBADF,CAC8BC,QAAQ,EAAG,CACjCz4B,CAAA,CAASm4B,CAAAO,WAAT,CAAJ,EAAmC,iBAAAxuB,KAAA,CAAuBiuB,CAAAO,WAAvB,CAAnC,GACEP,CAAAK,mBACA,CAD4B,IAC5B,CAAAvZ,CAAA,CAAS,MACD,MADC,CAAT,CAFF,CADqC,CADzC,CAWAV,EAAA8Z,KAAA7kB,YAAA,CAA6B2kB,CAA7B,CACA,OAAOlZ,EA7CgC,CAxHzC,IAAI0Z,EAAW,EAGf,OAAO,SAAQ,CAACrsB,CAAD,CAAS8R,CAAT,CAAcoL,CAAd,CAAoBvK,CAApB,CAA8BiQ,CAA9B,CAAuCmI,CAAvC,CAAgD5B,CAAhD,CAAiE6B,CAAjE,CAA+E,CA0F5FsB,QAASA,EAAc,EAAG,CACxBxE,CAAA,CAASuE,CACTE,EAAA,EAAaA,CAAA,EACbC,EAAA,EAAOA,CAAAC,MAAA,EAHiB,CAM1BC,QAASA,EAAe,CAAC/Z,CAAD,CAAWmV,CAAX,CAAmBpF,CAAnB,CAA6BwH,CAA7B,CAA4CC,CAA5C,CAAwD,CAE9E1V,CAAA,EAAaiX,CAAAhX,OAAA,CAAqBD,CAArB,CACb8X,EAAA,CAAYC,CAAZ,CAAkB,IAKH,EAAf,GAAI1E,CAAJ,GACEA,CADF,CACWpF,CAAA,CAAW,GAAX,CAA6C,MAA5B,EAAAiK,EAAA,CAAW7a,CAAX,CAAA8a,SAAA,CAAqC,GAArC,CAA2C,CADvE,CAQAja,EAAA,CAHoB,IAAXmV,GAAAA,CAAAA,CAAkB,GAAlBA,CAAwBA,CAGjC,CAAiBpF,CAAjB,CAA2BwH,CAA3B,CAFaC,CAEb,EAF2B,EAE3B,CACA1C,EAAAnV,6BAAA,CAAsCrc,CAAtC,CAjB8E,CAhGY;AAC5F,IAAI6xB,CACJL,EAAAlV,6BAAA,EACAT,EAAA,CAAMA,CAAN,EAAa2V,CAAA3V,IAAA,EAEb,IAAyB,OAAzB,EAAItX,CAAA,CAAUwF,CAAV,CAAJ,CAAkC,CAChC,IAAI4rB,EAAa,GAAbA,CAAoBl1B,CAAA+0B,CAAAoB,QAAA,EAAAn2B,UAAA,CAA8B,EAA9B,CACxB+0B,EAAA,CAAUG,CAAV,CAAA,CAAwB,QAAQ,CAACluB,CAAD,CAAO,CACrC+tB,CAAA,CAAUG,CAAV,CAAAluB,KAAA,CAA6BA,CAC7B+tB,EAAA,CAAUG,CAAV,CAAAI,OAAA,CAA+B,CAAA,CAFM,CAKvC,KAAIO,EAAYZ,CAAA,CAAS7Z,CAAA1W,QAAA,CAAY,eAAZ,CAA6B,oBAA7B,CAAoDwwB,CAApD,CAAT,CACZA,CADY,CACA,QAAQ,CAAC9D,CAAD,CAAS3E,CAAT,CAAe,CACrCuJ,CAAA,CAAgB/Z,CAAhB,CAA0BmV,CAA1B,CAAkC2D,CAAA,CAAUG,CAAV,CAAAluB,KAAlC,CAA8D,EAA9D,CAAkEylB,CAAlE,CACAsI,EAAA,CAAUG,CAAV,CAAA,CAAwB31B,CAFa,CADvB,CAPgB,CAAlC,IAYO,CAEL,IAAIu2B,EAAMnB,CAAA,CAAUrrB,CAAV,CAEVwsB,EAAAM,KAAA,CAAS9sB,CAAT,CAAiB8R,CAAjB,CAAsB,CAAA,CAAtB,CACAle,EAAA,CAAQgvB,CAAR,CAAiB,QAAQ,CAACjuB,CAAD,CAAQZ,CAAR,CAAa,CAChCuC,CAAA,CAAU3B,CAAV,CAAJ,EACI63B,CAAAO,iBAAA,CAAqBh5B,CAArB,CAA0BY,CAA1B,CAFgC,CAAtC,CASA63B,EAAAN,mBAAA,CAAyBc,QAAQ,EAAG,CAQlC,GAAIR,CAAJ,EAA6B,CAA7B,EAAWA,CAAAJ,WAAX,CAAgC,CAAA,IAC1Ba,EAAkB,IADQ,CAE1BvK,EAAW,IAEZoF,EAAH,GAAcuE,CAAd,GACEY,CAIA,CAJkBT,CAAAU,sBAAA,EAIlB,CAAAxK,CAAA,CAAY,UAAD,EAAe8J,EAAf,CAAsBA,CAAA9J,SAAtB,CAAqC8J,CAAAW,aALlD,CAQAT,EAAA,CAAgB/Z,CAAhB,CACImV,CADJ,EACc0E,CAAA1E,OADd;AAEIpF,CAFJ,CAGIuK,CAHJ,CAIIT,CAAArC,WAJJ,EAIsB,EAJtB,CAZ8B,CARE,CA4BhChB,EAAJ,GACEqD,CAAArD,gBADF,CACwB,CAAA,CADxB,CAIA,IAAI6B,CAAJ,CACE,GAAI,CACFwB,CAAAxB,aAAA,CAAmBA,CADjB,CAEF,MAAOlwB,CAAP,CAAU,CAQV,GAAqB,MAArB,GAAIkwB,CAAJ,CACE,KAAMlwB,EAAN,CATQ,CAcd0xB,CAAAY,KAAA,CAASlQ,CAAT,EAAiB,IAAjB,CA/DK,CAkEP,GAAc,CAAd,CAAI6N,CAAJ,CACE,IAAItW,EAAYiX,CAAA,CAAcY,CAAd,CAA8BvB,CAA9B,CADlB,KAEWA,EAAJ,EAAeA,CAAA1B,KAAf,EACL0B,CAAA1B,KAAA,CAAaiD,CAAb,CAtF0F,CAJT,CAgNvFvoB,QAASA,GAAoB,EAAG,CAC9B,IAAIyhB,EAAc,IAAlB,CACIC,EAAY,IAWhB,KAAAD,YAAA,CAAmB6H,QAAQ,CAAC14B,CAAD,CAAO,CAChC,MAAIA,EAAJ,EACE6wB,CACO,CADO7wB,CACP,CAAA,IAFT,EAIS6wB,CALuB,CAkBlC,KAAAC,UAAA,CAAiB6H,QAAQ,CAAC34B,CAAD,CAAO,CAC9B,MAAIA,EAAJ,EACE8wB,CACO,CADK9wB,CACL,CAAA,IAFT,EAIS8wB,CALqB,CAUhC,KAAAtY,KAAA,CAAY,CAAC,QAAD,CAAW,mBAAX,CAAgC,MAAhC,CAAwC,QAAQ,CAAC8K,CAAD,CAASd,CAAT,CAA4BgB,CAA5B,CAAkC,CA0C5FL,QAASA,EAAY,CAACqL,CAAD,CAAOoK,CAAP,CAA2BC,CAA3B,CAA2C,CAW9D,IAX8D,IAC1D9zB,CAD0D,CAE1D+zB,CAF0D,CAG1D54B,EAAQ,CAHkD,CAI1D+G,EAAQ,EAJkD,CAK1DpI,EAAS2vB,CAAA3vB,OALiD,CAM1Dk6B,EAAmB,CAAA,CANuC,CAS1D9zB,EAAS,EAEb,CAAM/E,CAAN,CAAcrB,CAAd,CAAA,CAC4D,EAA1D,GAAOkG,CAAP,CAAoBypB,CAAA3rB,QAAA,CAAaguB,CAAb,CAA0B3wB,CAA1B,CAApB,GAC+E,EAD/E,GACO44B,CADP,CACkBtK,CAAA3rB,QAAA,CAAaiuB,CAAb,CAAwB/rB,CAAxB,CAAqCi0B,CAArC,CADlB,GAEG94B,CAID,EAJU6E,CAIV,EAJyBkC,CAAAvH,KAAA,CAAW8uB,CAAA9O,UAAA,CAAexf,CAAf;AAAsB6E,CAAtB,CAAX,CAIzB,CAHAkC,CAAAvH,KAAA,CAAWkF,CAAX,CAAgB0e,CAAA,CAAO2V,CAAP,CAAazK,CAAA9O,UAAA,CAAe3a,CAAf,CAA4Bi0B,CAA5B,CAA+CF,CAA/C,CAAb,CAAhB,CAGA,CAFAl0B,CAAAq0B,IAEA,CAFSA,CAET,CADA/4B,CACA,CADQ44B,CACR,CADmBI,CACnB,CAAAH,CAAA,CAAmB,CAAA,CANrB,GASG74B,CACD,EADUrB,CACV,EADqBoI,CAAAvH,KAAA,CAAW8uB,CAAA9O,UAAA,CAAexf,CAAf,CAAX,CACrB,CAAAA,CAAA,CAAQrB,CAVV,CAcF,EAAMA,CAAN,CAAeoI,CAAApI,OAAf,IAEEoI,CAAAvH,KAAA,CAAW,EAAX,CACA,CAAAb,CAAA,CAAS,CAHX,CAYA,IAAIg6B,CAAJ,EAAqC,CAArC,CAAsB5xB,CAAApI,OAAtB,CACI,KAAMs6B,GAAA,CAAmB,UAAnB,CAGsD3K,CAHtD,CAAN,CAMJ,GAAI,CAACoK,CAAL,EAA4BG,CAA5B,CA4CE,MA3CA9zB,EAAApG,OA2CO+F,CA3CS/F,CA2CT+F,CA1CPA,CA0COA,CA1CFA,QAAQ,CAACzF,CAAD,CAAU,CACrB,GAAI,CACF,IADE,IACMU,EAAI,CADV,CACa6V,EAAK7W,CADlB,CAC0Bu6B,CAA5B,CAAkCv5B,CAAlC,CAAoC6V,CAApC,CAAwC7V,CAAA,EAAxC,CAA6C,CAC3C,GAAgC,UAAhC,EAAI,OAAQu5B,CAAR,CAAenyB,CAAA,CAAMpH,CAAN,CAAf,CAAJ,CAOE,GANAu5B,CAMI,CANGA,CAAA,CAAKj6B,CAAL,CAMH,CAJFi6B,CAIE,CALAP,CAAJ,CACSrV,CAAA6V,WAAA,CAAgBR,CAAhB,CAAgCO,CAAhC,CADT,CAGS5V,CAAA8V,QAAA,CAAaF,CAAb,CAEL,CAAQ,IAAR,EAAAA,CAAJ,CACEA,CAAA,CAAO,EADT,KAGE,QAAQ,MAAOA,EAAf,EACE,KAAK,QAAL,CAEE,KAEF,MAAK,QAAL,CAEEA,CAAA,CAAO,EAAP,CAAYA,CACZ,MAEF,SAEEA,CAAA,CAAOh0B,EAAA,CAAOg0B,CAAP,CAZX,CAiBJn0B,CAAA,CAAOpF,CAAP,CAAA,CAAYu5B,CA5B+B,CA8B7C,MAAOn0B,EAAA3E,KAAA,CAAY,EAAZ,CA/BL,CAiCJ,MAAMoZ,CAAN,CAAW,CACL6f,CAEJ,CAFaJ,EAAA,CAAmB,QAAnB,CAA4D3K,CAA5D,CACT9U,CAAA3X,SAAA,EADS,CAEb,CAAAygB,CAAA,CAAkB+W,CAAlB,CAHS,CAlCU,CA0ChB30B,CAFPA,CAAAq0B,IAEOr0B,CAFE4pB,CAEF5pB,CADPA,CAAAqC,MACOrC,CADIqC,CACJrC,CAAAA,CAzFqD,CA1C4B,IACxFo0B;AAAoBnI,CAAAhyB,OADoE,CAExFq6B,EAAkBpI,CAAAjyB,OAiJtBskB,EAAA0N,YAAA,CAA2B2I,QAAQ,EAAG,CACpC,MAAO3I,EAD6B,CAgBtC1N,EAAA2N,UAAA,CAAyB2I,QAAQ,EAAG,CAClC,MAAO3I,EAD2B,CAIpC,OAAO3N,EAvKqF,CAAlF,CAzCkB,CAoNhC9T,QAASA,GAAiB,EAAG,CAC3B,IAAAmJ,KAAA,CAAY,CAAC,YAAD,CAAe,SAAf,CAA0B,IAA1B,CACP,QAAQ,CAAC4C,CAAD,CAAeF,CAAf,CAA0B6X,CAA1B,CAA8B,CA+HzCrW,QAASA,EAAQ,CAAC9X,CAAD,CAAKib,CAAL,CAAY6Z,CAAZ,CAAmBC,CAAnB,CAAgC,CAAA,IAC3Cx3B,EAAc+Y,CAAA/Y,YAD6B,CAE3Cy3B,EAAgB1e,CAAA0e,cAF2B,CAG3CjE,EAAW5C,CAAApT,MAAA,EAHgC,CAI3CgV,EAAUgB,CAAAhB,QAJiC,CAK3CkF,EAAY,CAL+B,CAM3CC,EAAan4B,CAAA,CAAUg4B,CAAV,CAAbG,EAAuC,CAACH,CAE5CD,EAAA,CAAQ/3B,CAAA,CAAU+3B,CAAV,CAAA,CAAmBA,CAAnB,CAA2B,CAEnC/E,EAAAD,KAAA,CAAa,IAAb,CAAmB,IAAnB,CAAyB9vB,CAAzB,CAEA+vB,EAAAoF,aAAA,CAAuB53B,CAAA,CAAY63B,QAAa,EAAG,CACjDrE,CAAAsE,OAAA,CAAgBJ,CAAA,EAAhB,CAEY,EAAZ,CAAIH,CAAJ,EAAiBG,CAAjB,EAA8BH,CAA9B,GACE/D,CAAAC,QAAA,CAAiBiE,CAAjB,CAEA,CADAD,CAAA,CAAcjF,CAAAoF,aAAd,CACA,CAAA,OAAOG,CAAA,CAAUvF,CAAAoF,aAAV,CAHT,CAMKD,EAAL,EAAgB1e,CAAAtS,OAAA,EATiC,CAA5B,CAWpB+W,CAXoB,CAavBqa,EAAA,CAAUvF,CAAAoF,aAAV,CAAA,CAAkCpE,CAElC,OAAOhB,EA3BwC,CA9HjD,IAAIuF,EAAY,EAuKhBxd,EAAAqD,OAAA,CAAkBoa,QAAQ,CAACxF,CAAD,CAAU,CAClC,MAAIA,EAAJ,EAAeA,CAAAoF,aAAf;AAAuCG,CAAvC,EACEA,CAAA,CAAUvF,CAAAoF,aAAV,CAAA3G,OAAA,CAAuC,UAAvC,CAGO,CAFPwG,aAAA,CAAcjF,CAAAoF,aAAd,CAEO,CADP,OAAOG,CAAA,CAAUvF,CAAAoF,aAAV,CACA,CAAA,CAAA,CAJT,EAMO,CAAA,CAP2B,CAUpC,OAAOrd,EAlLkC,CAD/B,CADe,CAkM7BzQ,QAASA,GAAe,EAAE,CACxB,IAAAuM,KAAA,CAAY2H,QAAQ,EAAG,CACrB,MAAO,IACD,OADC,gBAGW,aACD,GADC,WAEH,GAFG,UAGJ,CACR,QACU,CADV,SAEW,CAFX,SAGW,CAHX,QAIU,EAJV,QAKU,EALV,QAMU,GANV,QAOU,EAPV,OAQS,CART,QASU,CATV,CADQ,CAWN,QACQ,CADR,SAES,CAFT,SAGS,CAHT,QAIQ,QAJR,QAKQ,EALR,QAMQ,SANR,QAOQ,GAPR,OAQO,CARP,QASQ,CATR,CAXM,CAHI,cA0BA,GA1BA,CAHX,kBAgCa,OAEZ,uFAAA,MAAA,CAAA,GAAA,CAFY;WAIH,iDAAA,MAAA,CAAA,GAAA,CAJG,KAKX,0DAAA,MAAA,CAAA,GAAA,CALW,UAMN,6BAAA,MAAA,CAAA,GAAA,CANM,OAOT,CAAC,IAAD,CAAM,IAAN,CAPS,QAQR,oBARQ,CAShBia,OATgB,CAST,eATS,UAUN,iBAVM,UAWN,WAXM,YAYJ,UAZI,WAaL,QAbK,YAcJ,WAdI,WAeL,QAfK,CAhCb,WAkDMC,QAAQ,CAACC,CAAD,CAAM,CACvB,MAAY,EAAZ,GAAIA,CAAJ,CACS,KADT,CAGO,OAJgB,CAlDpB,CADc,CADC,CAyE1BC,QAASA,GAAU,CAACnwB,CAAD,CAAO,CACpBowB,CAAAA,CAAWpwB,CAAArD,MAAA,CAAW,GAAX,CAGf,KAHA,IACIlH,EAAI26B,CAAA37B,OAER,CAAOgB,CAAA,EAAP,CAAA,CACE26B,CAAA,CAAS36B,CAAT,CAAA;AAAcuH,EAAA,CAAiBozB,CAAA,CAAS36B,CAAT,CAAjB,CAGhB,OAAO26B,EAAAl6B,KAAA,CAAc,GAAd,CARiB,CAW1Bm6B,QAASA,GAAgB,CAACC,CAAD,CAAcC,CAAd,CAA2BC,CAA3B,CAAoC,CACvDC,CAAAA,CAAY7C,EAAA,CAAW0C,CAAX,CAAwBE,CAAxB,CAEhBD,EAAAG,WAAA,CAAyBD,CAAA5C,SACzB0C,EAAAI,OAAA,CAAqBF,CAAAG,SACrBL,EAAAM,OAAA,CAAqBj6B,CAAA,CAAI65B,CAAAK,KAAJ,CAArB,EAA4CC,EAAA,CAAcN,CAAA5C,SAAd,CAA5C,EAAiF,IALtB,CAS7DmD,QAASA,GAAW,CAACC,CAAD,CAAcV,CAAd,CAA2BC,CAA3B,CAAoC,CACtD,IAAIU,EAAsC,GAAtCA,GAAYD,CAAAt3B,OAAA,CAAmB,CAAnB,CACZu3B,EAAJ,GACED,CADF,CACgB,GADhB,CACsBA,CADtB,CAGI70B,EAAAA,CAAQwxB,EAAA,CAAWqD,CAAX,CAAwBT,CAAxB,CACZD,EAAAY,OAAA,CAAqB50B,kBAAA,CAAmB20B,CAAA,EAAyC,GAAzC,GAAY90B,CAAAg1B,SAAAz3B,OAAA,CAAsB,CAAtB,CAAZ,CACpCyC,CAAAg1B,SAAA9b,UAAA,CAAyB,CAAzB,CADoC,CACNlZ,CAAAg1B,SADb,CAErBb,EAAAc,SAAA,CAAuB70B,EAAA,CAAcJ,CAAAk1B,OAAd,CACvBf,EAAAgB,OAAA,CAAqBh1B,kBAAA,CAAmBH,CAAA+U,KAAnB,CAGjBof,EAAAY,OAAJ,EAA0D,GAA1D,EAA0BZ,CAAAY,OAAAx3B,OAAA,CAA0B,CAA1B,CAA1B,GACE42B,CAAAY,OADF,CACuB,GADvB,CAC6BZ,CAAAY,OAD7B,CAZsD,CAyBxDK,QAASA,GAAU,CAACC,CAAD,CAAQC,CAAR,CAAe,CAChC,GAA6B,CAA7B,GAAIA,CAAAj5B,QAAA,CAAcg5B,CAAd,CAAJ,CACE,MAAOC,EAAA5U,OAAA,CAAa2U,CAAAh9B,OAAb,CAFuB,CAOlCk9B,QAASA,GAAS,CAAC5e,CAAD,CAAM,CACtB,IAAIjd;AAAQid,CAAAta,QAAA,CAAY,GAAZ,CACZ,OAAiB,EAAV,EAAA3C,CAAA,CAAcid,CAAd,CAAoBA,CAAA+J,OAAA,CAAW,CAAX,CAAchnB,CAAd,CAFL,CAMxB87B,QAASA,GAAS,CAAC7e,CAAD,CAAM,CACtB,MAAOA,EAAA+J,OAAA,CAAW,CAAX,CAAc6U,EAAA,CAAU5e,CAAV,CAAA8e,YAAA,CAA2B,GAA3B,CAAd,CAAgD,CAAhD,CADe,CAkBxBC,QAASA,GAAgB,CAACtB,CAAD,CAAUuB,CAAV,CAAsB,CAC7C,IAAAC,QAAA,CAAe,CAAA,CACfD,EAAA,CAAaA,CAAb,EAA2B,EAC3B,KAAIE,EAAgBL,EAAA,CAAUpB,CAAV,CACpBH,GAAA,CAAiBG,CAAjB,CAA0B,IAA1B,CAAgCA,CAAhC,CAQA,KAAA0B,QAAA,CAAeC,QAAQ,CAACpf,CAAD,CAAM,CAC3B,IAAIqf,EAAUZ,EAAA,CAAWS,CAAX,CAA0Blf,CAA1B,CACd,IAAI,CAACpe,CAAA,CAASy9B,CAAT,CAAL,CACE,KAAMC,GAAA,CAAgB,UAAhB,CAA6Etf,CAA7E,CACFkf,CADE,CAAN,CAIFjB,EAAA,CAAYoB,CAAZ,CAAqB,IAArB,CAA2B5B,CAA3B,CAEK,KAAAW,OAAL,GACE,IAAAA,OADF,CACgB,GADhB,CAIA,KAAAmB,UAAA,EAb2B,CAoB7B,KAAAA,UAAA,CAAiBC,QAAQ,EAAG,CAAA,IACtBjB,EAAS10B,EAAA,CAAW,IAAAy0B,SAAX,CADa,CAEtBlgB,EAAO,IAAAogB,OAAA,CAAc,GAAd,CAAoBv0B,EAAA,CAAiB,IAAAu0B,OAAjB,CAApB,CAAoD,EAE/D,KAAAiB,MAAA,CAAarC,EAAA,CAAW,IAAAgB,OAAX,CAAb,EAAwCG,CAAA,CAAS,GAAT,CAAeA,CAAf,CAAwB,EAAhE,EAAsEngB,CACtE,KAAAshB,SAAA,CAAgBR,CAAhB,CAAgC,IAAAO,MAAA1V,OAAA,CAAkB,CAAlB,CALN,CAQ5B,KAAA4V,UAAA,CAAiBC,QAAQ,CAAC5f,CAAD,CAAM,CAAA,IACzB6f,CAEJ;IAAMA,CAAN,CAAepB,EAAA,CAAWhB,CAAX,CAAoBzd,CAApB,CAAf,IAA6C3e,CAA7C,CAEE,MADAy+B,EACA,CADaD,CACb,CAAA,CAAMA,CAAN,CAAepB,EAAA,CAAWO,CAAX,CAAuBa,CAAvB,CAAf,IAAmDx+B,CAAnD,CACS69B,CADT,EAC0BT,EAAA,CAAW,GAAX,CAAgBoB,CAAhB,CAD1B,EACqDA,CADrD,EAGSpC,CAHT,CAGmBqC,CAEd,KAAMD,CAAN,CAAepB,EAAA,CAAWS,CAAX,CAA0Blf,CAA1B,CAAf,IAAmD3e,CAAnD,CACL,MAAO69B,EAAP,CAAuBW,CAClB,IAAIX,CAAJ,EAAqBlf,CAArB,CAA2B,GAA3B,CACL,MAAOkf,EAboB,CAxCc,CAoE/Ca,QAASA,GAAmB,CAACtC,CAAD,CAAUuC,CAAV,CAAsB,CAChD,IAAId,EAAgBL,EAAA,CAAUpB,CAAV,CAEpBH,GAAA,CAAiBG,CAAjB,CAA0B,IAA1B,CAAgCA,CAAhC,CAQA,KAAA0B,QAAA,CAAeC,QAAQ,CAACpf,CAAD,CAAM,CAC3B,IAAIigB,EAAiBxB,EAAA,CAAWhB,CAAX,CAAoBzd,CAApB,CAAjBigB,EAA6CxB,EAAA,CAAWS,CAAX,CAA0Blf,CAA1B,CAAjD,CACIkgB,EAA6C,GAC5B,EADAD,CAAAr5B,OAAA,CAAsB,CAAtB,CACA,CAAf63B,EAAA,CAAWuB,CAAX,CAAuBC,CAAvB,CAAe,CACd,IAAAhB,QACD,CAAEgB,CAAF,CACE,EAER,IAAI,CAACr+B,CAAA,CAASs+B,CAAT,CAAL,CACE,KAAMZ,GAAA,CAAgB,UAAhB,CAA6Etf,CAA7E,CACFggB,CADE,CAAN,CAGF/B,EAAA,CAAYiC,CAAZ,CAA4B,IAA5B,CAAkCzC,CAAlC,CAEqCW,EAAAA,CAAAA,IAAAA,OAoBnC,KAAI+B,EAAqB,iBAKC,EAA1B,GAAIngB,CAAAta,QAAA,CAzB4D+3B,CAyB5D,CAAJ,GACEzd,CADF,CACQA,CAAA1W,QAAA,CA1BwDm0B,CA0BxD,CAAkB,EAAlB,CADR,CAKI0C,EAAAr1B,KAAA,CAAwBkV,CAAxB,CAAJ,GAKA,CALA,CAKO,CADPogB,CACO,CADiBD,CAAAr1B,KAAA,CAAwBmC,CAAxB,CACjB,EAAwBmzB,CAAA,CAAsB,CAAtB,CAAxB,CAAmDnzB,CAL1D,CA9BF,KAAAmxB,OAAA,CAAc,CAEd,KAAAmB,UAAA,EAhB2B,CAyD7B,KAAAA,UAAA,CAAiBC,QAAQ,EAAG,CAAA,IACtBjB,EAAS10B,EAAA,CAAW,IAAAy0B,SAAX,CADa,CAEtBlgB,EAAO,IAAAogB,OAAA;AAAc,GAAd,CAAoBv0B,EAAA,CAAiB,IAAAu0B,OAAjB,CAApB,CAAoD,EAE/D,KAAAiB,MAAA,CAAarC,EAAA,CAAW,IAAAgB,OAAX,CAAb,EAAwCG,CAAA,CAAS,GAAT,CAAeA,CAAf,CAAwB,EAAhE,EAAsEngB,CACtE,KAAAshB,SAAA,CAAgBjC,CAAhB,EAA2B,IAAAgC,MAAA,CAAaO,CAAb,CAA0B,IAAAP,MAA1B,CAAuC,EAAlE,CAL0B,CAQ5B,KAAAE,UAAA,CAAiBC,QAAQ,CAAC5f,CAAD,CAAM,CAC7B,GAAG4e,EAAA,CAAUnB,CAAV,CAAH,EAAyBmB,EAAA,CAAU5e,CAAV,CAAzB,CACE,MAAOA,EAFoB,CA5EiB,CA6FlDqgB,QAASA,GAA0B,CAAC5C,CAAD,CAAUuC,CAAV,CAAsB,CACvD,IAAAf,QAAA,CAAe,CAAA,CACfc,GAAAl4B,MAAA,CAA0B,IAA1B,CAAgCjE,SAAhC,CAEA,KAAIs7B,EAAgBL,EAAA,CAAUpB,CAAV,CAEpB,KAAAkC,UAAA,CAAiBC,QAAQ,CAAC5f,CAAD,CAAM,CAC7B,IAAI6f,CAEJ,IAAKpC,CAAL,EAAgBmB,EAAA,CAAU5e,CAAV,CAAhB,CACE,MAAOA,EACF,IAAM6f,CAAN,CAAepB,EAAA,CAAWS,CAAX,CAA0Blf,CAA1B,CAAf,CACL,MAAOyd,EAAP,CAAiBuC,CAAjB,CAA8BH,CACzB,IAAKX,CAAL,GAAuBlf,CAAvB,CAA6B,GAA7B,CACL,MAAOkf,EARoB,CAY/B,KAAAK,UAAA,CAAiBC,QAAQ,EAAG,CAAA,IACtBjB,EAAS10B,EAAA,CAAW,IAAAy0B,SAAX,CADa,CAEtBlgB,EAAO,IAAAogB,OAAA,CAAc,GAAd,CAAoBv0B,EAAA,CAAiB,IAAAu0B,OAAjB,CAApB,CAAoD,EAE/D,KAAAiB,MAAA,CAAarC,EAAA,CAAW,IAAAgB,OAAX,CAAb,EAAwCG,CAAA,CAAS,GAAT,CAAeA,CAAf,CAAwB,EAAhE,EAAsEngB,CAEtE,KAAAshB,SAAA,CAAgBjC,CAAhB,CAA0BuC,CAA1B,CAAuC,IAAAP,MANb,CAlB2B,CAsPzDa,QAASA,GAAc,CAACC,CAAD,CAAW,CAChC,MAAO,SAAQ,EAAG,CAChB,MAAO,KAAA,CAAKA,CAAL,CADS,CADc,CA/wSK;AAsxSvCC,QAASA,GAAoB,CAACD,CAAD,CAAWE,CAAX,CAAuB,CAClD,MAAO,SAAQ,CAAC59B,CAAD,CAAQ,CACrB,GAAI0B,CAAA,CAAY1B,CAAZ,CAAJ,CACE,MAAO,KAAA,CAAK09B,CAAL,CAET,KAAA,CAAKA,CAAL,CAAA,CAAiBE,CAAA,CAAW59B,CAAX,CACjB,KAAA08B,UAAA,EAEA,OAAO,KAPc,CAD2B,CA6CpDltB,QAASA,GAAiB,EAAE,CAAA,IACtB2tB,EAAa,EADS,CAEtBU,EAAY,CAAA,CAShB,KAAAV,WAAA,CAAkBW,QAAQ,CAACC,CAAD,CAAS,CACjC,MAAIp8B,EAAA,CAAUo8B,CAAV,CAAJ,EACEZ,CACO,CADMY,CACN,CAAA,IAFT,EAISZ,CALwB,CAgBnC,KAAAU,UAAA,CAAiBG,QAAQ,CAACpU,CAAD,CAAO,CAC9B,MAAIjoB,EAAA,CAAUioB,CAAV,CAAJ,EACEiU,CACO,CADKjU,CACL,CAAA,IAFT,EAISiU,CALqB,CAoChC,KAAArlB,KAAA,CAAY,CAAC,YAAD,CAAe,UAAf,CAA2B,UAA3B,CAAuC,cAAvC,CACR,QAAQ,CAAE4C,CAAF,CAAgB0X,CAAhB,CAA4B3W,CAA5B,CAAwC0I,CAAxC,CAAsD,CAwIhEoZ,QAASA,EAAmB,CAACC,CAAD,CAAS,CACnC9iB,CAAA+iB,WAAA,CAAsB,wBAAtB,CAAgDhjB,CAAAijB,OAAA,EAAhD,CAAoEF,CAApE,CADmC,CAxI2B,IAC5D/iB,CAD4D,CAE5DkjB,CAF4D,CAG5Dxf,EAAWiU,CAAAjU,SAAA,EAHiD,CAI5Dyf,EAAaxL,CAAA3V,IAAA,EAJ+C,CAK5Dyd,CAEAiD,EAAJ,EACEjD,CACA,CADqB0D,CA/hBlB5e,UAAA,CAAc,CAAd,CA+hBkB4e,CA/hBDz7B,QAAA,CAAY,GAAZ,CA+hBCy7B,CA/hBgBz7B,QAAA,CAAY,IAAZ,CAAjB,CAAqC,CAArC,CAAjB,CAgiBH,EADoCgc,CACpC,EADgD,GAChD,EAAAwf,CAAA,CAAeliB,CAAAoB,QAAA,CAAmB2e,EAAnB,CAAsCsB,EAFvD,GAIE5C,CACA;AADUmB,EAAA,CAAUuC,CAAV,CACV,CAAAD,CAAA,CAAenB,EALjB,CAOA/hB,EAAA,CAAY,IAAIkjB,CAAJ,CAAiBzD,CAAjB,CAA0B,GAA1B,CAAgCuC,CAAhC,CACZhiB,EAAAmhB,QAAA,CAAkBnhB,CAAA2hB,UAAA,CAAoBwB,CAApB,CAAlB,CAEAzZ,EAAAlG,GAAA,CAAgB,OAAhB,CAAyB,QAAQ,CAACxI,CAAD,CAAQ,CAIvC,GAAIooB,CAAApoB,CAAAooB,QAAJ,EAAqBC,CAAAroB,CAAAqoB,QAArB,EAAqD,CAArD,EAAsCroB,CAAAsoB,MAAtC,CAAA,CAKA,IAHA,IAAIjjB,EAAMxV,CAAA,CAAOmQ,CAAAO,OAAP,CAGV,CAAsC,GAAtC,GAAO7Q,CAAA,CAAU2V,CAAA,CAAI,CAAJ,CAAAlZ,SAAV,CAAP,CAAA,CAEE,GAAIkZ,CAAA,CAAI,CAAJ,CAAJ,GAAeqJ,CAAA,CAAa,CAAb,CAAf,EAAkC,CAAC,CAACrJ,CAAD,CAAOA,CAAApa,OAAA,EAAP,EAAqB,CAArB,CAAnC,CAA4D,MAG9D,KAAIs9B,EAAUljB,CAAAjZ,KAAA,CAAS,MAAT,CAEVX,EAAA,CAAS88B,CAAT,CAAJ,EAAgD,4BAAhD,GAAyBA,CAAA38B,SAAA,EAAzB,GAGE28B,CAHF,CAGY1G,EAAA,CAAW0G,CAAAC,QAAX,CAAAxgB,KAHZ,CASA,IAAIkgB,CAAJ,GAAqBb,EAArB,CAAiD,CAG/C,IAAIrf,EAAO3C,CAAAhZ,KAAA,CAAS,MAAT,CAAP2b,EAA2B3C,CAAAhZ,KAAA,CAAS,YAAT,CAE/B,IAA0B,CAA1B,CAAI2b,CAAAtb,QAAA,CAAa,KAAb,CAAJ,CAEE,GADIk7B,CACA,CADS,GACT,CADeZ,CACf,CAAW,GAAX,EAAAhf,CAAA,CAAK,CAAL,CAAJ,CAEEugB,CAAA,CAAU9D,CAAV,CAAoBmD,CAApB,CAA6B5f,CAF/B,KAGO,IAAe,GAAf,EAAIA,CAAA,CAAK,CAAL,CAAJ,CAELugB,CAAA,CAAU9D,CAAV,CAAoBmD,CAApB,EAA8B5iB,CAAA/Q,KAAA,EAA9B,EAAkD,GAAlD,EAAyD+T,CAFpD,KAGA,CAIL,IAJK,IAED/E,EAAQ+B,CAAA/Q,KAAA,EAAArD,MAAA,CAAuB,GAAvB,CAFP,CAGHE,EAAQkX,CAAApX,MAAA,CAAW,GAAX,CAHL,CAIIlH;AAAE,CAAX,CAAcA,CAAd,CAAgBoH,CAAApI,OAAhB,CAA8BgB,CAAA,EAA9B,CACkB,GAAhB,EAAIoH,CAAA,CAAMpH,CAAN,CAAJ,GAEqB,IAAhB,EAAIoH,CAAA,CAAMpH,CAAN,CAAJ,CACHuZ,CAAAmD,IAAA,EADG,CAEItV,CAAA,CAAMpH,CAAN,CAAAhB,OAFJ,EAGHua,CAAA1Z,KAAA,CAAWuH,CAAA,CAAMpH,CAAN,CAAX,CALF,CAOF6+B,EAAA,CAAU9D,CAAV,CAAoBmD,CAApB,CAA6B3kB,CAAA9Y,KAAA,CAAW,GAAX,CAZxB,CAbsC,CA8B7Cs+B,CAAAA,CAAezjB,CAAA2hB,UAAA,CAAoB4B,CAApB,CAEfA,EAAJ,GAAgB,CAAAljB,CAAAhZ,KAAA,CAAS,QAAT,CAAhB,EAAsCo8B,CAAtC,EAAuD,CAAAzoB,CAAAW,mBAAA,EAAvD,IACEX,CAAAC,eAAA,EACA,CAAIwoB,CAAJ,EAAoB9L,CAAA3V,IAAA,EAApB,GAEEhC,CAAAmhB,QAAA,CAAkBsC,CAAlB,CAGA,CAFAxjB,CAAAtS,OAAA,EAEA,CAAAxK,CAAA4K,QAAA,CAAe,0BAAf,CAAA,CAA6C,CAAA,CAL/C,CAFF,CArDA,CAJuC,CAAzC,CAuEIiS,EAAAijB,OAAA,EAAJ,EAA0BE,CAA1B,EACExL,CAAA3V,IAAA,CAAahC,CAAAijB,OAAA,EAAb,CAAiC,CAAA,CAAjC,CAIFtL,EAAArU,YAAA,CAAqB,QAAQ,CAACogB,CAAD,CAAS,CAChC1jB,CAAAijB,OAAA,EAAJ,EAA0BS,CAA1B,GACEzjB,CAAA9X,WAAA,CAAsB,QAAQ,EAAG,CAC/B,IAAI46B,EAAS/iB,CAAAijB,OAAA,EAEbjjB,EAAAmhB,QAAA,CAAkBuC,CAAlB,CACIzjB,EAAA+iB,WAAA,CAAsB,sBAAtB,CAA8CU,CAA9C,CACsBX,CADtB,CAAAtnB,iBAAJ,EAEEuE,CAAAmhB,QAAA,CAAkB4B,CAAlB,CACA,CAAApL,CAAA3V,IAAA,CAAa+gB,CAAb,CAHF,EAKED,CAAA,CAAoBC,CAApB,CAT6B,CAAjC,CAYA,CAAK9iB,CAAAsa,QAAL,EAAyBta,CAAA0jB,QAAA,EAb3B,CADoC,CAAtC,CAmBA;IAAIC,EAAgB,CACpB3jB,EAAA7X,OAAA,CAAkBy7B,QAAuB,EAAG,CAC1C,IAAId,EAASpL,CAAA3V,IAAA,EAAb,CACI8hB,EAAiB9jB,CAAA+jB,UAEhBH,EAAL,EAAsBb,CAAtB,EAAgC/iB,CAAAijB,OAAA,EAAhC,GACEW,CAAA,EACA,CAAA3jB,CAAA9X,WAAA,CAAsB,QAAQ,EAAG,CAC3B8X,CAAA+iB,WAAA,CAAsB,sBAAtB,CAA8ChjB,CAAAijB,OAAA,EAA9C,CAAkEF,CAAlE,CAAAtnB,iBAAJ,CAEEuE,CAAAmhB,QAAA,CAAkB4B,CAAlB,CAFF,EAIEpL,CAAA3V,IAAA,CAAahC,CAAAijB,OAAA,EAAb,CAAiCa,CAAjC,CACA,CAAAhB,CAAA,CAAoBC,CAApB,CALF,CAD+B,CAAjC,CAFF,CAYA/iB,EAAA+jB,UAAA,CAAsB,CAAA,CAEtB,OAAOH,EAlBmC,CAA5C,CAqBA,OAAO5jB,EAtIyD,CADtD,CA/Dc,CAwP5B1L,QAASA,GAAY,EAAE,CAAA,IACjB0vB,EAAQ,CAAA,CADS,CAEjBx6B,EAAO,IASX,KAAAy6B,aAAA,CAAoBC,QAAQ,CAACC,CAAD,CAAO,CACjC,MAAI39B,EAAA,CAAU29B,CAAV,CAAJ,EACEH,CACK,CADGG,CACH,CAAA,IAFP,EAISH,CALwB,CASnC,KAAA3mB,KAAA,CAAY,CAAC,SAAD,CAAY,QAAQ,CAAC0C,CAAD,CAAS,CAwDvCqkB,QAASA,EAAW,CAAC11B,CAAD,CAAM,CACpBA,CAAJ,WAAmB21B,MAAnB,GACM31B,CAAAuP,MAAJ,CACEvP,CADF,CACSA,CAAAsP,QACD,EADoD,EACpD,GADgBtP,CAAAuP,MAAAvW,QAAA,CAAkBgH,CAAAsP,QAAlB,CAChB,CAAA,SAAA,CAAYtP,CAAAsP,QAAZ,CAA0B,IAA1B,CAAiCtP,CAAAuP,MAAjC,CACAvP,CAAAuP,MAHR,CAIWvP,CAAA41B,UAJX;CAKE51B,CALF,CAKQA,CAAAsP,QALR,CAKsB,IALtB,CAK6BtP,CAAA41B,UAL7B,CAK6C,GAL7C,CAKmD51B,CAAAioB,KALnD,CADF,CASA,OAAOjoB,EAViB,CAa1B61B,QAASA,EAAU,CAAC7rB,CAAD,CAAO,CAAA,IACpB8rB,EAAUzkB,CAAAykB,QAAVA,EAA6B,EADT,CAEpBC,EAAQD,CAAA,CAAQ9rB,CAAR,CAAR+rB,EAAyBD,CAAAE,IAAzBD,EAAwCt+B,CACxCw+B,EAAAA,CAAW,CAAA,CAIf,IAAI,CACFA,CAAA,CAAW,CAAC,CAACF,CAAA56B,MADX,CAEF,MAAOmB,CAAP,CAAU,EAEZ,MAAI25B,EAAJ,CACS,QAAQ,EAAG,CAChB,IAAIlmB,EAAO,EACX3a,EAAA,CAAQ8B,SAAR,CAAmB,QAAQ,CAAC8I,CAAD,CAAM,CAC/B+P,CAAAla,KAAA,CAAU6/B,CAAA,CAAY11B,CAAZ,CAAV,CAD+B,CAAjC,CAGA,OAAO+1B,EAAA56B,MAAA,CAAY26B,CAAZ,CAAqB/lB,CAArB,CALS,CADpB,CAYO,QAAQ,CAACmmB,CAAD,CAAOC,CAAP,CAAa,CAC1BJ,CAAA,CAAMG,CAAN,CAAoB,IAAR,EAAAC,CAAA,CAAe,EAAf,CAAoBA,CAAhC,CAD0B,CAvBJ,CApE1B,MAAO,KAQAN,CAAA,CAAW,KAAX,CARA,MAiBCA,CAAA,CAAW,MAAX,CAjBD,MA0BCA,CAAA,CAAW,MAAX,CA1BD,OAmCEA,CAAA,CAAW,OAAX,CAnCF,OA4CG,QAAS,EAAG,CAClB,IAAI96B,EAAK86B,CAAA,CAAW,OAAX,CAET,OAAO,SAAQ,EAAG,CACZP,CAAJ,EACEv6B,CAAAI,MAAA,CAASL,CAAT,CAAe5D,SAAf,CAFc,CAHA,CAAZ,EA5CH,CADgC,CAA7B,CApBS,CAwJvBk/B,QAASA,GAAoB,CAACn4B,CAAD,CAAOo4B,CAAP,CAAuB,CAClD,GAAa,aAAb,GAAIp4B,CAAJ,CACE,KAAMq4B,GAAA,CAAa,SAAb,CAEFD,CAFE,CAAN,CAIF,MAAOp4B,EAN2C,CASpDs4B,QAASA,GAAgB,CAACzhC,CAAD,CAAMuhC,CAAN,CAAsB,CAE7C,GAAIvhC,CAAJ,CAAS,CACP,GAAIA,CAAAsL,YAAJ;AAAwBtL,CAAxB,CACE,KAAMwhC,GAAA,CAAa,QAAb,CAEFD,CAFE,CAAN,CAGK,GACHvhC,CAAAJ,SADG,EACaI,CAAAsD,SADb,EAC6BtD,CAAAuD,MAD7B,EAC0CvD,CAAAwD,YAD1C,CAEL,KAAMg+B,GAAA,CAAa,YAAb,CAEFD,CAFE,CAAN,CAGK,GACHvhC,CAAA4S,SADG,GACc5S,CAAA2D,SADd,EAC+B3D,CAAA4D,KAD/B,EAC2C5D,CAAA6D,KAD3C,EACuD7D,CAAA8D,KADvD,EAEL,KAAM09B,GAAA,CAAa,SAAb,CAEFD,CAFE,CAAN,CAZK,CAiBT,MAAOvhC,EAnBsC,CA4wB/C0hC,QAASA,GAAM,CAAC1hC,CAAD,CAAMyL,CAAN,CAAYk2B,CAAZ,CAAsBC,CAAtB,CAA+BjgB,CAA/B,CAAwC,CAErDA,CAAA,CAAUA,CAAV,EAAqB,EAEjBva,EAAAA,CAAUqE,CAAArD,MAAA,CAAW,GAAX,CACd,KADA,IAA+B3H,CAA/B,CACSS,EAAI,CAAb,CAAiC,CAAjC,CAAgBkG,CAAAlH,OAAhB,CAAoCgB,CAAA,EAApC,CAAyC,CACvCT,CAAA,CAAM6gC,EAAA,CAAqBl6B,CAAAyL,MAAA,EAArB,CAAsC+uB,CAAtC,CACN,KAAIC,EAAc7hC,CAAA,CAAIS,CAAJ,CACbohC,EAAL,GACEA,CACA,CADc,EACd,CAAA7hC,CAAA,CAAIS,CAAJ,CAAA,CAAWohC,CAFb,CAIA7hC,EAAA,CAAM6hC,CACF7hC,EAAA+1B,KAAJ,EAAgBpU,CAAAmgB,eAAhB,GACEC,EAAA,CAAeH,CAAf,CASA,CARM,KAQN,EARe5hC,EAQf,EAPG,QAAQ,CAACg2B,CAAD,CAAU,CACjBA,CAAAD,KAAA,CAAa,QAAQ,CAACvvB,CAAD,CAAM,CAAEwvB,CAAAgM,IAAA,CAAcx7B,CAAhB,CAA3B,CADiB,CAAlB,CAECxG,CAFD,CAOH,CAHIA,CAAAgiC,IAGJ,GAHgBniC,CAGhB,GAFEG,CAAAgiC,IAEF,CAFY,EAEZ,EAAAhiC,CAAA,CAAMA,CAAAgiC,IAVR,CARuC,CAqBzCvhC,CAAA,CAAM6gC,EAAA,CAAqBl6B,CAAAyL,MAAA,EAArB,CAAsC+uB,CAAtC,CAEN,OADA5hC,EAAA,CAAIS,CAAJ,CACA,CADWkhC,CA3B0C,CAsCvDM,QAASA,GAAe,CAACC,CAAD,CAAOC,CAAP,CAAaC,CAAb,CAAmBC,CAAnB,CAAyBC,CAAzB,CAA+BV,CAA/B,CAAwCjgB,CAAxC,CAAiD,CACvE2f,EAAA,CAAqBY,CAArB,CAA2BN,CAA3B,CACAN,GAAA,CAAqBa,CAArB,CAA2BP,CAA3B,CACAN;EAAA,CAAqBc,CAArB,CAA2BR,CAA3B,CACAN,GAAA,CAAqBe,CAArB,CAA2BT,CAA3B,CACAN,GAAA,CAAqBgB,CAArB,CAA2BV,CAA3B,CAEA,OAAQjgB,EAAAmgB,eACD,CAwBDS,QAAoC,CAACv4B,CAAD,CAAQgR,CAAR,CAAgB,CAAA,IAC9CwnB,EAAWxnB,CAAD,EAAWA,CAAAra,eAAA,CAAsBuhC,CAAtB,CAAX,CAA0ClnB,CAA1C,CAAmDhR,CADf,CAE9CgsB,CAEJ,IAAe,IAAf,EAAIwM,CAAJ,CAAqB,MAAOA,EAG5B,EADAA,CACA,CADUA,CAAA,CAAQN,CAAR,CACV,GAAeM,CAAAzM,KAAf,GACEgM,EAAA,CAAeH,CAAf,CAMA,CALM,KAKN,EALeY,EAKf,GAJExM,CAEA,CAFUwM,CAEV,CADAxM,CAAAgM,IACA,CADcniC,CACd,CAAAm2B,CAAAD,KAAA,CAAa,QAAQ,CAACvvB,CAAD,CAAM,CAAEwvB,CAAAgM,IAAA,CAAcx7B,CAAhB,CAA3B,CAEF,EAAAg8B,CAAA,CAAUA,CAAAR,IAPZ,CAUA,IAAI,CAACG,CAAL,CAAW,MAAOK,EAClB,IAAe,IAAf,EAAIA,CAAJ,CAAqB,MAAO3iC,EAE5B,EADA2iC,CACA,CADUA,CAAA,CAAQL,CAAR,CACV,GAAeK,CAAAzM,KAAf,GACEgM,EAAA,CAAeH,CAAf,CAMA,CALM,KAKN,EALeY,EAKf,GAJExM,CAEA,CAFUwM,CAEV,CADAxM,CAAAgM,IACA,CADcniC,CACd,CAAAm2B,CAAAD,KAAA,CAAa,QAAQ,CAACvvB,CAAD,CAAM,CAAEwvB,CAAAgM,IAAA,CAAcx7B,CAAhB,CAA3B,CAEF,EAAAg8B,CAAA,CAAUA,CAAAR,IAPZ,CAUA,IAAI,CAACI,CAAL,CAAW,MAAOI,EAClB,IAAe,IAAf,EAAIA,CAAJ,CAAqB,MAAO3iC,EAE5B,EADA2iC,CACA,CADUA,CAAA,CAAQJ,CAAR,CACV,GAAeI,CAAAzM,KAAf,GACEgM,EAAA,CAAeH,CAAf,CAMA,CALM,KAKN,EALeY,EAKf,GAJExM,CAEA,CAFUwM,CAEV,CADAxM,CAAAgM,IACA,CADcniC,CACd,CAAAm2B,CAAAD,KAAA,CAAa,QAAQ,CAACvvB,CAAD,CAAM,CAAEwvB,CAAAgM,IAAA,CAAcx7B,CAAhB,CAA3B,CAEF,EAAAg8B,CAAA,CAAUA,CAAAR,IAPZ,CAUA,IAAI,CAACK,CAAL,CAAW,MAAOG,EAClB,IAAe,IAAf,EAAIA,CAAJ,CAAqB,MAAO3iC,EAE5B,EADA2iC,CACA,CADUA,CAAA,CAAQH,CAAR,CACV,GAAeG,CAAAzM,KAAf;CACEgM,EAAA,CAAeH,CAAf,CAMA,CALM,KAKN,EALeY,EAKf,GAJExM,CAEA,CAFUwM,CAEV,CADAxM,CAAAgM,IACA,CADcniC,CACd,CAAAm2B,CAAAD,KAAA,CAAa,QAAQ,CAACvvB,CAAD,CAAM,CAAEwvB,CAAAgM,IAAA,CAAcx7B,CAAhB,CAA3B,CAEF,EAAAg8B,CAAA,CAAUA,CAAAR,IAPZ,CAUA,IAAI,CAACM,CAAL,CAAW,MAAOE,EAClB,IAAe,IAAf,EAAIA,CAAJ,CAAqB,MAAO3iC,EAE5B,EADA2iC,CACA,CADUA,CAAA,CAAQF,CAAR,CACV,GAAeE,CAAAzM,KAAf,GACEgM,EAAA,CAAeH,CAAf,CAMA,CALM,KAKN,EALeY,EAKf,GAJExM,CAEA,CAFUwM,CAEV,CADAxM,CAAAgM,IACA,CADcniC,CACd,CAAAm2B,CAAAD,KAAA,CAAa,QAAQ,CAACvvB,CAAD,CAAM,CAAEwvB,CAAAgM,IAAA,CAAcx7B,CAAhB,CAA3B,CAEF,EAAAg8B,CAAA,CAAUA,CAAAR,IAPZ,CASA,OAAOQ,EApE2C,CAxBnD,CAADC,QAAsB,CAACz4B,CAAD,CAAQgR,CAAR,CAAgB,CACpC,IAAIwnB,EAAWxnB,CAAD,EAAWA,CAAAra,eAAA,CAAsBuhC,CAAtB,CAAX,CAA0ClnB,CAA1C,CAAmDhR,CAEjE,IAAe,IAAf,EAAIw4B,CAAJ,CAAqB,MAAOA,EAC5BA,EAAA,CAAUA,CAAA,CAAQN,CAAR,CAEV,IAAI,CAACC,CAAL,CAAW,MAAOK,EAClB,IAAe,IAAf,EAAIA,CAAJ,CAAqB,MAAO3iC,EAC5B2iC,EAAA,CAAUA,CAAA,CAAQL,CAAR,CAEV,IAAI,CAACC,CAAL,CAAW,MAAOI,EAClB,IAAe,IAAf,EAAIA,CAAJ,CAAqB,MAAO3iC,EAC5B2iC,EAAA,CAAUA,CAAA,CAAQJ,CAAR,CAEV,IAAI,CAACC,CAAL,CAAW,MAAOG,EAClB,IAAe,IAAf,EAAIA,CAAJ,CAAqB,MAAO3iC,EAC5B2iC,EAAA,CAAUA,CAAA,CAAQH,CAAR,CAEV,OAAKC,EAAL,CACe,IAAf,EAAIE,CAAJ,CAA4B3iC,CAA5B,CACA2iC,CADA,CACUA,CAAA,CAAQF,CAAR,CAFV,CAAkBE,CAlBkB,CAR2B,CAwGzEE,QAASA,GAAe,CAACR,CAAD,CAAON,CAAP,CAAgB,CACtCN,EAAA,CAAqBY,CAArB,CAA2BN,CAA3B,CAEA,OAAOc,SAAwB,CAAC14B,CAAD,CAAQgR,CAAR,CAAgB,CAC7C,MAAa,KAAb,EAAIhR,CAAJ,CAA0BnK,CAA1B,CACO,CAAEmb,CAAD,EAAWA,CAAAra,eAAA,CAAsBuhC,CAAtB,CAAX;AAA0ClnB,CAA1C,CAAmDhR,CAApD,EAA2Dk4B,CAA3D,CAFsC,CAHT,CASxCS,QAASA,GAAe,CAACT,CAAD,CAAOC,CAAP,CAAaP,CAAb,CAAsB,CAC5CN,EAAA,CAAqBY,CAArB,CAA2BN,CAA3B,CACAN,GAAA,CAAqBa,CAArB,CAA2BP,CAA3B,CAEA,OAAOe,SAAwB,CAAC34B,CAAD,CAAQgR,CAAR,CAAgB,CAC7C,GAAa,IAAb,EAAIhR,CAAJ,CAAmB,MAAOnK,EAC1BmK,EAAA,CAAQ,CAAEgR,CAAD,EAAWA,CAAAra,eAAA,CAAsBuhC,CAAtB,CAAX,CAA0ClnB,CAA1C,CAAmDhR,CAApD,EAA2Dk4B,CAA3D,CACR,OAAgB,KAAT,EAAAl4B,CAAA,CAAgBnK,CAAhB,CAA4BmK,CAAA,CAAMm4B,CAAN,CAHU,CAJH,CAW9CS,QAASA,GAAQ,CAACn3B,CAAD,CAAOkW,CAAP,CAAgBigB,CAAhB,CAAyB,CAIxC,GAAIiB,EAAAliC,eAAA,CAA6B8K,CAA7B,CAAJ,CACE,MAAOo3B,GAAA,CAAcp3B,CAAd,CAL+B,KAQpCq3B,EAAWr3B,CAAArD,MAAA,CAAW,GAAX,CARyB,CASpC26B,EAAiBD,CAAA5iC,OATmB,CAUpC+F,CAIJ,IAAK0b,CAAAmgB,eAAL,EAAkD,CAAlD,GAA+BiB,CAA/B,CAEO,GAAKphB,CAAAmgB,eAAL,EAAkD,CAAlD,GAA+BiB,CAA/B,CAEA,GAAIphB,CAAAhc,IAAJ,CAEHM,CAAA,CADmB,CAArB,CAAI88B,CAAJ,CACOd,EAAA,CAAgBa,CAAA,CAAS,CAAT,CAAhB,CAA6BA,CAAA,CAAS,CAAT,CAA7B,CAA0CA,CAAA,CAAS,CAAT,CAA1C,CAAuDA,CAAA,CAAS,CAAT,CAAvD,CAAoEA,CAAA,CAAS,CAAT,CAApE,CAAiFlB,CAAjF,CACejgB,CADf,CADP,CAIO1b,QAAQ,CAAC+D,CAAD,CAAQgR,CAAR,CAAgB,CAAA,IACvB9Z,EAAI,CADmB,CAChBsF,CACX,GACEA,EAIA,CAJMy7B,EAAA,CAAgBa,CAAA,CAAS5hC,CAAA,EAAT,CAAhB,CAA+B4hC,CAAA,CAAS5hC,CAAA,EAAT,CAA/B,CAA8C4hC,CAAA,CAAS5hC,CAAA,EAAT,CAA9C,CAA6D4hC,CAAA,CAAS5hC,CAAA,EAAT,CAA7D,CACgB4hC,CAAA,CAAS5hC,CAAA,EAAT,CADhB,CAC+B0gC,CAD/B,CACwCjgB,CADxC,CAAA,CACiD3X,CADjD,CACwDgR,CADxD,CAIN,CADAA,CACA,CADSnb,CACT,CAAAmK,CAAA,CAAQxD,CALV,OAMStF,CANT,CAMa6hC,CANb,CAOA,OAAOv8B,EAToB,CAL1B,KAiBA,CACL,IAAI6oB,EAAO,UACX/uB,EAAA,CAAQwiC,CAAR,CAAkB,QAAQ,CAACriC,CAAD,CAAMc,CAAN,CAAa,CACrC+/B,EAAA,CAAqB7gC,CAArB,CAA0BmhC,CAA1B,CACAvS,EAAA,EAAQ,qCAAR;CACe9tB,CAEA,CAAG,GAAH,CAEG,yBAFH,CAE+Bd,CAF/B,CAEqC,UALpD,EAKkE,IALlE,CAKyEA,CALzE,CAKsF,OALtF,EAMSkhB,CAAAmgB,eACA,CAAG,2BAAH,CACaF,CAAA95B,QAAA,CAAgB,YAAhB,CAA8B,MAA9B,CADb,CAQC,4GARD,CASG,EAhBZ,CAFqC,CAAvC,CAoBA,KAAAunB,EAAAA,CAAAA,CAAQ,WAAR,CAGI2T,EAAiB,IAAIC,QAAJ,CAAa,GAAb,CAAkB,GAAlB,CAAuB,IAAvB,CAA6B5T,CAA7B,CAErB2T,EAAA5/B,SAAA,CAA0BN,EAAA,CAAQusB,CAAR,CAC1BppB,EAAA,CAAK0b,CAAAmgB,eAAA,CAAyB,QAAQ,CAAC93B,CAAD,CAAQgR,CAAR,CAAgB,CACpD,MAAOgoB,EAAA,CAAeh5B,CAAf,CAAsBgR,CAAtB,CAA8B+mB,EAA9B,CAD6C,CAAjD,CAEDiB,CA9BC,CAnBA,IACL/8B,EAAA,CAAK08B,EAAA,CAAgBG,CAAA,CAAS,CAAT,CAAhB,CAA6BA,CAAA,CAAS,CAAT,CAA7B,CAA0ClB,CAA1C,CAHP,KACE37B,EAAA,CAAKy8B,EAAA,CAAgBI,CAAA,CAAS,CAAT,CAAhB,CAA6BlB,CAA7B,CAuDM,iBAAb,GAAIn2B,CAAJ,GACEo3B,EAAA,CAAcp3B,CAAd,CADF,CACwBxF,CADxB,CAGA,OAAOA,EAzEiC,CAgI1C8K,QAASA,GAAc,EAAG,CACxB,IAAI4J,EAAQ,EAAZ,CAEIuoB,EAAgB,KACb,CAAA,CADa,gBAEF,CAAA,CAFE;mBAGE,CAAA,CAHF,CAmDpB,KAAApB,eAAA,CAAsBqB,QAAQ,CAAC9hC,CAAD,CAAQ,CACpC,MAAI2B,EAAA,CAAU3B,CAAV,CAAJ,EACE6hC,CAAApB,eACO,CADwB,CAAC,CAACzgC,CAC1B,CAAA,IAFT,EAIS6hC,CAAApB,eAL2B,CA2BvC,KAAAsB,mBAAA,CAA0BC,QAAQ,CAAChiC,CAAD,CAAQ,CACvC,MAAI2B,EAAA,CAAU3B,CAAV,CAAJ,EACE6hC,CAAAE,mBACO,CAD4B/hC,CAC5B,CAAA,IAFT,EAIS6hC,CAAAE,mBAL8B,CAUzC,KAAAvpB,KAAA,CAAY,CAAC,SAAD,CAAY,UAAZ,CAAwB,MAAxB,CAAgC,QAAQ,CAACypB,CAAD,CAAU9lB,CAAV,CAAoBD,CAApB,CAA0B,CAC5E2lB,CAAAv9B,IAAA,CAAoB6X,CAAA7X,IAEpBo8B,GAAA,CAAiBA,QAAyB,CAACH,CAAD,CAAU,CAC7CsB,CAAAE,mBAAL,EAAyC,CAAAG,EAAA5iC,eAAA,CAAmCihC,CAAnC,CAAzC,GACA2B,EAAA,CAAoB3B,CAApB,CACA,CAD+B,CAAA,CAC/B,CAAArkB,CAAAqD,KAAA,CAAU,4CAAV,CAAyDghB,CAAzD,CACI,2EADJ,CAFA,CADkD,CAOpD,OAAO,SAAQ,CAACtH,CAAD,CAAM,CACnB,IAAIkJ,CAEJ;OAAQ,MAAOlJ,EAAf,EACE,KAAK,QAAL,CAEE,GAAI3f,CAAAha,eAAA,CAAqB25B,CAArB,CAAJ,CACE,MAAO3f,EAAA,CAAM2f,CAAN,CAGLmJ,EAAAA,CAAQ,IAAIC,EAAJ,CAAUR,CAAV,CAEZM,EAAA,CAAmBz8B,CADN48B,IAAIC,EAAJD,CAAWF,CAAXE,CAAkBL,CAAlBK,CAA2BT,CAA3BS,CACM58B,OAAA,CAAauzB,CAAb,CAEP,iBAAZ,GAAIA,CAAJ,GAGE3f,CAAA,CAAM2f,CAAN,CAHF,CAGekJ,CAHf,CAMA,OAAOA,EAET,MAAK,UAAL,CACE,MAAOlJ,EAET,SACE,MAAO33B,EAvBX,CAHmB,CAVuD,CAAlE,CA3FY,CA6S1BsO,QAASA,GAAU,EAAG,CAEpB,IAAA4I,KAAA,CAAY,CAAC,YAAD,CAAe,mBAAf,CAAoC,QAAQ,CAAC4C,CAAD,CAAaoH,CAAb,CAAgC,CACtF,MAAOggB,GAAA,CAAS,QAAQ,CAACxkB,CAAD,CAAW,CACjC5C,CAAA9X,WAAA,CAAsB0a,CAAtB,CADiC,CAA5B,CAEJwE,CAFI,CAD+E,CAA5E,CAFQ,CAkBtBggB,QAASA,GAAQ,CAACC,CAAD,CAAWC,CAAX,CAA6B,CAyR5CC,QAASA,EAAe,CAAC3iC,CAAD,CAAQ,CAC9B,MAAOA,EADuB,CAKhC4iC,QAASA,EAAc,CAAC94B,CAAD,CAAS,CAC9B,MAAOspB,EAAA,CAAOtpB,CAAP,CADuB,CAlRhC,IAAI6V,EAAQA,QAAQ,EAAG,CAAA,IACjBkjB,EAAU,EADO,CAEjB7iC,CAFiB,CAEV21B,CA+HX,OA7HAA,EA6HA,CA7HW,SAEAC,QAAQ,CAACzwB,CAAD,CAAM,CACrB,GAAI09B,CAAJ,CAAa,CACX,IAAI/L,EAAY+L,CAChBA,EAAA,CAAUrkC,CACVwB,EAAA,CAAQ8iC,CAAA,CAAI39B,CAAJ,CAEJ2xB,EAAAj4B,OAAJ,EACE4jC,CAAA,CAAS,QAAQ,EAAG,CAElB,IADA,IAAIzkB,CAAJ,CACSne,EAAI,CADb,CACgB6V,EAAKohB,CAAAj4B,OAArB,CAAuCgB,CAAvC;AAA2C6V,CAA3C,CAA+C7V,CAAA,EAA/C,CACEme,CACA,CADW8Y,CAAA,CAAUj3B,CAAV,CACX,CAAAG,CAAA00B,KAAA,CAAW1W,CAAA,CAAS,CAAT,CAAX,CAAwBA,CAAA,CAAS,CAAT,CAAxB,CAAqCA,CAAA,CAAS,CAAT,CAArC,CAJgB,CAApB,CANS,CADQ,CAFd,QAqBDoV,QAAQ,CAACtpB,CAAD,CAAS,CACvB6rB,CAAAC,QAAA,CAAiBmN,CAAA,CAA8Bj5B,CAA9B,CAAjB,CADuB,CArBhB,QA0BDmwB,QAAQ,CAAC+I,CAAD,CAAW,CACzB,GAAIH,CAAJ,CAAa,CACX,IAAI/L,EAAY+L,CAEZA,EAAAhkC,OAAJ,EACE4jC,CAAA,CAAS,QAAQ,EAAG,CAElB,IADA,IAAIzkB,CAAJ,CACSne,EAAI,CADb,CACgB6V,EAAKohB,CAAAj4B,OAArB,CAAuCgB,CAAvC,CAA2C6V,CAA3C,CAA+C7V,CAAA,EAA/C,CACEme,CACA,CADW8Y,CAAA,CAAUj3B,CAAV,CACX,CAAAme,CAAA,CAAS,CAAT,CAAA,CAAYglB,CAAZ,CAJgB,CAApB,CAJS,CADY,CA1BlB,SA2CA,MACDtO,QAAQ,CAAC1W,CAAD,CAAWilB,CAAX,CAAoBC,CAApB,CAAkC,CAC9C,IAAIz/B,EAASkc,CAAA,EAAb,CAEIwjB,EAAkBA,QAAQ,CAACnjC,CAAD,CAAQ,CACpC,GAAI,CACFyD,CAAAmyB,QAAA,CAAgB,CAAAv2B,CAAA,CAAW2e,CAAX,CAAA,CAAuBA,CAAvB,CAAkC2kB,CAAlC,EAAmD3iC,CAAnD,CAAhB,CADE,CAEF,MAAMmG,CAAN,CAAS,CACT1C,CAAA2vB,OAAA,CAAcjtB,CAAd,CACA,CAAAu8B,CAAA,CAAiBv8B,CAAjB,CAFS,CAHyB,CAFtC,CAWIi9B,EAAiBA,QAAQ,CAACt5B,CAAD,CAAS,CACpC,GAAI,CACFrG,CAAAmyB,QAAA,CAAgB,CAAAv2B,CAAA,CAAW4jC,CAAX,CAAA,CAAsBA,CAAtB,CAAgCL,CAAhC,EAAgD94B,CAAhD,CAAhB,CADE,CAEF,MAAM3D,CAAN,CAAS,CACT1C,CAAA2vB,OAAA,CAAcjtB,CAAd,CACA,CAAAu8B,CAAA,CAAiBv8B,CAAjB,CAFS,CAHyB,CAXtC,CAoBIk9B,EAAsBA,QAAQ,CAACL,CAAD,CAAW,CAC3C,GAAI,CACFv/B,CAAAw2B,OAAA,CAAe,CAAA56B,CAAA,CAAW6jC,CAAX,CAAA,CAA2BA,CAA3B,CAA0CP,CAA1C,EAA2DK,CAA3D,CAAf,CADE,CAEF,MAAM78B,CAAN,CAAS,CACTu8B,CAAA,CAAiBv8B,CAAjB,CADS,CAHgC,CAQzC08B,EAAJ,CACEA,CAAAnjC,KAAA,CAAa,CAACyjC,CAAD,CAAkBC,CAAlB,CAAkCC,CAAlC,CAAb,CADF,CAGErjC,CAAA00B,KAAA,CAAWyO,CAAX,CAA4BC,CAA5B,CAA4CC,CAA5C,CAGF,OAAO5/B,EAAAkxB,QAnCuC,CADzC,CAuCP,OAvCO,CAuCE2O,QAAQ,CAACtlB,CAAD,CAAW,CAC1B,MAAO,KAAA0W,KAAA,CAAU,IAAV;AAAgB1W,CAAhB,CADmB,CAvCrB,CA2CP,SA3CO,CA2CIulB,QAAQ,CAACvlB,CAAD,CAAW,CAE5BwlB,QAASA,EAAW,CAACxjC,CAAD,CAAQyjC,CAAR,CAAkB,CACpC,IAAIhgC,EAASkc,CAAA,EACT8jB,EAAJ,CACEhgC,CAAAmyB,QAAA,CAAe51B,CAAf,CADF,CAGEyD,CAAA2vB,OAAA,CAAcpzB,CAAd,CAEF,OAAOyD,EAAAkxB,QAP6B,CAUtC+O,QAASA,EAAc,CAAC1jC,CAAD,CAAQ2jC,CAAR,CAAoB,CACzC,IAAIC,EAAiB,IACrB,IAAI,CACFA,CAAA,CAAkB,CAAA5lB,CAAA,EAAW2kB,CAAX,GADhB,CAEF,MAAMx8B,CAAN,CAAS,CACT,MAAOq9B,EAAA,CAAYr9B,CAAZ,CAAe,CAAA,CAAf,CADE,CAGX,MAAIy9B,EAAJ,EAAsBvkC,CAAA,CAAWukC,CAAAlP,KAAX,CAAtB,CACSkP,CAAAlP,KAAA,CAAoB,QAAQ,EAAG,CACpC,MAAO8O,EAAA,CAAYxjC,CAAZ,CAAmB2jC,CAAnB,CAD6B,CAA/B,CAEJ,QAAQ,CAACnnB,CAAD,CAAQ,CACjB,MAAOgnB,EAAA,CAAYhnB,CAAZ,CAAmB,CAAA,CAAnB,CADU,CAFZ,CADT,CAOSgnB,CAAA,CAAYxjC,CAAZ,CAAmB2jC,CAAnB,CAdgC,CAkB3C,MAAO,KAAAjP,KAAA,CAAU,QAAQ,CAAC10B,CAAD,CAAQ,CAC/B,MAAO0jC,EAAA,CAAe1jC,CAAf,CAAsB,CAAA,CAAtB,CADwB,CAA1B,CAEJ,QAAQ,CAACwc,CAAD,CAAQ,CACjB,MAAOknB,EAAA,CAAelnB,CAAf,CAAsB,CAAA,CAAtB,CADU,CAFZ,CA9BqB,CA3CvB,CA3CA,CAJU,CAAvB,CAqIIsmB,EAAMA,QAAQ,CAAC9iC,CAAD,CAAQ,CACxB,MAAIA,EAAJ,EAAaX,CAAA,CAAWW,CAAA00B,KAAX,CAAb,CAA4C10B,CAA5C,CACO,MACC00B,QAAQ,CAAC1W,CAAD,CAAW,CACvB,IAAIva,EAASkc,CAAA,EACb8iB,EAAA,CAAS,QAAQ,EAAG,CAClBh/B,CAAAmyB,QAAA,CAAe5X,CAAA,CAAShe,CAAT,CAAf,CADkB,CAApB,CAGA,OAAOyD,EAAAkxB,QALgB,CADpB,CAFiB,CArI1B,CAuLIvB,EAASA,QAAQ,CAACtpB,CAAD,CAAS,CAC5B,IAAIrG,EAASkc,CAAA,EACblc,EAAA2vB,OAAA,CAActpB,CAAd,CACA,OAAOrG,EAAAkxB,QAHqB,CAvL9B,CA6LIoO,EAAgCA,QAAQ,CAACj5B,CAAD,CAAS,CACnD,MAAO,MACC4qB,QAAQ,CAAC1W,CAAD;AAAWilB,CAAX,CAAoB,CAChC,IAAIx/B,EAASkc,CAAA,EACb8iB,EAAA,CAAS,QAAQ,EAAG,CAClB,GAAI,CACFh/B,CAAAmyB,QAAA,CAAgB,CAAAv2B,CAAA,CAAW4jC,CAAX,CAAA,CAAsBA,CAAtB,CAAgCL,CAAhC,EAAgD94B,CAAhD,CAAhB,CADE,CAEF,MAAM3D,CAAN,CAAS,CACT1C,CAAA2vB,OAAA,CAAcjtB,CAAd,CACA,CAAAu8B,CAAA,CAAiBv8B,CAAjB,CAFS,CAHO,CAApB,CAQA,OAAO1C,EAAAkxB,QAVyB,CAD7B,CAD4C,CAiIrD,OAAO,OACEhV,CADF,QAEGyT,CAFH,MAlGIwB,QAAQ,CAAC50B,CAAD,CAAQge,CAAR,CAAkBilB,CAAlB,CAA2BC,CAA3B,CAAyC,CAAA,IACtDz/B,EAASkc,CAAA,EAD6C,CAEtD2V,CAFsD,CAItD6N,EAAkBA,QAAQ,CAACnjC,CAAD,CAAQ,CACpC,GAAI,CACF,MAAQ,CAAAX,CAAA,CAAW2e,CAAX,CAAA,CAAuBA,CAAvB,CAAkC2kB,CAAlC,EAAmD3iC,CAAnD,CADN,CAEF,MAAOmG,CAAP,CAAU,CAEV,MADAu8B,EAAA,CAAiBv8B,CAAjB,CACO,CAAAitB,CAAA,CAAOjtB,CAAP,CAFG,CAHwB,CAJoB,CAatDi9B,EAAiBA,QAAQ,CAACt5B,CAAD,CAAS,CACpC,GAAI,CACF,MAAQ,CAAAzK,CAAA,CAAW4jC,CAAX,CAAA,CAAsBA,CAAtB,CAAgCL,CAAhC,EAAgD94B,CAAhD,CADN,CAEF,MAAO3D,CAAP,CAAU,CAEV,MADAu8B,EAAA,CAAiBv8B,CAAjB,CACO,CAAAitB,CAAA,CAAOjtB,CAAP,CAFG,CAHwB,CAboB,CAsBtDk9B,EAAsBA,QAAQ,CAACL,CAAD,CAAW,CAC3C,GAAI,CACF,MAAQ,CAAA3jC,CAAA,CAAW6jC,CAAX,CAAA,CAA2BA,CAA3B,CAA0CP,CAA1C,EAA2DK,CAA3D,CADN,CAEF,MAAO78B,CAAP,CAAU,CACVu8B,CAAA,CAAiBv8B,CAAjB,CADU,CAH+B,CAQ7Cs8B,EAAA,CAAS,QAAQ,EAAG,CAClBK,CAAA,CAAI9iC,CAAJ,CAAA00B,KAAA,CAAgB,QAAQ,CAAC10B,CAAD,CAAQ,CAC1Bs1B,CAAJ,GACAA,CACA,CADO,CAAA,CACP,CAAA7xB,CAAAmyB,QAAA,CAAekN,CAAA,CAAI9iC,CAAJ,CAAA00B,KAAA,CAAgByO,CAAhB,CAAiCC,CAAjC,CAAiDC,CAAjD,CAAf,CAFA,CAD8B,CAAhC,CAIG,QAAQ,CAACv5B,CAAD,CAAS,CACdwrB,CAAJ,GACAA,CACA,CADO,CAAA,CACP,CAAA7xB,CAAAmyB,QAAA,CAAewN,CAAA,CAAet5B,CAAf,CAAf,CAFA,CADkB,CAJpB,CAQG,QAAQ,CAACk5B,CAAD,CAAW,CAChB1N,CAAJ,EACA7xB,CAAAw2B,OAAA,CAAcoJ,CAAA,CAAoBL,CAApB,CAAd,CAFoB,CARtB,CADkB,CAApB,CAeA,OAAOv/B,EAAAkxB,QA7CmD,CAkGrD;IAxBP5c,QAAY,CAAC8rB,CAAD,CAAW,CAAA,IACjBlO,EAAWhW,CAAA,EADM,CAEjBuY,EAAU,CAFO,CAGjBv1B,EAAU3D,CAAA,CAAQ6kC,CAAR,CAAA,CAAoB,EAApB,CAAyB,EAEvC5kC,EAAA,CAAQ4kC,CAAR,CAAkB,QAAQ,CAAClP,CAAD,CAAUv1B,CAAV,CAAe,CACvC84B,CAAA,EACA4K,EAAA,CAAInO,CAAJ,CAAAD,KAAA,CAAkB,QAAQ,CAAC10B,CAAD,CAAQ,CAC5B2C,CAAArD,eAAA,CAAuBF,CAAvB,CAAJ,GACAuD,CAAA,CAAQvD,CAAR,CACA,CADeY,CACf,CAAM,EAAEk4B,CAAR,EAAkBvC,CAAAC,QAAA,CAAiBjzB,CAAjB,CAFlB,CADgC,CAAlC,CAIG,QAAQ,CAACmH,CAAD,CAAS,CACdnH,CAAArD,eAAA,CAAuBF,CAAvB,CAAJ,EACAu2B,CAAAvC,OAAA,CAAgBtpB,CAAhB,CAFkB,CAJpB,CAFuC,CAAzC,CAYgB,EAAhB,GAAIouB,CAAJ,EACEvC,CAAAC,QAAA,CAAiBjzB,CAAjB,CAGF,OAAOgzB,EAAAhB,QArBc,CAwBhB,CA1UqC,CAkV9CxkB,QAASA,GAAa,EAAE,CACtB,IAAAqI,KAAA,CAAY,CAAC,SAAD,CAAY,UAAZ,CAAwB,QAAQ,CAAC0C,CAAD,CAAUa,CAAV,CAAoB,CAC9D,IAAI+nB,EAAwB5oB,CAAA4oB,sBAAxBA,EACwB5oB,CAAA6oB,4BADxBD,EAEwB5oB,CAAA8oB,yBAF5B,CAIIC,EAAuB/oB,CAAA+oB,qBAAvBA,EACuB/oB,CAAAgpB,2BADvBD,EAEuB/oB,CAAAipB,wBAFvBF,EAGuB/oB,CAAAkpB,kCAP3B,CASIC,EAAe,CAAC,CAACP,CATrB,CAUIQ,EAAMD,CACA;AAAN,QAAQ,CAACz/B,CAAD,CAAK,CACX,IAAI2/B,EAAKT,CAAA,CAAsBl/B,CAAtB,CACT,OAAO,SAAQ,EAAG,CAChBq/B,CAAA,CAAqBM,CAArB,CADgB,CAFP,CAAP,CAMN,QAAQ,CAAC3/B,CAAD,CAAK,CACX,IAAI4/B,EAAQzoB,CAAA,CAASnX,CAAT,CAAa,KAAb,CAAoB,CAAA,CAApB,CACZ,OAAO,SAAQ,EAAG,CAChBmX,CAAAgE,OAAA,CAAgBykB,CAAhB,CADgB,CAFP,CAOjBF,EAAAtoB,UAAA,CAAgBqoB,CAEhB,OAAOC,EA3BuD,CAApD,CADU,CAmGxB30B,QAASA,GAAkB,EAAE,CAC3B,IAAI80B,EAAM,EAAV,CACIC,EAAmBjmC,CAAA,CAAO,YAAP,CADvB,CAEIkmC,EAAiB,IAErB,KAAAC,UAAA,CAAiBC,QAAQ,CAAC7kC,CAAD,CAAQ,CAC3Be,SAAAlC,OAAJ,GACE4lC,CADF,CACQzkC,CADR,CAGA,OAAOykC,EAJwB,CAOjC,KAAAjsB,KAAA,CAAY,CAAC,WAAD,CAAc,mBAAd,CAAmC,QAAnC,CAA6C,UAA7C,CACR,QAAQ,CAAE4B,CAAF,CAAeoI,CAAf,CAAoCc,CAApC,CAA8CwP,CAA9C,CAAwD,CA0ClEgS,QAASA,EAAK,EAAG,CACf,IAAAC,IAAA,CAAW9kC,EAAA,EACX,KAAAy1B,QAAA,CAAe,IAAAsP,QAAf,CAA8B,IAAAC,WAA9B,CACe,IAAAC,cADf,CACoC,IAAAC,cADpC,CAEe,IAAAC,YAFf,CAEkC,IAAAC,YAFlC,CAEqD,IACrD,KAAA,CAAK,MAAL,CAAA,CAAe,IAAAC,MAAf,CAA6B,IAC7B;IAAAC,YAAA,CAAmB,CAAA,CACnB,KAAAC,aAAA,CAAoB,EACpB,KAAAC,kBAAA,CAAyB,EACzB,KAAAC,YAAA,CAAmB,EACnB,KAAAC,gBAAA,CAAuB,EACvB,KAAA1b,kBAAA,CAAyB,EAXV,CA69BjB2b,QAASA,EAAU,CAACC,CAAD,CAAQ,CACzB,GAAIzqB,CAAAsa,QAAJ,CACE,KAAMgP,EAAA,CAAiB,QAAjB,CAAsDtpB,CAAAsa,QAAtD,CAAN,CAGFta,CAAAsa,QAAA,CAAqBmQ,CALI,CAY3BC,QAASA,EAAW,CAAC7M,CAAD,CAAMnxB,CAAN,CAAY,CAC9B,IAAIlD,EAAK0e,CAAA,CAAO2V,CAAP,CACTlvB,GAAA,CAAYnF,CAAZ,CAAgBkD,CAAhB,CACA,OAAOlD,EAHuB,CAMhCmhC,QAASA,EAAsB,CAACC,CAAD,CAAUtM,CAAV,CAAiB5xB,CAAjB,CAAuB,CACpD,EACEk+B,EAAAL,gBAAA,CAAwB79B,CAAxB,CAEA,EAFiC4xB,CAEjC,CAAsC,CAAtC,GAAIsM,CAAAL,gBAAA,CAAwB79B,CAAxB,CAAJ,EACE,OAAOk+B,CAAAL,gBAAA,CAAwB79B,CAAxB,CAJX,OAMUk+B,CANV,CAMoBA,CAAAhB,QANpB,CADoD,CActDiB,QAASA,EAAY,EAAG,EAv+BxBnB,CAAA9qB,UAAA,CAAkB,aACH8qB,CADG,MA0BVvf,QAAQ,CAAC2gB,CAAD,CAAU,CAIlBA,CAAJ,EACEC,CAIA,CAJQ,IAAIrB,CAIZ,CAHAqB,CAAAb,MAGA,CAHc,IAAAA,MAGd,CADAa,CAAAX,aACA,CADqB,IAAAA,aACrB,CAAAW,CAAAV,kBAAA;AAA0B,IAAAA,kBAL5B,GASO,IAAAW,kBAWL,GAVE,IAAAA,kBAQA,CARyBC,QAAQ,EAAG,CAClC,IAAApB,WAAA,CAAkB,IAAAC,cAAlB,CACI,IAAAE,YADJ,CACuB,IAAAC,YADvB,CAC0C,IAC1C,KAAAK,YAAA,CAAmB,EACnB,KAAAC,gBAAA,CAAuB,EACvB,KAAAZ,IAAA,CAAW9kC,EAAA,EACX,KAAAmmC,kBAAA,CAAyB,IANS,CAQpC,CAAA,IAAAA,kBAAApsB,UAAA,CAAmC,IAErC,EAAAmsB,CAAA,CAAQ,IAAI,IAAAC,kBApBd,CAsBAD,EAAA,CAAM,MAAN,CAAA,CAAgBA,CAChBA,EAAAnB,QAAA,CAAgB,IAChBmB,EAAAhB,cAAA,CAAsB,IAAAE,YAClB,KAAAD,YAAJ,CAEE,IAAAC,YAFF,CACE,IAAAA,YAAAH,cADF,CACmCiB,CADnC,CAIE,IAAAf,YAJF,CAIqB,IAAAC,YAJrB,CAIwCc,CAExC,OAAOA,EAnCe,CA1BR,QAsLR5iC,QAAQ,CAAC+iC,CAAD;AAAWjpB,CAAX,CAAqBkpB,CAArB,CAAqC,CAAA,IAE/CxtB,EAAM+sB,CAAA,CAAYQ,CAAZ,CAAsB,OAAtB,CAFyC,CAG/CxjC,EAFQ6F,IAEAs8B,WAHuC,CAI/CuB,EAAU,IACJnpB,CADI,MAEF4oB,CAFE,KAGHltB,CAHG,KAIHutB,CAJG,IAKJ,CAAC,CAACC,CALE,CAQd5B,EAAA,CAAiB,IAGjB,IAAI,CAACtlC,CAAA,CAAWge,CAAX,CAAL,CAA2B,CACzB,IAAIopB,EAAWX,CAAA,CAAYzoB,CAAZ,EAAwB/b,CAAxB,CAA8B,UAA9B,CACfklC,EAAA5hC,GAAA,CAAa8hC,QAAQ,CAACC,CAAD,CAASC,CAAT,CAAiBj+B,CAAjB,CAAwB,CAAC89B,CAAA,CAAS99B,CAAT,CAAD,CAFpB,CAK3B,GAAuB,QAAvB,EAAI,MAAO29B,EAAX,EAAmCvtB,CAAAsB,SAAnC,CAAiD,CAC/C,IAAIwsB,EAAaL,CAAA5hC,GACjB4hC,EAAA5hC,GAAA,CAAa8hC,QAAQ,CAACC,CAAD,CAASC,CAAT,CAAiBj+B,CAAjB,CAAwB,CAC3Ck+B,CAAAtnC,KAAA,CAAgB,IAAhB,CAAsBonC,CAAtB,CAA8BC,CAA9B,CAAsCj+B,CAAtC,CACA5F,GAAA,CAAYD,CAAZ,CAAmB0jC,CAAnB,CAF2C,CAFE,CAQ5C1jC,CAAL,GACEA,CADF,CA3BY6F,IA4BFs8B,WADV,CAC6B,EAD7B,CAKAniC,EAAArC,QAAA,CAAc+lC,CAAd,CAEA,OAAOM,SAAwB,EAAG,CAChC/jC,EAAA,CAAYD,CAAZ,CAAmB0jC,CAAnB,CACA7B,EAAA,CAAiB,IAFe,CAnCiB,CAtLrC,kBAuREoC,QAAQ,CAACpoC,CAAD,CAAM0e,CAAN,CAAgB,CACxC,IAAI1Y,EAAO,IAAX,CAEIwqB,CAFJ,CAKIC,CALJ,CAOI4X,CAPJ,CASIC,EAAuC,CAAvCA,CAAqB5pB,CAAAxe,OATzB,CAUIqoC,EAAiB,CAVrB,CAWIC,EAAY7jB,CAAA,CAAO3kB,CAAP,CAXhB,CAYIyoC,EAAgB,EAZpB,CAaIC,EAAiB,EAbrB,CAcIC,EAAU,CAAA,CAdd,CAeIC,EAAY,CAsGhB,OAAO,KAAAhkC,OAAA,CApGPikC,QAA8B,EAAG,CAC/BrY,CAAA,CAAWgY,CAAA,CAAUxiC,CAAV,CADoB,KAE3B8iC,CAF2B,CAEhBroC,CAEf,IAAKwC,CAAA,CAASutB,CAAT,CAAL,CAKO,GAAIzwB,EAAA,CAAYywB,CAAZ,CAAJ,CAgBL,IAfIC,CAeKvvB,GAfQunC,CAeRvnC,GAbPuvB,CAEA,CAFWgY,CAEX,CADAG,CACA,CADYnY,CAAAvwB,OACZ,CAD8B,CAC9B,CAAAqoC,CAAA,EAWOrnC;AART4nC,CAQS5nC,CARGsvB,CAAAtwB,OAQHgB,CANL0nC,CAMK1nC,GANS4nC,CAMT5nC,GAJPqnC,CAAA,EACA,CAAA9X,CAAAvwB,OAAA,CAAkB0oC,CAAlB,CAA8BE,CAGvB5nC,EAAAA,CAAAA,CAAI,CAAb,CAAgBA,CAAhB,CAAoB4nC,CAApB,CAA+B5nC,CAAA,EAA/B,CACiBuvB,CAAA,CAASvvB,CAAT,CAEf,GAF+BuvB,CAAA,CAASvvB,CAAT,CAE/B,EADKsvB,CAAA,CAAStvB,CAAT,CACL,GADqBsvB,CAAA,CAAStvB,CAAT,CACrB,EAAiBuvB,CAAA,CAASvvB,CAAT,CAAjB,GAAiCsvB,CAAA,CAAStvB,CAAT,CAAjC,GACEqnC,CAAA,EACA,CAAA9X,CAAA,CAASvvB,CAAT,CAAA,CAAcsvB,CAAA,CAAStvB,CAAT,CAFhB,CAnBG,KAwBA,CACDuvB,CAAJ,GAAiBiY,CAAjB,GAEEjY,CAEA,CAFWiY,CAEX,CAF4B,EAE5B,CADAE,CACA,CADY,CACZ,CAAAL,CAAA,EAJF,CAOAO,EAAA,CAAY,CACZ,KAAKroC,CAAL,GAAY+vB,EAAZ,CACMA,CAAA7vB,eAAA,CAAwBF,CAAxB,CAAJ,GACEqoC,CAAA,EACA,CAAIrY,CAAA9vB,eAAA,CAAwBF,CAAxB,CAAJ,CACMgwB,CAAA,CAAShwB,CAAT,CADN,GACwB+vB,CAAA,CAAS/vB,CAAT,CADxB,GAEI8nC,CAAA,EACA,CAAA9X,CAAA,CAAShwB,CAAT,CAAA,CAAgB+vB,CAAA,CAAS/vB,CAAT,CAHpB,GAMEmoC,CAAA,EAEA,CADAnY,CAAA,CAAShwB,CAAT,CACA,CADgB+vB,CAAA,CAAS/vB,CAAT,CAChB,CAAA8nC,CAAA,EARF,CAFF,CAcF,IAAIK,CAAJ,CAAgBE,CAAhB,CAGE,IAAIroC,CAAJ,GADA8nC,EAAA,EACW9X,CAAAA,CAAX,CACMA,CAAA9vB,eAAA,CAAwBF,CAAxB,CAAJ,EAAqC,CAAA+vB,CAAA7vB,eAAA,CAAwBF,CAAxB,CAArC,GACEmoC,CAAA,EACA,CAAA,OAAOnY,CAAA,CAAShwB,CAAT,CAFT,CA5BC,CA7BP,IACMgwB,EAAJ,GAAiBD,CAAjB,GACEC,CACA,CADWD,CACX,CAAA+X,CAAA,EAFF,CA+DF,OAAOA,EApEwB,CAoG1B,CA7BPQ,QAA+B,EAAG,CAC5BJ,CAAJ,EACEA,CACA,CADU,CAAA,CACV,CAAAjqB,CAAA,CAAS8R,CAAT,CAAmBA,CAAnB,CAA6BxqB,CAA7B,CAFF,EAIE0Y,CAAA,CAAS8R,CAAT,CAAmB6X,CAAnB,CAAiCriC,CAAjC,CAIF,IAAIsiC,CAAJ,CACE,GAAKrlC,CAAA,CAASutB,CAAT,CAAL,CAGO,GAAIzwB,EAAA,CAAYywB,CAAZ,CAAJ,CAA2B,CAChC6X,CAAA,CAAmB5hB,KAAJ,CAAU+J,CAAAtwB,OAAV,CACf,KAAK,IAAIgB,EAAI,CAAb,CAAgBA,CAAhB,CAAoBsvB,CAAAtwB,OAApB,CAAqCgB,CAAA,EAArC,CACEmnC,CAAA,CAAannC,CAAb,CAAA,CAAkBsvB,CAAA,CAAStvB,CAAT,CAHY,CAA3B,IAOL,KAAST,CAAT,GADA4nC,EACgB7X,CADD,EACCA,CAAAA,CAAhB,CACM7vB,EAAAC,KAAA,CAAoB4vB,CAApB;AAA8B/vB,CAA9B,CAAJ,GACE4nC,CAAA,CAAa5nC,CAAb,CADF,CACsB+vB,CAAA,CAAS/vB,CAAT,CADtB,CAXJ,KAEE4nC,EAAA,CAAe7X,CAZa,CA6B3B,CAtHiC,CAvR1B,SAmcP2P,QAAQ,EAAG,CAAA,IACd6I,CADc,CACP3nC,CADO,CACAiY,CADA,CAEd2vB,CAFc,CAGdC,EAAa,IAAArC,aAHC,CAIdsC,EAAkB,IAAArC,kBAJJ,CAKd5mC,CALc,CAMdkpC,CANc,CAMPC,EAAMvD,CANC,CAORuB,CAPQ,CAQdiC,EAAW,EARG,CASdC,CATc,CASNC,CATM,CASEC,CAEpBxC,EAAA,CAAW,SAAX,CAEAjB,EAAA,CAAiB,IAEjB,GAAG,CACDoD,CAAA,CAAQ,CAAA,CAGR,KAFA/B,CAEA,CAZ0BtvB,IAY1B,CAAMmxB,CAAAhpC,OAAN,CAAA,CAAyB,CACvB,GAAI,CACFupC,CACA,CADYP,CAAAr2B,MAAA,EACZ,CAAA42B,CAAAz/B,MAAA0/B,MAAA,CAAsBD,CAAA3W,WAAtB,CAFE,CAGF,MAAOtrB,CAAP,CAAU,CAsflBiV,CAAAsa,QApfQ,CAofa,IApfb,CAAAlT,CAAA,CAAkBrc,CAAlB,CAFU,CAIZw+B,CAAA,CAAiB,IARM,CAWzB,CAAA,CACA,EAAG,CACD,GAAKiD,CAAL,CAAgB5B,CAAAf,WAAhB,CAGE,IADApmC,CACA,CADS+oC,CAAA/oC,OACT,CAAOA,CAAA,EAAP,CAAA,CACE,GAAI,CAIF,GAHA8oC,CAGA,CAHQC,CAAA,CAAS/oC,CAAT,CAGR,CACE,IAAKmB,CAAL,CAAa2nC,CAAA5uB,IAAA,CAAUitB,CAAV,CAAb,KAAsC/tB,CAAtC,CAA6C0vB,CAAA1vB,KAA7C,GACI,EAAE0vB,CAAAljB,GACA,CAAIzgB,EAAA,CAAOhE,CAAP,CAAciY,CAAd,CAAJ,CACqB,QADrB,EACK,MAAOjY,EADZ,EACgD,QADhD,EACiC,MAAOiY,EADxC,EAEQqwB,KAAA,CAAMtoC,CAAN,CAFR,EAEwBsoC,KAAA,CAAMrwB,CAAN,CAH1B,CADJ,CAKE8vB,CAIA,CAJQ,CAAA,CAIR,CAHApD,CAGA,CAHiBgD,CAGjB,CAFAA,CAAA1vB,KAEA,CAFa0vB,CAAAljB,GAAA,CAAWxhB,EAAA,CAAKjD,CAAL,CAAY,IAAZ,CAAX,CAA+BA,CAE5C,CADA2nC,CAAA/iC,GAAA,CAAS5E,CAAT,CAAkBiY,CAAD,GAAUguB,CAAV,CAA0BjmC,CAA1B,CAAkCiY,CAAnD,CAA0D+tB,CAA1D,CACA,CAAU,CAAV,CAAIgC,CAAJ,GACEE,CAMA,CANS,CAMT,CANaF,CAMb,CALKC,CAAA,CAASC,CAAT,CAKL,GALuBD,CAAA,CAASC,CAAT,CAKvB,CAL0C,EAK1C,EAJAC,CAIA;AAJU9oC,CAAA,CAAWsoC,CAAA1O,IAAX,CACD,CAAH,MAAG,EAAO0O,CAAA1O,IAAAnxB,KAAP,EAAyB6/B,CAAA1O,IAAAl3B,SAAA,EAAzB,EACH4lC,CAAA1O,IAEN,CADAkP,CACA,EADU,YACV,CADyB/iC,EAAA,CAAOpF,CAAP,CACzB,CADyC,YACzC,CADwDoF,EAAA,CAAO6S,CAAP,CACxD,CAAAgwB,CAAA,CAASC,CAAT,CAAAxoC,KAAA,CAAsByoC,CAAtB,CAPF,CATF,KAkBO,IAAIR,CAAJ,GAAchD,CAAd,CAA8B,CAGnCoD,CAAA,CAAQ,CAAA,CACR,OAAM,CAJ6B,CAvBrC,CA8BF,MAAO5hC,CAAP,CAAU,CA2ctBiV,CAAAsa,QAzcY,CAycS,IAzcT,CAAAlT,CAAA,CAAkBrc,CAAlB,CAFU,CAUhB,GAAI,EAAEoiC,CAAF,CAAUvC,CAAAZ,YAAV,EACCY,CADD,GArEoBtvB,IAqEpB,EACuBsvB,CAAAd,cADvB,CAAJ,CAEE,IAAA,CAAMc,CAAN,GAvEsBtvB,IAuEtB,EAA4B,EAAE6xB,CAAF,CAASvC,CAAAd,cAAT,CAA5B,CAAA,CACEc,CAAA,CAAUA,CAAAhB,QAhDb,CAAH,MAmDUgB,CAnDV,CAmDoBuC,CAnDpB,CAuDA,KAAIR,CAAJ,EAAaF,CAAAhpC,OAAb,GAAmC,CAAEmpC,CAAA,EAArC,CAEE,KAqbN5sB,EAAAsa,QArbY,CAqbS,IArbT,CAAAgP,CAAA,CAAiB,QAAjB,CAGFD,CAHE,CAGGr/B,EAAA,CAAO6iC,CAAP,CAHH,CAAN,CAzED,CAAH,MA+ESF,CA/ET,EA+EkBF,CAAAhpC,OA/ElB,CAmFA,KA2aFuc,CAAAsa,QA3aE,CA2amB,IA3anB,CAAMoS,CAAAjpC,OAAN,CAAA,CACE,GAAI,CACFipC,CAAAt2B,MAAA,EAAA,EADE,CAEF,MAAOrL,CAAP,CAAU,CACVqc,CAAA,CAAkBrc,CAAlB,CADU,CArGI,CAncJ,UAilBNqO,QAAQ,EAAG,CAEnB,GAAI+wB,CAAA,IAAAA,YAAJ,CAAA,CACA,IAAInkC,EAAS,IAAA4jC,QAEb,KAAA7G,WAAA,CAAgB,UAAhB,CACA;IAAAoH,YAAA,CAAmB,CAAA,CACf,KAAJ,GAAanqB,CAAb,GAEAnc,CAAA,CAAQ,IAAA0mC,gBAAR,CAA8BjhC,EAAA,CAAK,IAAL,CAAWqhC,CAAX,CAAmC,IAAnC,CAA9B,CA2BA,CAvBI3kC,CAAAgkC,YAuBJ,EAvB0B,IAuB1B,GAvBgChkC,CAAAgkC,YAuBhC,CAvBqD,IAAAF,cAuBrD,EAtBI9jC,CAAAikC,YAsBJ,EAtB0B,IAsB1B,GAtBgCjkC,CAAAikC,YAsBhC,CAtBqD,IAAAF,cAsBrD,EArBI,IAAAA,cAqBJ,GArBwB,IAAAA,cAAAD,cAqBxB,CArB2D,IAAAA,cAqB3D,EApBI,IAAAA,cAoBJ,GApBwB,IAAAA,cAAAC,cAoBxB,CApB2D,IAAAA,cAoB3D,EATA,IAAAH,QASA,CATe,IAAAE,cASf,CAToC,IAAAC,cASpC,CATyD,IAAAC,YASzD,CARI,IAAAC,YAQJ,CARuB,IAAAC,MAQvB,CARoC,IAQpC,CALA,IAAAI,YAKA,CALmB,EAKnB,CAJA,IAAAT,WAIA,CAJkB,IAAAO,aAIlB,CAJsC,IAAAC,kBAItC;AAJ+D,EAI/D,CADA,IAAAjxB,SACA,CADgB,IAAAsqB,QAChB,CAD+B,IAAAh2B,OAC/B,CAD6CxH,CAC7C,CAAA,IAAAknC,IAAA,CAAW,IAAAjlC,OAAX,CAAyBklC,QAAQ,EAAG,CAAE,MAAOnnC,EAAT,CA7BpC,CALA,CAFmB,CAjlBL,OAopBT+mC,QAAQ,CAACK,CAAD,CAAO/uB,CAAP,CAAe,CAC5B,MAAO2J,EAAA,CAAOolB,CAAP,CAAA,CAAa,IAAb,CAAmB/uB,CAAnB,CADqB,CAppBd,YAqrBJrW,QAAQ,CAAColC,CAAD,CAAO,CAGpBttB,CAAAsa,QAAL,EAA4Bta,CAAAoqB,aAAA3mC,OAA5B,EACEi0B,CAAAnT,MAAA,CAAe,QAAQ,EAAG,CACpBvE,CAAAoqB,aAAA3mC,OAAJ,EACEuc,CAAA0jB,QAAA,EAFsB,CAA1B,CAOF,KAAA0G,aAAA9lC,KAAA,CAAuB,OAAQ,IAAR,YAA0BgpC,CAA1B,CAAvB,CAXyB,CArrBX,cAmsBDC,QAAQ,CAAC/jC,CAAD,CAAK,CAC1B,IAAA6gC,kBAAA/lC,KAAA,CAA4BkF,CAA5B,CAD0B,CAnsBZ,QAovBRkE,QAAQ,CAAC4/B,CAAD,CAAO,CACrB,GAAI,CAEF,MADA9C,EAAA,CAAW,QAAX,CACO,CAAA,IAAAyC,MAAA,CAAWK,CAAX,CAFL,CAGF,MAAOviC,CAAP,CAAU,CACVqc,CAAA,CAAkBrc,CAAlB,CADU,CAHZ,OAKU,CAsNZiV,CAAAsa,QAAA,CAAqB,IApNjB,IAAI,CACFta,CAAA0jB,QAAA,EADE,CAEF,MAAO34B,CAAP,CAAU,CAEV,KADAqc,EAAA,CAAkBrc,CAAlB,CACMA,CAAAA,CAAN,CAFU,CAJJ,CANW,CApvBP,KA+xBXqiC,QAAQ,CAAC1gC,CAAD;AAAOuV,CAAP,CAAiB,CAC5B,IAAIurB,EAAiB,IAAAlD,YAAA,CAAiB59B,CAAjB,CAChB8gC,EAAL,GACE,IAAAlD,YAAA,CAAiB59B,CAAjB,CADF,CAC2B8gC,CAD3B,CAC4C,EAD5C,CAGAA,EAAAlpC,KAAA,CAAoB2d,CAApB,CAEA,KAAI2oB,EAAU,IACd,GACOA,EAAAL,gBAAA,CAAwB79B,CAAxB,CAGL,GAFEk+B,CAAAL,gBAAA,CAAwB79B,CAAxB,CAEF,CAFkC,CAElC,EAAAk+B,CAAAL,gBAAA,CAAwB79B,CAAxB,CAAA,EAJF,OAKUk+B,CALV,CAKoBA,CAAAhB,QALpB,CAOA,KAAIrgC,EAAO,IACX,OAAO,SAAQ,EAAG,CAChBikC,CAAA,CAAe/lC,EAAA,CAAQ+lC,CAAR,CAAwBvrB,CAAxB,CAAf,CAAA,CAAoD,IACpD0oB,EAAA,CAAuBphC,CAAvB,CAA6B,CAA7B,CAAgCmD,CAAhC,CAFgB,CAhBU,CA/xBd,OA40BT+gC,QAAQ,CAAC/gC,CAAD,CAAO8R,CAAP,CAAa,CAAA,IACtB1T,EAAQ,EADc,CAEtB0iC,CAFsB,CAGtBjgC,EAAQ,IAHc,CAItB4N,EAAkB,CAAA,CAJI,CAKtBJ,EAAQ,MACArO,CADA,aAEOa,CAFP,iBAGW4N,QAAQ,EAAG,CAACA,CAAA,CAAkB,CAAA,CAAnB,CAHtB,gBAIUH,QAAQ,EAAG,CACzBD,CAAAS,iBAAA,CAAyB,CAAA,CADA,CAJrB,kBAOY,CAAA,CAPZ,CALc,CActBkyB,EAAsBC,CAAC5yB,CAAD4yB,CA77WzB9jC,OAAA,CAAcH,EAAAvF,KAAA,CA67WoBwB,SA77WpB,CA67W+Bb,CA77W/B,CAAd,CA+6WyB,CAetBL,CAfsB,CAenBhB,CAEP,GAAG,CACD+pC,CAAA,CAAiBjgC,CAAA+8B,YAAA,CAAkB59B,CAAlB,CAAjB,EAA4C5B,CAC5CiQ,EAAA6yB,aAAA,CAAqBrgC,CAChB9I,EAAA,CAAE,CAAP,KAAUhB,CAAV,CAAiB+pC,CAAA/pC,OAAjB,CAAwCgB,CAAxC,CAA0ChB,CAA1C,CAAkDgB,CAAA,EAAlD,CAGE,GAAK+oC,CAAA,CAAe/oC,CAAf,CAAL,CAMA,GAAI,CAEF+oC,CAAA,CAAe/oC,CAAf,CAAAmF,MAAA,CAAwB,IAAxB;AAA8B8jC,CAA9B,CAFE,CAGF,MAAO3iC,CAAP,CAAU,CACVqc,CAAA,CAAkBrc,CAAlB,CADU,CATZ,IACEyiC,EAAA5lC,OAAA,CAAsBnD,CAAtB,CAAyB,CAAzB,CAEA,CADAA,CAAA,EACA,CAAAhB,CAAA,EAWJ,IAAI0X,CAAJ,CAAqB,KAErB5N,EAAA,CAAQA,CAAAq8B,QAtBP,CAAH,MAuBSr8B,CAvBT,CAyBA,OAAOwN,EA1CmB,CA50BZ,YA+4BJgoB,QAAQ,CAACr2B,CAAD,CAAO8R,CAAP,CAAa,CAgB/B,IAhB+B,IAE3BosB,EADStvB,IADkB,CAG3B6xB,EAFS7xB,IADkB,CAI3BP,EAAQ,MACArO,CADA,aAHC4O,IAGD,gBAGUN,QAAQ,EAAG,CACzBD,CAAAS,iBAAA,CAAyB,CAAA,CADA,CAHrB,kBAMY,CAAA,CANZ,CAJmB,CAY3BkyB,EAAsBC,CAAC5yB,CAAD4yB,CA9/WzB9jC,OAAA,CAAcH,EAAAvF,KAAA,CA8/WoBwB,SA9/WpB,CA8/W+Bb,CA9/W/B,CAAd,CAk/W8B,CAahBL,CAbgB,CAabhB,CAGlB,CAAQmnC,CAAR,CAAkBuC,CAAlB,CAAA,CAAyB,CACvBpyB,CAAA6yB,aAAA,CAAqBhD,CACrBpV,EAAA,CAAYoV,CAAAN,YAAA,CAAoB59B,CAApB,CAAZ,EAAyC,EACpCjI,EAAA,CAAE,CAAP,KAAUhB,CAAV,CAAmB+xB,CAAA/xB,OAAnB,CAAqCgB,CAArC,CAAuChB,CAAvC,CAA+CgB,CAAA,EAA/C,CAEE,GAAK+wB,CAAA,CAAU/wB,CAAV,CAAL,CAOA,GAAI,CACF+wB,CAAA,CAAU/wB,CAAV,CAAAmF,MAAA,CAAmB,IAAnB,CAAyB8jC,CAAzB,CADE,CAEF,MAAM3iC,CAAN,CAAS,CACTqc,CAAA,CAAkBrc,CAAlB,CADS,CATX,IACEyqB,EAAA5tB,OAAA,CAAiBnD,CAAjB,CAAoB,CAApB,CAEA,CADAA,CAAA,EACA,CAAAhB,CAAA,EAeJ,IAAI,EAAE0pC,CAAF,CAAWvC,CAAAL,gBAAA,CAAwB79B,CAAxB,CAAX,EAA4Ck+B,CAAAZ,YAA5C,EACCY,CADD,GAtCOtvB,IAsCP,EACuBsvB,CAAAd,cADvB,CAAJ,CAEE,IAAA,CAAMc,CAAN,GAxCStvB,IAwCT,EAA4B,EAAE6xB,CAAF;AAASvC,CAAAd,cAAT,CAA5B,CAAA,CACEc,CAAA,CAAUA,CAAAhB,QA1BS,CA+BzB,MAAO7uB,EA/CwB,CA/4BjB,CAk8BlB,KAAIiF,EAAa,IAAI0pB,CAErB,OAAO1pB,EApgC2D,CADxD,CAZe,CA4jC7BjP,QAASA,GAAqB,EAAG,CAAA,IAC3B2W,EAA6B,mCADF,CAE7BG,EAA8B,qCAkBhC,KAAAH,2BAAA,CAAkCC,QAAQ,CAACC,CAAD,CAAS,CACjD,MAAIrhB,EAAA,CAAUqhB,CAAV,CAAJ,EACEF,CACO,CADsBE,CACtB,CAAA,IAFT,EAIOF,CAL0C,CAyBnD,KAAAG,4BAAA,CAAmCC,QAAQ,CAACF,CAAD,CAAS,CAClD,MAAIrhB,EAAA,CAAUqhB,CAAV,CAAJ,EACEC,CACO,CADuBD,CACvB,CAAA,IAFT,EAIOC,CAL2C,CAQpD,KAAAzK,KAAA,CAAY2H,QAAQ,EAAG,CACrB,MAAO8oB,SAAoB,CAACC,CAAD,CAAMC,CAAN,CAAe,CACxC,IAAIC,EAAQD,CAAA,CAAUlmB,CAAV,CAAwCH,CAApD,CACIumB,CAEJ,IAAI,CAACpyB,CAAL,EAAqB,CAArB,EAAaA,CAAb,CAEE,GADAoyB,CACI,CADYrR,EAAA,CAAWkR,CAAX,CAAA/qB,KACZ,CAAkB,EAAlB,GAAAkrB,CAAA,EAAwB,CAACA,CAAA7iC,MAAA,CAAoB4iC,CAApB,CAA7B,CACE,MAAO,SAAP,CAAiBC,CAGrB,OAAOH,EAViC,CADrB,CArDQ,CA4FjCI,QAASA,GAAa,CAACC,CAAD,CAAU,CAC9B,GAAgB,MAAhB,GAAIA,CAAJ,CACE,MAAOA,EACF,IAAIxqC,CAAA,CAASwqC,CAAT,CAAJ,CAAuB,CAK5B,GAA8B,EAA9B,CAAIA,CAAA1mC,QAAA,CAAgB,KAAhB,CAAJ,CACE,KAAM2mC,GAAA,CAAW,QAAX;AACsDD,CADtD,CAAN,CAGFA,CAAA,CAA0BA,CAjBrB9iC,QAAA,CAAU,+BAAV,CAA2C,MAA3C,CAAAA,QAAA,CACU,OADV,CACmB,OADnB,CAiBKA,QAAA,CACY,QADZ,CACsB,IADtB,CAAAA,QAAA,CAEY,KAFZ,CAEmB,YAFnB,CAGV,OAAW7C,OAAJ,CAAW,GAAX,CAAiB2lC,CAAjB,CAA2B,GAA3B,CAZqB,CAavB,GAAIvnC,EAAA,CAASunC,CAAT,CAAJ,CAIL,MAAW3lC,OAAJ,CAAW,GAAX,CAAiB2lC,CAAArmC,OAAjB,CAAkC,GAAlC,CAEP,MAAMsmC,GAAA,CAAW,UAAX,CAAN,CAtB4B,CA4BhCC,QAASA,GAAc,CAACC,CAAD,CAAW,CAChC,IAAIC,EAAmB,EACnBhoC,EAAA,CAAU+nC,CAAV,CAAJ,EACEzqC,CAAA,CAAQyqC,CAAR,CAAkB,QAAQ,CAACH,CAAD,CAAU,CAClCI,CAAAjqC,KAAA,CAAsB4pC,EAAA,CAAcC,CAAd,CAAtB,CADkC,CAApC,CAIF,OAAOI,EAPyB,CA4ElC75B,QAASA,GAAoB,EAAG,CAC9B,IAAA85B,aAAA,CAAoBA,EADU,KAI1BC,EAAuB,CAAC,MAAD,CAJG,CAK1BC,EAAuB,EAwB3B,KAAAD,qBAAA,CAA4BE,QAAS,CAAC/pC,CAAD,CAAQ,CACvCe,SAAAlC,OAAJ,GACEgrC,CADF,CACyBJ,EAAA,CAAezpC,CAAf,CADzB,CAGA,OAAO6pC,EAJoC,CAkC7C,KAAAC,qBAAA,CAA4BE,QAAS,CAAChqC,CAAD,CAAQ,CACvCe,SAAAlC,OAAJ,GACEirC,CADF,CACyBL,EAAA,CAAezpC,CAAf,CADzB,CAGA,OAAO8pC,EAJoC,CAO7C,KAAAtxB,KAAA;AAAY,CAAC,WAAD,CAAc,QAAQ,CAAC4B,CAAD,CAAY,CA0C5C6vB,QAASA,EAAkB,CAACC,CAAD,CAAO,CAChC,IAAIC,EAAaA,QAA+B,CAACC,CAAD,CAAe,CAC7D,IAAAC,qBAAA,CAA4BC,QAAQ,EAAG,CACrC,MAAOF,EAD8B,CADsB,CAK3DF,EAAJ,GACEC,CAAAnwB,UADF,CACyB,IAAIkwB,CAD7B,CAGAC,EAAAnwB,UAAAsf,QAAA,CAA+BiR,QAAmB,EAAG,CACnD,MAAO,KAAAF,qBAAA,EAD4C,CAGrDF,EAAAnwB,UAAAjY,SAAA,CAAgCyoC,QAAoB,EAAG,CACrD,MAAO,KAAAH,qBAAA,EAAAtoC,SAAA,EAD8C,CAGvD,OAAOooC,EAfyB,CAxClC,IAAIM,EAAgBA,QAAsB,CAACnkC,CAAD,CAAO,CAC/C,KAAMkjC,GAAA,CAAW,QAAX,CAAN,CAD+C,CAI7CpvB,EAAAF,IAAA,CAAc,WAAd,CAAJ,GACEuwB,CADF,CACkBrwB,CAAArB,IAAA,CAAc,WAAd,CADlB,CAN4C,KA4DxC2xB,EAAyBT,CAAA,EA5De,CA6DxCU,EAAS,EAEbA,EAAA,CAAOf,EAAA7a,KAAP,CAAA,CAA4Bkb,CAAA,CAAmBS,CAAnB,CAC5BC,EAAA,CAAOf,EAAAgB,IAAP,CAAA,CAA2BX,CAAA,CAAmBS,CAAnB,CAC3BC,EAAA,CAAOf,EAAAiB,IAAP,CAAA,CAA2BZ,CAAA,CAAmBS,CAAnB,CAC3BC,EAAA,CAAOf,EAAAkB,GAAP,CAAA,CAA0Bb,CAAA,CAAmBS,CAAnB,CAC1BC,EAAA,CAAOf,EAAA5a,aAAP,CAAA,CAAoCib,CAAA,CAAmBU,CAAA,CAAOf,EAAAiB,IAAP,CAAnB,CAyGpC,OAAO,SAtFPE,QAAgB,CAACl3B,CAAD,CAAOu2B,CAAP,CAAqB,CACnC,IAAItwB;AAAe6wB,CAAArrC,eAAA,CAAsBuU,CAAtB,CAAA,CAA8B82B,CAAA,CAAO92B,CAAP,CAA9B,CAA6C,IAChE,IAAI,CAACiG,CAAL,CACE,KAAM0vB,GAAA,CAAW,UAAX,CAEF31B,CAFE,CAEIu2B,CAFJ,CAAN,CAIF,GAAqB,IAArB,GAAIA,CAAJ,EAA6BA,CAA7B,GAA8C5rC,CAA9C,EAA4E,EAA5E,GAA2D4rC,CAA3D,CACE,MAAOA,EAIT,IAA4B,QAA5B,GAAI,MAAOA,EAAX,CACE,KAAMZ,GAAA,CAAW,OAAX,CAEF31B,CAFE,CAAN,CAIF,MAAO,KAAIiG,CAAJ,CAAgBswB,CAAhB,CAjB4B,CAsF9B,YAzBP/Q,QAAmB,CAACxlB,CAAD,CAAOm3B,CAAP,CAAqB,CACtC,GAAqB,IAArB,GAAIA,CAAJ,EAA6BA,CAA7B,GAA8CxsC,CAA9C,EAA4E,EAA5E,GAA2DwsC,CAA3D,CACE,MAAOA,EAET,KAAI/gC,EAAe0gC,CAAArrC,eAAA,CAAsBuU,CAAtB,CAAA,CAA8B82B,CAAA,CAAO92B,CAAP,CAA9B,CAA6C,IAChE,IAAI5J,CAAJ,EAAmB+gC,CAAnB,WAA2C/gC,EAA3C,CACE,MAAO+gC,EAAAX,qBAAA,EAKT,IAAIx2B,CAAJ,GAAa+1B,EAAA5a,aAAb,CAAwC,CAzIpC6L,IAAAA,EAAY7C,EAAA,CA0ImBgT,CA1IRjpC,SAAA,EAAX,CAAZ84B,CACAh7B,CADAg7B,CACGla,CADHka,CACMoQ,EAAU,CAAA,CAEfprC,EAAA,CAAI,CAAT,KAAY8gB,CAAZ,CAAgBkpB,CAAAhrC,OAAhB,CAA6CgB,CAA7C,CAAiD8gB,CAAjD,CAAoD9gB,CAAA,EAApD,CACE,GAbc,MAAhB,GAaegqC,CAAAN,CAAqB1pC,CAArB0pC,CAbf,CACSrV,EAAA,CAY+B2G,CAZ/B,CADT,CAaegP,CAAAN,CAAqB1pC,CAArB0pC,CATJthC,KAAA,CAS6B4yB,CAThB1c,KAAb,CAST,CAAkD,CAChD8sB,CAAA,CAAU,CAAA,CACV,MAFgD,CAKpD,GAAIA,CAAJ,CAEE,IAAKprC,CAAO,CAAH,CAAG,CAAA8gB,CAAA,CAAImpB,CAAAjrC,OAAhB,CAA6CgB,CAA7C,CAAiD8gB,CAAjD,CAAoD9gB,CAAA,EAApD,CACE,GArBY,MAAhB,GAqBiBiqC,CAAAP,CAAqB1pC,CAArB0pC,CArBjB,CACSrV,EAAA,CAoBiC2G,CApBjC,CADT,CAqBiBiP,CAAAP,CAAqB1pC,CAArB0pC,CAjBNthC,KAAA,CAiB+B4yB,CAjBlB1c,KAAb,CAiBP,CAAkD,CAChD8sB,CAAA;AAAU,CAAA,CACV,MAFgD,CA8HpD,GAxHKA,CAwHL,CACE,MAAOD,EAEP,MAAMxB,GAAA,CAAW,UAAX,CAEFwB,CAAAjpC,SAAA,EAFE,CAAN,CAJoC,CAQjC,GAAI8R,CAAJ,GAAa+1B,EAAA7a,KAAb,CACL,MAAO0b,EAAA,CAAcO,CAAd,CAET,MAAMxB,GAAA,CAAW,QAAX,CAAN,CAtBsC,CAyBjC,SAhDPlQ,QAAgB,CAAC0R,CAAD,CAAe,CAC7B,MAAIA,EAAJ,WAA4BN,EAA5B,CACSM,CAAAX,qBAAA,EADT,CAGSW,CAJoB,CAgDxB,CA5KqC,CAAlC,CAtEkB,CAmhBhCn7B,QAASA,GAAY,EAAG,CACtB,IAAIq7B,EAAU,CAAA,CAad,KAAAA,QAAA,CAAeC,QAAS,CAACnrC,CAAD,CAAQ,CAC1Be,SAAAlC,OAAJ,GACEqsC,CADF,CACY,CAAC,CAAClrC,CADd,CAGA,OAAOkrC,EAJuB,CAsDhC,KAAA1yB,KAAA,CAAY,CAAC,QAAD,CAAW,UAAX,CAAuB,cAAvB,CAAuC,QAAQ,CAC7C8K,CAD6C,CACnCnH,CADmC,CACvBivB,CADuB,CACT,CAGhD,GAAIF,CAAJ,EAAe/uB,CAAAlF,KAAf,EAA4D,CAA5D,CAAgCkF,CAAAkvB,iBAAhC,CACE,KAAM7B,GAAA,CAAW,UAAX,CAAN,CAMF,IAAI8B,EAAMznC,EAAA,CAAY+lC,EAAZ,CAaV0B,EAAAC,UAAA,CAAgBC,QAAS,EAAG,CAC1B,MAAON,EADmB,CAG5BI,EAAAP,QAAA,CAAcK,CAAAL,QACdO,EAAAjS,WAAA,CAAiB+R,CAAA/R,WACjBiS,EAAAhS,QAAA,CAAc8R,CAAA9R,QAET4R,EAAL,GACEI,CAAAP,QACA;AADcO,CAAAjS,WACd,CAD+BoS,QAAQ,CAAC53B,CAAD,CAAO7T,CAAP,CAAc,CAAE,MAAOA,EAAT,CACrD,CAAAsrC,CAAAhS,QAAA,CAAc/3B,EAFhB,CAwBA+pC,EAAAI,QAAA,CAAcC,QAAmB,CAAC93B,CAAD,CAAO60B,CAAP,CAAa,CAC5C,IAAIz2B,EAASqR,CAAA,CAAOolB,CAAP,CACb,OAAIz2B,EAAAoY,QAAJ,EAAsBpY,CAAAoI,SAAtB,CACSpI,CADT,CAGS25B,QAA0B,CAACjnC,CAAD,CAAOgV,CAAP,CAAe,CAC9C,MAAO2xB,EAAAjS,WAAA,CAAexlB,CAAf,CAAqB5B,CAAA,CAAOtN,CAAP,CAAagV,CAAb,CAArB,CADuC,CALN,CAtDE,KAoT5CjU,EAAQ4lC,CAAAI,QApToC,CAqT5CrS,EAAaiS,CAAAjS,WArT+B,CAsT5C0R,EAAUO,CAAAP,QAEd9rC,EAAA,CAAQ2qC,EAAR,CAAsB,QAAS,CAACiC,CAAD,CAAY/jC,CAAZ,CAAkB,CAC/C,IAAIgkC,EAAQjmC,CAAA,CAAUiC,CAAV,CACZwjC,EAAA,CAAIj7B,EAAA,CAAU,WAAV,CAAwBy7B,CAAxB,CAAJ,CAAA,CAAsC,QAAS,CAACpD,CAAD,CAAO,CACpD,MAAOhjC,EAAA,CAAMmmC,CAAN,CAAiBnD,CAAjB,CAD6C,CAGtD4C,EAAA,CAAIj7B,EAAA,CAAU,cAAV,CAA2By7B,CAA3B,CAAJ,CAAA,CAAyC,QAAS,CAAC9rC,CAAD,CAAQ,CACxD,MAAOq5B,EAAA,CAAWwS,CAAX,CAAsB7rC,CAAtB,CADiD,CAG1DsrC,EAAA,CAAIj7B,EAAA,CAAU,WAAV,CAAwBy7B,CAAxB,CAAJ,CAAA,CAAsC,QAAS,CAAC9rC,CAAD,CAAQ,CACrD,MAAO+qC,EAAA,CAAQc,CAAR,CAAmB7rC,CAAnB,CAD8C,CARR,CAAjD,CAaA,OAAOsrC,EArUyC,CADtC,CApEU,CA6ZxBv7B,QAASA,GAAgB,EAAG,CAC1B,IAAAyI,KAAA,CAAY,CAAC,SAAD,CAAY,WAAZ,CAAyB,QAAQ,CAAC0C,CAAD,CAAUgF,CAAV,CAAqB,CAAA,IAC5D6rB,EAAe,EAD6C,CAE5DC,EACEhrC,CAAA,CAAI,CAAC,eAAAiH,KAAA,CAAqBpC,CAAA,CAAWomC,CAAA/wB,CAAAgxB,UAAAD;AAAqB,EAArBA,WAAX,CAArB,CAAD,EAAyE,EAAzE,EAA6E,CAA7E,CAAJ,CAH0D,CAI5DE,EAAQ,QAAAljC,KAAA,CAAegjC,CAAA/wB,CAAAgxB,UAAAD,EAAqB,EAArBA,WAAf,CAJoD,CAK5D1tC,EAAW2hB,CAAA,CAAU,CAAV,CAAX3hB,EAA2B,EALiC,CAM5D6tC,EAAe7tC,CAAA6tC,aAN6C,CAO5DC,CAP4D,CAQ5DC,EAAc,6BAR8C,CAS5DC,EAAYhuC,CAAA64B,KAAZmV,EAA6BhuC,CAAA64B,KAAAoV,MAT+B,CAU5DC,EAAc,CAAA,CAV8C,CAW5DC,EAAa,CAAA,CAGjB,IAAIH,CAAJ,CAAe,CACb,IAAIhqC,IAAIA,CAAR,GAAgBgqC,EAAhB,CACE,GAAG/lC,CAAH,CAAW8lC,CAAArkC,KAAA,CAAiB1F,CAAjB,CAAX,CAAmC,CACjC8pC,CAAA,CAAe7lC,CAAA,CAAM,CAAN,CACf6lC,EAAA,CAAeA,CAAAnlB,OAAA,CAAoB,CAApB,CAAuB,CAAvB,CAAAzW,YAAA,EAAf,CAAyD47B,CAAAnlB,OAAA,CAAoB,CAApB,CACzD,MAHiC,CAOjCmlB,CAAJ,GACEA,CADF,CACkB,eADlB,EACqCE,EADrC,EACmD,QADnD,CAIAE,EAAA,CAAc,CAAC,EAAG,YAAH,EAAmBF,EAAnB,EAAkCF,CAAlC,CAAiD,YAAjD,EAAiEE,EAAjE,CACfG,EAAA,CAAc,CAAC,EAAG,WAAH,EAAkBH,EAAlB,EAAiCF,CAAjC,CAAgD,WAAhD,EAA+DE,EAA/D,CAEXP,EAAAA,CAAJ,EAAiBS,CAAjB,EAA+BC,CAA/B,GACED,CACA,CADc1tC,CAAA,CAASR,CAAA64B,KAAAoV,MAAAG,iBAAT,CACd,CAAAD,CAAA,CAAa3tC,CAAA,CAASR,CAAA64B,KAAAoV,MAAAI,gBAAT,CAFf,CAhBa,CAuBf,MAAO,SAUI,EAAGrvB,CAAArC,CAAAqC,QAAH,EAAsBgB,CAAArD,CAAAqC,QAAAgB,UAAtB;AAA+D,CAA/D,CAAqDytB,CAArD,EAAsEG,CAAtE,CAVJ,YAYO,cAZP,EAYyBjxB,EAZzB,GAcQ,CAACkxB,CAdT,EAcwC,CAdxC,CAcyBA,CAdzB,WAeKS,QAAQ,CAAC12B,CAAD,CAAQ,CAIxB,GAAa,OAAb,EAAIA,CAAJ,EAAgC,CAAhC,EAAwBc,CAAxB,CAAmC,MAAO,CAAA,CAE1C,IAAIvV,CAAA,CAAYqqC,CAAA,CAAa51B,CAAb,CAAZ,CAAJ,CAAsC,CACpC,IAAI22B,EAASvuC,CAAAiU,cAAA,CAAuB,KAAvB,CACbu5B,EAAA,CAAa51B,CAAb,CAAA,CAAsB,IAAtB,CAA6BA,CAA7B,GAAsC22B,EAFF,CAKtC,MAAOf,EAAA,CAAa51B,CAAb,CAXiB,CAfrB,KA4BA7R,EAAA,EA5BA,cA6BS+nC,CA7BT,aA8BSI,CA9BT,YA+BQC,CA/BR,SAgCIV,CAhCJ,MAiCE/0B,CAjCF,kBAkCam1B,CAlCb,CArCyD,CAAtD,CADc,CA6E5Bn8B,QAASA,GAAgB,EAAG,CAC1B,IAAAuI,KAAA,CAAY,CAAC,YAAD,CAAe,UAAf,CAA2B,IAA3B,CAAiC,mBAAjC,CACP,QAAQ,CAAC4C,CAAD,CAAe0X,CAAf,CAA2BC,CAA3B,CAAiCvQ,CAAjC,CAAoD,CA6B/D4T,QAASA,EAAO,CAACxxB,CAAD,CAAKib,CAAL,CAAY8Z,CAAZ,CAAyB,CAAA,IACnChE,EAAW5C,CAAApT,MAAA,EADwB,CAEnCgV,EAAUgB,CAAAhB,QAFyB,CAGnCmF,EAAan4B,CAAA,CAAUg4B,CAAV,CAAbG,EAAuC,CAACH,CAG5C7Z,EAAA,CAAYgT,CAAAnT,MAAA,CAAe,QAAQ,EAAG,CACpC,GAAI,CACFgW,CAAAC,QAAA,CAAiBhxB,CAAA,EAAjB,CADE,CAEF,MAAMuB,CAAN,CAAS,CACTwvB,CAAAvC,OAAA,CAAgBjtB,CAAhB,CACA,CAAAqc,CAAA,CAAkBrc,CAAlB,CAFS,CAFX,OAMQ,CACN,OAAO4mC,CAAA,CAAUpY,CAAAqY,YAAV,CADD,CAIHlT,CAAL;AAAgB1e,CAAAtS,OAAA,EAXoB,CAA1B,CAYT+W,CAZS,CAcZ8U,EAAAqY,YAAA,CAAsBltB,CACtBitB,EAAA,CAAUjtB,CAAV,CAAA,CAAuB6V,CAEvB,OAAOhB,EAvBgC,CA5BzC,IAAIoY,EAAY,EAmEhB3W,EAAArW,OAAA,CAAiBktB,QAAQ,CAACtY,CAAD,CAAU,CACjC,MAAIA,EAAJ,EAAeA,CAAAqY,YAAf,GAAsCD,EAAtC,EACEA,CAAA,CAAUpY,CAAAqY,YAAV,CAAA5Z,OAAA,CAAsC,UAAtC,CAEO,CADP,OAAO2Z,CAAA,CAAUpY,CAAAqY,YAAV,CACA,CAAAla,CAAAnT,MAAAI,OAAA,CAAsB4U,CAAAqY,YAAtB,CAHT,EAKO,CAAA,CAN0B,CASnC,OAAO5W,EA7EwD,CADrD,CADc,CAkJ5B4B,QAASA,GAAU,CAAC7a,CAAD,CAAM+vB,CAAN,CAAY,CAC7B,IAAI/uB,EAAOhB,CAEPlG,EAAJ,GAGEk2B,CAAAh4B,aAAA,CAA4B,MAA5B,CAAoCgJ,CAApC,CACA,CAAAA,CAAA,CAAOgvB,CAAAhvB,KAJT,CAOAgvB,EAAAh4B,aAAA,CAA4B,MAA5B,CAAoCgJ,CAApC,CAGA,OAAO,MACCgvB,CAAAhvB,KADD,UAEKgvB,CAAAlV,SAAA,CAA0BkV,CAAAlV,SAAAxxB,QAAA,CAAgC,IAAhC,CAAsC,EAAtC,CAA1B,CAAsE,EAF3E,MAGC0mC,CAAAv3B,KAHD,QAIGu3B,CAAAzR,OAAA,CAAwByR,CAAAzR,OAAAj1B,QAAA,CAA8B,KAA9B,CAAqC,EAArC,CAAxB,CAAmE,EAJtE,MAKC0mC,CAAA5xB,KAAA,CAAsB4xB,CAAA5xB,KAAA9U,QAAA,CAA4B,IAA5B,CAAkC,EAAlC,CAAtB,CAA8D,EAL/D,UAMK0mC,CAAAnS,SANL,MAOCmS,CAAAjS,KAPD;SAQ4C,GACvC,GADCiS,CAAA3R,SAAAz3B,OAAA,CAA+B,CAA/B,CACD,CAANopC,CAAA3R,SAAM,CACN,GADM,CACA2R,CAAA3R,SAVL,CAbsB,CAkC/BtH,QAASA,GAAe,CAACkZ,CAAD,CAAa,CAC/Bn7B,CAAAA,CAAUlT,CAAA,CAASquC,CAAT,CAAD,CAAyBpV,EAAA,CAAWoV,CAAX,CAAzB,CAAkDA,CAC/D,OAAQn7B,EAAAgmB,SAAR,GAA4BoV,EAAApV,SAA5B,EACQhmB,CAAA2D,KADR,GACwBy3B,EAAAz3B,KAHW,CA8CrC1F,QAASA,GAAe,EAAE,CACxB,IAAAsI,KAAA,CAAY/W,EAAA,CAAQnD,CAAR,CADY,CAgG1B6Q,QAASA,GAAe,CAAC3G,CAAD,CAAW,CAWjC+oB,QAASA,EAAQ,CAACzpB,CAAD,CAAOkD,CAAP,CAAgB,CAC/B,GAAGpJ,CAAA,CAASkG,CAAT,CAAH,CAAmB,CACjB,IAAIwlC,EAAU,EACdruC,EAAA,CAAQ6I,CAAR,CAAc,QAAQ,CAACmJ,CAAD,CAAS7R,CAAT,CAAc,CAClCkuC,CAAA,CAAQluC,CAAR,CAAA,CAAemyB,CAAA,CAASnyB,CAAT,CAAc6R,CAAd,CADmB,CAApC,CAGA,OAAOq8B,EALU,CAOjB,MAAO9kC,EAAAwC,QAAA,CAAiBlD,CAAjB,CAAwBylC,CAAxB,CAAgCviC,CAAhC,CARsB,CAVjC,IAAIuiC,EAAS,QAqBb,KAAAhc,SAAA,CAAgBA,CAEhB,KAAA/Y,KAAA,CAAY,CAAC,WAAD,CAAc,QAAQ,CAAC4B,CAAD,CAAY,CAC5C,MAAO,SAAQ,CAACtS,CAAD,CAAO,CACpB,MAAOsS,EAAArB,IAAA,CAAcjR,CAAd,CAAqBylC,CAArB,CADa,CADsB,CAAlC,CAoBZhc,EAAA,CAAS,UAAT,CAAqBic,EAArB,CACAjc,EAAA,CAAS,MAAT,CAAiBkc,EAAjB,CACAlc,EAAA,CAAS,QAAT,CAAmBmc,EAAnB,CACAnc,EAAA,CAAS,MAAT,CAAiBoc,EAAjB,CACApc,EAAA,CAAS,SAAT,CAAoBqc,EAApB,CACArc,EAAA,CAAS,WAAT,CAAsBsc,EAAtB,CACAtc,EAAA,CAAS,QAAT,CAAmBuc,EAAnB,CACAvc,EAAA,CAAS,SAAT;AAAoBwc,EAApB,CACAxc,EAAA,CAAS,WAAT,CAAsByc,EAAtB,CApDiC,CAwKnCN,QAASA,GAAY,EAAG,CACtB,MAAO,SAAQ,CAAC5qC,CAAD,CAAQ2uB,CAAR,CAAoBwc,CAApB,CAAgC,CAC7C,GAAI,CAACjvC,CAAA,CAAQ8D,CAAR,CAAL,CAAqB,MAAOA,EADiB,KAGzCorC,EAAiB,MAAOD,EAHiB,CAIzCE,EAAa,EAEjBA,EAAAvxB,MAAA,CAAmBwxB,QAAQ,CAACpuC,CAAD,CAAQ,CACjC,IAAK,IAAIkT,EAAI,CAAb,CAAgBA,CAAhB,CAAoBi7B,CAAAtvC,OAApB,CAAuCqU,CAAA,EAAvC,CACE,GAAG,CAACi7B,CAAA,CAAWj7B,CAAX,CAAA,CAAclT,CAAd,CAAJ,CACE,MAAO,CAAA,CAGX,OAAO,CAAA,CAN0B,CASZ,WAAvB,GAAIkuC,CAAJ,GAEID,CAFJ,CACyB,SAAvB,GAAIC,CAAJ,EAAoCD,CAApC,CACeA,QAAQ,CAACtvC,CAAD,CAAM6vB,CAAN,CAAY,CAC/B,MAAOtlB,GAAAlF,OAAA,CAAerF,CAAf,CAAoB6vB,CAApB,CADwB,CADnC,CAKeyf,QAAQ,CAACtvC,CAAD,CAAM6vB,CAAN,CAAY,CAC/B,GAAI7vB,CAAJ,EAAW6vB,CAAX,EAAkC,QAAlC,GAAmB,MAAO7vB,EAA1B,EAA8D,QAA9D,GAA8C,MAAO6vB,EAArD,CAAwE,CACtE,IAAK6f,IAAIA,CAAT,GAAmB1vC,EAAnB,CACE,GAAyB,GAAzB,GAAI0vC,CAAAtqC,OAAA,CAAc,CAAd,CAAJ,EAAgCzE,EAAAC,KAAA,CAAoBZ,CAApB,CAAyB0vC,CAAzB,CAAhC,EACIJ,CAAA,CAAWtvC,CAAA,CAAI0vC,CAAJ,CAAX,CAAwB7f,CAAA,CAAK6f,CAAL,CAAxB,CADJ,CAEE,MAAO,CAAA,CAGX,OAAO,CAAA,CAP+D,CASxE7f,CAAA,CAAQ7kB,CAAA,EAAAA,CAAG6kB,CAAH7kB,aAAA,EACR,OAA+C,EAA/C,CAAQA,CAAA,EAAAA,CAAGhL,CAAHgL,aAAA,EAAA9G,QAAA,CAA8B2rB,CAA9B,CAXuB,CANrC,CAsBA,KAAIkN,EAASA,QAAQ,CAAC/8B,CAAD,CAAM6vB,CAAN,CAAW,CAC9B,GAAmB,QAAnB,EAAI,MAAOA,EAAX;AAAkD,GAAlD,GAA+BA,CAAAzqB,OAAA,CAAY,CAAZ,CAA/B,CACE,MAAO,CAAC23B,CAAA,CAAO/8B,CAAP,CAAY6vB,CAAAtH,OAAA,CAAY,CAAZ,CAAZ,CAEV,QAAQ,MAAOvoB,EAAf,EACE,KAAK,SAAL,CACA,KAAK,QAAL,CACA,KAAK,QAAL,CACE,MAAOsvC,EAAA,CAAWtvC,CAAX,CAAgB6vB,CAAhB,CACT,MAAK,QAAL,CACE,OAAQ,MAAOA,EAAf,EACE,KAAK,QAAL,CACE,MAAOyf,EAAA,CAAWtvC,CAAX,CAAgB6vB,CAAhB,CACT,SACE,IAAM6f,IAAIA,CAAV,GAAoB1vC,EAApB,CACE,GAAyB,GAAzB,GAAI0vC,CAAAtqC,OAAA,CAAc,CAAd,CAAJ,EAAgC23B,CAAA,CAAO/8B,CAAA,CAAI0vC,CAAJ,CAAP,CAAoB7f,CAApB,CAAhC,CACE,MAAO,CAAA,CANf,CAWA,MAAO,CAAA,CACT,MAAK,OAAL,CACE,IAAU3uB,CAAV,CAAc,CAAd,CAAiBA,CAAjB,CAAqBlB,CAAAE,OAArB,CAAiCgB,CAAA,EAAjC,CACE,GAAI67B,CAAA,CAAO/8B,CAAA,CAAIkB,CAAJ,CAAP,CAAe2uB,CAAf,CAAJ,CACE,MAAO,CAAA,CAGX,OAAO,CAAA,CACT,SACE,MAAO,CAAA,CA1BX,CAJ8B,CAiChC,QAAQ,MAAOiD,EAAf,EACE,KAAK,SAAL,CACA,KAAK,QAAL,CACA,KAAK,QAAL,CAEEA,CAAA,CAAa,GAAGA,CAAH,CAEf,MAAK,QAAL,CAEE,IAAKryB,IAAIA,CAAT,GAAgBqyB,EAAhB,CACG,SAAQ,CAACrnB,CAAD,CAAO,CACiB,WAA/B,EAAI,MAAOqnB,EAAA,CAAWrnB,CAAX,CAAX,EACA+jC,CAAAzuC,KAAA,CAAgB,QAAQ,CAACM,CAAD,CAAQ,CAC9B,MAAO07B,EAAA,CAAe,GAAR;AAAAtxB,CAAA,CAAcpK,CAAd,CAAuBA,CAAvB,EAAgCA,CAAA,CAAMoK,CAAN,CAAvC,CAAqDqnB,CAAA,CAAWrnB,CAAX,CAArD,CADuB,CAAhC,CAFc,CAAf,CAAA,CAKEhL,CALF,CAOH,MACF,MAAK,UAAL,CACE+uC,CAAAzuC,KAAA,CAAgB+xB,CAAhB,CACA,MACF,SACE,MAAO3uB,EAtBX,CAwBIwrC,CAAAA,CAAW,EACf,KAAUp7B,CAAV,CAAc,CAAd,CAAiBA,CAAjB,CAAqBpQ,CAAAjE,OAArB,CAAmCqU,CAAA,EAAnC,CAAwC,CACtC,IAAIlT,EAAQ8C,CAAA,CAAMoQ,CAAN,CACRi7B,EAAAvxB,MAAA,CAAiB5c,CAAjB,CAAJ,EACEsuC,CAAA5uC,KAAA,CAAcM,CAAd,CAHoC,CAMxC,MAAOsuC,EArGsC,CADzB,CA0JxBd,QAASA,GAAc,CAACe,CAAD,CAAU,CAC/B,IAAIC,EAAUD,CAAAE,eACd,OAAO,SAAQ,CAACC,CAAD,CAASC,CAAT,CAAwB,CACjCjtC,CAAA,CAAYitC,CAAZ,CAAJ,GAAiCA,CAAjC,CAAkDH,CAAAI,aAAlD,CACA,OAAOC,GAAA,CAAaH,CAAb,CAAqBF,CAAAM,SAAA,CAAiB,CAAjB,CAArB,CAA0CN,CAAAO,UAA1C,CAA6DP,CAAAQ,YAA7D,CAAkF,CAAlF,CAAAvoC,QAAA,CACa,SADb,CACwBkoC,CADxB,CAF8B,CAFR,CA4DjCb,QAASA,GAAY,CAACS,CAAD,CAAU,CAC7B,IAAIC,EAAUD,CAAAE,eACd,OAAO,SAAQ,CAACQ,CAAD,CAASC,CAAT,CAAuB,CACpC,MAAOL,GAAA,CAAaI,CAAb,CAAqBT,CAAAM,SAAA,CAAiB,CAAjB,CAArB,CAA0CN,CAAAO,UAA1C,CAA6DP,CAAAQ,YAA7D,CACLE,CADK,CAD6B,CAFT,CAS/BL,QAASA,GAAY,CAACI,CAAD,CAASE,CAAT,CAAkBC,CAAlB,CAA4BC,CAA5B,CAAwCH,CAAxC,CAAsD,CACzE,GAAc,IAAd,EAAID,CAAJ,EAAsB,CAACK,QAAA,CAASL,CAAT,CAAvB,EAA2CrtC,CAAA,CAASqtC,CAAT,CAA3C,CAA6D,MAAO,EAEpE,KAAIM,EAAsB,CAAtBA,CAAaN,CACjBA;CAAA,CAASziB,IAAAgjB,IAAA,CAASP,CAAT,CAJgE,KAKrEQ,EAASR,CAATQ,CAAkB,EALmD,CAMrEC,EAAe,EANsD,CAOrEzoC,EAAQ,EAP6D,CASrE0oC,EAAc,CAAA,CAClB,IAA6B,EAA7B,GAAIF,CAAA5sC,QAAA,CAAe,GAAf,CAAJ,CAAgC,CAC9B,IAAI2D,EAAQipC,CAAAjpC,MAAA,CAAa,qBAAb,CACRA,EAAJ,EAAyB,GAAzB,EAAaA,CAAA,CAAM,CAAN,CAAb,EAAgCA,CAAA,CAAM,CAAN,CAAhC,CAA2C0oC,CAA3C,CAA0D,CAA1D,CACEO,CADF,CACW,GADX,EAGEC,CACA,CADeD,CACf,CAAAE,CAAA,CAAc,CAAA,CAJhB,CAF8B,CAUhC,GAAKA,CAAL,CA2CqB,CAAnB,CAAIT,CAAJ,GAAkC,EAAlC,CAAwBD,CAAxB,EAAgD,CAAhD,CAAuCA,CAAvC,IACES,CADF,CACiBT,CAAAW,QAAA,CAAeV,CAAf,CADjB,CA3CF,KAAkB,CACZW,CAAAA,CAAehxC,CAAA4wC,CAAA1oC,MAAA,CAAaioC,EAAb,CAAA,CAA0B,CAA1B,CAAAnwC,EAAgC,EAAhCA,QAGf6C,EAAA,CAAYwtC,CAAZ,CAAJ,GACEA,CADF,CACiB1iB,IAAAsjB,IAAA,CAAStjB,IAAAC,IAAA,CAAS0iB,CAAAY,QAAT,CAA0BF,CAA1B,CAAT,CAAiDV,CAAAa,QAAjD,CADjB,CAIIC,EAAAA,CAAMzjB,IAAAyjB,IAAA,CAAS,EAAT,CAAaf,CAAb,CAA4B,CAA5B,CACVD,EAAA,CAASziB,IAAA0jB,MAAA,CAAWjB,CAAX,CAAoBgB,CAApB,CAA0B,CAA1B,CAAT,CAAwCA,CACpCE,EAAAA,CAAYppC,CAAA,EAAAA,CAAKkoC,CAALloC,OAAA,CAAmBioC,EAAnB,CACZlT,EAAAA,CAAQqU,CAAA,CAAS,CAAT,CACZA,EAAA,CAAWA,CAAA,CAAS,CAAT,CAAX,EAA0B,EAEnBzmC,KAAAA,EAAM,CAANA,CACH0mC,EAASjB,CAAAkB,OADN3mC,CAEH4mC,EAAQnB,CAAAoB,MAEZ,IAAIzU,CAAAj9B,OAAJ,EAAqBuxC,CAArB,CAA8BE,CAA9B,CAEE,IADA5mC,CACK,CADCoyB,CAAAj9B,OACD,CADgBuxC,CAChB,CAAAvwC,CAAA,CAAI,CAAT,CAAYA,CAAZ,CAAgB6J,CAAhB,CAAqB7J,CAAA,EAArB,CAC0B,CAGxB,IAHK6J,CAGL,CAHW7J,CAGX,EAHcywC,CAGd,EAHmC,CAGnC,GAH6BzwC,CAG7B,GAFE6vC,CAEF,EAFkBN,CAElB,EAAAM,CAAA,EAAgB5T,CAAA/3B,OAAA,CAAalE,CAAb,CAIpB,KAAKA,CAAL,CAAS6J,CAAT,CAAc7J,CAAd,CAAkBi8B,CAAAj9B,OAAlB,CAAgCgB,CAAA,EAAhC,CACoC,CAGlC,IAHKi8B,CAAAj9B,OAGL,CAHoBgB,CAGpB;AAHuBuwC,CAGvB,EAH6C,CAG7C,GAHuCvwC,CAGvC,GAFE6vC,CAEF,EAFkBN,CAElB,EAAAM,CAAA,EAAgB5T,CAAA/3B,OAAA,CAAalE,CAAb,CAIlB,KAAA,CAAMswC,CAAAtxC,OAAN,CAAwBqwC,CAAxB,CAAA,CACEiB,CAAA,EAAY,GAGVjB,EAAJ,EAAqC,GAArC,GAAoBA,CAApB,GAA0CQ,CAA1C,EAA0DL,CAA1D,CAAuEc,CAAAjpB,OAAA,CAAgB,CAAhB,CAAmBgoB,CAAnB,CAAvE,CAxCgB,CAgDlBjoC,CAAAvH,KAAA,CAAW6vC,CAAA,CAAaJ,CAAAqB,OAAb,CAA8BrB,CAAAsB,OAAzC,CACAxpC,EAAAvH,KAAA,CAAWgwC,CAAX,CACAzoC,EAAAvH,KAAA,CAAW6vC,CAAA,CAAaJ,CAAAuB,OAAb,CAA8BvB,CAAAwB,OAAzC,CACA,OAAO1pC,EAAA3G,KAAA,CAAW,EAAX,CAvEkE,CA0E3EswC,QAASA,GAAS,CAACtW,CAAD,CAAMuW,CAAN,CAAc9+B,CAAd,CAAoB,CACpC,IAAI++B,EAAM,EACA,EAAV,CAAIxW,CAAJ,GACEwW,CACA,CADO,GACP,CAAAxW,CAAA,CAAM,CAACA,CAFT,CAKA,KADAA,CACA,CADM,EACN,CADWA,CACX,CAAMA,CAAAz7B,OAAN,CAAmBgyC,CAAnB,CAAA,CAA2BvW,CAAA,CAAM,GAAN,CAAYA,CACnCvoB,EAAJ,GACEuoB,CADF,CACQA,CAAApT,OAAA,CAAWoT,CAAAz7B,OAAX,CAAwBgyC,CAAxB,CADR,CAEA,OAAOC,EAAP,CAAaxW,CAVuB,CActCyW,QAASA,EAAU,CAACjpC,CAAD,CAAOmZ,CAAP,CAAazQ,CAAb,CAAqBuB,CAArB,CAA2B,CAC5CvB,CAAA,CAASA,CAAT,EAAmB,CACnB,OAAO,SAAQ,CAACwgC,CAAD,CAAO,CAChBhxC,CAAAA,CAAQgxC,CAAA,CAAK,KAAL,CAAalpC,CAAb,CAAA,EACZ,IAAa,CAAb,CAAI0I,CAAJ,EAAkBxQ,CAAlB,CAA0B,CAACwQ,CAA3B,CACExQ,CAAA,EAASwQ,CACG,EAAd,GAAIxQ,CAAJ,EAA8B,GAA9B,EAAmBwQ,CAAnB,GAAmCxQ,CAAnC,CAA2C,EAA3C,CACA,OAAO4wC,GAAA,CAAU5wC,CAAV,CAAiBihB,CAAjB,CAAuBlP,CAAvB,CALa,CAFsB,CAW9Ck/B,QAASA,GAAa,CAACnpC,CAAD,CAAOopC,CAAP,CAAkB,CACtC,MAAO,SAAQ,CAACF,CAAD,CAAOxC,CAAP,CAAgB,CAC7B,IAAIxuC,EAAQgxC,CAAA,CAAK,KAAL,CAAalpC,CAAb,CAAA,EAAZ,CACIiR,EAAMhN,EAAA,CAAUmlC,CAAA,CAAa,OAAb,CAAuBppC,CAAvB,CAA+BA,CAAzC,CAEV,OAAO0mC,EAAA,CAAQz1B,CAAR,CAAA,CAAa/Y,CAAb,CAJsB,CADO,CAvidD;AA8qdvCytC,QAASA,GAAU,CAACc,CAAD,CAAU,CAK3B4C,QAASA,EAAgB,CAACC,CAAD,CAAS,CAChC,IAAI5qC,CACJ,IAAIA,CAAJ,CAAY4qC,CAAA5qC,MAAA,CAAa6qC,CAAb,CAAZ,CAAyC,CACnCL,CAAAA,CAAO,IAAIttC,IAAJ,CAAS,CAAT,CAD4B,KAEnC4tC,EAAS,CAF0B,CAGnCC,EAAS,CAH0B,CAInCC,EAAahrC,CAAA,CAAM,CAAN,CAAA,CAAWwqC,CAAAS,eAAX,CAAiCT,CAAAU,YAJX,CAKnCC,EAAanrC,CAAA,CAAM,CAAN,CAAA,CAAWwqC,CAAAY,YAAX,CAA8BZ,CAAAa,SAE3CrrC,EAAA,CAAM,CAAN,CAAJ,GACE8qC,CACA,CADStwC,CAAA,CAAIwF,CAAA,CAAM,CAAN,CAAJ,CAAeA,CAAA,CAAM,EAAN,CAAf,CACT,CAAA+qC,CAAA,CAAQvwC,CAAA,CAAIwF,CAAA,CAAM,CAAN,CAAJ,CAAeA,CAAA,CAAM,EAAN,CAAf,CAFV,CAIAgrC,EAAAjyC,KAAA,CAAgByxC,CAAhB,CAAsBhwC,CAAA,CAAIwF,CAAA,CAAM,CAAN,CAAJ,CAAtB,CAAqCxF,CAAA,CAAIwF,CAAA,CAAM,CAAN,CAAJ,CAArC,CAAqD,CAArD,CAAwDxF,CAAA,CAAIwF,CAAA,CAAM,CAAN,CAAJ,CAAxD,CACI7F,EAAAA,CAAIK,CAAA,CAAIwF,CAAA,CAAM,CAAN,CAAJ,EAAc,CAAd,CAAJ7F,CAAuB2wC,CACvBQ,EAAAA,CAAI9wC,CAAA,CAAIwF,CAAA,CAAM,CAAN,CAAJ,EAAc,CAAd,CAAJsrC,CAAuBP,CACvBQ,EAAAA,CAAI/wC,CAAA,CAAIwF,CAAA,CAAM,CAAN,CAAJ,EAAc,CAAd,CACJwrC,EAAAA,CAAKxlB,IAAAylB,MAAA,CAA8C,GAA9C,CAAWC,UAAA,CAAW,IAAX,EAAmB1rC,CAAA,CAAM,CAAN,CAAnB,EAA6B,CAA7B,EAAX,CACTmrC,EAAApyC,KAAA,CAAgByxC,CAAhB,CAAsBrwC,CAAtB,CAAyBmxC,CAAzB,CAA4BC,CAA5B,CAA+BC,CAA/B,CAhBuC,CAmBzC,MAAOZ,EArByB,CAFlC,IAAIC,EAAgB,sGA2BpB,OAAO,SAAQ,CAACL,CAAD,CAAOmB,CAAP,CAAe,CAAA,IACxB3jB,EAAO,EADiB,CAExBvnB,EAAQ,EAFgB,CAGxBrC,CAHwB,CAGpB4B,CAER2rC,EAAA,CAASA,CAAT,EAAmB,YACnBA;CAAA,CAAS5D,CAAA6D,iBAAA,CAAyBD,CAAzB,CAAT,EAA6CA,CACzCpzC,EAAA,CAASiyC,CAAT,CAAJ,GAEIA,CAFJ,CACMqB,EAAAppC,KAAA,CAAmB+nC,CAAnB,CAAJ,CACShwC,CAAA,CAAIgwC,CAAJ,CADT,CAGSG,CAAA,CAAiBH,CAAjB,CAJX,CAQInvC,GAAA,CAASmvC,CAAT,CAAJ,GACEA,CADF,CACS,IAAIttC,IAAJ,CAASstC,CAAT,CADT,CAIA,IAAI,CAAClvC,EAAA,CAAOkvC,CAAP,CAAL,CACE,MAAOA,EAGT,KAAA,CAAMmB,CAAN,CAAA,CAEE,CADA3rC,CACA,CADQ8rC,EAAArqC,KAAA,CAAwBkqC,CAAxB,CACR,GACElrC,CACA,CADeA,CA9vbdhC,OAAA,CAAcH,EAAAvF,KAAA,CA8vbOiH,CA9vbP,CA8vbctG,CA9vbd,CAAd,CA+vbD,CAAAiyC,CAAA,CAASlrC,CAAAsV,IAAA,EAFX,GAIEtV,CAAAvH,KAAA,CAAWyyC,CAAX,CACA,CAAAA,CAAA,CAAS,IALX,CASFlzC,EAAA,CAAQgI,CAAR,CAAe,QAAQ,CAACjH,CAAD,CAAO,CAC5B4E,CAAA,CAAK2tC,EAAA,CAAavyC,CAAb,CACLwuB,EAAA,EAAQ5pB,CAAA,CAAKA,CAAA,CAAGosC,CAAH,CAASzC,CAAA6D,iBAAT,CAAL,CACKpyC,CAAAyG,QAAA,CAAc,UAAd,CAA0B,EAA1B,CAAAA,QAAA,CAAsC,KAAtC,CAA6C,GAA7C,CAHe,CAA9B,CAMA,OAAO+nB,EAxCqB,CA9BH,CAuG7Bmf,QAASA,GAAU,EAAG,CACpB,MAAO,SAAQ,CAAC6E,CAAD,CAAS,CACtB,MAAOptC,GAAA,CAAOotC,CAAP,CAAe,CAAA,CAAf,CADe,CADJ,CAiGtB5E,QAASA,GAAa,EAAE,CACtB,MAAO,SAAQ,CAAC6E,CAAD,CAAQC,CAAR,CAAe,CAC5B,GAAI,CAAC1zC,CAAA,CAAQyzC,CAAR,CAAL,EAAuB,CAAC1zC,CAAA,CAAS0zC,CAAT,CAAxB,CAAyC,MAAOA,EAG9CC,EAAA,CAD8BC,QAAhC,GAAInmB,IAAAgjB,IAAA,CAASpuB,MAAA,CAAOsxB,CAAP,CAAT,CAAJ,CACUtxB,MAAA,CAAOsxB,CAAP,CADV,CAGU1xC,CAAA,CAAI0xC,CAAJ,CAGV,IAAI3zC,CAAA,CAAS0zC,CAAT,CAAJ,CAEE,MAAIC,EAAJ,CACkB,CAAT,EAAAA,CAAA,CAAaD,CAAA3tC,MAAA,CAAY,CAAZ,CAAe4tC,CAAf,CAAb,CAAqCD,CAAA3tC,MAAA,CAAY4tC,CAAZ,CAAmBD,CAAA5zC,OAAnB,CAD9C;AAGS,EAdiB,KAkBxB+zC,EAAM,EAlBkB,CAmB1B/yC,CAnB0B,CAmBvB8gB,CAGD+xB,EAAJ,CAAYD,CAAA5zC,OAAZ,CACE6zC,CADF,CACUD,CAAA5zC,OADV,CAES6zC,CAFT,CAEiB,CAACD,CAAA5zC,OAFlB,GAGE6zC,CAHF,CAGU,CAACD,CAAA5zC,OAHX,CAKY,EAAZ,CAAI6zC,CAAJ,EACE7yC,CACA,CADI,CACJ,CAAA8gB,CAAA,CAAI+xB,CAFN,GAIE7yC,CACA,CADI4yC,CAAA5zC,OACJ,CADmB6zC,CACnB,CAAA/xB,CAAA,CAAI8xB,CAAA5zC,OALN,CAQA,KAAA,CAAOgB,CAAP,CAAS8gB,CAAT,CAAY9gB,CAAA,EAAZ,CACE+yC,CAAAlzC,KAAA,CAAS+yC,CAAA,CAAM5yC,CAAN,CAAT,CAGF,OAAO+yC,EAvCqB,CADR,CAwJxB7E,QAASA,GAAa,CAACzqB,CAAD,CAAQ,CAC5B,MAAO,SAAQ,CAACxgB,CAAD,CAAQ+vC,CAAR,CAAuBC,CAAvB,CAAqC,CAkClDC,QAASA,EAAiB,CAACC,CAAD,CAAOC,CAAP,CAAmB,CAC3C,MAAOttC,GAAA,CAAUstC,CAAV,CACA,CAAD,QAAQ,CAAC3oB,CAAD,CAAGC,CAAH,CAAK,CAAC,MAAOyoB,EAAA,CAAKzoB,CAAL,CAAOD,CAAP,CAAR,CAAZ,CACD0oB,CAHqC,CAK7ChpB,QAASA,EAAO,CAACkpB,CAAD,CAAKC,CAAL,CAAQ,CACtB,IAAIhvC,EAAK,MAAO+uC,EAAhB,CACI9uC,EAAK,MAAO+uC,EAChB,OAAIhvC,EAAJ,EAAUC,CAAV,EACY,QAIV,EAJID,CAIJ,GAHG+uC,CACA,CADKA,CAAAvpC,YAAA,EACL,CAAAwpC,CAAA,CAAKA,CAAAxpC,YAAA,EAER,EAAIupC,CAAJ,GAAWC,CAAX,CAAsB,CAAtB,CACOD,CAAA,CAAKC,CAAL,CAAW,EAAX,CAAe,CANxB,EAQShvC,CAAA,CAAKC,CAAL,CAAW,EAAX,CAAe,CAXF,CArCxB,GADI,CAACpF,CAAA,CAAQ8D,CAAR,CACL,EAAI,CAAC+vC,CAAL,CAAoB,MAAO/vC,EAC3B+vC,EAAA,CAAgB7zC,CAAA,CAAQ6zC,CAAR,CAAA,CAAyBA,CAAzB,CAAwC,CAACA,CAAD,CACxDA,EAAA,CAAgBnwC,EAAA,CAAImwC,CAAJ,CAAmB,QAAQ,CAACO,CAAD,CAAW,CAAA,IAChDH,EAAa,CAAA,CADmC,CAC5Bl6B,EAAMq6B,CAANr6B,EAAmBxX,EAC3C,IAAIxC,CAAA,CAASq0C,CAAT,CAAJ,CAAyB,CACvB,GAA4B,GAA5B,EAAKA,CAAArvC,OAAA,CAAiB,CAAjB,CAAL,EAA0D,GAA1D,EAAmCqvC,CAAArvC,OAAA,CAAiB,CAAjB,CAAnC,CACEkvC,CACA,CADoC,GACpC,EADaG,CAAArvC,OAAA,CAAiB,CAAjB,CACb;AAAAqvC,CAAA,CAAYA,CAAA1zB,UAAA,CAAoB,CAApB,CAEd3G,EAAA,CAAMuK,CAAA,CAAO8vB,CAAP,CACN,IAAIr6B,CAAAsB,SAAJ,CAAkB,CAChB,IAAIjb,EAAM2Z,CAAA,EACV,OAAOg6B,EAAA,CAAkB,QAAQ,CAACzoB,CAAD,CAAGC,CAAH,CAAM,CACrC,MAAOP,EAAA,CAAQM,CAAA,CAAElrB,CAAF,CAAR,CAAgBmrB,CAAA,CAAEnrB,CAAF,CAAhB,CAD8B,CAAhC,CAEJ6zC,CAFI,CAFS,CANK,CAazB,MAAOF,EAAA,CAAkB,QAAQ,CAACzoB,CAAD,CAAGC,CAAH,CAAK,CACpC,MAAOP,EAAA,CAAQjR,CAAA,CAAIuR,CAAJ,CAAR,CAAevR,CAAA,CAAIwR,CAAJ,CAAf,CAD6B,CAA/B,CAEJ0oB,CAFI,CAf6C,CAAtC,CAoBhB,KADA,IAAII,EAAY,EAAhB,CACUxzC,EAAI,CAAd,CAAiBA,CAAjB,CAAqBiD,CAAAjE,OAArB,CAAmCgB,CAAA,EAAnC,CAA0CwzC,CAAA3zC,KAAA,CAAeoD,CAAA,CAAMjD,CAAN,CAAf,CAC1C,OAAOwzC,EAAA1zC,KAAA,CAAeozC,CAAA,CAEtB9E,QAAmB,CAAChqC,CAAD,CAAKC,CAAL,CAAQ,CACzB,IAAM,IAAIrE,EAAI,CAAd,CAAiBA,CAAjB,CAAqBgzC,CAAAh0C,OAArB,CAA2CgB,CAAA,EAA3C,CAAgD,CAC9C,IAAImzC,EAAOH,CAAA,CAAchzC,CAAd,CAAA,CAAiBoE,CAAjB,CAAqBC,CAArB,CACX,IAAa,CAAb,GAAI8uC,CAAJ,CAAgB,MAAOA,EAFuB,CAIhD,MAAO,EALkB,CAFL,CAA8BF,CAA9B,CAAf,CAzB2C,CADxB,CAyD9BQ,QAASA,GAAW,CAACjnC,CAAD,CAAY,CAC1BhN,CAAA,CAAWgN,CAAX,CAAJ,GACEA,CADF,CACc,MACJA,CADI,CADd,CAKAA,EAAAwW,SAAA,CAAqBxW,CAAAwW,SAArB,EAA2C,IAC3C,OAAOphB,GAAA,CAAQ4K,CAAR,CAPuB,CAqfhCknC,QAASA,GAAc,CAACxtC,CAAD,CAAU4f,CAAV,CAAiBmF,CAAjB,CAAyBrH,CAAzB,CAAmC,CAqBxD+vB,QAASA,EAAc,CAACC,CAAD,CAAUC,CAAV,CAA8B,CACnDA,CAAA,CAAqBA,CAAA,CAAqB,GAArB,CAA2BpqC,EAAA,CAAWoqC,CAAX,CAA+B,GAA/B,CAA3B,CAAiE,EACtFjwB,EAAA0M,YAAA,CAAqBpqB,CAArB,EAA+B0tC,CAAA,CAAUE,EAAV,CAA0BC,EAAzD,EAAwEF,CAAxE,CACAjwB,EAAAkB,SAAA,CAAkB5e,CAAlB,EAA4B0tC,CAAA,CAAUG,EAAV,CAAwBD,EAApD,EAAqED,CAArE,CAHmD,CArBG,IACpDG,EAAO,IAD6C,CAEpDC,EAAa/tC,CAAA3E,OAAA,EAAAwhB,WAAA,CAA4B,MAA5B,CAAbkxB;AAAoDC,EAFA,CAGpDC,EAAe,CAHqC,CAIpDC,EAASJ,CAAAK,OAATD,CAAuB,EAJ6B,CAKpDE,EAAW,EAGfN,EAAAO,MAAA,CAAazuB,CAAA7d,KAAb,EAA2B6d,CAAA0uB,OAC3BR,EAAAS,OAAA,CAAc,CAAA,CACdT,EAAAU,UAAA,CAAiB,CAAA,CACjBV,EAAAW,OAAA,CAAc,CAAA,CACdX,EAAAY,SAAA,CAAgB,CAAA,CAEhBX,EAAAY,YAAA,CAAuBb,CAAvB,CAGA9tC,EAAA4e,SAAA,CAAiBgwB,EAAjB,CACAnB,EAAA,CAAe,CAAA,CAAf,CAkBAK,EAAAa,YAAA,CAAmBE,QAAQ,CAACC,CAAD,CAAU,CAGnC3qC,EAAA,CAAwB2qC,CAAAT,MAAxB,CAAuC,OAAvC,CACAD,EAAAz0C,KAAA,CAAcm1C,CAAd,CAEIA,EAAAT,MAAJ,GACEP,CAAA,CAAKgB,CAAAT,MAAL,CADF,CACwBS,CADxB,CANmC,CAoBrChB,EAAAiB,eAAA,CAAsBC,QAAQ,CAACF,CAAD,CAAU,CAClCA,CAAAT,MAAJ,EAAqBP,CAAA,CAAKgB,CAAAT,MAAL,CAArB,GAA6CS,CAA7C,EACE,OAAOhB,CAAA,CAAKgB,CAAAT,MAAL,CAETn1C,EAAA,CAAQg1C,CAAR,CAAgB,QAAQ,CAACe,CAAD,CAAQC,CAAR,CAAyB,CAC/CpB,CAAAqB,aAAA,CAAkBD,CAAlB,CAAmC,CAAA,CAAnC,CAAyCJ,CAAzC,CAD+C,CAAjD,CAIA9xC,GAAA,CAAYoxC,CAAZ,CAAsBU,CAAtB,CARsC,CAoBxChB,EAAAqB,aAAA,CAAoBC,QAAQ,CAACF,CAAD,CAAkBxB,CAAlB,CAA2BoB,CAA3B,CAAoC,CAC9D,IAAIG,EAAQf,CAAA,CAAOgB,CAAP,CAEZ,IAAIxB,CAAJ,CACMuB,CAAJ,GACEjyC,EAAA,CAAYiyC,CAAZ,CAAmBH,CAAnB,CACA,CAAKG,CAAAn2C,OAAL,GACEm1C,CAAA,EAQA,CAPKA,CAOL,GANER,CAAA,CAAeC,CAAf,CAEA,CADAI,CAAAW,OACA,CADc,CAAA,CACd,CAAAX,CAAAY,SAAA,CAAgB,CAAA,CAIlB,EAFAR,CAAA,CAAOgB,CAAP,CAEA,CAF0B,CAAA,CAE1B,CADAzB,CAAA,CAAe,CAAA,CAAf,CAAqByB,CAArB,CACA,CAAAnB,CAAAoB,aAAA,CAAwBD,CAAxB,CAAyC,CAAA,CAAzC,CAA+CpB,CAA/C,CATF,CAFF,CADF,KAgBO,CACAG,CAAL;AACER,CAAA,CAAeC,CAAf,CAEF,IAAIuB,CAAJ,CACE,IAv7dyB,EAu7dzB,EAv7dCnyC,EAAA,CAu7dYmyC,CAv7dZ,CAu7dmBH,CAv7dnB,CAu7dD,CAA8B,MAA9B,CADF,IAGEZ,EAAA,CAAOgB,CAAP,CAGA,CAH0BD,CAG1B,CAHkC,EAGlC,CAFAhB,CAAA,EAEA,CADAR,CAAA,CAAe,CAAA,CAAf,CAAsByB,CAAtB,CACA,CAAAnB,CAAAoB,aAAA,CAAwBD,CAAxB,CAAyC,CAAA,CAAzC,CAAgDpB,CAAhD,CAEFmB,EAAAt1C,KAAA,CAAWm1C,CAAX,CAEAhB,EAAAW,OAAA,CAAc,CAAA,CACdX,EAAAY,SAAA,CAAgB,CAAA,CAfX,CAnBuD,CAgDhEZ,EAAAuB,UAAA,CAAiBC,QAAQ,EAAG,CAC1B5xB,CAAA0M,YAAA,CAAqBpqB,CAArB,CAA8B4uC,EAA9B,CACAlxB,EAAAkB,SAAA,CAAkB5e,CAAlB,CAA2BuvC,EAA3B,CACAzB,EAAAS,OAAA,CAAc,CAAA,CACdT,EAAAU,UAAA,CAAiB,CAAA,CACjBT,EAAAsB,UAAA,EAL0B,CAsB5BvB,EAAA0B,aAAA,CAAoBC,QAAS,EAAG,CAC9B/xB,CAAA0M,YAAA,CAAqBpqB,CAArB,CAA8BuvC,EAA9B,CACA7xB,EAAAkB,SAAA,CAAkB5e,CAAlB,CAA2B4uC,EAA3B,CACAd,EAAAS,OAAA,CAAc,CAAA,CACdT,EAAAU,UAAA,CAAiB,CAAA,CACjBt1C,EAAA,CAAQk1C,CAAR,CAAkB,QAAQ,CAACU,CAAD,CAAU,CAClCA,CAAAU,aAAA,EADkC,CAApC,CAL8B,CAlJwB,CAwyB1DE,QAASA,GAAQ,CAACC,CAAD,CAAOC,CAAP,CAAsBC,CAAtB,CAAgC51C,CAAhC,CAAsC,CACrD01C,CAAAR,aAAA,CAAkBS,CAAlB,CAAiCC,CAAjC,CACA,OAAOA,EAAA,CAAW51C,CAAX,CAAmBxB,CAF2B,CAMvDq3C,QAASA,GAAwB,CAACH,CAAD,CAAOC,CAAP,CAAsB5vC,CAAtB,CAA+B,CAC9D,IAAI6vC,EAAW7vC,CAAAxD,KAAA,CAAa,UAAb,CACXX,EAAA,CAASg0C,CAAT,CAAJ,EAWEF,CAAAI,SAAAp2C,KAAA,CAVgBq2C,QAAQ,CAAC/1C,CAAD,CAAQ,CAG9B,GAAK01C,CAAAxB,OAAA,CAAYyB,CAAZ,CAAL,EAAoC,EAAAC,CAAAI,SAAA;AAAqBJ,CAAAK,YAArB,EAChCL,CAAAM,aADgC,CAApC,EAC+BN,CAAAO,aAD/B,CAKA,MAAOn2C,EAHL01C,EAAAR,aAAA,CAAkBS,CAAlB,CAAiC,CAAA,CAAjC,CAL4B,CAUhC,CAb4D,CAiBhES,QAASA,GAAa,CAACztC,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuBkzC,CAAvB,CAA6Bv5B,CAA7B,CAAuC2W,CAAvC,CAAiD,CACrE,IAAI8iB,EAAW7vC,CAAAxD,KAAA,CAAa,UAAb,CAAf,CACI8zC,EAActwC,CAAA,CAAQ,CAAR,CAAAswC,YADlB,CAC0CC,EAAU,EAKpD,IAAI,CAACn6B,CAAA6vB,QAAL,CAAuB,CACrB,IAAIuK,EAAY,CAAA,CAEhBxwC,EAAA4Y,GAAA,CAAW,kBAAX,CAA+B,QAAQ,CAAC5V,CAAD,CAAO,CAC5CwtC,CAAA,CAAY,CAAA,CADgC,CAA9C,CAIAxwC,EAAA4Y,GAAA,CAAW,gBAAX,CAA6B,QAAQ,EAAG,CACtC43B,CAAA,CAAY,CAAA,CACZl5B,EAAA,EAFsC,CAAxC,CAPqB,CAavB,IAAIA,EAAWA,QAAQ,CAACm5B,CAAD,CAAK,CAC1B,GAAID,CAAAA,CAAJ,CAAA,CACA,IAAIv2C,EAAQ+F,CAAAZ,IAAA,EAMZ,IAAI8R,CAAJ,EAAqC,OAArC,GAAapD,CAAA2iC,CAAA3iC,EAAMyiC,CAANziC,MAAb,EAAgD9N,CAAA,CAAQ,CAAR,CAAAswC,YAAhD,GAA2EA,CAA3E,CACEA,CAAA,CAActwC,CAAA,CAAQ,CAAR,CAAAswC,YADhB,KAYA,IAJI1wC,EAAA,CAAUnD,CAAAi0C,OAAV,EAAyB,GAAzB,CAIA,GAHFz2C,CAGE,CAHM+R,EAAA,CAAK/R,CAAL,CAGN,EAAA01C,CAAAgB,WAAA,GAAoB12C,CAApB,EAIC41C,CAJD,EAIuB,EAJvB,GAIa51C,CAJb,EAI6B,CAAC41C,CAAAO,aAJlC,CAKMxtC,CAAA+sB,QAAJ,CACEggB,CAAAiB,cAAA,CAAmB32C,CAAnB,CADF,CAGE2I,CAAAG,OAAA,CAAa,QAAQ,EAAG,CACtB4sC,CAAAiB,cAAA,CAAmB32C,CAAnB,CADsB,CAAxB,CA3BJ,CAD0B,CAqC5B;GAAImc,CAAA0wB,SAAA,CAAkB,OAAlB,CAAJ,CACE9mC,CAAA4Y,GAAA,CAAW,OAAX,CAAoBtB,CAApB,CADF,KAEO,CACL,IAAI+Y,CAAJ,CAEIwgB,EAAgBA,QAAQ,EAAG,CACxBxgB,CAAL,GACEA,CADF,CACYtD,CAAAnT,MAAA,CAAe,QAAQ,EAAG,CAClCtC,CAAA,EACA+Y,EAAA,CAAU,IAFwB,CAA1B,CADZ,CAD6B,CAS/BrwB,EAAA4Y,GAAA,CAAW,SAAX,CAAsB,QAAQ,CAACxI,CAAD,CAAQ,CAChC/W,CAAAA,CAAM+W,CAAA0gC,QAIE,GAAZ,GAAIz3C,CAAJ,GAAmB,EAAnB,CAAwBA,CAAxB,EAAqC,EAArC,CAA+BA,CAA/B,EAA6C,EAA7C,EAAmDA,CAAnD,EAAiE,EAAjE,EAA0DA,CAA1D,GAEAw3C,CAAA,EAPoC,CAAtC,CAWA,IAAIz6B,CAAA0wB,SAAA,CAAkB,OAAlB,CAAJ,CACE9mC,CAAA4Y,GAAA,CAAW,WAAX,CAAwBi4B,CAAxB,CAxBG,CA8BP7wC,CAAA4Y,GAAA,CAAW,QAAX,CAAqBtB,CAArB,CAEAq4B,EAAAoB,QAAA,CAAeC,QAAQ,EAAG,CACxBhxC,CAAAZ,IAAA,CAAYuwC,CAAAsB,SAAA,CAActB,CAAAgB,WAAd,CAAA,CAAiC,EAAjC,CAAsChB,CAAAgB,WAAlD,CADwB,CA3F2C,KAgGjEvH,EAAU3sC,CAAAy0C,UAIV9H,EAAJ,GAKE,CADA3oC,CACA,CADQ2oC,CAAA3oC,MAAA,CAAc,oBAAd,CACR,GACE2oC,CACA,CADcvrC,MAAJ,CAAW4C,CAAA,CAAM,CAAN,CAAX,CAAqBA,CAAA,CAAM,CAAN,CAArB,CACV,CAAA0wC,CAAA,CAAmBA,QAAQ,CAACl3C,CAAD,CAAQ,CACjC,MANKy1C,GAAA,CAASC,CAAT,CAAe,SAAf,CAA0BA,CAAAsB,SAAA,CAMDh3C,CANC,CAA1B,EAMgBmvC,CANkClmC,KAAA,CAMzBjJ,CANyB,CAAlD,CAMyBA,CANzB,CAK4B,CAFrC,EAMEk3C,CANF,CAMqBA,QAAQ,CAACl3C,CAAD,CAAQ,CACjC,IAAIm3C,EAAaxuC,CAAA0/B,MAAA,CAAY8G,CAAZ,CAEjB,IAAI,CAACgI,CAAL,EAAmB,CAACA,CAAAluC,KAApB,CACE,KAAMxK,EAAA,CAAO,WAAP,CAAA,CAAoB,UAApB;AACqD0wC,CADrD,CAEJgI,CAFI,CAEQrxC,EAAA,CAAYC,CAAZ,CAFR,CAAN,CAIF,MAjBK0vC,GAAA,CAASC,CAAT,CAAe,SAAf,CAA0BA,CAAAsB,SAAA,CAiBEh3C,CAjBF,CAA1B,EAiBgBm3C,CAjBkCluC,KAAA,CAiBtBjJ,CAjBsB,CAAlD,CAiB4BA,CAjB5B,CAS4B,CAarC,CADA01C,CAAA0B,YAAA13C,KAAA,CAAsBw3C,CAAtB,CACA,CAAAxB,CAAAI,SAAAp2C,KAAA,CAAmBw3C,CAAnB,CAxBF,CA4BA,IAAI10C,CAAA60C,YAAJ,CAAsB,CACpB,IAAIC,EAAYt2C,CAAA,CAAIwB,CAAA60C,YAAJ,CACZE,EAAAA,CAAqBA,QAAQ,CAACv3C,CAAD,CAAQ,CACvC,MAAOy1C,GAAA,CAASC,CAAT,CAAe,WAAf,CAA4BA,CAAAsB,SAAA,CAAch3C,CAAd,CAA5B,EAAoDA,CAAAnB,OAApD,EAAoEy4C,CAApE,CAA+Et3C,CAA/E,CADgC,CAIzC01C,EAAAI,SAAAp2C,KAAA,CAAmB63C,CAAnB,CACA7B,EAAA0B,YAAA13C,KAAA,CAAsB63C,CAAtB,CAPoB,CAWtB,GAAI/0C,CAAAg1C,YAAJ,CAAsB,CACpB,IAAIC,EAAYz2C,CAAA,CAAIwB,CAAAg1C,YAAJ,CACZE,EAAAA,CAAqBA,QAAQ,CAAC13C,CAAD,CAAQ,CACvC,MAAOy1C,GAAA,CAASC,CAAT,CAAe,WAAf,CAA4BA,CAAAsB,SAAA,CAAch3C,CAAd,CAA5B,EAAoDA,CAAAnB,OAApD,EAAoE44C,CAApE,CAA+Ez3C,CAA/E,CADgC,CAIzC01C,EAAAI,SAAAp2C,KAAA,CAAmBg4C,CAAnB,CACAhC,EAAA0B,YAAA13C,KAAA,CAAsBg4C,CAAtB,CAPoB,CA3I+C,CAyzCvEC,QAASA,GAAc,CAAC7vC,CAAD,CAAOiN,CAAP,CAAiB,CACtCjN,CAAA,CAAO,SAAP,CAAmBA,CACnB,OAAO,CAAC,UAAD,CAAa,QAAQ,CAAC2b,CAAD,CAAW,CAiFrCm0B,QAASA,EAAe,CAACzmB,CAAD,CAAUC,CAAV,CAAmB,CACzC,IAAIF,EAAS,EAAb,CAGQrxB,EAAI,CADZ,EAAA,CACA,IAAA,CAAeA,CAAf;AAAmBsxB,CAAAtyB,OAAnB,CAAmCgB,CAAA,EAAnC,CAAwC,CAEtC,IADA,IAAIwxB,EAAQF,CAAA,CAAQtxB,CAAR,CAAZ,CACQqT,EAAI,CAAZ,CAAeA,CAAf,CAAmBke,CAAAvyB,OAAnB,CAAmCqU,CAAA,EAAnC,CACE,GAAGme,CAAH,EAAYD,CAAA,CAAQle,CAAR,CAAZ,CAAwB,SAAS,CAEnCge,EAAAxxB,KAAA,CAAY2xB,CAAZ,CALsC,CAOxC,MAAOH,EAXkC,CAc3C2mB,QAASA,EAAa,CAAC5nB,CAAD,CAAW,CAC/B,GAAI,CAAAjxB,CAAA,CAAQixB,CAAR,CAAJ,CAEO,CAAA,GAAIlxB,CAAA,CAASkxB,CAAT,CAAJ,CACL,MAAOA,EAAAlpB,MAAA,CAAe,GAAf,CACF,IAAInF,CAAA,CAASquB,CAAT,CAAJ,CAAwB,CAAA,IACzB6nB,EAAU,EACd74C,EAAA,CAAQgxB,CAAR,CAAkB,QAAQ,CAACrqB,CAAD,CAAIiqB,CAAJ,CAAO,CAC3BjqB,CAAJ,GACEkyC,CADF,CACYA,CAAA7yC,OAAA,CAAe4qB,CAAA9oB,MAAA,CAAQ,GAAR,CAAf,CADZ,CAD+B,CAAjC,CAKA,OAAO+wC,EAPsB,CAFxB,CAWP,MAAO7nB,EAdwB,CA9FjC,MAAO,UACK,IADL,MAECrP,QAAQ,CAACjY,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CAiCnCu1C,QAASA,EAAkB,CAACD,CAAD,CAAUpe,CAAV,CAAiB,CAC1C,IAAIse,EAAcjyC,CAAAgD,KAAA,CAAa,cAAb,CAAdivC,EAA8C,EAAlD,CACIC,EAAkB,EACtBh5C,EAAA,CAAQ64C,CAAR,CAAiB,QAAS,CAAC5vC,CAAD,CAAY,CACpC,GAAY,CAAZ,CAAIwxB,CAAJ,EAAiBse,CAAA,CAAY9vC,CAAZ,CAAjB,CACE8vC,CAAA,CAAY9vC,CAAZ,CACA,EAD0B8vC,CAAA,CAAY9vC,CAAZ,CAC1B,EADoD,CACpD,EADyDwxB,CACzD,CAAIse,CAAA,CAAY9vC,CAAZ,CAAJ,GAA+B,EAAU,CAAV,CAAEwxB,CAAF,CAA/B,EACEue,CAAAv4C,KAAA,CAAqBwI,CAArB,CAJgC,CAAtC,CAQAnC,EAAAgD,KAAA,CAAa,cAAb,CAA6BivC,CAA7B,CACA,OAAOC,EAAA33C,KAAA,CAAqB,GAArB,CAZmC,CA8B5C43C,QAASA,EAAkB,CAACvR,CAAD,CAAS,CAClC,GAAiB,CAAA,CAAjB,GAAI5xB,CAAJ,EAAyBpM,CAAAwvC,OAAzB,CAAwC,CAAxC,GAA8CpjC,CAA9C,CAAwD,CACtD,IAAIqb,EAAaynB,CAAA,CAAalR,CAAb,EAAuB,EAAvB,CACjB,IAAI,CAACC,CAAL,CAAa,CA1Cf,IAAIxW;AAAa2nB,CAAA,CA2CF3nB,CA3CE,CAA2B,CAA3B,CACjB5tB,EAAAwtB,UAAA,CAAeI,CAAf,CAyCe,CAAb,IAEO,IAAI,CAACpsB,EAAA,CAAO2iC,CAAP,CAAcC,CAAd,CAAL,CAA4B,CAEnB9Y,IAAAA,EADG+pB,CAAA/pB,CAAa8Y,CAAb9Y,CACHA,CArBduC,EAAQunB,CAAA,CAqBkBxnB,CArBlB,CAA4BtC,CAA5B,CAqBMA,CApBdyC,EAAWqnB,CAAA,CAAgB9pB,CAAhB,CAoBesC,CApBf,CAoBGtC,CAnBlByC,EAAWwnB,CAAA,CAAkBxnB,CAAlB,CAA6B,EAA7B,CAmBOzC,CAlBlBuC,EAAQ0nB,CAAA,CAAkB1nB,CAAlB,CAAyB,CAAzB,CAEa,EAArB,GAAIA,CAAAxxB,OAAJ,CACE4kB,CAAA0M,YAAA,CAAqBpqB,CAArB,CAA8BwqB,CAA9B,CADF,CAE+B,CAAxB,GAAIA,CAAA1xB,OAAJ,CACL4kB,CAAAkB,SAAA,CAAkB5e,CAAlB,CAA2BsqB,CAA3B,CADK,CAGL5M,CAAA+M,SAAA,CAAkBzqB,CAAlB,CAA2BsqB,CAA3B,CAAkCE,CAAlC,CASmC,CAJmB,CASxDqW,CAAA,CAAS/iC,EAAA,CAAY8iC,CAAZ,CAVyB,CA9DpC,IAAIC,CAEJj+B,EAAApF,OAAA,CAAaf,CAAA,CAAKsF,CAAL,CAAb,CAAyBowC,CAAzB,CAA6C,CAAA,CAA7C,CAEA11C,EAAA0nB,SAAA,CAAc,OAAd,CAAuB,QAAQ,CAAClqB,CAAD,CAAQ,CACrCk4C,CAAA,CAAmBvvC,CAAA0/B,MAAA,CAAY7lC,CAAA,CAAKsF,CAAL,CAAZ,CAAnB,CADqC,CAAvC,CAKa,UAAb,GAAIA,CAAJ,EACEa,CAAApF,OAAA,CAAa,QAAb,CAAuB,QAAQ,CAAC40C,CAAD,CAASC,CAAT,CAAoB,CAEjD,IAAIC,EAAMF,CAANE,CAAe,CACnB,IAAIA,CAAJ,IAAaD,CAAb,CAAyB,CAAzB,EAA6B,CAC3B,IAAIN,EAAUD,CAAA,CAAalvC,CAAA0/B,MAAA,CAAY7lC,CAAA,CAAKsF,CAAL,CAAZ,CAAb,CACduwC,EAAA,GAAQtjC,CAAR,EAQAqb,CACJ,CADiB2nB,CAAA,CAPAD,CAOA,CAA2B,CAA3B,CACjB,CAAAt1C,CAAAwtB,UAAA,CAAeI,CAAf,CATI,GAaAA,CACJ,CADiB2nB,CAAA,CAXGD,CAWH,CAA4B,EAA5B,CACjB,CAAAt1C,CAAA0tB,aAAA,CAAkBE,CAAlB,CAdI,CAF2B,CAHoB,CAAnD,CAXiC,CAFhC,CAD8B,CAAhC,CAF+B,CAx/iBxC,IAAIvqB,EAAYA,QAAQ,CAACurC,CAAD,CAAQ,CAAC,MAAOryC,EAAA,CAASqyC,CAAT,CAAA,CAAmBA,CAAAznC,YAAA,EAAnB,CAA0CynC,CAAlD,CAAhC,CACI9xC,GAAiBg5C,MAAAt+B,UAAA1a,eADrB,CAaIyM;AAAYA,QAAQ,CAACqlC,CAAD,CAAQ,CAAC,MAAOryC,EAAA,CAASqyC,CAAT,CAAA,CAAmBA,CAAA3gC,YAAA,EAAnB,CAA0C2gC,CAAlD,CAbhC,CAwCIn6B,CAxCJ,CAyCIjR,CAzCJ,CA0CI2L,EA1CJ,CA2CI7M,GAAoB,EAAAA,MA3CxB,CA4CIpF,GAAoB,EAAAA,KA5CxB,CA6CIqC,GAAoBu2C,MAAAt+B,UAAAjY,SA7CxB,CA8CIyB,GAAoB/E,CAAA,CAAO,IAAP,CA9CxB,CAiDIyK,GAAoB5K,CAAA4K,QAApBA,GAAuC5K,CAAA4K,QAAvCA,CAAwD,EAAxDA,CAjDJ,CAkDI8C,EAlDJ,CAmDI0a,EAnDJ,CAoDIvmB,GAAoB,CAAC,GAAD,CAAM,GAAN,CAAW,GAAX,CAMxB8W,EAAA,CAAOjW,CAAA,CAAI,CAAC,YAAAiH,KAAA,CAAkBpC,CAAA,CAAUqmC,SAAAD,UAAV,CAAlB,CAAD,EAAsD,EAAtD,EAA0D,CAA1D,CAAJ,CACH3D,MAAA,CAAMrxB,CAAN,CAAJ,GACEA,CADF,CACSjW,CAAA,CAAI,CAAC,uBAAAiH,KAAA,CAA6BpC,CAAA,CAAUqmC,SAAAD,UAAV,CAA7B,CAAD,EAAiE,EAAjE,EAAqE,CAArE,CAAJ,CADT,CAiNA3qC,EAAAkW,QAAA,CAAe,EAoBfjW,GAAAiW,QAAA,CAAmB,EA8KnB,KAAIzF,GAAQ,QAAQ,EAAG,CAIrB,MAAKxR,OAAAyZ,UAAAjI,KAAL,CAKO,QAAQ,CAAC/R,CAAD,CAAQ,CACrB,MAAOjB,EAAA,CAASiB,CAAT,CAAA,CAAkBA,CAAA+R,KAAA,EAAlB,CAAiC/R,CADnB,CALvB,CACS,QAAQ,CAACA,CAAD,CAAQ,CACrB,MAAOjB,EAAA,CAASiB,CAAT,CAAA,CAAkBA,CAAAyG,QAAA,CAAc,QAAd,CAAwB,EAAxB,CAAAA,QAAA,CAAoC,QAApC,CAA8C,EAA9C,CAAlB,CAAsEzG,CADxD,CALJ,CAAX,EA8CV0mB,GAAA,CADS,CAAX,CAAIzP,CAAJ,CACcyP,QAAQ,CAAC3gB,CAAD,CAAU,CAC5BA,CAAA;AAAUA,CAAAzD,SAAA,CAAmByD,CAAnB,CAA6BA,CAAA,CAAQ,CAAR,CACvC,OAAQA,EAAA2jB,UACD,EAD2C,MAC3C,EADsB3jB,CAAA2jB,UACtB,CAAH3d,EAAA,CAAUhG,CAAA2jB,UAAV,CAA8B,GAA9B,CAAoC3jB,CAAAzD,SAApC,CAAG,CAAqDyD,CAAAzD,SAHhC,CADhC,CAOcokB,QAAQ,CAAC3gB,CAAD,CAAU,CAC5B,MAAOA,EAAAzD,SAAA,CAAmByD,CAAAzD,SAAnB,CAAsCyD,CAAA,CAAQ,CAAR,CAAAzD,SADjB,CAqtBhC,KAAIkH,GAAoB,QAAxB,CAsgBIsC,GAAU,MACN,QADM,OAEL,CAFK,OAGL,CAHK,KAIP,EAJO,UAKF,yBALE,CAtgBd,CAyuBIyI,GAAUzC,CAAAwH,MAAV/E,CAAyB,EAzuB7B,CA0uBIF,GAASvC,CAAA8d,QAATvb,CAA0B,IAA1BA,CAAiC1Q,CAAA,IAAID,IAAJC,SAAA,EA1uBrC,CA2uBI8Q,GAAO,CA3uBX,CA4uBI6iB,GAAsBh5B,CAAAC,SAAAg6C,iBACA,CAAlB,QAAQ,CAACxyC,CAAD,CAAU8N,CAAV,CAAgBjP,CAAhB,CAAoB,CAACmB,CAAAwyC,iBAAA,CAAyB1kC,CAAzB,CAA+BjP,CAA/B,CAAmC,CAAA,CAAnC,CAAD,CAAV,CAClB,QAAQ,CAACmB,CAAD,CAAU8N,CAAV,CAAgBjP,CAAhB,CAAoB,CAACmB,CAAAyyC,YAAA,CAAoB,IAApB,CAA2B3kC,CAA3B,CAAiCjP,CAAjC,CAAD,CA9uBpC,CA+uBIuP,GAAyB7V,CAAAC,SAAAk6C,oBACA,CAArB,QAAQ,CAAC1yC,CAAD,CAAU8N,CAAV,CAAgBjP,CAAhB,CAAoB,CAACmB,CAAA0yC,oBAAA,CAA4B5kC,CAA5B,CAAkCjP,CAAlC,CAAsC,CAAA,CAAtC,CAAD,CAAP;AACrB,QAAQ,CAACmB,CAAD,CAAU8N,CAAV,CAAgBjP,CAAhB,CAAoB,CAACmB,CAAA2yC,YAAA,CAAoB,IAApB,CAA2B7kC,CAA3B,CAAiCjP,CAAjC,CAAD,CAKvBkN,EAAA6mC,MAAb,CAA4BC,QAAQ,CAACv2C,CAAD,CAAO,CAEzC,MAAO,KAAAiX,MAAA,CAAWjX,CAAA,CAAK,IAAAutB,QAAL,CAAX,CAAP,EAAyC,EAFA,CAQ3C,KAAItf,GAAuB,iBAA3B,CACII,GAAkB,aADtB,CAEIsB,GAAevT,CAAA,CAAO,QAAP,CAFnB,CA4DIyT,GAAoB,4BA5DxB,CA6DIG,GAAc,WA7DlB,CA8DII,GAAkB,WA9DtB,CA+DIK,GAAmB,yEA/DvB,CAiEIH,GAAU,QACF,CAAC,CAAD,CAAI,8BAAJ,CAAoC,WAApC,CADE,OAGH,CAAC,CAAD,CAAI,SAAJ,CAAe,UAAf,CAHG,KAIL,CAAC,CAAD,CAAI,mBAAJ,CAAyB,qBAAzB,CAJK,IAKN,CAAC,CAAD,CAAI,gBAAJ,CAAsB,kBAAtB,CALM,IAMN,CAAC,CAAD,CAAI,oBAAJ;AAA0B,uBAA1B,CANM,UAOA,CAAC,CAAD,CAAI,EAAJ,CAAQ,EAAR,CAPA,CAUdA,GAAAkmC,SAAA,CAAmBlmC,EAAAmmC,OACnBnmC,GAAAomC,MAAA,CAAgBpmC,EAAAqmC,MAAhB,CAAgCrmC,EAAAsmC,SAAhC,CAAmDtmC,EAAAumC,QAAnD,CAAqEvmC,EAAAwmC,MACrExmC,GAAAymC,GAAA,CAAazmC,EAAA0mC,GAgQb,KAAI70B,GAAkB1S,CAAAkI,UAAlBwK,CAAqC,OAChC80B,QAAQ,CAAC10C,CAAD,CAAK,CAGlB20C,QAASA,EAAO,EAAG,CACbC,CAAJ,GACAA,CACA,CADQ,CAAA,CACR,CAAA50C,CAAA,EAFA,CADiB,CAFnB,IAAI40C,EAAQ,CAAA,CASgB,WAA5B,GAAIj7C,CAAAk5B,WAAJ,CACE9a,UAAA,CAAW48B,CAAX,CADF,EAGE,IAAA56B,GAAA,CAAQ,kBAAR,CAA4B46B,CAA5B,CAGA,CAAAznC,CAAA,CAAOxT,CAAP,CAAAqgB,GAAA,CAAkB,MAAlB,CAA0B46B,CAA1B,CANF,CAVkB,CADmB,UAqB7Bx3C,QAAQ,EAAG,CACnB,IAAI/B,EAAQ,EACZf,EAAA,CAAQ,IAAR,CAAc,QAAQ,CAACkH,CAAD,CAAG,CAAEnG,CAAAN,KAAA,CAAW,EAAX,CAAgByG,CAAhB,CAAF,CAAzB,CACA,OAAO,GAAP,CAAanG,CAAAM,KAAA,CAAW,IAAX,CAAb,CAAgC,GAHb,CArBkB,IA2BnCmkB,QAAQ,CAACvkB,CAAD,CAAQ,CAChB,MAAiB,EAAV,EAACA,CAAD,CAAe8F,CAAA,CAAO,IAAA,CAAK9F,CAAL,CAAP,CAAf,CAAqC8F,CAAA,CAAO,IAAA,CAAK,IAAAnH,OAAL,CAAmBqB,CAAnB,CAAP,CAD5B,CA3BmB,QA+B/B,CA/B+B,MAgCjCR,EAhCiC,MAiCjC,EAAAC,KAjCiC,QAkC/B,EAAAqD,OAlC+B,CAAzC,CA0CIgT,GAAe,EACnB/W;CAAA,CAAQ,2DAAA,MAAA,CAAA,GAAA,CAAR,CAAgF,QAAQ,CAACe,CAAD,CAAQ,CAC9FgW,EAAA,CAAanQ,CAAA,CAAU7F,CAAV,CAAb,CAAA,CAAiCA,CAD6D,CAAhG,CAGA,KAAIiW,GAAmB,EACvBhX,EAAA,CAAQ,kDAAA,MAAA,CAAA,GAAA,CAAR,CAAuE,QAAQ,CAACe,CAAD,CAAQ,CACrFiW,EAAA,CAAiBlK,EAAA,CAAU/L,CAAV,CAAjB,CAAA,CAAqC,CAAA,CADgD,CAAvF,CAYAf,EAAA,CAAQ,MACAyV,EADA,eAESe,EAFT,OAIC9M,QAAQ,CAAC5C,CAAD,CAAU,CAEvB,MAAOC,EAAA,CAAOD,CAAP,CAAAgD,KAAA,CAAqB,QAArB,CAAP,EAAyC0M,EAAA,CAAoB1P,CAAA4P,WAApB,EAA0C5P,CAA1C,CAAmD,CAAC,eAAD,CAAkB,QAAlB,CAAnD,CAFlB,CAJnB,cASQqjB,QAAQ,CAACrjB,CAAD,CAAU,CAE9B,MAAOC,EAAA,CAAOD,CAAP,CAAAgD,KAAA,CAAqB,eAArB,CAAP,EAAgD/C,CAAA,CAAOD,CAAP,CAAAgD,KAAA,CAAqB,yBAArB,CAFlB,CAT1B,YAcMyM,EAdN,UAgBIlN,QAAQ,CAACvC,CAAD,CAAU,CAC1B,MAAO0P,GAAA,CAAoB1P,CAApB,CAA6B,WAA7B,CADmB,CAhBtB,YAoBM4qB,QAAQ,CAAC5qB,CAAD;AAAS+B,CAAT,CAAe,CACjC/B,CAAA0zC,gBAAA,CAAwB3xC,CAAxB,CADiC,CApB7B,UAwBIgN,EAxBJ,KA0BD4kC,QAAQ,CAAC3zC,CAAD,CAAU+B,CAAV,CAAgB9H,CAAhB,CAAuB,CAClC8H,CAAA,CAAOuI,EAAA,CAAUvI,CAAV,CAEP,IAAInG,CAAA,CAAU3B,CAAV,CAAJ,CACE+F,CAAAymC,MAAA,CAAc1kC,CAAd,CAAA,CAAsB9H,CADxB,KAEO,CACL,IAAImF,CAEQ,EAAZ,EAAI8R,CAAJ,GAEE9R,CACA,CADMY,CAAA4zC,aACN,EAD8B5zC,CAAA4zC,aAAA,CAAqB7xC,CAArB,CAC9B,CAAY,EAAZ,GAAI3C,CAAJ,GAAgBA,CAAhB,CAAsB,MAAtB,CAHF,CAMAA,EAAA,CAAMA,CAAN,EAAaY,CAAAymC,MAAA,CAAc1kC,CAAd,CAED,EAAZ,EAAImP,CAAJ,GAEE9R,CAFF,CAEiB,EAAT,GAACA,CAAD,CAAe3G,CAAf,CAA2B2G,CAFnC,CAKA,OAAQA,EAhBH,CAL2B,CA1B9B,MAmDA3C,QAAQ,CAACuD,CAAD,CAAU+B,CAAV,CAAgB9H,CAAhB,CAAsB,CAClC,IAAI45C,EAAiB/zC,CAAA,CAAUiC,CAAV,CACrB,IAAIkO,EAAA,CAAa4jC,CAAb,CAAJ,CACE,GAAIj4C,CAAA,CAAU3B,CAAV,CAAJ,CACQA,CAAN,EACE+F,CAAA,CAAQ+B,CAAR,CACA,CADgB,CAAA,CAChB,CAAA/B,CAAAoP,aAAA,CAAqBrN,CAArB,CAA2B8xC,CAA3B,CAFF,GAIE7zC,CAAA,CAAQ+B,CAAR,CACA,CADgB,CAAA,CAChB,CAAA/B,CAAA0zC,gBAAA,CAAwBG,CAAxB,CALF,CADF,KASE,OAAQ7zC,EAAA,CAAQ+B,CAAR,CAED,EADGif,CAAAhhB,CAAAoC,WAAA0xC,aAAA,CAAgC/xC,CAAhC,CAAAif,EAAwCzlB,CAAxCylB,WACH,CAAE6yB,CAAF,CACEp7C,CAbb,KAeO,IAAImD,CAAA,CAAU3B,CAAV,CAAJ,CACL+F,CAAAoP,aAAA,CAAqBrN,CAArB,CAA2B9H,CAA3B,CADK,KAEA,IAAI+F,CAAAiP,aAAJ,CAKL,MAFI8kC,EAEG,CAFG/zC,CAAAiP,aAAA,CAAqBlN,CAArB,CAA2B,CAA3B,CAEH,CAAQ,IAAR,GAAAgyC,CAAA,CAAet7C,CAAf,CAA2Bs7C,CAxBF,CAnD9B,MA+EAv3C,QAAQ,CAACwD,CAAD;AAAU+B,CAAV,CAAgB9H,CAAhB,CAAuB,CACnC,GAAI2B,CAAA,CAAU3B,CAAV,CAAJ,CACE+F,CAAA,CAAQ+B,CAAR,CAAA,CAAgB9H,CADlB,KAGE,OAAO+F,EAAA,CAAQ+B,CAAR,CAJ0B,CA/E/B,MAuFC,QAAQ,EAAG,CAYhBiyC,QAASA,EAAO,CAACh0C,CAAD,CAAU/F,CAAV,CAAiB,CAC/B,IAAIg6C,EAAWC,CAAA,CAAwBl0C,CAAAjH,SAAxB,CACf,IAAI4C,CAAA,CAAY1B,CAAZ,CAAJ,CACE,MAAOg6C,EAAA,CAAWj0C,CAAA,CAAQi0C,CAAR,CAAX,CAA+B,EAExCj0C,EAAA,CAAQi0C,CAAR,CAAA,CAAoBh6C,CALW,CAXjC,IAAIi6C,EAA0B,EACnB,EAAX,CAAIhjC,CAAJ,EACEgjC,CAAA,CAAwB,CAAxB,CACA,CAD6B,WAC7B,CAAAA,CAAA,CAAwB,CAAxB,CAAA,CAA6B,WAF/B,EAIEA,CAAA,CAAwB,CAAxB,CAJF,CAKEA,CAAA,CAAwB,CAAxB,CALF,CAK+B,aAE/BF,EAAAG,IAAA,CAAc,EACd,OAAOH,EAVS,CAAX,EAvFD,KA4GD50C,QAAQ,CAACY,CAAD,CAAU/F,CAAV,CAAiB,CAC5B,GAAI0B,CAAA,CAAY1B,CAAZ,CAAJ,CAAwB,CACtB,GAA2B,QAA3B,GAAI0mB,EAAA,CAAU3gB,CAAV,CAAJ,EAAuCA,CAAAo0C,SAAvC,CAAyD,CACvD,IAAI12C,EAAS,EACbxE,EAAA,CAAQ8G,CAAAua,QAAR,CAAyB,QAAS,CAACw4B,CAAD,CAAS,CACrCA,CAAAsB,SAAJ,EACE32C,CAAA/D,KAAA,CAAYo5C,CAAA94C,MAAZ,EAA4B84C,CAAAtqB,KAA5B,CAFuC,CAA3C,CAKA,OAAyB,EAAlB,GAAA/qB,CAAA5E,OAAA,CAAsB,IAAtB,CAA6B4E,CAPmB,CASzD,MAAOsC,EAAA/F,MAVe,CAYxB+F,CAAA/F,MAAA,CAAgBA,CAbY,CA5GxB,MA4HAsG,QAAQ,CAACP,CAAD,CAAU/F,CAAV,CAAiB,CAC7B,GAAI0B,CAAA,CAAY1B,CAAZ,CAAJ,CACE,MAAO+F,EAAA8M,UAET,KAJ6B,IAIpBhT,EAAI,CAJgB,CAIbuT,EAAarN,CAAAqN,WAA7B,CAAiDvT,CAAjD,CAAqDuT,CAAAvU,OAArD,CAAwEgB,CAAA,EAAxE,CACE6T,EAAA,CAAaN,CAAA,CAAWvT,CAAX,CAAb,CAEFkG,EAAA8M,UAAA;AAAoB7S,CAPS,CA5HzB,OAsIC6V,EAtID,CAAR,CAuIG,QAAQ,CAACjR,CAAD,CAAKkD,CAAL,CAAU,CAInBgK,CAAAkI,UAAA,CAAiBlS,CAAjB,CAAA,CAAyB,QAAQ,CAACi4B,CAAD,CAAOC,CAAP,CAAa,CAAA,IACxCngC,CADwC,CACrCT,CAKP,IAAIwF,CAAJ,GAAWiR,EAAX,GACoB,CAAd,EAACjR,CAAA/F,OAAD,EAAoB+F,CAApB,GAA2BkQ,EAA3B,EAA6ClQ,CAA7C,GAAoD4Q,EAApD,CAAyEuqB,CAAzE,CAAgFC,CADtF,IACgGxhC,CADhG,CAC4G,CAC1G,GAAIoD,CAAA,CAASm+B,CAAT,CAAJ,CAAoB,CAGlB,IAAKlgC,CAAL,CAAS,CAAT,CAAYA,CAAZ,CAAgB,IAAAhB,OAAhB,CAA6BgB,CAAA,EAA7B,CACE,GAAI+E,CAAJ,GAAW8P,EAAX,CAEE9P,CAAA,CAAG,IAAA,CAAK/E,CAAL,CAAH,CAAYkgC,CAAZ,CAFF,KAIE,KAAK3gC,CAAL,GAAY2gC,EAAZ,CACEn7B,CAAA,CAAG,IAAA,CAAK/E,CAAL,CAAH,CAAYT,CAAZ,CAAiB2gC,CAAA,CAAK3gC,CAAL,CAAjB,CAKN,OAAO,KAdW,CAiBdY,CAAAA,CAAQ4E,CAAAs1C,IAER/mC,EAAAA,CAAMnT,CAAD,GAAWxB,CAAX,CAAwBguB,IAAAsjB,IAAA,CAAS,IAAAjxC,OAAT,CAAsB,CAAtB,CAAxB,CAAmD,IAAAA,OAC5D,KAAK,IAAIqU,EAAI,CAAb,CAAgBA,CAAhB,CAAoBC,CAApB,CAAwBD,CAAA,EAAxB,CAA6B,CAC3B,IAAI8Q,EAAYpf,CAAA,CAAG,IAAA,CAAKsO,CAAL,CAAH,CAAY6sB,CAAZ,CAAkBC,CAAlB,CAChBhgC,EAAA,CAAQA,CAAA,CAAQA,CAAR,CAAgBgkB,CAAhB,CAA4BA,CAFT,CAI7B,MAAOhkB,EAzBiG,CA6B1G,IAAKH,CAAL,CAAS,CAAT,CAAYA,CAAZ,CAAgB,IAAAhB,OAAhB,CAA6BgB,CAAA,EAA7B,CACE+E,CAAA,CAAG,IAAA,CAAK/E,CAAL,CAAH,CAAYkgC,CAAZ,CAAkBC,CAAlB,CAGF,OAAO,KAxCmC,CAJ3B,CAvIrB,CAqPA/gC,EAAA,CAAQ,YACM0U,EADN,QAGED,EAHF,IAKF2mC,QAASA,EAAI,CAACt0C,CAAD,CAAU8N,CAAV,CAAgBjP,CAAhB,CAAoBkP,CAApB,CAAgC,CAC/C,GAAInS,CAAA,CAAUmS,CAAV,CAAJ,CAA4B,KAAM9B,GAAA,CAAa,QAAb,CAAN,CADmB,IAG3C+B,EAASC,EAAA,CAAmBjO,CAAnB,CAA4B,QAA5B,CAHkC,CAI3CkO,EAASD,EAAA,CAAmBjO,CAAnB,CAA4B,QAA5B,CAERgO;CAAL,EAAaC,EAAA,CAAmBjO,CAAnB,CAA4B,QAA5B,CAAsCgO,CAAtC,CAA+C,EAA/C,CACRE,EAAL,EAAaD,EAAA,CAAmBjO,CAAnB,CAA4B,QAA5B,CAAsCkO,CAAtC,CAA+CiC,EAAA,CAAmBnQ,CAAnB,CAA4BgO,CAA5B,CAA/C,CAEb9U,EAAA,CAAQ4U,CAAA9M,MAAA,CAAW,GAAX,CAAR,CAAyB,QAAQ,CAAC8M,CAAD,CAAM,CACrC,IAAIymC,EAAWvmC,CAAA,CAAOF,CAAP,CAEf,IAAI,CAACymC,CAAL,CAAe,CACb,GAAY,YAAZ,EAAIzmC,CAAJ,EAAoC,YAApC,EAA4BA,CAA5B,CAAkD,CAChD,IAAI0mC,EAAWh8C,CAAA64B,KAAAmjB,SAAA,EAA0Bh8C,CAAA64B,KAAAojB,wBAA1B,CACf,QAAQ,CAAElwB,CAAF,CAAKC,CAAL,CAAS,CAAA,IAEXkwB,EAAuB,CAAf,GAAAnwB,CAAAxrB,SAAA,CAAmBwrB,CAAAowB,gBAAnB,CAAuCpwB,CAFpC,CAGfqwB,EAAMpwB,CAANowB,EAAWpwB,CAAA5U,WACX,OAAO2U,EAAP,GAAaqwB,CAAb,EAAoB,CAAC,EAAGA,CAAH,EAA2B,CAA3B,GAAUA,CAAA77C,SAAV,GACnB27C,CAAAF,SAAA,CACAE,CAAAF,SAAA,CAAgBI,CAAhB,CADA,CAEArwB,CAAAkwB,wBAFA,EAE6BlwB,CAAAkwB,wBAAA,CAA2BG,CAA3B,CAF7B,CAEgE,EAH7C,EAJN,CADF,CAWb,QAAQ,CAAErwB,CAAF,CAAKC,CAAL,CAAS,CACf,GAAKA,CAAL,CACE,IAAA,CAASA,CAAT,CAAaA,CAAA5U,WAAb,CAAA,CACE,GAAK4U,CAAL,GAAWD,CAAX,CACE,MAAO,CAAA,CAIb,OAAO,CAAA,CARQ,CAWnBvW,EAAA,CAAOF,CAAP,CAAA,CAAe,EAOfwmC,EAAA,CAAKt0C,CAAL,CAFe60C,YAAe,UAAfA,YAAwC,WAAxCA,CAED,CAAS/mC,CAAT,CAAd;AAA8B,QAAQ,CAACsC,CAAD,CAAQ,CAC5C,IAAmB0kC,EAAU1kC,CAAA2kC,cAGvBD,EAAN,GAAkBA,CAAlB,GAHankC,IAGb,EAAyC6jC,CAAA,CAH5B7jC,IAG4B,CAAiBmkC,CAAjB,CAAzC,GACE5mC,CAAA,CAAOkC,CAAP,CAActC,CAAd,CAL0C,CAA9C,CA9BgD,CAAlD,IAwCEyjB,GAAA,CAAmBvxB,CAAnB,CAA4B8N,CAA5B,CAAkCI,CAAlC,CACA,CAAAF,CAAA,CAAOF,CAAP,CAAA,CAAe,EAEjBymC,EAAA,CAAWvmC,CAAA,CAAOF,CAAP,CA5CE,CA8CfymC,CAAA56C,KAAA,CAAckF,CAAd,CAjDqC,CAAvC,CAT+C,CAL3C,KAmEDgP,EAnEC,KAqEDmnC,QAAQ,CAACh1C,CAAD,CAAU8N,CAAV,CAAgBjP,CAAhB,CAAoB,CAC/BmB,CAAA,CAAUC,CAAA,CAAOD,CAAP,CAKVA,EAAA4Y,GAAA,CAAW9K,CAAX,CAAiBwmC,QAASA,EAAI,EAAG,CAC/Bt0C,CAAAi1C,IAAA,CAAYnnC,CAAZ,CAAkBjP,CAAlB,CACAmB,EAAAi1C,IAAA,CAAYnnC,CAAZ,CAAkBwmC,CAAlB,CAF+B,CAAjC,CAIAt0C,EAAA4Y,GAAA,CAAW9K,CAAX,CAAiBjP,CAAjB,CAV+B,CArE3B,aAkFOknB,QAAQ,CAAC/lB,CAAD,CAAUk1C,CAAV,CAAuB,CAAA,IACtC/6C,CADsC,CAC/BkB,EAAS2E,CAAA4P,WACpBjC,GAAA,CAAa3N,CAAb,CACA9G,EAAA,CAAQ,IAAI6S,CAAJ,CAAWmpC,CAAX,CAAR,CAAiC,QAAQ,CAAC54C,CAAD,CAAM,CACzCnC,CAAJ,CACEkB,CAAA85C,aAAA,CAAoB74C,CAApB,CAA0BnC,CAAA0K,YAA1B,CADF,CAGExJ,CAAAuuB,aAAA,CAAoBttB,CAApB,CAA0B0D,CAA1B,CAEF7F,EAAA,CAAQmC,CANqC,CAA/C,CAH0C,CAlFtC,UA+FIkP,QAAQ,CAACxL,CAAD,CAAU,CAC1B,IAAIwL,EAAW,EACftS,EAAA,CAAQ8G,CAAAqN,WAAR,CAA4B,QAAQ,CAACrN,CAAD,CAAS,CAClB,CAAzB,GAAIA,CAAAjH,SAAJ,EACEyS,CAAA7R,KAAA,CAAcqG,CAAd,CAFyC,CAA7C,CAIA,OAAOwL,EANmB,CA/FtB,UAwGIya,QAAQ,CAACjmB,CAAD,CAAU,CAC1B,MAAOA,EAAAo1C,gBAAP,EAAkCp1C,CAAAqN,WAAlC,EAAwD,EAD9B,CAxGtB,QA4GE/M,QAAQ,CAACN,CAAD;AAAU1D,CAAV,CAAgB,CAC9BpD,CAAA,CAAQ,IAAI6S,CAAJ,CAAWzP,CAAX,CAAR,CAA0B,QAAQ,CAAC8jC,CAAD,CAAO,CACd,CAAzB,GAAIpgC,CAAAjH,SAAJ,EAAmD,EAAnD,GAA8BiH,CAAAjH,SAA9B,EACEiH,CAAAwM,YAAA,CAAoB4zB,CAApB,CAFqC,CAAzC,CAD8B,CA5G1B,SAoHGiV,QAAQ,CAACr1C,CAAD,CAAU1D,CAAV,CAAgB,CAC/B,GAAyB,CAAzB,GAAI0D,CAAAjH,SAAJ,CAA4B,CAC1B,IAAIoB,EAAQ6F,CAAAiN,WACZ/T,EAAA,CAAQ,IAAI6S,CAAJ,CAAWzP,CAAX,CAAR,CAA0B,QAAQ,CAAC8jC,CAAD,CAAO,CACvCpgC,CAAAm1C,aAAA,CAAqB/U,CAArB,CAA4BjmC,CAA5B,CADuC,CAAzC,CAF0B,CADG,CApH3B,MA6HAwS,QAAQ,CAAC3M,CAAD,CAAUs1C,CAAV,CAAoB,CAChCA,CAAA,CAAWr1C,CAAA,CAAOq1C,CAAP,CAAA,CAAiB,CAAjB,CACX,KAAIj6C,EAAS2E,CAAA4P,WACTvU,EAAJ,EACEA,CAAAuuB,aAAA,CAAoB0rB,CAApB,CAA8Bt1C,CAA9B,CAEFs1C,EAAA9oC,YAAA,CAAqBxM,CAArB,CANgC,CA7H5B,QAsIEyb,QAAQ,CAACzb,CAAD,CAAU,CACxB2N,EAAA,CAAa3N,CAAb,CACA,KAAI3E,EAAS2E,CAAA4P,WACTvU,EAAJ,EAAYA,CAAA2R,YAAA,CAAmBhN,CAAnB,CAHY,CAtIpB,OA4ICu1C,QAAQ,CAACv1C,CAAD,CAAUw1C,CAAV,CAAsB,CAAA,IAC/Br7C,EAAQ6F,CADuB,CACd3E,EAAS2E,CAAA4P,WAC9B1W,EAAA,CAAQ,IAAI6S,CAAJ,CAAWypC,CAAX,CAAR,CAAgC,QAAQ,CAACl5C,CAAD,CAAM,CAC5CjB,CAAA85C,aAAA,CAAoB74C,CAApB,CAA0BnC,CAAA0K,YAA1B,CACA1K,EAAA,CAAQmC,CAFoC,CAA9C,CAFmC,CA5I/B,UAoJIgT,EApJJ,aAqJOJ,EArJP,aAuJOumC,QAAQ,CAACz1C,CAAD,CAAUgP,CAAV,CAAoB0mC,CAApB,CAA+B,CAC9C1mC,CAAJ;AACE9V,CAAA,CAAQ8V,CAAAhO,MAAA,CAAe,GAAf,CAAR,CAA6B,QAAQ,CAACmB,CAAD,CAAW,CAC9C,IAAIwzC,EAAiBD,CACjB/5C,EAAA,CAAYg6C,CAAZ,CAAJ,GACEA,CADF,CACmB,CAAC5mC,EAAA,CAAe/O,CAAf,CAAwBmC,CAAxB,CADpB,CAGC,EAAAwzC,CAAA,CAAiBrmC,EAAjB,CAAkCJ,EAAlC,EAAqDlP,CAArD,CAA8DmC,CAA9D,CAL6C,CAAhD,CAFgD,CAvJ9C,QAmKE9G,QAAQ,CAAC2E,CAAD,CAAU,CAExB,MAAO,CADH3E,CACG,CADM2E,CAAA4P,WACN,GAA8B,EAA9B,GAAUvU,CAAAtC,SAAV,CAAmCsC,CAAnC,CAA4C,IAF3B,CAnKpB,MAwKAmnC,QAAQ,CAACxiC,CAAD,CAAU,CACtB,GAAIA,CAAA41C,mBAAJ,CACE,MAAO51C,EAAA41C,mBAKT,KADIngC,CACJ,CADUzV,CAAA6E,YACV,CAAc,IAAd,EAAO4Q,CAAP,EAAuC,CAAvC,GAAsBA,CAAA1c,SAAtB,CAAA,CACE0c,CAAA,CAAMA,CAAA5Q,YAER,OAAO4Q,EAVe,CAxKlB,MAqLA/Y,QAAQ,CAACsD,CAAD,CAAUgP,CAAV,CAAoB,CAChC,MAAIhP,EAAA61C,qBAAJ,CACS71C,CAAA61C,qBAAA,CAA6B7mC,CAA7B,CADT,CAGS,EAJuB,CArL5B,OA6LCvB,EA7LD,gBA+LU/B,QAAQ,CAAC1L,CAAD,CAAU81C,CAAV,CAAqBC,CAArB,CAAgC,CAClDxB,CAAAA,CAAW,CAACtmC,EAAA,CAAmBjO,CAAnB,CAA4B,QAA5B,CAAD,EAA0C,EAA1C,EAA8C81C,CAA9C,CAEfC,EAAA,CAAYA,CAAZ,EAAyB,EAEzB,KAAI3lC,EAAQ,CAAC,gBACK7U,CADL,iBAEMA,CAFN,CAAD,CAKZrC,EAAA,CAAQq7C,CAAR,CAAkB,QAAQ,CAAC11C,CAAD,CAAK,CAC7BA,CAAAI,MAAA,CAASe,CAAT;AAAkBoQ,CAAAlR,OAAA,CAAa62C,CAAb,CAAlB,CAD6B,CAA/B,CAVsD,CA/LlD,CAAR,CA6MG,QAAQ,CAACl3C,CAAD,CAAKkD,CAAL,CAAU,CAInBgK,CAAAkI,UAAA,CAAiBlS,CAAjB,CAAA,CAAyB,QAAQ,CAACi4B,CAAD,CAAOC,CAAP,CAAa+b,CAAb,CAAmB,CAElD,IADA,IAAI/7C,CAAJ,CACQH,EAAE,CAAV,CAAaA,CAAb,CAAiB,IAAAhB,OAAjB,CAA8BgB,CAAA,EAA9B,CACM6B,CAAA,CAAY1B,CAAZ,CAAJ,EACEA,CACA,CADQ4E,CAAA,CAAG,IAAA,CAAK/E,CAAL,CAAH,CAAYkgC,CAAZ,CAAkBC,CAAlB,CAAwB+b,CAAxB,CACR,CAAIp6C,CAAA,CAAU3B,CAAV,CAAJ,GAEEA,CAFF,CAEUgG,CAAA,CAAOhG,CAAP,CAFV,CAFF,EAOEuT,EAAA,CAAevT,CAAf,CAAsB4E,CAAA,CAAG,IAAA,CAAK/E,CAAL,CAAH,CAAYkgC,CAAZ,CAAkBC,CAAlB,CAAwB+b,CAAxB,CAAtB,CAGJ,OAAOp6C,EAAA,CAAU3B,CAAV,CAAA,CAAmBA,CAAnB,CAA2B,IAbgB,CAiBpD8R,EAAAkI,UAAAtV,KAAA,CAAwBoN,CAAAkI,UAAA2E,GACxB7M,EAAAkI,UAAAgiC,OAAA,CAA0BlqC,CAAAkI,UAAAghC,IAtBP,CA7MrB,CA0QA3jC,GAAA2C,UAAA,CAAoB,KAMb1C,QAAQ,CAAClY,CAAD,CAAMY,CAAN,CAAa,CACxB,IAAA,CAAKmX,EAAA,CAAQ/X,CAAR,CAAL,CAAA,CAAqBY,CADG,CANR,KAcb+Y,QAAQ,CAAC3Z,CAAD,CAAM,CACjB,MAAO,KAAA,CAAK+X,EAAA,CAAQ/X,CAAR,CAAL,CADU,CAdD,QAsBVoiB,QAAQ,CAACpiB,CAAD,CAAM,CACpB,IAAIY,EAAQ,IAAA,CAAKZ,CAAL,CAAW+X,EAAA,CAAQ/X,CAAR,CAAX,CACZ,QAAO,IAAA,CAAKA,CAAL,CACP,OAAOY,EAHa,CAtBJ,CA0FpB,KAAI4X,GAAU,oCAAd,CACIC,GAAe,GADnB,CAEIC,GAAS,sBAFb,CAGIJ,GAAiB,kCAHrB;AAII5M,GAAkBrM,CAAA,CAAO,WAAP,CAJtB,CAo0BIw9C,GAAiBx9C,CAAA,CAAO,UAAP,CAp0BrB,CAm1BIoQ,GAAmB,CAAC,UAAD,CAAa,QAAQ,CAACrG,CAAD,CAAW,CAGrD,IAAA0zC,YAAA,CAAmB,EAkCnB,KAAA3qB,SAAA,CAAgBC,QAAQ,CAAC1pB,CAAD,CAAOkD,CAAP,CAAgB,CACtC,IAAI5L,EAAM0I,CAAN1I,CAAa,YACjB,IAAI0I,CAAJ,EAA8B,GAA9B,EAAYA,CAAA/D,OAAA,CAAY,CAAZ,CAAZ,CAAmC,KAAMk4C,GAAA,CAAe,SAAf,CACoBn0C,CADpB,CAAN,CAEnC,IAAAo0C,YAAA,CAAiBp0C,CAAAof,OAAA,CAAY,CAAZ,CAAjB,CAAA,CAAmC9nB,CACnCoJ,EAAAwC,QAAA,CAAiB5L,CAAjB,CAAsB4L,CAAtB,CALsC,CAsBxC,KAAAmxC,gBAAA,CAAuBC,QAAQ,CAAC3qB,CAAD,CAAa,CAClB,CAAxB,GAAG1wB,SAAAlC,OAAH,GACE,IAAAw9C,kBADF,CAC4B5qB,CAAD,WAAuB7tB,OAAvB,CAAiC6tB,CAAjC,CAA8C,IADzE,CAGA,OAAO,KAAA4qB,kBAJmC,CAO5C,KAAA7jC,KAAA,CAAY,CAAC,UAAD,CAAa,iBAAb,CAAgC,QAAQ,CAACuD,CAAD,CAAWugC,CAAX,CAA4B,CAuB9E,MAAO,OAiBGC,QAAQ,CAACx2C,CAAD,CAAU3E,CAAV,CAAkBk6C,CAAlB,CAAyBhmB,CAAzB,CAA+B,CACzCgmB,CAAJ,CACEA,CAAAA,MAAA,CAAYv1C,CAAZ,CADF,EAGO3E,CAGL,EAHgBA,CAAA,CAAO,CAAP,CAGhB,GAFEA,CAEF,CAFWk6C,CAAAl6C,OAAA,EAEX,EAAAA,CAAAiF,OAAA,CAAcN,CAAd,CANF,CAQMuvB,EA9CR;AAAMgnB,CAAA,CA8CEhnB,CA9CF,CAqCyC,CAjB1C,OAwCGknB,QAAQ,CAACz2C,CAAD,CAAUuvB,CAAV,CAAgB,CAC9BvvB,CAAAyb,OAAA,EACM8T,EA9DR,EAAMgnB,CAAA,CA8DEhnB,CA9DF,CA4D0B,CAxC3B,MA+DEmnB,QAAQ,CAAC12C,CAAD,CAAU3E,CAAV,CAAkBk6C,CAAlB,CAAyBhmB,CAAzB,CAA+B,CAG5C,IAAAinB,MAAA,CAAWx2C,CAAX,CAAoB3E,CAApB,CAA4Bk6C,CAA5B,CAAmChmB,CAAnC,CAH4C,CA/DzC,UAkFM3Q,QAAQ,CAAC5e,CAAD,CAAUmC,CAAV,CAAqBotB,CAArB,CAA2B,CAC5CptB,CAAA,CAAYnJ,CAAA,CAASmJ,CAAT,CAAA,CACEA,CADF,CAEElJ,CAAA,CAAQkJ,CAAR,CAAA,CAAqBA,CAAA5H,KAAA,CAAe,GAAf,CAArB,CAA2C,EACzDrB,EAAA,CAAQ8G,CAAR,CAAiB,QAAS,CAACA,CAAD,CAAU,CAClCsP,EAAA,CAAetP,CAAf,CAAwBmC,CAAxB,CADkC,CAApC,CAGMotB,EA7GR,EAAMgnB,CAAA,CA6GEhnB,CA7GF,CAsGwC,CAlFzC,aAyGSnF,QAAQ,CAACpqB,CAAD,CAAUmC,CAAV,CAAqBotB,CAArB,CAA2B,CAC/CptB,CAAA,CAAYnJ,CAAA,CAASmJ,CAAT,CAAA,CACEA,CADF,CAEElJ,CAAA,CAAQkJ,CAAR,CAAA,CAAqBA,CAAA5H,KAAA,CAAe,GAAf,CAArB,CAA2C,EACzDrB,EAAA,CAAQ8G,CAAR,CAAiB,QAAS,CAACA,CAAD,CAAU,CAClCkP,EAAA,CAAkBlP,CAAlB,CAA2BmC,CAA3B,CADkC,CAApC,CAGMotB,EApIR,EAAMgnB,CAAA,CAoIEhnB,CApIF,CA6H2C,CAzG5C,UAiIM9E,QAAQ,CAACzqB,CAAD,CAAU22C,CAAV,CAAel7B,CAAf,CAAuB8T,CAAvB,CAA6B,CAC9Cr2B,CAAA,CAAQ8G,CAAR,CAAiB,QAAS,CAACA,CAAD,CAAU,CAClCsP,EAAA,CAAetP,CAAf,CAAwB22C,CAAxB,CACAznC,GAAA,CAAkBlP,CAAlB,CAA2Byb,CAA3B,CAFkC,CAApC,CAIM8T,EA1JR,EAAMgnB,CAAA,CA0JEhnB,CA1JF,CAqJ0C,CAjI3C,SAyIKh0B,CAzIL,CAvBuE,CAApE,CAlEyC,CAAhC,CAn1BvB,CA8zEIsmB,GAAiBnpB,CAAA,CAAO,UAAP,CASrB2N,GAAAoL,QAAA,CAA2B,CAAC,UAAD,CAAa,uBAAb,CA45C3B,KAAIuZ,GAAgB,0BAApB,CAk9CIoI,GAAqB16B,CAAA,CAAO,cAAP,CAl9CzB,CA48DIk+C,GAAa,iCA58DjB;AA68DIxhB,GAAgB,MAAS,EAAT,OAAsB,GAAtB,KAAkC,EAAlC,CA78DpB,CA88DIsB,GAAkBh+B,CAAA,CAAO,WAAP,CAoRtB++B,GAAAxjB,UAAA,CACEkjB,EAAAljB,UADF,CAEEkiB,EAAAliB,UAFF,CAE+B,SAMpB,CAAA,CANoB,WAYlB,CAAA,CAZkB,QA0BrByjB,EAAA,CAAe,UAAf,CA1BqB,KA2CxBtgB,QAAQ,CAACA,CAAD,CAAM1W,CAAN,CAAe,CAC1B,GAAI/E,CAAA,CAAYyb,CAAZ,CAAJ,CACE,MAAO,KAAAyf,MAET,KAAIp2B,EAAQm2C,EAAA10C,KAAA,CAAgBkV,CAAhB,CACR3W,EAAA,CAAM,CAAN,CAAJ,EAAc,IAAA4D,KAAA,CAAUzD,kBAAA,CAAmBH,CAAA,CAAM,CAAN,CAAnB,CAAV,CACd,EAAIA,CAAA,CAAM,CAAN,CAAJ,EAAgBA,CAAA,CAAM,CAAN,CAAhB,GAA0B,IAAAk1B,OAAA,CAAYl1B,CAAA,CAAM,CAAN,CAAZ,EAAwB,EAAxB,CAC1B,KAAA+U,KAAA,CAAU/U,CAAA,CAAM,CAAN,CAAV,EAAsB,EAAtB,CAA0BC,CAA1B,CAEA,OAAO,KATmB,CA3CC,UAkEnBg3B,EAAA,CAAe,YAAf,CAlEmB,MA+EvBA,EAAA,CAAe,QAAf,CA/EuB,MA4FvBA,EAAA,CAAe,QAAf,CA5FuB,MA+GvBE,EAAA,CAAqB,QAArB,CAA+B,QAAQ,CAACvzB,CAAD,CAAO,CAClD,MAAyB,GAAlB,EAAAA,CAAArG,OAAA,CAAY,CAAZ,CAAA,CAAwBqG,CAAxB,CAA+B,GAA/B,CAAqCA,CADM,CAA9C,CA/GuB,QA8JrBsxB,QAAQ,CAACA,CAAD,CAASkhB,CAAT,CAAqB,CACnC,OAAQ77C,SAAAlC,OAAR,EACE,KAAK,CAAL,CACE,MAAO,KAAA48B,SACT;KAAK,CAAL,CACE,GAAI18B,CAAA,CAAS28B,CAAT,CAAJ,CACE,IAAAD,SAAA,CAAgB70B,EAAA,CAAc80B,CAAd,CADlB,KAEO,IAAI95B,CAAA,CAAS85B,CAAT,CAAJ,CACL,IAAAD,SAAA,CAAgBC,CADX,KAGL,MAAMe,GAAA,CAAgB,UAAhB,CAAN,CAGF,KACF,SACM/6B,CAAA,CAAYk7C,CAAZ,CAAJ,EAA8C,IAA9C,GAA+BA,CAA/B,CACE,OAAO,IAAAnhB,SAAA,CAAcC,CAAd,CADT,CAGE,IAAAD,SAAA,CAAcC,CAAd,CAHF,CAG0BkhB,CAjB9B,CAqBA,IAAAlgB,UAAA,EACA,OAAO,KAvB4B,CA9JR,MAsMvBiB,EAAA,CAAqB,QAArB,CAA+Bp8B,EAA/B,CAtMuB,SAgNpBkF,QAAQ,EAAG,CAClB,IAAAy4B,UAAA,CAAiB,CAAA,CACjB,OAAO,KAFW,CAhNS,CAynB/B,KAAIiB,GAAe1hC,CAAA,CAAO,QAAP,CAAnB,CACIyjC,GAAsB,EAD1B,CAEIxB,EAFJ,CAgEImc,GAAY,CAEZ,MAFY,CAELC,QAAQ,EAAE,CAAC,MAAO,KAAR,CAFL,CAGZ,MAHY,CAGLC,QAAQ,EAAE,CAAC,MAAO,CAAA,CAAR,CAHL,CAIZ,OAJY,CAIJC,QAAQ,EAAE,CAAC,MAAO,CAAA,CAAR,CAJN,WAKF17C,CALE,CAMZ,GANY,CAMR27C,QAAQ,CAACt4C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAC7BD,CAAA,CAAEA,CAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAiB4Q,EAAA,CAAEA,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CACrB,OAAIhY,EAAA,CAAU2oB,CAAV,CAAJ,CACM3oB,CAAA,CAAU4oB,CAAV,CAAJ,CACSD,CADT,CACaC,CADb,CAGOD,CAJT,CAMO3oB,CAAA,CAAU4oB,CAAV,CAAA,CAAaA,CAAb,CAAe/rB,CARO,CANnB,CAeZ,GAfY,CAeR0+C,QAAQ,CAACv4C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CACzBD,CAAA,CAAEA,CAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAiB4Q,EAAA;AAAEA,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CACrB,QAAQhY,CAAA,CAAU2oB,CAAV,CAAA,CAAaA,CAAb,CAAe,CAAvB,GAA2B3oB,CAAA,CAAU4oB,CAAV,CAAA,CAAaA,CAAb,CAAe,CAA1C,CAFyB,CAfnB,CAmBZ,GAnBY,CAmBR4yB,QAAQ,CAACx4C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,CAAuB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAxB,CAnBnB,CAoBZ,GApBY,CAoBRyjC,QAAQ,CAACz4C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,CAAuB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAxB,CApBnB,CAqBZ,GArBY,CAqBR0jC,QAAQ,CAAC14C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,CAAuB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAxB,CArBnB,CAsBZ,GAtBY,CAsBR2jC,QAAQ,CAAC34C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,CAAuB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAxB,CAtBnB,CAuBZ,GAvBY,CAuBRrY,CAvBQ,CAwBZ,KAxBY,CAwBNi8C,QAAQ,CAAC54C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAkBC,CAAlB,CAAoB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,GAAyB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAA1B,CAxBtB,CAyBZ,KAzBY,CAyBN6jC,QAAQ,CAAC74C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAkBC,CAAlB,CAAoB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,GAAyB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAA1B,CAzBtB,CA0BZ,IA1BY,CA0BP8jC,QAAQ,CAAC94C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,EAAwB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAzB,CA1BpB,CA2BZ,IA3BY,CA2BP+jC,QAAQ,CAAC/4C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,EAAwB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAzB,CA3BpB,CA4BZ,GA5BY,CA4BRgkC,QAAQ,CAACh5C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,CAAuB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAxB,CA5BnB,CA6BZ,GA7BY,CA6BRikC,QAAQ,CAACj5C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,CAAuB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAxB,CA7BnB,CA8BZ,IA9BY,CA8BPkkC,QAAQ,CAACl5C,CAAD;AAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,EAAwB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAzB,CA9BpB,CA+BZ,IA/BY,CA+BPmkC,QAAQ,CAACn5C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,EAAwB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAzB,CA/BpB,CAgCZ,IAhCY,CAgCPokC,QAAQ,CAACp5C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,EAAwB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAzB,CAhCpB,CAiCZ,IAjCY,CAiCPqkC,QAAQ,CAACr5C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,EAAwB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAzB,CAjCpB,CAkCZ,GAlCY,CAkCRskC,QAAQ,CAACt5C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOD,EAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAP,CAAuB4Q,CAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAxB,CAlCnB,CAoCZ,GApCY,CAoCRukC,QAAQ,CAACv5C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiBC,CAAjB,CAAmB,CAAC,MAAOA,EAAA,CAAE5lB,CAAF,CAAQgV,CAAR,CAAA,CAAgBhV,CAAhB,CAAsBgV,CAAtB,CAA8B2Q,CAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAA9B,CAAR,CApCnB,CAqCZ,GArCY,CAqCRwkC,QAAQ,CAACx5C,CAAD,CAAOgV,CAAP,CAAe2Q,CAAf,CAAiB,CAAC,MAAO,CAACA,CAAA,CAAE3lB,CAAF,CAAQgV,CAAR,CAAT,CArCjB,CAhEhB,CAwGIykC,GAAS,GAAK,IAAL,GAAe,IAAf,GAAyB,IAAzB,GAAmC,IAAnC,GAA6C,IAA7C,CAAmD,GAAnD,CAAuD,GAAvD,CAA4D,GAA5D,CAAgE,GAAhE,CAxGb,CAiHI/b,GAAQA,QAAS,CAAC/hB,CAAD,CAAU,CAC7B,IAAAA,QAAA,CAAeA,CADc,CAI/B+hB,GAAAroB,UAAA,CAAkB,aACHqoB,EADG,KAGXgc,QAAS,CAAC7vB,CAAD,CAAO,CACnB,IAAAA,KAAA,CAAYA,CAEZ,KAAAtuB,MAAA,CAAa,CACb,KAAAo+C,GAAA,CAAU9/C,CACV,KAAA+/C,OAAA,CAAc,GAId,KAFA,IAAAC,OAEA,CAFc,EAEd,CAAO,IAAAt+C,MAAP;AAAoB,IAAAsuB,KAAA3vB,OAApB,CAAA,CAAsC,CACpC,IAAAy/C,GAAA,CAAU,IAAA9vB,KAAAzqB,OAAA,CAAiB,IAAA7D,MAAjB,CACV,IAAI,IAAAu+C,GAAA,CAAQ,KAAR,CAAJ,CACE,IAAAC,WAAA,CAAgB,IAAAJ,GAAhB,CADF,KAEO,IAAI,IAAAz8C,SAAA,CAAc,IAAAy8C,GAAd,CAAJ,EAA8B,IAAAG,GAAA,CAAQ,GAAR,CAA9B,EAA8C,IAAA58C,SAAA,CAAc,IAAA88C,KAAA,EAAd,CAA9C,CACL,IAAAC,WAAA,EADK,KAEA,IAAI,IAAAC,QAAA,CAAa,IAAAP,GAAb,CAAJ,CACL,IAAAQ,UAAA,EADK,KAEA,IAAI,IAAAL,GAAA,CAAQ,aAAR,CAAJ,CACL,IAAAD,OAAA9+C,KAAA,CAAiB,OACR,IAAAQ,MADQ,MAET,IAAAo+C,GAFS,CAAjB,CAIA,CAAA,IAAAp+C,MAAA,EALK,KAMA,IAAI,IAAA6+C,aAAA,CAAkB,IAAAT,GAAlB,CAAJ,CAAgC,CACrC,IAAAp+C,MAAA,EACA,SAFqC,CAAhC,IAGA,CACD8+C,CAAAA,CAAM,IAAAV,GAANU,CAAgB,IAAAL,KAAA,EACpB,KAAIM,EAAMD,CAANC,CAAY,IAAAN,KAAA,CAAU,CAAV,CAAhB,CACI/5C,EAAKi4C,EAAA,CAAU,IAAAyB,GAAV,CADT,CAEIY,EAAMrC,EAAA,CAAUmC,CAAV,CAFV,CAGIG,EAAMtC,EAAA,CAAUoC,CAAV,CACNE,EAAJ,EACE,IAAAX,OAAA9+C,KAAA,CAAiB,OAAQ,IAAAQ,MAAR;KAA0B++C,CAA1B,IAAmCE,CAAnC,CAAjB,CACA,CAAA,IAAAj/C,MAAA,EAAc,CAFhB,EAGWg/C,CAAJ,EACL,IAAAV,OAAA9+C,KAAA,CAAiB,OAAQ,IAAAQ,MAAR,MAA0B8+C,CAA1B,IAAmCE,CAAnC,CAAjB,CACA,CAAA,IAAAh/C,MAAA,EAAc,CAFT,EAGI0E,CAAJ,EACL,IAAA45C,OAAA9+C,KAAA,CAAiB,OACR,IAAAQ,MADQ,MAET,IAAAo+C,GAFS,IAGX15C,CAHW,CAAjB,CAKA,CAAA,IAAA1E,MAAA,EAAc,CANT,EAQL,IAAAk/C,WAAA,CAAgB,4BAAhB,CAA8C,IAAAl/C,MAA9C,CAA0D,IAAAA,MAA1D,CAAuE,CAAvE,CApBG,CAuBP,IAAAq+C,OAAA,CAAc,IAAAD,GAxCsB,CA0CtC,MAAO,KAAAE,OAnDY,CAHL,IAyDZC,QAAQ,CAACY,CAAD,CAAQ,CAClB,MAAmC,EAAnC,GAAOA,CAAAx8C,QAAA,CAAc,IAAAy7C,GAAd,CADW,CAzDJ,KA6DXgB,QAAQ,CAACD,CAAD,CAAQ,CACnB,MAAuC,EAAvC,GAAOA,CAAAx8C,QAAA,CAAc,IAAA07C,OAAd,CADY,CA7DL,MAiEVI,QAAQ,CAAC9+C,CAAD,CAAI,CACZy6B,CAAAA,CAAMz6B,CAANy6B,EAAW,CACf,OAAQ,KAAAp6B,MAAD,CAAco6B,CAAd,CAAoB,IAAA9L,KAAA3vB,OAApB,CAAwC,IAAA2vB,KAAAzqB,OAAA,CAAiB,IAAA7D,MAAjB,CAA8Bo6B,CAA9B,CAAxC,CAA6E,CAAA,CAFpE,CAjEF,UAsENz4B,QAAQ,CAACy8C,CAAD,CAAK,CACrB,MAAQ,GAAR;AAAeA,CAAf,EAA2B,GAA3B,EAAqBA,CADA,CAtEP,cA0EFS,QAAQ,CAACT,CAAD,CAAK,CAEzB,MAAe,GAAf,GAAQA,CAAR,EAA6B,IAA7B,GAAsBA,CAAtB,EAA4C,IAA5C,GAAqCA,CAArC,EACe,IADf,GACQA,CADR,EAC8B,IAD9B,GACuBA,CADvB,EAC6C,QAD7C,GACsCA,CAHb,CA1EX,SAgFPO,QAAQ,CAACP,CAAD,CAAK,CACpB,MAAQ,GAAR,EAAeA,CAAf,EAA2B,GAA3B,EAAqBA,CAArB,EACQ,GADR,EACeA,CADf,EAC2B,GAD3B,EACqBA,CADrB,EAEQ,GAFR,GAEgBA,CAFhB,EAE6B,GAF7B,GAEsBA,CAHF,CAhFN,eAsFDiB,QAAQ,CAACjB,CAAD,CAAK,CAC1B,MAAe,GAAf,GAAQA,CAAR,EAA6B,GAA7B,GAAsBA,CAAtB,EAAoC,IAAAz8C,SAAA,CAAcy8C,CAAd,CADV,CAtFZ,YA0FJc,QAAQ,CAAC5iC,CAAD,CAAQgjC,CAAR,CAAeC,CAAf,CAAoB,CACtCA,CAAA,CAAMA,CAAN,EAAa,IAAAv/C,MACTw/C,EAAAA,CAAU/9C,CAAA,CAAU69C,CAAV,CACA,CAAJ,IAAI,CAAGA,CAAH,CAAY,GAAZ,CAAkB,IAAAt/C,MAAlB,CAA+B,IAA/B,CAAsC,IAAAsuB,KAAA9O,UAAA,CAAoB8/B,CAApB,CAA2BC,CAA3B,CAAtC,CAAwE,GAAxE,CACJ,GADI,CACEA,CAChB,MAAMtf,GAAA,CAAa,QAAb,CACF3jB,CADE,CACKkjC,CADL,CACa,IAAAlxB,KADb,CAAN,CALsC,CA1FxB,YAmGJowB,QAAQ,EAAG,CAGrB,IAFA,IAAI3P,EAAS,EAAb,CACIuQ,EAAQ,IAAAt/C,MACZ,CAAO,IAAAA,MAAP,CAAoB,IAAAsuB,KAAA3vB,OAApB,CAAA,CAAsC,CACpC,IAAIy/C,EAAKz4C,CAAA,CAAU,IAAA2oB,KAAAzqB,OAAA,CAAiB,IAAA7D,MAAjB,CAAV,CACT;GAAU,GAAV,EAAIo+C,CAAJ,EAAiB,IAAAz8C,SAAA,CAAcy8C,CAAd,CAAjB,CACErP,CAAA,EAAUqP,CADZ,KAEO,CACL,IAAIqB,EAAS,IAAAhB,KAAA,EACb,IAAU,GAAV,EAAIL,CAAJ,EAAiB,IAAAiB,cAAA,CAAmBI,CAAnB,CAAjB,CACE1Q,CAAA,EAAUqP,CADZ,KAEO,IAAI,IAAAiB,cAAA,CAAmBjB,CAAnB,CAAJ,EACHqB,CADG,EACO,IAAA99C,SAAA,CAAc89C,CAAd,CADP,EAEiC,GAFjC,EAEH1Q,CAAAlrC,OAAA,CAAckrC,CAAApwC,OAAd,CAA8B,CAA9B,CAFG,CAGLowC,CAAA,EAAUqP,CAHL,KAIA,IAAI,CAAA,IAAAiB,cAAA,CAAmBjB,CAAnB,CAAJ,EACDqB,CADC,EACU,IAAA99C,SAAA,CAAc89C,CAAd,CADV,EAEiC,GAFjC,EAEH1Q,CAAAlrC,OAAA,CAAckrC,CAAApwC,OAAd,CAA8B,CAA9B,CAFG,CAKL,KALK,KAGL,KAAAugD,WAAA,CAAgB,kBAAhB,CAXG,CAgBP,IAAAl/C,MAAA,EApBoC,CAsBtC+uC,CAAA,EAAS,CACT,KAAAuP,OAAA9+C,KAAA,CAAiB,OACR8/C,CADQ,MAETvQ,CAFS,SAGN,CAAA,CAHM,UAIL,CAAA,CAJK,IAKXrqC,QAAQ,EAAG,CAAE,MAAOqqC,EAAT,CALA,CAAjB,CA1BqB,CAnGP,WAsIL6P,QAAQ,EAAG,CAQpB,IAPA,IAAIxc,EAAS,IAAb,CAEIsd,EAAQ,EAFZ,CAGIJ,EAAQ,IAAAt/C,MAHZ,CAKI2/C,CALJ,CAKaC,CALb,CAKwBC,CALxB,CAKoCzB,CAEpC,CAAO,IAAAp+C,MAAP,CAAoB,IAAAsuB,KAAA3vB,OAApB,CAAA,CAAsC,CACpCy/C,CAAA,CAAK,IAAA9vB,KAAAzqB,OAAA,CAAiB,IAAA7D,MAAjB,CACL;GAAW,GAAX,GAAIo+C,CAAJ,EAAkB,IAAAO,QAAA,CAAaP,CAAb,CAAlB,EAAsC,IAAAz8C,SAAA,CAAcy8C,CAAd,CAAtC,CACa,GACX,GADIA,CACJ,GADgBuB,CAChB,CAD0B,IAAA3/C,MAC1B,EAAA0/C,CAAA,EAAStB,CAFX,KAIE,MAEF,KAAAp+C,MAAA,EARoC,CAYtC,GAAI2/C,CAAJ,CAEE,IADAC,CACA,CADY,IAAA5/C,MACZ,CAAO4/C,CAAP,CAAmB,IAAAtxB,KAAA3vB,OAAnB,CAAA,CAAqC,CACnCy/C,CAAA,CAAK,IAAA9vB,KAAAzqB,OAAA,CAAiB+7C,CAAjB,CACL,IAAW,GAAX,GAAIxB,CAAJ,CAAgB,CACdyB,CAAA,CAAaH,CAAA14B,OAAA,CAAa24B,CAAb,CAAuBL,CAAvB,CAA+B,CAA/B,CACbI,EAAA,CAAQA,CAAA14B,OAAA,CAAa,CAAb,CAAgB24B,CAAhB,CAA0BL,CAA1B,CACR,KAAAt/C,MAAA,CAAa4/C,CACb,MAJc,CAMhB,GAAI,IAAAf,aAAA,CAAkBT,CAAlB,CAAJ,CACEwB,CAAA,EADF,KAGE,MAXiC,CAiBnCzuB,CAAAA,CAAQ,OACHmuB,CADG,MAEJI,CAFI,CAMZ,IAAI/C,EAAAv9C,eAAA,CAAyBsgD,CAAzB,CAAJ,CACEvuB,CAAAzsB,GAEA,CAFWi4C,EAAA,CAAU+C,CAAV,CAEX,CADAvuB,CAAAhH,QACA,CADgB,CAAA,CAChB,CAAAgH,CAAAhX,SAAA,CAAiB,CAAA,CAHnB,KAIO,CACL,IAAIlQ,EAASo3B,EAAA,CAASqe,CAAT,CAAgB,IAAAt/B,QAAhB,CAA8B,IAAAkO,KAA9B,CACb6C,EAAAzsB,GAAA,CAAW/D,CAAA,CAAO,QAAQ,CAAC8D,CAAD,CAAOgV,CAAP,CAAe,CACvC,MAAQxP,EAAA,CAAOxF,CAAP,CAAagV,CAAb,CAD+B,CAA9B,CAER,QACO6Q,QAAQ,CAAC7lB,CAAD,CAAO3E,CAAP,CAAc,CAC5B,MAAOqgC,GAAA,CAAO17B,CAAP,CAAai7C,CAAb,CAAoB5/C,CAApB,CAA2BsiC,CAAA9T,KAA3B,CAAwC8T,CAAAhiB,QAAxC,CADqB,CAD7B,CAFQ,CAFN,CAWP,IAAAk+B,OAAA9+C,KAAA,CAAiB2xB,CAAjB,CAEI0uB;CAAJ,GACE,IAAAvB,OAAA9+C,KAAA,CAAiB,OACTmgD,CADS,MAET,GAFS,CAAjB,CAIA,CAAA,IAAArB,OAAA9+C,KAAA,CAAiB,OACRmgD,CADQ,CACE,CADF,MAETE,CAFS,CAAjB,CALF,CA9DoB,CAtIN,YAgNJrB,QAAQ,CAACsB,CAAD,CAAQ,CAC1B,IAAIR,EAAQ,IAAAt/C,MACZ,KAAAA,MAAA,EAIA,KAHA,IAAIkxC,EAAS,EAAb,CACI6O,EAAYD,CADhB,CAEI1gC,EAAS,CAAA,CACb,CAAO,IAAApf,MAAP,CAAoB,IAAAsuB,KAAA3vB,OAApB,CAAA,CAAsC,CACpC,IAAIy/C,EAAK,IAAA9vB,KAAAzqB,OAAA,CAAiB,IAAA7D,MAAjB,CAAT,CACA+/C,EAAAA,CAAAA,CAAa3B,CACb,IAAIh/B,CAAJ,CACa,GAAX,GAAIg/B,CAAJ,EACM4B,CAIJ,CAJU,IAAA1xB,KAAA9O,UAAA,CAAoB,IAAAxf,MAApB,CAAiC,CAAjC,CAAoC,IAAAA,MAApC,CAAiD,CAAjD,CAIV,CAHKggD,CAAA15C,MAAA,CAAU,aAAV,CAGL,EAFE,IAAA44C,WAAA,CAAgB,6BAAhB,CAAgDc,CAAhD,CAAsD,GAAtD,CAEF,CADA,IAAAhgD,MACA,EADc,CACd,CAAAkxC,CAAA,EAAU7wC,MAAAC,aAAA,CAAoBU,QAAA,CAASg/C,CAAT,CAAc,EAAd,CAApB,CALZ,EASI9O,CATJ,CAQE,CADI+O,CACJ,CADU/B,EAAA,CAAOE,CAAP,CACV,EACElN,CADF,CACY+O,CADZ,CAGE/O,CAHF,CAGYkN,CAGd,CAAAh/B,CAAA,CAAS,CAAA,CAfX,KAgBO,IAAW,IAAX,GAAIg/B,CAAJ,CACLh/B,CAAA,CAAS,CAAA,CADJ,KAEA,CAAA,GAAIg/B,CAAJ,GAAW0B,CAAX,CAAkB,CACvB,IAAA9/C,MAAA,EACA,KAAAs+C,OAAA9+C,KAAA,CAAiB,OACR8/C,CADQ;KAETS,CAFS,QAGP7O,CAHO,SAIN,CAAA,CAJM,UAKL,CAAA,CALK,IAMXxsC,QAAQ,EAAG,CAAE,MAAOwsC,EAAT,CANA,CAAjB,CAQA,OAVuB,CAYvBA,CAAA,EAAUkN,CAZL,CAcP,IAAAp+C,MAAA,EAnCoC,CAqCtC,IAAAk/C,WAAA,CAAgB,oBAAhB,CAAsCI,CAAtC,CA3C0B,CAhNZ,CAmQlB,KAAIjd,GAASA,QAAS,CAACH,CAAD,CAAQH,CAAR,CAAiB3hB,CAAjB,CAA0B,CAC9C,IAAA8hB,MAAA,CAAaA,CACb,KAAAH,QAAA,CAAeA,CACf,KAAA3hB,QAAA,CAAeA,CAH+B,CAMhDiiB,GAAA6d,KAAA,CAAcv/C,CAAA,CAAO,QAAS,EAAG,CAC/B,MAAO,EADwB,CAAnB,CAEX,UACS,CAAA,CADT,CAFW,CAMd0hC,GAAAvoB,UAAA,CAAmB,aACJuoB,EADI,OAGV78B,QAAS,CAAC8oB,CAAD,CAAO,CACrB,IAAAA,KAAA,CAAYA,CAEZ,KAAAgwB,OAAA,CAAc,IAAApc,MAAAic,IAAA,CAAe7vB,CAAf,CAEVxuB,EAAAA,CAAQ,IAAAqgD,WAAA,EAEe,EAA3B,GAAI,IAAA7B,OAAA3/C,OAAJ,EACE,IAAAugD,WAAA,CAAgB,wBAAhB,CAA0C,IAAAZ,OAAA,CAAY,CAAZ,CAA1C,CAGFx+C,EAAAqqB,QAAA,CAAgB,CAAC,CAACrqB,CAAAqqB,QAClBrqB,EAAAqa,SAAA,CAAiB,CAAC,CAACra,CAAAqa,SAEnB,OAAOra,EAdc,CAHN,SAoBRsgD,QAAS,EAAG,CACnB,IAAIA,CACJ;GAAI,IAAAC,OAAA,CAAY,GAAZ,CAAJ,CACED,CACA,CADU,IAAAE,YAAA,EACV,CAAA,IAAAC,QAAA,CAAa,GAAb,CAFF,KAGO,IAAI,IAAAF,OAAA,CAAY,GAAZ,CAAJ,CACLD,CAAA,CAAU,IAAAI,iBAAA,EADL,KAEA,IAAI,IAAAH,OAAA,CAAY,GAAZ,CAAJ,CACLD,CAAA,CAAU,IAAA9N,OAAA,EADL,KAEA,CACL,IAAInhB,EAAQ,IAAAkvB,OAAA,EAEZ,EADAD,CACA,CADUjvB,CAAAzsB,GACV,GACE,IAAAw6C,WAAA,CAAgB,0BAAhB,CAA4C/tB,CAA5C,CAEFivB,EAAAj2B,QAAA,CAAkB,CAAC,CAACgH,CAAAhH,QACpBi2B,EAAAjmC,SAAA,CAAmB,CAAC,CAACgX,CAAAhX,SAPhB,CAWP,IADA,IAAUlb,CACV,CAAQopC,CAAR,CAAe,IAAAgY,OAAA,CAAY,GAAZ,CAAiB,GAAjB,CAAsB,GAAtB,CAAf,CAAA,CACoB,GAAlB,GAAIhY,CAAA/Z,KAAJ,EACE8xB,CACA,CADU,IAAAK,aAAA,CAAkBL,CAAlB,CAA2BnhD,CAA3B,CACV,CAAAA,CAAA,CAAU,IAFZ,EAGyB,GAAlB,GAAIopC,CAAA/Z,KAAJ,EACLrvB,CACA,CADUmhD,CACV,CAAAA,CAAA,CAAU,IAAAM,YAAA,CAAiBN,CAAjB,CAFL,EAGkB,GAAlB,GAAI/X,CAAA/Z,KAAJ,EACLrvB,CACA,CADUmhD,CACV,CAAAA,CAAA,CAAU,IAAAO,YAAA,CAAiBP,CAAjB,CAFL,EAIL,IAAAlB,WAAA,CAAgB,YAAhB,CAGJ,OAAOkB,EAlCY,CApBJ,YAyDLlB,QAAQ,CAAC0B,CAAD;AAAMzvB,CAAN,CAAa,CAC/B,KAAM8O,GAAA,CAAa,QAAb,CAEA9O,CAAA7C,KAFA,CAEYsyB,CAFZ,CAEkBzvB,CAAAnxB,MAFlB,CAEgC,CAFhC,CAEoC,IAAAsuB,KAFpC,CAE+C,IAAAA,KAAA9O,UAAA,CAAoB2R,CAAAnxB,MAApB,CAF/C,CAAN,CAD+B,CAzDhB,WA+DN6gD,QAAQ,EAAG,CACpB,GAA2B,CAA3B,GAAI,IAAAvC,OAAA3/C,OAAJ,CACE,KAAMshC,GAAA,CAAa,MAAb,CAA0D,IAAA3R,KAA1D,CAAN,CACF,MAAO,KAAAgwB,OAAA,CAAY,CAAZ,CAHa,CA/DL,MAqEXG,QAAQ,CAACqC,CAAD,CAAKC,CAAL,CAASC,CAAT,CAAaC,CAAb,CAAiB,CAC7B,GAAyB,CAAzB,CAAI,IAAA3C,OAAA3/C,OAAJ,CAA4B,CAC1B,IAAIwyB,EAAQ,IAAAmtB,OAAA,CAAY,CAAZ,CAAZ,CACI4C,EAAI/vB,CAAA7C,KACR,IAAI4yB,CAAJ,GAAUJ,CAAV,EAAgBI,CAAhB,GAAsBH,CAAtB,EAA4BG,CAA5B,GAAkCF,CAAlC,EAAwCE,CAAxC,GAA8CD,CAA9C,EACK,EAACH,CAAD,EAAQC,CAAR,EAAeC,CAAf,EAAsBC,CAAtB,CADL,CAEE,MAAO9vB,EALiB,CAQ5B,MAAO,CAAA,CATsB,CArEd,QAiFTkvB,QAAQ,CAACS,CAAD,CAAKC,CAAL,CAASC,CAAT,CAAaC,CAAb,CAAgB,CAE9B,MAAA,CADI9vB,CACJ,CADY,IAAAstB,KAAA,CAAUqC,CAAV,CAAcC,CAAd,CAAkBC,CAAlB,CAAsBC,CAAtB,CACZ,GACE,IAAA3C,OAAAhtC,MAAA,EACO6f,CAAAA,CAFT,EAIO,CAAA,CANuB,CAjFf,SA0FRovB,QAAQ,CAACO,CAAD,CAAI,CACd,IAAAT,OAAA,CAAYS,CAAZ,CAAL,EACE,IAAA5B,WAAA,CAAgB,4BAAhB,CAA+C4B,CAA/C,CAAoD,GAApD,CAAyD,IAAArC,KAAA,EAAzD,CAFiB,CA1FJ;QAgGR0C,QAAQ,CAACz8C,CAAD,CAAK08C,CAAL,CAAY,CAC3B,MAAOzgD,EAAA,CAAO,QAAQ,CAAC8D,CAAD,CAAOgV,CAAP,CAAe,CACnC,MAAO/U,EAAA,CAAGD,CAAH,CAASgV,CAAT,CAAiB2nC,CAAjB,CAD4B,CAA9B,CAEJ,UACQA,CAAAjnC,SADR,CAFI,CADoB,CAhGZ,WAwGNknC,QAAQ,CAACC,CAAD,CAAOC,CAAP,CAAeH,CAAf,CAAqB,CACtC,MAAOzgD,EAAA,CAAO,QAAQ,CAAC8D,CAAD,CAAOgV,CAAP,CAAc,CAClC,MAAO6nC,EAAA,CAAK78C,CAAL,CAAWgV,CAAX,CAAA,CAAqB8nC,CAAA,CAAO98C,CAAP,CAAagV,CAAb,CAArB,CAA4C2nC,CAAA,CAAM38C,CAAN,CAAYgV,CAAZ,CADjB,CAA7B,CAEJ,UACS6nC,CAAAnnC,SADT,EAC0BonC,CAAApnC,SAD1B,EAC6CinC,CAAAjnC,SAD7C,CAFI,CAD+B,CAxGvB,UAgHPqnC,QAAQ,CAACF,CAAD,CAAO58C,CAAP,CAAW08C,CAAX,CAAkB,CAClC,MAAOzgD,EAAA,CAAO,QAAQ,CAAC8D,CAAD,CAAOgV,CAAP,CAAe,CACnC,MAAO/U,EAAA,CAAGD,CAAH,CAASgV,CAAT,CAAiB6nC,CAAjB,CAAuBF,CAAvB,CAD4B,CAA9B,CAEJ,UACQE,CAAAnnC,SADR,EACyBinC,CAAAjnC,SADzB,CAFI,CAD2B,CAhHnB,YAwHLgmC,QAAQ,EAAG,CAErB,IADA,IAAIA,EAAa,EACjB,CAAA,CAAA,CAGE,GAFyB,CAErB,CAFA,IAAA7B,OAAA3/C,OAEA,EAF2B,CAAA,IAAA8/C,KAAA,CAAU,GAAV,CAAe,GAAf,CAAoB,GAApB,CAAyB,GAAzB,CAE3B,EADF0B,CAAA3gD,KAAA,CAAgB,IAAA8gD,YAAA,EAAhB,CACE,CAAA,CAAC,IAAAD,OAAA,CAAY,GAAZ,CAAL,CAGE,MAA8B,EACvB,GADCF,CAAAxhD,OACD,CAADwhD,CAAA,CAAW,CAAX,CAAC,CACD,QAAQ,CAAC17C,CAAD,CAAOgV,CAAP,CAAe,CAErB,IADA,IAAI3Z,CAAJ,CACSH;AAAI,CAAb,CAAgBA,CAAhB,CAAoBwgD,CAAAxhD,OAApB,CAAuCgB,CAAA,EAAvC,CAA4C,CAC1C,IAAI8hD,EAAYtB,CAAA,CAAWxgD,CAAX,CACZ8hD,EAAJ,GACE3hD,CADF,CACU2hD,CAAA,CAAUh9C,CAAV,CAAgBgV,CAAhB,CADV,CAF0C,CAM5C,MAAO3Z,EARc,CAVZ,CAxHN,aAgJJwgD,QAAQ,EAAG,CAGtB,IAFA,IAAIgB,EAAO,IAAA/vB,WAAA,EAAX,CACIJ,CACJ,CAAA,CAAA,CACE,GAAKA,CAAL,CAAa,IAAAkvB,OAAA,CAAY,GAAZ,CAAb,CACEiB,CAAA,CAAO,IAAAE,SAAA,CAAcF,CAAd,CAAoBnwB,CAAAzsB,GAApB,CAA8B,IAAAqM,OAAA,EAA9B,CADT,KAGE,OAAOuwC,EAPW,CAhJP,QA4JTvwC,QAAQ,EAAG,CAIjB,IAHA,IAAIogB,EAAQ,IAAAkvB,OAAA,EAAZ,CACI37C,EAAK,IAAAq9B,QAAA,CAAa5Q,CAAA7C,KAAb,CADT,CAEIozB,EAAS,EACb,CAAA,CAAA,CACE,GAAKvwB,CAAL,CAAa,IAAAkvB,OAAA,CAAY,GAAZ,CAAb,CACEqB,CAAAliD,KAAA,CAAY,IAAA+xB,WAAA,EAAZ,CADF,KAEO,CACL,IAAIowB,EAAWA,QAAQ,CAACl9C,CAAD,CAAOgV,CAAP,CAAe84B,CAAf,CAAsB,CACvC74B,CAAAA,CAAO,CAAC64B,CAAD,CACX,KAAK,IAAI5yC,EAAI,CAAb,CAAgBA,CAAhB,CAAoB+hD,CAAA/iD,OAApB,CAAmCgB,CAAA,EAAnC,CACE+Z,CAAAla,KAAA,CAAUkiD,CAAA,CAAO/hD,CAAP,CAAA,CAAU8E,CAAV,CAAgBgV,CAAhB,CAAV,CAEF,OAAO/U,EAAAI,MAAA,CAASL,CAAT,CAAeiV,CAAf,CALoC,CAO7C,OAAO,SAAQ,EAAG,CAChB,MAAOioC,EADS,CARb,CAPQ,CA5JF,YAkLLpwB,QAAQ,EAAG,CACrB,MAAO,KAAAqwB,WAAA,EADc,CAlLN,YAsLLA,QAAQ,EAAG,CACrB,IAAIN;AAAO,IAAAO,QAAA,EAAX,CACIT,CADJ,CAEIjwB,CACJ,OAAA,CAAKA,CAAL,CAAa,IAAAkvB,OAAA,CAAY,GAAZ,CAAb,GACOiB,CAAAh3B,OAKE,EAJL,IAAA40B,WAAA,CAAgB,0BAAhB,CACI,IAAA5wB,KAAA9O,UAAA,CAAoB,CAApB,CAAuB2R,CAAAnxB,MAAvB,CADJ,CAC0C,0BAD1C,CACsEmxB,CADtE,CAIK,CADPiwB,CACO,CADC,IAAAS,QAAA,EACD,CAAA,QAAQ,CAACp5C,CAAD,CAAQgR,CAAR,CAAgB,CAC7B,MAAO6nC,EAAAh3B,OAAA,CAAY7hB,CAAZ,CAAmB24C,CAAA,CAAM34C,CAAN,CAAagR,CAAb,CAAnB,CAAyCA,CAAzC,CADsB,CANjC,EAUO6nC,CAdc,CAtLN,SAuMRO,QAAQ,EAAG,CAClB,IAAIP,EAAO,IAAAQ,UAAA,EAAX,CACIP,CADJ,CAEIpwB,CACJ,IAAa,IAAAkvB,OAAA,CAAY,GAAZ,CAAb,CAAgC,CAC9BkB,CAAA,CAAS,IAAAM,QAAA,EACT,IAAK1wB,CAAL,CAAa,IAAAkvB,OAAA,CAAY,GAAZ,CAAb,CACE,MAAO,KAAAgB,UAAA,CAAeC,CAAf,CAAqBC,CAArB,CAA6B,IAAAM,QAAA,EAA7B,CAEP,KAAA3C,WAAA,CAAgB,YAAhB,CAA8B/tB,CAA9B,CAL4B,CAAhC,IAQE,OAAOmwB,EAZS,CAvMH,WAuNNQ,QAAQ,EAAG,CAGpB,IAFA,IAAIR,EAAO,IAAAS,WAAA,EAAX,CACI5wB,CACJ,CAAA,CAAA,CACE,GAAKA,CAAL,CAAa,IAAAkvB,OAAA,CAAY,IAAZ,CAAb,CACEiB,CAAA,CAAO,IAAAE,SAAA,CAAcF,CAAd;AAAoBnwB,CAAAzsB,GAApB,CAA8B,IAAAq9C,WAAA,EAA9B,CADT,KAGE,OAAOT,EAPS,CAvNL,YAmOLS,QAAQ,EAAG,CACrB,IAAIT,EAAO,IAAAU,SAAA,EAAX,CACI7wB,CACJ,IAAKA,CAAL,CAAa,IAAAkvB,OAAA,CAAY,IAAZ,CAAb,CACEiB,CAAA,CAAO,IAAAE,SAAA,CAAcF,CAAd,CAAoBnwB,CAAAzsB,GAApB,CAA8B,IAAAq9C,WAAA,EAA9B,CAET,OAAOT,EANc,CAnON,UA4OPU,QAAQ,EAAG,CACnB,IAAIV,EAAO,IAAAW,WAAA,EAAX,CACI9wB,CACJ,IAAKA,CAAL,CAAa,IAAAkvB,OAAA,CAAY,IAAZ,CAAiB,IAAjB,CAAsB,KAAtB,CAA4B,KAA5B,CAAb,CACEiB,CAAA,CAAO,IAAAE,SAAA,CAAcF,CAAd,CAAoBnwB,CAAAzsB,GAApB,CAA8B,IAAAs9C,SAAA,EAA9B,CAET,OAAOV,EANY,CA5OJ,YAqPLW,QAAQ,EAAG,CACrB,IAAIX,EAAO,IAAAY,SAAA,EAAX,CACI/wB,CACJ,IAAKA,CAAL,CAAa,IAAAkvB,OAAA,CAAY,GAAZ,CAAiB,GAAjB,CAAsB,IAAtB,CAA4B,IAA5B,CAAb,CACEiB,CAAA,CAAO,IAAAE,SAAA,CAAcF,CAAd,CAAoBnwB,CAAAzsB,GAApB,CAA8B,IAAAu9C,WAAA,EAA9B,CAET,OAAOX,EANc,CArPN,UA8PPY,QAAQ,EAAG,CAGnB,IAFA,IAAIZ,EAAO,IAAAa,eAAA,EAAX,CACIhxB,CACJ,CAAQA,CAAR,CAAgB,IAAAkvB,OAAA,CAAY,GAAZ;AAAgB,GAAhB,CAAhB,CAAA,CACEiB,CAAA,CAAO,IAAAE,SAAA,CAAcF,CAAd,CAAoBnwB,CAAAzsB,GAApB,CAA8B,IAAAy9C,eAAA,EAA9B,CAET,OAAOb,EANY,CA9PJ,gBAuQDa,QAAQ,EAAG,CAGzB,IAFA,IAAIb,EAAO,IAAAc,MAAA,EAAX,CACIjxB,CACJ,CAAQA,CAAR,CAAgB,IAAAkvB,OAAA,CAAY,GAAZ,CAAgB,GAAhB,CAAoB,GAApB,CAAhB,CAAA,CACEiB,CAAA,CAAO,IAAAE,SAAA,CAAcF,CAAd,CAAoBnwB,CAAAzsB,GAApB,CAA8B,IAAA09C,MAAA,EAA9B,CAET,OAAOd,EANkB,CAvQV,OAgRVc,QAAQ,EAAG,CAChB,IAAIjxB,CACJ,OAAI,KAAAkvB,OAAA,CAAY,GAAZ,CAAJ,CACS,IAAAD,QAAA,EADT,CAEO,CAAKjvB,CAAL,CAAa,IAAAkvB,OAAA,CAAY,GAAZ,CAAb,EACE,IAAAmB,SAAA,CAAcnf,EAAA6d,KAAd,CAA2B/uB,CAAAzsB,GAA3B,CAAqC,IAAA09C,MAAA,EAArC,CADF,CAEA,CAAKjxB,CAAL,CAAa,IAAAkvB,OAAA,CAAY,GAAZ,CAAb,EACE,IAAAc,QAAA,CAAahwB,CAAAzsB,GAAb,CAAuB,IAAA09C,MAAA,EAAvB,CADF,CAGE,IAAAhC,QAAA,EATO,CAhRD,aA6RJO,QAAQ,CAACrO,CAAD,CAAS,CAC5B,IAAIlQ,EAAS,IAAb,CACIigB,EAAQ,IAAAhC,OAAA,EAAA/xB,KADZ,CAEIrkB,EAASo3B,EAAA,CAASghB,CAAT,CAAgB,IAAAjiC,QAAhB,CAA8B,IAAAkO,KAA9B,CAEb,OAAO3tB,EAAA,CAAO,QAAQ,CAAC8H,CAAD,CAAQgR,CAAR,CAAgBhV,CAAhB,CAAsB,CAC1C,MAAOwF,EAAA,CAAOxF,CAAP;AAAe6tC,CAAA,CAAO7pC,CAAP,CAAcgR,CAAd,CAAf,CADmC,CAArC,CAEJ,QACO6Q,QAAQ,CAAC7hB,CAAD,CAAQ3I,CAAR,CAAe2Z,CAAf,CAAuB,CACrC,MAAO0mB,GAAA,CAAOmS,CAAA,CAAO7pC,CAAP,CAAcgR,CAAd,CAAP,CAA8B4oC,CAA9B,CAAqCviD,CAArC,CAA4CsiC,CAAA9T,KAA5C,CAAyD8T,CAAAhiB,QAAzD,CAD8B,CADtC,CAFI,CALqB,CA7Rb,aA2SJsgC,QAAQ,CAACjiD,CAAD,CAAM,CACzB,IAAI2jC,EAAS,IAAb,CAEIkgB,EAAU,IAAA/wB,WAAA,EACd,KAAAgvB,QAAA,CAAa,GAAb,CAEA,OAAO5/C,EAAA,CAAO,QAAQ,CAAC8D,CAAD,CAAOgV,CAAP,CAAe,CAAA,IAC/B8oC,EAAI9jD,CAAA,CAAIgG,CAAJ,CAAUgV,CAAV,CAD2B,CAE/B9Z,EAAI2iD,CAAA,CAAQ79C,CAAR,CAAcgV,CAAd,CAF2B,CAG5BkH,CAEP,IAAI,CAAC4hC,CAAL,CAAQ,MAAOjkD,EAEf,EADAoH,CACA,CADIw6B,EAAA,CAAiBqiB,CAAA,CAAE5iD,CAAF,CAAjB,CAAuByiC,CAAA9T,KAAvB,CACJ,IAAS5oB,CAAA8uB,KAAT,EAAmB4N,CAAAhiB,QAAAmgB,eAAnB,IACE5f,CAKA,CALIjb,CAKJ,CAJM,KAIN,EAJeA,EAIf,GAHEib,CAAA8f,IACA,CADQniC,CACR,CAAAqiB,CAAA6T,KAAA,CAAO,QAAQ,CAACvvB,CAAD,CAAM,CAAE0b,CAAA8f,IAAA,CAAQx7B,CAAV,CAArB,CAEF,EAAAS,CAAA,CAAIA,CAAA+6B,IANN,CAQA,OAAO/6B,EAf4B,CAA9B,CAgBJ,QACO4kB,QAAQ,CAAC7lB,CAAD,CAAO3E,CAAP,CAAc2Z,CAAd,CAAsB,CACpC,IAAIva,EAAMojD,CAAA,CAAQ79C,CAAR,CAAcgV,CAAd,CAGV,OADWymB,GAAAsiB,CAAiB/jD,CAAA,CAAIgG,CAAJ,CAAUgV,CAAV,CAAjB+oC,CAAoCpgB,CAAA9T,KAApCk0B,CACJ,CAAKtjD,CAAL,CAAP,CAAmBY,CAJiB,CADrC,CAhBI,CANkB,CA3SV,cA2UH2gD,QAAQ,CAAC/7C,CAAD,CAAK+9C,CAAL,CAAoB,CACxC,IAAIf,EAAS,EACb,IAA8B,GAA9B,GAAI,IAAAb,UAAA,EAAAvyB,KAAJ,EACE,EACEozB,EAAAliD,KAAA,CAAY,IAAA+xB,WAAA,EAAZ,CADF;MAES,IAAA8uB,OAAA,CAAY,GAAZ,CAFT,CADF,CAKA,IAAAE,QAAA,CAAa,GAAb,CAEA,KAAIne,EAAS,IAEb,OAAO,SAAQ,CAAC35B,CAAD,CAAQgR,CAAR,CAAgB,CAI7B,IAHA,IAAIC,EAAO,EAAX,CACIza,EAAUwjD,CAAA,CAAgBA,CAAA,CAAch6C,CAAd,CAAqBgR,CAArB,CAAhB,CAA+ChR,CAD7D,CAGS9I,EAAI,CAAb,CAAgBA,CAAhB,CAAoB+hD,CAAA/iD,OAApB,CAAmCgB,CAAA,EAAnC,CACE+Z,CAAAla,KAAA,CAAUkiD,CAAA,CAAO/hD,CAAP,CAAA,CAAU8I,CAAV,CAAiBgR,CAAjB,CAAV,CAEEipC,EAAAA,CAAQh+C,CAAA,CAAG+D,CAAH,CAAUgR,CAAV,CAAkBxa,CAAlB,CAARyjD,EAAsCthD,CAE1C8+B,GAAA,CAAiBjhC,CAAjB,CAA0BmjC,CAAA9T,KAA1B,CACA4R,GAAA,CAAiBwiB,CAAjB,CAAwBtgB,CAAA9T,KAAxB,CAGI5oB,EAAAA,CAAIg9C,CAAA59C,MACA,CAAA49C,CAAA59C,MAAA,CAAY7F,CAAZ,CAAqBya,CAArB,CAAA,CACAgpC,CAAA,CAAMhpC,CAAA,CAAK,CAAL,CAAN,CAAeA,CAAA,CAAK,CAAL,CAAf,CAAwBA,CAAA,CAAK,CAAL,CAAxB,CAAiCA,CAAA,CAAK,CAAL,CAAjC,CAA0CA,CAAA,CAAK,CAAL,CAA1C,CAER,OAAOwmB,GAAA,CAAiBx6B,CAAjB,CAAoB08B,CAAA9T,KAApB,CAjBsB,CAXS,CA3UzB,kBA4WCkyB,QAAS,EAAG,CAC5B,IAAImC,EAAa,EAAjB,CACIC,EAAc,CAAA,CAClB,IAA8B,GAA9B,GAAI,IAAA/B,UAAA,EAAAvyB,KAAJ,EACE,EAAG,CACD,GAAI,IAAAmwB,KAAA,CAAU,GAAV,CAAJ,CAEE,KAEF,KAAIoE,EAAY,IAAAtxB,WAAA,EAChBoxB,EAAAnjD,KAAA,CAAgBqjD,CAAhB,CACKA,EAAA1oC,SAAL,GACEyoC,CADF,CACgB,CAAA,CADhB,CAPC,CAAH,MAUS,IAAAvC,OAAA,CAAY,GAAZ,CAVT,CADF,CAaA,IAAAE,QAAA,CAAa,GAAb,CAEA,OAAO5/C,EAAA,CAAO,QAAQ,CAAC8D,CAAD,CAAOgV,CAAP,CAAe,CAEnC,IADA,IAAI7W,EAAQ,EAAZ,CACSjD,EAAI,CAAb,CAAgBA,CAAhB,CAAoBgjD,CAAAhkD,OAApB,CAAuCgB,CAAA,EAAvC,CACEiD,CAAApD,KAAA,CAAWmjD,CAAA,CAAWhjD,CAAX,CAAA,CAAc8E,CAAd;AAAoBgV,CAApB,CAAX,CAEF,OAAO7W,EAL4B,CAA9B,CAMJ,SACQ,CAAA,CADR,UAESggD,CAFT,CANI,CAlBqB,CA5Wb,QA0YTtQ,QAAS,EAAG,CAClB,IAAIwQ,EAAY,EAAhB,CACIF,EAAc,CAAA,CAClB,IAA8B,GAA9B,GAAI,IAAA/B,UAAA,EAAAvyB,KAAJ,EACE,EAAG,CACD,GAAI,IAAAmwB,KAAA,CAAU,GAAV,CAAJ,CAEE,KAHD,KAKGttB,EAAQ,IAAAkvB,OAAA,EALX,CAMDnhD,EAAMiyB,CAAA+f,OAANhyC,EAAsBiyB,CAAA7C,KACtB,KAAAiyB,QAAA,CAAa,GAAb,CACA,KAAIzgD,EAAQ,IAAAyxB,WAAA,EACZuxB,EAAAtjD,KAAA,CAAe,KAAMN,CAAN,OAAkBY,CAAlB,CAAf,CACKA,EAAAqa,SAAL,GACEyoC,CADF,CACgB,CAAA,CADhB,CAVC,CAAH,MAaS,IAAAvC,OAAA,CAAY,GAAZ,CAbT,CADF,CAgBA,IAAAE,QAAA,CAAa,GAAb,CAEA,OAAO5/C,EAAA,CAAO,QAAQ,CAAC8D,CAAD,CAAOgV,CAAP,CAAe,CAEnC,IADA,IAAI64B,EAAS,EAAb,CACS3yC,EAAI,CAAb,CAAgBA,CAAhB,CAAoBmjD,CAAAnkD,OAApB,CAAsCgB,CAAA,EAAtC,CAA2C,CACzC,IAAIgH,EAAWm8C,CAAA,CAAUnjD,CAAV,CACf2yC,EAAA,CAAO3rC,CAAAzH,IAAP,CAAA,CAAuByH,CAAA7G,MAAA,CAAe2E,CAAf,CAAqBgV,CAArB,CAFkB,CAI3C,MAAO64B,EAN4B,CAA9B,CAOJ,SACQ,CAAA,CADR,UAESsQ,CAFT,CAPI,CArBW,CA1YH,CAidnB,KAAIthB,GAAgB,EAApB,CAwnEIgI,GAAa/qC,CAAA,CAAO,MAAP,CAxnEjB,CA0nEImrC,GAAe,MACX,MADW,KAEZ,KAFY,KAGZ,KAHY,cAMH,aANG;GAOb,IAPa,CA1nEnB,CA80GIuD,EAAiB5uC,CAAAiU,cAAA,CAAuB,GAAvB,CA90GrB,CA+0GI66B,GAAYrV,EAAA,CAAW15B,CAAA2D,SAAAkc,KAAX,CAAiC,CAAA,CAAjC,CAsOhBhP,GAAAqI,QAAA,CAA0B,CAAC,UAAD,CAkU1Bg2B,GAAAh2B,QAAA,CAAyB,CAAC,SAAD,CA4DzBs2B,GAAAt2B,QAAA,CAAuB,CAAC,SAAD,CASvB,KAAIw3B,GAAc,GAAlB,CA2HIuD,GAAe,MACXxB,CAAA,CAAW,UAAX,CAAuB,CAAvB,CADW,IAEXA,CAAA,CAAW,UAAX,CAAuB,CAAvB,CAA0B,CAA1B,CAA6B,CAAA,CAA7B,CAFW,GAGXA,CAAA,CAAW,UAAX,CAAuB,CAAvB,CAHW,MAIXE,EAAA,CAAc,OAAd,CAJW,KAKXA,EAAA,CAAc,OAAd,CAAuB,CAAA,CAAvB,CALW,IAMXF,CAAA,CAAW,OAAX,CAAoB,CAApB,CAAuB,CAAvB,CANW,GAOXA,CAAA,CAAW,OAAX,CAAoB,CAApB,CAAuB,CAAvB,CAPW,IAQXA,CAAA,CAAW,MAAX,CAAmB,CAAnB,CARW,GASXA,CAAA,CAAW,MAAX,CAAmB,CAAnB,CATW,IAUXA,CAAA,CAAW,OAAX,CAAoB,CAApB,CAVW,GAWXA,CAAA,CAAW,OAAX,CAAoB,CAApB,CAXW,IAYXA,CAAA,CAAW,OAAX,CAAoB,CAApB,CAAwB,GAAxB,CAZW,GAaXA,CAAA,CAAW,OAAX,CAAoB,CAApB,CAAwB,GAAxB,CAbW,IAcXA,CAAA,CAAW,SAAX,CAAsB,CAAtB,CAdW,GAeXA,CAAA,CAAW,SAAX,CAAsB,CAAtB,CAfW,IAgBXA,CAAA,CAAW,SAAX,CAAsB,CAAtB,CAhBW,GAiBXA,CAAA,CAAW,SAAX,CAAsB,CAAtB,CAjBW,KAoBXA,CAAA,CAAW,cAAX,CAA2B,CAA3B,CApBW,MAqBXE,EAAA,CAAc,KAAd,CArBW,KAsBXA,EAAA,CAAc,KAAd,CAAqB,CAAA,CAArB,CAtBW;EAJnBgS,QAAmB,CAACjS,CAAD,CAAOxC,CAAP,CAAgB,CACjC,MAAyB,GAAlB,CAAAwC,CAAAkS,SAAA,EAAA,CAAuB1U,CAAA2U,MAAA,CAAc,CAAd,CAAvB,CAA0C3U,CAAA2U,MAAA,CAAc,CAAd,CADhB,CAIhB,GAdnBC,QAAuB,CAACpS,CAAD,CAAO,CACxBqS,CAAAA,CAAQ,EAARA,CAAYrS,CAAAsS,kBAAA,EAMhB,OAHAC,EAGA,EAL0B,CAATA,EAACF,CAADE,CAAc,GAAdA,CAAoB,EAKrC,GAHc3S,EAAA,CAAUpkB,IAAA,CAAY,CAAP,CAAA62B,CAAA,CAAW,OAAX,CAAqB,MAA1B,CAAA,CAAkCA,CAAlC,CAAyC,EAAzC,CAAV,CAAwD,CAAxD,CAGd,CAFczS,EAAA,CAAUpkB,IAAAgjB,IAAA,CAAS6T,CAAT,CAAgB,EAAhB,CAAV,CAA+B,CAA/B,CAEd,CAP4B,CAcX,CA3HnB,CAsJI/Q,GAAqB,8EAtJzB,CAuJID,GAAgB,UAmFpB5E,GAAAj2B,QAAA,CAAqB,CAAC,SAAD,CAuHrB,KAAIq2B,GAAkBpsC,EAAA,CAAQoE,CAAR,CAAtB,CAWImoC,GAAkBvsC,EAAA,CAAQsK,EAAR,CA8NtBgiC,GAAAv2B,QAAA,CAAwB,CAAC,QAAD,CAiFxB,KAAIlL,GAAsB7K,EAAA,CAAQ,UACtB,GADsB,SAEvBmH,QAAQ,CAAC7C,CAAD,CAAUvD,CAAV,CAAgB,CAEnB,CAAZ,EAAIyU,CAAJ,GAIOzU,CAAA2b,KAQL,EARmB3b,CAAAsF,KAQnB,EAPEtF,CAAAuqB,KAAA,CAAU,MAAV,CAAkB,EAAlB,CAOF,CAAAhnB,CAAAM,OAAA,CAAe9H,CAAAstB,cAAA,CAAuB,QAAvB,CAAf,CAZF,CAeA,IAAI,CAACrpB,CAAA2b,KAAL,EAAkB,CAAC3b,CAAAghD,UAAnB;AAAqC,CAAChhD,CAAAsF,KAAtC,CACE,MAAO,SAAQ,CAACa,CAAD,CAAQ5C,CAAR,CAAiB,CAE9B,IAAIoY,EAA+C,4BAAxC,GAAApc,EAAAxC,KAAA,CAAcwG,CAAAxD,KAAA,CAAa,MAAb,CAAd,CAAA,CACA,YADA,CACe,MAC1BwD,EAAA4Y,GAAA,CAAW,OAAX,CAAoB,QAAQ,CAACxI,CAAD,CAAO,CAE5BpQ,CAAAvD,KAAA,CAAa2b,CAAb,CAAL,EACEhI,CAAAC,eAAA,EAH+B,CAAnC,CAJ8B,CAlBH,CAFD,CAAR,CAA1B,CAuXI1H,GAA6B,EAIjCzP,EAAA,CAAQ+W,EAAR,CAAsB,QAAQ,CAACytC,CAAD,CAAW95B,CAAX,CAAqB,CAEjD,GAAgB,UAAhB,EAAI85B,CAAJ,CAAA,CAEA,IAAIC,EAAaj9B,EAAA,CAAmB,KAAnB,CAA2BkD,CAA3B,CACjBjb,GAAA,CAA2Bg1C,CAA3B,CAAA,CAAyC,QAAQ,EAAG,CAClD,MAAO,UACK,GADL,MAEC9iC,QAAQ,CAACjY,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CACnCmG,CAAApF,OAAA,CAAaf,CAAA,CAAKkhD,CAAL,CAAb,CAA+BC,QAAiC,CAAC3jD,CAAD,CAAQ,CACtEwC,CAAAuqB,KAAA,CAAUpD,CAAV,CAAoB,CAAC,CAAC3pB,CAAtB,CADsE,CAAxE,CADmC,CAFhC,CAD2C,CAHpD,CAFiD,CAAnD,CAmBAf,EAAA,CAAQ,CAAC,KAAD,CAAQ,QAAR,CAAkB,MAAlB,CAAR,CAAmC,QAAQ,CAAC0qB,CAAD,CAAW,CACpD,IAAI+5B,EAAaj9B,EAAA,CAAmB,KAAnB,CAA2BkD,CAA3B,CACjBjb,GAAA,CAA2Bg1C,CAA3B,CAAA,CAAyC,QAAQ,EAAG,CAClD,MAAO,UACK,EADL,MAEC9iC,QAAQ,CAACjY,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CAAA,IAC/BihD,EAAW95B,CADoB,CAE/B7hB,EAAO6hB,CAEM,OAAjB,GAAIA,CAAJ,EAC4C,4BAD5C;AACI5nB,EAAAxC,KAAA,CAAcwG,CAAAxD,KAAA,CAAa,MAAb,CAAd,CADJ,GAEEuF,CAEA,CAFO,WAEP,CADAtF,CAAA+jB,MAAA,CAAWze,CAAX,CACA,CADmB,YACnB,CAAA27C,CAAA,CAAW,IAJb,CAOAjhD,EAAA0nB,SAAA,CAAcw5B,CAAd,CAA0B,QAAQ,CAAC1jD,CAAD,CAAQ,CACnCA,CAAL,GAGAwC,CAAAuqB,KAAA,CAAUjlB,CAAV,CAAgB9H,CAAhB,CAMA,CAAIiX,CAAJ,EAAYwsC,CAAZ,EAAsB19C,CAAAxD,KAAA,CAAakhD,CAAb,CAAuBjhD,CAAA,CAAKsF,CAAL,CAAvB,CATtB,CADwC,CAA1C,CAXmC,CAFhC,CAD2C,CAFA,CAAtD,CAkCA,KAAIisC,GAAe,aACJzyC,CADI,gBAEDA,CAFC,cAGHA,CAHG,WAINA,CAJM,cAKHA,CALG,CA6CnBiyC,GAAA/7B,QAAA,CAAyB,CAAC,UAAD,CAAa,QAAb,CAAuB,QAAvB,CAAiC,UAAjC,CA+TzB,KAAIosC,GAAuBA,QAAQ,CAACC,CAAD,CAAW,CAC5C,MAAO,CAAC,UAAD,CAAa,QAAQ,CAAC9nC,CAAD,CAAW,CAoDrC,MAnDoBvP,MACZ,MADYA,UAERq3C,CAAA,CAAW,KAAX,CAAmB,GAFXr3C,YAGN+mC,EAHM/mC,SAIT5D,QAAQ,EAAG,CAClB,MAAO,KACA0f,QAAQ,CAAC3f,CAAD,CAAQm7C,CAAR,CAAqBthD,CAArB,CAA2BogB,CAA3B,CAAuC,CAClD,GAAI,CAACpgB,CAAAuhD,OAAL,CAAkB,CAOhB,IAAIC,EAAyBA,QAAQ,CAAC7tC,CAAD,CAAQ,CAC3CA,CAAAC,eACA,CAAID,CAAAC,eAAA,EAAJ,CACID,CAAAG,YADJ;AACwB,CAAA,CAHmB,CAM7CghB,GAAA,CAAmBwsB,CAAA,CAAY,CAAZ,CAAnB,CAAmC,QAAnC,CAA6CE,CAA7C,CAIAF,EAAAnlC,GAAA,CAAe,UAAf,CAA2B,QAAQ,EAAG,CACpC5C,CAAA,CAAS,QAAQ,EAAG,CAClB5H,EAAA,CAAsB2vC,CAAA,CAAY,CAAZ,CAAtB,CAAsC,QAAtC,CAAgDE,CAAhD,CADkB,CAApB,CAEG,CAFH,CAEM,CAAA,CAFN,CADoC,CAAtC,CAjBgB,CADgC,IAyB9CC,EAAiBH,CAAA1iD,OAAA,EAAAwhB,WAAA,CAAgC,MAAhC,CAzB6B,CA0B9CshC,EAAQ1hD,CAAAsF,KAARo8C,EAAqB1hD,CAAA6xC,OAErB6P,EAAJ,EACE7jB,EAAA,CAAO13B,CAAP,CAAcu7C,CAAd,CAAqBthC,CAArB,CAAiCshC,CAAjC,CAEF,IAAID,CAAJ,CACEH,CAAAnlC,GAAA,CAAe,UAAf,CAA2B,QAAQ,EAAG,CACpCslC,CAAAnP,eAAA,CAA8BlyB,CAA9B,CACIshC,EAAJ,EACE7jB,EAAA,CAAO13B,CAAP,CAAcu7C,CAAd,CAAqB1lD,CAArB,CAAgC0lD,CAAhC,CAEFrjD,EAAA,CAAO+hB,CAAP,CAAmBmxB,EAAnB,CALoC,CAAtC,CAhCgD,CAD/C,CADW,CAJFvnC,CADiB,CAAhC,CADqC,CAA9C,CAyDIA,GAAgBo3C,EAAA,EAzDpB,CA0DIv2C,GAAkBu2C,EAAA,CAAqB,CAAA,CAArB,CA1DtB,CAoEIO,GAAa,qFApEjB,CAqEIC,GAAe,4DArEnB,CAsEIC,GAAgB,oCAtEpB,CAwEIC,GAAY,MA6ENlO,EA7EM,QA+kBhBmO,QAAwB,CAAC57C,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuBkzC,CAAvB,CAA6Bv5B,CAA7B,CAAuC2W,CAAvC,CAAiD,CACvEsjB,EAAA,CAAcztC,CAAd;AAAqB5C,CAArB,CAA8BvD,CAA9B,CAAoCkzC,CAApC,CAA0Cv5B,CAA1C,CAAoD2W,CAApD,CAEA4iB,EAAAI,SAAAp2C,KAAA,CAAmB,QAAQ,CAACM,CAAD,CAAQ,CACjC,IAAIkG,EAAQwvC,CAAAsB,SAAA,CAAch3C,CAAd,CACZ,IAAIkG,CAAJ,EAAam+C,EAAAp7C,KAAA,CAAmBjJ,CAAnB,CAAb,CAEE,MADA01C,EAAAR,aAAA,CAAkB,QAAlB,CAA4B,CAAA,CAA5B,CACO,CAAU,EAAV,GAAAl1C,CAAA,CAAe,IAAf,CAAuBkG,CAAA,CAAQlG,CAAR,CAAgBkyC,UAAA,CAAWlyC,CAAX,CAE9C01C,EAAAR,aAAA,CAAkB,QAAlB,CAA4B,CAAA,CAA5B,CACA,OAAO12C,EAPwB,CAAnC,CAWAq3C,GAAA,CAAyBH,CAAzB,CAA+B,QAA/B,CAAyC3vC,CAAzC,CAEA2vC,EAAA0B,YAAA13C,KAAA,CAAsB,QAAQ,CAACM,CAAD,CAAQ,CACpC,MAAO01C,EAAAsB,SAAA,CAAch3C,CAAd,CAAA,CAAuB,EAAvB,CAA4B,EAA5B,CAAiCA,CADJ,CAAtC,CAIIwC,EAAAstC,IAAJ,GACM0U,CAMJ,CANmBA,QAAQ,CAACxkD,CAAD,CAAQ,CACjC,IAAI8vC,EAAMoC,UAAA,CAAW1vC,CAAAstC,IAAX,CACV,OAAO2F,GAAA,CAASC,CAAT,CAAe,KAAf,CAAsBA,CAAAsB,SAAA,CAAch3C,CAAd,CAAtB,EAA8CA,CAA9C,EAAuD8vC,CAAvD,CAA4D9vC,CAA5D,CAF0B,CAMnC,CADA01C,CAAAI,SAAAp2C,KAAA,CAAmB8kD,CAAnB,CACA,CAAA9O,CAAA0B,YAAA13C,KAAA,CAAsB8kD,CAAtB,CAPF,CAUIhiD,EAAAiqB,IAAJ,GACMg4B,CAMJ,CANmBA,QAAQ,CAACzkD,CAAD,CAAQ,CACjC,IAAIysB,EAAMylB,UAAA,CAAW1vC,CAAAiqB,IAAX,CACV,OAAOgpB,GAAA,CAASC,CAAT,CAAe,KAAf,CAAsBA,CAAAsB,SAAA,CAAch3C,CAAd,CAAtB,EAA8CA,CAA9C,EAAuDysB,CAAvD,CAA4DzsB,CAA5D,CAF0B,CAMnC,CADA01C,CAAAI,SAAAp2C,KAAA,CAAmB+kD,CAAnB,CACA,CAAA/O,CAAA0B,YAAA13C,KAAA,CAAsB+kD,CAAtB,CAPF,CAUA/O;CAAA0B,YAAA13C,KAAA,CAAsB,QAAQ,CAACM,CAAD,CAAQ,CACpC,MAAOy1C,GAAA,CAASC,CAAT,CAAe,QAAf,CAAyBA,CAAAsB,SAAA,CAAch3C,CAAd,CAAzB,EAAiD6B,EAAA,CAAS7B,CAAT,CAAjD,CAAkEA,CAAlE,CAD6B,CAAtC,CAxCuE,CA/kBzD,KA4nBhB0kD,QAAqB,CAAC/7C,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuBkzC,CAAvB,CAA6Bv5B,CAA7B,CAAuC2W,CAAvC,CAAiD,CACpEsjB,EAAA,CAAcztC,CAAd,CAAqB5C,CAArB,CAA8BvD,CAA9B,CAAoCkzC,CAApC,CAA0Cv5B,CAA1C,CAAoD2W,CAApD,CAEI6xB,EAAAA,CAAeA,QAAQ,CAAC3kD,CAAD,CAAQ,CACjC,MAAOy1C,GAAA,CAASC,CAAT,CAAe,KAAf,CAAsBA,CAAAsB,SAAA,CAAch3C,CAAd,CAAtB,EAA8CmkD,EAAAl7C,KAAA,CAAgBjJ,CAAhB,CAA9C,CAAsEA,CAAtE,CAD0B,CAInC01C,EAAA0B,YAAA13C,KAAA,CAAsBilD,CAAtB,CACAjP,EAAAI,SAAAp2C,KAAA,CAAmBilD,CAAnB,CARoE,CA5nBtD,OAuoBhBC,QAAuB,CAACj8C,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuBkzC,CAAvB,CAA6Bv5B,CAA7B,CAAuC2W,CAAvC,CAAiD,CACtEsjB,EAAA,CAAcztC,CAAd,CAAqB5C,CAArB,CAA8BvD,CAA9B,CAAoCkzC,CAApC,CAA0Cv5B,CAA1C,CAAoD2W,CAApD,CAEI+xB,EAAAA,CAAiBA,QAAQ,CAAC7kD,CAAD,CAAQ,CACnC,MAAOy1C,GAAA,CAASC,CAAT,CAAe,OAAf,CAAwBA,CAAAsB,SAAA,CAAch3C,CAAd,CAAxB,EAAgDokD,EAAAn7C,KAAA,CAAkBjJ,CAAlB,CAAhD,CAA0EA,CAA1E,CAD4B,CAIrC01C,EAAA0B,YAAA13C,KAAA,CAAsBmlD,CAAtB,CACAnP,EAAAI,SAAAp2C,KAAA,CAAmBmlD,CAAnB,CARsE,CAvoBxD,OAkpBhBC,QAAuB,CAACn8C,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuBkzC,CAAvB,CAA6B,CAE9Ch0C,CAAA,CAAYc,CAAAsF,KAAZ,CAAJ,EACE/B,CAAAvD,KAAA,CAAa,MAAb,CAAqBvC,EAAA,EAArB,CAGF8F,EAAA4Y,GAAA,CAAW,OAAX,CAAoB,QAAQ,EAAG,CACzB5Y,CAAA,CAAQ,CAAR,CAAAg/C,QAAJ,EACEp8C,CAAAG,OAAA,CAAa,QAAQ,EAAG,CACtB4sC,CAAAiB,cAAA,CAAmBn0C,CAAAxC,MAAnB,CADsB,CAAxB,CAF2B,CAA/B,CAQA01C;CAAAoB,QAAA,CAAeC,QAAQ,EAAG,CAExBhxC,CAAA,CAAQ,CAAR,CAAAg/C,QAAA,CADYviD,CAAAxC,MACZ,EAA+B01C,CAAAgB,WAFP,CAK1Bl0C,EAAA0nB,SAAA,CAAc,OAAd,CAAuBwrB,CAAAoB,QAAvB,CAnBkD,CAlpBpC,UAwqBhBkO,QAA0B,CAACr8C,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuBkzC,CAAvB,CAA6B,CAAA,IACjDuP,EAAYziD,CAAA0iD,YADqC,CAEjDC,EAAa3iD,CAAA4iD,aAEZrmD,EAAA,CAASkmD,CAAT,CAAL,GAA0BA,CAA1B,CAAsC,CAAA,CAAtC,CACKlmD,EAAA,CAASomD,CAAT,CAAL,GAA2BA,CAA3B,CAAwC,CAAA,CAAxC,CAEAp/C,EAAA4Y,GAAA,CAAW,OAAX,CAAoB,QAAQ,EAAG,CAC7BhW,CAAAG,OAAA,CAAa,QAAQ,EAAG,CACtB4sC,CAAAiB,cAAA,CAAmB5wC,CAAA,CAAQ,CAAR,CAAAg/C,QAAnB,CADsB,CAAxB,CAD6B,CAA/B,CAMArP,EAAAoB,QAAA,CAAeC,QAAQ,EAAG,CACxBhxC,CAAA,CAAQ,CAAR,CAAAg/C,QAAA,CAAqBrP,CAAAgB,WADG,CAK1BhB,EAAAsB,SAAA,CAAgBqO,QAAQ,CAACrlD,CAAD,CAAQ,CAC9B,MAAOA,EAAP,GAAiBilD,CADa,CAIhCvP,EAAA0B,YAAA13C,KAAA,CAAsB,QAAQ,CAACM,CAAD,CAAQ,CACpC,MAAOA,EAAP,GAAiBilD,CADmB,CAAtC,CAIAvP,EAAAI,SAAAp2C,KAAA,CAAmB,QAAQ,CAACM,CAAD,CAAQ,CACjC,MAAOA,EAAA,CAAQilD,CAAR,CAAoBE,CADM,CAAnC,CA1BqD,CAxqBvC,QAyZJ7jD,CAzZI,QA0ZJA,CA1ZI,QA2ZJA,CA3ZI,OA4ZLA,CA5ZK,MA6ZNA,CA7ZM,CAxEhB,CA05BIiL,GAAiB,CAAC,UAAD,CAAa,UAAb,CAAyB,QAAQ,CAACumB,CAAD;AAAW3W,CAAX,CAAqB,CACzE,MAAO,UACK,GADL,SAEI,UAFJ,MAGCyE,QAAQ,CAACjY,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuBkzC,CAAvB,CAA6B,CACrCA,CAAJ,EACG,CAAA4O,EAAA,CAAUz+C,CAAA,CAAUrD,CAAAqR,KAAV,CAAV,CAAA,EAAmCywC,EAAA91B,KAAnC,EAAmD7lB,CAAnD,CAA0D5C,CAA1D,CAAmEvD,CAAnE,CAAyEkzC,CAAzE,CAA+Ev5B,CAA/E,CACmD2W,CADnD,CAFsC,CAHtC,CADkE,CAAtD,CA15BrB,CAu6BI8gB,GAAc,UAv6BlB,CAw6BID,GAAgB,YAx6BpB,CAy6BIgB,GAAiB,aAz6BrB,CA06BIW,GAAc,UA16BlB,CAujCIgQ,GAAoB,CAAC,QAAD,CAAW,mBAAX,CAAgC,QAAhC,CAA0C,UAA1C,CAAsD,QAAtD,CAAgE,UAAhE,CACpB,QAAQ,CAACx6B,CAAD,CAAStI,CAAT,CAA4B+D,CAA5B,CAAmC7B,CAAnC,CAA6CpB,CAA7C,CAAqDG,CAArD,CAA+D,CA6DzE+vB,QAASA,EAAc,CAACC,CAAD,CAAUC,CAAV,CAA8B,CACnDA,CAAA,CAAqBA,CAAA,CAAqB,GAArB,CAA2BpqC,EAAA,CAAWoqC,CAAX,CAA+B,GAA/B,CAA3B,CAAiE,EACtFjwB,EAAA0M,YAAA,CAAqBzL,CAArB,EAAgC+uB,CAAA,CAAUE,EAAV,CAA0BC,EAA1D,EAAyEF,CAAzE,CACAjwB,EAAAkB,SAAA,CAAkBD,CAAlB,EAA6B+uB,CAAA,CAAUG,EAAV,CAAwBD,EAArD,EAAsED,CAAtE,CAHmD,CA3DrD,IAAA6R,YAAA,CADA,IAAA7O,WACA,CADkBt1B,MAAAokC,IAElB,KAAA1P,SAAA,CAAgB,EAChB,KAAAsB,YAAA,CAAmB,EACnB,KAAAqO,qBAAA,CAA4B,EAC5B,KAAAlR,UAAA,CAAiB,CAAA,CACjB,KAAAD,OAAA,CAAc,CAAA,CACd;IAAAE,OAAA,CAAc,CAAA,CACd,KAAAC,SAAA,CAAgB,CAAA,CAChB,KAAAL,MAAA,CAAa7tB,CAAAze,KAV4D,KAYrE49C,EAAapiC,CAAA,CAAOiD,CAAAo/B,QAAP,CAZwD,CAarEC,EAAaF,CAAAl7B,OAEjB,IAAI,CAACo7B,CAAL,CACE,KAAMnnD,EAAA,CAAO,SAAP,CAAA,CAAkB,WAAlB,CACF8nB,CAAAo/B,QADE,CACa7/C,EAAA,CAAY4e,CAAZ,CADb,CAAN,CAYF,IAAAoyB,QAAA,CAAex1C,CAmBf,KAAA01C,SAAA,CAAgB6O,QAAQ,CAAC7lD,CAAD,CAAQ,CAC9B,MAAO0B,EAAA,CAAY1B,CAAZ,CAAP,EAAuC,EAAvC,GAA6BA,CAA7B,EAAuD,IAAvD,GAA6CA,CAA7C,EAA+DA,CAA/D,GAAyEA,CAD3C,CA/CyC,KAmDrE8zC,EAAapvB,CAAAohC,cAAA,CAAuB,iBAAvB,CAAbhS,EAA0DC,EAnDW,CAoDrEC,EAAe,CApDsD,CAqDrEE,EAAS,IAAAA,OAATA,CAAuB,EAI3BxvB,EAAAC,SAAA,CAAkBgwB,EAAlB,CACAnB,EAAA,CAAe,CAAA,CAAf,CA0BA,KAAA0B,aAAA,CAAoB6Q,QAAQ,CAACrS,CAAD,CAAqBD,CAArB,CAA8B,CAGpDS,CAAA,CAAOR,CAAP,CAAJ,GAAmC,CAACD,CAApC,GAGIA,CAAJ,EACMS,CAAA,CAAOR,CAAP,CACJ,EADgCM,CAAA,EAChC,CAAKA,CAAL,GACER,CAAA,CAAe,CAAA,CAAf,CAEA,CADA,IAAAgB,OACA,CADc,CAAA,CACd,CAAA,IAAAC,SAAA,CAAgB,CAAA,CAHlB,CAFF,GAQEjB,CAAA,CAAe,CAAA,CAAf,CAGA,CAFA,IAAAiB,SAEA,CAFgB,CAAA,CAEhB,CADA,IAAAD,OACA,CADc,CAAA,CACd,CAAAR,CAAA,EAXF,CAiBA,CAHAE,CAAA,CAAOR,CAAP,CAGA,CAH6B,CAACD,CAG9B,CAFAD,CAAA,CAAeC,CAAf,CAAwBC,CAAxB,CAEA,CAAAI,CAAAoB,aAAA,CAAwBxB,CAAxB,CAA4CD,CAA5C,CAAqD,IAArD,CApBA,CAHwD,CAoC1D,KAAA8B,aAAA;AAAoByQ,QAAS,EAAG,CAC9B,IAAA1R,OAAA,CAAc,CAAA,CACd,KAAAC,UAAA,CAAiB,CAAA,CACjB9wB,EAAA0M,YAAA,CAAqBzL,CAArB,CAA+B4wB,EAA/B,CACA7xB,EAAAkB,SAAA,CAAkBD,CAAlB,CAA4BiwB,EAA5B,CAJ8B,CA4BhC,KAAAgC,cAAA,CAAqBsP,QAAQ,CAACjmD,CAAD,CAAQ,CACnC,IAAA02C,WAAA,CAAkB12C,CAGd,KAAAu0C,UAAJ,GACE,IAAAD,OAIA,CAJc,CAAA,CAId,CAHA,IAAAC,UAGA,CAHiB,CAAA,CAGjB,CAFA9wB,CAAA0M,YAAA,CAAqBzL,CAArB,CAA+BiwB,EAA/B,CAEA,CADAlxB,CAAAkB,SAAA,CAAkBD,CAAlB,CAA4B4wB,EAA5B,CACA,CAAAxB,CAAAsB,UAAA,EALF,CAQAn2C,EAAA,CAAQ,IAAA62C,SAAR,CAAuB,QAAQ,CAAClxC,CAAD,CAAK,CAClC5E,CAAA,CAAQ4E,CAAA,CAAG5E,CAAH,CAD0B,CAApC,CAII,KAAAulD,YAAJ,GAAyBvlD,CAAzB,GACE,IAAAulD,YAEA,CAFmBvlD,CAEnB,CADA4lD,CAAA,CAAW96B,CAAX,CAAmB9qB,CAAnB,CACA,CAAAf,CAAA,CAAQ,IAAAwmD,qBAAR,CAAmC,QAAQ,CAACpoC,CAAD,CAAW,CACpD,GAAI,CACFA,CAAA,EADE,CAEF,MAAMlX,CAAN,CAAS,CACTqc,CAAA,CAAkBrc,CAAlB,CADS,CAHyC,CAAtD,CAHF,CAhBmC,CA8BrC,KAAIuvC,EAAO,IAEX5qB,EAAAvnB,OAAA,CAAc2iD,QAAqB,EAAG,CACpC,IAAIlmD,EAAQ0lD,CAAA,CAAW56B,CAAX,CAGZ,IAAI4qB,CAAA6P,YAAJ,GAAyBvlD,CAAzB,CAAgC,CAAA,IAE1BmmD,EAAazQ,CAAA0B,YAFa,CAG1BthB,EAAMqwB,CAAAtnD,OAGV,KADA62C,CAAA6P,YACA,CADmBvlD,CACnB,CAAM81B,CAAA,EAAN,CAAA,CACE91B,CAAA;AAAQmmD,CAAA,CAAWrwB,CAAX,CAAA,CAAgB91B,CAAhB,CAGN01C,EAAAgB,WAAJ,GAAwB12C,CAAxB,GACE01C,CAAAgB,WACA,CADkB12C,CAClB,CAAA01C,CAAAoB,QAAA,EAFF,CAV8B,CAgBhC,MAAO92C,EApB6B,CAAtC,CApLyE,CADnD,CAvjCxB,CA22CIoO,GAAmBA,QAAQ,EAAG,CAChC,MAAO,SACI,CAAC,SAAD,CAAY,QAAZ,CADJ,YAEOk3C,EAFP,MAGC1kC,QAAQ,CAACjY,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB4jD,CAAvB,CAA8B,CAAA,IAGtCC,EAAYD,CAAA,CAAM,CAAN,CAH0B,CAItCE,EAAWF,CAAA,CAAM,CAAN,CAAXE,EAAuBvS,EAE3BuS,EAAA5R,YAAA,CAAqB2R,CAArB,CAEA19C,EAAA6/B,IAAA,CAAU,UAAV,CAAsB,QAAQ,EAAG,CAC/B8d,CAAAxR,eAAA,CAAwBuR,CAAxB,CAD+B,CAAjC,CAR0C,CAHvC,CADyB,CA32ClC,CAy7CI/3C,GAAoB7M,EAAA,CAAQ,SACrB,SADqB,MAExBmf,QAAQ,CAACjY,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuBkzC,CAAvB,CAA6B,CACzCA,CAAA+P,qBAAA/lD,KAAA,CAA+B,QAAQ,EAAG,CACxCiJ,CAAA0/B,MAAA,CAAY7lC,CAAA+jD,SAAZ,CADwC,CAA1C,CADyC,CAFb,CAAR,CAz7CxB,CAm8CIh4C,GAAoBA,QAAQ,EAAG,CACjC,MAAO,SACI,UADJ,MAECqS,QAAQ,CAACjY,CAAD,CAAQ6S,CAAR,CAAahZ,CAAb,CAAmBkzC,CAAnB,CAAyB,CACrC,GAAKA,CAAL,CAAA,CACAlzC,CAAAgkD,SAAA,CAAgB,CAAA,CAEhB,KAAIzQ,EAAYA,QAAQ,CAAC/1C,CAAD,CAAQ,CAC9B,GAAIwC,CAAAgkD,SAAJ,EAAqB9Q,CAAAsB,SAAA,CAAch3C,CAAd,CAArB,CACE01C,CAAAR,aAAA,CAAkB,UAAlB;AAA8B,CAAA,CAA9B,CADF,KAKE,OADAQ,EAAAR,aAAA,CAAkB,UAAlB,CAA8B,CAAA,CAA9B,CACOl1C,CAAAA,CANqB,CAUhC01C,EAAA0B,YAAA13C,KAAA,CAAsBq2C,CAAtB,CACAL,EAAAI,SAAAr1C,QAAA,CAAsBs1C,CAAtB,CAEAvzC,EAAA0nB,SAAA,CAAc,UAAd,CAA0B,QAAQ,EAAG,CACnC6rB,CAAA,CAAUL,CAAAgB,WAAV,CADmC,CAArC,CAhBA,CADqC,CAFlC,CAD0B,CAn8CnC,CAqhDIroC,GAAkBA,QAAQ,EAAG,CAC/B,MAAO,SACI,SADJ,MAECuS,QAAQ,CAACjY,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuBkzC,CAAvB,CAA6B,CACzC,IACInsC,GADA/C,CACA+C,CADQ,UAAAtB,KAAA,CAAgBzF,CAAAikD,OAAhB,CACRl9C,GAAyB3F,MAAJ,CAAW4C,CAAA,CAAM,CAAN,CAAX,CAArB+C,EAA6C/G,CAAAikD,OAA7Cl9C,EAA4D,GAiBhEmsC,EAAAI,SAAAp2C,KAAA,CAfYgG,QAAQ,CAACghD,CAAD,CAAY,CAE9B,GAAI,CAAAhlD,CAAA,CAAYglD,CAAZ,CAAJ,CAAA,CAEA,IAAI9jD,EAAO,EAEP8jD,EAAJ,EACEznD,CAAA,CAAQynD,CAAA3/C,MAAA,CAAgBwC,CAAhB,CAAR,CAAoC,QAAQ,CAACvJ,CAAD,CAAQ,CAC9CA,CAAJ,EAAW4C,CAAAlD,KAAA,CAAUqS,EAAA,CAAK/R,CAAL,CAAV,CADuC,CAApD,CAKF,OAAO4C,EAVP,CAF8B,CAehC,CACA8yC,EAAA0B,YAAA13C,KAAA,CAAsB,QAAQ,CAACM,CAAD,CAAQ,CACpC,MAAIhB,EAAA,CAAQgB,CAAR,CAAJ,CACSA,CAAAM,KAAA,CAAW,IAAX,CADT,CAIO9B,CAL6B,CAAtC,CASAk3C,EAAAsB,SAAA,CAAgBqO,QAAQ,CAACrlD,CAAD,CAAQ,CAC9B,MAAO,CAACA,CAAR,EAAiB,CAACA,CAAAnB,OADY,CA7BS,CAFtC,CADwB,CArhDjC,CA6jDI8nD,GAAwB,oBA7jD5B;AAinDIn4C,GAAmBA,QAAQ,EAAG,CAChC,MAAO,UACK,GADL,SAEI5F,QAAQ,CAACg+C,CAAD,CAAMC,CAAN,CAAe,CAC9B,MAAIF,GAAA19C,KAAA,CAA2B49C,CAAAC,QAA3B,CAAJ,CACSC,QAA4B,CAACp+C,CAAD,CAAQ6S,CAAR,CAAahZ,CAAb,CAAmB,CACpDA,CAAAuqB,KAAA,CAAU,OAAV,CAAmBpkB,CAAA0/B,MAAA,CAAY7lC,CAAAskD,QAAZ,CAAnB,CADoD,CADxD,CAKSE,QAAoB,CAACr+C,CAAD,CAAQ6S,CAAR,CAAahZ,CAAb,CAAmB,CAC5CmG,CAAApF,OAAA,CAAaf,CAAAskD,QAAb,CAA2BG,QAAyB,CAACjnD,CAAD,CAAQ,CAC1DwC,CAAAuqB,KAAA,CAAU,OAAV,CAAmB/sB,CAAnB,CAD0D,CAA5D,CAD4C,CANlB,CAF3B,CADyB,CAjnDlC,CAsrDI6M,GAAkBymC,EAAA,CAAY,QAAQ,CAAC3qC,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CAC/DuD,CAAA4e,SAAA,CAAiB,YAAjB,CAAA5b,KAAA,CAAoC,UAApC,CAAgDvG,CAAA0kD,OAAhD,CACAv+C,EAAApF,OAAA,CAAaf,CAAA0kD,OAAb,CAA0BC,QAA0B,CAACnnD,CAAD,CAAQ,CAI1D+F,CAAAyoB,KAAA,CAAaxuB,CAAA,EAASxB,CAAT,CAAqB,EAArB,CAA0BwB,CAAvC,CAJ0D,CAA5D,CAF+D,CAA3C,CAtrDtB,CAmvDI+M,GAA0B,CAAC,cAAD,CAAiB,QAAQ,CAACoW,CAAD,CAAe,CACpE,MAAO,SAAQ,CAACxa,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CAEhCisB,CAAAA,CAAgBtL,CAAA,CAAapd,CAAAvD,KAAA,CAAaA,CAAA+jB,MAAA6gC,eAAb,CAAb,CACpBrhD,EAAA4e,SAAA,CAAiB,YAAjB,CAAA5b,KAAA,CAAoC,UAApC,CAAgD0lB,CAAhD,CACAjsB,EAAA0nB,SAAA,CAAc,gBAAd,CAAgC,QAAQ,CAAClqB,CAAD,CAAQ,CAC9C+F,CAAAyoB,KAAA,CAAaxuB,CAAb,CAD8C,CAAhD,CAJoC,CAD8B,CAAxC,CAnvD9B;AA6yDI8M,GAAsB,CAAC,MAAD,CAAS,QAAT,CAAmB,QAAQ,CAAC0W,CAAD,CAAOF,CAAP,CAAe,CAClE,MAAO,SAAQ,CAAC3a,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CACpCuD,CAAA4e,SAAA,CAAiB,YAAjB,CAAA5b,KAAA,CAAoC,UAApC,CAAgDvG,CAAA6kD,WAAhD,CAEA,KAAIp1C,EAASqR,CAAA,CAAO9gB,CAAA6kD,WAAP,CAGb1+C,EAAApF,OAAA,CAFA+jD,QAAuB,EAAG,CAAE,MAAQvlD,CAAAkQ,CAAA,CAAOtJ,CAAP,CAAA5G,EAAiB,EAAjBA,UAAA,EAAV,CAE1B,CAA6BwlD,QAA8B,CAACvnD,CAAD,CAAQ,CACjE+F,CAAAO,KAAA,CAAakd,CAAAgkC,eAAA,CAAoBv1C,CAAA,CAAOtJ,CAAP,CAApB,CAAb,EAAmD,EAAnD,CADiE,CAAnE,CANoC,CAD4B,CAA1C,CA7yD1B,CA8jEIqE,GAAmB2qC,EAAA,CAAe,EAAf,CAAmB,CAAA,CAAnB,CA9jEvB,CA8mEIzqC,GAAsByqC,EAAA,CAAe,KAAf,CAAsB,CAAtB,CA9mE1B,CA8pEI1qC,GAAuB0qC,EAAA,CAAe,MAAf,CAAuB,CAAvB,CA9pE3B,CAwtEIxqC,GAAmBmmC,EAAA,CAAY,SACxB1qC,QAAQ,CAAC7C,CAAD,CAAUvD,CAAV,CAAgB,CAC/BA,CAAAuqB,KAAA,CAAU,SAAV,CAAqBvuB,CAArB,CACAuH,EAAAoqB,YAAA,CAAoB,UAApB,CAF+B,CADA,CAAZ,CAxtEvB,CAo7EI/iB,GAAwB,CAAC,QAAQ,EAAG,CACtC,MAAO,OACE,CAAA,CADF,YAEO,GAFP,UAGK,GAHL,CAD+B,CAAZ,CAp7E5B,CA0gFIuB,GAAoB,EACxB1P,EAAA,CACE,6IAAA,MAAA,CAAA,GAAA,CADF;AAEE,QAAQ,CAAC6I,CAAD,CAAO,CACb,IAAI0gB,EAAgB/B,EAAA,CAAmB,KAAnB,CAA2B3e,CAA3B,CACpB6G,GAAA,CAAkB6Z,CAAlB,CAAA,CAAmC,CAAC,QAAD,CAAW,QAAQ,CAAClF,CAAD,CAAS,CAC7D,MAAO,SACI1a,QAAQ,CAAC8b,CAAD,CAAWliB,CAAX,CAAiB,CAChC,IAAIoC,EAAK0e,CAAA,CAAO9gB,CAAA,CAAKgmB,CAAL,CAAP,CACT,OAAO,SAAQ,CAAC7f,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CACpCuD,CAAA4Y,GAAA,CAAW9Y,CAAA,CAAUiC,CAAV,CAAX,CAA4B,QAAQ,CAACqO,CAAD,CAAQ,CAC1CxN,CAAAG,OAAA,CAAa,QAAQ,EAAG,CACtBlE,CAAA,CAAG+D,CAAH,CAAU,QAAQwN,CAAR,CAAV,CADsB,CAAxB,CAD0C,CAA5C,CADoC,CAFN,CAD7B,CADsD,CAA5B,CAFtB,CAFjB,CAqeA,KAAI5I,GAAgB,CAAC,UAAD,CAAa,QAAQ,CAACkW,CAAD,CAAW,CAClD,MAAO,YACO,SADP,UAEK,GAFL,UAGK,CAAA,CAHL,UAIK,GAJL,OAKE,CAAA,CALF,MAMC7C,QAAS,CAACkK,CAAD,CAASpG,CAAT,CAAmB6B,CAAnB,CAA0BmvB,CAA1B,CAAgC+R,CAAhC,CAA6C,CAAA,IACpD77C,CADoD,CAC7CqZ,CAD6C,CACjCyiC,CACvB58B,EAAAvnB,OAAA,CAAcgjB,CAAAohC,KAAd,CAA0BC,QAAwB,CAAC5nD,CAAD,CAAQ,CAEpD2F,EAAA,CAAU3F,CAAV,CAAJ,CACOilB,CADP,GAEIA,CACA,CADa6F,CAAAvF,KAAA,EACb,CAAAkiC,CAAA,CAAYxiC,CAAZ,CAAwB,QAAS,CAAChf,CAAD,CAAQ,CACvCA,CAAA,CAAMA,CAAApH,OAAA,EAAN,CAAA,CAAwBN,CAAAstB,cAAA,CAAuB,aAAvB,CAAuCtF,CAAAohC,KAAvC,CAAoD,GAApD,CAIxB/7C,EAAA,CAAQ,OACC3F,CADD,CAGRwd,EAAA84B,MAAA,CAAet2C,CAAf,CAAsBye,CAAAtjB,OAAA,EAAtB,CAAyCsjB,CAAzC,CARuC,CAAzC,CAHJ,GAeKgjC,CAQH,GAPEA,CAAAlmC,OAAA,EACA;AAAAkmC,CAAA,CAAmB,IAMrB,EAJGziC,CAIH,GAHEA,CAAAzQ,SAAA,EACA,CAAAyQ,CAAA,CAAa,IAEf,EAAGrZ,CAAH,GACE87C,CAIA,CAJmBl9C,EAAA,CAAiBoB,CAAA3F,MAAjB,CAInB,CAHAwd,CAAA+4B,MAAA,CAAekL,CAAf,CAAiC,QAAQ,EAAG,CAC1CA,CAAA,CAAmB,IADuB,CAA5C,CAGA,CAAA97C,CAAA,CAAQ,IALV,CAvBF,CAFwD,CAA1D,CAFwD,CANvD,CAD2C,CAAhC,CAApB,CA8MI4B,GAAqB,CAAC,OAAD,CAAU,gBAAV,CAA4B,eAA5B,CAA6C,UAA7C,CAAyD,MAAzD,CACP,QAAQ,CAAC4V,CAAD,CAAUC,CAAV,CAA4BwkC,CAA5B,CAA6CpkC,CAA7C,CAAyDD,CAAzD,CAA+D,CACvF,MAAO,UACK,KADL,UAEK,GAFL,UAGK,CAAA,CAHL,YAIO,SAJP,YAKOta,EAAA5H,KALP,SAMIsH,QAAQ,CAAC7C,CAAD,CAAUvD,CAAV,CAAgB,CAAA,IAC3BslD,EAAStlD,CAAAulD,UAATD,EAA2BtlD,CAAAsB,IADA,CAE3BkkD,EAAYxlD,CAAAylD,OAAZD,EAA2B,EAFA,CAG3BE,EAAgB1lD,CAAA2lD,WAEpB,OAAO,SAAQ,CAACx/C,CAAD,CAAQ+b,CAAR,CAAkB6B,CAAlB,CAAyBmvB,CAAzB,CAA+B+R,CAA/B,CAA4C,CAAA,IACrD1oB,EAAgB,CADqC,CAErDiK,CAFqD,CAGrDof,CAHqD,CAIrDC,CAJqD,CAMrDC,EAA4BA,QAAQ,EAAG,CACtCF,CAAH,GACEA,CAAA5mC,OAAA,EACA,CAAA4mC,CAAA,CAAkB,IAFpB,CAIGpf,EAAH,GACEA,CAAAx0B,SAAA,EACA,CAAAw0B,CAAA,CAAe,IAFjB,CAIGqf,EAAH,GACE5kC,CAAA+4B,MAAA,CAAe6L,CAAf,CAA+B,QAAQ,EAAG,CACxCD,CAAA,CAAkB,IADsB,CAA1C,CAIA,CADAA,CACA,CADkBC,CAClB,CAAAA,CAAA,CAAiB,IALnB,CATyC,CAkB3C1/C,EAAApF,OAAA,CAAaigB,CAAA+kC,mBAAA,CAAwBT,CAAxB,CAAb;AAA8CU,QAA6B,CAAC1kD,CAAD,CAAM,CAC/E,IAAI2kD,EAAiBA,QAAQ,EAAG,CAC1B,CAAA9mD,CAAA,CAAUumD,CAAV,CAAJ,EAAkCA,CAAlC,EAAmD,CAAAv/C,CAAA0/B,MAAA,CAAY6f,CAAZ,CAAnD,EACEL,CAAA,EAF4B,CAAhC,CAKIa,EAAe,EAAE3pB,CAEjBj7B,EAAJ,EACEsf,CAAArK,IAAA,CAAUjV,CAAV,CAAe,OAAQuf,CAAR,CAAf,CAAAmK,QAAA,CAAgD,QAAQ,CAACO,CAAD,CAAW,CACjE,GAAI26B,CAAJ,GAAqB3pB,CAArB,CAAA,CACA,IAAI4pB,EAAWhgD,CAAA4c,KAAA,EACfmwB,EAAA1qB,SAAA,CAAgB+C,CAQZ9nB,EAAAA,CAAQwhD,CAAA,CAAYkB,CAAZ,CAAsB,QAAQ,CAAC1iD,CAAD,CAAQ,CAChDqiD,CAAA,EACA7kC,EAAA84B,MAAA,CAAet2C,CAAf,CAAsB,IAAtB,CAA4Bye,CAA5B,CAAsC+jC,CAAtC,CAFgD,CAAtC,CAKZzf,EAAA,CAAe2f,CACfN,EAAA,CAAiBpiD,CAEjB+iC,EAAAH,MAAA,CAAmB,uBAAnB,CACAlgC,EAAA0/B,MAAA,CAAY2f,CAAZ,CAnBA,CADiE,CAAnE,CAAAxrC,MAAA,CAqBS,QAAQ,EAAG,CACdksC,CAAJ,GAAqB3pB,CAArB,EAAoCupB,CAAA,EADlB,CArBpB,CAwBA,CAAA3/C,CAAAkgC,MAAA,CAAY,0BAAZ,CAzBF,GA2BEyf,CAAA,EACA,CAAA5S,CAAA1qB,SAAA,CAAgB,IA5BlB,CAR+E,CAAjF,CAxByD,CAL5B,CAN5B,CADgF,CADhE,CA9MzB,CAoSIvc,GAAgC,CAAC,UAAD,CAClC,QAAQ,CAACm6C,CAAD,CAAW,CACjB,MAAO,UACK,KADL,UAEM,IAFN,SAGI,WAHJ,MAIChoC,QAAQ,CAACjY,CAAD,CAAQ+b,CAAR,CAAkB6B,CAAlB,CAAyBmvB,CAAzB,CAA+B,CAC3ChxB,CAAApe,KAAA,CAAcovC,CAAA1qB,SAAd,CACA49B,EAAA,CAASlkC,CAAAsH,SAAA,EAAT,CAAA,CAA8BrjB,CAA9B,CAF2C,CAJxC,CADU,CADe,CApSpC,CAwWI8E,GAAkB6lC,EAAA,CAAY,UACtB,GADsB;QAEvB1qC,QAAQ,EAAG,CAClB,MAAO,KACA0f,QAAQ,CAAC3f,CAAD,CAAQ5C,CAAR,CAAiB4f,CAAjB,CAAwB,CACnChd,CAAA0/B,MAAA,CAAY1iB,CAAAkjC,OAAZ,CADmC,CADhC,CADW,CAFY,CAAZ,CAxWtB,CAmZIn7C,GAAyB4lC,EAAA,CAAY,UAAY,CAAA,CAAZ,UAA4B,GAA5B,CAAZ,CAnZ7B,CAgkBI3lC,GAAuB,CAAC,SAAD,CAAY,cAAZ,CAA4B,QAAQ,CAAC4gC,CAAD,CAAUprB,CAAV,CAAwB,CACrF,IAAI2lC,EAAQ,KACZ,OAAO,UACK,IADL,MAECloC,QAAQ,CAACjY,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CAAA,IAC/BumD,EAAYvmD,CAAAk3B,MADmB,CAE/BsvB,EAAUxmD,CAAA+jB,MAAAqO,KAAVo0B,EAA6BjjD,CAAAvD,KAAA,CAAaA,CAAA+jB,MAAAqO,KAAb,CAFE,CAG/BpkB,EAAShO,CAAAgO,OAATA,EAAwB,CAHO,CAI/By4C,EAAQtgD,CAAA0/B,MAAA,CAAY2gB,CAAZ,CAARC,EAAgC,EAJD,CAK/BC,EAAc,EALiB,CAM/Br4B,EAAc1N,CAAA0N,YAAA,EANiB,CAO/BC,EAAY3N,CAAA2N,UAAA,EAPmB,CAQ/Bq4B,EAAS,oBAEblqD,EAAA,CAAQuD,CAAR,CAAc,QAAQ,CAACivB,CAAD,CAAa23B,CAAb,CAA4B,CAC5CD,CAAAlgD,KAAA,CAAYmgD,CAAZ,CAAJ,GACEH,CAAA,CAAMpjD,CAAA,CAAUujD,CAAA3iD,QAAA,CAAsB,MAAtB,CAA8B,EAA9B,CAAAA,QAAA,CAA0C,OAA1C,CAAmD,GAAnD,CAAV,CAAN,CADF,CAEIV,CAAAvD,KAAA,CAAaA,CAAA+jB,MAAA,CAAW6iC,CAAX,CAAb,CAFJ,CADgD,CAAlD,CAMAnqD,EAAA,CAAQgqD,CAAR,CAAe,QAAQ,CAACx3B,CAAD,CAAaryB,CAAb,CAAkB,CACvC8pD,CAAA,CAAY9pD,CAAZ,CAAA,CACE+jB,CAAA,CAAasO,CAAAhrB,QAAA,CAAmBqiD,CAAnB,CAA0Bj4B,CAA1B,CAAwCk4B,CAAxC,CAAoD,GAApD,CACXv4C,CADW,CACFsgB,CADE,CAAb,CAFqC,CAAzC,CAMAnoB,EAAApF,OAAA,CAAa8lD,QAAyB,EAAG,CACvC,IAAIrpD;AAAQkyC,UAAA,CAAWvpC,CAAA0/B,MAAA,CAAY0gB,CAAZ,CAAX,CAEZ,IAAKzgB,KAAA,CAAMtoC,CAAN,CAAL,CAME,MAAO,EAHDA,EAAN,GAAeipD,EAAf,GAAuBjpD,CAAvB,CAA+BuuC,CAAAlU,UAAA,CAAkBr6B,CAAlB,CAA0BwQ,CAA1B,CAA/B,CACC,OAAO04C,EAAA,CAAYlpD,CAAZ,CAAA,CAAmB2I,CAAnB,CAA0B5C,CAA1B,CAAmC,CAAA,CAAnC,CAP6B,CAAzC,CAWGujD,QAA+B,CAAC3iB,CAAD,CAAS,CACzC5gC,CAAAyoB,KAAA,CAAamY,CAAb,CADyC,CAX3C,CAtBmC,CAFhC,CAF8E,CAA5D,CAhkB3B,CAkzBI/4B,GAAoB,CAAC,QAAD,CAAW,UAAX,CAAuB,QAAQ,CAAC0V,CAAD,CAASG,CAAT,CAAmB,CAExE,IAAI8lC,EAAiB9qD,CAAA,CAAO,UAAP,CACrB,OAAO,YACO,SADP,UAEK,GAFL,UAGK,CAAA,CAHL,OAIE,CAAA,CAJF,MAKCmiB,QAAQ,CAACkK,CAAD,CAASpG,CAAT,CAAmB6B,CAAnB,CAA0BmvB,CAA1B,CAAgC+R,CAAhC,CAA4C,CACtD,IAAIh2B,EAAalL,CAAAijC,SAAjB,CACIhjD,EAAQirB,CAAAjrB,MAAA,CAAiB,qEAAjB,CADZ,CAEcijD,CAFd,CAEgCC,CAFhC,CAEgDC,CAFhD,CAEkEC,CAFlE,CAGYC,CAHZ,CAG6BC,CAH7B,CAIEC,EAAe,KAAM5yC,EAAN,CAEjB,IAAI,CAAC3Q,CAAL,CACE,KAAM+iD,EAAA,CAAe,MAAf,CACJ93B,CADI,CAAN,CAIFu4B,CAAA,CAAMxjD,CAAA,CAAM,CAAN,CACNyjD,EAAA,CAAMzjD,CAAA,CAAM,CAAN,CAGN,EAFA0jD,CAEA,CAFa1jD,CAAA,CAAM,CAAN,CAEb,GACEijD,CACA,CADmBnmC,CAAA,CAAO4mC,CAAP,CACnB,CAAAR,CAAA,CAAiBA,QAAQ,CAACtqD,CAAD,CAAMY,CAAN,CAAaE,CAAb,CAAoB,CAEvC4pD,CAAJ,GAAmBC,CAAA,CAAaD,CAAb,CAAnB,CAAiD1qD,CAAjD,CACA2qD,EAAA,CAAaF,CAAb,CAAA,CAAgC7pD,CAChC+pD,EAAA5R,OAAA,CAAsBj4C,CACtB,OAAOupD,EAAA,CAAiB3+B,CAAjB;AAAyBi/B,CAAzB,CALoC,CAF/C,GAUEJ,CAGA,CAHmBA,QAAQ,CAACvqD,CAAD,CAAMY,CAAN,CAAa,CACtC,MAAOmX,GAAA,CAAQnX,CAAR,CAD+B,CAGxC,CAAA4pD,CAAA,CAAiBA,QAAQ,CAACxqD,CAAD,CAAM,CAC7B,MAAOA,EADsB,CAbjC,CAkBAoH,EAAA,CAAQwjD,CAAAxjD,MAAA,CAAU,+CAAV,CACR,IAAI,CAACA,CAAL,CACE,KAAM+iD,EAAA,CAAe,QAAf,CACoDS,CADpD,CAAN,CAGFH,CAAA,CAAkBrjD,CAAA,CAAM,CAAN,CAAlB,EAA8BA,CAAA,CAAM,CAAN,CAC9BsjD,EAAA,CAAgBtjD,CAAA,CAAM,CAAN,CAOhB,KAAI2jD,EAAe,EAGnBr/B,EAAAic,iBAAA,CAAwBkjB,CAAxB,CAA6BG,QAAuB,CAACC,CAAD,CAAY,CAAA,IAC1DnqD,CAD0D,CACnDrB,CADmD,CAE1DyrD,EAAe5lC,CAAA,CAAS,CAAT,CAF2C,CAG1D6lC,CAH0D,CAM1DC,EAAe,EAN2C,CAO1DC,CAP0D,CAQ1DxlC,CAR0D,CAS1D7lB,CAT0D,CASrDY,CATqD,CAY1D0qD,CAZ0D,CAa1D9+C,CAb0D,CAc1D++C,EAAiB,EAIrB,IAAIjsD,EAAA,CAAY2rD,CAAZ,CAAJ,CACEK,CACA,CADiBL,CACjB,CAAAO,CAAA,CAAclB,CAAd,EAAgCC,CAFlC,KAGO,CACLiB,CAAA,CAAclB,CAAd,EAAgCE,CAEhCc,EAAA,CAAiB,EACjB,KAAKtrD,CAAL,GAAYirD,EAAZ,CACMA,CAAA/qD,eAAA,CAA0BF,CAA1B,CAAJ,EAAuD,GAAvD,EAAsCA,CAAA2E,OAAA,CAAW,CAAX,CAAtC,EACE2mD,CAAAhrD,KAAA,CAAoBN,CAApB,CAGJsrD,EAAA/qD,KAAA,EATK,CAYP8qD,CAAA,CAAcC,CAAA7rD,OAGdA,EAAA,CAAS8rD,CAAA9rD,OAAT,CAAiC6rD,CAAA7rD,OACjC,KAAIqB,CAAJ,CAAY,CAAZ,CAAeA,CAAf,CAAuBrB,CAAvB,CAA+BqB,CAAA,EAA/B,CAKC,GAJAd,CAIG,CAJIirD,CAAD,GAAgBK,CAAhB,CAAkCxqD,CAAlC,CAA0CwqD,CAAA,CAAexqD,CAAf,CAI7C,CAHHF,CAGG,CAHKqqD,CAAA,CAAWjrD,CAAX,CAGL,CAFHyrD,CAEG,CAFSD,CAAA,CAAYxrD,CAAZ,CAAiBY,CAAjB,CAAwBE,CAAxB,CAET,CADHgK,EAAA,CAAwB2gD,CAAxB,CAAmC,eAAnC,CACG,CAAAV,CAAA7qD,eAAA,CAA4BurD,CAA5B,CAAH,CACEj/C,CAGA,CAHQu+C,CAAA,CAAaU,CAAb,CAGR,CAFA,OAAOV,CAAA,CAAaU,CAAb,CAEP,CADAL,CAAA,CAAaK,CAAb,CACA;AAD0Bj/C,CAC1B,CAAA++C,CAAA,CAAezqD,CAAf,CAAA,CAAwB0L,CAJ1B,KAKO,CAAA,GAAI4+C,CAAAlrD,eAAA,CAA4BurD,CAA5B,CAAJ,CAML,KAJA5rD,EAAA,CAAQ0rD,CAAR,CAAwB,QAAQ,CAAC/+C,CAAD,CAAQ,CAClCA,CAAJ,EAAaA,CAAAjD,MAAb,GAA0BwhD,CAAA,CAAav+C,CAAA24B,GAAb,CAA1B,CAAmD34B,CAAnD,CADsC,CAAxC,CAIM,CAAA29C,CAAA,CAAe,OAAf,CACiI93B,CADjI,CACmJo5B,CADnJ,CAAN,CAIAF,CAAA,CAAezqD,CAAf,CAAA,CAAwB,IAAM2qD,CAAN,CACxBL,EAAA,CAAaK,CAAb,CAAA,CAA0B,CAAA,CAXrB,CAgBR,IAAKzrD,CAAL,GAAY+qD,EAAZ,CAEMA,CAAA7qD,eAAA,CAA4BF,CAA5B,CAAJ,GACEwM,CAIA,CAJQu+C,CAAA,CAAa/qD,CAAb,CAIR,CAHAkwB,CAGA,CAHmB9kB,EAAA,CAAiBoB,CAAA3F,MAAjB,CAGnB,CAFAwd,CAAA+4B,MAAA,CAAeltB,CAAf,CAEA,CADArwB,CAAA,CAAQqwB,CAAR,CAA0B,QAAQ,CAACvpB,CAAD,CAAU,CAAEA,CAAA,aAAA,CAAsB,CAAA,CAAxB,CAA5C,CACA,CAAA6F,CAAAjD,MAAA6L,SAAA,EALF,CAUGtU,EAAA,CAAQ,CAAb,KAAgBrB,CAAhB,CAAyB6rD,CAAA7rD,OAAzB,CAAgDqB,CAAhD,CAAwDrB,CAAxD,CAAgEqB,CAAA,EAAhE,CAAyE,CACvEd,CAAA,CAAOirD,CAAD,GAAgBK,CAAhB,CAAkCxqD,CAAlC,CAA0CwqD,CAAA,CAAexqD,CAAf,CAChDF,EAAA,CAAQqqD,CAAA,CAAWjrD,CAAX,CACRwM,EAAA,CAAQ++C,CAAA,CAAezqD,CAAf,CACJyqD,EAAA,CAAezqD,CAAf,CAAuB,CAAvB,CAAJ,GAA+BoqD,CAA/B,CAA0DK,CAAA/+C,CAAe1L,CAAf0L,CAAuB,CAAvBA,CAwD3D3F,MAAA,CAxD2D0kD,CAAA/+C,CAAe1L,CAAf0L,CAAuB,CAAvBA,CAwD/C3F,MAAApH,OAAZ,CAAiC,CAAjC,CAxDC,CAEA,IAAI+M,CAAAjD,MAAJ,CAAiB,CAGfsc,CAAA,CAAarZ,CAAAjD,MAEb4hD,EAAA,CAAWD,CACX,GACEC,EAAA,CAAWA,CAAA3/C,YADb,OAEQ2/C,CAFR,EAEoBA,CAAA,aAFpB,CAIkB3+C,EAwCrB3F,MAAA,CAAY,CAAZ,CAxCG,EAA4BskD,CAA5B,EAEE9mC,CAAAg5B,KAAA,CAAcjyC,EAAA,CAAiBoB,CAAA3F,MAAjB,CAAd,CAA6C,IAA7C,CAAmDD,CAAA,CAAOskD,CAAP,CAAnD,CAEFA,EAAA,CAA2B1+C,CAwC9B3F,MAAA,CAxC8B2F,CAwClB3F,MAAApH,OAAZ,CAAiC,CAAjC,CAtDkB,CAAjB,IAiBEomB,EAAA,CAAa6F,CAAAvF,KAAA,EAGfN;CAAA,CAAW4kC,CAAX,CAAA,CAA8B7pD,CAC1B8pD,EAAJ,GAAmB7kC,CAAA,CAAW6kC,CAAX,CAAnB,CAA+C1qD,CAA/C,CACA6lB,EAAAkzB,OAAA,CAAoBj4C,CACpB+kB,EAAA6lC,OAAA,CAA+B,CAA/B,GAAqB5qD,CACrB+kB,EAAA8lC,MAAA,CAAoB7qD,CAApB,GAA+BuqD,CAA/B,CAA6C,CAC7CxlC,EAAA+lC,QAAA,CAAqB,EAAE/lC,CAAA6lC,OAAF,EAAuB7lC,CAAA8lC,MAAvB,CAErB9lC,EAAAgmC,KAAA,CAAkB,EAAEhmC,CAAAimC,MAAF,CAAmC,CAAnC,IAAsBhrD,CAAtB,CAA4B,CAA5B,EAGb0L,EAAAjD,MAAL,EACE8+C,CAAA,CAAYxiC,CAAZ,CAAwB,QAAQ,CAAChf,CAAD,CAAQ,CACtCA,CAAA,CAAMA,CAAApH,OAAA,EAAN,CAAA,CAAwBN,CAAAstB,cAAA,CAAuB,iBAAvB,CAA2C4F,CAA3C,CAAwD,GAAxD,CACxBhO,EAAA84B,MAAA,CAAet2C,CAAf,CAAsB,IAAtB,CAA4BD,CAAA,CAAOskD,CAAP,CAA5B,CACAA,EAAA,CAAerkD,CACf2F,EAAAjD,MAAA,CAAcsc,CAIdrZ,EAAA3F,MAAA,CAAcA,CACdukD,EAAA,CAAa5+C,CAAA24B,GAAb,CAAA,CAAyB34B,CATa,CAAxC,CArCqE,CAkDzEu+C,CAAA,CAAeK,CA7H+C,CAAhE,CAlDsD,CALrD,CAHiE,CAAlD,CAlzBxB,CA2oCI38C,GAAkB,CAAC,UAAD,CAAa,QAAQ,CAAC4V,CAAD,CAAW,CACpD,MAAO,SAAQ,CAAC9a,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CACpCmG,CAAApF,OAAA,CAAaf,CAAA2oD,OAAb,CAA0BC,QAA0B,CAACprD,CAAD,CAAO,CACzDyjB,CAAA,CAAS9d,EAAA,CAAU3F,CAAV,CAAA,CAAmB,aAAnB,CAAmC,UAA5C,CAAA,CAAwD+F,CAAxD,CAAiE,SAAjE,CADyD,CAA3D,CADoC,CADc,CAAhC,CA3oCtB,CAuyCIuH,GAAkB,CAAC,UAAD,CAAa,QAAQ,CAACmW,CAAD,CAAW,CACpD,MAAO,SAAQ,CAAC9a,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CACpCmG,CAAApF,OAAA,CAAaf,CAAA6oD,OAAb,CAA0BC,QAA0B,CAACtrD,CAAD,CAAO,CACzDyjB,CAAA,CAAS9d,EAAA,CAAU3F,CAAV,CAAA,CAAmB,UAAnB,CAAgC,aAAzC,CAAA,CAAwD+F,CAAxD;AAAiE,SAAjE,CADyD,CAA3D,CADoC,CADc,CAAhC,CAvyCtB,CA61CI+H,GAAmBwlC,EAAA,CAAY,QAAQ,CAAC3qC,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CAChEmG,CAAApF,OAAA,CAAaf,CAAA+oD,QAAb,CAA2BC,QAA2B,CAACC,CAAD,CAAYC,CAAZ,CAAuB,CACvEA,CAAJ,EAAkBD,CAAlB,GAAgCC,CAAhC,EACEzsD,CAAA,CAAQysD,CAAR,CAAmB,QAAQ,CAACvmD,CAAD,CAAMqnC,CAAN,CAAa,CAAEzmC,CAAA2zC,IAAA,CAAYlN,CAAZ,CAAmB,EAAnB,CAAF,CAAxC,CAEEif,EAAJ,EAAe1lD,CAAA2zC,IAAA,CAAY+R,CAAZ,CAJ4D,CAA7E,CAKG,CAAA,CALH,CADgE,CAA3C,CA71CvB,CAq+CI19C,GAAoB,CAAC,UAAD,CAAa,QAAQ,CAAC0V,CAAD,CAAW,CACtD,MAAO,UACK,IADL,SAEI,UAFJ,YAKO,CAAC,QAAD,CAAWkoC,QAA2B,EAAG,CACpD,IAAAC,MAAA,CAAa,EADuC,CAAzC,CALP,MAQChrC,QAAQ,CAACjY,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuBmpD,CAAvB,CAA2C,CAAA,IAEnDE,EAAsB,EAF6B,CAGnDC,EAAmB,EAHgC,CAInDpE,EAAmB,EAJgC,CAKnDqE,EAAiB,EAErBpjD,EAAApF,OAAA,CANgBf,CAAAwpD,SAMhB,EANiCxpD,CAAAmc,GAMjC,CAAwBstC,QAA4B,CAACjsD,CAAD,CAAQ,CAAA,IACtDH,CADsD,CACnD6V,CACF7V,EAAA,CAAI,CAAT,KAAY6V,CAAZ,CAAiBgyC,CAAA7oD,OAAjB,CAA0CgB,CAA1C,CAA8C6V,CAA9C,CAAkD,EAAE7V,CAApD,CACE6nD,CAAA,CAAiB7nD,CAAjB,CAAA2hB,OAAA,EAIG3hB,EAAA,CAFL6nD,CAAA7oD,OAEK,CAFqB,CAE1B,KAAY6W,CAAZ,CAAiBq2C,CAAAltD,OAAjB,CAAwCgB,CAAxC,CAA4C6V,CAA5C,CAAgD,EAAE7V,CAAlD,CAAqD,CACnD,IAAIu6C,EAAW0R,CAAA,CAAiBjsD,CAAjB,CACfksD,EAAA,CAAelsD,CAAf,CAAA2U,SAAA,EACAkzC,EAAA,CAAiB7nD,CAAjB,CAAA,CAAsBu6C,CACtB32B,EAAA+4B,MAAA,CAAepC,CAAf,CAAyB,QAAQ,EAAG,CAClCsN,CAAA1kD,OAAA,CAAwBnD,CAAxB,CAA2B,CAA3B,CADkC,CAApC,CAJmD,CASrDisD,CAAAjtD,OAAA,CAA0B,CAC1BktD,EAAAltD,OAAA;AAAwB,CAExB,IAAKgtD,CAAL,CAA2BF,CAAAC,MAAA,CAAyB,GAAzB,CAA+B5rD,CAA/B,CAA3B,EAAoE2rD,CAAAC,MAAA,CAAyB,GAAzB,CAApE,CACEjjD,CAAA0/B,MAAA,CAAY7lC,CAAA0pD,OAAZ,CACA,CAAAjtD,CAAA,CAAQ4sD,CAAR,CAA6B,QAAQ,CAACM,CAAD,CAAqB,CACxD,IAAIC,EAAgBzjD,CAAA4c,KAAA,EACpBwmC,EAAArsD,KAAA,CAAoB0sD,CAApB,CACAD,EAAA1mC,WAAA,CAA8B2mC,CAA9B,CAA6C,QAAQ,CAACC,CAAD,CAAc,CACjE,IAAIC,EAASH,CAAApmD,QAEb+lD,EAAApsD,KAAA,CAAsB2sD,CAAtB,CACA5oC,EAAA84B,MAAA,CAAe8P,CAAf,CAA4BC,CAAAlrD,OAAA,EAA5B,CAA6CkrD,CAA7C,CAJiE,CAAnE,CAHwD,CAA1D,CArBwD,CAA5D,CAPuD,CARpD,CAD+C,CAAhC,CAr+CxB,CA0hDIt+C,GAAwBslC,EAAA,CAAY,YAC1B,SAD0B,UAE5B,GAF4B,SAG7B,WAH6B,MAIhC1yB,QAAQ,CAACjY,CAAD,CAAQ5C,CAAR,CAAiB4f,CAAjB,CAAwB+vB,CAAxB,CAA8B+R,CAA9B,CAA2C,CACvD/R,CAAAkW,MAAA,CAAW,GAAX,CAAiBjmC,CAAA4mC,aAAjB,CAAA,CAAwC7W,CAAAkW,MAAA,CAAW,GAAX,CAAiBjmC,CAAA4mC,aAAjB,CAAxC,EAAgF,EAChF7W,EAAAkW,MAAA,CAAW,GAAX,CAAiBjmC,CAAA4mC,aAAjB,CAAA7sD,KAAA,CAA0C,YAAc+nD,CAAd,SAAoC1hD,CAApC,CAA1C,CAFuD,CAJnB,CAAZ,CA1hD5B,CAoiDIkI,GAA2BqlC,EAAA,CAAY,YAC7B,SAD6B,UAE/B,GAF+B,SAGhC,WAHgC,MAInC1yB,QAAQ,CAACjY,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuBkzC,CAAvB,CAA6B+R,CAA7B,CAA0C,CACtD/R,CAAAkW,MAAA,CAAW,GAAX,CAAA,CAAmBlW,CAAAkW,MAAA,CAAW,GAAX,CAAnB;AAAsC,EACtClW,EAAAkW,MAAA,CAAW,GAAX,CAAAlsD,KAAA,CAAqB,YAAc+nD,CAAd,SAAoC1hD,CAApC,CAArB,CAFsD,CAJf,CAAZ,CApiD/B,CAqmDIoI,GAAwBmlC,EAAA,CAAY,MAChC1yB,QAAQ,CAACkK,CAAD,CAASpG,CAAT,CAAmB8nC,CAAnB,CAA2B5pC,CAA3B,CAAuC6kC,CAAvC,CAAoD,CAChE,GAAI,CAACA,CAAL,CACE,KAAMhpD,EAAA,CAAO,cAAP,CAAA,CAAuB,QAAvB,CAILqH,EAAA,CAAY4e,CAAZ,CAJK,CAAN,CAOF+iC,CAAA,CAAY,QAAQ,CAACxhD,CAAD,CAAQ,CAC1Bye,CAAAxe,MAAA,EACAwe,EAAAre,OAAA,CAAgBJ,CAAhB,CAF0B,CAA5B,CATgE,CAD5B,CAAZ,CArmD5B,CAupDIwG,GAAkB,CAAC,gBAAD,CAAmB,QAAQ,CAAC4W,CAAD,CAAiB,CAChE,MAAO,UACK,GADL,UAEK,CAAA,CAFL,SAGIza,QAAQ,CAAC7C,CAAD,CAAUvD,CAAV,CAAgB,CACd,kBAAjB,EAAIA,CAAAqR,KAAJ,EAKEwP,CAAA/L,IAAA,CAJkB9U,CAAA+hC,GAIlB,CAFWx+B,CAAA,CAAQ,CAAR,CAAAyoB,KAEX,CAN6B,CAH5B,CADyD,CAA5C,CAvpDtB,CAuqDIi+B,GAAkBhuD,CAAA,CAAO,WAAP,CAvqDtB,CA6yDIyP,GAAqBzM,EAAA,CAAQ,UAAY,CAAA,CAAZ,CAAR,CA7yDzB,CA+yDIiL,GAAkB,CAAC,UAAD,CAAa,QAAb,CAAuB,QAAQ,CAACk8C,CAAD,CAAatlC,CAAb,CAAqB,CAAA,IAEpEopC,EAAoB,wMAFgD;AAGpEC,EAAgB,eAAgBrrD,CAAhB,CAGpB,OAAO,UACK,GADL,SAEI,CAAC,QAAD,CAAW,UAAX,CAFJ,YAGO,CAAC,UAAD,CAAa,QAAb,CAAuB,QAAvB,CAAiC,QAAQ,CAACojB,CAAD,CAAWoG,CAAX,CAAmB0hC,CAAnB,CAA2B,CAAA,IAC1E7nD,EAAO,IADmE,CAE1EioD,EAAa,EAF6D,CAG1EC,EAAcF,CAH4D,CAK1EG,CAGJnoD,EAAAooD,UAAA,CAAiBP,CAAA7G,QAGjBhhD,EAAAqoD,KAAA,CAAYC,QAAQ,CAACC,CAAD,CAAeC,CAAf,CAA4BC,CAA5B,CAA4C,CAC9DP,CAAA,CAAcK,CAEdJ,EAAA,CAAgBM,CAH8C,CAOhEzoD,EAAA0oD,UAAA,CAAiBC,QAAQ,CAACttD,CAAD,CAAQ,CAC/BkK,EAAA,CAAwBlK,CAAxB,CAA+B,gBAA/B,CACA4sD,EAAA,CAAW5sD,CAAX,CAAA,CAAoB,CAAA,CAEhB6sD,EAAAnW,WAAJ,EAA8B12C,CAA9B,GACE0kB,CAAAvf,IAAA,CAAanF,CAAb,CACA,CAAI8sD,CAAA1rD,OAAA,EAAJ,EAA4B0rD,CAAAtrC,OAAA,EAF9B,CAJ+B,CAWjC7c,EAAA4oD,aAAA,CAAoBC,QAAQ,CAACxtD,CAAD,CAAQ,CAC9B,IAAAytD,UAAA,CAAeztD,CAAf,CAAJ,GACE,OAAO4sD,CAAA,CAAW5sD,CAAX,CACP,CAAI6sD,CAAAnW,WAAJ,EAA8B12C,CAA9B,EACE,IAAA0tD,oBAAA,CAAyB1tD,CAAzB,CAHJ,CADkC,CAUpC2E,EAAA+oD,oBAAA,CAA2BC,QAAQ,CAACxoD,CAAD,CAAM,CACnCyoD,CAAAA,CAAa,IAAbA,CAAoBz2C,EAAA,CAAQhS,CAAR,CAApByoD,CAAmC,IACvCd,EAAA3nD,IAAA,CAAkByoD,CAAlB,CACAlpC,EAAA02B,QAAA,CAAiB0R,CAAjB,CACApoC,EAAAvf,IAAA,CAAayoD,CAAb,CACAd,EAAAvqD,KAAA,CAAmB,UAAnB;AAA+B,CAAA,CAA/B,CALuC,CASzCoC,EAAA8oD,UAAA,CAAiBI,QAAQ,CAAC7tD,CAAD,CAAQ,CAC/B,MAAO4sD,EAAAttD,eAAA,CAA0BU,CAA1B,CADwB,CAIjC8qB,EAAA0d,IAAA,CAAW,UAAX,CAAuB,QAAQ,EAAG,CAEhC7jC,CAAA+oD,oBAAA,CAA2BpsD,CAFK,CAAlC,CApD8E,CAApE,CAHP,MA6DCsf,QAAQ,CAACjY,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB4jD,CAAvB,CAA8B,CA0C1C0H,QAASA,EAAa,CAACnlD,CAAD,CAAQolD,CAAR,CAAuBlB,CAAvB,CAAoCmB,CAApC,CAAgD,CACpEnB,CAAA/V,QAAA,CAAsBmX,QAAQ,EAAG,CAC/B,IAAIvH,EAAYmG,CAAAnW,WAEZsX,EAAAP,UAAA,CAAqB/G,CAArB,CAAJ,EACMoG,CAAA1rD,OAAA,EAEJ,EAF4B0rD,CAAAtrC,OAAA,EAE5B,CADAusC,CAAA5oD,IAAA,CAAkBuhD,CAAlB,CACA,CAAkB,EAAlB,GAAIA,CAAJ,EAAsBwH,CAAA3rD,KAAA,CAAiB,UAAjB,CAA6B,CAAA,CAA7B,CAHxB,EAKMb,CAAA,CAAYglD,CAAZ,CAAJ,EAA8BwH,CAA9B,CACEH,CAAA5oD,IAAA,CAAkB,EAAlB,CADF,CAGE6oD,CAAAN,oBAAA,CAA+BhH,CAA/B,CAX2B,CAgBjCqH,EAAApvC,GAAA,CAAiB,QAAjB,CAA2B,QAAQ,EAAG,CACpChW,CAAAG,OAAA,CAAa,QAAQ,EAAG,CAClBgkD,CAAA1rD,OAAA,EAAJ,EAA4B0rD,CAAAtrC,OAAA,EAC5BqrC,EAAAlW,cAAA,CAA0BoX,CAAA5oD,IAAA,EAA1B,CAFsB,CAAxB,CADoC,CAAtC,CAjBoE,CAyBtEgpD,QAASA,EAAe,CAACxlD,CAAD,CAAQolD,CAAR,CAAuBrY,CAAvB,CAA6B,CACnD,IAAI0Y,CACJ1Y,EAAAoB,QAAA,CAAeC,QAAQ,EAAG,CACxB,IAAIsX,EAAQ,IAAIh3C,EAAJ,CAAYq+B,CAAAgB,WAAZ,CACZz3C,EAAA,CAAQ8uD,CAAAtrD,KAAA,CAAmB,QAAnB,CAAR;AAAsC,QAAQ,CAACq2C,CAAD,CAAS,CACrDA,CAAAsB,SAAA,CAAkBz4C,CAAA,CAAU0sD,CAAAt1C,IAAA,CAAU+/B,CAAA94C,MAAV,CAAV,CADmC,CAAvD,CAFwB,CAS1B2I,EAAApF,OAAA,CAAa+qD,QAA4B,EAAG,CACrCtqD,EAAA,CAAOoqD,CAAP,CAAiB1Y,CAAAgB,WAAjB,CAAL,GACE0X,CACA,CADWvqD,EAAA,CAAY6xC,CAAAgB,WAAZ,CACX,CAAAhB,CAAAoB,QAAA,EAFF,CAD0C,CAA5C,CAOAiX,EAAApvC,GAAA,CAAiB,QAAjB,CAA2B,QAAQ,EAAG,CACpChW,CAAAG,OAAA,CAAa,QAAQ,EAAG,CACtB,IAAIhG,EAAQ,EACZ7D,EAAA,CAAQ8uD,CAAAtrD,KAAA,CAAmB,QAAnB,CAAR,CAAsC,QAAQ,CAACq2C,CAAD,CAAS,CACjDA,CAAAsB,SAAJ,EACEt3C,CAAApD,KAAA,CAAWo5C,CAAA94C,MAAX,CAFmD,CAAvD,CAKA01C,EAAAiB,cAAA,CAAmB7zC,CAAnB,CAPsB,CAAxB,CADoC,CAAtC,CAlBmD,CA+BrDyrD,QAASA,EAAc,CAAC5lD,CAAD,CAAQolD,CAAR,CAAuBrY,CAAvB,CAA6B,CA6GlD8Y,QAASA,EAAM,EAAG,CAAA,IAEZC,EAAe,CAAC,EAAD,CAAI,EAAJ,CAFH,CAGZC,EAAmB,CAAC,EAAD,CAHP,CAIZC,CAJY,CAKZC,CALY,CAMZ9V,CANY,CAOZ+V,CAPY,CAOIC,CAChBC,EAAAA,CAAarZ,CAAA6P,YACbr0B,EAAAA,CAAS89B,CAAA,CAASrmD,CAAT,CAATuoB,EAA4B,EAThB,KAUZzxB,EAAOwvD,CAAA,CAAUzvD,EAAA,CAAW0xB,CAAX,CAAV,CAA+BA,CAV1B,CAYCryB,CAZD,CAaZqwD,CAbY,CAaAhvD,CACZyZ,EAAAA,CAAS,EAETw1C,EAAAA,CAAc,CAAA,CAhBF,KAiBZC,CAjBY,CAkBZrpD,CAGJ,IAAIo0C,CAAJ,CACE,GAAIkV,CAAJ,EAAerwD,CAAA,CAAQ+vD,CAAR,CAAf,CAEE,IADAI,CACSG,CADK,IAAIj4C,EAAJ,CAAY,EAAZ,CACLi4C,CAAAA,CAAAA,CAAa,CAAtB,CAAyBA,CAAzB,CAAsCP,CAAAlwD,OAAtC,CAAyDywD,CAAA,EAAzD,CACE31C,CAAA,CAAO41C,CAAP,CACA,CADoBR,CAAA,CAAWO,CAAX,CACpB,CAAAH,CAAA73C,IAAA,CAAgB+3C,CAAA,CAAQ1mD,CAAR,CAAegR,CAAf,CAAhB,CAAwCo1C,CAAA,CAAWO,CAAX,CAAxC,CAJJ,KAOEH,EAAA,CAAc,IAAI93C,EAAJ,CAAY03C,CAAZ,CAKlB,KAAK7uD,CAAL,CAAa,CAAb,CAAgBrB,CAAA,CAASY,CAAAZ,OAAT;AAAsBqB,CAAtB,CAA8BrB,CAA9C,CAAsDqB,CAAA,EAAtD,CAA+D,CAE7Dd,CAAA,CAAMc,CACN,IAAI+uD,CAAJ,CAAa,CACX7vD,CAAA,CAAMK,CAAA,CAAKS,CAAL,CACN,IAAuB,GAAvB,GAAKd,CAAA2E,OAAA,CAAW,CAAX,CAAL,CAA6B,QAC7B4V,EAAA,CAAOs1C,CAAP,CAAA,CAAkB7vD,CAHP,CAMbua,CAAA,CAAO41C,CAAP,CAAA,CAAoBr+B,CAAA,CAAO9xB,CAAP,CAEpBuvD,EAAA,CAAkBa,CAAA,CAAU7mD,CAAV,CAAiBgR,CAAjB,CAAlB,EAA8C,EAC9C,EAAMi1C,CAAN,CAAoBH,CAAA,CAAaE,CAAb,CAApB,IACEC,CACA,CADcH,CAAA,CAAaE,CAAb,CACd,CAD8C,EAC9C,CAAAD,CAAAhvD,KAAA,CAAsBivD,CAAtB,CAFF,CAIIxU,EAAJ,CACEC,CADF,CACaz4C,CAAA,CACTwtD,CAAA3tC,OAAA,CAAmB6tC,CAAA,CAAUA,CAAA,CAAQ1mD,CAAR,CAAegR,CAAf,CAAV,CAAmClY,CAAA,CAAQkH,CAAR,CAAegR,CAAf,CAAtD,CADS,CADb,EAKM01C,CAAJ,EACMI,CAEJ,CAFgB,EAEhB,CADAA,CAAA,CAAUF,CAAV,CACA,CADuBR,CACvB,CAAA3U,CAAA,CAAWiV,CAAA,CAAQ1mD,CAAR,CAAe8mD,CAAf,CAAX,GAAyCJ,CAAA,CAAQ1mD,CAAR,CAAegR,CAAf,CAH3C,EAKEygC,CALF,CAKa2U,CALb,GAK4BttD,CAAA,CAAQkH,CAAR,CAAegR,CAAf,CAE5B,CAAAw1C,CAAA,CAAcA,CAAd,EAA6B/U,CAZ/B,CAcAsV,EAAA,CAAQC,CAAA,CAAUhnD,CAAV,CAAiBgR,CAAjB,CAGR+1C,EAAA,CAAQ/tD,CAAA,CAAU+tD,CAAV,CAAA,CAAmBA,CAAnB,CAA2B,EACnCd,EAAAlvD,KAAA,CAAiB,IAEX2vD,CAAA,CAAUA,CAAA,CAAQ1mD,CAAR,CAAegR,CAAf,CAAV,CAAoCs1C,CAAA,CAAUxvD,CAAA,CAAKS,CAAL,CAAV,CAAwBA,CAFjD,OAGRwvD,CAHQ,UAILtV,CAJK,CAAjB,CAlC6D,CAyC1DD,CAAL,GACMyV,CAAJ,EAAiC,IAAjC,GAAkBb,CAAlB,CAEEN,CAAA,CAAa,EAAb,CAAAhuD,QAAA,CAAyB,IAAI,EAAJ,OAAc,EAAd,UAA2B,CAAC0uD,CAA5B,CAAzB,CAFF,CAGYA,CAHZ,EAKEV,CAAA,CAAa,EAAb,CAAAhuD,QAAA,CAAyB,IAAI,GAAJ,OAAe,EAAf,UAA4B,CAAA,CAA5B,CAAzB,CANJ,CAWKyuD,EAAA,CAAa,CAAlB,KAAqBW,CAArB,CAAmCnB,CAAA7vD,OAAnC,CACKqwD,CADL,CACkBW,CADlB,CAEKX,CAAA,EAFL,CAEmB,CAEjBP,CAAA,CAAkBD,CAAA,CAAiBQ,CAAjB,CAGlBN,EAAA,CAAcH,CAAA,CAAaE,CAAb,CAEVmB,EAAAjxD,OAAJ,EAAgCqwD,CAAhC,EAEEL,CAMA,CANiB,SACNkB,CAAA9pD,MAAA,EAAAzD,KAAA,CAA8B,OAA9B,CAAuCmsD,CAAvC,CADM,OAERC,CAAAc,MAFQ,CAMjB,CAFAZ,CAEA,CAFkB,CAACD,CAAD,CAElB,CADAiB,CAAApwD,KAAA,CAAuBovD,CAAvB,CACA;AAAAf,CAAA1nD,OAAA,CAAqBwoD,CAAA9oD,QAArB,CARF,GAUE+oD,CAIA,CAJkBgB,CAAA,CAAkBZ,CAAlB,CAIlB,CAHAL,CAGA,CAHiBC,CAAA,CAAgB,CAAhB,CAGjB,CAAID,CAAAa,MAAJ,EAA4Bf,CAA5B,EACEE,CAAA9oD,QAAAvD,KAAA,CAA4B,OAA5B,CAAqCqsD,CAAAa,MAArC,CAA4Df,CAA5D,CAfJ,CAmBAS,EAAA,CAAc,IACVlvD,EAAA,CAAQ,CAAZ,KAAerB,CAAf,CAAwB+vD,CAAA/vD,OAAxB,CAA4CqB,CAA5C,CAAoDrB,CAApD,CAA4DqB,CAAA,EAA5D,CACE44C,CACA,CADS8V,CAAA,CAAY1uD,CAAZ,CACT,CAAA,CAAK8vD,CAAL,CAAsBlB,CAAA,CAAgB5uD,CAAhB,CAAsB,CAAtB,CAAtB,GAEEkvD,CAQA,CARcY,CAAAjqD,QAQd,CAPIiqD,CAAAN,MAOJ,GAP6B5W,CAAA4W,MAO7B,EANEN,CAAA5gC,KAAA,CAAiBwhC,CAAAN,MAAjB,CAAwC5W,CAAA4W,MAAxC,CAMF,CAJIM,CAAAzrB,GAIJ,GAJ0BuU,CAAAvU,GAI1B,EAHE6qB,CAAAjqD,IAAA,CAAgB6qD,CAAAzrB,GAAhB,CAAoCuU,CAAAvU,GAApC,CAGF,CAAIyrB,CAAA5V,SAAJ,GAAgCtB,CAAAsB,SAAhC,EACEgV,CAAA7sD,KAAA,CAAiB,UAAjB,CAA8BytD,CAAA5V,SAA9B,CAAwDtB,CAAAsB,SAAxD,CAXJ,GAiBoB,EAAlB,GAAItB,CAAAvU,GAAJ,EAAwBqrB,CAAxB,CAEE7pD,CAFF,CAEY6pD,CAFZ,CAOGzqD,CAAAY,CAAAZ,CAAU8qD,CAAAhqD,MAAA,EAAVd,KAAA,CACQ2zC,CAAAvU,GADR,CAAA/hC,KAAA,CAES,UAFT,CAEqBs2C,CAAAsB,SAFrB,CAAA5rB,KAAA,CAGSsqB,CAAA4W,MAHT,CAiBH,CAXAZ,CAAApvD,KAAA,CAAsC,SACzBqG,CADyB,OAE3B+yC,CAAA4W,MAF2B,IAG9B5W,CAAAvU,GAH8B,UAIxBuU,CAAAsB,SAJwB,CAAtC,CAWA,CALIgV,CAAJ,CACEA,CAAA9T,MAAA,CAAkBv1C,CAAlB,CADF,CAGE8oD,CAAA9oD,QAAAM,OAAA,CAA8BN,CAA9B,CAEF,CAAAqpD,CAAA,CAAcrpD,CAzChB,CA8CF,KADA7F,CAAA,EACA,CAAM4uD,CAAAjwD,OAAN,CAA+BqB,CAA/B,CAAA,CACE4uD,CAAAvyC,IAAA,EAAAxW,QAAAyb,OAAA,EA5Ee,CAgFnB,IAAA,CAAMsuC,CAAAjxD,OAAN;AAAiCqwD,CAAjC,CAAA,CACEY,CAAAvzC,IAAA,EAAA,CAAwB,CAAxB,CAAAxW,QAAAyb,OAAA,EAzKc,CA5GlB,IAAIhb,CAEJ,IAAI,EAAEA,CAAF,CAAU0pD,CAAA1pD,MAAA,CAAiBkmD,CAAjB,CAAV,CAAJ,CACE,KAAMD,GAAA,CAAgB,MAAhB,CAIJyD,CAJI,CAIQpqD,EAAA,CAAYioD,CAAZ,CAJR,CAAN,CAJgD,IAW9C4B,EAAYrsC,CAAA,CAAO9c,CAAA,CAAM,CAAN,CAAP,EAAmBA,CAAA,CAAM,CAAN,CAAnB,CAXkC,CAY9C+oD,EAAY/oD,CAAA,CAAM,CAAN,CAAZ+oD,EAAwB/oD,CAAA,CAAM,CAAN,CAZsB,CAa9CyoD,EAAUzoD,CAAA,CAAM,CAAN,CAboC,CAc9CgpD,EAAYlsC,CAAA,CAAO9c,CAAA,CAAM,CAAN,CAAP,EAAmB,EAAnB,CAdkC,CAe9C/E,EAAU6hB,CAAA,CAAO9c,CAAA,CAAM,CAAN,CAAA,CAAWA,CAAA,CAAM,CAAN,CAAX,CAAsB+oD,CAA7B,CAfoC,CAgB9CP,EAAW1rC,CAAA,CAAO9c,CAAA,CAAM,CAAN,CAAP,CAhBmC,CAkB9C6oD,EADQ7oD,CAAA2pD,CAAM,CAANA,CACE,CAAQ7sC,CAAA,CAAO9c,CAAA,CAAM,CAAN,CAAP,CAAR,CAA2B,IAlBS,CAuB9CspD,EAAoB,CAAC,CAAC,SAAU/B,CAAV,OAA+B,EAA/B,CAAD,CAAD,CAEpB6B,EAAJ,GAEEhH,CAAA,CAASgH,CAAT,CAAA,CAAqBjnD,CAArB,CAQA,CAJAinD,CAAAz/B,YAAA,CAAuB,UAAvB,CAIA,CAAAy/B,CAAApuC,OAAA,EAVF,CAcAusC,EAAA7nD,MAAA,EAEA6nD,EAAApvC,GAAA,CAAiB,QAAjB,CAA2B,QAAQ,EAAG,CACpChW,CAAAG,OAAA,CAAa,QAAQ,EAAG,CAAA,IAClB8lD,CADkB,CAElBvE,EAAa2E,CAAA,CAASrmD,CAAT,CAAb0hD,EAAgC,EAFd,CAGlB1wC,EAAS,EAHS,CAIlBva,CAJkB,CAIbY,CAJa,CAISE,CAJT,CAIgBgvD,CAJhB,CAI4BrwD,CAJ5B,CAIoCgxD,CAJpC,CAIiDP,CAEvE,IAAInV,CAAJ,CAEE,IADAn6C,CACqB,CADb,EACa,CAAhBkvD,CAAgB,CAAH,CAAG,CAAAW,CAAA,CAAcC,CAAAjxD,OAAnC,CACKqwD,CADL,CACkBW,CADlB,CAEKX,CAAA,EAFL,CAME,IAFAN,CAEe,CAFDkB,CAAA,CAAkBZ,CAAlB,CAEC,CAAXhvD,CAAW,CAAH,CAAG,CAAArB,CAAA,CAAS+vD,CAAA/vD,OAAxB,CAA4CqB,CAA5C,CAAoDrB,CAApD,CAA4DqB,CAAA,EAA5D,CACE,IAAI,CAACkwD,CAAD,CAAiBxB,CAAA,CAAY1uD,CAAZ,CAAA6F,QAAjB,EAA6C,CAA7C,CAAAq0C,SAAJ,CAA8D,CAC5Dh7C,CAAA,CAAMgxD,CAAAjrD,IAAA,EACF8pD,EAAJ,GAAat1C,CAAA,CAAOs1C,CAAP,CAAb,CAA+B7vD,CAA/B,CACA,IAAIiwD,CAAJ,CACE,IAAKC,CAAL,CAAkB,CAAlB,CAAqBA,CAArB,CAAkCjF,CAAAxrD,OAAlC;CACE8a,CAAA,CAAO41C,CAAP,CACI,CADgBlF,CAAA,CAAWiF,CAAX,CAChB,CAAAD,CAAA,CAAQ1mD,CAAR,CAAegR,CAAf,CAAA,EAA0Bva,CAFhC,EAAqDkwD,CAAA,EAArD,EADF,IAME31C,EAAA,CAAO41C,CAAP,CAAA,CAAoBlF,CAAA,CAAWjrD,CAAX,CAEtBY,EAAAN,KAAA,CAAW+B,CAAA,CAAQkH,CAAR,CAAegR,CAAf,CAAX,CAX4D,CAA9D,CATN,IAwBO,CACLva,CAAA,CAAM2uD,CAAA5oD,IAAA,EACN,IAAW,GAAX,EAAI/F,CAAJ,CACEY,CAAA,CAAQxB,CADV,KAEO,IAAY,EAAZ,GAAIY,CAAJ,CACLY,CAAA,CAAQ,IADH,KAGL,IAAIqvD,CAAJ,CACE,IAAKC,CAAL,CAAkB,CAAlB,CAAqBA,CAArB,CAAkCjF,CAAAxrD,OAAlC,CAAqDywD,CAAA,EAArD,CAEE,IADA31C,CAAA,CAAO41C,CAAP,CACI,CADgBlF,CAAA,CAAWiF,CAAX,CAChB,CAAAD,CAAA,CAAQ1mD,CAAR,CAAegR,CAAf,CAAA,EAA0Bva,CAA9B,CAAmC,CACjCY,CAAA,CAAQyB,CAAA,CAAQkH,CAAR,CAAegR,CAAf,CACR,MAFiC,CAAnC,CAHJ,IASEA,EAAA,CAAO41C,CAAP,CAEA,CAFoBlF,CAAA,CAAWjrD,CAAX,CAEpB,CADI6vD,CACJ,GADat1C,CAAA,CAAOs1C,CAAP,CACb,CAD+B7vD,CAC/B,EAAAY,CAAA,CAAQyB,CAAA,CAAQkH,CAAR,CAAegR,CAAf,CAIsB,EAAlC,CAAIm2C,CAAA,CAAkB,CAAlB,CAAAjxD,OAAJ,EACMixD,CAAA,CAAkB,CAAlB,CAAA,CAAqB,CAArB,CAAAvrB,GADN,GACqCnlC,CADrC,GAEI0wD,CAAA,CAAkB,CAAlB,CAAA,CAAqB,CAArB,CAAA1V,SAFJ,CAEuC,CAAA,CAFvC,CAtBK,CA4BP1E,CAAAiB,cAAA,CAAmB32C,CAAnB,CA1DsB,CAAxB,CADoC,CAAtC,CA+DA01C,EAAAoB,QAAA,CAAe0X,CAGf7lD,EAAApF,OAAA,CAAairD,CAAb,CA3GkD,CAhGpD,GAAKpI,CAAA,CAAM,CAAN,CAAL,CAAA,CAF0C,IAItC4H,EAAa5H,CAAA,CAAM,CAAN,CACbyG,EAAAA,CAAczG,CAAA,CAAM,CAAN,CALwB,KAMtCjM,EAAW33C,CAAA23C,SAN2B,CAOtC+V,EAAa1tD,CAAA6tD,UAPyB,CAQtCT,EAAa,CAAA,CARyB,CAStC1B,CATsC,CAYtC+B,EAAiBjqD,CAAA,CAAOzH,CAAAiU,cAAA,CAAuB,QAAvB,CAAP,CAZqB,CAatCu9C,EAAkB/pD,CAAA,CAAOzH,CAAAiU,cAAA,CAAuB,UAAvB,CAAP,CAboB,CActCs6C,EAAgBmD,CAAAhqD,MAAA,EAGZpG,EAAAA,CAAI,CAAZ,KAjB0C,IAiB3B0R,EAAWxL,CAAAwL,SAAA,EAjBgB,CAiBImE,EAAKnE,CAAA1S,OAAnD,CAAoEgB,CAApE,CAAwE6V,CAAxE,CAA4E7V,CAAA,EAA5E,CACE,GAA0B,EAA1B;AAAI0R,CAAA,CAAS1R,CAAT,CAAAG,MAAJ,CAA8B,CAC5BkuD,CAAA,CAAc0B,CAAd,CAA2Br+C,CAAAkT,GAAA,CAAY5kB,CAAZ,CAC3B,MAF4B,CAMhCmuD,CAAAhB,KAAA,CAAgBH,CAAhB,CAA6B+C,CAA7B,CAAyC9C,CAAzC,CAGI3S,EAAJ,GACE0S,CAAA7V,SADF,CACyBsZ,QAAQ,CAACtwD,CAAD,CAAQ,CACrC,MAAO,CAACA,CAAR,EAAkC,CAAlC,GAAiBA,CAAAnB,OADoB,CADzC,CAMIqxD,EAAJ,CAAgB3B,CAAA,CAAe5lD,CAAf,CAAsB5C,CAAtB,CAA+B8mD,CAA/B,CAAhB,CACS1S,CAAJ,CAAcgU,CAAA,CAAgBxlD,CAAhB,CAAuB5C,CAAvB,CAAgC8mD,CAAhC,CAAd,CACAiB,CAAA,CAAcnlD,CAAd,CAAqB5C,CAArB,CAA8B8mD,CAA9B,CAA2CmB,CAA3C,CAjCL,CAF0C,CA7DvC,CANiE,CAApD,CA/yDtB,CAkvEIphD,GAAkB,CAAC,cAAD,CAAiB,QAAQ,CAACuW,CAAD,CAAe,CAC5D,IAAIotC,EAAiB,WACRjvD,CADQ,cAELA,CAFK,CAKrB,OAAO,UACK,GADL,UAEK,GAFL,SAGIsH,QAAQ,CAAC7C,CAAD,CAAUvD,CAAV,CAAgB,CAC/B,GAAId,CAAA,CAAYc,CAAAxC,MAAZ,CAAJ,CAA6B,CAC3B,IAAIyuB,EAAgBtL,CAAA,CAAapd,CAAAyoB,KAAA,EAAb,CAA6B,CAAA,CAA7B,CACfC,EAAL,EACEjsB,CAAAuqB,KAAA,CAAU,OAAV,CAAmBhnB,CAAAyoB,KAAA,EAAnB,CAHyB,CAO7B,MAAO,SAAS,CAAC7lB,CAAD,CAAQ5C,CAAR,CAAiBvD,CAAjB,CAAuB,CAAA,IAEjCpB,EAAS2E,CAAA3E,OAAA,EAFwB,CAGjC4sD,EAAa5sD,CAAA2H,KAAA,CAFIynD,mBAEJ,CAAbxC,EACE5sD,CAAAA,OAAA,EAAA2H,KAAA,CAHeynD,mBAGf,CAEFxC,EAAJ,EAAkBA,CAAAjB,UAAlB,CAGEhnD,CAAAxD,KAAA,CAAa,UAAb,CAAyB,CAAA,CAAzB,CAHF,CAKEyrD,CALF,CAKeuC,CAGX9hC,EAAJ,CACE9lB,CAAApF,OAAA,CAAakrB,CAAb,CAA4BgiC,QAA+B,CAAC9pB,CAAD,CAASC,CAAT,CAAiB,CAC1EpkC,CAAAuqB,KAAA,CAAU,OAAV;AAAmB4Z,CAAnB,CACIA,EAAJ,GAAeC,CAAf,EAAuBonB,CAAAT,aAAA,CAAwB3mB,CAAxB,CACvBonB,EAAAX,UAAA,CAAqB1mB,CAArB,CAH0E,CAA5E,CADF,CAOEqnB,CAAAX,UAAA,CAAqB7qD,CAAAxC,MAArB,CAGF+F,EAAA4Y,GAAA,CAAW,UAAX,CAAuB,QAAQ,EAAG,CAChCqvC,CAAAT,aAAA,CAAwB/qD,CAAAxC,MAAxB,CADgC,CAAlC,CAxBqC,CARR,CAH5B,CANqD,CAAxC,CAlvEtB,CAmyEI2M,GAAiBlL,EAAA,CAAQ,UACjB,GADiB,UAEjB,CAAA,CAFiB,CAAR,CAKfnD,EAAA4K,QAAA1B,UAAJ,CAEEm4B,OAAAE,IAAA,CAAY,gDAAZ,CAFF,EAlvnBA,CAHAluB,EAGA,CAHSrT,CAAAqT,OAGT,GAAcA,EAAA/M,GAAA+Z,GAAd,EACE3Y,CAYA,CAZS2L,EAYT,CAXA9Q,CAAA,CAAO8Q,EAAA/M,GAAP,CAAkB,OACT4f,EAAA7b,MADS,cAEF6b,EAAA4E,aAFE,YAGJ5E,EAAA5B,WAHI,UAIN4B,EAAAlc,SAJM,eAKDkc,EAAAshC,cALC,CAAlB,CAWA,CAFAn1C,EAAA,CAAwB,QAAxB,CAAkC,CAAA,CAAlC,CAAwC,CAAA,CAAxC,CAA8C,CAAA,CAA9C,CAEA,CADAA,EAAA,CAAwB,OAAxB,CAAiC,CAAA,CAAjC,CAAwC,CAAA,CAAxC,CAA+C,CAAA,CAA/C,CACA,CAAAA,EAAA,CAAwB,MAAxB,CAAgC,CAAA,CAAhC,CAAuC,CAAA,CAAvC,CAA8C,CAAA,CAA9C,CAbF,EAeE3K,CAfF,CAeW8L,CA+unBX,CA7unBA5I,EAAAnD,QA6unBA,CA7unBkBC,CA6unBlB,CAFA6F,EAAA,CAAmB3C,EAAnB,CAEA,CAAAlD,CAAA,CAAOzH,CAAP,CAAA+6C,MAAA,CAAuB,QAAQ,EAAG,CAChC/xC,EAAA,CAAYhJ,CAAZ;AAAsBiJ,EAAtB,CADgC,CAAlC,CAZA,CAlpqBqC,CAAtC,CAAA,CAkqqBElJ,MAlqqBF,CAkqqBUC,QAlqqBV,CAoqqBD,EAACD,MAAA4K,QAAAwnD,MAAA,EAAD,EAA2BpyD,MAAA4K,QAAAnD,QAAA,CAAuBxH,QAAvB,CAAAkE,KAAA,CAAsC,MAAtC,CAAA24C,QAAA,CAAsD,oVAAtD;",
"sources":["angular.js"],
"names":["window","document","undefined","minErr","isArrayLike","obj","isWindow","length","nodeType","isString","isArray","forEach","iterator","context","key","isFunction","hasOwnProperty","call","sortedKeys","keys","push","sort","forEachSorted","i","reverseParams","iteratorFn","value","nextUid","index","uid","digit","charCodeAt","join","String","fromCharCode","unshift","setHashKey","h","$$hashKey","extend","dst","arguments","int","str","parseInt","inherit","parent","extra","noop","identity","$","valueFn","isUndefined","isDefined","isObject","isNumber","isDate","toString","isRegExp","location","alert","setInterval","isElement","node","nodeName","prop","attr","find","map","results","list","indexOf","array","arrayRemove","splice","copy","source","destination","stackSource","stackDest","$evalAsync","$watch","ngMinErr","result","Date","getTime","RegExp","shallowCopy","src","charAt","equals","o1","o2","t1","t2","keySet","csp","securityPolicy","isActive","querySelector","bind","self","fn","curryArgs","slice","startIndex","apply","concat","toJsonReplacer","val","toJson","pretty","JSON","stringify","fromJson","json","parse","toBoolean","v","lowercase","startingTag","element","jqLite","clone","empty","e","elemHtml","append","html","TEXT_NODE","match","replace","tryDecodeURIComponent","decodeURIComponent","parseKeyValue","keyValue","key_value","split","toKeyValue","parts","arrayValue","encodeUriQuery","encodeUriSegment","pctEncodeSpaces","encodeURIComponent","angularInit","bootstrap","elements","appElement","module","names","NG_APP_CLASS_REGEXP","name","getElementById","querySelectorAll","exec","className","attributes","modules","doBootstrap","injector","tag","$provide","createInjector","invoke","scope","compile","animate","$apply","data","NG_DEFER_BOOTSTRAP","test","angular","resumeBootstrap","angular.resumeBootstrap","extraModules","snake_case","separator","SNAKE_CASE_REGEXP","letter","pos","toLowerCase","assertArg","arg","reason","assertArgFn","acceptArrayAnnotation","constructor","assertNotHasOwnProperty","getter","path","bindFnToScope","lastInstance","len","getBlockElements","nodes","startNode","endNode","nextSibling","setupModuleLoader","$injectorMinErr","$$minErr","factory","requires","configFn","invokeLater","provider","method","insertMethod","invokeQueue","moduleInstance","runBlocks","config","run","block","publishExternalAPI","version","uppercase","angularModule","$LocaleProvider","ngModule","$$SanitizeUriProvider","$CompileProvider","directive","htmlAnchorDirective","inputDirective","formDirective","scriptDirective","selectDirective","styleDirective","optionDirective","ngBindDirective","ngBindHtmlDirective","ngBindTemplateDirective","ngClassDirective","ngClassEvenDirective","ngClassOddDirective","ngCloakDirective","ngControllerDirective","ngFormDirective","ngHideDirective","ngIfDirective","ngIncludeDirective","ngInitDirective","ngNonBindableDirective","ngPluralizeDirective","ngRepeatDirective","ngShowDirective","ngStyleDirective","ngSwitchDirective","ngSwitchWhenDirective","ngSwitchDefaultDirective","ngOptionsDirective","ngTranscludeDirective","ngModelDirective","ngListDirective","ngChangeDirective","requiredDirective","ngValueDirective","ngIncludeFillContentDirective","ngAttributeAliasDirectives","ngEventDirectives","$AnchorScrollProvider","$AnimateProvider","$BrowserProvider","$CacheFactoryProvider","$ControllerProvider","$DocumentProvider","$ExceptionHandlerProvider","$FilterProvider","$InterpolateProvider","$IntervalProvider","$HttpProvider","$HttpBackendProvider","$LocationProvider","$LogProvider","$ParseProvider","$RootScopeProvider","$QProvider","$SceProvider","$SceDelegateProvider","$SnifferProvider","$TemplateCacheProvider","$TimeoutProvider","$WindowProvider","$$RAFProvider","$$AsyncCallbackProvider","camelCase","SPECIAL_CHARS_REGEXP","_","offset","toUpperCase","MOZ_HACK_REGEXP","jqLitePatchJQueryRemove","dispatchThis","filterElems","getterIfNoArguments","removePatch","param","filter","fireEvent","set","setIndex","setLength","childIndex","children","shift","triggerHandler","childLength","jQuery","originalJqFn","$original","JQLite","trim","jqLiteMinErr","parsed","SINGLE_TAG_REGEXP","fragment","createDocumentFragment","HTML_REGEXP","tmp","appendChild","createElement","TAG_NAME_REGEXP","wrap","wrapMap","_default","innerHTML","XHTML_TAG_REGEXP","removeChild","firstChild","lastChild","j","jj","childNodes","textContent","createTextNode","jqLiteAddNodes","jqLiteClone","cloneNode","jqLiteDealoc","jqLiteRemoveData","jqLiteOff","type","unsupported","events","jqLiteExpandoStore","handle","eventHandler","removeEventListenerFn","expandoId","jqName","expandoStore","jqCache","$destroy","jqId","jqLiteData","isSetter","keyDefined","isSimpleGetter","jqLiteHasClass","selector","getAttribute","jqLiteRemoveClass","cssClasses","setAttribute","cssClass","jqLiteAddClass","existingClasses","root","jqLiteController","jqLiteInheritedData","ii","parentNode","host","jqLiteEmpty","getBooleanAttrName","booleanAttr","BOOLEAN_ATTR","BOOLEAN_ELEMENTS","createEventHandler","event","preventDefault","event.preventDefault","returnValue","stopPropagation","event.stopPropagation","cancelBubble","target","srcElement","defaultPrevented","prevent","isDefaultPrevented","event.isDefaultPrevented","eventHandlersCopy","msie","elem","hashKey","objType","HashMap","put","annotate","$inject","fnText","STRIP_COMMENTS","argDecl","FN_ARGS","FN_ARG_SPLIT","FN_ARG","all","underscore","last","modulesToLoad","supportObject","delegate","provider_","providerInjector","instantiate","$get","providerCache","providerSuffix","factoryFn","loadModules","moduleFn","loadedModules","get","_runBlocks","_invokeQueue","invokeArgs","message","stack","createInternalInjector","cache","getService","serviceName","INSTANTIATING","err","locals","args","Type","Constructor","returnedValue","prototype","instance","has","service","$injector","constant","instanceCache","decorator","decorFn","origProvider","orig$get","origProvider.$get","origInstance","instanceInjector","servicename","autoScrollingEnabled","disableAutoScrolling","this.disableAutoScrolling","$window","$location","$rootScope","getFirstAnchor","scroll","hash","elm","scrollIntoView","getElementsByName","scrollTo","autoScrollWatch","autoScrollWatchAction","$$rAF","$timeout","supported","Browser","$log","$sniffer","completeOutstandingRequest","outstandingRequestCount","outstandingRequestCallbacks","pop","error","startPoller","interval","setTimeout","check","pollFns","pollFn","pollTimeout","fireUrlChange","newLocation","lastBrowserUrl","url","urlChangeListeners","listener","rawDocument","history","clearTimeout","pendingDeferIds","isMock","$$completeOutstandingRequest","$$incOutstandingRequestCount","self.$$incOutstandingRequestCount","notifyWhenNoOutstandingRequests","self.notifyWhenNoOutstandingRequests","callback","addPollFn","self.addPollFn","href","baseElement","self.url","replaceState","pushState","urlChangeInit","onUrlChange","self.onUrlChange","on","hashchange","baseHref","self.baseHref","lastCookies","lastCookieString","cookiePath","cookies","self.cookies","cookieLength","cookie","escape","warn","cookieArray","unescape","substring","defer","self.defer","delay","timeoutId","cancel","self.defer.cancel","deferId","$document","this.$get","cacheFactory","cacheId","options","refresh","entry","freshEnd","staleEnd","n","link","p","nextEntry","prevEntry","caches","size","stats","capacity","Number","MAX_VALUE","lruHash","lruEntry","remove","removeAll","destroy","info","cacheFactory.info","cacheFactory.get","$cacheFactory","$$sanitizeUriProvider","hasDirectives","Suffix","COMMENT_DIRECTIVE_REGEXP","CLASS_DIRECTIVE_REGEXP","EVENT_HANDLER_ATTR_REGEXP","this.directive","registerDirective","directiveFactory","$exceptionHandler","directives","priority","require","controller","restrict","aHrefSanitizationWhitelist","this.aHrefSanitizationWhitelist","regexp","imgSrcSanitizationWhitelist","this.imgSrcSanitizationWhitelist","$interpolate","$http","$templateCache","$parse","$controller","$sce","$animate","$$sanitizeUri","$compileNodes","transcludeFn","maxPriority","ignoreDirective","previousCompileContext","nodeValue","compositeLinkFn","compileNodes","safeAddClass","publicLinkFn","cloneConnectFn","transcludeControllers","$linkNode","JQLitePrototype","eq","$element","addClass","nodeList","$rootElement","boundTranscludeFn","childLinkFn","$node","childScope","nodeListLength","stableNodeList","Array","linkFns","nodeLinkFn","$new","childTranscludeFn","transclude","createBoundTranscludeFn","attrs","linkFnFound","Attributes","collectDirectives","applyDirectivesToNode","terminal","transcludedScope","cloneFn","controllers","scopeCreated","$$transcluded","attrsMap","$attr","addDirective","directiveNormalize","nodeName_","nName","nAttrs","attrStartName","attrEndName","specified","ngAttrName","NG_ATTR_BINDING","substr","directiveNName","addAttrInterpolateDirective","addTextInterpolateDirective","byPriority","groupScan","attrStart","attrEnd","depth","hasAttribute","$compileMinErr","groupElementsLinkFnWrapper","linkFn","compileNode","templateAttrs","jqCollection","originalReplaceDirective","preLinkFns","postLinkFns","addLinkFns","pre","post","directiveName","newIsolateScopeDirective","$$isolateScope","cloneAndAnnotateFn","getControllers","elementControllers","retrievalMethod","optional","linkNode","controllersBoundTransclude","cloneAttachFn","hasElementTranscludeDirective","isolateScope","$$element","LOCAL_REGEXP","templateDirective","$$originalDirective","definition","scopeName","attrName","mode","lastValue","parentGet","parentSet","compare","$$isolateBindings","$observe","$$observers","$$scope","literal","a","b","assign","parentValueWatch","parentValue","controllerDirectives","controllerInstance","controllerAs","$scope","scopeToChild","template","templateUrl","terminalPriority","newScopeDirective","nonTlbTranscludeDirective","hasTranscludeDirective","$compileNode","$template","$$start","$$end","directiveValue","assertNoDuplicate","$$tlb","createComment","replaceWith","replaceDirective","contents","denormalizeTemplate","newTemplateAttrs","templateDirectives","unprocessedDirectives","markDirectivesAsIsolate","mergeTemplateAttributes","compileTemplateUrl","Math","max","tDirectives","startAttrName","endAttrName","srcAttr","dstAttr","$set","tAttrs","linkQueue","afterTemplateNodeLinkFn","afterTemplateChildLinkFn","beforeTemplateCompileNode","origAsyncDirective","derivedSyncDirective","getTrustedResourceUrl","success","content","childBoundTranscludeFn","tempTemplateAttrs","beforeTemplateLinkNode","linkRootElement","oldClasses","response","code","headers","delayedNodeLinkFn","ignoreChildLinkFn","rootElement","diff","what","previousDirective","text","interpolateFn","textInterpolateLinkFn","bindings","interpolateFnWatchAction","getTrustedContext","attrNormalizedName","HTML","RESOURCE_URL","attrInterpolatePreLinkFn","$$inter","newValue","oldValue","$updateClass","elementsToRemove","newNode","firstElementToRemove","removeCount","j2","replaceChild","expando","k","kk","annotation","$addClass","classVal","$removeClass","removeClass","newClasses","toAdd","tokenDifference","toRemove","setClass","writeAttr","booleanKey","removeAttr","listeners","startSymbol","endSymbol","PREFIX_REGEXP","str1","str2","values","tokens1","tokens2","token","CNTRL_REG","register","this.register","expression","identifier","exception","cause","parseHeaders","line","headersGetter","headersObj","transformData","fns","JSON_START","JSON_END","PROTECTION_PREFIX","CONTENT_TYPE_APPLICATION_JSON","defaults","d","interceptorFactories","interceptors","responseInterceptorFactories","responseInterceptors","$httpBackend","$browser","$q","requestConfig","transformResponse","resp","status","reject","transformRequest","mergeHeaders","execHeaders","headerContent","headerFn","header","defHeaders","reqHeaders","defHeaderName","reqHeaderName","common","lowercaseDefHeaderName","xsrfValue","urlIsSameOrigin","xsrfCookieName","xsrfHeaderName","chain","serverRequest","reqData","withCredentials","sendReq","then","promise","when","reversedInterceptors","interceptor","request","requestError","responseError","thenFn","rejectFn","promise.success","promise.error","done","headersString","statusText","resolvePromise","$$phase","deferred","resolve","removePendingReq","idx","pendingRequests","cachedResp","buildUrl","params","defaultCache","timeout","responseType","interceptorFactory","responseFn","createShortMethods","createShortMethodsWithData","createXhr","XMLHttpRequest","ActiveXObject","createHttpBackend","callbacks","$browserDefer","jsonpReq","callbackId","script","async","body","called","addEventListenerFn","onreadystatechange","script.onreadystatechange","readyState","ABORTED","timeoutRequest","jsonpDone","xhr","abort","completeRequest","urlResolve","protocol","counter","open","setRequestHeader","xhr.onreadystatechange","responseHeaders","getAllResponseHeaders","responseText","send","this.startSymbol","this.endSymbol","mustHaveExpression","trustedContext","endIndex","hasInterpolation","startSymbolLength","exp","endSymbolLength","$interpolateMinErr","part","getTrusted","valueOf","newErr","$interpolate.startSymbol","$interpolate.endSymbol","count","invokeApply","clearInterval","iteration","skipApply","$$intervalId","tick","notify","intervals","interval.cancel","short","pluralCat","num","encodePath","segments","parseAbsoluteUrl","absoluteUrl","locationObj","appBase","parsedUrl","$$protocol","$$host","hostname","$$port","port","DEFAULT_PORTS","parseAppUrl","relativeUrl","prefixed","$$path","pathname","$$search","search","$$hash","beginsWith","begin","whole","stripHash","stripFile","lastIndexOf","LocationHtml5Url","basePrefix","$$html5","appBaseNoFile","$$parse","this.$$parse","pathUrl","$locationMinErr","$$compose","this.$$compose","$$url","$$absUrl","$$rewrite","this.$$rewrite","appUrl","prevAppUrl","LocationHashbangUrl","hashPrefix","withoutBaseUrl","withoutHashUrl","windowsFilePathExp","firstPathSegmentMatch","LocationHashbangInHtml5Url","locationGetter","property","locationGetterSetter","preprocess","html5Mode","this.hashPrefix","prefix","this.html5Mode","afterLocationChange","oldUrl","$broadcast","absUrl","LocationMode","initialUrl","ctrlKey","metaKey","which","absHref","animVal","rewrittenUrl","newUrl","$digest","changeCounter","$locationWatch","currentReplace","$$replace","debug","debugEnabled","this.debugEnabled","flag","formatError","Error","sourceURL","consoleLog","console","logFn","log","hasApply","arg1","arg2","ensureSafeMemberName","fullExpression","$parseMinErr","ensureSafeObject","setter","setValue","fullExp","propertyObj","unwrapPromises","promiseWarning","$$v","cspSafeGetterFn","key0","key1","key2","key3","key4","cspSafePromiseEnabledGetter","pathVal","cspSafeGetter","simpleGetterFn1","simpleGetterFn2","getterFn","getterFnCache","pathKeys","pathKeysLength","evaledFnGetter","Function","$parseOptions","this.unwrapPromises","logPromiseWarnings","this.logPromiseWarnings","$filter","promiseWarningCache","parsedExpression","lexer","Lexer","parser","Parser","qFactory","nextTick","exceptionHandler","defaultCallback","defaultErrback","pending","ref","createInternalRejectedPromise","progress","errback","progressback","wrappedCallback","wrappedErrback","wrappedProgressback","catch","finally","makePromise","resolved","handleCallback","isResolved","callbackOutput","promises","requestAnimationFrame","webkitRequestAnimationFrame","mozRequestAnimationFrame","cancelAnimationFrame","webkitCancelAnimationFrame","mozCancelAnimationFrame","webkitCancelRequestAnimationFrame","rafSupported","raf","id","timer","TTL","$rootScopeMinErr","lastDirtyWatch","digestTtl","this.digestTtl","Scope","$id","$parent","$$watchers","$$nextSibling","$$prevSibling","$$childHead","$$childTail","$root","$$destroyed","$$asyncQueue","$$postDigestQueue","$$listeners","$$listenerCount","beginPhase","phase","compileToFn","decrementListenerCount","current","initWatchVal","isolate","child","$$childScopeClass","this.$$childScopeClass","watchExp","objectEquality","watcher","listenFn","watcher.fn","newVal","oldVal","originalFn","deregisterWatch","$watchCollection","veryOldValue","trackVeryOldValue","changeDetected","objGetter","internalArray","internalObject","initRun","oldLength","$watchCollectionWatch","newLength","$watchCollectionAction","watch","watchers","asyncQueue","postDigestQueue","dirty","ttl","watchLog","logIdx","logMsg","asyncTask","$eval","isNaN","next","$on","this.$watch","expr","$$postDigest","namedListeners","$emit","listenerArgs","array1","currentScope","sanitizeUri","uri","isImage","regex","normalizedVal","adjustMatcher","matcher","$sceMinErr","adjustMatchers","matchers","adjustedMatchers","SCE_CONTEXTS","resourceUrlWhitelist","resourceUrlBlacklist","this.resourceUrlWhitelist","this.resourceUrlBlacklist","generateHolderType","Base","holderType","trustedValue","$$unwrapTrustedValue","this.$$unwrapTrustedValue","holderType.prototype.valueOf","holderType.prototype.toString","htmlSanitizer","trustedValueHolderBase","byType","CSS","URL","JS","trustAs","maybeTrusted","allowed","enabled","this.enabled","$sceDelegate","msieDocumentMode","sce","isEnabled","sce.isEnabled","sce.getTrusted","parseAs","sce.parseAs","sceParseAsTrusted","enumValue","lName","eventSupport","android","userAgent","navigator","boxee","documentMode","vendorPrefix","vendorRegex","bodyStyle","style","transitions","animations","webkitTransition","webkitAnimation","hasEvent","divElm","deferreds","$$timeoutId","timeout.cancel","base","urlParsingNode","requestUrl","originUrl","filters","suffix","currencyFilter","dateFilter","filterFilter","jsonFilter","limitToFilter","lowercaseFilter","numberFilter","orderByFilter","uppercaseFilter","comparator","comparatorType","predicates","predicates.check","objKey","filtered","$locale","formats","NUMBER_FORMATS","amount","currencySymbol","CURRENCY_SYM","formatNumber","PATTERNS","GROUP_SEP","DECIMAL_SEP","number","fractionSize","pattern","groupSep","decimalSep","isFinite","isNegative","abs","numStr","formatedText","hasExponent","toFixed","fractionLen","min","minFrac","maxFrac","pow","floor","fraction","lgroup","lgSize","group","gSize","negPre","posPre","negSuf","posSuf","padNumber","digits","neg","dateGetter","date","dateStrGetter","shortForm","jsonStringToDate","string","R_ISO8601_STR","tzHour","tzMin","dateSetter","setUTCFullYear","setFullYear","timeSetter","setUTCHours","setHours","m","s","ms","round","parseFloat","format","DATETIME_FORMATS","NUMBER_STRING","DATE_FORMATS_SPLIT","DATE_FORMATS","object","input","limit","Infinity","out","sortPredicate","reverseOrder","reverseComparator","comp","descending","v1","v2","predicate","arrayCopy","ngDirective","FormController","toggleValidCss","isValid","validationErrorKey","INVALID_CLASS","VALID_CLASS","form","parentForm","nullFormCtrl","invalidCount","errors","$error","controls","$name","ngForm","$dirty","$pristine","$valid","$invalid","$addControl","PRISTINE_CLASS","form.$addControl","control","$removeControl","form.$removeControl","queue","validationToken","$setValidity","form.$setValidity","$setDirty","form.$setDirty","DIRTY_CLASS","$setPristine","form.$setPristine","validate","ctrl","validatorName","validity","addNativeHtml5Validators","$parsers","validator","badInput","customError","typeMismatch","valueMissing","textInputType","placeholder","noevent","composing","ev","ngTrim","$viewValue","$setViewValue","deferListener","keyCode","$render","ctrl.$render","$isEmpty","ngPattern","patternValidator","patternObj","$formatters","ngMinlength","minlength","minLengthValidator","ngMaxlength","maxlength","maxLengthValidator","classDirective","arrayDifference","arrayClasses","classes","digestClassCounts","classCounts","classesToUpdate","ngClassWatchAction","$index","old$index","mod","Object","addEventListener","attachEvent","removeEventListener","detachEvent","_data","JQLite._data","optgroup","option","tbody","tfoot","colgroup","caption","thead","th","td","ready","trigger","fired","removeAttribute","css","currentStyle","lowercasedName","getNamedItem","ret","getText","textProp","NODE_TYPE_TEXT_PROPERTY","$dv","multiple","selected","onFn","eventFns","contains","compareDocumentPosition","adown","documentElement","bup","eventmap","related","relatedTarget","one","off","replaceNode","insertBefore","contentDocument","prepend","wrapNode","after","newElement","toggleClass","condition","classCondition","nextElementSibling","getElementsByTagName","eventName","eventData","arg3","unbind","$animateMinErr","$$selectors","classNameFilter","this.classNameFilter","$$classNameFilter","$$asyncCallback","enter","leave","move","add","PATH_MATCH","paramValue","OPERATORS","null","true","false","+","-","*","/","%","^","===","!==","==","!=","<",">","<=",">=","&&","||","&","|","!","ESCAPE","lex","ch","lastCh","tokens","is","readString","peek","readNumber","isIdent","readIdent","isWhitespace","ch2","ch3","fn2","fn3","throwError","chars","was","isExpOperator","start","end","colStr","peekCh","ident","lastDot","peekIndex","methodName","quote","rawString","hex","rep","ZERO","statements","primary","expect","filterChain","consume","arrayDeclaration","functionCall","objectIndex","fieldAccess","msg","peekToken","e1","e2","e3","e4","t","unaryFn","right","ternaryFn","left","middle","binaryFn","statement","argsFn","fnInvoke","assignment","ternary","logicalOR","logicalAND","equality","relational","additive","multiplicative","unary","field","indexFn","o","safe","contextGetter","fnPtr","elementFns","allConstant","elementFn","keyValues","ampmGetter","getHours","AMPMS","timeZoneGetter","zone","getTimezoneOffset","paddedZone","xlinkHref","propName","normalized","ngBooleanAttrWatchAction","formDirectiveFactory","isNgForm","formElement","action","preventDefaultListener","parentFormCtrl","alias","URL_REGEXP","EMAIL_REGEXP","NUMBER_REGEXP","inputType","numberInputType","minValidator","maxValidator","urlInputType","urlValidator","emailInputType","emailValidator","radioInputType","checked","checkboxInputType","trueValue","ngTrueValue","falseValue","ngFalseValue","ctrl.$isEmpty","NgModelController","$modelValue","NaN","$viewChangeListeners","ngModelGet","ngModel","ngModelSet","this.$isEmpty","inheritedData","this.$setValidity","this.$setPristine","this.$setViewValue","ngModelWatch","formatters","ctrls","modelCtrl","formCtrl","ngChange","required","ngList","viewValue","CONSTANT_VALUE_REGEXP","tpl","tplAttr","ngValue","ngValueConstantLink","ngValueLink","valueWatchAction","ngBind","ngBindWatchAction","ngBindTemplate","ngBindHtml","getStringValue","ngBindHtmlWatchAction","getTrustedHtml","$transclude","previousElements","ngIf","ngIfWatchAction","$anchorScroll","srcExp","ngInclude","onloadExp","onload","autoScrollExp","autoscroll","previousElement","currentElement","cleanupLastIncludeContent","parseAsResourceUrl","ngIncludeWatchAction","afterAnimation","thisChangeId","newScope","$compile","ngInit","BRACE","numberExp","whenExp","whens","whensExpFns","isWhen","attributeName","ngPluralizeWatch","ngPluralizeWatchAction","ngRepeatMinErr","ngRepeat","trackByExpGetter","trackByIdExpFn","trackByIdArrayFn","trackByIdObjFn","valueIdentifier","keyIdentifier","hashFnLocals","lhs","rhs","trackByExp","lastBlockMap","ngRepeatAction","collection","previousNode","nextNode","nextBlockMap","arrayLength","collectionKeys","nextBlockOrder","trackByIdFn","trackById","$first","$last","$middle","$odd","$even","ngShow","ngShowWatchAction","ngHide","ngHideWatchAction","ngStyle","ngStyleWatchAction","newStyles","oldStyles","ngSwitchController","cases","selectedTranscludes","selectedElements","selectedScopes","ngSwitch","ngSwitchWatchAction","change","selectedTransclude","selectedScope","caseElement","anchor","ngSwitchWhen","$attrs","ngOptionsMinErr","NG_OPTIONS_REGEXP","nullModelCtrl","optionsMap","ngModelCtrl","unknownOption","databound","init","self.init","ngModelCtrl_","nullOption_","unknownOption_","addOption","self.addOption","removeOption","self.removeOption","hasOption","renderUnknownOption","self.renderUnknownOption","unknownVal","self.hasOption","setupAsSingle","selectElement","selectCtrl","ngModelCtrl.$render","emptyOption","setupAsMultiple","lastView","items","selectMultipleWatch","setupAsOptions","render","optionGroups","optionGroupNames","optionGroupName","optionGroup","existingParent","existingOptions","modelValue","valuesFn","keyName","groupIndex","selectedSet","lastElement","trackFn","trackIndex","valueName","groupByFn","modelCast","label","displayFn","nullOption","groupLength","optionGroupsCache","optGroupTemplate","existingOption","optionTemplate","optionsExp","track","optionElement","ngOptions","ngModelCtrl.$isEmpty","nullSelectCtrl","selectCtrlName","interpolateWatchAction","$$csp"]
}
;
// This is a manifest file that'll be compiled into including all the files listed below.
// // Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// // be included in the compiled file accessible from http://example.com/assets/application.js
// // It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// // the compiled file.
// //






;
