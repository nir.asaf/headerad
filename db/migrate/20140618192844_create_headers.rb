class CreateHeaders < ActiveRecord::Migration[5.2]
  def change
    create_table :headers do |t|
      t.string :name
      t.string :site
      t.integer :height
      t.string :background
      t.string :logo

      t.timestamps
    end
  end
end
