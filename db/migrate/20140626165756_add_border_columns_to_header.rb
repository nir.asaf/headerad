class AddBorderColumnsToHeader < ActiveRecord::Migration[5.2]
  def change
  	add_column :headers, :show_border, :boolean
  	add_column :headers, :border_color, :string
  	add_column :headers, :box_shadow, :integer
  end
end
