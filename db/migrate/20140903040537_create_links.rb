class CreateLinks < ActiveRecord::Migration[5.2]
  def change
    create_table :links do |t|
      t.integer :header_id
      t.string :links

      t.timestamps
    end
  end
end
