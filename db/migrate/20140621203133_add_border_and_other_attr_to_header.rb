class AddBorderAndOtherAttrToHeader < ActiveRecord::Migration[5.2]
  def change
  	add_column :headers, :split_header, :integer
  	add_column :headers, :add_back_button, :boolean
  	add_column :headers, :border_bottom, :boolean
  end
end
