class AddSocialMediaToHeader < ActiveRecord::Migration[5.2]
  def change
  	add_column :headers, :fb_page, :string
  	add_column :headers, :twitter_page, :string
  	add_column :headers, :header_text, :string
  	add_column :headers, :newsletter_email, :string
  end
end
