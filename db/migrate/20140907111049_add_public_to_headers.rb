class AddPublicToHeaders < ActiveRecord::Migration[5.2]
  def change
    add_column :headers, :public, :boolean
  end
end
