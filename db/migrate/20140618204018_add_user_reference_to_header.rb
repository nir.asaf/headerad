class AddUserReferenceToHeader < ActiveRecord::Migration[5.2]
  def change
    add_reference :headers, :user, index: true
  end
end
