class CreateTrackers < ActiveRecord::Migration[5.2]
  def change
    create_table :trackers do |t|
      t.integer :link_id
      t.integer :header_id

      t.timestamps
    end
  end
end
