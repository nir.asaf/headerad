class CreateNewsletters < ActiveRecord::Migration[5.2]
  def change
    create_table :newsletters do |t|
      t.integer :header_id
      t.string :email

      t.timestamps
    end
  end
end
